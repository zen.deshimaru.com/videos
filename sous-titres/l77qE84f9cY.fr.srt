1
00:00:00,290 --> 00:00:08,820
Je suis la nonne Gyu Ji, ordonnée par mon Maître il y a 40 ans.

2
00:00:08,820 --> 00:00:16,440
Je suis également certifiée par Maître Kosen, dont j’ai reçu le « Shihô ».

3
00:00:16,720 --> 00:00:21,580
Cela me permet d’enseigner,

4
00:00:21,880 --> 00:00:25,200
d’aider à transmettre le zen.

5
00:00:25,520 --> 00:00:29,880
Je vis au temple : c’est moi qui organise,

6
00:00:30,220 --> 00:00:35,400
en dehors des sesshins [retraites spirituelles] la vie du temple.

7
00:00:35,680 --> 00:00:40,680
Qu’il soit entretenu, qu’il accueille des gens en dehors des sesshins.

8
00:00:40,680 --> 00:00:43,700
Qu’il puisse vivre toute l’année.

9
00:00:43,700 --> 00:00:49,950
On est dans le Caroux, à 1h30 de Montpellier, 1h de Béziers, en pleine nature.

10
00:00:49,950 --> 00:00:55,370
Ce temple existe depuis 10 ans maintenant.

11
00:00:55,370 --> 00:01:03,390
La vie du temple Yujo Nyusanji est simple, rythmée.

12
00:01:03,390 --> 00:01:09,600
Rythmée par pratique du zazen. On se lève le matin, on va au dojo, on fait zazen.

13
00:01:09,600 --> 00:01:15,450
Ce que nous enseigne la lignée des Maîtres :
Maître Kosen, qui est à l’origine de ce temple,

14
00:01:15,450 --> 00:01:21,180
son Maître Taisen Deshimaru.
Et on remonte jusqu’à Shakyamuni Bouddha.

15
00:01:21,180 --> 00:01:26,700
Zazen c’est le centre de la pratique de ce temple.

16
00:01:26,700 --> 00:01:33,960
C’est une méditation assise, silencieuse, dans la tranquillité.

17
00:01:33,960 --> 00:01:42,570
Où la posture et l’état d’esprit sont harmonisés.

18
00:01:42,570 --> 00:01:47,820
La vie du temple c’est aussi la vie en commun avec les autres,

19
00:01:48,140 --> 00:01:52,220
les moments où on fait le « samu » : le partage des tâches.

20
00:01:52,500 --> 00:02:00,140
Cuisiner, jardiner, peindre un mur : tout ce qu’il y a à faire pour que ce temple vive.

21
00:02:00,420 --> 00:02:06,960
C’est un lieu ouvert, pas un lieu où on est forcément engagé dans le bouddhisme.

22
00:02:06,960 --> 00:02:12,680
Où on veut forcément devenir un religieux.

23
00:02:12,680 --> 00:02:18,109
C’est un endroit pour tous. Maître Kosen à un moment nous disait :

24
00:02:18,109 --> 00:02:23,390
« Nous sommes des moines laïcs. » C’est important d’être confrontés

25
00:02:23,390 --> 00:02:28,579
à la vie de tout le monde en y incluant notre pratique.

26
00:02:28,579 --> 00:02:33,280
Notre pratique étant le moteur qui nous amène à vivre une vie de tous les jours.

27
00:02:33,280 --> 00:02:40,010
Le temple c’est l’endroit où l’on va
passer un peu de temps pour approfondir sa pratique.

28
00:02:40,010 --> 00:02:45,900
Prendre un peu de recul
par rapport à sa vie quotidienne,

29
00:02:46,440 --> 00:02:51,280
Juste un week-end, un mois, trois mois...

30
00:02:51,780 --> 00:02:59,500
C’est un passage, c’est un moment dans sa vie, dans son année.

31
00:03:00,580 --> 00:03:05,180
La vie du du temple aussi comprend les retraites spirituelles, les sesshins.

32
00:03:05,180 --> 00:03:11,540
On est plus nombreux,
ce sont des périodes plus intensives

33
00:03:11,540 --> 00:03:16,820
de pratique du zazen et de samu.

34
00:03:17,340 --> 00:03:27,520
Dans ce temple, on n’a pas l’optique d’imiter quoi que ce soit qui existe déjà.

35
00:03:27,820 --> 00:03:33,300
C’est un zen tel que Maître Deshimaru l’a enseigné, très créatif,

36
00:03:33,840 --> 00:03:37,800
avec ce qu’on est, avec ce qu’on a.

37
00:03:38,260 --> 00:03:46,120
On pratique aussi les rituels, mais de manière parcimonieuse,

38
00:03:46,440 --> 00:03:51,360
comme dirait Maître Kosen : « La bonne dose, ni trop, ni trop peu ».

39
00:03:51,900 --> 00:03:56,820
Moi, j’habite sur place, et je vous accueille !
