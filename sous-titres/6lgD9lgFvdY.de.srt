1
00:00:00,800 --> 00:00:06,680
Kin-hin "Zen’s meditativer Gang"

2
00:00:07,600 --> 00:00:09,140
Guten Morgen

3
00:00:09,400 --> 00:00:12,060
Heute möchte ich erklären

4
00:00:12,060 --> 00:00:16,600
wie Zen-Meditation Gehen praktiziert wird

5
00:00:16,820 --> 00:00:20,300
die wir "kin hin" nennen

6
00:00:20,400 --> 00:00:23,340
Dieser Spaziergang ist getan

7
00:00:23,340 --> 00:00:26,515
zwischen zwei Perioden sitzender Meditation.

8
00:00:26,520 --> 00:00:31,140
Im Allgemeinen, eine Zen-Meditationssitzung

9
00:00:31,140 --> 00:00:36,060
beginnt mit dreißig oder vierzig Minuten Zazen.

10
00:00:36,360 --> 00:00:38,920
In der Haltung erklärte ich 
(in einem anderen Video):

11
00:00:38,920 --> 00:00:41,740
still, schweigend,

12
00:00:41,740 --> 00:00:43,500
vor der Mauer.

13
00:00:43,660 --> 00:00:46,240
Also, dieser Spaziergang

14
00:00:46,240 --> 00:00:49,860
wird für 5 oder 10 Minuten durchgeführt.

15
00:00:50,140 --> 00:00:53,420
und am Ende dieses Spaziergangs,

16
00:00:53,480 --> 00:00:58,480
gehen wir dorthin zurück, wo wir unser Meditationskissen haben

17
00:00:58,700 --> 00:01:04,160
in Zazen zu sitzen und vor der Wand zu stehen.

18
00:01:04,620 --> 00:01:08,580
Wie gehen Sie, wenn Sie "Kin hin" machen?

19
00:01:08,900 --> 00:01:11,480
Da wir gerade gehen,

20
00:01:11,480 --> 00:01:13,780
Los geht’s.

21
00:01:14,000 --> 00:01:16,820
Wie bewegen wir uns?

22
00:01:17,660 --> 00:01:20,740
Wir stellen unsere Füße richtig.

23
00:01:21,380 --> 00:01:23,500
So ist es nicht.

24
00:01:23,800 --> 00:01:25,500
So ist es nicht.

25
00:01:25,500 --> 00:01:28,480
Parallel und ausgerichtet.

26
00:01:28,480 --> 00:01:34,580
Von einander getrennt, der Abstand einer Faust.

27
00:01:35,300 --> 00:01:40,420
Lassen Sie uns das Äquivalent von einem halben Fuß bewegen.

28
00:01:40,880 --> 00:01:47,180
Ein Fuß misst etwa 30 oder 35 Zentimeter.

29
00:01:47,640 --> 00:01:50,820
Also, wir werden 6 oder 8 Zoll Schritte machen

30
00:01:51,000 --> 00:01:52,300
höchstens.

31
00:01:52,600 --> 00:01:55,555
Wenn Sie auf meine Füße schauen,

32
00:01:55,560 --> 00:01:59,100
werden Sie sehen, wie meine Schritte so ablaufen.

33
00:01:59,240 --> 00:02:02,120
Wir sagen einen halben Fuß.

34
00:02:08,120 --> 00:02:09,120
Nun...

35
00:02:09,520 --> 00:02:12,425
Wie werden wir laufen?

36
00:02:12,425 --> 00:02:16,620
Lassen Sie uns mit unserem Atem gehen.

37
00:02:17,540 --> 00:02:23,840
Unsere Atmung gibt uns den Rhythmus vor.

38
00:02:25,100 --> 00:02:28,320
Inspirieren.

39
00:02:28,320 --> 00:02:31,720
Es ist die gleiche Art des Atmens, die wir während Zazen machen.

40
00:02:31,980 --> 00:02:34,760
Wir inspirieren...

41
00:02:34,760 --> 00:02:38,720
Während dieser Zeit, wenn wir Luft aufnehmen,

42
00:02:38,720 --> 00:02:42,040
ein Bein geht vorwärts

43
00:02:44,955 --> 00:02:47,875
Während der Ausatmung,

44
00:02:47,875 --> 00:02:51,075
langsam und tief,

45
00:02:51,075 --> 00:02:53,725
Herunterdrücken

46
00:02:54,100 --> 00:02:56,960
durch Herunterdrücken des Darms

47
00:02:57,265 --> 00:02:59,440
Während der Ausatmung

48
00:02:59,780 --> 00:03:02,120
an der Nase,

49
00:03:02,660 --> 00:03:07,340
wir werden das ganze Gewicht auf den Körper legen

50
00:03:07,520 --> 00:03:10,800
im Vorderbein.

51
00:03:11,140 --> 00:03:15,160
Vorderes Bein ausgestreckt.

52
00:03:15,160 --> 00:03:18,700
die gesamte Fußsohle ruht auf dem Boden,

53
00:03:18,700 --> 00:03:21,220
einschließlich der Ferse.

54
00:03:21,620 --> 00:03:23,980
Das Hinterbein,

55
00:03:23,980 --> 00:03:26,660
die kein Gewicht hat,

56
00:03:27,060 --> 00:03:31,880
lose gehalten wird.

57
00:03:31,880 --> 00:03:34,300
Aber auch,

58
00:03:34,300 --> 00:03:36,340
die gesamte Fußsohle,

59
00:03:36,360 --> 00:03:42,660
einschließlich der Ferse, bleibt auf dem Boden liegen.

60
00:03:42,660 --> 00:03:46,540
wir heben nicht die Fersen.

61
00:03:47,465 --> 00:03:48,595
Also,

62
00:03:48,920 --> 00:03:53,260
Ich inspiriere und mache den Schritt.

63
00:03:53,460 --> 00:03:57,460
Lange, tiefe Ausatmung.

64
00:03:57,660 --> 00:04:00,500
Während der Ausatmung

65
00:04:00,720 --> 00:04:06,100
das Körpergewicht auf das vordere Bein zu verlagern,

66
00:04:06,100 --> 00:04:11,420
bis zur Wurzel des großen Fingers davor,

67
00:04:11,520 --> 00:04:15,140
als wollte er eine Spur auf dem Boden hinterlassen.

68
00:04:15,300 --> 00:04:21,380
Stellen Sie sich vor, der Boden besteht aus Lehm oder Sand.

69
00:04:22,380 --> 00:04:27,620
Dann würde der Fußabdruck Ihres Fußes aufgezeichnet werden

70
00:04:27,880 --> 00:04:30,900
Inspiration: Ich mache den Schritt.

71
00:04:31,340 --> 00:04:37,740
Ausatmung: Verlagerung des Gewichts vom Körper auf das Vorderbein

72
00:04:37,940 --> 00:04:40,420
Ich habe das Ende meiner Ausatmung erreicht.

73
00:04:40,660 --> 00:04:45,600
Ich habe keine andere Wahl, als wieder etwas Luft zu schnappen.

74
00:04:45,640 --> 00:04:52,060
Ich gebe das weiter hinten liegende Bein nach vorne weiter.

75
00:04:52,300 --> 00:04:55,600
Ich übertrage das Körpergewicht

76
00:04:55,600 --> 00:04:58,680
auf dem Bein, das den Zug gemacht hat

77
00:04:58,680 --> 00:05:02,260
und wieder auf dem anderen Bein.

78
00:05:07,340 --> 00:05:10,620
Der Rhythmus unserer Atmung

79
00:05:10,640 --> 00:05:15,300
das Tempo der Wanderung bestimmen

80
00:05:15,860 --> 00:05:17,800
Offensichtlich,

81
00:05:17,800 --> 00:05:25,100
wir die gleiche geistige Haltung wie bei der Sitzmeditation beibehalten

82
00:05:25,360 --> 00:05:27,920
Ich habe mich hier und jetzt konzentriert,

83
00:05:27,920 --> 00:05:30,000
in dem, was ich tue.

84
00:05:30,000 --> 00:05:32,400
Ich kann meinen Gedanken nicht folgen.

85
00:05:32,400 --> 00:05:35,305
Gleicher Atemzug.

86
00:05:35,305 --> 00:05:39,960
Lediglich die körperliche Aktivität hat sich geändert.

87
00:05:39,960 --> 00:05:45,060
"Die Haltung der Hand"

88
00:05:46,360 --> 00:05:48,380
Während wir gehen,

89
00:05:48,540 --> 00:05:51,660
Was machen wir mit unseren Händen?

90
00:05:52,355 --> 00:05:55,455
wenn wir vor der Mauer stehen,

91
00:05:55,765 --> 00:05:58,545
wir haben diese Einstellung

92
00:06:00,020 --> 00:06:02,935
Lassen Sie die rechte Hand fallen.

93
00:06:02,940 --> 00:06:05,800
Lassen Sie Ihre linke Hand.

94
00:06:05,920 --> 00:06:08,760
Ich wickle meinen Daumen mit meinen Fingern ein.

95
00:06:08,940 --> 00:06:11,920
Alle Finger zusammen,

96
00:06:12,100 --> 00:06:17,020
die Wurzel des linken Daumens herausstehen lassen.

97
00:06:17,260 --> 00:06:19,400
Rechte Hand.

98
00:06:19,540 --> 00:06:22,580
Alle Finger zusammen

99
00:06:22,580 --> 00:06:28,800
Bei den Fingern der rechten Hand ist die linke Hand beteiligt.

100
00:06:29,560 --> 00:06:33,180
Ich werde meine Handgelenke etwas zusammendrücken.

101
00:06:33,760 --> 00:06:36,720
so dass die beiden Unterarme

102
00:06:36,720 --> 00:06:39,720
in der gleichen Linie liegen.

103
00:06:39,720 --> 00:06:45,780
Ich habe keinen Unterarm, der höher ist als der andere.

104
00:06:47,300 --> 00:06:51,860
Die Wurzel des linken Daumens

105
00:06:52,640 --> 00:06:57,260
in Kontakt mit dem Solarplexus steht.

106
00:06:57,820 --> 00:06:59,740
Die Basis des Brustbeins.

107
00:06:59,980 --> 00:07:03,860
Jeder Mensch hat hier ein kleines Loch,

108
00:07:03,880 --> 00:07:05,760
an der Basis des Brustbeins.

109
00:07:05,780 --> 00:07:14,160
Dort müssen Sie Kontakt mit der Daumenwurzel aufnehmen.

110
00:07:15,060 --> 00:07:18,640
Meine Unterarme parallel zum Boden.

111
00:07:18,700 --> 00:07:22,420
Meine Hände auch.

112
00:07:22,820 --> 00:07:25,020
Nicht auf diese Weise.

113
00:07:25,480 --> 00:07:30,080
Ich halte diese Haltung mit entspannten Schultern.

114
00:07:30,200 --> 00:07:33,360
Ich halte diese Haltung mit meinen Schultern nicht so:

115
00:07:33,400 --> 00:07:35,360
mit den Ellenbogen nach oben.

116
00:07:35,500 --> 00:07:38,260
aber auch nicht auf diese Weise.

117
00:07:40,820 --> 00:07:42,880
Wie en zazen:

118
00:07:42,940 --> 00:07:44,600
gerade Spalte,

119
00:07:44,680 --> 00:07:46,020
Nacken verlängert,

120
00:07:46,360 --> 00:07:48,320
Kinn kam herein,

121
00:07:48,340 --> 00:07:51,920
Sehen Sie sich die 45. Diagonale an.

122
00:07:54,520 --> 00:07:57,320
Nun, wie werde ich laufen?

123
00:07:57,900 --> 00:08:02,600
Während der Inspiration,

124
00:08:02,600 --> 00:08:04,120
wir machen einen Schritt vorwärts.

125
00:08:06,800 --> 00:08:09,660
Beim Ausatmen

126
00:08:10,840 --> 00:08:15,740
...übertragen Sie das Körpergewicht von einem Bein auf das andere.

127
00:08:15,820 --> 00:08:18,060
genau wie ich erklärt habe.

128
00:08:18,060 --> 00:08:20,240
Zur gleichen Zeit,

129
00:08:20,240 --> 00:08:25,740
werden Sie leicht die Hand schütteln.

130
00:08:25,740 --> 00:08:30,500
Deshalb ist Flexibilität in den Handgelenken wichtig.

131
00:08:31,980 --> 00:08:33,560
Zur gleichen Zeit,

132
00:08:33,680 --> 00:08:38,235
ein leichter Druck von der Wurzel des linken Daumens

133
00:08:38,235 --> 00:08:41,405
gegen den Solarplexus.

134
00:08:42,020 --> 00:08:45,120
All das während der Ausatmung.

135
00:08:46,120 --> 00:08:51,540
Lebenslauf

136
00:08:53,040 --> 00:08:56,500
Linke Hand, Daumen, Finger zusammen,

137
00:08:56,520 --> 00:08:58,760
rechte Hand,

138
00:08:58,760 --> 00:08:59,580
oben.

139
00:08:59,780 --> 00:09:04,000
Die Wurzel des linken Daumens auf dem Brustbein.

140
00:09:04,160 --> 00:09:08,880
Ich inspiriere.

141
00:09:09,760 --> 00:09:13,680
Mein Bein geht vorwärts

142
00:09:14,140 --> 00:09:16,120
Ausatmungsphase:

143
00:09:16,300 --> 00:09:17,340
En wie viel

144
00:09:17,480 --> 00:09:22,120
Ich verlagere mein gesamtes Körpergewicht auf dieses Bein,

145
00:09:22,120 --> 00:09:24,480
gut gestreckt,

146
00:09:24,800 --> 00:09:27,920
Handpresse

147
00:09:27,920 --> 00:09:30,340
und gegen den Solarplexus.

148
00:09:31,280 --> 00:09:32,620
Inspiration.

149
00:09:32,860 --> 00:09:35,620
Einen Schritt voraus.

150
00:09:35,620 --> 00:09:37,840
Lange und tiefe Ausatmung

151
00:09:38,020 --> 00:09:41,300
das Gewicht auf dem vorderen Bein.

152
00:09:41,320 --> 00:09:45,940
Druck zwischen den Händen und gegen den Solarplexus.

153
00:09:48,440 --> 00:09:51,180
Ende der Ausatmung

154
00:09:51,320 --> 00:09:55,640
Ich mache einen Schritt und nutze die Gelegenheit, mich zu entspannen.

155
00:09:55,740 --> 00:09:57,500
Ich mach’s noch mal...

156
00:09:57,540 --> 00:10:00,660
Ich drücke den Boden

157
00:10:00,820 --> 00:10:03,815
mit dem Gewicht meines Körpers.

158
00:10:03,820 --> 00:10:05,040
Druck zwischen den Händen

159
00:10:05,240 --> 00:10:07,380
und gegen den Plexus.

160
00:10:07,420 --> 00:10:09,460
Der Druck ist sanft.

161
00:10:09,460 --> 00:10:13,000
Es geht nicht darum, verletzt zu werden.

162
00:10:14,140 --> 00:10:15,840
All dies,

163
00:10:15,840 --> 00:10:20,340
geleitet durch den Rhythmus unserer eigenen Atmung,

164
00:10:20,340 --> 00:10:22,460
für 5 oder 10 Minuten

165
00:10:22,660 --> 00:10:24,920
ist, dass wir dann zurückkommen,

166
00:10:24,920 --> 00:10:28,980
dorthin, wo unser Meditationskissen ist.

167
00:10:29,280 --> 00:10:30,280
Im täglichen Leben,

168
00:10:30,560 --> 00:10:34,740
dieser meditative Spaziergang mit dem Namen "Kin hin".

169
00:10:35,560 --> 00:10:37,940
ist sehr praktisch, sehr effizient.

170
00:10:38,960 --> 00:10:43,040
Vielleicht sind Sie in einem Moment des Wartens

171
00:10:43,520 --> 00:10:46,075
oder in Zeiten von Stress.

172
00:10:46,075 --> 00:10:47,805
Und Sie können sich isolieren

173
00:10:48,200 --> 00:10:50,860
für 2, 3 oder 5 Minuten.

174
00:10:50,980 --> 00:10:54,340
Wo werden Sie sich mit dem Hier und Jetzt verbinden?

175
00:10:54,815 --> 00:10:57,965
Im Bewusstsein Ihres Atems

176
00:10:57,965 --> 00:11:03,840
und unter Berücksichtigung der von mir erläuterten Punkte.

177
00:11:04,620 --> 00:11:07,755
Diesen Meditationsweg üben

178
00:11:07,760 --> 00:11:15,480
Sie werden sehen, wie schnell Sie sich weniger gestresst fühlen

179
00:11:15,480 --> 00:11:21,880
entspannter und in Harmonie.

180
00:11:22,880 --> 00:11:27,820
Ich hoffe, Sie können jetzt ein volles Zazen machen.

181
00:11:27,820 --> 00:11:31,060
Das heißt, ein erster Sitzungsteil,

182
00:11:31,060 --> 00:11:32,055
dann,

183
00:11:32,055 --> 00:11:34,855
5 oder 10 Minuten dieses Spaziergangs

184
00:11:35,360 --> 00:11:38,960
und gehen Sie zurück zu einem zweiten Teil der Meditation.

185
00:11:39,140 --> 00:11:40,300
Vielen Dank.