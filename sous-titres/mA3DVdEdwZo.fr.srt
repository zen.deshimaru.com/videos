1
00:00:02,555 --> 00:00:04,935
On a parlé dans le kusen…

2
00:00:05,365 --> 00:00:08,125
ici en Europe…

3
00:00:14,475 --> 00:00:17,625
… de la volition.

4
00:00:18,965 --> 00:00:21,545
Ce n’est pas la volonté.

5
00:00:22,980 --> 00:00:27,800
La volition est une chose qui se déclenche

6
00:00:42,860 --> 00:00:44,860
…inconsciemment.

7
00:00:56,040 --> 00:00:58,740
Après, j’expliquais…

8
00:01:00,840 --> 00:01:03,660
… ici, en Occident…

9
00:01:11,580 --> 00:01:16,280
D’abord, bonjour à tous !

10
00:01:21,100 --> 00:01:23,915
Partageons ce zazen en ce moment.

11
00:01:27,795 --> 00:01:29,465
avec grand plaisir

12
00:01:41,220 --> 00:01:46,880
Nous avons réussi à organiser et à pratiquer un camp d’été.

13
00:01:49,540 --> 00:01:53,520
malgré les événements compliqués

14
00:01:56,080 --> 00:01:59,870
Nous sommes en nombre limité, car vous n’êtes pas venus

15
00:02:05,140 --> 00:02:08,080
à cause de l’espace de sécurité

16
00:02:13,600 --> 00:02:19,900
Nous sommes obligés de nous espacer davantage dans le Dojo

17
00:02:20,140 --> 00:02:25,820
Nous avons tout fait sérieusement, nous avons suivi les règles

18
00:02:48,715 --> 00:02:52,160
C’est une des…

19
00:02:52,160 --> 00:02:54,860
spécificités du zen

20
00:02:58,940 --> 00:03:01,860
que de suivre la règle

21
00:03:02,380 --> 00:03:07,780
"Ouais, mais moi j’ai pas été éduqué comme ça… je suis pas comme ça, moi !"

22
00:03:12,340 --> 00:03:16,840
Dans le zen… on suit la règle

23
00:03:20,685 --> 00:03:23,535
La règle qu’il y a là où l’on est.

24
00:03:33,200 --> 00:03:36,580
J’ai donc expliqué que la volition

25
00:03:39,480 --> 00:03:42,380
est Yin ou Yang

26
00:03:47,760 --> 00:03:52,500
Quand je dis qu’il faut relâcher les jambes, c’est la volition Yin

27
00:03:53,840 --> 00:03:56,220
On lâche

28
00:04:06,125 --> 00:04:08,765
En lâchant, on enclenche une résonance

29
00:04:19,460 --> 00:04:23,340
la volition Yang…

30
00:04:23,640 --> 00:04:26,820
c’est pousser la terre avec les genoux

31
00:04:40,600 --> 00:04:43,640
J’ai raconté l’histoire

32
00:04:46,480 --> 00:04:48,380
et les réflexions

33
00:04:50,300 --> 00:04:52,760
Sur la méthode Yang de Baso

34
00:04:54,760 --> 00:04:57,500
Et la méthode Yin de Sekito

35
00:04:59,440 --> 00:05:03,380
Je vous fais partager ce kusen

36
00:05:22,140 --> 00:05:25,300
C’est une histoire très célèbre

37
00:05:27,420 --> 00:05:31,780
mais vue sous un autre angle que d’habitude

38
00:05:33,960 --> 00:05:39,300
On parle de Baso qui avait déjà reçu la transmission du Dharma

39
00:05:40,620 --> 00:05:42,680
De son maître

40
00:05:49,820 --> 00:05:54,400
Il a continué à suivre l’enseignement de son maître et les sesshins

41
00:06:03,270 --> 00:06:08,320
Il avait une drôle de manie

42
00:06:11,580 --> 00:06:18,180
Il allait faire zazen seul dans le Dojo même si ce n’était pas l’heure

43
00:06:19,740 --> 00:06:23,730
Il avait toujours envie de faire zazen

44
00:06:28,320 --> 00:06:34,360
Un jour, son maître vient le voir alors qu’il était assis

45
00:06:36,360 --> 00:06:38,400
et il lui demande

46
00:06:40,480 --> 00:06:42,920
Grand moine,

47
00:06:43,425 --> 00:06:46,645
-Baso était très grand-

48
00:06:48,400 --> 00:06:53,700
Que recherchez-vous en vous asseyant en zazen ?

49
00:06:57,060 --> 00:06:58,640
Autrement dit :

50
00:06:58,860 --> 00:07:03,920
que cherchez-vous ? Avez-vous un but ?

51
00:07:04,300 --> 00:07:08,560
Avez-vous un but au-delà…

52
00:07:10,580 --> 00:07:15,000
qui soit plus important que de simplement s’asseoir ?

53
00:07:20,040 --> 00:07:22,540
Cette histoire est très connue

54
00:07:24,325 --> 00:07:28,875
C’est l’aspect Yang de la volition

55
00:07:31,705 --> 00:07:34,625
Après, j’ai inventé les dialogues

56
00:07:41,045 --> 00:07:44,175
"- Vous avez un but ?"

57
00:07:44,565 --> 00:07:47,525
"-Oui, j’ai un but." dit Baso.

58
00:07:50,435 --> 00:07:54,045
Je veux devenir Bouddha. Je vise à devenir Bouddha.

59
00:07:56,345 --> 00:07:59,445
Alors…

60
00:07:59,535 --> 00:08:02,035
Je vise

61
00:08:02,225 --> 00:08:05,195
le centre de la cible

62
00:08:08,525 --> 00:08:11,655
et je l’atteins à tous les coups

63
00:08:21,920 --> 00:08:25,000
C’est l’aspect Yang du zazen

64
00:08:25,240 --> 00:08:26,720
dont j’ai parlé hier

65
00:08:33,620 --> 00:08:35,080
J’ai raconté

66
00:08:36,800 --> 00:08:42,360
comment Maître Deshimaru nous enseignait quand on était jeunes

67
00:08:49,120 --> 00:08:53,785
Il faisait régner une grande tension dans le dojo avec son kyosaku

68
00:08:55,045 --> 00:08:57,875
C’est la méthode Yang

69
00:08:59,385 --> 00:09:02,385
Dès que quelqu’un bougeait

70
00:09:03,105 --> 00:09:05,395
ou s’endormait

71
00:09:06,385 --> 00:09:09,345
il le frappait

72
00:09:09,545 --> 00:09:12,765
Nous étions en camp d’été

73
00:09:12,875 --> 00:09:15,405
Il faisait très chaud

74
00:09:15,915 --> 00:09:17,455
on était trempés

75
00:09:19,765 --> 00:09:22,925
il y avait une flaque d’eau sur le tatami après zazen

76
00:09:32,260 --> 00:09:37,340
"Je m’assieds pour devenir un Bouddha, un point c’est tout. J’y mets toute mon énergie.

77
00:09:40,910 --> 00:09:43,550
C’est la méthode de Deshimaru

78
00:09:45,080 --> 00:09:48,490
Pratiquer de toutes ses forces pour le plaisir

79
00:09:51,435 --> 00:09:54,475
C’est ce que faisait Baso

80
00:10:00,980 --> 00:10:03,980
Alors la méthode Yin,

81
00:10:05,960 --> 00:10:10,240
Je voulais dire que Baso n’avait aucun doute

82
00:10:13,420 --> 00:10:19,180
Ça lui était égal de savoir s’il avait un but ou non, s’il était Mushotoku ou non.

83
00:10:24,460 --> 00:10:28,020
C’était la volition. Il avait envie, il avait la volonté.

84
00:10:37,535 --> 00:10:40,345
Sensei disait :

85
00:10:41,185 --> 00:10:44,415
il vaut mieux faire moins longtemps zazen

86
00:10:48,985 --> 00:10:51,825
mais faire un zazen fort, comme Baso

87
00:10:58,875 --> 00:11:02,865
Plutôt que faire longtemps zazen, toute la journée,

88
00:11:06,525 --> 00:11:10,005
et de dormir ou penser

89
00:11:18,425 --> 00:11:19,425
20 minutes

90
00:11:19,565 --> 00:11:22,445
de toutes ses forces

91
00:11:24,055 --> 00:11:26,785
c’est très efficace

92
00:11:31,355 --> 00:11:33,725
Quand on aime le zazen,

93
00:11:35,995 --> 00:11:38,475
pour le plaisir…

94
00:11:42,375 --> 00:11:45,085
Nous ne sommes pas des prosélytes.

95
00:11:56,515 --> 00:12:01,235
On partage, simplement, pour le plaisir

96
00:12:41,325 --> 00:12:44,085
Revenons à l’exemple Yin,

97
00:12:44,205 --> 00:12:45,865
relâcher les jambes

98
00:12:47,820 --> 00:12:51,610
quand je relâche les jambes, ça relâche tout le bassin

99
00:12:54,180 --> 00:12:57,340
J’ai l’impression d’être en apesanteur

100
00:13:10,580 --> 00:13:13,040
L’exemple qu’il est donné

101
00:13:14,165 --> 00:13:18,815
c’est d’arriver au moment tant attendu où nous allons faire une sieste

102
00:13:26,135 --> 00:13:29,145
Là où on lâche tout

103
00:13:30,775 --> 00:13:33,425
pour s’endormir

104
00:13:35,935 --> 00:13:39,845
L’exemple de la sieste est évidemment une métaphore

105
00:13:42,045 --> 00:13:45,005
qui fait référence à zazen

106
00:13:59,545 --> 00:14:02,295
Cependant, 7 siècles avant Jésus-Christ

107
00:14:09,240 --> 00:14:12,240
un scientifique nommé Pythagore

108
00:14:14,595 --> 00:14:16,545
disait :

109
00:14:18,960 --> 00:14:21,300
soyez attentifs à la nuit

110
00:14:27,500 --> 00:14:33,120
comme vous ne contrôlez pas la nuit

111
00:14:34,600 --> 00:14:39,980
Soyez attentifs au moment de l’endormissement

112
00:14:43,600 --> 00:14:48,240
Ne vous endormez pas sans contrôler ce dernier moment avant de s’endormir

113
00:15:02,695 --> 00:15:05,735
Le zazen est composé de ces deux aspects

114
00:15:05,885 --> 00:15:08,785
le yin et le yang

115
00:15:11,440 --> 00:15:16,260
qu’exprimaient chacun à leur tour par les grands maîtres Baso et Sekito

116
00:15:21,370 --> 00:15:36,380
L’un peut être appelé "Tuer le Bouddha" et l’autre "Lâcher le Bouddha".

117
00:15:41,680 --> 00:15:49,020
Le grand maître Sekito était son contemporain. C’étaient des moines qui vivaient à la même époque.

118
00:15:52,300 --> 00:15:56,300
Baso et Sekito étaient de la même époque.

119
00:16:00,980 --> 00:16:04,560
Souvent, les disciples de l’un allaient voir l’autre.

120
00:16:10,570 --> 00:16:14,000
A cette époque, Sekito n’avait pas encore de disciples

121
00:16:17,455 --> 00:16:20,865
Il vivait dans les bois… dans la montagne

122
00:16:22,445 --> 00:16:24,945
Il s’était construit

123
00:16:25,385 --> 00:16:28,405
une petite hutte

124
00:16:34,335 --> 00:16:36,915
avec du bois et de la paille

125
00:16:44,315 --> 00:16:48,895
sur la même montagne où avait enseigné Maître Nangaku

126
00:16:54,435 --> 00:16:57,825
Sekito dit :

127
00:16:58,545 --> 00:17:01,475
"Je me sens protégé dans cette hutte"

128
00:17:02,865 --> 00:17:07,355
C’est moi qui l’ai construite. C’est ma seule richesse.

129
00:17:10,965 --> 00:17:13,395
Mais quand j’ai fini mon repas,

130
00:17:16,045 --> 00:17:19,015
Je prépare calmement le moment de la sieste

131
00:17:23,655 --> 00:17:26,825
et je le répète ça chaque jour

132
00:17:29,295 --> 00:17:34,525
et encore chaque jour

133
00:17:38,875 --> 00:17:41,665
L’expérience du repas et de la sieste,

134
00:17:44,135 --> 00:17:46,955
est un pouvoir des patriarches

135
00:17:50,035 --> 00:17:53,055
celui qui n’a pas encore fini son repas,

136
00:17:56,345 --> 00:18:03,075
n’est pas encore allé au bout de l’expérience.

137
00:18:08,215 --> 00:18:10,015
Il n’est pas encore satisfait.
