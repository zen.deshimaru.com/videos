1
00:00:00,280 --> 00:00:03,640
Vida cotidiana 

2
00:00:03,840 --> 00:00:06,460
Un dia … 

3
00:00:07,200 --> 00:00:12,320
Durante el día tenemos que parar lo que hacemos regularmente. 

4
00:00:13,040 --> 00:00:15,320
Deja su trabajo. 

5
00:00:15,780 --> 00:00:18,780
Y vamos 

6
00:00:19,020 --> 00:00:22,220
En su trabajo, en la vida cotidiana, encuentras la mente. 

7
00:00:22,560 --> 00:00:24,720
Solo toma dos respiraciones. 

8
00:00:25,060 --> 00:00:26,340
Trabajas en tu escritorio … 

9
00:00:26,680 --> 00:00:28,820
Solo enderezarlo. 

10
00:00:29,340 --> 00:00:32,760
Y respira. 

11
00:00:41,120 --> 00:00:49,260
Y volver inmediatamente a su ser. 

12
00:00:50,960 --> 00:00:54,140
Conocía una comunidad llamada Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
Ellos trabajan muy duro. 

14
00:00:57,580 --> 00:01:00,400
Ellos trabajan la tierra. 

15
00:01:00,660 --> 00:01:02,660
Ellos construyen su casa. 

16
00:01:02,880 --> 00:01:04,540
Hacen mucho trabajo manual. 

17
00:01:04,740 --> 00:01:06,040
Trabajan todo el dia. 

18
00:01:06,300 --> 00:01:07,500
Se levantan a las 5 a.m. 

19
00:01:07,720 --> 00:01:08,820
Dicen sus oraciones. 

20
00:01:09,080 --> 00:01:10,840
Ellos están desayunando. 

21
00:01:11,160 --> 00:01:12,420
Ellos van a trabajar 

22
00:01:12,780 --> 00:01:15,820
Suena una campana cada hora. 

23
00:01:16,180 --> 00:01:18,060
Todos tienen que dejar de trabajar. 

24
00:01:18,720 --> 00:01:25,520
Y reenfoque por un minuto. 

25
00:01:26,060 --> 00:01:28,960
Cada uno a su manera. 

26
00:01:34,080 --> 00:01:36,040
Y luego regresan. 

27
00:01:36,560 --> 00:01:39,160
Entonces no solo hay zazen. 

28
00:01:39,540 --> 00:01:42,100
Hay muchas maneras de volver a ti mismo. 

29
00:01:42,640 --> 00:01:45,220
La vida cotidiana de los monjes zen … 

30
00:01:45,540 --> 00:01:48,820
Vivimos nuestras vidas por zazen. 

31
00:01:49,080 --> 00:01:52,150
Para recoger esta conciencia, este conocimiento. 

32
00:01:52,440 --> 00:01:53,580
Esa energía 

33
00:01:54,880 --> 00:01:58,780
En el momento de la experiencia zazen. 

34
00:01:58,900 --> 00:02:03,120
No se trata de renunciar a la vida por Zazen. 

35
00:02:03,380 --> 00:02:05,400
En un espíritu sectario. 

36
00:02:05,740 --> 00:02:07,680
No es una ideología. 

37
00:02:07,900 --> 00:02:10,380
No es "-ismo". 

38
00:02:10,840 --> 00:02:14,780
Es: "Vivo mi vida de tal manera que puedo tener esta experiencia". 

39
00:02:15,060 --> 00:02:16,560
"Lo más profundo posible". 

40
00:02:16,900 --> 00:02:18,980
Esta es una experiencia de meditación zazen. 

41
00:02:19,220 --> 00:02:25,060
Cuando estás en zazen, tienes la oportunidad de cambiar toda tu vida. 

42
00:02:25,400 --> 00:02:27,900
¡De eso se trata el zazen! 

43
00:02:28,340 --> 00:02:31,980
No es una pequeña forma de sentirse bien. 

44
00:02:32,500 --> 00:02:35,040
Zen en la vida cotidiana … 

45
00:02:35,640 --> 00:02:39,200
Incluso en beneficio y trabajo … 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku", (sin objetivo ni espíritu de lucro) … 

47
00:02:41,760 --> 00:02:43,140
Ese es el secreto. 

48
00:02:43,340 --> 00:02:46,320
En otras palabras, manténgase enfocado en su esencia. 

49
00:02:46,540 --> 00:02:48,880
Entonces podemos obtener todo. 

50
00:02:49,080 --> 00:02:50,360
Lo tenemos todo 

51
00:02:50,880 --> 00:02:55,280
¿Cuál es la diferencia entre la vida de un maestro y la de un ser ordinario? 

52
00:02:55,660 --> 00:02:59,520
No existe lo ordinario. 

53
00:02:59,840 --> 00:03:05,780
Solo hay criaturas extraordinarias. 

54
00:03:06,660 --> 00:03:14,900
Lo malo es que las personas no saben que son extraordinarias. 

55
00:03:16,420 --> 00:03:22,260
¡No me gusta la frase "solo sé"! 

