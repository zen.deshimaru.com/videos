1
00:00:10,260 --> 00:00:23,160
Tous les jours, partout dans le monde, des membres de la Kosen sangha pratiquent zazen ensemble.

2
00:00:24,360 --> 00:00:28,520
Je vous ai parlé de la jeunesse de Maître Deshimaru.

3
00:00:28,820 --> 00:00:35,760
De ses doutes, de ses recherches, jusqu’à sa rencontre avec Kodo Sawaki et sa posture de zazen.

4
00:00:37,100 --> 00:00:41,860
La rencontre avec sa propre posture de zazen : quel bonheur !

5
00:00:42,240 --> 00:00:49,120
Je crois que ceux qui ont rencontré leur posture de zazen n’ont pas perdu leur temps sur cette planète.

6
00:00:49,500 --> 00:00:54,239
C’est un cadeau incommensurable, même si dans cette posture

7
00:00:54,540 --> 00:00:59,920
il y a aussi de la souffrance et de l’angoisse.

8
00:01:00,320 --> 00:01:06,160
Dans cette posture, il y a tout, mais que l’on regarde depuis le sommet.

9
00:01:06,500 --> 00:01:17,040
Avant de rencontrer l’éducation de Kodo Sawaki, Maître Deshimaru avait étudié la bible.

10
00:01:17,360 --> 00:01:24,440
Il était un peu amoureux d’une Hollandaise, fille d’un pasteur.

11
00:01:24,960 --> 00:01:31,220
Il en avait profité pour étudier la bible, et l’anglais.

12
00:01:32,140 --> 00:01:37,060
Il a fréquenté les églises chrétiennes et chanté des cantiques.

13
00:01:37,220 --> 00:01:41,360
Je vous raconte ça parce qu’on est à Pâques.

14
00:01:41,660 --> 00:01:51,880
Et actuellement, Jésus est entre son cercueil et la cinquième dimension.

15
00:01:52,340 --> 00:01:56,580
Sensei nous racontait que lorsqu’il est venu en France,

16
00:01:57,000 --> 00:02:02,360
il a apporté sa bible et les cantiques de sa jeunesse.

17
00:02:02,680 --> 00:02:07,940
Pendant une sesshin de Pâques, il nous a fait un kusen sur le Christ.

18
00:02:08,280 --> 00:02:15,620
Selon l’évangile selon Saint Luc, lorsque le Christ a dû s’expliquer

19
00:02:16,140 --> 00:02:20,240
devant Ponce Pilate pour être jugé,

20
00:02:20,600 --> 00:02:28,820
Ponce Pilate convoqua les grands prêtres et interrogea Jésus devant eux.

21
00:02:29,180 --> 00:02:30,340
Il leur dit :

22
00:02:30,600 --> 00:02:39,820
« Vous m’avez présenté cet homme comme détournant le peuple dans une mauvaise direction. »

23
00:02:40,280 --> 00:02:47,640
« Je l’ai interrogé et n’ai rien trouvé en cet homme de mauvais. »

24
00:02:48,000 --> 00:02:50,260
« Je n’ai trouvé aucun des motifs dont vous l’accusez. »

25
00:02:50,580 --> 00:02:56,940
« Hérode n’a pas non plus trouvé de motif d’accusation. »

26
00:02:57,260 --> 00:03:02,620
« J’en conclus que cet homme n’a rien fait qui mérite la mort. »

27
00:03:03,240 --> 00:03:15,100
À cet instant, le peuple s’est écrié : « À mort Jésus, relâche Barabbas ! »

28
00:03:16,240 --> 00:03:24,280
« Barabbas avait été emprisonné pour trouble et pour meurtre. »

29
00:03:24,620 --> 00:03:32,200
Ponce Pilate proposait de libérer un des prisonniers.

30
00:03:32,800 --> 00:03:38,480
Mais le peuple disait « Libère Barabbas ! »

31
00:03:38,940 --> 00:03:49,620
Ponce Pilate s’adresse encore au peuple :

32
00:03:49,960 --> 00:03:55,120
« Soyez raisonnables, Jésus n’a rien fait de mal, il a juste parlé ! »

33
00:03:55,420 --> 00:03:59,620
Tout le monde cria « Crucifie-le ! »

34
00:03:59,860 --> 00:04:02,320
Ponce Pilate a dit finalement :

35
00:04:02,620 --> 00:04:07,280
« Comme vous voulez, ce n’est pas mon problème… »

36
00:04:07,720 --> 00:04:13,240
Il a dit la phrase célèbre : « Je m’en lave les mains ! »

37
00:04:13,560 --> 00:04:22,100
Ça veut dire qu’il faut se laver les mains le plus souvent possible pour éviter le coronavirus.

38
00:04:22,400 --> 00:04:24,920
Il faut dire à chaque fois : « Je m’en lave les mains ! »

39
00:04:25,240 --> 00:04:29,880
Maître Deshimaru dit ceci :

40
00:04:30,360 --> 00:04:35,760
« Ainsi, Jésus a été condamné par l’opinion publique. »

41
00:04:36,020 --> 00:04:39,140
« La psychologie de la foule a tué le Christ. »

42
00:04:39,380 --> 00:04:46,000
« Le mimétisme, le refus de se mettre en question ont tué le Christ. »

43
00:04:46,300 --> 00:04:57,080
La foule contient une énergie puissante qui peut être négative et dangereuse.

44
00:04:57,800 --> 00:05:02,900
Quand on se réunit pour faire zazen, c’est différent.

45
00:05:03,180 --> 00:05:11,820
Car chacun se regarde lui-même tout en étant à l’unisson avec le groupe.

46
00:05:12,160 --> 00:05:16,260
C’est très important surtout pour Zazoom !

47
00:05:16,680 --> 00:05:20,520
J’ai fait brûler de l’encens dans la pièce, par exemple.

48
00:05:20,920 --> 00:05:25,260
Sensei a souvent dit « En zazen, on est seul. »

49
00:05:25,640 --> 00:05:31,460
Mais dans un dojo, on fait zazen en groupe, même si on est tout seul.

50
00:05:31,820 --> 00:05:35,260
Mais il n’y a pas alors de confusion émotionnelle.

51
00:05:35,560 --> 00:05:41,640
Kodo Sawaki disait : « Nous ne devons pas vivre dans l’imbécilité de groupe. »

52
00:05:42,020 --> 00:05:45,680
« Et prendre cette aberration pour l’expérience véritable. »

53
00:05:45,940 --> 00:05:50,540
« Ceci est le lot des gens ordinaires qui ne savent pas voir les choses autrement »

54
00:05:50,860 --> 00:05:54,180
« qu’avec les yeux de l’imbécilité collective. »

55
00:05:54,700 --> 00:06:02,080
Le Bouddha donnait un enseignement public une fois par semaine.

56
00:06:02,600 --> 00:06:11,000
Tous les disciples, moines et laïcs, se réunissaient dehors, sûrement dans des lieux privilégiés.

57
00:06:11,880 --> 00:06:14,960
Le Bouddha donnait un kusen.

58
00:06:15,320 --> 00:06:23,260
Tout le monde l’écoutait en zazen, puis repartait chacun de son côté.

59
00:06:23,760 --> 00:06:27,960
Chacun pratiquait seul pendant une semaine, un mois,

60
00:06:28,240 --> 00:06:32,320
puis revenait régulièrement aux rendez-vous écouter les paroles du Bouddha.

61
00:06:32,660 --> 00:06:37,840
C’est seulement en Chine que sont apparus les dojos.

62
00:06:38,180 --> 00:06:43,060
Qu’on a commencé à pratiquer zazen en groupe, avec un ordre et des règles.

63
00:06:43,780 --> 00:06:48,280
Le groupe est quelque chose de très puissant,

64
00:06:48,800 --> 00:06:50,960
dans le bon comme dans le mauvais.

65
00:06:51,200 --> 00:06:57,240
Sensei disait : « Jésus Christ a donné son corps au goût de la mort. »

66
00:06:57,620 --> 00:07:01,700
« À travers le lac profond du néant et du désespoir. »

67
00:07:02,180 --> 00:07:06,420
« Il est devenu par là un symbole de la vie éternelle. »

68
00:07:06,740 --> 00:07:12,860
« Sa vie est identique à un koan zen. »
