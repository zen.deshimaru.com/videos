1
00:00:00,000 --> 00:00:02,240
Ik ben een Zen-monnik.

2
00:00:04,540 --> 00:00:09,960
Ik ben ook een kalligraaf.

3
00:00:10,980 --> 00:00:18,660
Chinese kalligrafie wordt gewaardeerd door mensen die zazen beoefenen.

4
00:00:19,280 --> 00:00:24,980
Het is niet gemakkelijk om over Zen te praten.

5
00:00:25,400 --> 00:00:28,820
Om het uit te drukken, om het te manifesteren.

6
00:00:29,240 --> 00:00:34,480
Door middel van kalligrafie zien we de geest van een persoon die zazen doet.

7
00:00:35,000 --> 00:00:39,100
Die de ontwakende houding beoefent.

8
00:00:40,020 --> 00:00:46,260
Meester Deshimaru legt uit dat kalligrafie niet alleen een techniek is.

9
00:00:46,980 --> 00:00:52,700
We kalligraferen niet alleen volgens technieken, eigenschappen…

10
00:00:53,360 --> 00:00:58,400
We kalligrafie met de geest, met scheenbeen.

11
00:00:58,880 --> 00:01:09,440
Met de geest, met het hele lichaam en de geest in één.

12
00:01:10,720 --> 00:01:18,400
Kalligrafie drukt dat af op papier. Het is een aanwezigheid.

13
00:01:18,960 --> 00:01:24,320
Het wordt gevoeld in de kalligrafie van grote meesters als Deshimaru en Niwa Zenji.

14
00:01:25,760 --> 00:01:30,000
De waarde van kalligrafie ligt niet alleen in de vorm.

15
00:01:31,440 --> 00:01:36,560
Maar ook in het momentum, de aanwezigheid, de energie.

16
00:01:37,280 --> 00:01:46,240
In de activiteit die ontstaat….

17
00:01:47,040 --> 00:01:52,080
En dat we voelen en waarnemen.

18
00:01:52,500 --> 00:01:55,800
We beginnen vanuit een momentum dat voortkomt uit immobiliteit.

19
00:01:56,950 --> 00:01:58,853
Zen, kalm, rustig, stilte.

20
00:02:00,160 --> 00:02:06,093
Om niets in stand te houden.

21
00:02:06,100 --> 00:02:07,910
Om gedachten te laten passeren.

22
00:02:07,950 --> 00:02:10,750
Niet om niet gemonopoliseerd te worden.

23
00:02:10,840 --> 00:02:15,040
Om gefocust te zijn in een houding.

24
00:02:15,920 --> 00:02:21,329
Gekruiste benen.

25
00:02:21,329 --> 00:02:26,880
Het kussen maakt het mogelijk het bekken naar voren te kantelen.

26
00:02:26,880 --> 00:02:31,280
Om de hele ribbenkast vrij te maken.

27
00:02:32,240 --> 00:02:37,040
In deze houding, brengen we onze handen terug.

28
00:02:38,160 --> 00:02:43,680
Het stelt ons in staat om ons te verenigen en te centreren.

29
00:02:44,400 --> 00:02:51,040
Het is de houding van Zen-meditatie, hokkai jo in.

30
00:02:52,560 --> 00:02:57,600
Het is uit dit, en uit het momentum, uit de activiteit, dat de kalligrafie zal ontstaan.

31
00:02:58,480 --> 00:03:05,200
Niet van complicaties, van het goed willen doen, van het goed willen doen, van het willen doen van goed weer…

32
00:03:06,240 --> 00:03:11,520
Alleen dit momentum van het leven.

33
00:03:12,560 --> 00:03:22,160
Uit deze zen-impuls, deze impuls, deze impuls, zal kalligrafie tevoorschijn komen.

34
00:03:23,280 --> 00:03:33,200
Dat is waar Zen kalligrafie over gaat.

35
00:03:34,720 --> 00:03:59,600
Gezien de beschavingsfase die China had bereikt tijdens de Tang-dynastie….

36
00:04:01,120 --> 00:04:15,520
In de 7e, 8e en 9e eeuw werden de omliggende landen, Japan, Korea en Vietnam, aangetrokken door deze cultuur en door de taal, de Chinese karakters.

37
00:04:16,400 --> 00:04:27,200
Het Chinese schrift heeft zich dus verspreid naar deze landen.

38
00:04:28,080 --> 00:04:34,480
Maar iedereen heeft het opgenomen volgens zijn eigen gevoel, zijn eigen taal.

39
00:04:35,280 --> 00:04:44,560
Elk van hen heeft zijn eigen manier van kalligrafie ontwikkeld.

40
00:04:45,520 --> 00:04:56,080
Ze introduceerden ook syllabi, tekens die dicht bij een alfabet staan, gemengd met Chinese karakters.

41
00:04:58,320 --> 00:05:04,480
Japanse kalligrafie heeft kleine verschillen met Chinese kalligrafie.

42
00:05:05,680 --> 00:05:13,440
Allen gebruiken borstels, tekenen Chinese karakters, maar gebruiken ook syllabische systemen.

43
00:05:15,040 --> 00:05:23,280
De Vietnamezen, die eerder door China werden beïnvloed, waren nog creatiever.

44
00:05:24,800 --> 00:05:31,600
Ze hebben hun eigen ideografisch systeem uitgevonden om hun taal naar het model van het Chinees te transcriberen.

45
00:05:33,440 --> 00:05:38,400
Het is geschreven:

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo…

47
00:05:49,520 --> 00:05:59,320
Het is de soûtra van de Geest van de Grote Wijsheid die ons verder doet gaan.

48
00:05:59,840 --> 00:06:06,160
Dit is de universele leer van de Boeddha, specifiek voor alle boeddhistische scholen.

49
00:06:07,360 --> 00:06:16,800
Het wordt gezongen in oud-Chinees, oud-Japans, in fonetische transcriptie uit het Sanskriet.

50
00:06:18,080 --> 00:06:24,000
Dat maakt het een universele tekst, omdat niemand hem begrijpt of in zijn eigen taal leest.

51
00:06:25,360 --> 00:06:39,120
De Japanners, de Chinezen, de Indiërs…. We spreken het zelf uit met enkele kleine aanpassingen….

52
00:06:40,980 --> 00:06:47,460
Het is iets dat van iedereen en van niemand in het bijzonder is.

53
00:06:48,220 --> 00:06:53,440
Dit is de zin die ik hier schreef, in een schrijfstijl die cursive wordt genoemd.

54
00:06:53,680 --> 00:06:58,520
Het is niet het schrijven van het woordenboek.

55
00:06:58,560 --> 00:07:03,680
Het is niet regelmatig schrijven.

56
00:07:05,740 --> 00:07:09,100
Het is ontwikkeld om snel te schrijven.

57
00:07:09,480 --> 00:07:15,960
Een soort stenografie.

58
00:07:16,240 --> 00:07:28,560
We houden de hoofdlijnen van het personage in stand door het vereenvoudigen, door krommingen, door punten, waar een lijn was….

59
00:07:29,600 --> 00:07:39,440
Het karakter is tot het uiterste vereenvoudigd, het krijgt een andere vorm dan zijn oorspronkelijke vorm.

60
00:07:39,760 --> 00:07:47,680
We bewegen ons in de richting van een abstractie, met behoud van de essentie van de oorspronkelijke vorm.

61
00:07:48,800 --> 00:07:55,920
Het stelt u in staat om sneller te schrijven.

62
00:07:58,080 --> 00:08:05,240
We vinden in het plot deze impuls, deze impuls, deze impuls van zazen.

63
00:08:05,640 --> 00:08:13,280
Omdat we het hele personage met elkaar verbinden.

64
00:08:13,660 --> 00:08:18,479
In één keer.

65
00:08:18,479 --> 00:08:23,470
We kunnen zelfs de karakters met elkaar verbinden.

66
00:08:24,000 --> 00:08:31,840
We koppelen elk van de karakters, elk van de eigenschappen in het karakter.

67
00:08:33,120 --> 00:08:38,520
Als er meerdere tekens worden getekend, kunnen deze aan elkaar worden gekoppeld.

68
00:08:38,940 --> 00:08:44,960
We doen het op de adem, op de impuls.

69
00:08:45,400 --> 00:08:51,540
Op het momentum, de energie, de dynamiek, de activiteit van het hele lichaam, de hele geest in één.

70
00:08:51,540 --> 00:08:55,200
In één beweging, van begin tot eind.

71
00:08:55,200 --> 00:08:59,200
We kunnen niet terug, we kunnen het niet bijwerken.

72
00:08:59,200 --> 00:09:02,900
We kunnen het niet corrigeren.

73
00:09:03,000 --> 00:09:05,120
We doen het in geweten.

74
00:09:05,920 --> 00:09:11,600
Ook met vreugde laten we ons gaan.

75
00:09:12,060 --> 00:09:18,220
Maar we kunnen niets corrigeren.

76
00:09:18,560 --> 00:09:22,160
Als het getraceerd is, is het getraceerd.

77
00:09:22,200 --> 00:09:25,529
Het is het hier en nu.

78
00:09:25,529 --> 00:09:29,000
Het is iets dat nooit meer terug zal komen.

79
00:09:29,100 --> 00:09:33,800
Dat bestaat volledig, eeuwig, in het moment.

80
00:09:33,900 --> 00:09:36,200
Dat is Zen ook.

81
00:09:36,250 --> 00:09:39,640
Elke keer, elke zazen adem is uniek.

82
00:09:42,520 --> 00:09:44,640
Ze komt niet terug.

83
00:09:45,340 --> 00:09:52,540
Het is een punt, een koppelteken van de houding.

84
00:09:52,600 --> 00:09:58,320
In de gemoedstoestand, ademhaling en totale activiteit.

85
00:09:58,420 --> 00:10:03,160
Het is een tijd van het leven, van bewustzijn, absoluut, universeel.

86
00:10:03,720 --> 00:10:06,760
Die niet terugkomen.

87
00:10:07,120 --> 00:10:11,000
In de kalligrafie is het erg belangrijk.

88
00:10:11,000 --> 00:10:17,500
Als je kalligrafie als een robot, zonder dat je er al je gedachten in steekt, is het een techniek.

89
00:10:17,540 --> 00:10:18,920
Zazen is hetzelfde.

90
00:10:19,000 --> 00:10:25,880
Als je zazen beoefent als een techniek van welzijn, is het niet de dimensie van zen.

