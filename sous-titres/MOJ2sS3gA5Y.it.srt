1
00:00:04,240 --> 00:00:07,040
Che cos’è lo Zen? 

2
00:00:07,320 --> 00:00:10,120
Lo Zen riguarda la comprensione di te stesso. 

3
00:00:10,400 --> 00:00:11,800
La tua stessa mente. 

4
00:00:12,080 --> 00:00:14,140
Sono affari di tutti. 

5
00:00:14,420 --> 00:00:16,500
Questo è ciò che Deshimaru voleva trasmettere. 

6
00:00:16,800 --> 00:00:19,460
Questo è ciò che voglio trasmettere e realizzare. 

7
00:00:19,720 --> 00:00:22,460
Avevo visto un discepolo di Kodo Sawaki. 

8
00:00:22,840 --> 00:00:24,720
Mi ha detto: 

9
00:00:25,120 --> 00:00:27,620
"Devi guardarti!" 

10
00:00:27,960 --> 00:00:29,960
Aveva ragione! 

11
00:00:30,380 --> 00:00:33,300
Cosa significa un cammino spirituale? 

12
00:00:33,640 --> 00:00:41,880
Il significato di un percorso spirituale è "tornare al sognatore". 

13
00:00:46,020 --> 00:00:48,260
Dogen dice: 

14
00:00:48,520 --> 00:00:51,260
"Studiare il buddismo è studiare te stesso". 

15
00:00:51,540 --> 00:00:53,980
Studia il sognatore. 

16
00:00:54,380 --> 00:00:57,300
Perché sogna così? 

17
00:00:58,260 --> 00:01:00,240
Come può cambiare il suo sogno? 

18
00:01:00,540 --> 00:01:02,020
Come può controllare il suo sogno? 

19
00:01:02,840 --> 00:01:04,000
Quando il Buddha dice: 

20
00:01:04,440 --> 00:01:06,300
"Tutto è un sogno." 

21
00:01:06,620 --> 00:01:09,820
Questo non vuol dire "Nulla importa". 

22
00:01:10,100 --> 00:01:13,580
Non è il sogno che è importante, è il sognatore. 

23
00:01:13,900 --> 00:01:16,540
Smetti di sognare, questo è ciò che chiamiamo "risveglio" … 

24
00:01:17,180 --> 00:01:21,040
Il risveglio sta diventando un sognatore. 

25
00:01:21,340 --> 00:01:22,980
Controlla il tuo sogno. 

26
00:01:24,000 --> 00:01:26,120
Questo è il percorso spirituale. 

27
00:01:26,420 --> 00:01:27,980
È molto difficile. 

28
00:01:28,560 --> 00:01:31,760
Come posso iniziare? 

29
00:01:32,660 --> 00:01:45,080
La motivazione principale per praticare un percorso spirituale è: essere in grado di controllare il tuo sogno. 

30
00:01:46,700 --> 00:01:49,340
Cosa è importante nello Zen? 

31
00:01:49,920 --> 00:01:55,880
In questo modo è la tua mente che è importante. 

32
00:01:56,280 --> 00:02:01,280
Zazen non è un oggetto di culto. 

33
00:02:01,540 --> 00:02:03,980
Il secondo punto importante del suo insegnamento: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku. 

35
00:02:07,180 --> 00:02:10,780
Il che significa "no profit". 

36
00:02:11,340 --> 00:02:23,340
È la porta che ci conduce dallo spirito di Dio al mondo materiale. 

37
00:02:23,640 --> 00:02:27,000
È molto importante 

38
00:02:27,360 --> 00:02:37,420
Anche nel mondo di tutti i giorni, anche nei fenomeni, non perdi la concentrazione. 

39
00:02:37,800 --> 00:02:42,920
Non perdi la tua vera natura. 

40
00:02:43,180 --> 00:02:45,100
Questo è Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
Ma cos’è facile? 

42
00:02:55,780 --> 00:03:00,260
Le persone prendono sempre la strada facile. 

43
00:03:00,620 --> 00:03:03,780
Cadiamo nelle cose facili. 

44
00:03:10,640 --> 00:03:13,600
Il potere è facile. 

45
00:03:14,060 --> 00:03:19,220
È facile stare con persone che provano sempre lo stesso. 

46
00:03:21,160 --> 00:03:26,400
Creare una chiesa dallo zen è facile. 

47
00:03:26,800 --> 00:03:32,080
Associarsi a una chiesa è facile. 

48
00:03:32,360 --> 00:03:37,300
Le persone entrano in qualcosa di sicuro. 

49
00:03:37,600 --> 00:03:41,060
Senza alcuno sforzo. 

50
00:03:41,420 --> 00:03:44,620
Dove non sono mai realmente confrontati con se stessi. 

51
00:03:44,920 --> 00:03:47,080
Dove non sono mai soli. 

52
00:03:47,500 --> 00:03:52,040
Come vedi i tuoi ex compagni di follower? 

53
00:03:52,360 --> 00:03:58,720
Ammiro molti discepoli del Maestro Deshimaru. 

54
00:03:58,980 --> 00:04:07,980
Chi ha ricevuto la sua istruzione e continua a praticare da solo. 

55
00:04:08,240 --> 00:04:14,320
È meglio esercitarsi insieme. 

56
00:04:14,700 --> 00:04:19,140
Ma almeno fanno le loro cose. 

57
00:04:19,660 --> 00:04:23,980
Sono soli. 

