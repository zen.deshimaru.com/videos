1
00:00:00,010 --> 00:00:11,610
Les points clés sont : le bassin,

2
00:00:11,740 --> 00:00:17,895
la cinquième vertèbre lombaire,

3
00:00:18,635 --> 00:00:24,900
la première vertèbre dorsale, celle qui ressort.

4
00:00:26,940 --> 00:00:31,820
Et après on voit la morphologie, chacun est différent.

5
00:00:31,960 --> 00:00:38,620
Admettons que je pense qu’il faudrait qu’elle tende un peu plus la nuque.

6
00:00:41,210 --> 00:00:48,145
On doit toujours toucher deux points.

7
00:00:48,645 --> 00:00:52,262
Sauf quand c’est le sommet du crâne.

8
00:00:52,602 --> 00:00:55,460
Un yin et un yang.

9
00:00:55,640 --> 00:00:58,675
Je prends mes aises, je me mets à genoux.

10
00:00:59,855 --> 00:01:03,020
Je vais toucher justement cette vertèbre-la.

11
00:01:08,280 --> 00:01:09,815
Je vais toucher le menton avec deux doigts.

12
00:01:09,975 --> 00:01:11,790
Je ne lui fais pas ça.

13
00:01:14,170 --> 00:01:16,825
On donne des indications.

14
00:01:16,855 --> 00:01:19,540
Mais quand elles sont vraiment bien données,

15
00:01:19,620 --> 00:01:21,635
si on laisse la personne garder sa concentration,

16
00:01:21,865 --> 00:01:24,132
ça a un effet très profond.

17
00:01:25,602 --> 00:01:30,670
Donc je vais pousser les deux points antagonistes.

18
00:01:30,753 --> 00:01:35,976
On peut aussi faire comme ça.

19
00:01:36,156 --> 00:01:39,650
Je sais qu’elle a eu un une opération…

20
00:01:41,560 --> 00:01:43,680
On peut aussi faire comme ça.

21
00:01:43,785 --> 00:01:46,570
Senseï [Deshimaru] le faisait souvent.

22
00:01:48,030 --> 00:01:53,480
On peut toucher un point spécifique où la personne a des faiblesses.

23
00:01:53,740 --> 00:01:56,860
Par exemple, à l’estomac ou au foie.

24
00:02:00,470 --> 00:02:03,900
Il faut toujours regarder la cambrure.

25
00:02:06,820 --> 00:02:10,385
Souvent, elle est trop ou insuffisamment accentuée.

26
00:02:10,575 --> 00:02:15,140
N’oubliez pas que vous avez un kyosaku à la main.

27
00:02:18,340 --> 00:02:23,950
Il n’est pas question de commencer à bricoler.

28
00:02:25,680 --> 00:02:30,245
Il faut garder son kyosaku.

29
00:02:32,525 --> 00:02:36,740
On peut faire ça avec le kyosaku, ça ne gêne pas.

30
00:02:36,875 --> 00:02:39,895
On peut aussi faire ça.

31
00:02:40,010 --> 00:02:45,140
Si la personne est trop cambrée.

32
00:02:48,195 --> 00:02:50,880
On peut aussi faire ça.

33
00:02:52,790 --> 00:02:56,313
Il faut générer une correction que la personne va prendre.

34
00:02:56,405 --> 00:02:59,930
C’est important également de faire ça.

35
00:03:04,960 --> 00:03:08,450
C’est très délicat et je ne dérange pas la méditation.

36
00:03:11,440 --> 00:03:13,570
On peut aussi utiliser le kyosaku.

37
00:03:16,235 --> 00:03:19,825
On prend le côté large en bas.

38
00:03:20,330 --> 00:03:23,325
On met un genou à terre.

39
00:03:24,216 --> 00:03:27,264
Et on va le placer à l’endroit de la cambrure.

40
00:03:29,334 --> 00:03:31,587
Ça va donner une indication.

41
00:03:32,747 --> 00:03:37,940
Très négative, parce qu’on a l’impression de n’être pas droit du tout.

42
00:03:38,150 --> 00:03:41,930
Parce que le bâton est droit, et la colonne n’est pas droite.

43
00:03:48,600 --> 00:03:50,800
La colonne n’est jamais droite.

44
00:03:51,130 --> 00:03:52,995
Elle a une légère courbure.

45
00:03:53,175 --> 00:03:59,240
Quand on met ça, forcément, on a l’impression d’être tordu.

46
00:03:59,400 --> 00:04:06,705
Si vous le placez bien sur le dos…

47
00:04:06,955 --> 00:04:09,490
C’est une méthode un peu spartiate.

48
00:04:13,940 --> 00:04:16,660
Tu as une très belle posture, Cristina !

49
00:04:22,280 --> 00:04:25,430
Lui, il a une très belle posture.

50
00:04:28,960 --> 00:04:32,320
Il y a des petites choses dont on ne se rend pas compte.

51
00:04:32,480 --> 00:04:34,340
Par exemple, le pouce, là.

52
00:04:36,040 --> 00:04:39,820
Il doit être droit.

53
00:04:41,850 --> 00:04:44,710
C’est quelque chose dont il ne se rend pas compte.

54
00:04:44,890 --> 00:04:50,520
Mais tu vas continuer un an, deux ans, et tes pouces vont être bien.

55
00:04:50,855 --> 00:04:53,735
C’est la première chose que je vois.

56
00:04:55,180 --> 00:04:58,900
Mets bien tes pouces à l’aplomb de ton nombril.

57
00:05:10,260 --> 00:05:12,600
Un peu trop dans la pensée.

58
00:05:14,440 --> 00:05:20,300
La tête part un peu en avant.

59
00:05:28,920 --> 00:05:32,970
Je fais carrément de l’ostéopathie.

60
00:05:34,420 --> 00:05:36,260
Très bien !

61
00:05:38,315 --> 00:05:42,875
Décontracte un peu là.

62
00:05:53,292 --> 00:05:54,862
Très beau !

63
00:05:56,060 --> 00:06:00,290
Quand tu fais gassho, fais-le bien comme ça.

64
00:06:04,605 --> 00:06:06,335
La posture est belle.

65
00:06:08,920 --> 00:06:11,120
Un petit peu trop cambré.

66
00:06:11,225 --> 00:06:14,765
Décambre un peu.

67
00:06:24,537 --> 00:06:28,737
À ce moment-là, parce qu’on veut tenir en force,

68
00:06:35,023 --> 00:06:37,393
Non, non, tranquille !

69
00:06:37,591 --> 00:06:38,591
Voilà !

70
00:06:38,780 --> 00:06:39,780
Parfait !

71
00:06:45,450 --> 00:06:49,410
N’oublie pas que c’est la main gauche dans la main droite.

72
00:06:51,200 --> 00:06:52,950
Détends bien tes mains !

73
00:06:53,010 --> 00:06:55,540
Détends tes doigts.

74
00:06:56,150 --> 00:06:58,530
Tu les mets bien l’un sur l’autre.

75
00:07:03,395 --> 00:07:06,015
C’est un peu crispé, mais bon…

76
00:07:08,797 --> 00:07:09,947
Ici.

77
00:07:16,500 --> 00:07:19,185
Ce que je ferai dans ce cas là…

78
00:07:19,275 --> 00:07:21,870
Je vois beaucoup de tension, c’est normal.

79
00:07:21,910 --> 00:07:24,435
Parce qu’au début, on doit faire en force.

80
00:07:24,585 --> 00:07:27,130
Tu fais ça.

81
00:07:30,560 --> 00:07:33,810
Les mains sont pas mal, maintenant.

82
00:07:34,930 --> 00:07:38,150
Décontracte, pas besoin qu’elles soient trop tendues.

83
00:07:39,690 --> 00:07:40,470
Souple.

84
00:07:40,560 --> 00:07:42,110
C’est bien, super !

85
00:07:48,975 --> 00:07:54,395
Moi, j’accentuerais un peu sur la cinquième vertèbre.

86
00:08:03,997 --> 00:08:07,027
Tu vois, je sens de la crispation.

87
00:08:10,253 --> 00:08:12,099
Très bien !

88
00:08:12,829 --> 00:08:15,839
Relâche bien les coudes.

89
00:08:19,849 --> 00:08:27,759
Senseï, pas sur les débutants, mais souvent il faisait ça.

90
00:08:27,894 --> 00:08:31,684
Carrément, il appuyait fort.

91
00:08:33,682 --> 00:08:37,282
Pour l’instant, ce n’est pas la peine.

92
00:08:37,941 --> 00:08:39,471
Très bien.

93
00:08:40,660 --> 00:08:42,680
Tu peux commencer à respirer.

94
00:08:42,790 --> 00:08:44,875
Prends ta respiration.

95
00:08:45,065 --> 00:08:46,577
Suis ta respiration.

96
00:08:46,727 --> 00:08:48,010
Prends plaisir.

97
00:08:54,425 --> 00:08:55,625
C’est très bien.

98
00:08:55,717 --> 00:08:58,987
C’est la première sesshin que tu fais ?

99
00:09:00,103 --> 00:09:02,613
Bravo !

100
00:09:02,853 --> 00:09:04,530
J’ai une question :

101
00:09:04,608 --> 00:09:07,268
Pour la respiration,

102
00:09:07,380 --> 00:09:12,640
parfois, j’aspire en force et je ne sais pas comment arrêter ?

103
00:09:20,830 --> 00:09:22,820
Tant que tu te sens bien…

104
00:09:26,310 --> 00:09:29,775
C’est mon point de vue, il y a d’autres méthodes…

105
00:09:29,935 --> 00:09:32,860
Mais en zazen, pour la respiration.

106
00:09:33,060 --> 00:09:36,780
Il faut se sentir bien, ne pas forcer.

107
00:09:37,130 --> 00:09:40,530
Tant que tu te sens bien, tu continues.

108
00:09:45,380 --> 00:09:48,570
Tu reprends ta respiration au moment où tu le sens.

109
00:09:48,731 --> 00:09:51,461
C’est ton corps qui doit le dicter.

110
00:10:25,390 --> 00:10:27,310
La posture en général

111
00:11:05,935 --> 00:11:08,115
Très bien !

112
00:11:15,740 --> 00:11:18,100
Il faut regarder les points principaux :

113
00:11:18,190 --> 00:11:21,200
cinquième vertèbre lombaire, première dorsale,

114
00:11:21,266 --> 00:11:24,790
la tête, les bras pas trop crispés.

115
00:11:26,070 --> 00:11:31,060
On peut aider la personne par une position des mains.

116
00:11:31,240 --> 00:11:34,280
Je voudrais bien voir une posture de kin-hin.

117
00:11:41,400 --> 00:11:44,925
Il n’y a que de bonnes postures.

118
00:11:46,485 --> 00:11:50,500
Il a une très belle posture, très naturelle.

119
00:11:51,230 --> 00:11:59,200
Sauf un détail : le petit doigt doit être bien serré.

120
00:11:59,785 --> 00:12:01,855
Souvent, les gens oublient de le serrer.

121
00:12:01,967 --> 00:12:03,822
Senseï le disait toujours :

122
00:12:03,982 --> 00:12:08,977
les deux petits doigts doivent être bien serrés.

123
00:12:15,043 --> 00:12:17,623
Je t’ai même déjà corrigé l’autre fois…

124
00:12:19,766 --> 00:12:22,006
Très bien, bravo !

125
00:12:24,018 --> 00:12:27,238
Il y a souvent des gens avec des postures comme ça.

126
00:12:27,424 --> 00:12:28,974
Doit-on les corriger ?

127
00:12:29,047 --> 00:12:30,387
Il faudrait corriger.

128
00:12:30,510 --> 00:12:33,615
Par exemple Pierre, prends la posture…

129
00:12:33,920 --> 00:12:38,170
J’ai remarqué un truc, on va voir si tu le fais…

130
00:12:46,127 --> 00:12:50,227
La main est un peu comme ça…

131
00:12:55,366 --> 00:12:57,176
Relâche un peu.

132
00:12:59,735 --> 00:13:04,130
Il faut vraiment que ça soit superposé.

133
00:13:13,740 --> 00:13:15,870
Parfois, on peut faire une erreur.

134
00:13:15,935 --> 00:13:18,195
Si on la corrige…

135
00:13:18,317 --> 00:13:20,887
Serre un peu les petits doigts…

136
00:13:23,093 --> 00:13:27,443
Senseï montrait toujours de l’énergie dans les deux doigts.

137
00:13:27,821 --> 00:13:30,621
On fait comme ça, puis comme ça.

138
00:13:32,250 --> 00:13:35,010
Et après, celle-là, on la met au-dessus.

139
00:13:36,000 --> 00:13:38,040
Bien perpendiculaire.

140
00:13:38,310 --> 00:13:42,130
Quand on ouvre les mains, elles sont parallèles au sol.

141
00:13:44,370 --> 00:13:46,580
Il faut éviter de finir comme ça…

142
00:13:47,610 --> 00:13:48,830
C’est bien.

143
00:13:48,990 --> 00:13:50,410
C’est parfait.
