1
00:00:06,180 --> 00:00:09,540
Ik geef niet om Zen.

2
00:00:09,840 --> 00:00:12,100
Het is gewoon nog een stuk kerkafval.

3
00:00:12,480 --> 00:00:17,520
Er zijn duizenden Zen-meesters in de Verenigde Staten.

4
00:00:17,820 --> 00:00:20,880
Iedereen wil een Zen-meester zijn.

5
00:00:21,220 --> 00:00:24,680
En Zen is een rage,

6
00:00:25,280 --> 00:00:28,820
en ik ben niet geïnteresseerd.

7
00:00:29,320 --> 00:00:31,960
Waarom denk je dat het een rage is?

8
00:00:32,260 --> 00:00:36,240
Nee, nu is het uit de mode!

9
00:00:37,300 --> 00:00:40,060
In Argentinië koopt iedereen een kleine zentuin.

10
00:00:40,340 --> 00:00:42,720
We zetten hem in het kantoor, in zijn woonkamer.

11
00:00:43,020 --> 00:00:45,740
Het is prachtig. Het is leuk. Het is leuk.

12
00:00:46,100 --> 00:00:49,000
Maar zen is nutteloos!

13
00:00:53,900 --> 00:00:57,360
Wat vind je van Krishnamurti’s uitdrukking:

14
00:00:57,640 --> 00:01:01,060
"Laat niemand tussen jou en de waarheid komen"?

15
00:01:01,380 --> 00:01:07,140
Als er een discrepantie is tussen jou en de waarheid, dan is het niet de waarheid.

16
00:01:11,480 --> 00:01:16,780
Hij bedoelde dat hij altijd tegen het tij was.

17
00:01:17,040 --> 00:01:21,960
Hij had zich afgescheiden van de hele theologische samenleving, die hem als referentie wilde nemen.

18
00:01:22,300 --> 00:01:24,260
Bewonder je dat over hem?

19
00:01:24,540 --> 00:01:31,440
Ja. Mijn moeder, toen ze zwanger was van mij, was een discipel van Krishnamurti.

20
00:01:31,760 --> 00:01:37,500
Op een dag, op een conferentie, voelde ze een extase, een satori.

21
00:01:37,760 --> 00:01:42,620
Ze zei tegen zichzelf, "mijn zoon wordt een heilige".

22
00:01:43,020 --> 00:01:50,060
Ik vind Krishnamurti erg leuk, hij was een buitengewoon persoon.

23
00:01:51,640 --> 00:01:56,940
Sai Baba? Ik weet het niet, ik ken hem niet.

24
00:01:57,360 --> 00:02:02,020
Hij is de enige leraar die ik ooit heb horen zeggen dat hij God was.

25
00:02:02,320 --> 00:02:04,680
Maar iedereen is God!

26
00:02:05,740 --> 00:02:11,540
Een echte meester hoeft niet te zeggen "Ik ben God".

27
00:02:11,940 --> 00:02:16,200
Maar je moet anderen vertellen dat je God bent.

28
00:02:16,500 --> 00:02:22,740
Dus de man zegt, "Ik ben God. Geef me je geld! »

29
00:02:23,060 --> 00:02:27,960
En het feit dat hij trucjes doet zoals horloges laten verschijnen, wat denk je?

30
00:02:28,240 --> 00:02:32,940
Ik weet het niet. Ik weet het niet. Ik weet het niet. Ja, misschien heeft hij een kracht. Ja, het is mogelijk.

31
00:02:33,080 --> 00:02:34,400
Is het mogelijk?
Ja, het is mogelijk.

32
00:02:34,680 --> 00:02:37,040
Er zijn tovenaars die zeggen dat het illusionisme is.

33
00:02:37,320 --> 00:02:40,820
Het is ook mogelijk dat het een list was.

34
00:02:40,980 --> 00:02:48,180
Ik denk dat alle religies, alle kerken, tegen de mens zijn.

35
00:02:48,280 --> 00:02:50,280
Iedereen, zonder onderscheid?

36
00:02:51,740 --> 00:02:54,760
Het boeddhisme ook, allemaal.

37
00:02:55,020 --> 00:02:57,820
Dit zijn kerken.

38
00:02:58,400 --> 00:03:00,400
Ik vind het niet leuk.

39
00:03:02,740 --> 00:03:08,100
De ware Zen van Deshimaru is geen kerk, het is een school.

40
00:03:08,480 --> 00:03:10,060
Het is iets anders.

41
00:03:10,380 --> 00:03:12,380
Hoe kun je het verschil zien?

42
00:03:12,900 --> 00:03:16,740
De kerk, het zijn de Japanners!

43
00:03:20,060 --> 00:03:26,000
In een kerk zoeken mensen een spirituele weg.

44
00:03:26,400 --> 00:03:33,240
En na 10 jaar zijn ze op zoek naar een graad, in een hiërarchie, een macht over anderen.

45
00:03:33,560 --> 00:03:42,620
Het is een vreselijk iets dat de waarheid doodt.

46
00:03:43,160 --> 00:03:45,680
Ik oefen zazen.

47
00:03:46,040 --> 00:03:49,820
Ik beoefen de leer van de Boeddha’s, die heel eenvoudig is.

48
00:03:50,120 --> 00:03:54,520
Voor mij is het een schat van de mensheid.

49
00:03:56,500 --> 00:03:58,640
Zazen [zittende meditatie].

50
00:03:58,960 --> 00:04:02,180
Kinhin. [Langzaam lopen]

51
00:04:02,700 --> 00:04:04,700
Sampaï.

52
00:04:04,900 --> 00:04:09,020
Genmai [ochtendmaaltijd] en samu [concentratie op het werk].

53
00:04:09,360 --> 00:04:10,880
Het is heel eenvoudig.

54
00:04:11,180 --> 00:04:12,880
Maar het verandert altijd.

55
00:04:13,140 --> 00:04:16,000
Je voegt altijd nieuwe dingen toe.

56
00:04:16,320 --> 00:04:18,340
Ja, ik ben aan het leren.

57
00:04:18,480 --> 00:04:27,580
Omdat ik denk dat ik graag leer en meer wijsheid heb, om te evolueren.

58
00:04:27,880 --> 00:04:29,480
Mij ondervragen.

59
00:04:29,800 --> 00:04:33,660
Ik vind het veel leuker om een discipel te zijn dan een meester.

60
00:04:34,100 --> 00:04:35,740
Ik vind ze allebei leuk.

61
00:04:36,120 --> 00:04:40,540
Ik ben een goede meester, maar ik ben een zeer slechte leerling.

62
00:04:40,820 --> 00:04:42,780
Zeer ongedisciplineerd.

63
00:04:43,200 --> 00:04:46,160
Ben je altijd zo geweest? Inclusief met Deshimaru?

64
00:04:46,400 --> 00:04:47,140
Ja.

65
00:04:47,420 --> 00:04:48,840
En hij heeft je gecorrigeerd?

66
00:04:49,080 --> 00:04:50,700
Ja, hij heeft me veel geholpen.

67
00:04:50,980 --> 00:04:54,240
Hij gaf me de leiding, het kalmeerde me een beetje.

68
00:04:58,060 --> 00:05:00,280
"Wat ga ik doen met Stéphane?"

69
00:05:00,560 --> 00:05:04,180
"Hier, ik maak hem de baas! »

70
00:05:08,920 --> 00:05:11,860
Weet je nog de dag dat je Deshimaru ontmoette?

71
00:05:12,120 --> 00:05:12,620
Ja.

72
00:05:12,920 --> 00:05:14,920
Hoe was het?

73
00:05:15,280 --> 00:05:16,820
Ik kan het niet zeggen.

74
00:05:17,060 --> 00:05:19,240
Ik was in Zazen en hij stond achter me met de kyosaku.

75
00:05:22,920 --> 00:05:28,320
Later heb ik in de metro bloemen gekocht op weg naar de dojo.

76
00:05:28,640 --> 00:05:33,060
Bij de ingang van de dojo zeg ik: "Ik heb bloemen voor de meester". »

77
00:05:33,400 --> 00:05:36,080
En na Zazen, noemen ze me.

78
00:05:36,340 --> 00:05:38,540
"De meester vraagt naar jou."

79
00:05:38,780 --> 00:05:42,860
Ik ga naar zijn kamer, waar hij zit.

80
00:05:43,060 --> 00:05:46,760
"Wie heeft me deze bloemen gebracht? »

81
00:05:47,040 --> 00:05:47,800
Ik.

82
00:05:48,660 --> 00:05:51,580
"Goed, goed, goed."

83
00:05:54,280 --> 00:05:58,080
Weet je nog de laatste dag dat je hem zag?

84
00:05:58,580 --> 00:05:59,920
Hij was dood.

85
00:06:05,280 --> 00:06:07,280
Zijn lichaam zou worden verbrand.

86
00:06:07,520 --> 00:06:09,160
Tot ziens!

87
00:06:10,320 --> 00:06:12,320
Hoe zit het met de laatste dag dat je hem levend zag?

88
00:06:13,860 --> 00:06:19,800
Toen ging hij voor de laatste keer naar Japan en zei "vaarwel! ».

89
00:06:20,200 --> 00:06:23,680
De dag dat Deshimaru stierf, heb je gehuild?

90
00:06:24,360 --> 00:06:34,420
Toen hij stierf, was ik in de dojo, zittend in zazen…

91
00:06:37,260 --> 00:06:40,780
Iemand komt in de dojo en zegt:

92
00:06:41,060 --> 00:06:45,840
"We zullen verder moeten gaan zonder Deshimaru. »

93
00:06:46,100 --> 00:06:48,900
Ik heb het in zazen gewoond.

94
00:06:49,200 --> 00:06:50,560
Ik stond altijd voor hem.

95
00:06:50,900 --> 00:06:53,500
Ik was altijd een pilaar voor hem.

96
00:06:53,940 --> 00:06:57,120
Zo heb ik zijn dood ervaren.

97
00:06:57,420 --> 00:07:02,200
Ik moest sterk blijven voor de anderen.

98
00:07:02,840 --> 00:07:11,040
Daarna ben ik direct naar Japan gegaan voor de crematieceremonie.

99
00:07:13,100 --> 00:07:23,920
Er was een ceremonie met veel leraren, met de hele Japanse kerk. Enorm.

100
00:07:24,660 --> 00:07:31,060
En het lag in een kist, met veel bloemen.

101
00:07:31,500 --> 00:07:37,790
Zijn erg mooie gezicht, erg glimlachend.

102
00:07:38,100 --> 00:07:50,620
Toen hij nog leefde, werd hij, telkens als hij een hoed of een valse neus opdeed, heel komisch.

103
00:07:50,940 --> 00:07:54,380
Daar was het hetzelfde, met de bloemen eromheen.

104
00:07:54,720 --> 00:07:59,520
We deden de ceremonie, ik bleef erg gefocust en onbewogen.

105
00:07:59,820 --> 00:08:05,400
Dan zetten we de kist in een auto.

106
00:08:05,880 --> 00:08:09,720
Aan vier discipelen.

107
00:08:09,840 --> 00:08:20,280
Ik was de laatste en ik duw de kist op een karretje.

108
00:08:20,600 --> 00:08:27,160
Op het laatste moment knijp ik in mijn vinger met de kist.

109
00:08:29,180 --> 00:08:37,700
Ik begon te huilen en ik kon niet stoppen voor twee, drie uur.

110
00:08:38,020 --> 00:08:42,220
Het kwam naar buiten en ik huilde en huilde.

111
00:08:42,520 --> 00:08:45,560
Iedereen keek me aan omdat niemand zo huilde.

112
00:08:46,040 --> 00:08:51,440
In het bijzijn van de familie en de andere deelnemers huilde ik onophoudelijk.

113
00:08:52,280 --> 00:08:57,500
Ik kon mezelf niet tegenhouden.

114
00:09:00,560 --> 00:09:05,720
Totdat ik de rook uit de schoorsteen zag komen.

115
00:09:19,379 --> 00:09:27,799
Dood - levend, alles leeft, alles is dood.

116
00:09:28,840 --> 00:09:33,500
We leven nog, maar we zijn dood.

117
00:09:33,800 --> 00:09:42,220
Als je sterft, zie je je hele leven voorbij flitsen.

118
00:09:42,540 --> 00:09:48,580
We kijken nu al naar ons hele leven.

119
00:10:10,010 --> 00:10:13,620
Zazen is de normale staat.

