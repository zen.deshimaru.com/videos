1
00:00:00,000 --> 00:00:05,180
Mi sono esercitato con suor Deshimaru in sesshin e nei campi estivi. 

2
00:00:05,500 --> 00:00:08,360
Mi è stato detto: "Vedrai, è un maestro!" 

3
00:00:08,700 --> 00:00:13,040
Non osavo nemmeno guardarlo, ero così intimidito. 

4
00:00:13,540 --> 00:00:16,780
Quando ho scoperto che era malato, ho pensato: 

5
00:00:17,120 --> 00:00:20,340
"Devo continuare lo Zen se muore?" 

6
00:00:20,620 --> 00:00:26,180
Ho avuto l’impressione che mi avesse insegnato qualcosa che mi ha dato il coraggio di continuare. 

7
00:00:26,620 --> 00:00:30,400
Mi chiamo Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
Avrei potuto pensare prima di poter essere una suora cattolica … 

9
00:00:35,400 --> 00:00:39,320
Ma quanto è bello essere una suora zen nella vita lavorativa! 

10
00:00:39,620 --> 00:00:43,200
Ho continuato a seguire il dojo di Lione con André Meissner. 

11
00:00:43,440 --> 00:00:49,000
Seguendo Meissner, che in seguito seguì Stéphane, seguii naturalmente il Maestro Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen è uno studente vicino del Maestro Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
Sono stato ordinato Bodhisattva nel 1980 dal Maestro Deshimaru. 

14
00:00:59,680 --> 00:01:03,560
Il Bodhisattva è colui che vuole aiutare tutti gli esseri. 

15
00:01:03,780 --> 00:01:07,960
L’ordinazione è l’adempimento di questo voto. 

16
00:01:08,200 --> 00:01:10,560
Non ho mai provato a praticare lo Zen. 

17
00:01:10,860 --> 00:01:12,460
Non sapevo cosa fosse. 

18
00:01:12,740 --> 00:01:15,180
C’era un poster durante una conferenza a Lione. 

19
00:01:15,400 --> 00:01:18,180
È stata una conferenza con Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
Dice che ti mancherebbe qualcosa se non ci andassi. 

21
00:01:26,320 --> 00:01:33,860
Durante la sua lezione c’erano persone nello zazen, ma non ero nemmeno impressionato dall’atteggiamento. 

22
00:01:34,280 --> 00:01:37,780
È stata una mossa intellettuale da parte mia. 

23
00:01:38,020 --> 00:01:42,220
Quando ho iniziato ad allenarmi, era un modo per fermare il tempo. 

24
00:01:42,500 --> 00:01:44,640
Va tutto così veloce! 

25
00:01:44,940 --> 00:01:50,140
Ho anche pensato che potesse risolvere tutti i problemi sulla base. 

26
00:01:50,460 --> 00:01:55,260
Risolveva un problema perché mi sentivo espulso dalla carriera del mio insegnante di matematica. 

27
00:01:55,540 --> 00:02:00,280
Mi è piaciuta la prima frase della poesia Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"La grande strada non è difficile, basta evitare una scelta." 

29
00:02:05,860 --> 00:02:11,100
Mi sono detto: "Sì, sono molto disponibile per quello che sta succedendo!" 

30
00:02:11,420 --> 00:02:16,560
Mentre mi esercitavo, mi resi conto di quanto fossi ancora in uno stato di scelta e rifiuto. 

31
00:02:16,940 --> 00:02:23,900
Praticare lo Zen ci aiuta anche a vedere la nostra oscurità. 

32
00:02:25,100 --> 00:02:32,740
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

