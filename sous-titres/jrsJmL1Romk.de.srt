1
00:00:08,039 --> 00:00:12,389
Wo auch immer Sie sind.

2
00:00:21,107 --> 00:00:23,587
Setzen Sie sich ruhig hin.

3
00:00:23,995 --> 00:00:26,755
Seien Sie vollkommen präsent.

4
00:00:27,698 --> 00:00:32,578
Wer auch immer Sie sind.

5
00:00:39,450 --> 00:00:41,700
Raus aus der täglichen Routine.

6
00:00:41,790 --> 00:00:43,254
Üben Sie Zazen.

7
00:00:43,401 --> 00:00:45,881
mit der Kosen-Sangha.

8
00:00:51,238 --> 00:00:57,194
Ereignisse im Zusammenhang mit dieser Pandemie

9
00:00:58,324 --> 00:01:02,861
Dogen’s Zen zertifizieren

10
00:01:03,081 --> 00:01:05,538
und den Zen von Deshimaru.

11
00:01:05,862 --> 00:01:09,802
Vergänglichkeit, wir leben sie.

12
00:01:11,770 --> 00:01:16,620
Interdependenz ist etwas, das wir ebenfalls erleben.

13
00:01:21,799 --> 00:01:24,759
Der einzige Moment, der zählt,

14
00:01:26,209 --> 00:01:29,069
es ist genau jetzt.

15
00:01:29,259 --> 00:01:30,919
Hier und jetzt.

16
00:01:31,008 --> 00:01:33,698
Egal, was Sie tun.

17
00:01:36,710 --> 00:01:40,610
Richten Sie Ihre Aufmerksamkeit nach innen.

18
00:01:40,908 --> 00:01:43,698
Setzen Sie sich einfach hin.

19
00:01:44,461 --> 00:01:47,161
Geradlinig und geräuschlos.

20
00:01:48,301 --> 00:01:49,821
Der Buddha,

21
00:01:50,388 --> 00:01:53,538
unser innerer Meister.

22
00:01:53,942 --> 00:01:56,332
Dharma, der Pfad, die Praxis.

23
00:01:56,671 --> 00:01:59,091
La sangha, die virtuelle Sangha.

24
00:02:03,066 --> 00:02:07,486
Wenn Sie können.

25
00:02:12,081 --> 00:02:16,081
Kommen Sie hier und jetzt zu uns.

26
00:02:16,630 --> 00:02:20,630
Kein Ego.

27
00:02:20,769 --> 00:02:24,269
Ziellos.

28
00:02:52,434 --> 00:02:54,384
Guten Morgen, allerseits!

29
00:02:54,904 --> 00:02:57,614
Und danke, dass Sie gemeinsam Zazen machen!
