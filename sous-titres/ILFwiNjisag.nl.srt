1
00:00:07,490 --> 00:00:10,656
Mensen denken dat zazen een techniek is….

2
00:00:10,656 --> 00:00:13,173
die hen zullen helpen in hun leven.

3
00:00:13,173 --> 00:00:16,800
Ze maken er een belachelijk klein ding van.

4
00:00:16,800 --> 00:00:21,820
en zazen doen in de hoop dat het hen zal helpen in hun leven.

5
00:00:21,820 --> 00:00:31,280
Dat is waar, want op dit zeer bewuste moment…,

6
00:00:31,280 --> 00:00:35,300
de meest energieke, de meest aanwezige die er is….

7
00:00:35,300 --> 00:00:39,820
….we hebben volledige invloed op alle fenomenen in ons leven, dat is zeker.

8
00:00:40,020 --> 00:00:46,240
Maar we doen dit niet om ons ego te verbeteren….

9
00:00:46,250 --> 00:00:50,720
Om ons rustiger te maken….

10
00:00:50,720 --> 00:00:52,880
Omdat het ons goed zal doen….

11
00:00:52,880 --> 00:00:58,500
Omdat het ons een paar uur slaap zal besparen….

12
00:00:58,500 --> 00:01:02,759
Ons grotere seksuele prestaties geven….

13
00:01:02,759 --> 00:01:06,100
Of ons helpen omgaan met stress.

14
00:01:06,100 --> 00:01:11,500
Dit verlaagt de zazen….

15
00:01:11,500 --> 00:01:16,230
op het niveau van een instrument voor ons eigen illusoire leven.

16
00:01:16,230 --> 00:01:23,760
Maar het is het tegenovergestelde.

17
00:01:23,920 --> 00:01:28,980
We zullen ons leven concentreren om de meest effectieve zazen te genereren.

18
00:01:30,890 --> 00:01:33,880
Ga in de positie van de Boeddha zitten,

19
00:01:33,880 --> 00:01:39,380
het is het meest intense, meest bewuste moment.

20
00:01:39,380 --> 00:01:43,020
Daar gaat het allemaal om.

21
00:01:43,020 --> 00:01:46,340
Alles komt samen: het verleden, het heden, de toekomst.

22
00:01:46,340 --> 00:01:52,060
Alle fenomenen van ons leven zijn in zazen.

23
00:01:52,060 --> 00:02:00,040
Het is in zazen dat we macht hebben, bewustzijn,

24
00:02:06,620 --> 00:02:12,020
om de ins en outs van ons leven fundamenteel te veranderen.

25
00:02:16,320 --> 00:02:21,240
In zazen zitten we in een waardige houding.

26
00:02:21,240 --> 00:02:25,380
Een houding van helden, Boeddha, God.

27
00:02:25,500 --> 00:02:31,320
We zijn hetero, sterk, knap, rustig, kalm, kalm, immobiel.

28
00:02:31,320 --> 00:02:36,600
Je moet niet in slaap vallen, je moet de wervelkolom strekken.

29
00:02:36,600 --> 00:02:41,980
Je moet ademen, ontspannen.

30
00:02:46,640 --> 00:02:50,740
In zazen is er alles, alles wat we zijn.

31
00:02:50,740 --> 00:02:55,800
Al ons verleden, ons heden en indirect onze toekomst.

32
00:02:55,980 --> 00:03:05,460
Er is ons lichaam, met zijn zwakheden, zijn vermoeidheid,

33
00:03:05,600 --> 00:03:11,200
zijn onzuiverheden, zijn giftige stoffen,

34
00:03:11,209 --> 00:03:14,900
zijn ziekten, zijn karma, etc.

35
00:03:14,900 --> 00:03:21,380
Daar is ons verstand, ons geweten.

36
00:03:21,380 --> 00:03:25,055
In zazen is alles verenigd.

37
00:03:25,055 --> 00:03:29,320
De zazen houding,

38
00:03:29,320 --> 00:03:33,880
het is het mooiste zelfbeeld dat je kunt laten zien.

39
00:03:40,620 --> 00:03:46,090
Ik ontmoette Meester Deshimaru, hij leerde me zazen, ik ging in zazen zitten.

40
00:03:46,090 --> 00:03:48,620
En ik zei meteen: "Dit is de beste! »

41
00:04:01,360 --> 00:04:05,680
Om te weten wat je creëert, wat is de realiteit die je creëert….

42
00:04:05,680 --> 00:04:10,680
Jij bent degene die de realiteit creëert. Je leeft je droom.

43
00:04:10,680 --> 00:04:15,920
Iedereen leeft zijn droom, en je moet je droom aannemen, dat is het ding.

44
00:04:15,920 --> 00:04:19,480
Voor mij is Zazen altijd zo geweest.

