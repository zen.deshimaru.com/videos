1
00:00:00,180 --> 00:00:04,420
I started practicing Zen when I was 16. 

2
00:00:04,740 --> 00:00:07,660
I’m turning 49. 

3
00:00:07,860 --> 00:00:14,360
I had tried to take the position of zazen alone on my bed. 

4
00:00:14,960 --> 00:00:19,460
I kept it up for 2, 3, 4 minutes, it was really really painful. 

5
00:00:19,700 --> 00:00:21,700
I couldn’t do it at all. 

6
00:00:21,840 --> 00:00:30,800
There they showed me the pose and I held the normal zazen for about half an hour. 

7
00:00:31,120 --> 00:00:33,420
We are getting ready to leave, and here: 

8
00:00:33,640 --> 00:00:36,100
"Ah, don’t forget to pay!" 

9
00:00:36,540 --> 00:00:38,500
I thought it was free… 

10
00:00:38,880 --> 00:00:42,580
I left without my allowance … 

11
00:00:45,460 --> 00:00:48,680
I practice in the Zen Dojo of Lyon, in Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
It is an old dojo made by Master Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
My name is Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
My monk name is Ryurin. 

15
00:01:05,200 --> 00:01:09,700
I have been a disciple of Master Kosen for 25 years. 

16
00:01:10,000 --> 00:01:13,000
It took me a while to meet Master Kosen. 

17
00:01:13,320 --> 00:01:17,480
I knew him, I used to do sesshins with him… 

18
00:01:17,760 --> 00:01:21,000
But I was not very close. 

19
00:01:21,340 --> 00:01:26,980
I have come closer due to circumstances beyond my control. 

20
00:01:27,260 --> 00:01:31,800
And I was able to discover his teaching. 

21
00:01:32,160 --> 00:01:36,120
So I have practiced zazen with him until today. 

22
00:01:36,280 --> 00:01:39,800
Master Kosen gave me about ten years ago, 

23
00:01:40,060 --> 00:01:42,920
as it did with many of its former followers, 

24
00:01:43,260 --> 00:01:46,900
the shiho, the transmission of Dharma. 

25
00:01:47,740 --> 00:01:50,740
Zen master, it is a samu, it is not a title. 

26
00:01:51,020 --> 00:01:57,420
As we say "master", it suggests the idea that one has mastered something, but he is a samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" is a Chinese word, Japanese. 

28
00:02:01,740 --> 00:02:03,840
These are the tasks to be performed. 

29
00:02:04,100 --> 00:02:06,380
He also teaches, he is a samu. 

30
00:02:06,560 --> 00:02:08,720
The Zen master does his samu … 

31
00:02:09,000 --> 00:02:11,020
When he is done with his samurai, he goes home. 

32
00:02:11,300 --> 00:02:12,980
He will go to work or not … 

33
00:02:13,280 --> 00:02:15,560
He has a wife and family or not … 

34
00:02:15,980 --> 00:02:17,240
It depends on each person … 

35
00:02:18,060 --> 00:02:21,180
And now he’s working on something else… 

36
00:02:21,500 --> 00:02:23,120
That’s life! 

37
00:02:23,640 --> 00:02:26,660
But the Zen master, he is a disciple. 

38
00:02:27,060 --> 00:02:35,860
If you liked this video, feel free to like it and subscribe to the channel! 

