1
00:00:13,360 --> 00:00:18,820
Una poesia Shodoka, la poesia di questa mattina del maestro Yoka Daishi:

2
00:00:19,740 --> 00:00:29,120
"Se qualcuno mi chiede a quale religione appartengo, io dico il potere di Maka Hannya".

3
00:00:30,800 --> 00:00:35,200
Sto leggendo un estratto di un commento del Maestro Deshimaru "Maka Hannya, significa la massima saggezza,

4
00:00:35,640 --> 00:00:39,160
il più profondo, l’universale.

5
00:00:39,740 --> 00:00:42,000
Qual è il suo potere, quanto è potente?

6
00:00:42,340 --> 00:00:45,900
Tutti hanno potere, ma in

7
00:00:46,140 --> 00:00:51,300
L’Hannya Shingyo, nel Sutra della Grande Saggezza, il Bodhisattva Avalokiteshvara

8
00:00:51,820 --> 00:00:55,540
...ha il potere supremo,
Di Maka Hannya.

9
00:00:56,560 --> 00:01:02,600
Ha capito che il suo corpo e tutte le cose sono ku, senza alcuna sostanza pulita,

10
00:01:03,420 --> 00:01:07,980
può aiutare tutti gli esseri
e liberarli dalla loro ansia".

11
00:01:08,300 --> 00:01:14,440
L’inizio del sutra di Hannya Shingyo è legato a questa poesia. Leggerò anche

12
00:01:15,340 --> 00:01:19,260
un estratto da uno degli insegnamenti del Maestro 
Kosen. Insegna" diceva il mio Maestro

13
00:01:19,780 --> 00:01:25,460
sempre, non importa quale sia la situazione,
difficile o facile, meglio è per l’essere umano

14
00:01:26,440 --> 00:01:30,920
è di continuare, di perseverare
concentrarsi sulla pratica dello zazen,

15
00:01:31,380 --> 00:01:37,960
sviluppare la sua natura di Buddha". Il Maestro Kosen dice "è più importante di qualsiasi altra cosa.

16
00:01:39,200 --> 00:01:43,900
Sempre se c’è una crisi, una crisi.
rivoluzione, una guerra è sempre

17
00:01:44,780 --> 00:01:49,020
possibile continuare la pratica, per
per respirare, per connettersi con il cosmo.

18
00:01:49,620 --> 00:01:55,480
con forza, con saggezza
fondamentale. Si dice che il loto che è

19
00:01:56,100 --> 00:02:00,740
nati dal fuoco non saranno mai distrutti".
