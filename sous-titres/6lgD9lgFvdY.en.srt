1
00:00:00,800 --> 00:00:06,680
Kin-hin "Zen’s meditative walk

2
00:00:07,600 --> 00:00:09,140
Good Morning

3
00:00:09,400 --> 00:00:12,060
Today I would like to explain

4
00:00:12,060 --> 00:00:16,600
how the Zen meditative walk is practiced

5
00:00:16,820 --> 00:00:20,300
that we call "kin hin"

6
00:00:20,400 --> 00:00:23,340
This walk is done

7
00:00:23,340 --> 00:00:26,515
between two periods of sitting meditation.

8
00:00:26,520 --> 00:00:31,140
In general, a Zen meditation session

9
00:00:31,140 --> 00:00:36,060
starts with thirty or forty minutes of zazen.

10
00:00:36,360 --> 00:00:38,920
In the posture I explained 
(in another video):

11
00:00:38,920 --> 00:00:41,740
still, silent,

12
00:00:41,740 --> 00:00:43,500
in front of the wall.

13
00:00:43,660 --> 00:00:46,240
So, this walk

14
00:00:46,240 --> 00:00:49,860
is done for 5 or 10 minutes.

15
00:00:50,140 --> 00:00:53,420
and at the end of this walk,

16
00:00:53,480 --> 00:00:58,480
we go back to where we have our meditation pillow

17
00:00:58,700 --> 00:01:04,160
to sit in zazen, immobile in front of the wall.

18
00:01:04,620 --> 00:01:08,580
How to walk when doing "Kin hin"?

19
00:01:08,900 --> 00:01:11,480
Since we are going to walk,

20
00:01:11,480 --> 00:01:13,780
let’s move.

21
00:01:14,000 --> 00:01:16,820
How do we move?

22
00:01:17,660 --> 00:01:20,740
We put our right feet.

23
00:01:21,380 --> 00:01:23,500
It is not like that.

24
00:01:23,800 --> 00:01:25,500
It is not like that.

25
00:01:25,500 --> 00:01:28,480
Parallel and aligned.

26
00:01:28,480 --> 00:01:34,580
Separated from each other, the distance of a fist.

27
00:01:35,300 --> 00:01:40,420
We will advance the equivalent of half a foot.

28
00:01:40,880 --> 00:01:47,180
One foot measures approximately 30 or 35 centimeters.

29
00:01:47,640 --> 00:01:50,820
So, let’s make steps of 15 or 20 centimeters

30
00:01:51,000 --> 00:01:52,300
at most.

31
00:01:52,600 --> 00:01:55,555
If you look at my feet,

32
00:01:55,560 --> 00:01:59,100
you’ll see my steps go like this.

33
00:01:59,240 --> 00:02:02,120
We say half foot.

34
00:02:08,120 --> 00:02:09,120
Well...

35
00:02:09,520 --> 00:02:12,425
How will we walk?

36
00:02:12,425 --> 00:02:16,620
Let’s walk with our breath.

37
00:02:17,540 --> 00:02:23,840
Our breathing gives us the rhythm.

38
00:02:25,100 --> 00:02:28,320
Inspire.

39
00:02:28,320 --> 00:02:31,720
It is the same kind of breathing we do during zazen.

40
00:02:31,980 --> 00:02:34,760
We inspire...

41
00:02:34,760 --> 00:02:38,720
During this time we take air,

42
00:02:38,720 --> 00:02:42,040
one leg passes forward

43
00:02:44,955 --> 00:02:47,875
During the exhalation,

44
00:02:47,875 --> 00:02:51,075
slow and deep,

45
00:02:51,075 --> 00:02:53,725
pushing down

46
00:02:54,100 --> 00:02:56,960
pushing the intestines down

47
00:02:57,265 --> 00:02:59,440
During the exhalation

48
00:02:59,780 --> 00:03:02,120
through the nose,

49
00:03:02,660 --> 00:03:07,340
we will put all the weight of the body

50
00:03:07,520 --> 00:03:10,800
on the front leg.

51
00:03:11,140 --> 00:03:15,160
Front leg well stretched.

52
00:03:15,160 --> 00:03:18,700
all the sole of the foot resting on the ground,

53
00:03:18,700 --> 00:03:21,220
including the heel.

54
00:03:21,620 --> 00:03:23,980
The rear leg,

55
00:03:23,980 --> 00:03:26,660
that has no weight,

56
00:03:27,060 --> 00:03:31,880
is kept loose.

57
00:03:31,880 --> 00:03:34,300
But also,

58
00:03:34,300 --> 00:03:36,340
the whole sole of the foot,

59
00:03:36,360 --> 00:03:42,660
including the heel, remains resting on the ground.

60
00:03:42,660 --> 00:03:46,540
we do not lift the heel.

61
00:03:47,465 --> 00:03:48,595
So,

62
00:03:48,920 --> 00:03:53,260
I inspire and take the step.

63
00:03:53,460 --> 00:03:57,460
Long and deep exhalation.

64
00:03:57,660 --> 00:04:00,500
During the exhalation

65
00:04:00,720 --> 00:04:06,100
transfer the weight from the body to the front leg,

66
00:04:06,100 --> 00:04:11,420
to the root of the big finger in front,

67
00:04:11,520 --> 00:04:15,140
as if he wanted to leave a mark on the ground.

68
00:04:15,300 --> 00:04:21,380
Imagine that the soil is clay or sand.

69
00:04:22,380 --> 00:04:27,620
Then, the footprint of your foot would be recorded

70
00:04:27,880 --> 00:04:30,900
Inspiration: I take the step.

71
00:04:31,340 --> 00:04:37,740
Exhalation: transfer the weight from the body to the front leg

72
00:04:37,940 --> 00:04:40,420
I have reached the end of the exhalation.

73
00:04:40,660 --> 00:04:45,600
I have no choice but to take some air again.

74
00:04:45,640 --> 00:04:52,060
I pass the leg that is further back, forward.

75
00:04:52,300 --> 00:04:55,600
I transfer the weight of the body

76
00:04:55,600 --> 00:04:58,680
on the leg that took the step

77
00:04:58,680 --> 00:05:02,260
and again on the other leg.

78
00:05:07,340 --> 00:05:10,620
The rhythm of our breathing

79
00:05:10,640 --> 00:05:15,300
sets the pace of the walk

80
00:05:15,860 --> 00:05:17,800
Obviously,

81
00:05:17,800 --> 00:05:25,100
we maintain the same mental attitude as during sitting meditation

82
00:05:25,360 --> 00:05:27,920
I have concentrated here and now,

83
00:05:27,920 --> 00:05:30,000
in what I am doing.

84
00:05:30,000 --> 00:05:32,400
I am not following my thoughts.

85
00:05:32,400 --> 00:05:35,305
The same breath.

86
00:05:35,305 --> 00:05:39,960
Only physical activity has changed.

87
00:05:39,960 --> 00:05:45,060
"The posture of the hand"

88
00:05:46,360 --> 00:05:48,380
While we walk,

89
00:05:48,540 --> 00:05:51,660
what do we do with our hands?

90
00:05:52,355 --> 00:05:55,455
when we are in front of the wall,

91
00:05:55,765 --> 00:05:58,545
we have this posture

92
00:06:00,020 --> 00:06:02,935
Drop your right hand.

93
00:06:02,940 --> 00:06:05,800
Leave your left hand.

94
00:06:05,920 --> 00:06:08,760
I’ll wrap my thumb with my fingers.

95
00:06:08,940 --> 00:06:11,920
All fingers well together,

96
00:06:12,100 --> 00:06:17,020
letting the root of the left thumb stand out.

97
00:06:17,260 --> 00:06:19,400
Right hand.

98
00:06:19,540 --> 00:06:22,580
All fingers together

99
00:06:22,580 --> 00:06:28,800
The fingers of the right hand involve the left hand.

100
00:06:29,560 --> 00:06:33,180
I’ll squeeze my wrists slightly.

101
00:06:33,760 --> 00:06:36,720
so that the two forearms

102
00:06:36,720 --> 00:06:39,720
are on the same line.

103
00:06:39,720 --> 00:06:45,780
I don’t have one forearm higher than the other.

104
00:06:47,300 --> 00:06:51,860
The root of the left thumb

105
00:06:52,640 --> 00:06:57,260
is in contact with the solar plexus.

106
00:06:57,820 --> 00:06:59,740
The base of the sternum.

107
00:06:59,980 --> 00:07:03,860
Every human being has a small hole here,

108
00:07:03,880 --> 00:07:05,760
at the base of the sternum.

109
00:07:05,780 --> 00:07:14,160
that’s where you need to make contact with the root of your thumb.

110
00:07:15,060 --> 00:07:18,640
My forearms parallel to the ground.

111
00:07:18,700 --> 00:07:22,420
My hands too.

112
00:07:22,820 --> 00:07:25,020
Not like this.

113
00:07:25,480 --> 00:07:30,080
I keep this posture with my shoulders relaxed.

114
00:07:30,200 --> 00:07:33,360
I don’t keep this posture with my shoulders like this:

115
00:07:33,400 --> 00:07:35,360
with the elbows that go up.

116
00:07:35,500 --> 00:07:38,260
but not like this either.

117
00:07:40,820 --> 00:07:42,880
Like en zazen:

118
00:07:42,940 --> 00:07:44,600
straight column,

119
00:07:44,680 --> 00:07:46,020
nape extended,

120
00:07:46,360 --> 00:07:48,320
chin entered,

121
00:07:48,340 --> 00:07:51,920
Look at the 45º diagonal.

122
00:07:54,520 --> 00:07:57,320
Now, how will I walk?

123
00:07:57,900 --> 00:08:02,600
During the inspiration,

124
00:08:02,600 --> 00:08:04,120
we are taking a step forward.

125
00:08:06,800 --> 00:08:09,660
In exhalation

126
00:08:10,840 --> 00:08:15,740
transfers the weight of the body from one leg to the other.

127
00:08:15,820 --> 00:08:18,060
just as I explained.

128
00:08:18,060 --> 00:08:20,240
At the same time,

129
00:08:20,240 --> 00:08:25,740
you will shake hands slightly.

130
00:08:25,740 --> 00:08:30,500
This is why flexibility in the wrists is important.

131
00:08:31,980 --> 00:08:33,560
At the same time,

132
00:08:33,680 --> 00:08:38,235
a slight pressure from the root of the left thumb

133
00:08:38,235 --> 00:08:41,405
against the solar plexus.

134
00:08:42,020 --> 00:08:45,120
All this during the exhalation.

135
00:08:46,120 --> 00:08:51,540
Resumen

136
00:08:53,040 --> 00:08:56,500
Left hand, thumb, fingers together,

137
00:08:56,520 --> 00:08:58,760
right hand,

138
00:08:58,760 --> 00:08:59,580
on top.

139
00:08:59,780 --> 00:09:04,000
The root of the left thumb on the sternum.

140
00:09:04,160 --> 00:09:08,880
I inspire.

141
00:09:09,760 --> 00:09:13,680
My leg goes forward

142
00:09:14,140 --> 00:09:16,120
Exhalation phase:

143
00:09:16,300 --> 00:09:17,340
In how much

144
00:09:17,480 --> 00:09:22,120
I move all the weight of my body on this leg,

145
00:09:22,120 --> 00:09:24,480
well stretched,

146
00:09:24,800 --> 00:09:27,920
pressure exercise between the hands

147
00:09:27,920 --> 00:09:30,340
and against the solar plexus.

148
00:09:31,280 --> 00:09:32,620
Inspiration.

149
00:09:32,860 --> 00:09:35,620
One step ahead.

150
00:09:35,620 --> 00:09:37,840
Long and deep exhalation

151
00:09:38,020 --> 00:09:41,300
the weight on the front leg.

152
00:09:41,320 --> 00:09:45,940
Pressure between the hands and against the solar plexus.

153
00:09:48,440 --> 00:09:51,180
End of exhalation

154
00:09:51,320 --> 00:09:55,640
I take a step and take the opportunity to relax.

155
00:09:55,740 --> 00:09:57,500
I’m going again...

156
00:09:57,540 --> 00:10:00,660
I press the floor

157
00:10:00,820 --> 00:10:03,815
with the weight of my body.

158
00:10:03,820 --> 00:10:05,040
pressure between hands

159
00:10:05,240 --> 00:10:07,380
and against the plexus.

160
00:10:07,420 --> 00:10:09,460
The pressures are gentle.

161
00:10:09,460 --> 00:10:13,000
It’s not about getting hurt.

162
00:10:14,140 --> 00:10:15,840
All this,

163
00:10:15,840 --> 00:10:20,340
guided by the rhythm of our own breathing,

164
00:10:20,340 --> 00:10:22,460
for 5 or 10 minutes

165
00:10:22,660 --> 00:10:24,920
is back then,

166
00:10:24,920 --> 00:10:28,980
where our meditation pillow is.

167
00:10:29,280 --> 00:10:30,280
In everyday life,

168
00:10:30,560 --> 00:10:34,740
this meditative walk called "Kin hin"

169
00:10:35,560 --> 00:10:37,940
is very practical, very efficient.

170
00:10:38,960 --> 00:10:43,040
Maybe you’re in a moment of waiting

171
00:10:43,520 --> 00:10:46,075
or in time of stress.

172
00:10:46,075 --> 00:10:47,805
And you can isolate yourself

173
00:10:48,200 --> 00:10:50,860
for 2, 3 or 5 minutes.

174
00:10:50,980 --> 00:10:54,340
Where you will connect with the here and now

175
00:10:54,815 --> 00:10:57,965
In the consciousness of your breath

176
00:10:57,965 --> 00:11:03,840
and taking into account the points I explained.

177
00:11:04,620 --> 00:11:07,755
Practicing this meditation walk

178
00:11:07,760 --> 00:11:15,480
you will see how quickly you feel less stressed

179
00:11:15,480 --> 00:11:21,880
more relaxed and in harmony.

180
00:11:22,880 --> 00:11:27,820
I hope you can make a full zazen now.

181
00:11:27,820 --> 00:11:31,060
That is, a first sitting part,

182
00:11:31,060 --> 00:11:32,055
then,

183
00:11:32,055 --> 00:11:34,855
5 or 10 minutes of this walk

184
00:11:35,360 --> 00:11:38,960
and return to a second part of the meditation.

185
00:11:39,140 --> 00:11:40,300
Thanks a lot.