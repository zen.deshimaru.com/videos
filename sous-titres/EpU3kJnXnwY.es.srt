1
00:00:12,840 --> 00:00:20,310
En un poema de Yoka Daishi,
un maestro chino que escribió

2
00:00:20,310 --> 00:00:25,100
La Canción de Satori Inmediato,

3
00:00:25,100 --> 00:00:27,100
el Shôdôka

4
00:00:27,800 --> 00:00:35,760
y fue un discípulo del Maestro Eno, 
cuya transmisión recibió

5
00:00:35,760 --> 00:00:37,210
en una reunión que duró un día y una noche.

6
00:00:37,210 --> 00:00:43,100
Después continuó enseñando.

7
00:00:44,040 --> 00:00:50,040
Su poema: "Abandona los cuatro elementos, no busques más acaparar,

8
00:00:50,740 --> 00:00:57,880
en paz y con una finalización absoluta,

9
00:00:58,500 --> 00:01:01,940
Bebe y come como quieras.

10
00:01:02,360 --> 00:01:08,660
Todos los fenómenos son impermanentes, todo es KU, sin noumenon, sin ninguna sustancia propia...

11
00:01:09,080 --> 00:01:14,080
y esa es la gran
y completar el satori de Buda".

12
00:01:14,560 --> 00:01:17,820
El Maestro Deshimaru dice que aquí el poema trata de

13
00:01:18,260 --> 00:01:23,620
de los Tres Tesoros del Budismo el abandono de los cuatro elementos,

14
00:01:23,900 --> 00:01:30,300
los cuatro elementos
es agua, tierra, fuego, aire.

15
00:01:30,880 --> 00:01:35,440
El segundo tesoro es la paz y la tranquilidad y el
tranquilidad de zazen, nirvana,

16
00:01:36,060 --> 00:01:44,400
la tercera la impermanencia de los fenómenos, la conciencia, la ausencia de noumenon de existencias.

17
00:01:44,740 --> 00:01:50,120
En el primer tesoro, deja caer los cuatro elementos

18
00:01:50,120 --> 00:01:56,720
y ya no busca atraparlos, retenerlos, apoderarse de ellos.

19
00:01:56,720 --> 00:02:04,300
El Maestro Deshimaru dice que no debemos buscar el noumenon o el ego.

20
00:02:04,300 --> 00:02:09,980
Todos buscan el ego de su cuerpo.
quiere ser dueño de su cuerpo

21
00:02:10,700 --> 00:02:15,580
y de ahí nacen sentimientos como los celos, el egoísmo, la envidia.

22
00:02:15,580 --> 00:02:21,940
así que en el primer tesoro,
...aprendes a rendirte, a soltarte,

23
00:02:21,940 --> 00:02:28,150
su cuerpo, su ego, su mente.
Es interesante porque también puedes ver

24
00:02:28,150 --> 00:02:33,580
que estos cuatro elementos, abandonando la
cuatro elementos y ya no buscan

25
00:02:33,580 --> 00:02:39,560
para atraparlos, para apoderarse de ellos
en conexión con nuestro mundo.

26
00:02:39,560 --> 00:02:44,000
Agua, tierra, fuego y aire.

27
00:02:44,000 --> 00:02:48,840
En las enseñanzas
de los Maestros Zen siempre hay

28
00:02:48,850 --> 00:02:53,560
una profunda intuición, y esto es
interesante, porque esta intuición

29
00:02:53,560 --> 00:03:00,370
se une a la ciencia moderna, la
ciencias de la vida

30
00:03:00,370 --> 00:03:07,120
y más y más de estos científicos de la vida también nos están diciendo

31
00:03:07,120 --> 00:03:13,630
para abandonar los cuatro elementos,
dejar de intentar atraparlos

32
00:03:13,630 --> 00:03:18,490
oceanólogos, hidrólogos
nos explican que los océanos

33
00:03:18,490 --> 00:03:24,250
son los verdaderos pulmones de la tierra y
que tenemos que dejar de acidificarlos,

34
00:03:24,250 --> 00:03:30,280
para matarlos, llenarlos con plástico.
Especialistas en la Tierra,

35
00:03:30,280 --> 00:03:34,590
con su vida vegetal y animal, con lo que hay en sus entrañas...

36
00:03:34,590 --> 00:03:40,900
sus recursos, sus combustibles fósiles,
sus tierras raras, debemos dejarlas,

37
00:03:40,900 --> 00:03:48,060
tenemos que dejarlos ir, los agrónomos, geólogos, bioquímicos nos dicen...

38
00:03:48,060 --> 00:03:53,290
los vulcanólogos. Necesitamos encontrar un
otro funcionamiento.

39
00:03:53,290 --> 00:03:56,740
No son los Maestros Zen los que están aquí, es
realmente los científicos

40
00:03:56,740 --> 00:04:03,760
Ellos también tienen ahora una profunda comprensión del principio de interdependencia.

41
00:04:03,760 --> 00:04:10,810
Fuego que los humanos usan sin precaución, sin saber cómo controlarlo,

42
00:04:10,810 --> 00:04:15,959
la fusión nuclear atómica,
el fuego del sol,

43
00:04:15,959 --> 00:04:21,669
tenemos que dejarlo ir, no lo sabemos, es...
demasiado complicado.

44
00:04:21,669 --> 00:04:27,340
Tenemos que encontrar otra forma más simple de trabajar y finalmente el aire, que hacemos irrespirable...

45
00:04:27,340 --> 00:04:33,580
con una atmósfera saturada de CO2
metano, gases tóxicos,

46
00:04:33,960 --> 00:04:41,160
climatólogos, meteorólogos
glaciólogos y muchos otros,

47
00:04:41,160 --> 00:04:47,760
nos están diciendo, detente, déjalo ir,
manténgase fuera del camino

48
00:04:47,770 --> 00:04:52,330
así que también nos enseñan a todos,
para dejar de intentar agarrar, para dejar de agarrar...

49
00:04:52,330 --> 00:04:59,100
los cuatro elementos. En shôdôka
"abandona" el carácter chino es "放 fang"

50
00:04:59,100 --> 00:05:06,070
"放 fang" también significa libre, significa
...di que te rindas, déjalo ir, cobarde.

51
00:05:06,070 --> 00:05:12,600
Es como cuando tú
...llevar a las ovejas a pastar, las dejamos en libertad,

52
00:05:12,600 --> 00:05:20,139
pastan la hierba, en silencio.
Dejar los recursos fósiles en la tierra,

53
00:05:20,139 --> 00:05:27,190
dejar que la tierra descanse. El Maestro Deshimaru en su comentario dijo

54
00:05:27,190 --> 00:05:32,300
que no deberíamos buscar el ego en el
cuerpo, todos quieren tener y poseer ese cuerpo.

55
00:05:32,300 --> 00:05:36,450
pero es lo mismo entre nosotros y
el planeta.

56
00:05:36,450 --> 00:05:43,479
Lo consideramos nuestro
pero no sabemos nada de eso.

57
00:05:43,479 --> 00:05:51,970
la sustancia divina,
espiritual, es una tontería,

58
00:05:51,970 --> 00:05:59,420
nos comportamos con el planeta
como con nuestros cuerpos.

59
00:05:59,760 --> 00:06:07,680
Por zazen podemos entenderlo muy exactamente y si dejamos ir los cuatro elementos

60
00:06:08,080 --> 00:06:14,340
es la Tierra de Buda, la paz, el satori, lo que se realiza.

61
00:06:14,780 --> 00:06:22,280
en una operación simple y armoniosa que no daña ni mata la vida.

62
00:06:22,460 --> 00:06:27,920
la semilla de Buda, la posibilidad
para elevarse por encima de ella.

63
00:06:28,260 --> 00:06:35,020
Lo que me interesa aquí es que ahora los propios científicos

64
00:06:35,700 --> 00:06:44,060
están empezando a descubrir lo que estamos haciendo,
que percibimos y practicamos de forma natural.

65
00:06:47,560 --> 00:06:55,560
Los científicos también explican
que estamos hechos de estos cuatro elementos nosotros mismos.

66
00:06:55,900 --> 00:07:02,920
pero también que somos un cuerpo vivo en el que

67
00:07:03,400 --> 00:07:09,379
hay más bacterias, más virus, 
microbiana

68
00:07:09,379 --> 00:07:18,439
tenemos más, creo, virus que incluso
de células vivas,

69
00:07:18,439 --> 00:07:23,649
funciona porque hay un equilibrio, una protección.

70
00:07:23,649 --> 00:07:29,019
Por supuesto, cuando no hay equilibrio, no hay protección...

71
00:07:29,019 --> 00:07:35,959
Afortunadamente pudimos conocernos,
para conocer el zazen, para practicarlo,

72
00:07:35,959 --> 00:07:42,979
para darlo a conocer. Es muy valioso, no sólo para nosotros sino para todas la existencias
.