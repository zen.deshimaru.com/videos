1
00:00:06,180 --> 00:00:11,550
I have been practicing zazen for more than half of my life.

2
00:00:14,890 --> 00:00:20,170
And it is very rare that I am in perfect balance.

3
00:00:25,650 --> 00:00:29,445
There is therefore very little chance

4
00:00:29,605 --> 00:00:33,910
that I totally realize the Buddha?

5
00:00:38,110 --> 00:00:40,910
The Buddha also had back pain!

6
00:00:47,340 --> 00:00:54,790
You try to harmonize with something perfect and alive.

7
00:00:55,110 --> 00:00:59,740
What is perfect in us is that we are alive.

8
00:01:01,860 --> 00:01:06,660
There are people who are thinking of doing
of genetic modification.

9
00:01:13,480 --> 00:01:22,120
It is said in the fables that the Buddha had
the thirty-two marks of perfection.

10
00:01:23,400 --> 00:01:27,440
That is to say, it had no defect.

11
00:01:41,780 --> 00:01:48,180
It's already good to be able to get close,
to imitate his god.

12
00:01:51,530 --> 00:01:55,780
But I think the Buddha was a man like us.
and that he had flaws.

13
00:02:02,690 --> 00:02:05,685
The first defect is that he is dead.

14
00:02:05,685 --> 00:02:08,710
If he hadn't had a defect, he wouldn't have died.

15
00:02:11,630 --> 00:02:15,510
He died, he made all his disciples cry.

16
00:02:18,733 --> 00:02:21,406
We do zazen with an imperfect body.

17
00:02:21,686 --> 00:02:24,100
Like an imperfect being.

18
00:02:26,130 --> 00:02:30,650
But what's already great is to give us the direction

19
00:02:32,670 --> 00:02:39,020
cellular, muscular, mental.

20
00:02:39,670 --> 00:02:43,320
The direction to approach or imitate.

21
00:02:48,450 --> 00:02:51,840
Muslims, for example, do not wear Buddha.

22
00:02:51,910 --> 00:02:55,900
They say, “We don't have a statue,
no image of God”.

23
00:02:57,440 --> 00:02:59,165
I think it's not bad.

24
00:03:02,695 --> 00:03:07,520
But we know the direction to imitate God.

25
00:03:11,630 --> 00:03:18,510
To do all we can to make our being divine.

26
00:03:22,120 --> 00:03:24,730
But we are not perfect
and we are going to die.

27
00:03:27,000 --> 00:03:29,375
And there are some that are not perfect.

28
00:03:29,375 --> 00:03:31,932
and others that are NOT PERFECT.

29
00:03:34,552 --> 00:03:39,830
Often, perfect people don't have to…

30
00:03:40,390 --> 00:03:43,060
Me, when I was making music…

31
00:03:46,120 --> 00:03:49,685
I often tell this story.

32
00:03:50,445 --> 00:03:53,590
… I had a producer.

33
00:03:54,070 --> 00:03:58,640
He was an Italian multimillionaire.

34
00:04:00,860 --> 00:04:04,415
It didn't matter what he was doing.

35
00:04:05,265 --> 00:04:10,450
He was very rich and he wanted to set up a publishing house.

36
00:04:13,480 --> 00:04:17,123
He offered to produce my record.

37
00:04:19,480 --> 00:04:23,820
And I don't know why, he passed away.
at the temple of La Gendronnière.

38
00:04:24,780 --> 00:04:28,520
During the preparation days of a summer camp.

39
00:04:28,750 --> 00:04:33,050
He said, “I wanted to see what you do!”

40
00:04:33,520 --> 00:04:36,215
He was shown the rules of the dojo:

41
00:04:36,425 --> 00:04:39,417
“You come in like this and then you sit down.”

42
00:04:39,877 --> 00:04:42,420
It sits: complete lotus direct.

43
00:04:46,690 --> 00:04:48,450
Impeccable lotus.

44
00:04:48,570 --> 00:04:51,190
“Well, I see you've got it all figured out!”

45
00:04:51,190 --> 00:04:54,080
“Zazen is going to be in an hour.”

46
00:04:55,870 --> 00:04:58,580
“You can visit the temple.”

47
00:04:58,760 --> 00:05:02,870
He did his zazen for an hour.

48
00:05:04,300 --> 00:05:06,060
- Wasn't it hard?”

49
00:05:06,190 --> 00:05:08,720
- No, no, it was very nice!”

50
00:05:11,590 --> 00:05:14,655
I swear it's true!

51
00:05:15,314 --> 00:05:18,080
And he never did zazen again in his life.

52
00:05:19,420 --> 00:05:21,920
It was he who taught me a lesson.

53
00:05:27,150 --> 00:05:32,332
Two or three years ago, my grandson, about this tall

54
00:05:32,632 --> 00:05:37,460
said to me, “Grandpa, show me zazen!”

55
00:05:40,840 --> 00:05:45,540
I brought him into the room
where I do zazen online through Zoom.

56
00:05:47,140 --> 00:05:50,780
I said, “Sit here, put your legs like this.”

57
00:05:51,000 --> 00:05:52,019
It stresses.

58
00:05:52,115 --> 00:05:55,950
He looks like Stephen Zeisler [Deshimaru's disciple].

59
00:05:58,050 --> 00:06:00,410
- You put yourself like this.”

60
00:06:00,570 --> 00:06:04,130
I correct his posture.

61
00:06:04,400 --> 00:06:06,485
He has a great posture.

62
00:06:06,575 --> 00:06:08,750
I say to him: “That's it, that's it!”

63
00:06:08,910 --> 00:06:10,250
- Thanks Grandpa!”

64
00:06:15,755 --> 00:06:17,270
He has it.
65
00:06:17,350 --> 00:06:19,645
He knows what zazen is.

66
00:06:22,430 --> 00:06:25,660
I don't know which question I was answering…

67
00:06:27,345 --> 00:06:30,995
Does the total realization of Buddha exist?

68
00:06:33,520 --> 00:06:37,090
All is total realization of the Buddha!

69
00:06:38,090 --> 00:06:42,240
Even if we are twisted, even if we don't do zazen.

70
00:06:43,130 --> 00:06:47,830
That's why sometimes it's good to say that zazen is useless.

71
00:06:49,410 --> 00:06:55,340
Because when you do zazen in shorts at home

72
00:06:55,760 --> 00:07:01,180
without kesa, without ceremony, without friends,
without master, without disciples,

73
00:07:01,980 --> 00:07:07,360
You say to yourself, “It's the same zazens.
that I used to do when I was a beginner!”

74
00:07:07,970 --> 00:07:12,030
At the very beginning, when I was at home and I was thinking:

75
00:07:12,030 --> 00:07:14,410
“Ah, I want to do zazen!”

76
00:07:16,521 --> 00:07:17,751
I was thinking…
