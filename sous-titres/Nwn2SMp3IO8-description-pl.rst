Zen i sofrologia: podstawowe postawy i ich wpływ na mózg

Mistrz Kosen został zaproszony na kongres Federacji Zawodowych Szkół Sofirologicznych w 2013 roku (www.sophro.fr).

Wyjaśnia on cztery podstawowe postawy Zen i ich wpływ na mózg. Wraca do źródeł dynamicznego relaksu. Podkreśla znaczenie trzech filarów postawy, oddechu i stanu umysłu w buddyjskiej medytacji, a zwłaszcza praktyki Zen, zazen.
