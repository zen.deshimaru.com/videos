1
00:00:00,800 --> 00:00:06,680
Kin-hin "A caminhada meditativa do Zen"

2
00:00:07,600 --> 00:00:09,140
Bom dia

3
00:00:09,400 --> 00:00:12,060
Hoje eu gostaria de explicar

4
00:00:12,060 --> 00:00:16,600
como é praticada a caminhada meditativa Zen

5
00:00:16,820 --> 00:00:20,300
que chamamos de "kin hin"

6
00:00:20,400 --> 00:00:23,340
Essa caminhada é feita

7
00:00:23,340 --> 00:00:26,515
entre dois períodos de meditação sentada.

8
00:00:26,520 --> 00:00:31,140
Em geral, uma sessão de meditação Zen

9
00:00:31,140 --> 00:00:36,060
começa com trinta ou quarenta minutos de zazen.

10
00:00:36,360 --> 00:00:38,920
Na postura que expliquei 
(em outro vídeo):

11
00:00:38,920 --> 00:00:41,740
imóvel, silencioso,

12
00:00:41,740 --> 00:00:43,500
em frente à parede.

13
00:00:43,660 --> 00:00:46,240
Então, esta caminhada

14
00:00:46,240 --> 00:00:49,860
é feita por 5 ou 10 minutos.

15
00:00:50,140 --> 00:00:53,420
e no final desta caminhada,

16
00:00:53,480 --> 00:00:58,480
voltamos para onde temos a nossa almofada de meditação

17
00:00:58,700 --> 00:01:04,160
para nos sentarmos em zazen, imóvel em frente à parede.

18
00:01:04,620 --> 00:01:08,580
Como andar quando se faz "Kin hin"?

19
00:01:08,900 --> 00:01:11,480
Já que vamos andar,

20
00:01:11,480 --> 00:01:13,780
vamos mover-nos.

21
00:01:14,000 --> 00:01:16,820
Como nos movemos?

22
00:01:17,660 --> 00:01:20,740
Colocamos os pés direito.

23
00:01:21,380 --> 00:01:23,500
Não é assim.

24
00:01:23,800 --> 00:01:25,500
Não é assim.

25
00:01:25,500 --> 00:01:28,480
Paralelo e alinhado.

26
00:01:28,480 --> 00:01:34,580
Separados um do outro, a distância de um punho.

27
00:01:35,300 --> 00:01:40,420
Vamos avançar o equivalente a meio pé.

28
00:01:40,880 --> 00:01:47,180
Um pé mede aproximadamente 30 ou 35 centímetros.

29
00:01:47,640 --> 00:01:50,820
Então, vamos fazer passos de 15 ou 20 centímetros

30
00:01:51,000 --> 00:01:52,300
no máximo.

31
00:01:52,600 --> 00:01:55,555
Se olhares para os meus pés,

32
00:01:55,560 --> 00:01:59,100
vais ver que os meus passos vão se assim.

33
00:01:59,240 --> 00:02:02,120
Dizemos meio pé.

34
00:02:08,120 --> 00:02:09,120
Bom...

35
00:02:09,520 --> 00:02:12,425
Como é que vamos andar?

36
00:02:12,425 --> 00:02:16,620
Vamos andar com o nosso hálito.

37
00:02:17,540 --> 00:02:23,840
A nossa respiração dá-nos o ritmo.

38
00:02:25,100 --> 00:02:28,320
Inspirar.

39
00:02:28,320 --> 00:02:31,720
É o mesmo tipo de respiração que fazemos durante o zazen.

40
00:02:31,980 --> 00:02:34,760
Inspiramos...

41
00:02:34,760 --> 00:02:38,720
Durante este tempo em que tomamos ar,

42
00:02:38,720 --> 00:02:42,040
uma perna passa para a frente

43
00:02:44,955 --> 00:02:47,875
Durante a exalação,

44
00:02:47,875 --> 00:02:51,075
lenta e profunda,

45
00:02:51,075 --> 00:02:53,725
empurrando para baixo

46
00:02:54,100 --> 00:02:56,960
empurrando os intestinos para baixo

47
00:02:57,265 --> 00:02:59,440
Durante a exalação

48
00:02:59,780 --> 00:03:02,120
pelo nariz,

49
00:03:02,660 --> 00:03:07,340
colocaremos todo o peso do corpo

50
00:03:07,520 --> 00:03:10,800
na perna dianteira.

51
00:03:11,140 --> 00:03:15,160
Perna dianteira bem esticada.

52
00:03:15,160 --> 00:03:18,700
toda a sola do pé descansando no chão,

53
00:03:18,700 --> 00:03:21,220
incluindo o calcanhar.

54
00:03:21,620 --> 00:03:23,980
A perna traseira,

55
00:03:23,980 --> 00:03:26,660
que não tem peso,

56
00:03:27,060 --> 00:03:31,880
é mantida solta.

57
00:03:31,880 --> 00:03:34,300
Mas também,

58
00:03:34,300 --> 00:03:36,340
toda a sola do pé,

59
00:03:36,360 --> 00:03:42,660
incluindo o calcanhar, permanece descansando no chão.

60
00:03:42,660 --> 00:03:46,540
não levantamos o calcanhar.

61
00:03:47,465 --> 00:03:48,595
Então,

62
00:03:48,920 --> 00:03:53,260
eu inspiro e dou o passo.

63
00:03:53,460 --> 00:03:57,460
Exalação longa e profunda.

64
00:03:57,660 --> 00:04:00,500
Durante a exalação

65
00:04:00,720 --> 00:04:06,100
transfiram o peso do corpo para a perna dianteira,

66
00:04:06,100 --> 00:04:11,420
até a raiz do dedo grande que está à frente,

67
00:04:11,520 --> 00:04:15,140
como se quisesse deixar uma marca no chão.

68
00:04:15,300 --> 00:04:21,380
Imagine que o solo é argila ou areia.

69
00:04:22,380 --> 00:04:27,620
Então, a pegada do seu pé seria gravado

70
00:04:27,880 --> 00:04:30,900
Inspiração: Dou o passo.

71
00:04:31,340 --> 00:04:37,740
Exalação: transferir o peso do corpo para a perna dianteira

72
00:04:37,940 --> 00:04:40,420
Cheguei ao fim da exalação.

73
00:04:40,660 --> 00:04:45,600
Não tenho escolha a não ser tomar um pouco de ar de novo.

74
00:04:45,640 --> 00:04:52,060
Eu passo a perna que está mais para trás, para a frente.

75
00:04:52,300 --> 00:04:55,600
Eu transfiram o peso do corpo

76
00:04:55,600 --> 00:04:58,680
na perna que deu o passo

77
00:04:58,680 --> 00:05:02,260
e novamente na outra perna.

78
00:05:07,340 --> 00:05:10,620
O ritmo da nossa respiração

79
00:05:10,640 --> 00:05:15,300
define o ritmo da caminhada

80
00:05:15,860 --> 00:05:17,800
Obviamente,

81
00:05:17,800 --> 00:05:25,100
mantemos a mesma atitude mental que durante a meditação sentada

82
00:05:25,360 --> 00:05:27,920
Concentrei-me aqui e agora,

83
00:05:27,920 --> 00:05:30,000
no que estou a fazer.

84
00:05:30,000 --> 00:05:32,400
Não estou a seguir os meus pensamentos.

85
00:05:32,400 --> 00:05:35,305
A mesma respiração.

86
00:05:35,305 --> 00:05:39,960
Só a atividade física mudou.

87
00:05:39,960 --> 00:05:45,060
"A postura da mão"

88
00:05:46,360 --> 00:05:48,380
Enquanto caminhamos,

89
00:05:48,540 --> 00:05:51,660
o que fazemos com as mãos?

90
00:05:52,355 --> 00:05:55,455
quando estamos na frente da parede,

91
00:05:55,765 --> 00:05:58,545
temos esta postura

92
00:06:00,020 --> 00:06:02,935
Larga a mão direita.

93
00:06:02,940 --> 00:06:05,800
Deixa a mão esquerda.

94
00:06:05,920 --> 00:06:08,760
Vou embrulhar o polegar com os dedos.

95
00:06:08,940 --> 00:06:11,920
Todos os dedos bem juntos,

96
00:06:12,100 --> 00:06:17,020
deixando a raiz do polegar esquerdo sobressaír.

97
00:06:17,260 --> 00:06:19,400
Mão direita.

98
00:06:19,540 --> 00:06:22,580
Todos os dedos bem juntos

99
00:06:22,580 --> 00:06:28,800
Os dedos da mão direita envolvem a mão esquerda.

100
00:06:29,560 --> 00:06:33,180
Vou apertar levemente os pulsos.

101
00:06:33,760 --> 00:06:36,720
para que os dois antebraços

102
00:06:36,720 --> 00:06:39,720
estejam na mesma linha.

103
00:06:39,720 --> 00:06:45,780
Não tenho um antebraço mais alto que o outro.

104
00:06:47,300 --> 00:06:51,860
A raiz do polegar esquerdo

105
00:06:52,640 --> 00:06:57,260
está em contacto com o plexo solar.

106
00:06:57,820 --> 00:06:59,740
A base do esterno.

107
00:06:59,980 --> 00:07:03,860
Cada ser humano tem um pequeno buraco aqui,

108
00:07:03,880 --> 00:07:05,760
na base do esterno.

109
00:07:05,780 --> 00:07:14,160
é aí que precisa de fazer contacto com a raiz do polegar.

110
00:07:15,060 --> 00:07:18,640
Os meus antebraços paralelos ao chão.

111
00:07:18,700 --> 00:07:22,420
As minhas mãos também.

112
00:07:22,820 --> 00:07:25,020
Não assim.

113
00:07:25,480 --> 00:07:30,080
Mantenho esta postura com os ombros relaxados.

114
00:07:30,200 --> 00:07:33,360
Não mantenho esta postura com os ombros assim:

115
00:07:33,400 --> 00:07:35,360
com os cotovelos que sobem.

116
00:07:35,500 --> 00:07:38,260
mas não assim também.

117
00:07:40,820 --> 00:07:42,880
Como en zazen:

118
00:07:42,940 --> 00:07:44,600
coluna reta,

119
00:07:44,680 --> 00:07:46,020
nape estendida,

120
00:07:46,360 --> 00:07:48,320
queixo entrou,

121
00:07:48,340 --> 00:07:51,920
Olhe para o 45º na diagonal.

122
00:07:54,520 --> 00:07:57,320
Agora, como vou andar?

123
00:07:57,900 --> 00:08:02,600
Durante a inspiração,

124
00:08:02,600 --> 00:08:04,120
estamos a dar um passo em frente.

125
00:08:06,800 --> 00:08:09,660
Na exalação

126
00:08:10,840 --> 00:08:15,740
trasferinos o peso do corpo de uma perna para a outra.

127
00:08:15,820 --> 00:08:18,060
assim como expliquei.

128
00:08:18,060 --> 00:08:20,240
Ao mesmo tempo,

129
00:08:20,240 --> 00:08:25,740
vais apertar as mãos, ligeiramente.

130
00:08:25,740 --> 00:08:30,500
é por isso que a flexibilidade nos pulsos é importante.

131
00:08:31,980 --> 00:08:33,560
Ao mesmo tempo,

132
00:08:33,680 --> 00:08:38,235
uma ligeira pressão da raiz do polegar esquerdo

133
00:08:38,235 --> 00:08:41,405
contra o plexo solar.

134
00:08:42,020 --> 00:08:45,120
Tudo isso durante a exalação.

135
00:08:46,120 --> 00:08:51,540
Resumen

136
00:08:53,040 --> 00:08:56,500
Mão esquerda, polegar, dedos juntos,

137
00:08:56,520 --> 00:08:58,760
mão direita,

138
00:08:58,760 --> 00:08:59,580
por cima.

139
00:08:59,780 --> 00:09:04,000
A raiz do polegar esquerdo no esterno.

140
00:09:04,160 --> 00:09:08,880
Inspiro.

141
00:09:09,760 --> 00:09:13,680
Minha perna vai para a frente

142
00:09:14,140 --> 00:09:16,120
Fase de exalação:

143
00:09:16,300 --> 00:09:17,340
En quanto

144
00:09:17,480 --> 00:09:22,120
movo todo o peso do meu corpo nesta perna,

145
00:09:22,120 --> 00:09:24,480
bem esticada,

146
00:09:24,800 --> 00:09:27,920
exercito pressão entre as mãos

147
00:09:27,920 --> 00:09:30,340
e contra o plexo solar.

148
00:09:31,280 --> 00:09:32,620
Inspiração.

149
00:09:32,860 --> 00:09:35,620
Um passo à frente.

150
00:09:35,620 --> 00:09:37,840
Exalação longa e profunda

151
00:09:38,020 --> 00:09:41,300
o peso na perna dianteira.

152
00:09:41,320 --> 00:09:45,940
Pressão entre as mãos e contra o plexo solar.

153
00:09:48,440 --> 00:09:51,180
Fim da exalação

154
00:09:51,320 --> 00:09:55,640
Dou um passo e aproveito a oportunidade para relaxar.

155
00:09:55,740 --> 00:09:57,500
Vou de novo...

156
00:09:57,540 --> 00:10:00,660
Pressiono o chão

157
00:10:00,820 --> 00:10:03,815
com o peso do meu corpo.

158
00:10:03,820 --> 00:10:05,040
pressão entre as mãos

159
00:10:05,240 --> 00:10:07,380
e contra o plexo.

160
00:10:07,420 --> 00:10:09,460
As pressões são suaves.

161
00:10:09,460 --> 00:10:13,000
Não se trata de se magoar.

162
00:10:14,140 --> 00:10:15,840
Tudo isto,

163
00:10:15,840 --> 00:10:20,340
guiado pelo ritmo da nossa própria respiração,

164
00:10:20,340 --> 00:10:22,460
por 5 ou 10 minutos

165
00:10:22,660 --> 00:10:24,920
é voltamos então,

166
00:10:24,920 --> 00:10:28,980
para onde está o nosso travesseiro de meditação.

167
00:10:29,280 --> 00:10:30,280
No dia-a-dia,

168
00:10:30,560 --> 00:10:34,740
esta caminhada meditativa chamada "Kin hin"

169
00:10:35,560 --> 00:10:37,940
é muito prático, muito eficiente.

170
00:10:38,960 --> 00:10:43,040
Talvez estejas num momento de espera

171
00:10:43,520 --> 00:10:46,075
ou em tempo de stress.

172
00:10:46,075 --> 00:10:47,805
E pode isolar-se

173
00:10:48,200 --> 00:10:50,860
por 2, 3 ou 5 minutos.

174
00:10:50,980 --> 00:10:54,340
Onde vai se conectar com o aqui e agora

175
00:10:54,815 --> 00:10:57,965
Na consciência da sua respiração

176
00:10:57,965 --> 00:11:03,840
e tendo em conta os pontos que expliquei.

177
00:11:04,620 --> 00:11:07,755
Praticando esta caminhada meditação

178
00:11:07,760 --> 00:11:15,480
vai ver a rapidez com que se sente menos stressado

179
00:11:15,480 --> 00:11:21,880
mais relaxado e em harmonia.

180
00:11:22,880 --> 00:11:27,820
Espero que agora possa fazer um zazen completo.

181
00:11:27,820 --> 00:11:31,060
Isto é, uma primeira parte sentada,

182
00:11:31,060 --> 00:11:32,055
em seguida,

183
00:11:32,055 --> 00:11:34,855
5 ou 10 minutos desta caminhada

184
00:11:35,360 --> 00:11:38,960
e voltar para uma segunda parte da meditação.

185
00:11:39,140 --> 00:11:40,300
Muito obrigado.