1
00:00:08,480 --> 00:00:13,559
Hallo allerseits, ich werde erklären, wie du 

2
00:00:13,559 --> 00:00:20,760
Setzen Sie sich, um Zen-Meditation zu üben, die Zazen genannt wird 

3
00:00:20,760 --> 00:00:27,090
Es ist sehr einfach, ein Mensch und ein Meditationsküken zu sein, wenn Sie eine haben 

4
00:00:27,090 --> 00:00:30,869
Kissen eher Hamburg und das ist in Ordnung 

5
00:00:30,869 --> 00:00:35,730
und wenn nicht, machst du eins mit einer Decke, wo ein Schlafsack vorbei ist 

6
00:00:35,730 --> 00:00:41,060
Zum Beispiel muss es fest genug sein 

7
00:00:57,490 --> 00:01:06,070
Also, wie Sie erklären, ist die Zen-Meditationshaltung Zen-Meditation 

8
00:01:06,070 --> 00:01:13,620
vor allem zu wissen, dass es sich um eine unbewegliche und stille Sitzhaltung handelt 

9
00:01:13,620 --> 00:01:22,630
und die Besonderheit dieser Haltung und dass wir sie hier gegenüber der Wand üben 

10
00:01:22,630 --> 00:01:26,620
Offensichtlich stehe ich vor der Kamera, um Sie zu informieren 

11
00:01:26,620 --> 00:01:32,560
Was Sie also tun müssen, ist zusammenzusitzen und das zu polstern, das ich habe 

12
00:01:32,560 --> 00:01:37,479
Erklären Sie, dass Sie ausreichend hart gepolstert werden müssen 

13
00:01:37,479 --> 00:01:42,520
Sitzen Sie gut am Boden des Kükens. Setzen Sie sich nicht auf die Kante, wenn Sie haben 

14
00:01:42,520 --> 00:01:49,330
rutschte aus und du solltest so weit wie möglich deine Beine kreuzen 

15
00:01:49,330 --> 00:01:55,600
Kreuzen Sie sie wie diese wie diese wie allein und es heißt das 2010 wenn 

16
00:01:55,600 --> 00:02:04,270
Wenn Sie Ihre anderen Leute hierher bringen, heißt das Lotus. Wichtig ist, dass 

17
00:02:04,270 --> 00:02:13,480
Ihre Knie gehen zu Boden, um Druck auf die Knie auszuüben 

18
00:02:13,480 --> 00:02:19,780
Also, wie geht das, um es ohne Organisation oder ohne Anstrengung zu tun? 

19
00:02:19,780 --> 00:02:27,810
körperlich gute Muskeln bei jeder Zen-Meditationshaltung als loslassen 

20
00:02:27,810 --> 00:02:37,950
Damit Ihre Knie nach unten gehen, neigen Sie das Becken leicht 

21
00:02:37,950 --> 00:02:46,180
vorwärts geht die Oberseite des Beckens vorwärts wie 6 bis ein bisschen zuletzt ich 

22
00:02:46,180 --> 00:02:49,440
zeigt es dir im Profil 

23
00:02:55,200 --> 00:03:02,769
Mein Becken ist nicht zynisch, als ob ich zum Beispiel fahre 

24
00:03:02,769 --> 00:03:07,629
ein Auto sehen dort, aber ich zeige uns zuletzt 

25
00:03:07,629 --> 00:03:11,860
weil mein Schwerpunkt hinter meinem Körper liegt, möchte ich 

26
00:03:11,860 --> 00:03:19,420
sieht qualitativ hochwertig aus hallo vorne und ich mache das bei dassin brücken natürlich die 

27
00:03:19,420 --> 00:03:24,970
Was wir möchten und vor der Schwerkraft vorbeigehen, gehen meine Knie zu Boden 

28
00:03:24,970 --> 00:03:32,970
Dies ist, was Sie beginnen müssen, indem Sie in der Haltung einen Sitzplatz festlegen 

29
00:03:32,970 --> 00:03:42,609
bei dem das Knie in den Streik tritt, wenn Sie 7 bis 6 vom Becken aus ausgeführt haben 

30
00:03:42,609 --> 00:03:44,920
der leicht geneigt war, um die Anfrage zu stellen 

31
00:03:44,920 --> 00:03:51,940
Sie werden die Wirbelsäule so weit wie möglich strecken 

32
00:03:51,940 --> 00:03:57,000
oben auf dem Kopf betrachten unsere als das betrachtet 

33
00:03:57,000 --> 00:04:05,889
Köln geht so weit, als ob ich ein Spiel setzen würde, ist kein Haken, den ich noch einmal lese 

34
00:04:05,889 --> 00:04:12,280
was bedeutet, dass ich den Nacken gezogen habe, den ich an mein Kinn gepasst habe 

35
00:04:12,280 --> 00:04:16,349
Kopf dreht sich vorne Ich habe keinen gebogenen Rücken 

36
00:04:16,349 --> 00:04:24,610
Ich bin so ehrlich wie möglich. Ich zeige Ihnen zwei Profile, die ich nicht bin 

37
00:04:24,610 --> 00:04:33,910
als ob ich vorher nicht versteigert wäre oder was auch immer ich am meisten bin 

38
00:04:33,910 --> 00:04:43,659
Auch wenn Sie es geschafft haben, aufrecht zu sitzen, bewegen Sie sich nicht 

39
00:04:43,659 --> 00:04:53,080
Je weiter ich mit der linken Hand fortfahre, desto mehr sind die Finger mit der rechten Hand verbunden 

40
00:04:53,080 --> 00:04:58,690
Die Finger sind gesegnet und ich werde die linken Finger auf die legen 

41
00:04:58,690 --> 00:05:08,030
Spuren der rechten Hand, aber Drücke treffen auf den Finger der 

42
00:05:08,030 --> 00:05:14,300
linke Hand eine horizontale Linie bildet den Rand meiner Hände 

43
00:05:14,300 --> 00:05:17,840
so konnten wir die Grafschaft über das ganze Handgelenk 

44
00:05:17,840 --> 00:05:23,480
Bis dahin wirst du morgen die Kante meiner Hände auf die Basis auftragen 

45
00:05:23,480 --> 00:05:31,370
des Bauches, um dies zu tun, benutzte etwas, das 

46
00:05:31,370 --> 00:05:37,460
kann es dir ermöglichen, in den Händen von Terrasson zu stehen, die du loslassen kannst 

47
00:05:37,460 --> 00:05:43,780
volle Aufmerksamkeit von den Schultern die Haltung einer Haltung, in der Sie loslassen müssen 

48
00:05:43,780 --> 00:05:47,990
Lass die Spannung los, die du siehst. Meine Ellbogen sind frei. Ich bin schon lange 

49
00:05:47,990 --> 00:05:56,090
so oder so leise, aber ich bemühe mich nicht zu pflegen 

50
00:05:56,090 --> 00:06:01,070
meine Hände die Mühe Ideen, um die Pushs im Restaurant aufrechtzuerhalten 

51
00:06:01,070 --> 00:06:08,650
Sobald ich merke, dass dies der Punkt der Haltung ist, berühre ich ihn 

52
00:06:08,650 --> 00:06:13,120
Das ist körperliche Haltung 

53
00:06:20,000 --> 00:06:25,460
nach diesem ersten Teil auf Körperhaltung zweiter Teil 

54
00:06:25,460 --> 00:06:30,860
die mentale Einstellung, sobald ich in der Haltung von bin 

55
00:06:30,860 --> 00:06:39,220
Zen-Meditation vor der Wand Was mache ich, was soll ich tun? 

56
00:06:39,220 --> 00:06:46,190
In der Stille dieser Haltung wirst du zu anderen gehen 

57
00:06:46,190 --> 00:06:53,390
Erkenne, dass du denkst, du siehst, dass du weiter denkst und dass es solche gibt 

58
00:06:53,390 --> 00:06:58,490
Tausende von Gedankenbürsten, die kommen und kommen, kommen zu dir 

59
00:06:58,490 --> 00:07:03,010
Komm und erkenne, dass dies ein natürliches Phänomen ist 

60
00:07:03,010 --> 00:07:08,840
Sie können nicht kontrollieren, was wir während der Zen-Meditation tun werden 

61
00:07:08,840 --> 00:07:16,790
wir werden unsere Gedanken vergehen lassen, das heißt in dieser Unbeweglichkeit 

62
00:07:16,790 --> 00:07:23,150
Wir werden in der Lage sein zu beobachten, dass Sie mit einem Lösegeld Ihre Gedanken beobachten und 

63
00:07:23,150 --> 00:07:29,419
Gedanken anstatt sie zu pflegen anstatt sie zu züchten, gehst du 

64
00:07:29,419 --> 00:07:34,610
Lass sie passieren, damit es nicht darum geht, was abzulehnen 

65
00:07:34,610 --> 00:07:39,580
Lass es wieder da sein. Dies ist die Maschine, die du passieren lassen wirst 

66
00:07:39,580 --> 00:07:46,190
wie man Aufmerksamkeitsfokus durchlässt 

67
00:07:46,190 --> 00:07:51,400
hier jetzt auf deinen Atem 

68
00:07:55,430 --> 00:07:59,960
Das Einatmen von Zen ist Bauchatmung 

69
00:07:59,960 --> 00:08:10,400
Sie atmen durch Ihr Nasenloch ein und Sie atmen langsam langsam ein 

70
00:08:10,400 --> 00:08:14,270
geräuschlose Pause stille Meditation 

71
00:08:14,270 --> 00:08:20,090
die Eigenschaft, dass das Atmen und Sie Ihre Aufmerksamkeit lenken werden 

72
00:08:20,090 --> 00:08:30,930
Angesichts der Erhebung inspirieren wir, indem wir ruhig im Sein ankommen 

73
00:08:30,930 --> 00:08:37,620
großzügig füllen und sich der Nation stellen, wenn 

74
00:08:37,620 --> 00:08:44,070
du gehst die ganze Luft raus du machst es langsam langsam wie für 

75
00:08:44,070 --> 00:08:47,720
immer länger werden 

76
00:08:56,540 --> 00:09:03,889
Wenn Sie am Ende Ihrer Beziehung sind, sehnen Sie sich immer wieder 

77
00:09:03,889 --> 00:09:12,709
Sie atmen aus und Sie waren nicht mehr langsam tiefer nackt, je länger 

78
00:09:12,709 --> 00:09:18,069
tief neu wie die Wellen auf einem Ozean 

79
00:09:18,069 --> 00:09:31,639
weil sie der Strand sind und alle diese mit der v6 sprechen, auf die Sie sich konzentrieren 

80
00:09:31,639 --> 00:09:36,440
hier nun zu den Punkten der physischen Punktehaltung 

81
00:09:36,440 --> 00:09:42,259
Also pass auf, pass hier auf deinen Körper auf, wenn du 

82
00:09:42,259 --> 00:09:47,660
konzentrierte sich hier jetzt zur gleichen Zeit nicht auf deine zu folgen 

83
00:09:47,660 --> 00:09:55,040
Gedanken lassen Sie uns zu den Punkten zurückkehren, die Sie einnehmen, wenn Sie sich hier konzentrieren 

84
00:09:55,040 --> 00:10:03,139
jetzt auf eine Atmung in tapfer harmonisch und nach und nach 

85
00:10:03,139 --> 00:10:11,110
Holen Sie sich den Atem aus dem Atem immer länger 

86
00:10:11,110 --> 00:10:18,860
Aber die Flossen haben keine Zeit mehr darüber nachzudenken, was Sie hier sind und 

87
00:10:18,860 --> 00:10:27,860
jetzt in zaza Haltung ist hier in Haltung noch die Körpermaterie in 

88
00:10:27,860 --> 00:10:35,569
Haltung Ich erinnere Sie daran, dass Sie Eis ein Druckknie in Richtung drücken 

89
00:10:35,569 --> 00:10:38,680
Boden schieben Sie die Erde 

90
00:10:38,680 --> 00:10:44,920
und strecken Sie Ihre Wirbelsäule so weit wie möglich nach oben 

91
00:10:44,920 --> 00:10:49,260
Schädel, als wollte er den Himmel mit dem Kopf schieben 

92
00:10:49,260 --> 00:10:55,290
die linke Hand wie rechts die horizontalen Daumen die Schneide der Hände 

93
00:10:55,290 --> 00:11:05,020
gegen den ball gut ich bin immer noch in saumur vor der wand gibt es nichts zu tun 

94
00:11:05,020 --> 00:11:11,459
Besonders was ich tun werde, ist meinen Blick nach innen zu richten 

95
00:11:11,459 --> 00:11:16,330
Also die Sinne werde ich nicht mehr benutzen 

96
00:11:16,330 --> 00:11:22,060
Diese Tatsache Meditation Ich bin vor der Wand, so gibt es nichts zu sehen, so ist es 

97
00:11:22,060 --> 00:11:29,890
als ob meine Vision ich es vor mir auf meinem geschäftigen deshalb während stelle 

98
00:11:29,890 --> 00:11:38,620
Meditation gab es die 45 ° Ansicht diagonal nach Europa, die Sie können 

99
00:11:38,620 --> 00:11:43,839
Meditiere mit geschlossenen Augen, aber wenn du deine Augen offen hältst oder die letzte 

100
00:11:43,839 --> 00:11:50,100
Kontakt mit der Realität geht nicht besonders in Ihren Gedanken 

101
00:11:50,100 --> 00:11:56,110
Da wir in der Stille sind, benutzen wir das Wasser dort auch nicht so 

102
00:11:56,110 --> 00:12:00,880
Zu hören, dass es kein bisschen Musik braucht und wir haben bereits gesehen, dass wir es sind 

103
00:12:00,880 --> 00:12:09,040
Gémonies so nach und nach schneiden wir plötzlich mit den Bitten der Welt 

104
00:12:09,040 --> 00:12:15,880
Draußen machen wir den Meditationsschuss, den du sitzt 

105
00:12:15,880 --> 00:12:21,970
physisch ist es der Körper, der sich in einer Haltung befindet, in der Sie sich schneiden 

106
00:12:21,970 --> 00:12:28,360
Anfragen an die Stände, an denen Sie alles in den Sofortflugzeugmodus versetzen, sagen es mir nicht 

107
00:12:28,360 --> 00:12:34,340
Meditiere nicht mit deinem Handy nebenan 

108
00:13:09,540 --> 00:13:14,399
Einen Tipp für Anfänger zu vervollständigen ist also nicht einfach 

109
00:13:14,399 --> 00:13:23,519
so etwas zu finden, ist jetzt weder eine Rüstung noch eine Rüstung 

110
00:13:23,519 --> 00:13:28,579
eine wirklich gute eine gute Hilfe dir wirklich, ich rate dir dazu 

111
00:13:28,579 --> 00:13:33,959
Wenn Sie es ausprobieren, werden Sie sehen, anstatt unterhaltsame Gedanken zu haben 

112
00:13:33,959 --> 00:13:38,790
dass sie vielleicht nicht bereit sind hell, vielleicht nicht bereit fröhlich stattdessen 

113
00:13:38,790 --> 00:13:43,380
schlechte Laune zu haben oder im Mittelpunkt des Stresses zu stehen 

114
00:13:43,380 --> 00:13:52,620
Normal hier mit dieser Arbeit am Gold die Haltung des Geistes, den Sie loslassen 

115
00:13:52,620 --> 00:13:57,810
Sie distanzieren sich von allem, was uns passiert, um zu sehen, dass Sie 

116
00:13:57,810 --> 00:14:03,329
Wirst du dich körperlich beruhigen und das alles emotional atmen? 

117
00:14:03,329 --> 00:14:08,310
es wird dir helfen loszulassen es wird dir und deinem zutiefst helfen 

118
00:14:08,310 --> 00:14:13,079
Gefolge, wenn Sie so wollen, wurde begleitet, als Sie nicht daran zweifelten 

119
00:14:13,079 --> 00:14:15,529
üben 

