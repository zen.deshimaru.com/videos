1
00:00:02,555 --> 00:00:04,935
Abbiamo parlato nel kusen...

2
00:00:05,365 --> 00:00:08,125
qui in Europa...

3
00:00:14,475 --> 00:00:17,625
...volizione.

4
00:00:18,965 --> 00:00:21,545
Non è la volontà.

5
00:00:22,980 --> 00:00:27,800
La volizione è qualcosa che si innesca

6
00:00:42,860 --> 00:00:44,860
...inconsciamente.

7
00:00:56,040 --> 00:00:58,740
In seguito, ho spiegato...

8
00:01:00,840 --> 00:01:03,660
...qui in Occidente...

9
00:01:11,580 --> 00:01:16,280
Prima di tutto, buongiorno a tutti!

10
00:01:21,100 --> 00:01:23,915
Condividiamo subito questo zazen.

11
00:01:27,795 --> 00:01:29,465
con grande piacere

12
00:01:41,220 --> 00:01:46,880
Siamo riusciti a organizzare e a praticare un campo estivo.

13
00:01:49,540 --> 00:01:53,520
nonostante gli eventi complicati

14
00:01:56,080 --> 00:01:59,870
Siamo limitati nel numero, perché non siete venuti

15
00:02:05,140 --> 00:02:08,080
a causa dello spazio di sicurezza

16
00:02:13,600 --> 00:02:19,900
Siamo costretti a essere più distanziati nel Dojo

17
00:02:20,140 --> 00:02:25,820
Abbiamo fatto tutto seriamente, abbiamo seguito le regole...

18
00:02:48,715 --> 00:02:52,160
E’ una delle...

19
00:02:52,160 --> 00:02:54,860
le particolarità zen

20
00:02:58,940 --> 00:03:01,860
che seguire la regola

21
00:03:02,380 --> 00:03:07,780
"Sì, ma non sono stato educato così... Non sono così, io!"

22
00:03:12,340 --> 00:03:16,840
In zen... seguiamo la regola...

23
00:03:20,685 --> 00:03:23,535
La regola dove siamo.

24
00:03:33,200 --> 00:03:36,580
Così ho spiegato che la volontà

25
00:03:39,480 --> 00:03:42,380
è Yin o Yang

26
00:03:47,760 --> 00:03:52,500
Quando dico rilassa le gambe, questa è la volontà di Yin...

27
00:03:53,840 --> 00:03:56,220
Ci lasciamo andare

28
00:04:06,125 --> 00:04:08,765
Quando ti lasci andare, fai scattare una risonanza...

29
00:04:19,460 --> 00:04:23,340
la volontà Yang...

30
00:04:23,640 --> 00:04:26,820
è spingere la terra con le ginocchia.

31
00:04:40,600 --> 00:04:43,640
Ho raccontato la storia

32
00:04:46,480 --> 00:04:48,380
e pensieri

33
00:04:50,300 --> 00:04:52,760
Sul metodo Yang di Baso

34
00:04:54,760 --> 00:04:57,500
E il metodo Yin di Sekito

35
00:04:59,440 --> 00:05:03,380
Condividerò questo kusen con te

36
00:05:22,140 --> 00:05:25,300
È una storia molto famosa

37
00:05:27,420 --> 00:05:31,780
ma visto da un’angolazione diversa dal solito.

38
00:05:33,960 --> 00:05:39,300
Stiamo parlando di Baso che aveva già ricevuto la trasmissione del Dharma...

39
00:05:40,620 --> 00:05:42,680
Dal suo padrone

40
00:05:49,820 --> 00:05:54,400
Ha continuato a seguire gli insegnamenti del suo maestro e sesshins...

41
00:06:03,270 --> 00:06:08,320
Aveva un modo divertente

42
00:06:11,580 --> 00:06:18,180
Avrebbe fatto zazen da solo nel Dojo anche se non era il momento...

43
00:06:19,740 --> 00:06:23,730
Ha sempre voluto fare zazen

44
00:06:28,320 --> 00:06:34,360
Un giorno, il suo padrone è venuto a trovarlo mentre era seduto...

45
00:06:36,360 --> 00:06:38,400
e le chiede

46
00:06:40,480 --> 00:06:42,920
Grande monaco,

47
00:06:43,425 --> 00:06:46,645
-Baso era molto grande-

48
00:06:48,400 --> 00:06:53,700
Cosa cerchi quando ti siedi in zazen?

49
00:06:57,060 --> 00:06:58,640
In altre parole:

50
00:06:58,860 --> 00:07:03,920
cosa stai cercando? Avete uno scopo?

51
00:07:04,300 --> 00:07:08,560
Hai uno scopo che va oltre...

52
00:07:10,580 --> 00:07:15,000
che è più importante del semplice sedersi?

53
00:07:20,040 --> 00:07:22,540
Questa storia è molto conosciuta

54
00:07:24,325 --> 00:07:28,875
E’ l’aspetto Yang della volizione...

55
00:07:31,705 --> 00:07:34,625
In seguito, ho inventato i dialoghi

56
00:07:41,045 --> 00:07:44,175
"- Hai uno scopo?"

57
00:07:44,565 --> 00:07:47,525
"-Sì, ho uno scopo", dice Baso.

58
00:07:50,435 --> 00:07:54,045
Voglio diventare Buddha. Voglio diventare Buddha.

59
00:07:56,345 --> 00:07:59,445
Quindi...

60
00:07:59,535 --> 00:08:02,035
Sto mirando

61
00:08:02,225 --> 00:08:05,195
il centro del bersaglio

62
00:08:08,525 --> 00:08:11,655
e lo raggiungo ogni volta.

63
00:08:21,920 --> 00:08:25,000
Questo è l’aspetto Yang di zazen.

64
00:08:25,240 --> 00:08:26,720
che ho menzionato ieri

65
00:08:33,620 --> 00:08:35,080
Ho detto a

66
00:08:36,800 --> 00:08:42,360
come il maestro Deshimaru ci ha insegnato quando eravamo giovani...

67
00:08:49,120 --> 00:08:53,785
Stava causando molta tensione nel dojo con il suo kyosaku.

68
00:08:55,045 --> 00:08:57,875
E’ il metodo Yang

69
00:08:59,385 --> 00:09:02,385
Appena qualcuno si è mosso

70
00:09:03,105 --> 00:09:05,395
o si è addormentato

71
00:09:06,385 --> 00:09:09,345
lo ha colpito

72
00:09:09,545 --> 00:09:12,765
Eravamo al campo estivo

73
00:09:12,875 --> 00:09:15,405
Faceva molto caldo

74
00:09:15,915 --> 00:09:17,455
eravamo bagnati fradici.

75
00:09:19,765 --> 00:09:22,925
C’era una pozzanghera sul tatami dopo lo zazen.

76
00:09:32,260 --> 00:09:37,340
"Mi siedo per diventare un Buddha, e questo è tutto. Ci ho messo tutta la mia energia.

77
00:09:40,910 --> 00:09:43,550
È il metodo di Deshimaru.

78
00:09:45,080 --> 00:09:48,490
Praticare con tutte le proprie forze per il piacere

79
00:09:51,435 --> 00:09:54,475
Ecco cosa stava facendo Baso

80
00:10:00,980 --> 00:10:03,980
Quindi il metodo Yin,

81
00:10:05,960 --> 00:10:10,240
Quello che volevo dire è che Baso non aveva dubbi

82
00:10:13,420 --> 00:10:19,180
Non gli importava se aveva uno scopo o meno, se era Mushotoku o no.

83
00:10:24,460 --> 00:10:28,020
E’ stata la volizione. Aveva il desiderio, aveva la volontà.

84
00:10:37,535 --> 00:10:40,345
Sensei ha detto:

85
00:10:41,185 --> 00:10:44,415
è meglio fare zazen meno tempo

86
00:10:48,985 --> 00:10:51,825
ma facendo un forte zazen, come Baso...

87
00:10:58,875 --> 00:11:02,865
Piuttosto che fare zazen per molto tempo, tutto il giorno,

88
00:11:06,525 --> 00:11:10,005
e per dormire o pensare

89
00:11:18,425 --> 00:11:19,425
20 minuti.

90
00:11:19,565 --> 00:11:22,445
con tutte le sue forze

91
00:11:24,055 --> 00:11:26,785
è molto efficace

92
00:11:31,355 --> 00:11:33,725
Quando si ama zazen,

93
00:11:35,995 --> 00:11:38,475
per divertimento...

94
00:11:42,375 --> 00:11:45,085
Non siamo dei proselitisti.

95
00:11:56,515 --> 00:12:01,235
Stiamo solo condividendo, semplicemente per il gusto di farlo.

96
00:12:41,325 --> 00:12:44,085
Torniamo all’esempio di Yin,

97
00:12:44,205 --> 00:12:45,865
Allentare la tensione

98
00:12:47,820 --> 00:12:51,610
quando rilascio le gambe, rilascia tutto il bacino...

99
00:12:54,180 --> 00:12:57,340
Mi sento come se fossi senza peso.

100
00:13:10,580 --> 00:13:13,040
L’esempio dato

101
00:13:14,165 --> 00:13:18,815
è arrivare al momento tanto atteso in cui andremo a fare un pisolino

102
00:13:26,135 --> 00:13:29,145
Dove si lascia tutto

103
00:13:30,775 --> 00:13:33,425
per dormire

104
00:13:35,935 --> 00:13:39,845
L’esempio del pisolino è ovviamente una metafora...

105
00:13:42,045 --> 00:13:45,005
che si riferisce a zazen

106
00:13:59,545 --> 00:14:02,295
Tuttavia, 7 secoli a.C.

107
00:14:09,240 --> 00:14:12,240
uno scienziato di nome Pitagora

108
00:14:14,595 --> 00:14:16,545
ha detto:

109
00:14:18,960 --> 00:14:21,300
prestare attenzione alla notte

110
00:14:27,500 --> 00:14:33,120
dato che non si può controllare la notte

111
00:14:34,600 --> 00:14:39,980
Fate attenzione quando vi addormentate.

112
00:14:43,600 --> 00:14:48,240
Non addormentatevi senza aver controllato quell’ultimo momento prima di addormentarvi.

113
00:15:02,695 --> 00:15:05,735
Zazen è composto da questi due aspetti

114
00:15:05,885 --> 00:15:08,785
yin e yang

115
00:15:11,440 --> 00:15:16,260
che sono stati espressi a loro volta dai grandi maestri Baso e Sekito

116
00:15:21,370 --> 00:15:36,380
Uno può essere chiamato "Uccidi il Buddha" e l’altro "Libera il Buddha".

117
00:15:41,680 --> 00:15:49,020
Il grande maestro Sekito era il suo contemporaneo. Erano monaci che vivevano nello stesso tempo.

118
00:15:52,300 --> 00:15:56,300
Baso e Sekito erano dello stesso periodo.

119
00:16:00,980 --> 00:16:04,560
Spesso i discepoli dell’uno andavano all’altro.

120
00:16:10,570 --> 00:16:14,000
A quel tempo, Sekito non aveva ancora discepoli...

121
00:16:17,455 --> 00:16:20,865
Viveva nel bosco... in montagna...

122
00:16:22,445 --> 00:16:24,945
Si era costruito

123
00:16:25,385 --> 00:16:28,405
una piccola capanna

124
00:16:34,335 --> 00:16:36,915
con legno e paglia

125
00:16:44,315 --> 00:16:48,895
sulla stessa montagna dove il Maestro Nangaku aveva insegnato...

126
00:16:54,435 --> 00:16:57,825
Sekito dice:

127
00:16:58,545 --> 00:17:01,475
"Mi sento protetto in questa capanna".

128
00:17:02,865 --> 00:17:07,355
L’ho costruita io stesso. È la mia unica ricchezza.

129
00:17:10,965 --> 00:17:13,395
Ma quando finisco il mio pasto,

130
00:17:16,045 --> 00:17:19,015
Preparo con calma il momento del pisolino...

131
00:17:23,655 --> 00:17:26,825
e lo dico ogni giorno

132
00:17:29,295 --> 00:17:34,525
e di nuovo ogni giorno

133
00:17:38,875 --> 00:17:41,665
L’esperienza del pasto e del pisolino,

134
00:17:44,135 --> 00:17:46,955
è un potere dei patriarchi

135
00:17:50,035 --> 00:17:53,055
quello che non ha ancora finito il suo pasto,

136
00:17:56,345 --> 00:18:03,075
non è stato ancora completato.

137
00:18:08,215 --> 00:18:10,015
Non è ancora soddisfatto.
