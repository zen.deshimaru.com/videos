1
00:00:00,000 --> 00:00:04,660
Você disse que se os chefes de estado realizam prostações, 

2
00:00:05,080 --> 00:00:08,560
o mundo seria mais pacífico. 

3
00:00:08,880 --> 00:00:13,040
Eu estava pensando … 

4
00:00:13,340 --> 00:00:18,340
comparado com outras religiões. 

5
00:00:18,620 --> 00:00:23,080
Na religião muçulmana, fala-se em prostração. 

6
00:00:23,460 --> 00:00:34,540
Ainda assim, alguns países do Oriente Médio estão se despedaçando. 

7
00:00:35,180 --> 00:00:42,020
Eu queria saber o porquê. 

8
00:00:42,320 --> 00:00:46,000
Porque a prostração é a mesma no Zen e no Islã. 

9
00:00:46,340 --> 00:00:52,660
Eu acho que existe o bem e o mal em todas as religiões. 

10
00:00:53,080 --> 00:00:58,240
Mas a prostração é uma coisa boa, na religião muçulmana. 

11
00:00:58,400 --> 00:01:04,620
Os cristãos também dão as mãos da mesma maneira que no Zen, em Gasshô. 

12
00:01:05,100 --> 00:01:08,520
Tudo bem. 

13
00:01:08,700 --> 00:01:14,040
Também há fanatismo, sempre esteve lá. 

14
00:01:14,300 --> 00:01:18,820
Sempre houve guerras religiosas. 

15
00:01:21,320 --> 00:01:26,920
No Zen, a prostração não é feita para uma finalidade específica. 

16
00:01:27,280 --> 00:01:33,440
Nem para adorar a estátua no altar, nem para orar a Buda. 

17
00:01:33,960 --> 00:01:39,640
É a prática da postura com todo o corpo. 

18
00:01:40,020 --> 00:01:46,140
No momento em que você bate no chão, há uma consciência. 

19
00:01:46,500 --> 00:01:50,860
Normalmente, o cérebro nunca toca o chão. 

20
00:01:51,100 --> 00:01:55,820
Está sempre no céu, sempre apontando para as estrelas. 

21
00:01:56,100 --> 00:02:01,480
Então, isso parece certo. Para reconectar, incline-se, toque em. 

22
00:02:01,740 --> 00:02:06,300
Quando os muçulmanos fazem isso, é porque há coisas muito boas no Islã. 

23
00:02:06,660 --> 00:02:13,000
As famílias criam bem os filhos. 

24
00:02:15,500 --> 00:02:21,620
Muçulmanos de verdade estão cheios de amor. 

25
00:02:24,200 --> 00:02:30,020
Não jogue o bebê fora com a água do banho! 

26
00:02:30,550 --> 00:02:34,450
É bom que eles se prostrem! 

