1
00:00:00,290 --> 00:00:08,820
Eu sou a freira Gyu Ji, ordenada por meu mestre há quase 40 anos

2
00:00:08,820 --> 00:00:16,440
e também sou certificado por meu Mestre, Mestre Kosen, de quem recebi o que se chama Shihô.

3
00:00:16,720 --> 00:00:21,580
o que me permite
abrir-me ao ensino

4
00:00:21,880 --> 00:00:25,200
para ajudar a passar o Zen.

5
00:00:25,520 --> 00:00:29,880
Eu vivo no templo, o que significa que sou eu quem organiza

6
00:00:30,220 --> 00:00:35,400
fora de sesshin a vida deste templo,

7
00:00:35,680 --> 00:00:40,680
que garante a sua manutenção, que pode acomodar pessoas fora de sesshin.

8
00:00:40,680 --> 00:00:43,700
para que ele possa viver sua vida inteira
o ano.

9
00:00:43,700 --> 00:00:49,950
Estamos no Caroux, a uma hora e meia de Montpellier, a uma hora de Béziers.

10
00:00:49,950 --> 00:00:55,370
Estamos no meio da natureza, é um templo...
que já existe há dez anos.

11
00:00:55,370 --> 00:01:03,390
Assim, a vida do Templo Yujo Nyusanji é...
simples, é rítmico.

12
00:01:03,390 --> 00:01:09,600
Ritmados pela prática do zazen, levantamo-nos pela manhã, vamos ao dojo, fazemos zazen, o que nos é ensinado.

13
00:01:09,600 --> 00:01:15,450
a linhagem dos Mestres, seja ela qual for 
Mestre Kosen que está na origem

14
00:01:15,450 --> 00:01:21,180
deste templo, seu Mestre Deshimaru e
subimos, subimos, voltamos a subir, voltamos a subir

15
00:01:21,180 --> 00:01:26,700
Shakyamuni Buda. Zazen é
o centro da prática deste templo

16
00:01:26,700 --> 00:01:33,960
é uma meditação sentada, silenciosa, uma meditação em tranqüilidade

17
00:01:33,960 --> 00:01:42,570
onde, postura e condição
de espírito estão em harmonia.

18
00:01:42,570 --> 00:01:47,820
A vida no templo também é vida em comum com os outros,

19
00:01:48,140 --> 00:01:52,220
os momentos em que fazemos o samu, ou seja, a partilha de tarefas.

20
00:01:52,500 --> 00:02:00,140
se é cozinhar, jardinar, pintar um muro, tudo o que há para fazer desde
para que este templo possa viver.

21
00:02:00,420 --> 00:02:06,960
Mas é um lugar aberto, não é um lugar
onde você está obrigado a ser contratado

22
00:02:06,960 --> 00:02:12,680
no budismo, onde você está obrigado a se tornar uma pessoa religiosa.

23
00:02:12,680 --> 00:02:18,109
é um lugar para todos. Além disso, o Mestre Kosen nos disse uma vez

24
00:02:18,109 --> 00:02:23,390
somos monges leigos é importante para nós sermos confrontados

25
00:02:23,390 --> 00:02:28,579
para a vida de todos, incluindo nossa prática,

26
00:02:28,579 --> 00:02:33,280
nossa prática é a força motriz que nos leva a viver uma vida para todos
os dias.

27
00:02:33,280 --> 00:02:40,010
O templo é para onde estamos indo.
gastar algum tempo para aprofundar sua prática,

28
00:02:40,010 --> 00:02:45,900
para dar um passo atrás
em relação à sua vida diária,

29
00:02:46,440 --> 00:02:51,280
Pode ser apenas um fim de semana. Pode ser...
poderia ser um mês, poderia ser três meses.

30
00:02:51,780 --> 00:02:59,500
É uma passagem do templo, é um
momento em sua vida, em seu ano.

31
00:03:00,580 --> 00:03:05,180
A vida do templo também inclui os retiros, o chamado sesshin.

32
00:03:05,180 --> 00:03:11,540
Agora estamos em desvantagem numérica,
são períodos mais intensivos

33
00:03:11,540 --> 00:03:16,820
de praticar zazen também com samu, é claro.

34
00:03:17,340 --> 00:03:27,520
A peculiaridade deste templo é que não temos a ótica para imitar nada que já exista.

35
00:03:27,820 --> 00:03:33,300
é um zen como o Mestre Deshimaru o ensinou, muito criativo,

36
00:03:33,840 --> 00:03:37,800
com o que somos, com o que temos.

37
00:03:38,260 --> 00:03:46,120
Rituais também são praticados neste templo, mas com parcimônia,

38
00:03:46,440 --> 00:03:51,360
como diria o Mestre Kosen, a dose certa não é muito nem muito pouca.

39
00:03:51,900 --> 00:03:56,820
Eu vivo no local e lhe dou as boas-vindas.
