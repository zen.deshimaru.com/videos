1
00:00:10,260 --> 00:00:23,160
Ogni giorno, in tutto il mondo, i membri del sangha Kosen praticano insieme lo zazen. 

2
00:00:24,360 --> 00:00:28,520
Ti ho parlato dell’infanzia del Maestro Deshimaru. 

3
00:00:28,820 --> 00:00:30,820
Dai suoi dubbi, la sua ricerca … 

4
00:00:30,820 --> 00:00:35,760
Fino a incontrare Kodo Sawaki e trovare la sua posa da zazen. 

5
00:00:36,040 --> 00:00:39,100
Trovare la tua posa di zazen: 

6
00:00:40,240 --> 00:00:41,860
"Che piacere!" 

7
00:00:42,240 --> 00:00:46,100
Credo che quelli che hanno avuto modo di conoscere il loro atteggiamento zazen … 

8
00:00:46,100 --> 00:00:49,120
Non hanno perso tempo su questo pianeta. 

9
00:00:49,500 --> 00:00:52,540
È un dono incommensurabile .. 

10
00:00:52,540 --> 00:00:54,239
Anche se in questo atteggiamento … 

11
00:00:54,540 --> 00:00:59,920
C’è anche sofferenza e paura. 

12
00:01:00,320 --> 00:01:01,960
Tutto è presente in questo atteggiamento. 

13
00:01:01,960 --> 00:01:06,160
Ma lo guardi dall’alto. 

14
00:01:06,500 --> 00:01:10,740
Prima di imparare le lezioni di Kodo Sawaki .. 

15
00:01:12,040 --> 00:01:15,980
Il maestro Deshimaru aveva studiato la Bibbia. 

16
00:01:17,360 --> 00:01:21,020
Era un po ’innamorato di una ragazza olandese … 

17
00:01:23,020 --> 00:01:24,820
La figlia di un ministro. 

18
00:01:24,960 --> 00:01:28,380
Ha colto l’occasione per studiare la Bibbia e l’inglese. 

19
00:01:28,720 --> 00:01:32,140
In realtà, ha studiato l’inglese dallo studio della Bibbia. 

20
00:01:32,140 --> 00:01:37,060
Ha visitato chiese cristiane e ha cantato inni. 

21
00:01:37,220 --> 00:01:41,360
Te lo dico perché è Pasqua. 

22
00:01:41,660 --> 00:01:51,880
E ora Gesù è tra la sua tomba e la quarta o quinta dimensione. 

23
00:01:52,340 --> 00:01:56,580
Sensei ci ha detto che quando è venuto in Francia .. 

24
00:01:57,000 --> 00:02:02,360
Aveva portato la sua Bibbia e gli inni della sua giovinezza. 

25
00:02:02,680 --> 00:02:07,940
Durante una sesshin di Pasqua, ci ha dato un bacio su Cristo. 

26
00:02:08,280 --> 00:02:11,720
Secondo il Vangelo di Luca .. 

27
00:02:11,720 --> 00:02:15,620
In modo che Cristo … 

28
00:02:16,140 --> 00:02:20,240
Potrebbero parlare per Pontius Pilatus di essere giudicato .. 

29
00:02:20,600 --> 00:02:26,240
Ponzio Pilato chiamò i sommi sacerdoti. 

30
00:02:26,240 --> 00:02:28,820
E interrogò Gesù in loro presenza. 

31
00:02:29,180 --> 00:02:30,340
Dice loro: 

32
00:02:30,600 --> 00:02:33,020
"Mi hai presentato quest’uomo come … 

33
00:02:33,020 --> 00:02:39,820
Qualcuno che sta guidando le persone nella direzione sbagliata ". 

34
00:02:40,280 --> 00:02:47,640
"L’ho interrogato e non ho trovato nulla di male in quest’uomo." 

35
00:02:48,000 --> 00:02:50,260
"Non ho trovato nessuno dei motivi per cui lo stai accusando." 

36
00:02:50,580 --> 00:02:56,940
"Anche Erode non ha trovato alcun motivo per caricare". 

37
00:02:57,260 --> 00:03:02,620
"Concludo che quest’uomo non ha fatto nulla per meritare la pena di morte". 

38
00:03:03,240 --> 00:03:06,480
A quel tempo, le persone hanno esclamato con una sola voce: 

39
00:03:06,480 --> 00:03:15,100
"Morto a Gesù, libera Barabba!" 

40
00:03:16,240 --> 00:03:24,280
"Barabba è stato incarcerato per condotta disordinata e omicidio." 

41
00:03:24,620 --> 00:03:29,100
Pontius Pilatus ha offerto .. 

42
00:03:29,100 --> 00:03:32,200
Per liberare uno dei prigionieri. 

43
00:03:32,800 --> 00:03:38,480
Ma il popolo disse: "Barabba gratis!" 

44
00:03:38,940 --> 00:03:44,260
Ponzio Pilato si rivolge di nuovo alle persone: 

45
00:03:44,260 --> 00:03:49,640
E li dice di nuovo e parla alla gente: 

46
00:03:49,960 --> 00:03:53,820
"Sii ragionevole, Gesù non ha fatto nulla di male .. 

47
00:03:53,820 --> 00:03:55,800
Ha appena parlato, tutto qui! " 

48
00:03:55,800 --> 00:03:59,620
Tutti gridarono: "Crocifiggilo!" 

49
00:03:59,860 --> 00:04:02,320
Ponzio Pilato alla fine disse: 

50
00:04:02,620 --> 00:04:04,340
"Come desideri .. 

51
00:04:04,340 --> 00:04:06,300
Non è un mio problema .. " 

52
00:04:07,200 --> 00:04:11,640
E in risposta pronunciò la famosa frase: 

53
00:04:11,640 --> 00:04:13,500
"Ero le mie mani nell’innocenza!" 

54
00:04:13,560 --> 00:04:17,540
Questa espressione significa che .. 

55
00:04:17,540 --> 00:04:19,960
Dovresti lavarti le mani il più spesso possibile. 

56
00:04:20,140 --> 00:04:22,320
Per evitare il coronavirus. 

57
00:04:22,400 --> 00:04:24,920
Devi dire ogni volta, "Mi lavo le mani in innocenza!" 

58
00:04:25,240 --> 00:04:29,880
Questo è un insegnamento di Deshimaru, dice questo. 

59
00:04:30,360 --> 00:04:35,760
"Così Gesù fu condannato dall’opinione pubblica". 

60
00:04:36,020 --> 00:04:39,140
"La psicologia di massa ha ucciso Cristo". 

61
00:04:39,380 --> 00:04:44,200
"Mimetismo, rifiuto di mettersi in discussione … 

62
00:04:44,200 --> 00:04:46,160
Che ha ucciso Cristo ". 

63
00:04:46,300 --> 00:04:48,940
La folla, la folla .. 

64
00:04:48,940 --> 00:04:54,800
Contiene in sé un’energia potente .. 

65
00:04:54,800 --> 00:04:57,720
Che può essere sia negativo che pericoloso. 

66
00:04:57,800 --> 00:05:02,900
È diverso quando ci riuniamo per fare lo zazen. 

67
00:05:03,180 --> 00:05:05,640
Ognuno di noi si guarda .. 

68
00:05:05,640 --> 00:05:11,820
In armonia con il gruppo. 

69
00:05:12,160 --> 00:05:16,260
Questo è particolarmente importante per ora con Zazoom! 

70
00:05:16,680 --> 00:05:20,520
Ad esempio, ho bruciato incenso nella mia stanza. 

71
00:05:20,920 --> 00:05:25,260
Sensei diceva spesso: "In zazen sei solo". 

72
00:05:25,640 --> 00:05:28,220
Ma se pratichi zazen in un dojo .. 

73
00:05:28,220 --> 00:05:31,460
Siamo in un gruppo, anche quando siamo soli. 

74
00:05:31,820 --> 00:05:35,260
Ma poi non c’è confusione emotiva. 

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki diceva a questo proposito: 

76
00:05:38,320 --> 00:05:41,640
"Non dovremmo vivere nella follia di gruppo." 

77
00:05:42,020 --> 00:05:45,680
"E non mantenere questa aberrazione per la vera esperienza." 

78
00:05:45,940 --> 00:05:48,480
"Questo è il destino della gente comune .. 

79
00:05:48,480 --> 00:05:50,540
Chi non ha altro modo di vederlo .. 

80
00:05:50,860 --> 00:05:54,180
Quindi con gli occhi dell’imbecillità collettiva ". 

81
00:05:54,700 --> 00:06:02,080
Il Buddha teneva una lezione pubblica una volta alla settimana. 

82
00:06:02,600 --> 00:06:05,620
Tutti i discepoli, i monaci e i laici .. 

83
00:06:05,620 --> 00:06:08,360
Riuniti fuori .. 

84
00:06:08,360 --> 00:06:11,000
Certamente in luoghi privilegiati. 

85
00:06:11,880 --> 00:06:14,960
Il Buddha ha dato un bacio. 

86
00:06:15,320 --> 00:06:16,980
E tutti erano in zazen .. 

87
00:06:17,960 --> 00:06:19,880
E l’ho ascoltato .. 

88
00:06:19,980 --> 00:06:23,260
E poi tutti sono tornati per la loro strada. 

89
00:06:23,760 --> 00:06:25,560
Tutti si sono esercitati da soli .. 

90
00:06:25,560 --> 00:06:27,960
Una settimana, un mese .. 

91
00:06:28,240 --> 00:06:30,100
E poi tornò regolarmente .. 

92
00:06:30,280 --> 00:06:32,680
per ascoltare le parole del Buddha. 

93
00:06:32,680 --> 00:06:37,840
I dojo sono apparsi solo in Cina. 

94
00:06:38,180 --> 00:06:40,900
E abbiamo iniziato a praticare in gruppo … 

95
00:06:40,900 --> 00:06:42,560
Con un ordine e regole. 

96
00:06:44,580 --> 00:06:48,280
E così il gruppo è molto potente .. 

97
00:06:48,800 --> 00:06:50,960
Sia nel bene che nel male. 

98
00:06:51,200 --> 00:06:53,020
Sensei in realtà ha detto: 

99
00:06:53,020 --> 00:06:57,240
"Gesù Cristo ha dato al suo corpo il sapore della morte". 

100
00:06:57,620 --> 00:07:01,700
"Sul profondo lago del nulla e della disperazione." 

101
00:07:02,180 --> 00:07:06,420
"È diventato un simbolo della vita eterna." 

102
00:07:06,740 --> 00:07:09,180
"La sua vita è identica a quella di un koan Zen." 

103
00:07:09,380 --> 00:07:11,380
Questo è ciò che ha detto Sensei. 
