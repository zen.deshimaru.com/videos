1
00:00:38,480 --> 00:00:40,340
La sessione avrà inizio, 

2
00:00:42,060 --> 00:00:45,680
prima di sederti, orienta bene la fotocamera. 

3
00:00:48,700 --> 00:00:54,020
Non devi parlare, non è questo il momento, facciamo zazen in silenzio. 

4
00:01:00,860 --> 00:01:05,320
Installa te stesso. 

5
00:01:05,320 --> 00:01:10,240
Spingi la terra con le radici 

6
00:01:10,240 --> 00:01:18,780
e allo stesso tempo la colonna vertebrale è allungata verso il cielo. 

7
00:01:21,600 --> 00:01:24,320
La respirazione è nasale, 

8
00:01:24,320 --> 00:01:27,560
inspiri attraverso le narici 

9
00:01:27,560 --> 00:01:29,540
per quelli che hanno 

10
00:01:29,540 --> 00:01:34,820
e espiri profondamente 

11
00:01:34,820 --> 00:01:38,460
ed è qui che lasci andare la tensione 

12
00:01:45,340 --> 00:01:50,960
Zana, stai respirando troppo forte, il tuo corpo si sta muovendo troppo. 

13
00:01:50,960 --> 00:01:56,810
Pensavo che dovessi andare su e giù. 

14
00:01:56,810 --> 00:02:00,240
Soprattutto, fai silenzio Zana. Silenzio! 

15
00:02:04,360 --> 00:02:06,840
Hai delle buone posture. 

16
00:02:11,880 --> 00:02:19,940
Anche se è un dojo virtuale, devi fare i capelli un minimo prima di venire, 

17
00:02:19,940 --> 00:02:23,940
senza nominare nessuno, ad esempio El Broco. 

18
00:02:24,740 --> 00:02:29,460
Stamattina hai pettinato i capelli con un petardo? 

19
00:02:30,720 --> 00:02:33,220
È anche necessario modellare le radici. 

20
00:02:36,000 --> 00:02:39,080
Ci sono posture molto belle. 

21
00:02:42,740 --> 00:02:44,380
Nessuna paura 

22
00:02:45,100 --> 00:02:51,660
in questi tempi difficili, non lasciarti assorbire dall’esterno. 

23
00:02:52,160 --> 00:02:57,940
Refocus, sii il tuo padrone, 

24
00:02:59,480 --> 00:03:04,940
e come diceva Ronaldo: 

25
00:03:38,340 --> 00:03:41,960
Non lasciarti ingannare dalle tue emozioni, 

26
00:03:44,640 --> 00:03:48,620
vedendo questo, come ti senti? 

27
00:03:55,480 --> 00:03:57,640
E vedendo questo? 

28
00:04:07,160 --> 00:04:10,300
Non cercare la gioia, 

29
00:04:12,040 --> 00:04:14,960
non scappare dalla paura 

30
00:04:17,460 --> 00:04:20,620
Perché come diceva Maître Deshimaru: 

31
00:04:22,380 --> 00:04:26,060
"Anche se ti piacciono i fiori appassiscono. 

32
00:04:27,640 --> 00:04:32,780
Anche se non ti piacciono le erbacce, crescono. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Vero! Vero! 

35
00:04:55,600 --> 00:04:58,700
Non abbiamo ancora iniziato? Possiamo muoverci? 

36
00:04:58,700 --> 00:05:02,640
Ricominceremo con Radis e Rose che è in calo. 

37
00:05:03,780 --> 00:05:09,160
Non capisco, devo fermare la fotocamera? Cosa si deve fare? Entrambi allo stesso tempo? 

38
00:05:10,820 --> 00:05:15,340
Chi ha parlato? Sono Muriel, no, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, guarda la tua carota perché se ti muovi troppo, ti fa venire voglia di vomitare. 

