1
00:00:23,160 --> 00:00:26,820
Arti, ¿estás ahí? 

2
00:00:26,820 --> 00:00:28,560
Estoy aquí. 

3
00:00:28,560 --> 00:00:32,940
Es bueno verte, ¿cómo has estado? 

4
00:00:32,940 --> 00:00:34,240
Estoy bien… 

5
00:00:34,240 --> 00:00:38,620
Sabes que te ves gracioso, ¿qué te pasa últimamente? 

6
00:00:38,740 --> 00:00:41,620
Estoy cansado, tengo fiesta con los vecinos. 

7
00:00:41,620 --> 00:00:46,184
¿Qué, pero es un encierro, estás bromeando? No tienes derecho a salir. 

8
00:00:46,184 --> 00:00:50,520
No se me permite, pero no salí, está en el balcón. 

9
00:00:50,520 --> 00:00:56,280
Me haces sentir mejor. Cuidado, justo detrás, retroceda. 

10
00:00:56,440 --> 00:00:58,360
Oh, hombre, te duele. 

11
00:00:58,360 --> 00:01:01,980
Cada noche es un aperitivo. 

12
00:01:01,980 --> 00:01:09,000
Y ayer me caí en la casa del vecino, me encontré en calzoncillos, un piso más abajo, en el contenedor de geranio. 

13
00:01:09,000 --> 00:01:12,180
Deberías tener cuidado Arti de todos modos. 

14
00:01:12,180 --> 00:01:14,140
¿Como estas? 

15
00:01:14,140 --> 00:01:21,220
Está bien, sabes que soy una papa, no es por nada que me llaman patata. 

16
00:01:21,400 --> 00:01:24,840
Oh sí, claro, ¿qué haces en tu día? 

17
00:01:24,840 --> 00:01:29,957
Comenzaré con un poco de meditación. 

18
00:01:29,960 --> 00:01:31,340
Dime… 

19
00:01:31,340 --> 00:01:40,920
No hay nada que contar, me siento así, mira … Me concentro en respirar. 

20
00:01:40,980 --> 00:01:44,140
Tal vez debería hacer esto … 

21
00:01:44,140 --> 00:01:49,340
Entonces me siento mejor, con todos los demás aquí … Estamos llenos. 

22
00:01:49,340 --> 00:01:53,420
¿Estamos llenos? Pero no está permitido, es … ¡encierro! 

23
00:01:53,420 --> 00:01:55,980
No, pero no lo entiendes, lo hacemos con el zoom. 

24
00:01:55,980 --> 00:01:57,460
Con el zoom? 

25
00:01:57,460 --> 00:02:03,200
Sí, te conectas y luego estás con mucha gente … 

26
00:02:03,200 --> 00:02:05,820
Estoy interesado en lo tuyo. 

27
00:02:05,820 --> 00:02:10,452
Mañana sábado practicamos con el Maestro Kosen. 

28
00:02:10,460 --> 00:02:13,320
Estoy interesado. ¿Quién es él? 

29
00:02:13,320 --> 00:02:17,340
Bueno, mira, no está mal. Mañana al mediodía. 

30
00:02:17,500 --> 00:02:20,740
De acuerdo, estaré allí. Me lo explicaras? 

31
00:02:20,740 --> 00:02:25,580
Sí, vamos, te lo explicaré ahora. 

32
00:02:25,740 --> 00:02:28,040
¡Gorrón! ¿Gorrón? 

33
00:02:28,040 --> 00:02:29,760
¿Qué pasa Arti? 

34
00:02:29,760 --> 00:02:33,360
Me estoy poniendo un poco caliente en la sartén. 

35
00:02:33,360 --> 00:02:39,260
Es bueno para lo que tienes Arti, mata el virus, te lo garantizo. 

36
00:02:39,260 --> 00:02:43,560
Sí, bueno, estoy empezando a ponerme caliente. 

37
00:02:43,560 --> 00:02:45,280
No te preocupes … 

