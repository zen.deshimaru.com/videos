1
00:00:13,360 --> 00:00:15,750
A tale told by Master Deshimaru

2
00:00:17,320 --> 00:00:19,320
which is called "Walking in the mountains".

3
00:00:19,980 --> 00:00:21,980
So already just the title,

4
00:00:22,560 --> 00:00:24,080
is wonderful,

5
00:00:24,609 --> 00:00:30,360
To be able to walk in the mountains. A master was walking in the mountain and on his return

6
00:00:31,179 --> 00:00:33,179
one of his disciples asked him:

7
00:00:33,700 --> 00:00:35,700
"Master, where did you go for a walk?"

8
00:00:35,980 --> 00:00:40,420
"In the mountain", replied the master and the disciple insisted: "But what way

9
00:00:40,900 --> 00:00:43,380
did you take, what did you see? "

10
00:00:45,260 --> 00:00:52,940
The master replied: "I followed the scent of the flowers and wandered through the young spring shoots."

11
00:00:55,480 --> 00:00:57,480
Master Deshimaru continues

12
00:00:58,540 --> 00:01:02,180
"We must let ourselves be guided by the Dharma of the Buddha

13
00:01:02,380 --> 00:01:03,820
trust

14
00:01:04,140 --> 00:01:08,040
in herbs and flowers growing aimlessly,

15
00:01:08,600 --> 00:01:10,220
without selfishness,

16
00:01:10,440 --> 00:01:15,640
naturally, unconsciously. This answer came from the source of wisdom

17
00:01:16,900 --> 00:01:20,560
true wisdom must be created beyond knowledge

18
00:01:21,100 --> 00:01:26,240
and memory. "This is a pretty powerful teaching.

19
00:01:26,700 --> 00:01:29,160
In a kusen,

20
00:01:29,340 --> 00:01:37,040
the teaching given in the dojo, Master Kosen teaches: "The philosophy of Shikantaza,

21
00:01:37,900 --> 00:01:40,580
the philosophy of ’only zazen’

22
00:01:40,800 --> 00:01:47,380
must guide our life, this is the true essence of Buddhism, the most perfect method

23
00:01:48,249 --> 00:01:50,699
zazen itself is satori, awakening.

24
00:01:51,520 --> 00:01:53,520
We are wrong

25
00:01:54,159 --> 00:01:56,368
if we think there is a zazen before

26
00:01:57,100 --> 00:02:02,460
and that with the accumulation of zazen, zazen, fifteen days, something happens, the awakening.

27
00:02:05,300 --> 00:02:11,520
Immediately anyone who practices zazen triggers the awakening process.

28
00:02:13,020 --> 00:02:15,800
So, this process doesn’t fit our categories

29
00:02:16,900 --> 00:02:18,900
sometimes people say

30
00:02:18,959 --> 00:02:20,959
’zazen was very very hard,

31
00:02:21,420 --> 00:02:24,949
it was a bad zazen ’or sometimes

32
00:02:25,860 --> 00:02:27,860
’it’s very comfortable’

33
00:02:28,409 --> 00:02:35,929
and the process that triggers zazen is beyond our categories or beyond our personal consciousness. "

34
00:02:36,660 --> 00:02:42,169
So Dogen’s method, the same of other Masters, it is great, it is unique.

35
00:02:43,799 --> 00:02:48,769
His lessons that seem very simple that we receive,

36
00:02:50,190 --> 00:02:52,190
that we receive during zazen

37
00:02:53,400 --> 00:02:55,400
and we are going to

38
00:02:58,530 --> 00:03:02,750
integrate, that is to say that we are going to appropriate them,

39
00:03:04,019 --> 00:03:09,319
gently, deeply, unconsciously, these are precious lessons.
