1
00:00:00,200 --> 00:00:03,460
Parfois, vous ne pouvez trouver cela qu’en Zen, 

2
00:00:03,780 --> 00:00:06,680
quand on est ensemble pendant un moment, 

3
00:00:06,920 --> 00:00:08,960
que les gens manquent de compassion. 

4
00:00:09,180 --> 00:00:11,600
Je pense que parfois, vous devez changer votre façon de voir les choses. 

5
00:00:11,980 --> 00:00:16,400
Au lieu de penser: "Hé, c’est l’autre qui m’a volé!" 

6
00:00:17,000 --> 00:00:20,680
pensant, "qu’est-ce que je suis censé faire avec ça?" 

7
00:00:21,180 --> 00:00:23,160
Et essayez de le retourner. 

8
00:00:24,360 --> 00:00:27,900
C’est très sincère. 

9
00:00:28,300 --> 00:00:30,340
Parce que les gens sont eux-mêmes. 

10
00:00:30,600 --> 00:00:32,560
Ils essaient de ne pas jouer de rôle: 

11
00:00:32,760 --> 00:00:36,900
"Je vais bien, je vais bien, je vais bien …" 

12
00:00:38,240 --> 00:00:41,760
Il y a certainement des moments où nous sommes fâchés. 

13
00:00:42,040 --> 00:00:45,280
Et où nos réactions se démarquent. 

14
00:00:45,600 --> 00:00:50,780
Il est important pour moi que nous ne nous y installions pas. 

15
00:00:51,040 --> 00:00:52,880
Ne pas se vautrer dans l’idée: 

16
00:00:53,200 --> 00:00:55,700
"Dans une sesshin, il n’y a pas de gens sympas." 

17
00:00:55,960 --> 00:00:59,320
Quand j’arrive à Sesshin, 

18
00:00:59,880 --> 00:01:04,060
mon idée est de tout transformer en positif. 

19
00:01:04,600 --> 00:01:07,340
Je ne sais pas si je peux le faire tout le temps! 

20
00:01:08,360 --> 00:01:10,960
Pour moi, toutes les difficultés qui se posent 

21
00:01:11,180 --> 00:01:16,960
ses invitations à continuer dans son état d’esprit. 

22
00:01:17,360 --> 00:01:24,680
Je dirais même que si nous sommes injustement critiqués, 

23
00:01:24,980 --> 00:01:27,600
nous pouvons en faire quelque chose de positif. 

24
00:01:27,900 --> 00:01:33,580
Par exemple, cela vous oblige à vous prendre moins au sérieux. 

25
00:01:37,620 --> 00:01:40,340
Ce que j’aime dans cette sangha, 

26
00:01:40,640 --> 00:01:42,860
c’est que nous essayons de nous écouter un peu plus. 

27
00:01:43,140 --> 00:01:45,720
Parce qu’il faut du temps pour s’écouter davantage, 

28
00:01:46,040 --> 00:01:50,460
pour prendre en compte d’autres points de vue. 

29
00:01:50,760 --> 00:01:53,580
Nous savons que les choses vont mal, 

30
00:01:53,980 --> 00:01:58,120
mais nous nous efforçons toujours d’améliorer. 

31
00:01:59,400 --> 00:02:07,120
Si vous avez aimé cette vidéo, n’hésitez pas à l’aimer et à vous abonner à la chaîne! 

