1
00:00:00,000 --> 00:00:04,799
Come sincronizzi i parenti che camminano e respirano lentamente? 

2
00:00:04,799 --> 00:00:08,550
Hai detto che avremmo fatto un passo avanti ad ogni espirazione. 

3
00:00:08,550 --> 00:00:15,690
Ma la maggior parte delle persone respira due volte in un solo passaggio. 

4
00:00:15,690 --> 00:00:19,470
Certo che voglio fare due. 

5
00:00:19,470 --> 00:00:24,180
Le persone raddoppiano il respiro perché non fanno una pausa tra i respiri. 

6
00:00:24,180 --> 00:00:28,650
Quando si cammina lentamente verso il bambino, la piccola pausa è fondamentale. 

7
00:00:28,650 --> 00:00:33,059
Se ti riposi un po ’sopra l’espirazione … 

8
00:00:33,059 --> 00:00:38,210
Sei nel ritmo. 

9
00:00:38,210 --> 00:00:44,309
Dogen e il suo maestro Nyojo dicono …. 

10
00:00:44,309 --> 00:00:50,219
… che il Buddha stesso ha detto che non raddoppi il respiro. 

11
00:00:50,219 --> 00:00:55,050
Per fare questo, alla fine dell’espirazione, è necessario fare una piccola pausa … 

12
00:00:55,050 --> 00:01:03,239
…. in cui faremo mezzo passo avanti … 

13
00:01:03,239 --> 00:01:09,299
… dopo una breve pausa prima di inalare e siamo a buon ritmo. 

14
00:01:09,299 --> 00:01:14,610
Ci stufiamo … molto meno. 

15
00:01:14,610 --> 00:01:20,580
Durante la sesshin, ho ampliato i hins della famiglia. 

16
00:01:20,580 --> 00:01:26,159
L’avevo già vissuto in un altro esercizio con un altro maestro … 

17
00:01:26,159 --> 00:01:32,670
Dovevi camminare a lungo con gli occhi bendati. 

18
00:01:32,670 --> 00:01:38,280
Dobbiamo camminare senza vedere nulla. 

19
00:01:38,280 --> 00:01:42,270
È stancante perché devi anche mettere le mani in posizione …. 

20
00:01:42,270 --> 00:01:45,540
… meno comodo rispetto al kind-hin. 

21
00:01:45,540 --> 00:01:51,180
Se fai il hin della famiglia per molto tempo si stancherà …. 

22
00:01:51,180 --> 00:01:54,659
… un altro aspetto di noi stessi. 

23
00:01:54,659 --> 00:01:59,360
È un aspetto attivo. Ho imparato in medicina cinese …. 

24
00:01:59,360 --> 00:02:06,920
…. che per curare l’attività eccessiva, hai bisogno di riposo. 

25
00:02:13,340 --> 00:02:18,170
Se fai il hin della famiglia per molto tempo, sei esausto. 

26
00:02:18,170 --> 00:02:23,230
Hai mal di schiena, dolore alla spalla, dolore alle braccia … 

27
00:02:23,230 --> 00:02:29,330
Se cammini così per un’ora, le persone sono esauste. 

28
00:02:29,330 --> 00:02:33,290
Vorranno solo una cosa: essere nello zazen. 

29
00:02:33,290 --> 00:02:41,470
Che sollievo quando sei nello zazen: è complementare. 

30
00:02:41,470 --> 00:02:46,549
Ti dici: che relax, che resa! 

31
00:02:46,549 --> 00:02:51,650
Vuoi davvero essere a Zazen. 

32
00:02:51,650 --> 00:02:57,920
Dopo un’ora a Zazen, avrai la sensazione … 

33
00:02:57,920 --> 00:03:02,720
….. formicolio spiacevole … 

34
00:03:02,720 --> 00:03:07,040
Avrai voglia di muoverti e desiderare che finisca per il dolore. 

35
00:03:07,040 --> 00:03:11,630
Zazen e camminata lenta sono completamente complementari. 

36
00:03:11,630 --> 00:03:14,870
Ma poiché pratichiamo il bambino hin per un breve periodo, non ce ne rendiamo conto. 

37
00:03:14,870 --> 00:03:19,430
Ora farò il bambino hins più a lungo durante sesshinsµ. 

38
00:03:19,430 --> 00:03:25,130
In modo che alla gente piaccia sedersi e lasciar andare certe cose nello zazen. 

39
00:03:25,130 --> 00:03:29,350
La tua domanda è molto buona e non nuova. 

40
00:03:29,350 --> 00:03:38,920
Il Maestro Dogen ha posto il problema 1.400 anni fa. 

