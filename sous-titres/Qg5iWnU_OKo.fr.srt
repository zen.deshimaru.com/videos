1
00:00:00,100 --> 00:00:01,460
Je m’appelle Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
Mon moine zen s’appelle Ryurin. 

3
00:00:03,880 --> 00:00:05,580
Je suis également calligraphe. 

4
00:00:05,900 --> 00:00:09,560
J’enseigne la calligraphie chinoise, que j’ai découverte en zen. 

5
00:00:09,840 --> 00:00:13,860
Je l’ai appris d’un moine zen. 

6
00:00:14,320 --> 00:00:18,260
J’aime partager la calligraphie avec tout le monde. 

7
00:00:18,860 --> 00:00:22,540
Je fais beaucoup de travail dans les écoles. 

8
00:00:22,900 --> 00:00:27,360
Je fais de la calligraphie avec des enfants dans des ateliers. 

9
00:00:27,700 --> 00:00:31,020
Aussi pour adultes et personnes handicapées. 

10
00:00:31,380 --> 00:00:33,300
Ils l’aiment beaucoup. 

11
00:00:33,780 --> 00:00:39,220
Mes plus jeunes élèves ont 6 ans et mon plus âgé a 90 ans. 

12
00:00:39,760 --> 00:00:42,800
J’enseigne également le chinois. 

13
00:00:43,180 --> 00:00:44,840
C’est mon cœur de métier. 

14
00:00:45,140 --> 00:00:48,020
Cette calligraphie est simplement "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
Il s’agit d’une calligraphie réalisée dans le style semi-cursif actuel. 

16
00:00:54,580 --> 00:01:06,760
Le pinceau est court, il va des points aux lignes, pour former un caractère chinois ou japonais. 

17
00:01:08,420 --> 00:01:11,320
C’est le "Reiki" (靈氣), l’énergie spirituelle. 

18
00:01:11,620 --> 00:01:13,880
C’est une technique de guérison. 

19
00:01:14,220 --> 00:01:20,260
Nous continuons cette tradition car elle vient d’un maître qui a beaucoup pratiqué la calligraphie. 

20
00:01:20,500 --> 00:01:23,920
Il pratiquait également le sumi-e, (墨 絵), il était peintre. 

21
00:01:24,280 --> 00:01:27,580
J’apprécie vraiment les tableaux de Maître Deshimaru, c’était un artiste. 

22
00:01:27,840 --> 00:01:31,020
Il a très bien peint et fait de la calligraphie. 

23
00:01:31,440 --> 00:01:37,860
Sa calligraphie est très forte, dense et énergique. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) est le nom d’un sutra qui est chanté. 

25
00:01:43,700 --> 00:01:52,480
Je l’ai tracé en bouclé: un peu comme des petits spaghettis. 

26
00:01:52,780 --> 00:01:56,440
Les personnages sont très liés et simplifiés. 

27
00:01:56,780 --> 00:02:00,800
Nous aimons aussi cela dans la pratique zen. 

28
00:02:01,240 --> 00:02:08,040
Les pratiquants zen ont vraiment aimé cette écriture, assez libre et expressive. 

29
00:02:09,260 --> 00:02:10,740
C’est un art énergique. 

30
00:02:11,020 --> 00:02:14,840
Elle se pratique avec tout le corps, en lien avec la respiration. 

31
00:02:15,240 --> 00:02:21,900
En relation avec sa disponibilité de l’esprit, sa conscience. 

32
00:02:22,360 --> 00:02:24,720
C’est donc un art énergétique. 

33
00:02:25,000 --> 00:02:28,120
Nous allons nous calmer, réorienter quelque chose en nous. 

34
00:02:28,360 --> 00:02:33,240
C’est une métamorphose complète, une alchimie d’équilibre. 

35
00:02:33,560 --> 00:02:40,040
Horizontalité, verticalité, centrage, couplage lors du dessin des signes. 

36
00:02:40,540 --> 00:02:43,120
Disponibilité à la main. 

37
00:02:43,460 --> 00:02:48,080
Dans la méditation zen, la présence dans les mains est très importante. 

38
00:02:48,420 --> 00:02:55,600
Que ce soit à l’arrêt ou en cours d’exécution. 

39
00:02:55,940 --> 00:02:57,760
Le geste est différent en calligraphie. 

40
00:02:58,180 --> 00:03:00,120
Mais il y a une présence par les mains. 

41
00:03:00,340 --> 00:03:02,320
C’est la relation de la main au cerveau. 

42
00:03:02,600 --> 00:03:04,140
La calligraphie a une vie. 

43
00:03:04,440 --> 00:03:05,440
Même si vous ne le savez pas! 

44
00:03:05,680 --> 00:03:07,980
Certaines calligraphies vous toucheront, d’autres non. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) est chinois et plus taoïste. 

46
00:03:11,740 --> 00:03:15,820
Cela signifie «nourrir le principe vital». 

47
00:03:16,540 --> 00:03:23,440
Si vous avez aimé cette vidéo, n’hésitez pas à l’aimer et à vous abonner à la chaîne! 

