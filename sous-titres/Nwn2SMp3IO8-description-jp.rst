禅と詭弁：基本的な姿勢と脳への影響

古泉師範が2013年のFederation of Professional Schools of Sophrology (www.sophro.fr)の大会に招待されました。

禅の四つの基本姿勢と脳への影響を解説。彼は動的緩和の源に戻る。仏教の瞑想、特に禅、坐禅の修行において、姿勢、呼吸、心の状態の三本柱の重要性を主張している。
