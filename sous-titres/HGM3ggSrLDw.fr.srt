1
00:00:03,300 --> 00:00:07,060
Quelle heure est-il? 

2
00:00:07,240 --> 00:00:11,780
Le temps n’est que maintenant. 

3
00:00:12,080 --> 00:00:15,080
Le Zen est "ici et maintenant". 

4
00:00:15,340 --> 00:00:16,820
C’est important. 

5
00:00:17,220 --> 00:00:25,160
Mais comprendre le passé est un truc extraordinaire pour comprendre le présent. 

6
00:00:25,580 --> 00:00:31,640
Et comprendre l’avenir est également très important. 

7
00:00:32,460 --> 00:00:34,780
Comment traversez-vous le temps? 

8
00:00:35,360 --> 00:00:38,400
Le temps est une question de fréquence. 

9
00:00:38,700 --> 00:00:43,640
Plus la fréquence est élevée, plus le temps est court, c’est tout. 

10
00:00:44,100 --> 00:00:49,760
La conscience évolue également en fonction des fréquences. 

11
00:00:59,700 --> 00:01:03,440
Cher n’est qu’une impression matérielle. 

12
00:01:03,820 --> 00:01:06,000
C’est un délice. 

13
00:01:06,400 --> 00:01:09,100
Et zazen? 

14
00:01:09,400 --> 00:01:16,700
Pour un amoureux de zazen, le temps zazen est très précieux. 

15
00:01:17,120 --> 00:01:22,560
Nous arrivons au dojo, et nous n’avons qu’une heure et demie, jusqu’à deux heures. 

16
00:01:23,020 --> 00:01:27,720
Pour pouvoir vivre dans cette dimension. 

17
00:01:28,000 --> 00:01:31,520
Et remets les choses à leur place. 

18
00:01:31,780 --> 00:01:35,440
Vous devez enregistrer votre temps zazen. 

19
00:01:35,540 --> 00:01:38,760
Il faut du temps pour aller au fond des choses. 

20
00:01:39,000 --> 00:01:40,760
Et pour être en bonne posture. 

21
00:01:41,060 --> 00:01:43,960
Le temps est-il lié au karma? 

22
00:01:44,720 --> 00:01:46,920
Le passé n’existe que maintenant. 

23
00:01:47,280 --> 00:01:57,240
Chaque "maintenant" correspond à une fréquence spécifique au passé avec votre "maintenant". 

24
00:01:57,620 --> 00:02:00,900
Votre passé change constamment. 

25
00:02:01,000 --> 00:02:10,700
Mais quand vous allez dans votre passé, vous oubliez votre présent. 

26
00:02:11,040 --> 00:02:13,560
Et votre présent devient le passé. 

27
00:02:13,900 --> 00:02:17,720
Et vous entrez dans quelque chose qui devient de plus en plus compliqué. 

28
00:02:17,940 --> 00:02:19,340
Dans le karma. 

29
00:02:19,820 --> 00:02:24,160
"C’est la faute de mon karma …" 

30
00:02:25,240 --> 00:02:27,960
"Mes parents, mes affaires …" 

31
00:02:28,280 --> 00:02:30,440
"Ma vie est compliquée …" 

32
00:02:30,700 --> 00:02:32,380
"Mes ancêtres étaient alcooliques …" 

33
00:02:32,660 --> 00:02:35,420
Etc. Et vous vous perdez là-dedans. 

34
00:02:35,780 --> 00:02:40,060
Vous vous identifiez à ce personnage. 

35
00:02:40,340 --> 00:02:41,720
Vous devenez plus faible. 

36
00:02:42,060 --> 00:02:44,140
Tu n’es plus dieu. 

37
00:02:44,420 --> 00:02:45,720
Vous êtes confus. 

38
00:02:46,080 --> 00:02:47,760
Vous êtes perdu dans la matérialisation. 

39
00:02:48,060 --> 00:02:53,300
Avec notre état actuel, il y a toujours des causes dans le passé. 

40
00:02:53,680 --> 00:02:58,540
Si nous changeons la situation actuelle, le passé changera également. 

41
00:02:59,160 --> 00:03:01,940
Et l’avenir? 

42
00:03:02,340 --> 00:03:05,300
L’avenir est une autre histoire … 

