1
00:00:23,160 --> 00:00:26,820
Arti, are you there?

2
00:00:26,820 --> 00:00:28,560
I’m right here.

3
00:00:28,560 --> 00:00:32,940
It’s good to see you, how have you been?

4
00:00:32,940 --> 00:00:34,240
I’m fine…

5
00:00:34,240 --> 00:00:38,620
You know you look funny, what’s going on with you lately?

6
00:00:38,740 --> 00:00:41,620
I’m tired, I have
party with the neighbors.

7
00:00:41,620 --> 00:00:46,184
What, but it’s lockdown, are you kidding? You don’t have
the right to go out.

8
00:00:46,184 --> 00:00:50,520
I’m not allowed to, but I didn’t go out, it’s on the balcony.

9
00:00:50,520 --> 00:00:56,280
You make me feel better. Careful, right behind, back up.

10
00:00:56,440 --> 00:00:58,360
Oh, man, you’re hurting.

11
00:00:58,360 --> 00:01:01,980
Every night’s an aperitif.

12
00:01:01,980 --> 00:01:09,000
And yesterday I fell in the neighbor’s house, I found myself in my underpants,
one floor down, in the geranium bin.

13
00:01:09,000 --> 00:01:12,180
You should be careful Arti anyway.

14
00:01:12,180 --> 00:01:14,140
How are you doing?

15
00:01:14,140 --> 00:01:21,220
It’s all right, you know I’m a potatoe, it’s not for nothing that they call me patata.

16
00:01:21,400 --> 00:01:24,840
Oh yeah, right, what do you do in your day?

17
00:01:24,840 --> 00:01:29,957
I’ll start with a little meditation.

18
00:01:29,960 --> 00:01:31,340
Tell me…

19
00:01:31,340 --> 00:01:40,920
There’s nothing to tell, I sit like this, look… I concentrate on breathing.

20
00:01:40,980 --> 00:01:44,140
Maybe I should do this…

21
00:01:44,140 --> 00:01:49,340
Then I feel better, with everyone else here… We’re full.

22
00:01:49,340 --> 00:01:53,420
Are we full? But you’re not allowed, it’s…
lockdown!

23
00:01:53,420 --> 00:01:55,980
No, but you don’t get it, we do it with the zoom.

24
00:01:55,980 --> 00:01:57,460
With the zoom?

25
00:01:57,460 --> 00:02:03,200
Yeah, you connect and then you’re with a lot of people…

26
00:02:03,200 --> 00:02:05,820
I’m interested in your thing.

27
00:02:05,820 --> 00:02:10,452
Tomorrow Saturday, we practice with Master Kosen.

28
00:02:10,460 --> 00:02:13,320
I’m interested. Who’s he?

29
00:02:13,320 --> 00:02:17,340
Well, look, he’s not
badly. Tomorrow at noon.

30
00:02:17,500 --> 00:02:20,740
All right, I’ll be there. You’ll explain it to me?

31
00:02:20,740 --> 00:02:25,580
Yes, come on, I’ll explain now.

32
00:02:25,740 --> 00:02:28,040
Bummer! Bummer?

33
00:02:28,040 --> 00:02:29,760
What’s up Arti?

34
00:02:29,760 --> 00:02:33,360
I’m getting a little hot in the pan.

35
00:02:33,360 --> 00:02:39,260
It’s good for what you have Arti, it kills the virus, I guarantee it.

36
00:02:39,260 --> 00:02:43,560
Yeah, well, I’m starting to get hot.

37
00:02:43,560 --> 00:02:45,280
Don’t worry…

