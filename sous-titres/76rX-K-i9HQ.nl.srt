1
00:00:00,280 --> 00:00:03,640
Dagelijks leven

2
00:00:03,840 --> 00:00:06,460
Op een dag…

3
00:00:07,200 --> 00:00:12,320
Overdag moeten we regelmatig stoppen met wat we doen.

4
00:00:13,040 --> 00:00:15,320
Stop zijn werk.

5
00:00:15,780 --> 00:00:18,780
En kom maar op.

6
00:00:19,020 --> 00:00:22,220
In zijn werk, in het dagelijks leven, vind je de geest.

7
00:00:22,560 --> 00:00:24,720
Het duurt maar twee keer ademen.

8
00:00:25,060 --> 00:00:26,340
Je werkt aan je bureau…

9
00:00:26,680 --> 00:00:28,820
Maak het gewoon recht.

10
00:00:29,340 --> 00:00:32,760
En ademen.

11
00:00:41,120 --> 00:00:49,260
En om onmiddellijk terug te keren naar zijn wezen.

12
00:00:50,960 --> 00:00:54,140
Ik kende een gemeenschap genaamd Lanza del Vasto.

13
00:00:55,000 --> 00:00:57,240
Ze werken heel hard.

14
00:00:57,580 --> 00:01:00,400
Ze bewerken het land.

15
00:01:00,660 --> 00:01:02,660
Ze bouwen hun huis.

16
00:01:02,880 --> 00:01:04,540
Ze doen veel handenarbeid.

17
00:01:04,740 --> 00:01:06,040
Ze werken de hele dag.

18
00:01:06,300 --> 00:01:07,500
Ze staan om 5 uur ’s morgens op.

19
00:01:07,720 --> 00:01:08,820
Ze zeggen hun gebeden.

20
00:01:09,080 --> 00:01:10,840
Ze zijn aan het ontbijten.

21
00:01:11,160 --> 00:01:12,420
Ze gaan aan het werk.

22
00:01:12,780 --> 00:01:15,820
Elk uur gaat er een belletje rinkelen.

23
00:01:16,180 --> 00:01:18,060
Iedereen moet stoppen met werken.

24
00:01:18,720 --> 00:01:25,520
En heroriëntatie voor een minuut.

25
00:01:26,060 --> 00:01:28,960
Ieder op zijn eigen manier.

26
00:01:34,080 --> 00:01:36,040
En dan gaan ze terug.

27
00:01:36,560 --> 00:01:39,160
Dus er is niet alleen zazen.

28
00:01:39,540 --> 00:01:42,100
Er zijn veel manieren om terug te komen op jezelf.

29
00:01:42,640 --> 00:01:45,220
Het dagelijks leven van de Zen-monniken…

30
00:01:45,540 --> 00:01:48,820
We leven ons leven voor zazen.

31
00:01:49,080 --> 00:01:52,150
Om dit bewustzijn, deze kennis te verzamelen.

32
00:01:52,440 --> 00:01:53,580
Die energie.

33
00:01:54,880 --> 00:01:58,780
Op het moment van de zazen-ervaring.

34
00:01:58,900 --> 00:02:03,120
Het is geen kwestie van het opgeven van je leven voor Zazen.

35
00:02:03,380 --> 00:02:05,400
In een sektarische geest.

36
00:02:05,740 --> 00:02:07,680
Het is geen ideologie.

37
00:02:07,900 --> 00:02:10,380
Het is geen ”-isme”.

38
00:02:10,840 --> 00:02:14,780
Het is: ”Ik leef mijn leven op zo’n manier dat ik deze ervaring kan hebben.”

39
00:02:15,060 --> 00:02:16,560
”Zo diep mogelijk.”

40
00:02:16,900 --> 00:02:18,980
Dit is een ervaring van zazen meditatie.

41
00:02:19,220 --> 00:02:25,060
Als je in zazen zit, heb je de mogelijkheid om je hele leven te veranderen.

42
00:02:25,400 --> 00:02:27,900
Dat is waar zazen om draait!

43
00:02:28,340 --> 00:02:31,980
Het is geen kleine manier om je goed te voelen.

44
00:02:32,500 --> 00:02:35,040
Zen in het dagelijks leven…

45
00:02:35,640 --> 00:02:39,200
Zelfs in de winst en het werk…

46
00:02:39,480 --> 00:02:41,500
”Mushotoku”, (zonder doel of geest van winst)…

47
00:02:41,760 --> 00:02:43,140
dat is het geheim.

48
00:02:43,340 --> 00:02:46,320
Met andere woorden: blijf gefocust op je essentie.

49
00:02:46,540 --> 00:02:48,880
Dan kunnen we alles krijgen.

50
00:02:49,080 --> 00:02:50,360
We hebben alles.

51
00:02:50,880 --> 00:02:55,280
Wat is het verschil tussen het leven van een meester en dat van een gewoon wezen?

52
00:02:55,660 --> 00:02:59,520
Er bestaat niet zoiets als gewoon.

53
00:02:59,840 --> 00:03:05,780
Er zijn alleen maar buitengewone wezens.

54
00:03:06,660 --> 00:03:14,900
Het slechte ding is dat mensen niet
weet niet dat ze buitengewoon zijn.

55
00:03:16,420 --> 00:03:22,260
Ik hou niet van die uitdrukking ”gewoon zijn”!

