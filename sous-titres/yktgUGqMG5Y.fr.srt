1
00:00:23,160 --> 00:00:26,820
Arti, tu es là? 

2
00:00:26,820 --> 00:00:28,560
Je suis ici. 

3
00:00:28,560 --> 00:00:32,940
C’est bon de te voir, comment vas-tu? 

4
00:00:32,940 --> 00:00:34,240
Je vais bien… 

5
00:00:34,240 --> 00:00:38,620
Vous savez que vous avez l’air drôle, que se passe-t-il récemment? 

6
00:00:38,740 --> 00:00:41,620
Je suis fatigué, j’ai fait la fête avec les voisins. 

7
00:00:41,620 --> 00:00:46,184
Quoi, mais c’est le verrouillage, vous plaisantez? Tu n’as pas le droit de sortir. 

8
00:00:46,184 --> 00:00:50,520
Je n’y suis pas autorisé, mais je ne suis pas sorti, c’est sur le balcon. 

9
00:00:50,520 --> 00:00:56,280
Tu me fais me sentir mieux. Attention, juste derrière, recule. 

10
00:00:56,440 --> 00:00:58,360
Oh, mec, tu as mal. 

11
00:00:58,360 --> 00:01:01,980
Un apéritif tous les soirs. 

12
00:01:01,980 --> 00:01:09,000
Et hier, je suis tombé dans la maison du voisin, je me suis retrouvé en slip, un étage plus bas, dans le bac à géranium. 

13
00:01:09,000 --> 00:01:12,180
Vous devez quand même faire attention Arti. 

14
00:01:12,180 --> 00:01:14,140
Comment allez vous? 

15
00:01:14,140 --> 00:01:21,220
C’est bon, tu sais que je suis une pomme de terre, ce n’est pas pour rien qu’ils m’appellent patata. 

16
00:01:21,400 --> 00:01:24,840
Oh oui, qu’est-ce que tu fais dans ta journée? 

17
00:01:24,840 --> 00:01:29,957
Je vais commencer par une petite méditation. 

18
00:01:29,960 --> 00:01:31,340
Dîtes-moi… 

19
00:01:31,340 --> 00:01:40,920
Il n’y a rien à dire, je m’assois comme ça, regarde … Je me concentre sur la respiration. 

20
00:01:40,980 --> 00:01:44,140
Peut-être que je devrais faire ça … 

21
00:01:44,140 --> 00:01:49,340
Ensuite, je me sens mieux, avec tout le monde ici … Nous sommes rassasiés. 

22
00:01:49,340 --> 00:01:53,420
Sommes-nous rassasiés? Mais vous n’êtes pas autorisé, c’est … le verrouillage! 

23
00:01:53,420 --> 00:01:55,980
Non, mais vous ne comprenez pas, nous le faisons avec le zoom. 

24
00:01:55,980 --> 00:01:57,460
Avec le zoom? 

25
00:01:57,460 --> 00:02:03,200
Oui, vous vous connectez et vous êtes avec beaucoup de gens … 

26
00:02:03,200 --> 00:02:05,820
Je suis intéressé par ton truc. 

27
00:02:05,820 --> 00:02:10,452
Demain samedi, nous nous entraînons avec Maître Kosen. 

28
00:02:10,460 --> 00:02:13,320
Je suis intéressé. Qui est-il? 

29
00:02:13,320 --> 00:02:17,340
Eh bien, regardez, il n’est pas mal. Demain à midi. 

30
00:02:17,500 --> 00:02:20,740
D’accord, je serai là. Tu vas m’expliquer? 

31
00:02:20,740 --> 00:02:25,580
Oui, allez, je vais vous expliquer maintenant. 

32
00:02:25,740 --> 00:02:28,040
Bummer! Bummer? 

33
00:02:28,040 --> 00:02:29,760
Quoi de neuf Arti? 

34
00:02:29,760 --> 00:02:33,360
Je chauffe un peu dans la poêle. 

35
00:02:33,360 --> 00:02:39,260
C’est bon pour ce que tu as Arti, ça tue le virus, je le garantis. 

36
00:02:39,260 --> 00:02:43,560
Ouais, eh bien, je commence à avoir chaud. 

37
00:02:43,560 --> 00:02:45,280
Ne t’inquiète pas … 

