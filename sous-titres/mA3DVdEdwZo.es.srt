1
00:00:02,555 --> 00:00:04,935
Hablamos en el kusen…

2
00:00:05,365 --> 00:00:08,125
aquí en Europa…

3
00:00:14,475 --> 00:00:17,625
…la volición.

4
00:00:18,965 --> 00:00:21,545
No es la voluntad.

5
00:00:22,980 --> 00:00:27,800
La volición es algo que se desencadena

6
00:00:42,860 --> 00:00:44,860
…inconscientemente.

7
00:00:56,040 --> 00:00:58,740
Después, le expliqué…

8
00:01:00,840 --> 00:01:03,660
…aquí en el Oeste…

9
00:01:11,580 --> 00:01:16,280
En primer lugar, ¡buenos días a todos!

10
00:01:21,100 --> 00:01:23,915
Compartamos este zazen ahora mismo.

11
00:01:27,795 --> 00:01:29,465
con gran placer

12
00:01:41,220 --> 00:01:46,880
Nos las arreglamos para organizar y practicar un campamento de verano.

13
00:01:49,540 --> 00:01:53,520
a pesar de los complicados acontecimientos

14
00:01:56,080 --> 00:01:59,870
Estamos limitados en número, porque no has venido

15
00:02:05,140 --> 00:02:08,080
debido a la autorización de seguridad

16
00:02:13,600 --> 00:02:19,900
Estamos obligados a estar más espaciados en el Dojo

17
00:02:20,140 --> 00:02:25,820
Hicimos todo en serio, seguimos las reglas…

18
00:02:48,715 --> 00:02:52,160
Ella es una de las…

19
00:02:52,160 --> 00:02:54,860
características especiales del zen

20
00:02:58,940 --> 00:03:01,860
que seguir la regla

21
00:03:02,380 --> 00:03:07,780
"Sí, pero yo no fui educado así… ¡Yo no soy así, yo!"

22
00:03:12,340 --> 00:03:16,840
En el zen… seguimos la regla…

23
00:03:20,685 --> 00:03:23,535
La regla donde estamos.

24
00:03:33,200 --> 00:03:36,580
Así que expliqué que la volición

25
00:03:39,480 --> 00:03:42,380
es Yin o Yang

26
00:03:47,760 --> 00:03:52,500
Cuando digo que relajes las piernas, es la volición de Yin…

27
00:03:53,840 --> 00:03:56,220
Nos dejamos llevar

28
00:04:06,125 --> 00:04:08,765
Cuando te sueltas, provocas una resonancia…

29
00:04:19,460 --> 00:04:23,340
la volición del Yang…

30
00:04:23,640 --> 00:04:26,820
es empujar la tierra con las rodillas.

31
00:04:40,600 --> 00:04:43,640
Conté la historia

32
00:04:46,480 --> 00:04:48,380
y pensamientos

33
00:04:50,300 --> 00:04:52,760
En el método Yang de Baso

34
00:04:54,760 --> 00:04:57,500
Y el método Yin de Sekito

35
00:04:59,440 --> 00:05:03,380
Compartiré este kusen contigo.

36
00:05:22,140 --> 00:05:25,300
Es una historia muy famosa

37
00:05:27,420 --> 00:05:31,780
pero visto desde un ángulo diferente al habitual.

38
00:05:33,960 --> 00:05:39,300
Estamos hablando de Baso que ya había recibido la transmisión de Dharma…

39
00:05:40,620 --> 00:05:42,680
De su maestro

40
00:05:49,820 --> 00:05:54,400
Continuó siguiendo las enseñanzas de su maestro y las sesshins…

41
00:06:03,270 --> 00:06:08,320
Tenía una manera divertida

42
00:06:11,580 --> 00:06:18,180
Iba a hacer zazen solo en el Dojo aunque no fuera el momento…

43
00:06:19,740 --> 00:06:23,730
Siempre quiso hacer zazen

44
00:06:28,320 --> 00:06:34,360
Un día, su amo vino a verlo mientras estaba sentado…

45
00:06:36,360 --> 00:06:38,400
y le pide

46
00:06:40,480 --> 00:06:42,920
Gran monje,

47
00:06:43,425 --> 00:06:46,645
-Baso era muy grande.

48
00:06:48,400 --> 00:06:53,700
¿Qué buscas cuando te sientas en zazen?

49
00:06:57,060 --> 00:06:58,640
En otras palabras:

50
00:06:58,860 --> 00:07:03,920
¿qué estás buscando? ¿Tienes un propósito?

51
00:07:04,300 --> 00:07:08,560
¿Tienes un propósito más allá de…

52
00:07:10,580 --> 00:07:15,000
que es más importante que sólo sentarse?

53
00:07:20,040 --> 00:07:22,540
Esta historia es muy conocida

54
00:07:24,325 --> 00:07:28,875
Es el aspecto Yang de la volición…

55
00:07:31,705 --> 00:07:34,625
Después, inventé los diálogos

56
00:07:41,045 --> 00:07:44,175
"- ¿Tienes un propósito?"

57
00:07:44,565 --> 00:07:47,525
"-Sí, tengo un propósito", dice Baso.

58
00:07:50,435 --> 00:07:54,045
Quiero convertirme en Buda. Quiero convertirme en Buda.

59
00:07:56,345 --> 00:07:59,445
Así que…

60
00:07:59,535 --> 00:08:02,035
Estoy apuntando

61
00:08:02,225 --> 00:08:05,195
el centro del objetivo

62
00:08:08,525 --> 00:08:11,655
y lo alcanzo cada vez.

63
00:08:21,920 --> 00:08:25,000
Es el aspecto Yang del zazen.

64
00:08:25,240 --> 00:08:26,720
que mencioné ayer

65
00:08:33,620 --> 00:08:35,080
Le dije a

66
00:08:36,800 --> 00:08:42,360
cómo el Maestro Deshimaru nos enseñó cuando éramos jóvenes…

67
00:08:49,120 --> 00:08:53,785
Causaba mucha tensión en el dojo con su kyosaku.

68
00:08:55,045 --> 00:08:57,875
Es el método Yang

69
00:08:59,385 --> 00:09:02,385
Tan pronto como alguien se mudó

70
00:09:03,105 --> 00:09:05,395
o se durmió

71
00:09:06,385 --> 00:09:09,345
lo golpeó

72
00:09:09,545 --> 00:09:12,765
Estábamos en el campamento de verano

73
00:09:12,875 --> 00:09:15,405
Hacía mucho calor

74
00:09:15,915 --> 00:09:17,455
estábamos empapados.


75
00:09:19,765 --> 00:09:22,925
Había un charco en el tatami después de zazen.

76
00:09:32,260 --> 00:09:37,340
"Me siento para convertirme en Buda, y eso es todo. Puse toda mi energía en ello.

77
00:09:40,910 --> 00:09:43,550
Es el método de Deshimaru.

78
00:09:45,080 --> 00:09:48,490
Practicar con todas las fuerzas para el placer

79
00:09:51,435 --> 00:09:54,475
Eso es lo que Baso estaba haciendo

80
00:10:00,980 --> 00:10:03,980
Así que el método Yin,

81
00:10:05,960 --> 00:10:10,240
Lo que quería decir es que Baso no tenía ninguna duda

82
00:10:13,420 --> 00:10:19,180
No le importaba si tenía un propósito o no, si era Mushotoku o no.

83
00:10:24,460 --> 00:10:28,020
Fue por volición propia. Tenía el deseo, tenía la voluntad.

84
00:10:37,535 --> 00:10:40,345
El Sensei dijo:

85
00:10:41,185 --> 00:10:44,415
es mejor hacer zazen menos tiempo

86
00:10:48,985 --> 00:10:51,825
pero haciendo un fuerte zazen, como Baso…

87
00:10:58,875 --> 00:11:02,865
En lugar de hacer zazen durante mucho tiempo, todo el día,

88
00:11:06,525 --> 00:11:10,005
y para dormir o pensar

89
00:11:18,425 --> 00:11:19,425
20 minutos.

90
00:11:19,565 --> 00:11:22,445
con todo su poderío

91
00:11:24,055 --> 00:11:26,785
es muy eficaz

92
00:11:31,355 --> 00:11:33,725
Cuando amas el zazen,

93
00:11:35,995 --> 00:11:38,475
por diversión…

94
00:11:42,375 --> 00:11:45,085
No somos proselitistas.

95
00:11:56,515 --> 00:12:01,235
Sólo estamos compartiendo, simplemente por diversión.

96
00:12:41,325 --> 00:12:44,085
Volvamos al ejemplo del Yin,

97
00:12:44,205 --> 00:12:45,865
aflojar…

98
00:12:47,820 --> 00:12:51,610
cuando suelto las piernas, libera toda la pelvis…

99
00:12:54,180 --> 00:12:57,340
Me siento como si no pesara nada.

100
00:13:10,580 --> 00:13:13,040
El ejemplo dado

101
00:13:14,165 --> 00:13:18,815
es llegar al tan esperado momento en que vamos a tomar una siesta

102
00:13:26,135 --> 00:13:29,145
Donde dejas todo

103
00:13:30,775 --> 00:13:33,425
para dormir

104
00:13:35,935 --> 00:13:39,845
El ejemplo de la siesta es obviamente una metáfora…

105
00:13:42,045 --> 00:13:45,005
que se refiere a zazen

106
00:13:59,545 --> 00:14:02,295
Sin embargo, 7 siglos antes de Cristo.

107
00:14:09,240 --> 00:14:12,240
un científico llamado Pitágoras

108
00:14:14,595 --> 00:14:16,545
dijo:

109
00:14:18,960 --> 00:14:21,300
prestar atención a la noche

110
00:14:27,500 --> 00:14:33,120
ya que no puedes controlar la noche

111
00:14:34,600 --> 00:14:39,980
Ten cuidado al dormirte.

112
00:14:43,600 --> 00:14:48,240
No te quedes dormido sin comprobar el último momento antes de dormirte.

113
00:15:02,695 --> 00:15:05,735
Zazen se compone de estos dos aspectos

114
00:15:05,885 --> 00:15:08,785
yin y yang

115
00:15:11,440 --> 00:15:16,260
que fueron expresados a su vez por los grandes maestros Baso y Sekito

116
00:15:21,370 --> 00:15:36,380
Uno puede llamarse "Matar al Buda" y el otro "Liberar al Buda".

117
00:15:41,680 --> 00:15:49,020
El gran maestro Sekito fue su contemporáneo. Eran monjes que vivían al mismo tiempo.

118
00:15:52,300 --> 00:15:56,300
Baso y Sekito eran de la misma época.

119
00:16:00,980 --> 00:16:04,560
A menudo los discípulos de uno iban al otro.

120
00:16:10,570 --> 00:16:14,000
En ese momento, Sekito no tenía ningún discípulo todavía…

121
00:16:17,455 --> 00:16:20,865
Vivía en el bosque… en las montañas…

122
00:16:22,445 --> 00:16:24,945
Se había construido a sí mismo

123
00:16:25,385 --> 00:16:28,405
una pequeña cabaña

124
00:16:34,335 --> 00:16:36,915
con madera y paja

125
00:16:44,315 --> 00:16:48,895
…en la misma montaña donde el Maestro Nangaku había enseñado…

126
00:16:54,435 --> 00:16:57,825
Sekito dice:

127
00:16:58,545 --> 00:17:01,475
"Me siento protegido en esta cabaña".

128
00:17:02,865 --> 00:17:07,355
Lo construí yo mismo. Es mi única riqueza.

129
00:17:10,965 --> 00:17:13,395
Pero cuando termine mi comida,

130
00:17:16,045 --> 00:17:19,015
Preparo con calma el momento de la siesta…

131
00:17:23,655 --> 00:17:26,825
y digo que todos los días

132
00:17:29,295 --> 00:17:34,525
y otra vez todos los días

133
00:17:38,875 --> 00:17:41,665
La experiencia de la comida y la siesta,

134
00:17:44,135 --> 00:17:46,955
es un poder de los patriarcas

135
00:17:50,035 --> 00:17:53,055
el que no ha terminado su comida todavía,

136
00:17:56,345 --> 00:18:03,075
no se ha completado todavía.

137
00:18:08,215 --> 00:18:10,015
Aún no está satisfecho.
