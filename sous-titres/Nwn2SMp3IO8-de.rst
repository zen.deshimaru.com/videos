1
00:00:00,280 --> 00:00:02,560
Ich weiß nicht viel über Sophrologie.

2
00:00:02,800 --> 00:00:12,140
Ich weiß, dass es eine Anwendung bestimmter Praktiken des Wohlbefindens, der persönlichen Entwicklung ist,

3
00:00:12,540 --> 00:00:17,160
und es gibt eine Sophrologie, die "orientalistisch" genannt wird,

4
00:00:17,440 --> 00:00:24,080
die sehr stark von yogischen, buddhistischen oder indischen Praktiken inspiriert ist.

5
00:00:24,540 --> 00:00:27,940
Zen ist sehr einfach.

6
00:00:28,380 --> 00:00:38,400
Es basiert auf grundlegenden menschlichen Körperhaltungen.

7
00:00:39,820 --> 00:00:48,480
Ich werde Ihnen die erste Grundhaltung des Zen zeigen.

8
00:01:01,500 --> 00:01:06,120
Früher waren die Menschen Affen.

9
00:01:06,460 --> 00:01:08,580
So sind sie gelaufen.

10
00:01:09,200 --> 00:01:20,360
Nach und nach saßen sie aufrecht, wie Zen-Meister.

11
00:01:22,900 --> 00:01:24,900
Dann richteten sie sich auf.

12
00:01:25,180 --> 00:01:28,600
So sind sie gelaufen.

13
00:01:29,680 --> 00:01:33,760
Und das menschliche Bewusstsein hat sich entwickelt

14
00:01:35,180 --> 00:01:39,980
zusammen mit der Körperhaltung.

15
00:01:40,340 --> 00:01:46,000
Die Haltung des Körpers ist ein grundlegender Gedanke.

16
00:01:46,340 --> 00:01:52,200
Zum Beispiel Usain Bolt, der schnellste Läufer der Welt,

17
00:01:52,540 --> 00:01:55,160
machte diese Geste.

18
00:01:55,440 --> 00:02:02,300
Jeder versteht, was es bedeutet, und doch gibt es keine Worte.

19
00:02:02,640 --> 00:02:05,440
Es ist nur eine Körperhaltung.

20
00:02:05,900 --> 00:02:11,120
Die Art, wie wir sitzen und gehen,

21
00:02:11,520 --> 00:02:13,400
was Sie mit Ihren Händen tun,

22
00:02:13,700 --> 00:02:16,500
es ist alles grundlegend wichtig.

23
00:02:18,160 --> 00:02:21,440
Im Zen gibt es die Buddha-Haltung.

24
00:02:22,980 --> 00:02:31,120
Sie existierte schon vor dem Buddha, sie wird in der Bhagavad-Gita erwähnt.

25
00:02:32,400 --> 00:02:39,880
Sie sitzen auf einem Graskissen und beugen die Beine.

26
00:02:41,560 --> 00:02:47,140
In der Wildnis kommunizierte der prähistorische Mensch

27
00:02:47,440 --> 00:02:53,220
mit den Ohren, mit der Nase, mit den Augen, um weit zu sehen.

28
00:02:54,920 --> 00:02:59,000
Also wollten sie sich aufrichten.

29
00:02:59,400 --> 00:03:04,220
Und während sie sich aufrichteten, schärfte sich ihr Bewusstsein, erwachte ihr Bewusstsein.

30
00:03:06,040 --> 00:03:08,720
Dies ist die erste Körperhaltung.

31
00:03:08,980 --> 00:03:11,580
Normalerweise müssen wir einen vollen Lotus machen.

32
00:03:12,060 --> 00:03:14,260
So kreuzt man die Beine.

33
00:03:14,700 --> 00:03:18,460
Man nennt sie die "Brezel".

34
00:03:24,960 --> 00:03:30,980
Es ist sehr wichtig, diese Haltung einzunehmen.

35
00:03:31,380 --> 00:03:37,380
Mein Vorredner sprach über Dynamik und Entspannung.

36
00:03:37,780 --> 00:03:42,340
Wir haben ein sympathisches und parasympathisches Nervensystem.

37
00:03:42,600 --> 00:03:45,800
Wir haben die äußeren Muskeln und die tiefen Muskeln.

38
00:03:46,320 --> 00:03:51,400
Wir haben eine flache und eine tiefe Atmung.

39
00:03:51,820 --> 00:03:54,620
Wir haben ein oberflächliches, kortikales Bewusstsein,

40
00:03:54,920 --> 00:03:59,760
und tiefes Bewusstsein, das Reptilien- oder Mittelhirn.

41
00:04:00,340 --> 00:04:04,160
In dieser Haltung aufrecht zu stehen,

42
00:04:04,440 --> 00:04:06,800
muss man die Erde mit den Knien schieben.

43
00:04:10,240 --> 00:04:15,640
Man muss sich bemühen, dynamisch zu sein und eine gute Körperhaltung einzunehmen.

44
00:04:16,000 --> 00:04:22,180
Mein Meister [Deshimaru] bestand auf der Schönheit und Stärke der Körperhaltung.

45
00:04:22,460 --> 00:04:31,280
Aber beim Lotus, je mehr man sich entspannt, desto mehr wachsen die Füße auf den Oberschenkeln,

46
00:04:31,520 --> 00:04:35,100
und je mehr die Knie auf dem Boden wachsen.

47
00:04:35,620 --> 00:04:40,280
Je mehr Sie sich entspannen, desto dynamischer sind Sie, desto aufrechter und stärker sind Sie.

48
00:04:40,560 --> 00:04:46,560
Das hat nichts mit Entspannung auf einem Stuhl oder in einem Bett zu tun.

49
00:04:46,940 --> 00:04:57,280
Es ist eine Art, den Körper sich selbst zu überlassen,

50
00:05:04,380 --> 00:05:09,700
während ich hellwach war.

51
00:05:10,120 --> 00:05:16,240
Dies ist die erste Haltung, die Sitzhaltung des Buddha.

52
00:05:16,680 --> 00:05:20,280
Hände sind auch sehr wichtig.

53
00:05:20,640 --> 00:05:23,600
Sie tun immer etwas mit Ihren Händen:

54
00:05:23,860 --> 00:05:26,560
arbeiten, sich selbst schützen...

55
00:05:26,960 --> 00:05:28,740
Hände sind sehr ausdrucksstark

56
00:05:29,040 --> 00:05:31,040
und sind mit dem Gehirn verbunden.

57
00:05:31,500 --> 00:05:41,040
Diese Mudra mit den Händen zu nehmen, verändert das Bewusstsein.

58
00:05:41,680 --> 00:05:47,680
Die Konzentration liegt in unseren Händen, unserem Körper, Nervensystem und Gehirn.

59
00:05:48,100 --> 00:05:53,360
Eine weitere wichtige Sache ist das Denken, das Bewusstsein.

60
00:05:53,820 --> 00:05:57,680
Wenn Sie wie Rodins Denker sind,

61
00:05:58,000 --> 00:06:01,100
Sie haben überhaupt nicht dasselbe Gewissen.

62
00:06:09,960 --> 00:06:12,240
dass, wenn Ihre Körperhaltung vollkommen gerade ist,

63
00:06:12,580 --> 00:06:14,640
vor allem mit gestrecktem Hals,

64
00:06:15,700 --> 00:06:18,400
mit aufrechtem Kopf, offen zum Himmel,

65
00:06:18,760 --> 00:06:22,020
Nase und Nabel ausgerichtet

66
00:06:25,060 --> 00:06:27,220
und Sie atmen mit Ihrem ganzen Wesen.

67
00:06:29,020 --> 00:06:31,860
Das Denken ist dann nicht mehr vom Körper abgekoppelt.

68
00:06:32,240 --> 00:06:36,100
Es gibt keine Dissoziation mehr zwischen unserem Bewusstsein

69
00:06:36,300 --> 00:06:39,080
und jede Zelle in unserem Körper.

70
00:06:39,500 --> 00:06:45,520
Im Zen nennen wir dies "Geist-Körper", mit einem Wort.

71
00:06:46,360 --> 00:06:50,540
Dies ist die Grundhaltung: Zazen.

72
00:06:56,380 --> 00:07:00,200
Die zweite Haltung ist die Gehhaltung.

73
00:07:00,680 --> 00:07:06,340
In Zazen geben wir alles auf und bewegen uns überhaupt nicht.

74
00:07:06,560 --> 00:07:08,280
Bewusstsein ist also etwas Besonderes.

75
00:07:08,720 --> 00:07:13,800
Aber es gibt noch eine andere Meditation, die beim Gehen durchgeführt wird.

76
00:07:16,180 --> 00:07:18,160
Wir müssen uns bewegen.

77
00:07:18,540 --> 00:07:20,540
Wir tun etwas, wir machen weiter.

78
00:07:21,180 --> 00:07:24,180
Man geht nicht vorwärts, um etwas zu fangen.

79
00:07:24,580 --> 00:07:28,380
Sie regulieren Ihre Atmung mit Ihrem Gehen.

80
00:07:29,020 --> 00:07:30,740
Der Choreograf Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
der ein Schüler meines Meisters Deshimaru war,

82
00:07:34,060 --> 00:07:38,040
ließ seine Tänzer das jeden Tag üben.

83
00:07:39,060 --> 00:07:45,340
Die zweite Körperhaltung ist der "kin-hin"-Gang.

84
00:07:45,880 --> 00:07:51,040
Beachten Sie, was Sie mit Ihren Händen tun.

85
00:07:51,680 --> 00:07:57,840
Ein Boxer, der sich fotografieren lässt, macht das mit den Händen.

86
00:07:58,240 --> 00:08:05,220
Wenn Sie Ihre Hände so anlegen, drückt das Gehirn automatisch eine starke Aggressivität aus.

87
00:08:05,820 --> 00:08:10,360
Sie können Ihre Hände in Ihre Taschen stecken.

88
00:08:10,840 --> 00:08:13,140
"Wo habe ich meine Brieftasche hingelegt? »

89
00:08:13,640 --> 00:08:16,360
"Jemand hat mein Handy gestohlen! »

90
00:08:16,700 --> 00:08:19,580
Wir können uns die Finger in die Nase stecken.

91
00:08:21,660 --> 00:08:25,820
Alles, was hier gebaut wurde, ist von Hand gebaut worden.

92
00:08:26,260 --> 00:08:28,820
Es ist außergewöhnlich, die Hand.

93
00:08:29,160 --> 00:08:31,860
Wir sind die einzigen Tiere mit Händen,

94
00:08:32,260 --> 00:08:35,340
sie hängen mit unserer Entwicklung zusammen.

95
00:08:37,700 --> 00:08:40,660
Auch die Hände meditieren.

96
00:08:40,940 --> 00:08:43,580
Sie stecken den Daumen in die Faust.

97
00:08:43,820 --> 00:08:49,420
Legen Sie die Daumenwurzel unter das Brustbein, legen Sie die andere Hand darauf.

98
00:08:49,760 --> 00:08:55,060
Diese Haltungsdetails gibt es seit dem Buddha.

99
00:08:55,380 --> 00:09:02,520
Alte Texte erklären, dass sie von Indien nach China überliefert wurden.

100
00:09:02,960 --> 00:09:08,920
Es handelt sich um eine mündliche Übertragung von Person zu Person.

101
00:09:09,480 --> 00:09:12,500
Im Buddhismus gibt es viele Aspekte.

102
00:09:12,780 --> 00:09:17,320
Aber der meditative und posturale Aspekt ist sehr wichtig.

103
00:09:17,820 --> 00:09:28,540
Es gab zwei große Schüler des Buddha, Śāriputra und Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Ihr spiritueller Meister war sich nicht sicher:

105
00:09:37,220 --> 00:09:43,680
"Ich bin mir nicht sicher, ob ich den Trick habe, um aus Leben und Tod herauszukommen. »

106
00:09:43,940 --> 00:09:52,860
"Wenn Sie einen Meister finden, der dies lehrt, lassen Sie es mich wissen, und ich werde mit Ihnen gehen. »

107
00:09:53,620 --> 00:10:04,320
Eines Tages sehen sie einen Mönch, so wie mich, sehen Sie, wie er die Straße entlang geht.

108
00:10:04,780 --> 00:10:10,000
Sie sind erstaunt über die Schönheit dieses Mannes.

109
00:10:10,420 --> 00:10:16,200
Dann hält dieser Mönch in einem Hain an, um zu pinkeln.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, die zu dieser Zeit als eine der größten Intelligenzen Indiens gilt, erzählt ihm:

111
00:10:34,180 --> 00:10:43,940
"Wir sind erstaunt über Ihre Anwesenheit, wer ist Ihr Herr? »

112
00:10:46,560 --> 00:10:49,540
- "Mein Meister ist Buddha Shakyamuni."

113
00:10:49,860 --> 00:10:53,520
- "Erklären Sie uns Ihre Doktrin! »

114
00:10:53,780 --> 00:10:58,220
- "Ich kann es Ihnen nicht erklären, ich bin zu neu in diesem Bereich. »

115
00:10:59,100 --> 00:11:04,440
Und sie folgten diesem Jungen nur wegen der Art, wie er geht.

116
00:11:04,800 --> 00:11:06,980
Die Art, wie er etwas aus ihr herausbekommt.


116
00:11:04,800 --> 00:11:06,980
Die Art, wie er etwas aus ihr herausbekommt.

117
00:11:07,440 --> 00:11:11,680
Die zweite Körperhaltung ist also das Gehen.

118
00:11:12,020 --> 00:11:15,280
Wenn Menschen Zazen machen, ist es sehr stark.

119
00:11:15,500 --> 00:11:21,580
Selbst Kampfsportler sind beeindruckt von der Kraft, die dies ausstrahlt.

120
00:11:21,940 --> 00:11:25,860
Durch die Kraft des Atems.

121
00:11:26,240 --> 00:11:35,520
Aber wie findet man diese Energie, diese Fülle im Alltag?

122
00:11:35,840 --> 00:11:39,220
Diese Ruhe, diese Konzentration.

123
00:11:39,600 --> 00:11:42,960
Das ist ein schwieriger Punkt.

124
00:11:43,280 --> 00:11:47,140
Denn die Haltung von Zazen ist äußerst präzise.

125
00:11:47,600 --> 00:11:55,100
Zu Beginn konzentrieren wir uns auf den technischen Aspekt des Kinnhin-Gangs.

126
00:11:55,540 --> 00:11:58,800
Aber wenn wir erst alle Details kennen,

127
00:11:59,300 --> 00:12:02,160
zu jeder Zeit in unserem täglichen Leben

128
00:12:02,440 --> 00:12:08,400
(auch wenn ich es immer vergesse), können wir es üben.

129
00:12:10,600 --> 00:12:16,740
Sie können sich wieder auf Ihr spirituelles Wesen konzentrieren.

130
00:12:17,740 --> 00:12:22,760
Auf Ihr Sein, das bringt einen Scheiß.

131
00:12:23,320 --> 00:12:31,340
Ich habe Ihnen von den Händen erzählt.

132
00:12:31,840 --> 00:12:39,980
Die religiöse Grundhaltung ist diese.

133
00:12:40,560 --> 00:12:44,240
Wenn man die Hände so zusammen hat,

134
00:12:45,080 --> 00:12:48,260
Ich mache nur das hier,

135
00:12:48,580 --> 00:12:51,280
es hat eine Wirkung auf das Gehirn,

136
00:12:51,620 --> 00:12:55,340
Es bildet eine Brücke zwischen den beiden Gehirnhälften.

137
00:12:55,740 --> 00:12:59,960
Diese Haltung wird in allen Religionen verwendet.

138
00:13:00,280 --> 00:13:02,180
Es ist ganz einfach.

139
00:13:02,560 --> 00:13:05,640
Sehen Sie die Bedeutung der Hände!

140
00:13:06,060 --> 00:13:13,280
Die dritte Haltung...

141
00:13:13,860 --> 00:13:16,520
(Ich dachte, es wären vier...)

142
00:13:16,880 --> 00:13:20,580
Oh, ja, da ist die Bauchlage, aber darüber werden wir nicht reden.

143
00:13:21,000 --> 00:13:25,520
Die Liegehaltung, wie man schläft.

144
00:13:26,700 --> 00:13:31,980
Ich habe Ihnen erklärt, wie prähistorische Menschen auf allen Vieren herumkriechen mussten,

145
00:13:32,260 --> 00:13:34,440
Nach und nach richteten sie sich auf,

146
00:13:34,700 --> 00:13:38,760
und dass unsere Entwicklung von unserer Körperhaltung abhängt,

147
00:13:39,060 --> 00:13:42,580
der Spülung der Wirbelsäule, die zum Gehirn hinaufgeht,

148
00:13:42,940 --> 00:13:46,400
und das Gehirn, das ein außergewöhnliches Instrument ist,

149
00:13:46,700 --> 00:13:51,440
von dem nur 10-15 % seines Gesamtnutzens bekannt sind.

150
00:13:51,760 --> 00:13:57,740
Wir haben ein großes Ding zwischen den Ohren, das wir nicht voll ausnutzen.

151
00:14:08,640 --> 00:14:12,380
Jeder hat sein eigenes Gehirn!

152
00:14:19,020 --> 00:14:22,140
Jeder hat sein eigenes Gehirn!

153
00:14:22,400 --> 00:14:25,200
Wie eine IP-Adresse.

154
00:14:30,220 --> 00:14:33,220
Jeder hat sein eigenes Gehirn!

155
00:14:33,940 --> 00:14:35,900
Und wir sind allein!

156
00:14:36,700 --> 00:14:41,940
Es ist sehr wichtig für das, was ich Ihnen jetzt sagen werde.

157
00:14:44,760 --> 00:14:50,040
Unsere Füße (wir gehen zurück zu unseren Füßen...).

158
00:14:51,720 --> 00:14:56,420
Sie wissen, dass wir auf einer kleinen Erde leben.

159
00:14:57,040 --> 00:15:01,420
Haben Sie den Film Gravity gesehen?

160
00:15:02,120 --> 00:15:07,060
Es ist einer der Filme, die das menschliche Bewusstsein wirklich wecken.

161
00:15:07,560 --> 00:15:13,380
In Hollywood leisten sie manchmal manipulative Arbeit.

162
00:15:13,680 --> 00:15:17,220
Aber auch manchmal von Information und geistiger Entwicklung.

163
00:15:17,760 --> 00:15:19,660
Dieser Film ist einer von ihnen.

164
00:15:20,040 --> 00:15:26,920
Und Sie können sich selbst davon überzeugen, wie zerbrechlich unsere Erde ist.

165
00:15:29,200 --> 00:15:33,400
Normalerweise schwärmen wir, wenn wir in den Himmel schauen: "Er ist unendlich blau! ».

166
00:15:33,760 --> 00:15:38,940
Nein. Nicht für immer. 30 Kilometer Atmosphäre.

167
00:15:39,360 --> 00:15:44,660
Das Ende der Atmosphäre liegt zwischen 30 und 1.000 Kilometern.

168
00:15:52,260 --> 00:15:56,700
Selbst tausend Meilen sind nichts.

169
00:15:58,800 --> 00:16:04,780
Wenn ich Sie 20 Meilen weit weg bringe, würden Sie keine Minute überleben.

170
00:16:05,380 --> 00:16:15,940
Auch abgesehen von den Steuern sind unsere Lebensbedingungen prekär.

171
00:16:17,420 --> 00:16:20,440
Die Erde ist ein kleines, zerbrechliches Lebewesen.

172
00:16:21,120 --> 00:16:30,660
Unsere Biosphäre ist winzig und zerbrechlich, sie muss erhalten werden.

173
00:16:30,940 --> 00:16:35,100
Wir sollten immer darauf bedacht sein, diese Sache zu bewahren.

174
00:16:36,860 --> 00:16:39,940
Weil wir von der Erde geboren sind.

175
00:16:40,220 --> 00:16:45,700
Dieses Wunder des Lebens, das sich auf der Erde ereignet hat, ist vergänglich.

176
00:16:46,240 --> 00:16:47,840
Wir sind von der Erde geboren.

177
00:16:48,220 --> 00:16:53,320
Und wir alle haben unsere Füße, die auf dasselbe Zentrum zeigen, den Mittelpunkt der Erde.

178
00:16:53,900 --> 00:16:57,760
Unsere Füße sind also in der Gemeinschaft.

179
00:17:01,860 --> 00:17:08,680
Aber unser Kopf ist einzigartig, niemand zeigt in die gleiche Richtung.

180
00:17:18,400 --> 00:17:23,500
Daher die dritte Haltung, die ich Ihnen zeigen werde.

181
00:17:23,800 --> 00:17:28,140
Sie ist in vielen Religionen üblich, sie wird "Niederwerfung" genannt.

182
00:17:28,380 --> 00:17:31,240
So machen wir das.

183
00:17:40,640 --> 00:17:42,780
Ah, das fühlt sich gut an!

184
00:17:43,060 --> 00:17:49,940
Wenn wir diese Praxis anwenden, ist es wie bei der Liebe:

185
00:17:50,260 --> 00:17:58,460
Manchmal haben wir 10 Jahre, 20 Jahre, 6 Monate lang keinen Sex...

186
00:17:58,780 --> 00:18:07,460
und wenn wir wieder Liebe machen, sagen wir: "Ah, das fühlt sich gut an, ich hatte vergessen, wie gut es war! ».

187
00:18:08,220 --> 00:18:12,200
Wenn Sie diese Niederwerfung machen, ist es dasselbe.

188
00:18:13,460 --> 00:18:15,740
Das ist kein Scherz.

189
00:18:16,240 --> 00:18:20,260
Wir reformieren unsere Gehirne.

190
00:18:21,060 --> 00:18:26,280
Wenn Sie Ihr Gehirn mit dem Mittelpunkt der Erde in eine Linie bringen.

191
00:18:26,620 --> 00:18:29,080
Es geht nicht nur um Bescheidenheit,

192
00:18:29,360 --> 00:18:32,720
oder sich vor Statuen oder was auch immer zu verbeugen.

193
00:18:34,600 --> 00:18:38,880
Es ist eine magische, fundamentale, schamanische Sache, vielleicht...

194
00:18:39,320 --> 00:18:41,860
Von dem Moment an, wo Sie sich niederwerfen...

195
00:18:42,180 --> 00:18:43,340
Mein Herr pflegte zu sagen:

196
00:18:43,660 --> 00:18:47,440
"Wenn sich die Staatsoberhäupter einmal am Tag niederwerfen würden,"

197
00:18:47,800 --> 00:18:50,200
"Es würde die Welt verändern. »

198
00:18:50,640 --> 00:18:54,560
Es ist kein Kauderwelsch oder Mystizismus.

199
00:18:55,280 --> 00:18:58,780
Sie muss wissenschaftlich messbar sein.

200
00:19:00,780 --> 00:19:05,280
Es formatiert das Gehirn neu, um aufzusetzen.

201
00:19:05,740 --> 00:19:09,280
Er ist ständig in seiner individuellen Linie.

202
00:19:09,620 --> 00:19:17,300
Und dort verbinden Sie sich mit Ihrer Wurzel, mit Ihren Brüdern, mit dem Mittelpunkt der Erde.

203
00:19:19,940 --> 00:19:25,380
Manchmal tue ich es und spüre etwas Starkes in meinem Kopf.

204
00:19:25,660 --> 00:19:27,540
Es fühlt sich so gut an.

205
00:19:27,820 --> 00:19:39,420
Gleichzeitig relativiert sie das Böse, das wir unbeabsichtigt um uns herum tun.

206
00:19:40,560 --> 00:19:42,960
Das ist gute Medizin.

207
00:19:43,180 --> 00:19:47,040
All diese Haltungen sind grundlegende Medikamente.

208
00:19:47,760 --> 00:19:53,260
Ich hatte einen etwas intellektuellen Text vorbereitet.

209
00:19:53,520 --> 00:19:58,140
Aber ich scheine nicht von einem Thema zum anderen zu kommen.

210
00:19:58,440 --> 00:20:02,300
Was wollte ich sagen?

211
00:20:06,860 --> 00:20:12,040
Ich gehe zurück zum Körper und zur Vorgeschichte.

212
00:20:12,780 --> 00:20:28,200
Es ist erwiesen, dass Neandertaler eine religiöse Praxis hatten.

213
00:20:30,680 --> 00:20:36,340
Wir haben einige Gräber gefunden.

214
00:20:36,860 --> 00:20:40,440
Sie brachten ihren Toten Opfergaben.

215
00:20:40,760 --> 00:20:43,680
Sie pflegten eine religiöse Zeremonie für ihre Toten durchzuführen.

216
00:20:48,420 --> 00:20:52,180
Interessant ist aber nicht nur das!

217
00:20:52,720 --> 00:20:56,260
Damals, als sie diese religiösen Zeremonien abhielten -

218
00:20:56,740 --> 00:21:01,700
- religiös in dem Sinne, dass sie mit dem Unsichtbaren kommunizierten -

219
00:21:02,540 --> 00:21:06,940
- mit dem ewigen Leben, mit den Toten -

220
00:21:08,800 --> 00:21:11,020
- verbanden sie sich mit dem Unsichtbaren.

221
00:21:11,420 --> 00:21:18,140
Zu dieser Zeit gab es jedoch noch keine artikulierte Sprache.

222
00:21:18,560 --> 00:21:24,380
Die Religion existierte also schon vor der artikulierten Sprache.

223
00:21:24,840 --> 00:21:30,140
(Ich bin bereit, zur artikulierten Sprache überzugehen...)

224
00:21:31,480 --> 00:21:36,500
(Es ist sehr interessant, ich habe vier Tage gebraucht, um es zu schreiben...)

225
00:21:43,820 --> 00:21:45,660
Mein Herr pflegte zu sagen:

226
00:21:45,920 --> 00:21:48,880
"Zazen ist die Religion vor der Religion. »

227
00:21:49,380 --> 00:21:54,380
Wir sagten: "Ja, du willst immer der Erste sein..."

228
00:21:55,860 --> 00:21:59,700
Aber das ist die Religion vor der Religion, wie wir sie kennen.

229
00:21:59,960 --> 00:22:02,720
Wo auf Bücher verwiesen wird.

230
00:22:03,080 --> 00:22:06,280
Bücher kommen nach der Religion, nicht vorher.

231
00:22:06,720 --> 00:22:11,940
Vorher konnte der Mann nicht einmal sprechen.

232
00:22:12,360 --> 00:22:15,060
Er war also bereits in dieser Haltung.


233
00:22:15,480 --> 00:22:19,620
Dies sind prähistorische Haltungen.

234
00:22:21,460 --> 00:22:23,740
(Wie viel Zeit habe ich noch?)

235
00:22:23,960 --> 00:22:28,320
(Ich habe nur noch 3 Minuten? Also gut!)

236
00:22:31,020 --> 00:22:34,460
Also, was die Sophrologie betrifft...

237
00:22:36,500 --> 00:22:39,280
Ich wollte mit Ihnen über das traditionelle Zazen sprechen.

238
00:22:39,740 --> 00:22:43,660
Ich bin als traditioneller Mönch gekleidet.

239
00:22:47,440 --> 00:22:51,700
Traditionelles Zazen wird nicht allein praktiziert.

240
00:22:52,100 --> 00:22:54,500
Es handelt sich nicht um eine Wellness-Technik.

241
00:22:58,260 --> 00:23:05,060
Ich lese Ihnen vor, was der Buddha gesagt hat, ich finde das so toll!

242
00:23:08,520 --> 00:23:12,780
sagte Shakyamuni Buddha zu seiner Zuhörerschaft:

243
00:23:13,120 --> 00:23:18,540
"Das Studium von Sutras und Lehren" - Texte -

244
00:23:18,900 --> 00:23:28,260
Die "Befolgung von Geboten" - in den Religionen ist dies wichtig, nicht zu töten, nicht zu stehlen usw. - ist ein sehr wichtiger Teil der "Befolgung von Geboten". -

245
00:23:29,300 --> 00:23:34,960
"und sogar die Praxis von Zazen, der Sitzmeditation".

246
00:23:35,900 --> 00:23:43,060
"sind nicht in der Lage, menschliche Wünsche, Ängste und Befürchtungen auszulöschen. »

247
00:23:43,340 --> 00:23:45,080
Das hat Shakyamuni Buddha gesagt.

248
00:23:45,340 --> 00:23:51,660
Ich bin also ein Fanatiker und er ist eine Nervensäge...

249
00:23:52,080 --> 00:23:56,800
Zazen, sogar der Buddha sagt, es sei nutzlos!

250
00:23:57,600 --> 00:24:01,680
Also, was werden wir dagegen tun?

251
00:24:14,440 --> 00:24:17,480
Natürlich gibt es eine Suite...

252
00:24:23,360 --> 00:24:29,360
Mein Meister [Deshimaru], als er seinen Meister [Kodo Sawaki] traf,

253
00:24:29,820 --> 00:24:34,280
Letzterer sagte auf einer Konferenz: "Zazen ist nutzlos, es bringt keinen Verdienst! ».

254
00:24:34,680 --> 00:24:38,800
Mein Meister dachte: "Aber es ist wirklich interessant, wenn es nutzlos ist! »

255
00:24:39,140 --> 00:24:41,940
"Wir tun immer Dinge, die für etwas gut sind! »

256
00:24:42,280 --> 00:24:43,760
"Etwas, das nutzlos ist? »

257
00:24:44,120 --> 00:24:47,400
"Ich will gehen, ich will es tun! »

