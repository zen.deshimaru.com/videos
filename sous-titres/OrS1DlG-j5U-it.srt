1
00:00:00,000 --> 00:00:08,750
Faccio zazen con un kesa di Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
Seta.

3
00:00:15,330 --> 00:00:19,215
Sono molto felice.

4
00:00:20,235 --> 00:00:22,830
Barbara mi piace molto.

5
00:00:24,580 --> 00:00:27,210
È una grande santa.

6
00:00:33,770 --> 00:00:38,190
Fortunatamente, i miei discepoli in generale sono migliori di me.

7
00:00:42,910 --> 00:00:45,830
Soprattutto Barbara.

8
00:00:48,850 --> 00:00:51,270
E anche Ariadna.

9
00:00:55,710 --> 00:00:58,515
I tre tesori.

10
00:01:01,015 --> 00:01:03,817
Buddha, dharma, sangha.

11
00:01:06,267 --> 00:01:10,440
sono inseparabili.

12
00:01:24,820 --> 00:01:28,470
Ti ho preparato come kusen...

13
00:01:35,340 --> 00:01:41,560
...la continuazione di quello che ho fatto al campo estivo.

14
00:01:45,170 --> 00:01:50,930
Sulle quattro fondamenta dei poteri magici.

15
00:01:56,750 --> 00:02:01,770
L’ho fatto al campo estivo...

16
00:02:02,130 --> 00:02:10,530
Dicono che le quattro fondamenta sono come i quattro zoccoli di un cavallo.

17
00:02:35,940 --> 00:02:40,120
La prima è la forza di volontà, la volizione.

18
00:02:44,460 --> 00:02:50,300
La volizione è qualcosa di più forte di te.

19
00:02:52,290 --> 00:02:55,400
E questo ti spinge ad agire.

20
00:02:58,920 --> 00:03:02,400
È come una questione di sopravvivenza.

21
00:03:06,430 --> 00:03:08,350
È molto forte.

22
00:03:09,410 --> 00:03:12,160
Ne ho parlato al campo estivo.

23
00:03:15,530 --> 00:03:18,550
Il secondo è lo spirito.

24
00:03:20,745 --> 00:03:23,015
Il secondo zoccolo.

25
00:03:27,730 --> 00:03:33,390
Il terzo è il progresso, andare avanti.

26
00:03:38,440 --> 00:03:40,810
Il quarto è il pensiero.

27
00:03:43,810 --> 00:03:46,855
Questo è quello che penso.

28
00:03:48,135 --> 00:03:50,020
Il secondo.

29
00:03:54,970 --> 00:03:57,515
Ne ho parlato al campo estivo,

30
00:03:57,625 --> 00:04:00,860
ma non avevo finito.

31
00:04:03,270 --> 00:04:09,595
Ti racconterò tre storie zen...

32
00:04:11,995 --> 00:04:16,060
...che fanno capire meglio la natura della mente.

33
00:04:19,990 --> 00:04:25,860
Maestro Eka, un discepolo di Bodhidharma...

34
00:04:32,370 --> 00:04:35,760
...in piedi nella neve...

35
00:04:39,330 --> 00:04:41,145
...ghiacciato...

36
00:04:44,635 --> 00:04:47,564
... si rivolse al suo padrone!

37
00:04:53,470 --> 00:04:57,240
"Maestro, la mia mente non è pacifica. »

38
00:05:08,460 --> 00:05:12,130
"Vi prego, dategli la pace! »

39
00:05:18,940 --> 00:05:25,380
Eka aveva ucciso molti uomini perché era stato nell’esercito.

40
00:05:33,130 --> 00:05:37,370
Si sentiva estremamente in colpa per tutto questo.

41
00:05:40,780 --> 00:05:44,100
Non riusciva a liberarsi del senso di colpa.

42
00:05:49,410 --> 00:05:54,485
È nella neve e chiede a Bodhidharma: "Accettami! »

43
00:06:00,245 --> 00:06:02,050
"Come discepolo! »

44
00:06:06,000 --> 00:06:09,410
Bodhidharma non risponde.

45
00:06:17,770 --> 00:06:19,615
Insiste.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma non si muove.

47
00:06:30,960 --> 00:06:35,420
Alla fine, si taglia il braccio con la spada.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma dice: "Beh..."

49
00:06:58,695 --> 00:07:02,350
Bodhidharma grugnisce: "Han..."

50
00:07:06,620 --> 00:07:09,980
E continua zazen.

51
00:07:14,280 --> 00:07:17,145
Eka insiste, chiede l’elemosina:

52
00:07:20,910 --> 00:07:24,630
"Vi prego di pacificare la mia mente! »

53
00:07:29,190 --> 00:07:31,335
"È insopportabile! »

54
00:07:32,305 --> 00:07:34,400
"Sto impazzendo! »

55
00:07:44,290 --> 00:07:48,140
Infine, Bodhidharma gli dà un’occhiata:

56
00:07:54,430 --> 00:07:58,595
"Portami il tuo spirito. »

57
00:08:10,745 --> 00:08:13,680
"E io gli darò la pace. »

58
00:08:20,300 --> 00:08:22,590
Il Maestro Eka gli rispose:

59
00:08:26,210 --> 00:08:29,105
"Maestro, l’ho cercato, mio spirito! »

60
00:08:33,665 --> 00:08:37,750
"E rimase inaccessibile, inafferrabile! »

61
00:08:46,240 --> 00:08:49,020
Eka sta cercando la pace della mente.

62
00:08:52,630 --> 00:08:55,050
Sta soffrendo.

63
00:08:56,520 --> 00:08:59,340
Vuole la pace, la tranquillità.

64
00:09:04,085 --> 00:09:05,490
Non importa quante volte glielo diciamo:

65
00:09:05,550 --> 00:09:07,845
"È la tua mente! »

66
00:09:13,950 --> 00:09:16,360
"Dov’è, la mia mente? »

67
00:09:17,805 --> 00:09:19,655
"Dov’è, mio spirito, dov’è? »

68
00:09:22,452 --> 00:09:25,182
"Dov’è la radice di questo spirito? »

69
00:09:29,400 --> 00:09:32,480
"Qual è l’essenza di questo spirito? »

70
00:09:35,925 --> 00:09:38,528
"È nel cervello? »

71
00:09:46,670 --> 00:09:48,510
"È nel cuore? »

72
00:09:56,655 --> 00:10:00,025
Anche se non tutti si tagliano il braccio,

73
00:10:03,270 --> 00:10:07,120
nella neve, nel freddo...

74
00:10:08,810 --> 00:10:12,880
Tutti vogliono trovare qualcosa sulla propria mente.

75
00:10:16,040 --> 00:10:18,570
Tutti vogliono essere pacifici.

76
00:10:21,970 --> 00:10:23,691
Pacificare la sua mente.

77
00:10:27,930 --> 00:10:31,515
Eka ha avuto una risposta notevole quando ha detto:

78
00:10:31,645 --> 00:10:35,920
"L’ho cercato, ma è sfuggente! »

79
00:10:43,990 --> 00:10:46,030
"Non sono riuscito a trovarlo. »

80
00:10:48,615 --> 00:10:53,235
In giapponese si chiama "Shin fukatoku".

81
00:11:07,460 --> 00:11:09,885
"Fu" è la negazione, "niente".

82
00:11:13,960 --> 00:11:15,870
"Ta": Cogliere.

83
00:11:19,270 --> 00:11:21,100
Prendere, trovare.

84
00:11:24,230 --> 00:11:28,260
"Toku" è come "Mushotoku".

85
00:11:29,550 --> 00:11:33,470
"Toku" significa "ottenere".

86
00:11:35,910 --> 00:11:37,930
"Raggiungere il suo obiettivo".

87
00:11:40,370 --> 00:11:43,320
"Shin fukatoku",

88
00:11:47,710 --> 00:11:51,310
È la mente completamente irrintracciabile e sfuggente.

89
00:11:57,710 --> 00:11:58,955
Eka dice:

90
00:11:59,085 --> 00:12:01,650
"L’ho cercato, ma non l’ho trovato. »

91
00:12:04,480 --> 00:12:06,670
Questa è un’ottima risposta.

92
00:12:09,470 --> 00:12:13,340
Ma quello di Bodhidharma è ancora più bello.

93
00:12:18,060 --> 00:12:22,330
"Portami il tuo spirito e io lo pacificherò. »

94
00:12:29,700 --> 00:12:30,660
Eka ha detto:

95
00:12:30,720 --> 00:12:32,670
"Non posso! »

96
00:12:34,760 --> 00:12:37,320
"Non solo non posso prenderlo,"

97
00:12:37,400 --> 00:12:40,320
"ma non riesco nemmeno a trovarlo! »

98
00:12:44,610 --> 00:12:46,270
Bodhidharma gli dice:

99
00:12:48,910 --> 00:12:52,340
"In questo caso, si è già pacificato! »

100
00:13:07,610 --> 00:13:09,520
Seconda storia:

101
00:13:11,955 --> 00:13:14,575
Mi piacciono le storie.

102
00:13:16,890 --> 00:13:18,750
Maestro Obaku.

103
00:13:20,240 --> 00:13:22,330
Il maestro di Baso.

104
00:13:25,480 --> 00:13:30,240
Dare il suo insegnamento, davanti ai monaci.

105
00:13:33,405 --> 00:13:35,605
Qualcuno lo chiama:

106
00:13:41,550 --> 00:13:46,580
"Il maestro deve avere una grande facoltà di concentrazione. »

107
00:13:52,200 --> 00:13:53,985
Il Maestro Obaku risponde:

108
00:13:58,735 --> 00:14:02,110
"Concentrazione".

109
00:14:03,950 --> 00:14:07,594
"...o al contrario, una distrazione,"

110
00:14:10,310 --> 00:14:14,610
"...stai dicendo che queste cose esistono? »

111
00:14:18,300 --> 00:14:20,600
"Io vi dico:"

112
00:14:23,770 --> 00:14:27,100
"Cercateli e non li troverete da nessuna parte. »

113
00:14:32,840 --> 00:14:36,340
"Ma se mi dici che non esistono".

114
00:14:40,390 --> 00:14:42,490
"Te lo dico io:"

115
00:14:45,510 --> 00:14:47,390
"qualunque cosa tu pensi".

116
00:14:53,490 --> 00:14:58,050
"sei costantemente presente dove sei! »

117
00:15:08,040 --> 00:15:11,650
Anche quando pensi di essere distratto,

118
00:15:16,330 --> 00:15:20,610
guarda il luogo dove ha avuto origine la distrazione.

119
00:15:24,850 --> 00:15:27,950
Vedrete che finalmente non ha origine.

120
00:15:33,360 --> 00:15:37,260
Lo stato mentale della distrazione che appare...

121
00:15:40,870 --> 00:15:43,490
...non va da nessuna parte.

122
00:15:44,920 --> 00:15:46,695
In tutte e dieci le direzioni.

123
00:15:48,305 --> 00:15:50,690
E non andrà da nessun’altra parte.

124
00:16:10,270 --> 00:16:13,490
In zazen non c’è distrazione o concentrazione.

125
00:16:21,040 --> 00:16:23,470
Stai fermo.

126
00:16:30,800 --> 00:16:33,090
Il resto sono tecniche.

127
00:16:35,820 --> 00:16:40,168
Ci porta via dall’autentico zazen.

128
00:16:45,129 --> 00:16:46,769
Terza storia.

129
00:16:47,119 --> 00:16:49,719
- Sì, ti ho viziato! -

130
00:17:00,600 --> 00:17:03,750
Viene dal tempo di Baso in Cina.

131
00:17:05,760 --> 00:17:18,250
Baso, questo è il discepolo del maestro di cui parlavo prima, Obaku.

132
00:17:24,410 --> 00:17:28,330
A quel tempo viveva uno studioso

133
00:17:30,950 --> 00:17:34,280
...che si chiamava Ryo.

134
00:17:36,560 --> 00:17:41,090
Era famoso per la sua conoscenza del buddismo.

135
00:17:45,320 --> 00:17:48,840
Ha passato la vita a tenere conferenze.

136
00:17:53,410 --> 00:17:58,790
Un giorno è venuto fuori che aveva un’intervista con Baso.

137
00:18:04,890 --> 00:18:07,160
Glielo chiese Baso:

138
00:18:09,190 --> 00:18:12,770
- Su quali argomenti tiene la sua lezione? »

139
00:18:15,850 --> 00:18:20,040
- "Sullo Spirito Sutra, l’Hannya Shingyo. »

140
00:18:25,360 --> 00:18:28,220
Si chiama anche Heart Sutra.

141
00:18:32,420 --> 00:18:36,750
Non è chiaro se la mente nel cervello o nel cuore.

142
00:18:41,320 --> 00:18:47,240
In "Shingyo", "Shin" significa mente, o cuore.

143
00:18:52,600 --> 00:18:55,460
"Gyô" significa Sutra.

144
00:18:58,090 --> 00:19:00,285
Così Baso glielo chiede:

145
00:19:03,825 --> 00:19:07,260
- "Cosa usate per le vostre lezioni? »

146
00:19:09,950 --> 00:19:11,755
Ryo ha risposto:

147
00:19:14,525 --> 00:19:17,527
- "Con lo spirito. »

148
00:19:19,530 --> 00:19:21,355
Baso gli dice:

149
00:19:24,685 --> 00:19:30,210
- Lo spirito, è davvero un bravo attore! »

150
00:19:34,480 --> 00:19:39,390
"Il significato del suo pensiero è come la stravaganza di un buffone. »

151
00:19:45,690 --> 00:19:51,580
"Per quanto riguarda i sei sensi che ci fanno sentire come in un film al cinema...".

152
00:20:07,270 --> 00:20:10,420
"Questi sei sensi sono vuoti! »

153
00:20:12,420 --> 00:20:16,970
"Come un’onda senza un vero inizio o una vera fine. »

154
00:20:21,260 --> 00:20:26,340
"Come può la mente tenere una conferenza su un sutra?

155
00:20:33,590 --> 00:20:38,200
Ryo rispose, pensando di essere furbo:

156
00:20:42,150 --> 00:20:47,880
- Se non può farlo lo spirito, forse può farlo la non-mente? »

157
00:20:51,630 --> 00:20:53,315
Baso gli dice:

158
00:20:55,055 --> 00:20:58,607
- "Sì, esattamente! »

159
00:21:00,167 --> 00:21:04,150
"Il non-spirito può dare totalmente lezioni. »

160
00:21:09,050 --> 00:21:13,230
Ryo, credendosi soddisfatto della sua risposta...

161
00:21:16,350 --> 00:21:19,730
...si è spolverato le maniche...

162
00:21:25,070 --> 00:21:28,100
...e si preparò ad andarsene.

163
00:21:31,390 --> 00:21:33,820
Lo chiama il maestro Baso:

164
00:21:35,920 --> 00:21:38,605
- "Professore! »

165
00:21:39,305 --> 00:21:41,160
Ryo ha girato la testa.

166
00:21:47,460 --> 00:21:49,115
Baso gli dice:

167
00:21:49,185 --> 00:21:51,560
- "Cosa stai facendo? »

168
00:21:55,630 --> 00:21:58,225
Ryo ha avuto una profonda rivelazione.

169
00:22:03,905 --> 00:22:07,490
Ha preso coscienza della sua mancanza di spirito.

170
00:22:10,940 --> 00:22:12,440
E Baso gli dice:

171
00:22:12,550 --> 00:22:16,000
"Dalla nascita alla morte, è così. »

172
00:22:21,750 --> 00:22:24,976
Pensiamo, facciamo delle cose,

173
00:22:28,400 --> 00:22:30,670
spontaneamente.

174
00:22:33,320 --> 00:22:36,315
Ryo voleva inchinarsi a Baso,

175
00:22:42,215 --> 00:22:44,840
ma questo gli dice:

176
00:22:46,660 --> 00:22:51,220
"Non c’è bisogno di fare finta di niente! »

177
00:23:02,150 --> 00:23:07,280
Tutto il corpo di Ryo era coperto di sudore.

178
00:23:16,080 --> 00:23:19,360
Tornò al suo tempio.

179
00:23:21,260 --> 00:23:24,295
E disse ai suoi discepoli:

180
00:23:27,575 --> 00:23:31,645
"Stavo pensando che quando morirò, potremmo dire: "

181
00:23:37,670 --> 00:23:41,440
"Che avevo dato le migliori lezioni..."

182
00:23:44,650 --> 00:23:47,170
"...sullo Zen. »

183
00:23:52,430 --> 00:23:55,830
"Oggi ho fatto una domanda a Baso. »

184
00:24:00,250 --> 00:24:03,710
"E ha cancellato la nebbia di tutta la mia vita. »

185
00:24:10,790 --> 00:24:14,400
Ryo ha rinunciato a fare lezione.

186
00:24:18,180 --> 00:24:21,125
E si ritirò nel cuore delle montagne.

187
00:24:24,475 --> 00:24:28,290
E non abbiamo più avuto sue notizie.

188
00:25:15,750 --> 00:25:21,950
Perché la mente è così difficile da catturare?

189
00:25:24,620 --> 00:25:25,755
Perché?

190
00:25:35,090 --> 00:25:39,490
Perché il pensiero, la coscienza...

191
00:25:44,230 --> 00:25:46,680
...e anche la realtà...

192
00:26:00,830 --> 00:26:02,620
...a volte appare...

193
00:26:06,380 --> 00:26:08,210
...a volte scompare.

194
00:26:10,690 --> 00:26:16,570
A volte appare, a volte scompare...

195
00:26:26,840 --> 00:26:30,275
Quando scompare, non sappiamo dove si trova.

196
00:26:32,895 --> 00:26:35,532
Ma dove si trova...

197
00:26:37,512 --> 00:26:39,260
...lei è lì.

198
00:26:42,620 --> 00:26:44,475
Lei è lì.

199
00:26:53,205 --> 00:26:56,900
Dove non c’è, c’è anche lei.

200
00:27:04,820 --> 00:27:09,140
Il tempo e la coscienza non sono lineari.

201
00:28:08,350 --> 00:28:10,574
Per quanto riguarda la distrazione...

202
00:28:19,220 --> 00:28:24,070
Quando ero un ragazzino, mi dicevano sempre
che non ero concentrato.

203
00:28:37,820 --> 00:28:39,365
Distratto.

204
00:28:42,805 --> 00:28:45,670
Ma quando ho giocato...

205
00:28:47,257 --> 00:28:50,930
Quando giocavo con le mie macchinine...

206
00:28:55,110 --> 00:28:58,880
...ero completamente concentrato.

207
00:29:06,020 --> 00:29:11,980
L’Hannya Shingyo e la cerimonia sono dedicati a Jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
E Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra della grande saggezza che permette di andare oltre

210
00:30:12,312 --> 00:30:17,006
Il Bodhisattva della Vera Libertà,

211
00:30:17,196 --> 00:30:21,144
dalla pratica profonda della Grande Saggezza,

212
00:30:21,264 --> 00:30:25,022
capisce che il corpo e i cinque skandah non sono altro che dei vuoti.

213
00:30:25,152 --> 00:30:30,151
e attraverso questa comprensione, aiuta tutti coloro che soffrono.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, i fenomeni non sono diversi dal ku.

215
00:30:34,970 --> 00:30:39,527
Il Ku non è diverso dagli anormali.

216
00:30:39,987 --> 00:30:42,996
I fenomeni diventano ku.

217
00:30:43,196 --> 00:30:51,305
Il Ku diventa un fenomeno (la forma è il vuoto, il vuoto è la forma),

218
00:30:51,575 --> 00:30:56,155
Anche i cinque skanda sono fenomeni.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, tutta l’esistenza ha il carattere del ku,

220
00:31:03,451 --> 00:31:08,429
non c’è nascita e non c’è inizio,

221
00:31:08,989 --> 00:31:14,707
nessuna purezza, nessuna macchia, nessuna crescita, nessun decadimento.

222
00:31:14,977 --> 00:31:20,130
Ecco perché nel ku non c’è forma, non c’è skanda,

223
00:31:20,939 --> 00:31:26,080
niente occhi, niente orecchie, niente naso, niente lingua, niente corpo, niente coscienza;

224
00:31:26,491 --> 00:31:32,397
non c’è colore, suono, odore, sapore, gusto, tatto o pensiero;

225
00:31:32,527 --> 00:31:37,915
non c’è conoscenza, non c’è ignoranza, non c’è ignoranza, non c’è illusione, non c’è cessazione della sofferenza,

226
00:31:38,225 --> 00:31:43,283
non c’e’ conoscenza, non c’e’ profitto, non c’e’ profitto.

227
00:31:43,413 --> 00:31:48,649
Per il Bodhisattva, grazie a questa Saggezza che porta oltre,

228
00:31:48,969 --> 00:31:54,622
non esiste la paura o il terrore.

229
00:31:54,882 --> 00:32:02,024
Tutte le illusioni e gli attacchi vengono rimossi

230
00:32:02,504 --> 00:32:11,116
e può cogliere il fine ultimo della vita, il Nirvana.

231
00:32:12,325 --> 00:32:16,407
Tutti i Buddha del passato, del presente e del futuro...

232
00:32:16,497 --> 00:32:21,489
può raggiungere la comprensione di questa Suprema Saggezza...

233
00:32:21,569 --> 00:32:25,021
che porta sofferenza, il Satori,

234
00:32:25,081 --> 00:32:30,329
da questo incantesimo, incomparabile e ineguagliabile, autentico,

235
00:32:30,432 --> 00:32:36,163
che rimuove ogni sofferenza e permette di trovare la realtà, il vero ku:

236
00:32:38,113 --> 00:32:52,170
"Andate, andate, andate insieme oltre l’aldilà sulla riva del satori. »

237
00:33:01,240 --> 00:33:14,420
Non importa quanti siano gli esseri, io giuro di salvarli tutti.

238
00:33:14,600 --> 00:33:23,095
Non importa quante passioni ci siano, io giuro di sconfiggerle tutte.

239
00:33:25,635 --> 00:33:34,292
Non importa quanti Dharma ci sono, io giuro di acquisirli tutti.

240
00:33:35,112 --> 00:33:43,700
Non importa quanto sia perfetto un Buddha, io giuro di diventarlo.

241
00:33:44,085 --> 00:33:48,247
Possano i meriti di questa recitazione

242
00:33:48,247 --> 00:33:52,768
penetrare in tutti gli esseri in ogni luogo,

243
00:33:52,768 --> 00:33:57,538
in modo che tutti noi esseri senzienti

244
00:33:57,538 --> 00:34:03,129
possiamo realizzare insieme la Via del Buddha.

245
00:34:05,332 --> 00:34:16,597
Tutti i Buddha del passato, del presente e del futuro nelle dieci direzioni.

246
00:34:17,517 --> 00:34:26,629
Tutti i Bodhisattva e patriarchi

247
00:34:27,869 --> 00:34:38,142
Il Sutra della "Saggezza che va oltre".

248
00:34:53,546 --> 00:34:58,216
Sanpaï!

249
00:37:25,590 --> 00:37:28,060
Whew!

250
00:37:46,750 --> 00:37:48,965
Ciao a tutti!

251
00:37:57,955 --> 00:38:03,310
Spero che siate contenti che la pandemia sia finita!

252
00:38:32,700 --> 00:38:34,600
Hola Paola!
