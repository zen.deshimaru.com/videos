1
00:00:12,840 --> 00:00:20,310
Em um poema de Yoka Daishi,
um mestre chinês que escreveu

2
00:00:20,310 --> 00:00:25,100
A Canção do Satori Imediato,

3
00:00:25,100 --> 00:00:27,100
o Shôdôka

4
00:00:27,800 --> 00:00:35,760
e foi discípulo do Mestre Eno, 
cuja transmissão ele recebeu

5
00:00:35,760 --> 00:00:37,210
em uma reunião que durou um dia e uma noite.

6
00:00:37,210 --> 00:00:43,100
Depois disso, continuou a ensinar.

7
00:00:44,040 --> 00:00:50,040
Seu poema: "Abandonem os quatro elementos, não procurem mais acumular,

8
00:00:50,740 --> 00:00:57,880
em paz e conclusão absoluta,

9
00:00:58,500 --> 00:01:01,940
Beba e coma como quiser.

10
00:01:02,360 --> 00:01:08,660
Todos os fenômenos são impermanentes, tudo é KU, sem noumenon, sem substância própria...

11
00:01:09,080 --> 00:01:14,080
e esse é apenas o grande
e completo Buddha satori".

12
00:01:14,560 --> 00:01:17,820
Mestre Deshimaru diz que aqui o poema trata de

13
00:01:18,260 --> 00:01:23,620
dos Três Tesouros do Budismo o abandono dos quatro elementos,

14
00:01:23,900 --> 00:01:30,300
os quatro elementos
é água, terra, fogo, ar.

15
00:01:30,880 --> 00:01:35,440
O segundo tesouro é a paz e o sossego e o
tranqüilidade do zazen, nirvana,

16
00:01:36,060 --> 00:01:44,400
a terceira a impermanência dos fenômenos, a consciência, a ausência de noumenon de existências.

17
00:01:44,740 --> 00:01:50,120
Na primeira tesouraria, soltar os quatro elementos

18
00:01:50,120 --> 00:01:56,720
e não procura mais capturá-los, segurá-los, apreendê-los.

19
00:01:56,720 --> 00:02:04,300
O Mestre Deshimaru diz que não devemos buscar o noumenon ou o ego.

20
00:02:04,300 --> 00:02:09,980
Todos estão procurando o ego de seu corpo.
ele quer ser dono de seu corpo

21
00:02:10,700 --> 00:02:15,580
e daí nascem sentimentos como ciúme, egoísmo, inveja.

22
00:02:15,580 --> 00:02:21,940
portanto, no primeiro tesouro,
você aprende a desistir, soltar,

23
00:02:21,940 --> 00:02:28,150
seu corpo, seu ego, sua mente.
É interessante porque você também pode ver

24
00:02:28,150 --> 00:02:33,580
que estes quatro elementos, abandonando a
quatro elementos e não mais procurar

25
00:02:33,580 --> 00:02:39,560
para pegá-los, para apreendê-los
em conexão com nosso mundo.

26
00:02:39,560 --> 00:02:44,000
Água, terra, fogo e ar.

27
00:02:44,000 --> 00:02:48,840
Nos ensinamentos
dos Mestres Zen, há sempre

28
00:02:48,850 --> 00:02:53,560
uma intuição profunda, e isto é
interessante, porque esta intuição

29
00:02:53,560 --> 00:03:00,370
une-se à ciência moderna, o
ciências da vida

30
00:03:00,370 --> 00:03:07,120
e cada vez mais desses cientistas da vida também estão nos dizendo

31
00:03:07,120 --> 00:03:13,630
para abandonar os quatro elementos,
parar de tentar pegá-los

32
00:03:13,630 --> 00:03:18,490
oceanólogos, hidrólogos
explicar-nos que os oceanos

33
00:03:18,490 --> 00:03:24,250
eles são os verdadeiros pulmões da terra e
que temos que parar de acidificá-los,

34
00:03:24,250 --> 00:03:30,280
para matá-los, enchê-los de plástico.
Especialistas em Terra,

35
00:03:30,280 --> 00:03:34,590
com sua vida vegetal e animal, com o que está em suas entranhas...

36
00:03:34,590 --> 00:03:40,900
seus recursos, seus combustíveis fósseis,
suas terras raras, devemos deixá-las,

37
00:03:40,900 --> 00:03:48,060
temos que deixá-los ir, nos dizem agrônomos, geólogos, bioquímicos...

38
00:03:48,060 --> 00:03:53,290
vulcanólogos. Precisamos encontrar um
outro funcionamento.

39
00:03:53,290 --> 00:03:56,740
Não são os Mestres Zen aqui, são
realmente cientistas

40
00:03:56,740 --> 00:04:03,760
Eles também têm agora uma profunda compreensão do princípio de interdependência.

41
00:04:03,760 --> 00:04:10,810
Fogo que os humanos usam sem cautela, sem saber como controlá-lo,

42
00:04:10,810 --> 00:04:15,959
fusão nuclear atômica,
o fogo do sol,

43
00:04:15,959 --> 00:04:21,669
temos que deixar ir, não sabemos, é...
muito complicado.

44
00:04:21,669 --> 00:04:27,340
Temos que encontrar outra maneira mais simples de trabalhar e finalmente o ar, que tornamos irrespirável.

45
00:04:27,340 --> 00:04:33,580
com uma atmosfera saturada de CO2
metano, gases tóxicos,

46
00:04:33,960 --> 00:04:41,160
climatologistas, meteorologistas
glaciólogos e muitos outros,

47
00:04:41,160 --> 00:04:47,760
estão nos dizendo, pare, deixe ir,
manter fora do caminho

48
00:04:47,770 --> 00:04:52,330
por isso, eles nos ensinam a todos também,
para parar de tentar agarrar, para parar de agarrar...

49
00:04:52,330 --> 00:04:59,100
os quatro elementos. Em shôdôka
"abandona" o caracter chinês é "放 fang".

50
00:04:59,100 --> 00:05:06,070
"放 fang" também significa livre, significa
dizer desistir, deixar ir, covarde.

51
00:05:06,070 --> 00:05:12,600
É como quando você
trazemos as ovelhas para o pasto, nós as deixamos ir livres,

52
00:05:12,600 --> 00:05:20,139
pastar a grama, silenciosamente.
Deixar os recursos fósseis na terra,

53
00:05:20,139 --> 00:05:27,190
deixar a terra descansar. Mestre Deshimaru, em seu comentário, disse

54
00:05:27,190 --> 00:05:32,300
que não devemos procurar o ego no
corpo, todos querem ter e possuir esse corpo.

55
00:05:32,300 --> 00:05:36,450
mas é a mesma coisa entre nós e
o planeta.

56
00:05:36,450 --> 00:05:43,479
Consideramos que é nosso
corpo, mas não sabemos nada sobre isso.

57
00:05:43,479 --> 00:05:51,970
a substância divina,
espiritual, é um absurdo,

58
00:05:51,970 --> 00:05:59,420
nos comportamos com o planeta
como com nossos corpos.

59
00:05:59,760 --> 00:06:07,680
Por zazen podemos entendê-lo com muita exatidão e se soltarmos os quatro elementos

60
00:06:08,080 --> 00:06:14,340
é a Terra de Buda, paz, satori, que são realizadas.

61
00:06:14,780 --> 00:06:22,280
em uma operação simples, harmoniosa, que não fere nem mata a vida.

62
00:06:22,460 --> 00:06:27,920
a semente de Buda, a possibilidade
para elevar-se acima dela.

63
00:06:28,260 --> 00:06:35,020
O que me interessa aqui é que agora os próprios cientistas

64
00:06:35,700 --> 00:06:44,060
estão começando a descobrir o que estamos fazendo,
percebemos e praticamos naturalmente.

65
00:06:47,560 --> 00:06:55,560
Os cientistas também explicam
que nós mesmos somos feitos destes quatro elementos.

66
00:06:55,900 --> 00:07:02,920
mas também que somos um corpo vivo no qual

67
00:07:03,400 --> 00:07:09,379
há mais bactérias, mais vírus, 
microbiana

68
00:07:09,379 --> 00:07:18,439
temos mais, acho eu, vírus do que até
de células vivas,

69
00:07:18,439 --> 00:07:23,649
funciona porque há um equilíbrio, uma proteção.

70
00:07:23,649 --> 00:07:29,019
É claro que, quando não há equilíbrio, não há proteção...

71
00:07:29,019 --> 00:07:35,959
Felizmente, pudemos nos encontrar,
para conhecer zazen, para praticá-lo,

72
00:07:35,959 --> 00:07:42,979
para torná-lo conhecido. É muito precioso, não só para nós, mas para todas as existências.
