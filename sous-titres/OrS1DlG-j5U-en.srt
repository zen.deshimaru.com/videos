1
00:00:00,000 --> 00:00:08,750
I do zazen with a kesa by Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
In silk.

3
00:00:15,330 --> 00:00:19,215
I’m very happy.

4
00:00:20,235 --> 00:00:22,830
I like Barbara very much.

5
00:00:24,580 --> 00:00:27,210
She’s a great saint.

6
00:00:33,770 --> 00:00:38,190
Fortunately, my disciples in general are better than me.

7
00:00:42,910 --> 00:00:45,830
Especially Barbara.

8
00:00:48,850 --> 00:00:51,270
And Ariadna [the translator] too.

9
00:00:55,710 --> 00:00:58,515
The three treasures.

10
00:01:01,015 --> 00:01:03,817
Buddha, dharma, sangha,

11
00:01:06,267 --> 00:01:10,440
are inseparable.

12
00:01:24,820 --> 00:01:28,470
I have prepared you a kusen…

13
00:01:35,340 --> 00:01:41,560
… the continuity of what I did during the summer camp…

14
00:01:45,170 --> 00:01:50,930
… on the four foundations of magical powers.

15
00:01:56,750 --> 00:02:01,770
I did this during summer camp…

16
00:02:02,130 --> 00:02:10,530
It’s said that the four foundations are like the four hooves of a horse.

17
00:02:35,940 --> 00:02:40,120
The first is will, volition.

18
00:02:44,460 --> 00:02:50,300
Volition is something stronger than you.

19
00:02:52,290 --> 00:02:55,400
And that pushes you to act.

20
00:02:58,920 --> 00:03:02,400
It’s like a matter of survival.

21
00:03:06,430 --> 00:03:08,350
It’s very strong.

22
00:03:09,410 --> 00:03:12,160
I talked about it during summer camp.

23
00:03:15,530 --> 00:03:18,550
The second is the mind.

24
00:03:20,745 --> 00:03:23,015
The second hoof.

25
00:03:27,730 --> 00:03:33,390
The third is forward progression, moving forward.

26
00:03:38,440 --> 00:03:40,810
The fourth is thought.

27
00:03:43,810 --> 00:03:46,855
That’s what I’m talking about now.

28
00:03:48,135 --> 00:03:50,020
The second.

29
00:03:54,970 --> 00:03:57,515
I had talked about it during summer camp,

30
00:03:57,625 --> 00:04:00,860
but I wasn’t done.

31
00:04:03,270 --> 00:04:09,595
I’m going to tell you three Zen stories…

32
00:04:11,995 --> 00:04:16,060
… that explain better the nature of mind.

33
00:04:19,990 --> 00:04:25,860
Master Eka, a disciple of Bodhidharma…

34
00:04:32,370 --> 00:04:35,760
… standing in the snow…

35
00:04:39,330 --> 00:04:41,145
… in the cold…

36
00:04:44,635 --> 00:04:47,564
… spoke to his master:

37
00:04:53,470 --> 00:04:57,240
- “Master, my mind is’nt peaceful.”

38
00:05:08,460 --> 00:05:12,130
“I beg you to give it peace!”

39
00:05:18,940 --> 00:05:25,380
Eka had killed a lot of men because he had been in the military.

40
00:05:33,130 --> 00:05:37,370
He felt extremely guilty about all this.

41
00:05:40,780 --> 00:05:44,100
He couldn’t get rid of the guilt.

42
00:05:49,410 --> 00:05:54,485
He’s in the snow and asks Bodhidharma: “Accept me!”

43
00:06:00,245 --> 00:06:02,050
“As a disciple!”

44
00:06:06,000 --> 00:06:09,410
Bodhidharma does’nt answer.

45
00:06:17,770 --> 00:06:19,615
He insists.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma does’nt move.

47
00:06:30,960 --> 00:06:35,420
At the end, he cuts his arm with his sword.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma says, “Well…”

49
00:06:58,695 --> 00:07:02,350
Bodhidharma grunts: “Han… ”

50
00:07:06,620 --> 00:07:09,980
And he continues zazen.

51
00:07:14,280 --> 00:07:17,145
Eka insists, begs:

52
00:07:20,910 --> 00:07:24,630
- “I beg you to pacify my mind!”

53
00:07:29,190 --> 00:07:31,335
“It’s unbearable!”

54
00:07:32,305 --> 00:07:34,400
“I’m going crazy!”

55
00:07:44,290 --> 00:07:48,140
Finally, Bodhidharma gives him a look:

56
00:07:54,430 --> 00:07:58,595
- “Bring me your mind.”

57
00:08:10,745 --> 00:08:13,680
“And I will give it peace.”

58
00:08:20,300 --> 00:08:22,590
Master Eka answers him:

59
00:08:26,210 --> 00:08:29,105
- “Master, I have sought it, my mind!”

60
00:08:33,665 --> 00:08:37,750
“And it remained inaccessible, elusive!”

61
00:08:46,240 --> 00:08:49,020
Eka seeks peace of mind.

62
00:08:52,630 --> 00:08:55,050
He’s in pain.

63
00:08:56,520 --> 00:08:59,340
He wants peace, calm.

64
00:09:04,085 --> 00:09:05,490
We can tell him:

65
00:09:05,550 --> 00:09:07,845
- “It’s your mind!”

66
00:09:13,950 --> 00:09:16,360
- “Where’s my mind?”

67
00:09:17,805 --> 00:09:19,655
“Where is it, my mind?”

68
00:09:22,452 --> 00:09:25,182
“Where’s the root of this mind?”

69
00:09:29,400 --> 00:09:32,480
“What’s the essence of this mind?”

70
00:09:35,925 --> 00:09:38,528
“Is it in the brain?”

71
00:09:46,670 --> 00:09:48,510
“Is it in the heart?”

72
00:09:56,655 --> 00:10:00,025
Even if not everyone cuts their arm,

73
00:10:03,270 --> 00:10:07,120
in the snow, in the cold…

74
00:10:08,810 --> 00:10:12,880
Everyone wants to find something about their mind.

75
00:10:16,040 --> 00:10:18,570
Everyone wants to become peaceful.

76
00:10:21,970 --> 00:10:23,691
Pacify their mind.

77
00:10:27,930 --> 00:10:31,515
Eka had a remarkable response when he said:

78
00:10:31,645 --> 00:10:35,920
“I looked for it, but it’s elusive!”

79
00:10:43,990 --> 00:10:46,030
“I couldn’t find it.”

80
00:10:48,615 --> 00:10:53,235
In Japanese, they said: “Shin fukatoku”.

81
00:11:07,460 --> 00:11:09,885
“Fu” is negation, “nothing”.

82
00:11:13,960 --> 00:11:15,870
“Ta”: Enter.

83
00:11:19,270 --> 00:11:21,100
Enter, find.

84
00:11:24,230 --> 00:11:28,260
“Toku” is like “Mushotoku”.

85
00:11:29,550 --> 00:11:33,470
“Toku” means “to get”.

86
00:11:35,910 --> 00:11:37,930
“Reaching your goal.

87
00:11:40,370 --> 00:11:43,320
“Shin fukatoku”,

88
00:11:47,710 --> 00:11:51,310
It’s the completely untraceable, elusive mind.

89
00:11:57,710 --> 00:11:58,955
Eka says:

90
00:11:59,085 --> 00:12:01,650
“I looked for it, but I couldn’t find it.”

91
00:12:04,480 --> 00:12:06,670
This is an excellent answer.

92
00:12:09,470 --> 00:12:13,340
But Bodhidharma’s is even more magnificent.

93
00:12:18,060 --> 00:12:22,330
“Bring me your mind and I will pacify it.”

94
00:12:29,700 --> 00:12:30,660
Eka said:

95
00:12:30,720 --> 00:12:32,670
“I can’t!”

96
00:12:34,760 --> 00:12:37,320
“Not only can’t I catch it,”

97
00:12:37,400 --> 00:12:40,320
“but I can’t even find it!”

98
00:12:44,610 --> 00:12:46,270
Bodhidharma said to him:

99
00:12:48,910 --> 00:12:52,340
“In this case, it’s already pacified!”

100
00:13:07,610 --> 00:13:09,520
Second story :

101
00:13:11,955 --> 00:13:14,575
I like stories.

102
00:13:16,890 --> 00:13:18,750
Master Obaku.

103
00:13:20,240 --> 00:13:22,330
Baso’s master.

104
00:13:25,480 --> 00:13:30,240
Gives his teaching, in front of monks.

105
00:13:33,405 --> 00:13:35,605
Someone calls out to him:

106
00:13:41,550 --> 00:13:46,580
“The master must have a great faculty of concentration.”

107
00:13:52,200 --> 00:13:53,985
Master Obaku answers:

108
00:13:58,735 --> 00:14:02,110
“Concentration,”

109
00:14:03,950 --> 00:14:07,594
“…or on the contrary, distraction,”

110
00:14:10,310 --> 00:14:14,610
“… you say these things exist?”

111
00:14:18,300 --> 00:14:20,600
“I say to you:”

112
00:14:23,770 --> 00:14:27,100
“Look for them, and you won’t find them anywhere.”

113
00:14:32,840 --> 00:14:36,340
“But if you tell me they don’t exist,”

114
00:14:40,390 --> 00:14:42,490
“I will tell you:”

115
00:14:45,510 --> 00:14:47,390
“whatever you think,”

116
00:14:53,490 --> 00:14:58,050
“You are constantly present where you are!”

117
00:15:08,040 --> 00:15:11,650
Even when you think you’re distracted,

118
00:15:16,330 --> 00:15:20,610
look at the place where the distraction originated.

119
00:15:24,850 --> 00:15:27,950
You will see that it has no origin after all.

120
00:15:33,360 --> 00:15:37,260
The mental state of the distraction that appears…

121
00:15:40,870 --> 00:15:43,490
… goes nowhere.

122
00:15:44,920 --> 00:15:46,695
In all ten directions.

123
00:15:48,305 --> 00:15:50,690
And it doesn’t go anywhere else either.

124
00:16:10,270 --> 00:16:13,490
In zazen, there’s no distraction or concentration.

125
00:16:21,040 --> 00:16:23,470
Simply sit still.

126
00:16:30,800 --> 00:16:33,090
The rest are techniques.

127
00:16:35,820 --> 00:16:40,168
It takes us away from authentic zazen.

128
00:16:45,129 --> 00:16:46,769
Third story.

129
00:16:47,119 --> 00:16:49,719
- Yeah, I spoiled you! -

130
00:17:00,600 --> 00:17:03,750
It’s at the time of Baso, in China.

131
00:17:05,760 --> 00:17:18,250
Baso is the disciple of the master I just mentioned, Obaku.

132
00:17:24,410 --> 00:17:28,330
At that time there lived a scholar

133
00:17:30,950 --> 00:17:34,280
… who was called Ryo.

134
00:17:36,560 --> 00:17:41,090
He was famous for his knowledge of Buddhism.

135
00:17:45,320 --> 00:17:48,840
He spent his life lecturing.

136
00:17:53,410 --> 00:17:58,790
One day he happened to have an interview with Baso.

137
00:18:04,890 --> 00:18:07,160
Baso asked him:

138
00:18:09,190 --> 00:18:12,770
- “On what topics do you give lectures?”

139
00:18:15,850 --> 00:18:20,040
- “On the Mind Sutra, the Hannya Shingyo.”

140
00:18:25,360 --> 00:18:28,220
It’s also called the Heart Sutra.

141
00:18:32,420 --> 00:18:36,750
It’s not clear whether the mind is in the brain or in the heart.

142
00:18:41,320 --> 00:18:47,240
In “Shingyo”, “Shin” means mind, or heart.

143
00:18:52,600 --> 00:18:55,460
“Gyo” means sutra.

144
00:18:58,090 --> 00:19:00,285
Then Baso asks him:

145
00:19:03,825 --> 00:19:07,260
- “What do you use for your conferences?”

146
00:19:09,950 --> 00:19:11,755
Ryo answered:

147
00:19:14,525 --> 00:19:17,527
- “The mind.”

148
00:19:19,530 --> 00:19:21,355
Baso said to him:

149
00:19:24,685 --> 00:19:30,210
- “The mind, it’s like a super good actor!”

150
00:19:34,480 --> 00:19:39,390
“The meaning of his thought is like the extravagance of a jester.”

151
00:19:45,690 --> 00:19:51,580
“As for the six senses that make us feel like we’re watching a movie at the cinema…”

152
00:20:07,270 --> 00:20:10,420
“These six senses are empty!”

153
00:20:12,420 --> 00:20:16,970
“Like a wave with no real beginning nor end.”

154
00:20:21,260 --> 00:20:26,340
“How could the mind lecture on a sutra?”

155
00:20:33,590 --> 00:20:38,200
Ryo replied, thinking he was clever:

156
00:20:42,150 --> 00:20:47,880
- “If the mind can’t do it, maybe the non-mind can?”

157
00:20:51,630 --> 00:20:53,315
Baso answers him:

158
00:20:55,055 --> 00:20:58,607
- “Yes, exactly!”

159
00:21:00,167 --> 00:21:04,150
“The non-mind can very well give a lecture.”

160
00:21:09,050 --> 00:21:13,230
Ryo, believing himself satisfied with his answer…

161
00:21:16,350 --> 00:21:19,730
… dusted his sleeves…

162
00:21:25,070 --> 00:21:28,100
… and got ready to leave.

163
00:21:31,390 --> 00:21:33,820
Master Baso calls him :

164
00:21:35,920 --> 00:21:38,605
- “Professor!”

165
00:21:39,305 --> 00:21:41,160
Ryo turned his head.

166
00:21:47,460 --> 00:21:49,115
Baso said to him:

167
00:21:49,185 --> 00:21:51,560
- “What are you doing?”

168
00:21:55,630 --> 00:21:58,225
Ryo had a profound revelation.

169
00:22:03,905 --> 00:22:07,490
He became aware of his non-mind.

170
00:22:10,940 --> 00:22:12,440
And Baso said to him:

171
00:22:12,550 --> 00:22:16,000
“From birth to death, it is so.”

172
00:22:21,750 --> 00:22:24,976
“We think, we do things,”

173
00:22:28,400 --> 00:22:30,670
“spontaneously.”

174
00:22:33,320 --> 00:22:36,315
Ryo wanted to prostrate himself before Baso,

175
00:22:42,215 --> 00:22:44,840
but he says to him:

176
00:22:46,660 --> 00:22:51,220
“No need to make silly fakes!”

177
00:23:02,150 --> 00:23:07,280
Ryo’s body was completely covered with sweat.

178
00:23:16,080 --> 00:23:19,360
He returned to his temple.

179
00:23:21,260 --> 00:23:24,295
And he said to his disciples:

180
00:23:27,575 --> 00:23:31,645
- “I thought that when I died, they might say, ”

181
00:23:37,670 --> 00:23:41,440
“that I had given the best lectures…”

182
00:23:44,650 --> 00:23:47,170
“… on Zen.”

183
00:23:52,430 --> 00:23:55,830
“Today I asked Baso a question.”

184
00:24:00,250 --> 00:24:03,710
“And he cleared the fog of my whole life.”

185
00:24:10,790 --> 00:24:14,400
Ryo gave up lectures.

186
00:24:18,180 --> 00:24:21,125
And withdrew to the heart of the mountains.

187
00:24:24,475 --> 00:24:28,290
And one never heard from him again.

188
00:25:15,750 --> 00:25:21,950
Why is the mind so hard to catch?

189
00:25:24,620 --> 00:25:25,755
Why?

190
00:25:35,090 --> 00:25:39,490
Because the thought, the consciousness…

191
00:25:44,230 --> 00:25:46,680
… and even reality…

192
00:26:00,830 --> 00:26:02,620
… sometimes appears…

193
00:26:06,380 --> 00:26:08,210
… sometimes disappears.

194
00:26:10,690 --> 00:26:16,570
Sometimes appears, sometimes disappears…

195
00:26:26,840 --> 00:26:30,275
When it disappears, we don’t know where it is.

196
00:26:32,895 --> 00:26:35,532
But where it is…

197
00:26:37,512 --> 00:26:39,260
… it’s there.

198
00:26:42,620 --> 00:26:44,475
It’s present.

199
00:26:53,205 --> 00:26:56,900
Where it is’nt, it’s present too.

200
00:27:04,820 --> 00:27:09,140
Time and consciousness are not linear.

201
00:28:08,350 --> 00:28:10,574
As for distraction…

202
00:28:19,220 --> 00:28:24,070
When I was a child, I was always told
that I wasn’t focused.

203
00:28:37,820 --> 00:28:39,365
Distracted.

204
00:28:42,805 --> 00:28:45,670
But when I played…

205
00:28:47,257 --> 00:28:50,930
… when I was playing with my toy cars…

206
00:28:55,110 --> 00:28:58,880
… I was completely focused.

207
00:29:06,020 --> 00:29:11,980
The Hannya Shingyo and the ceremony are dedicated to Jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
And Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra of the great wisdom that allows to go beyond

210
00:30:12,312 --> 00:30:17,006
The Bodhisattva of True Freedom ,

211
00:30:17,196 --> 00:30:21,144
by the profound practice of the Great Wisdom,

212
00:30:21,264 --> 00:30:25,022
understands that the body and the five skanda are nothing but vacuity

213
00:30:25,152 --> 00:30:30,151
and through this understanding he helps all those who suffer.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, the phenomena are not different from ku.

215
00:30:34,970 --> 00:30:39,527
Ku is no different from phenomena.

216
00:30:39,987 --> 00:30:42,996
Phenomena become ku.

217
00:30:43,196 --> 00:30:51,305
Ku becomes a phenomenon (form is emptiness, emptiness is form),

218
00:30:51,575 --> 00:30:56,155
the five skanda are phenomena as well.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, all existence has the character of ku,

220
00:31:03,451 --> 00:31:08,429
there is no birth and no beginning,

221
00:31:08,989 --> 00:31:14,707
no purity, no staining, no growth, no decay.

222
00:31:14,977 --> 00:31:20,130
That’s why in ku there is neither form nor skanda,

223
00:31:20,939 --> 00:31:26,080
no eyes, no ears, no nose, no tongue, no body, no consciousness ;

224
00:31:26,491 --> 00:31:32,397
there is no color, sound, smell, taste, touch, or object of thought;

225
00:31:32,527 --> 00:31:37,915
there is neither knowledge, nor ignorance, nor illusion, nor cessation of suffering,

226
00:31:38,225 --> 00:31:43,283
there is no knowledge, no profit, no non-profit.

227
00:31:43,413 --> 00:31:48,649
For the Bodhisattva, thanks to this Wisdom that leads beyond,

228
00:31:48,969 --> 00:31:54,622
there is no fear or dread.

229
00:31:54,882 --> 00:32:02,024
All illusion and attachment are removed

230
00:32:02,504 --> 00:32:11,116
and he can grasp the ultimate end of life, Nirvana.

231
00:32:12,325 --> 00:32:16,407
All the Buddhas of the past, present and future.

232
00:32:16,497 --> 00:32:21,489
can attain to the understanding of this Supreme Wisdom.

233
00:32:21,569 --> 00:32:25,021
that delivers suffering, the Satori,

234
00:32:25,081 --> 00:32:30,329
by this incantation, incomparable and unparalleled, authentic,

235
00:32:30,432 --> 00:32:36,163
which removes all suffering and allows to find the reality, the true ku :

236
00:32:38,113 --> 00:32:52,170
"Go, go, go together beyond the beyond on the bank of the satori. »

237
00:33:01,240 --> 00:33:14,420
No matter how many beings there are, I vow to save them all.

238
00:33:14,600 --> 00:33:23,095
No matter how many passions there are, I vow to overcome them all.

239
00:33:25,635 --> 00:33:34,292
No matter how many Dharma there are, I vow to acquire them all.

240
00:33:35,112 --> 00:33:43,700
No matter how perfect a Buddha is, I vow to become one.

241
00:33:44,085 --> 00:33:48,247
May the merits of this recitation

242
00:33:48,247 --> 00:33:52,768
penetrate all beings everywhere,

243
00:33:52,768 --> 00:33:57,538
so that all of us sentient beings

244
00:33:57,538 --> 00:34:03,129
can realize the Buddha’s path together.

245
00:34:05,332 --> 00:34:16,597
All Buddhas past, present and future in the ten directions.

246
00:34:17,517 --> 00:34:26,629
All Bodhisattvas and Patriarchs

247
00:34:27,869 --> 00:34:38,142
The Sutra of the "Wisdom that goes beyond".

248
00:34:53,546 --> 00:34:58,216
Sanpai!

249
00:37:25,590 --> 00:37:28,060
Phew!

250
00:37:46,750 --> 00:37:48,965
Hello everyone!

251
00:37:57,955 --> 00:38:03,310
I hope you’re happy that the pandemic is over!

252
00:38:32,700 --> 00:38:34,600
Hola Paola!
