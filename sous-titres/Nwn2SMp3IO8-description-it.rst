Zen e sofrologia: le posture fondamentali e la loro influenza sul cervello

Il Maestro Kosen è stato invitato al congresso 2013 della Federazione delle Scuole Professionali di Sofrologia (www.sophro.fr).

Spiega le quattro posture fondamentali dello Zen e la loro influenza sul cervello. Ritorna alle fonti del rilassamento dinamico. Insiste sull’importanza dei tre pilastri della postura, della respirazione e dello stato d’animo nella meditazione buddista, in particolare la pratica dello Zen, lo zazen.
