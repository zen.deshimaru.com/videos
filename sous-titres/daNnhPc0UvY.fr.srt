1
00:00:05,860 --> 00:00:08,580
Deshimaru, je sais que je vais bientôt mourir.

2
00:00:08,820 --> 00:00:17,320
Le temps est venu de vous ordonner moine pour transmettre l'enseignement de Bodhidarma dans un nouveau pays.

3
00:00:31,020 --> 00:00:32,820
Je veux aider Dieu.

4
00:00:33,100 --> 00:00:34,860
Je veux aider le Christ.

5
00:00:43,260 --> 00:00:48,000
Je veux aider le vrai Dieu.

6
00:00:48,240 --> 00:00:52,760
Je veux revenir au vrai dieu.

7
00:00:53,360 --> 00:01:00,360
40 ans après l'arrivée de Maître Deshimaru en Europe.

8
00:01:00,800 --> 00:01:04,000
Dieu est très bas.

9
00:01:04,320 --> 00:01:09,040
Dieu est fatigué.

10
00:02:59,140 --> 00:03:03,060
Si nous pouvons revenir à l'histoire du Zen...

11
00:03:04,380 --> 00:03:09,520
Tout a commencé en Inde et ensuite : que s'est-il passé ?

12
00:03:09,720 --> 00:03:13,340
Comment ça s'est passé
de l'Inde au Japon ?

13
00:03:13,640 --> 00:03:19,020
Inde : méditation très profonde.

14
00:03:19,480 --> 00:03:22,340
Mais trop traditionnelle.

15
00:03:24,380 --> 00:03:29,980
Le yoga est fini, en Inde.

16
00:03:30,720 --> 00:03:36,160
Certaines personnes ont un niveau spirituel élevé, mais la plupart, pas du tout.

17
00:03:36,760 --> 00:03:43,440
Mais, le yoga, fini.

18
00:03:43,760 --> 00:03:48,240
La terre y est trop vieille.

19
00:03:48,940 --> 00:03:53,660
Bodhidarma a fait venir le zazen d'Inde en Chine.

20
00:03:53,900 --> 00:03:58,280
Mais il n'y a plus de zen en Inde.

21
00:03:58,860 --> 00:04:04,880
Ensuite, Dogen a fait venir le zazen de Chine au Japon.

22
00:04:05,160 --> 00:04:11,360
Voulez-vous dire que le sol est trop vieux en Asie, au Japon, en Chine ou en Inde ?

23
00:04:11,740 --> 00:04:14,740
Oui, l'Inde est trop vieille.

24
00:04:14,960 --> 00:04:17,700
La Chine aussi est trop vieille.

25
00:04:18,280 --> 00:04:20,280
Le zen est terminé en Chine.

26
00:04:20,700 --> 00:04:23,940
Le Japon est trop habitué au zen.

27
00:04:24,680 --> 00:04:27,240
Les Japonais ne s'intéressent qu'aux cérémonies...

28
00:04:27,680 --> 00:04:30,380
L'essence du zen leur manque.

29
00:04:36,160 --> 00:04:48,320
Pensez-vous que l'esprit du Zen a tendance à décliner ?

30
00:04:48,640 --> 00:04:50,620
Oui, oui, oui !

31
00:04:51,040 --> 00:04:58,720
Pensez-vous que l'esprit du zen puisse s'installer en France, comme la graine d'une fleur ?

32
00:04:59,040 --> 00:05:01,120
Oui !

33
00:06:24,680 --> 00:06:30,080
Pourquoi avez-vous choisi de vous installer en France ?

34
00:06:30,400 --> 00:06:37,000
La France est mieux placée qu'un autre pays pour comprendre le zen.

35
00:06:37,520 --> 00:06:44,300
Le zazen y est plus facile à comprendre.

36
00:06:45,120 --> 00:06:46,700
Grâce à la philosophie.

37
00:06:46,980 --> 00:06:48,980
Aux idées.

38
00:06:49,300 --> 00:06:52,060
Très profondes.

39
00:06:52,520 --> 00:06:57,980
Par exemple, Montaigne :

40
00:06:58,420 --> 00:07:03,900
"La plus grande chose du monde, c'est de savoir être à soi.".

41
00:07:05,880 --> 00:07:09,640
Descartes, Bergson...

42
00:07:10,140 --> 00:07:14,080
Grands esprits, philosophes...

43
00:07:14,800 --> 00:07:16,840
Ils auraient pu comprendre le zen.

44
00:07:18,200 --> 00:07:25,740
Ils ne connaissaient pas le zen, mais leur pensée est la même...

45
00:07:26,460 --> 00:07:29,760
Pourquoi n'êtes-vous pas allé en Amérique ?

46
00:07:30,280 --> 00:07:33,840
J'ai d'abord voulu introduire le zen aux États-Unis.

47
00:07:34,220 --> 00:07:41,180
Mais ils veulent utiliser le zen pour les affaires, pour l'industrie, la psychologie, la psychanalyse...

48
00:07:41,540 --> 00:07:44,180
Ils veulent l'instrumenter.

49
00:07:44,340 --> 00:07:49,020
Ce n'est pas le vrai zen. Ce n'est pas profond.

50
00:09:08,270 --> 00:09:14,100
Avant, l'éducation était bonne, mais à l'époque moderne, elle n'est plus que moyenne.

51
00:09:14,100 --> 00:09:23,740
L'essence est perdue.

52
00:09:24,560 --> 00:09:33,700
Nous ne cherchons que la moyenne...

53
00:09:34,380 --> 00:09:41,600
Seule l'essence est nécessaire pour une éducation forte.

54
00:09:43,380 --> 00:09:49,960
On distingue la droite de la gauche...

55
00:09:51,040 --> 00:09:54,260
Mais nous devons accepter les contradictions.

56
00:09:54,600 --> 00:09:59,720
Ce n'est qu'en abandonnant notre ego que nous pouvons trouver notre véritable individualité.

57
00:10:03,920 --> 00:10:13,100
Notre plus haute personnalité, c'est lorsque nous suivons la vérité cosmique, le cosmos.

58
00:10:13,500 --> 00:10:19,480
Nous pouvons trouver notre véritable ego, fort.

59
00:10:20,580 --> 00:10:25,680
Vous voulez rendre les gens forts ?

60
00:10:26,540 --> 00:10:32,060
Oui, équilibrés et forts, petit à petit.

61
00:10:32,800 --> 00:10:41,600
Nous devons trouver la véritable austérité.

62
00:12:46,000 --> 00:12:57,680
L'essence du Zen est "Mushotoku", sans but.

63
00:12:59,200 --> 00:13:05,520
Toutes les religions ont un objet.

64
00:13:06,400 --> 00:13:10,560
Vous priez pour devenir riche ou heureux.

65
00:13:11,440 --> 00:13:17,040
Mais si nous avons un but, il n'est pas fort.

66
00:13:18,400 --> 00:13:30,000
Nous ne devons avoir aucun but.

67
00:13:31,680 --> 00:13:38,000
Vous ne pouvez pas vous concentrer, sinon.

68
00:13:39,360 --> 00:13:47,680
Voulez-vous dire qu'une bonne réalisation doit être spontanée ?

69
00:13:48,480 --> 00:13:56,080
Nous devons revenir à des conditions normales.

70
00:13:57,040 --> 00:14:01,440
Le vrai Satori, transcendantal,
la conscience...

71
00:14:02,560 --> 00:14:07,520
est de revenir à des conditions normales.

72
00:14:09,440 --> 00:14:18,080
Les gens sont anormaux à l'époque moderne.

73
00:14:19,120 --> 00:14:27,120
Seul le vrai dieu, par exemple, le Christ ou Bouddha, avait des conditions normales pour les humains.

74
00:14:27,920 --> 00:14:33,900
La première condition nécessaire est-elle la détermination de chacun ?

75
00:14:34,340 --> 00:14:39,500
Et la seconde, d'abandonner cette détermination ?

76
00:14:39,760 --> 00:14:45,940
Oui, l'individualité est nécessaire, et nous devons l'abandonner.

77
00:14:46,320 --> 00:14:50,180
Nous devons accepter les contradictions.

78
00:14:50,920 --> 00:14:56,560
Il semble que vous fumez beaucoup...

79
00:14:57,540 --> 00:15:03,260
Pas tant que ça !

80
00:15:04,220 --> 00:15:10,620
Le zen vous a-t-il rendu assez fort pour accepter quoi que ce soit ?

81
00:15:11,140 --> 00:15:15,360
Il faut tout accepter !

82
00:15:15,920 --> 00:15:20,240
Il n'est pas si important de se soucier de sa vie.

83
00:15:20,540 --> 00:15:25,960
Les Européens ont trop d'égoïsme.

84
00:15:26,400 --> 00:15:31,840
Vous devez tout accepter, et aller au-delà !

85
00:15:32,180 --> 00:15:36,960
Mais le zen n'est pas une ascèse, ni une mortification.

86
00:15:37,920 --> 00:15:41,460
Nous devons vivre la vie cosmique.

87
00:15:41,680 --> 00:15:45,900
Nous ne devons pas fixer de limites.

88
00:15:46,320 --> 00:15:48,780
Sinon, la vie devient étroite.

89
00:18:11,940 --> 00:18:15,640
Sur l'éducation des enfants...

90
00:18:15,980 --> 00:18:27,960
Dans les temps modernes, nous ne nous concentrons que sur l'aspect intellectuel de l'éducation.

91
00:18:28,360 --> 00:18:35,880
Il faut aussi voir le côté physique, la tension musculaire juste.

92
00:18:38,600 --> 00:18:45,740
Nous devons également nous concentrer sur les aspects banals de la vie quotidienne.

93
00:18:46,100 --> 00:18:50,560
La concentration et la posture physique sont très importantes.

94
00:18:50,960 --> 00:18:59,200
Nous ne devons pas être avachis en mangeant, par exemple.

95
00:18:59,740 --> 00:19:04,200
Il est essentiel de se concentrer sur la vie quotidienne.

96
00:19:04,620 --> 00:19:10,920
Même notre posture pendant le sommeil est importante !

97
00:19:11,860 --> 00:19:19,460
Les enseignants ne prêtent pas assez attention à la posture physique.

98
00:19:19,800 --> 00:19:23,920
Ils ne s'intéressent qu'au côté intellectuel.

99
00:19:24,220 --> 00:19:27,679
Pourquoi les enfants ne sont-ils pas bien élevés ?

100
00:19:28,260 --> 00:19:30,680
L'éducation est-elle trop facile ?

101
00:19:31,200 --> 00:19:36,100
Nous devenons des animaux, dans les temps modernes !

102
00:19:37,160 --> 00:19:46,380
Tout est trop facile...

103
00:19:46,660 --> 00:19:50,040
Nous conduisons des voitures et ne voulons plus marcher...

104
00:19:50,420 --> 00:20:00,160
L'éducation familiale est également trop facile. C'est une erreur.

105
00:20:01,360 --> 00:20:09,479
De la nourriture chaude... Trop doux !

106
00:20:10,600 --> 00:20:19,320
Notre corps devient faible.

107
00:20:21,540 --> 00:20:26,180
Recherchons-nous la liberté des animaux ?

108
00:20:26,540 --> 00:20:28,540
Les animaux sauvages sont forts.

109
00:20:28,940 --> 00:20:40,340
Les animaux captifs et bien nourris dans les zoos sont faibles.

110
00:20:40,680 --> 00:20:42,680
Ils manquent d'activité.

111
00:20:45,200 --> 00:20:47,700
Les animaux sauvages sont forts !

112
00:20:56,140 --> 00:20:59,100
L'enseignement est important.

113
00:20:59,360 --> 00:21:01,100
Mais ce n'est pas suffisant.

114
00:21:01,420 --> 00:21:06,720
Nous avons également besoin d'un corps fort et d'une sagesse supérieure.

115
00:23:45,260 --> 00:23:50,540
Vous avez apporté en Europe le fruit du zen pour que les gens puissent le goûter.

116
00:23:50,820 --> 00:24:05,540
L'Inde, la Chine, le Japon ont la véritable essence de la culture spirituelle.

117
00:24:07,480 --> 00:24:18,000
Nous devons maintenant fusionner les civilisations asiatique et européenne.

118
00:24:23,660 --> 00:24:31,000
Le zen est l'essence même de la culture asiatique.

119
00:24:31,760 --> 00:24:39,580
Si vous pratiquez le zen, vous comprendrez les civilisations asiatiques.

120
00:24:40,660 --> 00:24:55,280
Y aura-t-il une renaissance spirituelle grâce à la rencontre de la tradition occidentale et du zen ?

121
00:24:55,620 --> 00:24:59,780
Je pense que oui. Je l'espère.

122
00:25:00,380 --> 00:25:04,780
Il est important que tout le monde devienne Dieu.

123
00:25:05,120 --> 00:25:07,120
Comment y parvenir ?

124
00:25:07,400 --> 00:25:11,320
Si vous pratiquez zazen, vous pouvez devenir Dieu.

125
00:25:11,660 --> 00:25:21,300
Le vrai dieu est-il le dieu naturel en nous ?

126
00:25:21,520 --> 00:25:29,520
Oui, chacun doit créer le vrai dieu.

127
00:25:36,600 --> 00:25:40,360
La philosophie zen est très large.

128
00:25:40,800 --> 00:25:44,980
Vous devez accepter les contradictions !

129
00:25:45,420 --> 00:25:47,440
La vie et la mort, par exemple ?

130
00:25:47,680 --> 00:25:50,400
Oui, la vie et la mort !
