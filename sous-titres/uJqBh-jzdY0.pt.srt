1
00:00:06,180 --> 00:00:09,540
Eu não me importo com o Zen. 

2
00:00:09,840 --> 00:00:12,100
É apenas mais um pedaço de lixo da igreja. 

3
00:00:12,480 --> 00:00:17,520
Existem milhares de mestres zen nos Estados Unidos. 

4
00:00:17,820 --> 00:00:20,880
Todo mundo quer ser um mestre zen. 

5
00:00:21,220 --> 00:00:24,680
E Zen é toda a raiva, 

6
00:00:25,280 --> 00:00:28,820
e eu não estou interessado. 

7
00:00:29,320 --> 00:00:31,960
Por que você acha que é toda a raiva? 

8
00:00:32,260 --> 00:00:36,240
Não, agora está fora de moda! 

9
00:00:37,300 --> 00:00:40,060
Na Argentina, todo mundo compra um pequeno jardim zen. 

10
00:00:40,340 --> 00:00:42,720
Nós o colocamos no escritório, em sua sala de estar. 

11
00:00:43,020 --> 00:00:45,740
É lindo. É divertido. É divertido. 

12
00:00:46,100 --> 00:00:49,000
Mas o Zen é inútil! 

13
00:00:53,900 --> 00:00:57,360
O que você acha da expressão de Krishnamurti: 

14
00:00:57,640 --> 00:01:01,060
"Não deixe ninguém ficar entre você e a verdade"? 

15
00:01:01,380 --> 00:01:07,140
Se existe uma discrepância entre você e a verdade, não é a verdade. 

16
00:01:11,480 --> 00:01:16,780
Ele quis dizer que estava sempre contra a maré. 

17
00:01:17,040 --> 00:01:21,960
Ele havia se separado de toda a sociedade teológica, que queria tomá-lo como referência. 

18
00:01:22,300 --> 00:01:24,260
Você admira isso nele? 

19
00:01:24,540 --> 00:01:31,440
Sim Minha mãe, quando estava grávida de mim, era discípula de Krishnamurti. 

20
00:01:31,760 --> 00:01:37,500
Um dia, em uma conferência, ela sentiu um êxtase, um satori. 

21
00:01:37,760 --> 00:01:42,620
Ela disse para si mesma: "meu filho vai ser um santo". 

22
00:01:43,020 --> 00:01:50,060
Eu realmente gosto de Krishnamurti, ele era uma pessoa extraordinária. 

23
00:01:51,640 --> 00:01:56,940
Sai Baba? Eu não sei, eu não o conheço. 

24
00:01:57,360 --> 00:02:02,020
Ele é o único professor que eu já ouvi dizer que ele era Deus. 

25
00:02:02,320 --> 00:02:04,680
Mas todo mundo é Deus! 

26
00:02:05,740 --> 00:02:11,540
Um verdadeiro mestre não precisa dizer "eu sou Deus". 

27
00:02:11,940 --> 00:02:16,200
Mas você tem que dizer aos outros que você é Deus. 

28
00:02:16,500 --> 00:02:22,740
Então o homem diz: "Eu sou Deus. Me dê seu dinheiro!" 

29
00:02:23,060 --> 00:02:27,960
E o fato de ele fazer truques como fazer relógios aparecerem, adivinhem? 

30
00:02:28,240 --> 00:02:32,940
Eu não sei Eu não sei Eu não sei Sim, talvez ele tenha uma força. Sim, é possível. 

31
00:02:33,080 --> 00:02:34,400
Isso é possível? Sim, é possível. 

32
00:02:34,680 --> 00:02:37,040
Existem magos que dizem que é ilusionismo. 

33
00:02:37,320 --> 00:02:40,820
Também é possível que tenha sido um ardil. 

34
00:02:40,980 --> 00:02:48,180
Eu acho que todas as religiões, todas as igrejas, são contra o homem. 

35
00:02:48,280 --> 00:02:50,280
Todo mundo, sem distinção? 

36
00:02:51,740 --> 00:02:54,760
Também budismo, todo mundo. 

37
00:02:55,020 --> 00:02:57,820
Estas são igrejas. 

38
00:02:58,400 --> 00:03:00,400
Eu não gosto disso 

39
00:03:02,740 --> 00:03:08,100
O verdadeiro Zen de Deshimaru não é uma igreja, é uma escola. 

40
00:03:08,480 --> 00:03:10,060
É outra coisa. 

41
00:03:10,380 --> 00:03:12,380
Como você pode dizer a diferença? 

42
00:03:12,900 --> 00:03:16,740
A igreja, são os japoneses! 

43
00:03:20,060 --> 00:03:26,000
Numa igreja, as pessoas procuram um caminho espiritual. 

44
00:03:26,400 --> 00:03:33,240
E depois de dez anos, eles estão procurando um diploma, em uma hierarquia, um poder sobre os outros. 

45
00:03:33,560 --> 00:03:42,620
É uma coisa terrível que mata a verdade. 

46
00:03:43,160 --> 00:03:45,680
Eu pratico zazen. 

47
00:03:46,040 --> 00:03:49,820
Eu pratico os ensinamentos dos Budas, que são muito simples. 

48
00:03:50,120 --> 00:03:54,520
Para mim, é um tesouro da humanidade. 

49
00:03:56,500 --> 00:03:58,640
Zazen [meditação sentada]. 

50
00:03:58,960 --> 00:04:02,180
Kinhin. [Ande devagar] 

51
00:04:02,700 --> 00:04:04,700
Sampaï. 

52
00:04:04,900 --> 00:04:09,020
Genmai [refeição da manhã] e samu [concentração no trabalho]. 

53
00:04:09,360 --> 00:04:10,880
É muito simples 

54
00:04:11,180 --> 00:04:12,880
Mas sempre muda. 

55
00:04:13,140 --> 00:04:16,000
Você sempre adiciona coisas novas. 

56
00:04:16,320 --> 00:04:18,340
Estou aprendendo sim 

57
00:04:18,480 --> 00:04:27,580
Porque acho que gosto de aprender e ter mais sabedoria para evoluir. 

58
00:04:27,880 --> 00:04:29,480
Me questione. 

59
00:04:29,800 --> 00:04:33,660
Gosto de ser um discípulo mais do que um mestre. 

60
00:04:34,100 --> 00:04:35,740
Eu gosto dos dois. 

61
00:04:36,120 --> 00:04:40,540
Sou um bom mestre, mas sou um aluno muito ruim. 

62
00:04:40,820 --> 00:04:42,780
Muito indisciplinado. 

63
00:04:43,200 --> 00:04:46,160
Você sempre foi assim? Incluído com Deshimaru? 

64
00:04:46,400 --> 00:04:47,140
Sim 

65
00:04:47,420 --> 00:04:48,840
E ele te corrigiu? 

66
00:04:49,080 --> 00:04:50,700
Sim, ele me ajudou muito. 

67
00:04:50,980 --> 00:04:54,240
Ele me colocou no comando, isso me acalmou um pouco. 

68
00:04:58,060 --> 00:05:00,280
"O que eu vou fazer com Stéphane?" 

69
00:05:00,560 --> 00:05:04,180
"Aqui, eu estou no comando dele!" 

70
00:05:08,920 --> 00:05:11,860
Você se lembra do dia em que conheceu Deshimaru? 

71
00:05:12,120 --> 00:05:12,620
Sim 

72
00:05:12,920 --> 00:05:14,920
Como foi 

73
00:05:15,280 --> 00:05:16,820
Eu não posso dizer isso. 

74
00:05:17,060 --> 00:05:19,240
Eu estava no Zazen e ele estava atrás de mim com o kyosaku. 

75
00:05:22,920 --> 00:05:28,320
Mais tarde comprei flores no metrô a caminho do dojo. 

76
00:05:28,640 --> 00:05:33,060
Na entrada do dojo, digo: "Tenho flores para o mestre". » 

77
00:05:33,400 --> 00:05:36,080
E depois do Zazen, eles me ligam. 

78
00:05:36,340 --> 00:05:38,540
"O mestre pede por você." 

79
00:05:38,780 --> 00:05:42,860
Eu vou para o quarto dele, onde ele está sentado. 

80
00:05:43,060 --> 00:05:46,760
"Quem me trouxe essas flores?" 

81
00:05:47,040 --> 00:05:47,800
Eu 

82
00:05:48,660 --> 00:05:51,580
"Bom, bom, bom." 

83
00:05:54,280 --> 00:05:58,080
Você se lembra do último dia em que o viu? 

84
00:05:58,580 --> 00:05:59,920
Ele estava morto. 

85
00:06:05,280 --> 00:06:07,280
Seu corpo seria queimado. 

86
00:06:07,520 --> 00:06:09,160
Tchau! 

87
00:06:10,320 --> 00:06:12,320
E o último dia em que você o viu vivo? 

88
00:06:13,860 --> 00:06:19,800
Então ele foi ao Japão pela última vez e disse "adeus!". 

89
00:06:20,200 --> 00:06:23,680
No dia em que Deshimaru morreu, você chorou? 

90
00:06:24,360 --> 00:06:34,420
Quando ele morreu, eu estava no dojo, sentado em zazen … 

91
00:06:37,260 --> 00:06:40,780
Alguém entra no dojo e diz: 

92
00:06:41,060 --> 00:06:45,840
"Teremos que continuar sem Deshimaru." 

93
00:06:46,100 --> 00:06:48,900
Eu morava em zazen. 

94
00:06:49,200 --> 00:06:50,560
Eu sempre estava diante dele. 

95
00:06:50,900 --> 00:06:53,500
Eu sempre fui um pilar para ele. 

96
00:06:53,940 --> 00:06:57,120
Foi assim que experimentei a morte dele. 

97
00:06:57,420 --> 00:07:02,200
Eu tive que ficar forte pelos outros. 

98
00:07:02,840 --> 00:07:11,040
Depois fui direto ao Japão para a cerimônia de cremação. 

99
00:07:13,100 --> 00:07:23,920
Houve uma cerimônia com muitos professores, com toda a igreja japonesa. Enorme. 

100
00:07:24,660 --> 00:07:31,060
E estava em uma caixa, com muitas flores. 

101
00:07:31,500 --> 00:07:37,790
Seu rosto muito bonito, muito sorridente. 

102
00:07:38,100 --> 00:07:50,620
Quando ele estava vivo, sempre que usava chapéu ou nariz falso, ele se tornava muito cômico. 

103
00:07:50,940 --> 00:07:54,380
Era o mesmo lá, com as flores ao redor. 

104
00:07:54,720 --> 00:07:59,520
Fizemos a cerimônia, permaneci muito concentrado e imóvel. 

105
00:07:59,820 --> 00:08:05,400
Então nós colocamos a caixa em um carro. 

106
00:08:05,880 --> 00:08:09,720
Para quatro discípulos. 

107
00:08:09,840 --> 00:08:20,280
Eu fui o último e empurro a caixa em um carrinho. 

108
00:08:20,600 --> 00:08:27,160
No último momento, aperto o dedo no peito. 

109
00:08:29,180 --> 00:08:37,700
Comecei a chorar e não consegui parar por duas ou três horas. 

110
00:08:38,020 --> 00:08:42,220
Saiu e eu chorei e chorei. 

111
00:08:42,520 --> 00:08:45,560
Todo mundo olhou para mim porque ninguém estava chorando assim. 

112
00:08:46,040 --> 00:08:51,440
Chorei sem parar na frente da família e dos outros participantes. 

113
00:08:52,280 --> 00:08:57,500
Eu não conseguia me conter. 

114
00:09:00,560 --> 00:09:05,720
Até que vi a fumaça saindo da chaminé. 

115
00:09:19,379 --> 00:09:27,799
Morto - vivo, tudo está vivo, tudo está morto. 

116
00:09:28,840 --> 00:09:33,500
Ainda estamos vivos, mas estamos mortos. 

117
00:09:33,800 --> 00:09:42,220
Quando você morre, você vê sua vida passando rapidamente. 

118
00:09:42,540 --> 00:09:48,580
Temos observado toda a nossa vida agora. 

119
00:10:10,010 --> 00:10:13,620
Zazen é o estado normal. 

