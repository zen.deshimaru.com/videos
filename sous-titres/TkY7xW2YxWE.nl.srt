1
00:00:00,200 --> 00:00:03,460
Soms kun je dat alleen in zen vinden,

2
00:00:03,780 --> 00:00:06,680
als we een tijdje samen zijn,

3
00:00:06,920 --> 00:00:08,960
dat het mensen aan medeleven ontbreekt.

4
00:00:09,180 --> 00:00:11,600
Ik denk dat je soms de manier waarop je naar de dingen kijkt moet veranderen.

5
00:00:11,980 --> 00:00:16,400
In plaats van te denken, "Hé, dat is de andere die me beroofd heeft!"

6
00:00:17,000 --> 00:00:20,680
denkend, "wat moet ik hier verdomme mee doen?"

7
00:00:21,180 --> 00:00:23,160
En probeer hem om te draaien.

8
00:00:24,360 --> 00:00:27,900
Dat is heel oprecht.

9
00:00:28,300 --> 00:00:30,340
Omdat mensen zichzelf zijn.

10
00:00:30,600 --> 00:00:32,560
Ze proberen geen rol te spelen:

11
00:00:32,760 --> 00:00:36,900
"Ik ben in orde, ik ben in orde, ik ben in orde…"

12
00:00:38,240 --> 00:00:41,760
Er zijn vast en zeker momenten dat we van streek raken.

13
00:00:42,040 --> 00:00:45,280
En waar onze reacties opvallen.

14
00:00:45,600 --> 00:00:50,780
Voor mij is het belangrijk dat we ons daar niet vestigen.

15
00:00:51,040 --> 00:00:52,880
Niet om je in het idee te wentelen:

16
00:00:53,200 --> 00:00:55,700
"In een sesshin, zijn er geen aardige mensen."

17
00:00:55,960 --> 00:00:59,320
Als ik bij Sesshin kom,

18
00:00:59,880 --> 00:01:04,060
mijn idee is om alles om te zetten in een positieve.

19
00:01:04,600 --> 00:01:07,340
Ik weet niet of ik het de hele tijd kan!

20
00:01:08,360 --> 00:01:10,960
Voor mij, alle moeilijkheden die zich voordoen,

21
00:01:11,180 --> 00:01:16,960
zijn uitnodigingen om verder te gaan in zijn gemoedstoestand.

22
00:01:17,360 --> 00:01:24,680
Ik zou zelfs zeggen dat als we onterecht worden bekritiseerd,

23
00:01:24,980 --> 00:01:27,600
kunnen we er iets positiefs van maken.

24
00:01:27,900 --> 00:01:33,580
Het dwingt je bijvoorbeeld om jezelf minder serieus te nemen.

25
00:01:37,620 --> 00:01:40,340
Wat ik leuk vind aan deze sangha,

26
00:01:40,640 --> 00:01:42,860
is dat we proberen wat meer naar elkaar te luisteren.

27
00:01:43,140 --> 00:01:45,720
Omdat het tijd kost om meer naar elkaar te luisteren,

28
00:01:46,040 --> 00:01:50,460
om rekening te houden met een aantal andere gezichtspunten.

29
00:01:50,760 --> 00:01:53,580
We weten dat er dingen mis zijn,

30
00:01:53,980 --> 00:01:58,120
maar we streven altijd naar verbetering.

31
00:01:59,400 --> 00:02:07,120
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

