1
00:00:00,100 --> 00:00:01,460
Meu nome é Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
Meu monge zen é chamado Ryurin. 

3
00:00:03,880 --> 00:00:05,580
Eu também sou um calígrafo. 

4
00:00:05,900 --> 00:00:09,560
Ensino caligrafia chinesa, que descobri no Zen. 

5
00:00:09,840 --> 00:00:13,860
Eu aprendi com um monge zen. 

6
00:00:14,320 --> 00:00:18,260
Eu gosto de compartilhar caligrafia com todos. 

7
00:00:18,860 --> 00:00:22,540
Eu faço muito trabalho nas escolas. 

8
00:00:22,900 --> 00:00:27,360
Faço caligrafia com crianças em oficinas. 

9
00:00:27,700 --> 00:00:31,020
Também para adultos e pessoas com deficiência. 

10
00:00:31,380 --> 00:00:33,300
Eles gostam muito. 

11
00:00:33,780 --> 00:00:39,220
Meus alunos mais novos têm 6 anos e meu aluno mais velho tem 90 anos. 

12
00:00:39,760 --> 00:00:42,800
Eu também ensino chinês. 

13
00:00:43,180 --> 00:00:44,840
Esse é o meu negócio principal. 

14
00:00:45,140 --> 00:00:48,020
Essa caligrafia é simplesmente "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
É uma caligrafia feita no atual estilo semi-cursivo. 

16
00:00:54,580 --> 00:01:06,760
O pincel é curto, vai de pontos a linhas, para formar um caractere chinês ou japonês. 

17
00:01:08,420 --> 00:01:11,320
Isso é "Reiki" (靈氣), a energia espiritual. 

18
00:01:11,620 --> 00:01:13,880
É uma técnica de cura. 

19
00:01:14,220 --> 00:01:20,260
Continuamos essa tradição porque vem de um mestre que praticou muita caligrafia. 

20
00:01:20,500 --> 00:01:23,920
Ele também praticou sumi-e, (墨 絵), ele era um pintor. 

21
00:01:24,280 --> 00:01:27,580
Eu realmente aprecio as pinturas do Mestre Deshimaru, ele era um artista. 

22
00:01:27,840 --> 00:01:31,020
Ele pintou e fez caligrafia muito bem. 

23
00:01:31,440 --> 00:01:37,860
Sua caligrafia é muito forte, densa e enérgica. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) é o nome de um sutra que é cantado. 

25
00:01:43,700 --> 00:01:52,480
Eu tracei o cacheado: mais ou menos como espaguete. 

26
00:01:52,780 --> 00:01:56,440
Os personagens são muito vinculados e simplificados. 

27
00:01:56,780 --> 00:02:00,800
Também gostamos disso na prática zen. 

28
00:02:01,240 --> 00:02:08,040
Os praticantes de zen realmente gostaram dessa escrita, que é bastante gratuita e expressiva. 

29
00:02:09,260 --> 00:02:10,740
É uma arte energética. 

30
00:02:11,020 --> 00:02:14,840
É praticado com todo o corpo, em conexão com a respiração. 

31
00:02:15,240 --> 00:02:21,900
Em conexão com sua disponibilidade da mente, sua consciência. 

32
00:02:22,360 --> 00:02:24,720
Portanto, é uma arte energética. 

33
00:02:25,000 --> 00:02:28,120
Vamos nos acalmar, reorientar algo dentro de nós mesmos. 

34
00:02:28,360 --> 00:02:33,240
É uma metamorfose completa, uma alquimia do equilíbrio. 

35
00:02:33,560 --> 00:02:40,040
Horizontalidade, verticalidade, centralização, acoplamento ao desenhar os sinais. 

36
00:02:40,540 --> 00:02:43,120
Disponibilidade à mão. 

37
00:02:43,460 --> 00:02:48,080
Na meditação zen, a presença nas mãos é muito importante. 

38
00:02:48,420 --> 00:02:55,600
Se está parado ou em execução. 

39
00:02:55,940 --> 00:02:57,760
O gesto é diferente em caligrafia. 

40
00:02:58,180 --> 00:03:00,120
Mas há uma presença pelas mãos. 

41
00:03:00,340 --> 00:03:02,320
É a relação mão-cérebro. 

42
00:03:02,600 --> 00:03:04,140
Caligrafia tem uma vida. 

43
00:03:04,440 --> 00:03:05,440
Mesmo que você não saiba disso! 

44
00:03:05,680 --> 00:03:07,980
Alguma caligrafia tocará você, outras não. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) é chinês e é mais taoísta. 

46
00:03:11,740 --> 00:03:15,820
Significa "nutrir o princípio vital". 

47
00:03:16,540 --> 00:03:23,440
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

