1
00:00:00,090 --> 00:00:05,980
During a Zen ceremony, we do sanpai :
three prostrations.

2
00:00:06,320 --> 00:00:09,145
A first accelerando of bells

3
00:00:09,385 --> 00:00:11,607
indicates to monks and nuns

4
00:00:11,727 --> 00:00:14,660
that they need to unfold their zagu.

5
00:00:16,080 --> 00:00:19,905
A prostration begins with a salute in gassho.

6
00:00:20,015 --> 00:00:22,562
Then we put our knees on the floor.

7
00:00:22,612 --> 00:00:24,685
The head is placed on the ground.

8
00:00:24,780 --> 00:00:26,620
Hands up.

9
00:00:26,750 --> 00:00:30,450
We get up without hands
by leaning on the toes.

10
00:00:30,660 --> 00:00:32,780
For each prostration,

11
00:00:32,870 --> 00:00:36,235
a bell ring tells us to come down,

12
00:00:36,295 --> 00:00:39,432
another, to go back up.

13
00:00:39,572 --> 00:00:41,676
For the third prostration,

14
00:00:41,846 --> 00:00:46,270
two bell rings to go down
indicate that it is the last one.

15
00:00:47,150 --> 00:00:50,770
During the last ascent,

16
00:00:50,930 --> 00:00:53,444
you catch your zagu by the right corner.

17
00:00:53,525 --> 00:00:56,470
We pull it up at the same time as we pull up.

18
00:00:56,520 --> 00:00:59,375
We folded it in half, then in half again.

19
00:00:59,515 --> 00:01:03,710
These explanations are intended for
those who practice on Zoom

20
00:01:03,820 --> 00:01:05,875
and who have never done sanpai

21
00:01:05,955 --> 00:01:08,702
so that they can practice the ceremony.

22
00:01:08,872 --> 00:01:13,000
zazenathome.org
