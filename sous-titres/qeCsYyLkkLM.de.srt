1
00:00:11,160 --> 00:00:13,720
Hallo, wie geht es Ihnen?

2
00:00:16,560 --> 00:00:21,280
Die Haltung, gelehrt von Meister Deshimaru...

3
00:00:25,000 --> 00:00:28,460
Er nannte es die Zen-Methode...

4
00:00:31,920 --> 00:00:35,640
Beginnen Sie damit, eine stabile Basis zu haben.

5
00:00:39,600 --> 00:00:43,160
Das bedeutet, dass beide Knie den Boden berühren.

6
00:00:47,480 --> 00:00:50,540
Und Sie legen den Damm auf das Zafu [Kissen].

7
00:00:53,860 --> 00:00:58,940
Wenn Sie den vollen Lotus machen, stellen Sie ein Bein voran.

8
00:01:04,220 --> 00:01:07,300
Dann die andere.

9
00:01:07,900 --> 00:01:12,140
Wenn man eine gute Stabilität hat, kann man den Körper wirklich loslassen.

10
00:01:12,170 --> 00:01:15,220
Wie sagt man so schön, lassen Sie den Verstand-Körper los.

11
00:01:22,460 --> 00:01:24,320
sagte Meister Deshimaru:

12
00:01:25,020 --> 00:01:28,640
"Zen ist der neue Humanismus für das 21. Jahrhundert".

13
00:01:29,560 --> 00:01:31,440
"Es muss immer frisch sein."

14
00:01:31,480 --> 00:01:34,000
"Wenn Sie wahres Zazen praktizieren,"

15
00:01:34,060 --> 00:01:36,600
"Das Kinn nach innen gestreckt,"

16
00:01:37,060 --> 00:01:39,980
Die "gestreckte Wirbelsäule".

17
00:01:40,580 --> 00:01:42,580
"nur Zazen,"

18
00:01:43,620 --> 00:01:46,270
"Der Rest ist nicht mehr so wichtig."

19
00:01:46,320 --> 00:01:49,600
Zu Beginn von Zazen gibt es drei Glocken.

20
00:01:52,040 --> 00:01:56,060
Und dann, wenn es Verwandte gibt, geht die Meditation weiter,

21
00:02:00,530 --> 00:02:03,580
Es gibt zwei Glocken.

22
00:02:15,980 --> 00:02:18,940
Das Knie des Vorderbeins gut strecken.

23
00:02:22,900 --> 00:02:28,340
Drücken Sie mit der Wurzel der Großzehe des Vorderfußes auf den Boden.

24
00:02:34,580 --> 00:02:36,580
Entspannen Sie Ihre Schultern.

25
00:02:38,260 --> 00:02:41,480
Am Ende des Verfalls,

26
00:02:44,100 --> 00:02:46,100
wir atmen ein,

27
00:02:47,560 --> 00:02:51,900
und wir machen einen kleinen Schritt vorwärts.

28
00:02:56,820 --> 00:03:00,060
Fangen Sie wieder an, atmen Sie aus...

29
00:03:01,360 --> 00:03:05,180
Sie legen Ihr ganzes Körpergewicht auf das vordere Bein.

30
00:03:10,920 --> 00:03:13,760
Der Daumen der linken Hand

31
00:03:16,120 --> 00:03:19,960
ballen wir es zu einer Faust.

32
00:03:22,400 --> 00:03:25,300
Dann nehmen wir die rechte Hand

33
00:03:26,960 --> 00:03:29,480
und wir decken die linke Hand ab.

34
00:03:36,140 --> 00:03:40,040
Zazen durch Zoomen zu machen ist sehr praktisch...

35
00:03:40,260 --> 00:03:43,710
insbesondere in diesen schwierigen Zeiten.

36
00:03:48,280 --> 00:03:53,440
Aber normalerweise ergänzt es die Praxis in einem echten Dojo.

37
00:03:58,860 --> 00:04:03,340
Natürlich widmen wir diese Praxis, die wir gemeinsam durchführen

38
00:04:12,540 --> 00:04:19,560
An all die Menschen, die sich der Heilung und Rettung anderer widmen.

39
00:04:34,720 --> 00:04:36,720
Hallo, Toshi. Hallo.

40
00:04:36,780 --> 00:04:38,720
Guten Morgen, Meister.

41
00:04:40,680 --> 00:04:43,940
Ah, da ist es! Hallo!

42
00:04:46,100 --> 00:04:48,100
Hallo, Ingrid.

43
00:04:51,840 --> 00:04:54,640
Das ist Grabriela, aber welche?

44
00:04:57,460 --> 00:05:00,520
Ah, ich habe Sie mit Ihrer Brille nicht erkannt!

45
00:05:06,020 --> 00:05:09,160
Ich bin die Übersetzerin, erkennen Sie mich nicht?
