1
00:01:31,170 --> 00:01:35,220
In Soto Zen, das wir praktizieren, sagen wir: "Suche keine große Erleuchtung". 

2
00:01:35,560 --> 00:01:38,407
Erleuchtung an sich existiert nicht. 

3
00:01:38,407 --> 00:01:40,170
Warum sollte sie existieren? 

4
00:01:40,780 --> 00:01:45,720
Man muss die Natur und die Dinge so betrachten, wie sie sind, um die Wahrheit zu erkennen. 

5
00:01:46,200 --> 00:01:49,663
Die Beleuchtung ist absolut natürlich. 

6
00:01:49,663 --> 00:01:52,780
Es ist keine besondere Bedingung. 

7
00:01:53,320 --> 00:01:59,400
Im Zen gibt es keine besonderen Bedingungen für die Erleuchtung, es sei denn, es liegt eine Geisteskrankheit vor. 

8
00:01:59,700 --> 00:02:04,760
Es gibt keine besondere Erleuchtung an sich und man sollte keine suchen. 

9
00:02:05,540 --> 00:02:07,433
Viele Meister bestehen darauf. 

10
00:02:07,433 --> 00:02:08,681
Das ist extrem wichtig. 

11
00:02:08,681 --> 00:02:11,000
Erleuchtung ist in jedem Moment, in jedem Zazen. 

12
00:02:11,320 --> 00:02:15,580
Ich habe mit einem Spezialisten für Psychopharmaka gesprochen … 

13
00:02:15,790 --> 00:02:20,700
entwickelt, um besondere Bewusstseinszustände zu erreichen. 

14
00:02:21,130 --> 00:02:24,686
Er behauptete, Zazen sei ein Mittel, um Erleuchtung zu erlangen. 

15
00:02:24,686 --> 00:02:25,889
Das stimmt überhaupt nicht. 

16
00:02:26,350 --> 00:02:31,379
Zazen ist die Beleuchtung selbst. 

17
00:02:32,410 --> 00:02:33,254
Dogen sagte: 

18
00:02:33,254 --> 00:02:36,269
"Ich ging nach China und traf einen echten Meister." 

19
00:02:37,390 --> 00:02:41,729
"Ich habe daraus gelernt, dass die Augen horizontal und die Nase vertikal sind." 

20
00:02:42,370 --> 00:02:45,810
"Und dieser sollte sich weder von sich selbst noch von anderen täuschen lassen." 

21
00:03:44,750 --> 00:03:46,750
und jeder Muskel im Bauch 

22
00:03:47,269 --> 00:03:52,358
sollte entspannt sein, um den Eindruck zu erwecken, dass der Darm belastet. 

23
00:04:09,010 --> 00:04:15,420
Wenn wir uns in diesem Moment vollständig auf die Haltung konzentrieren, können wir die Gedanken loslassen 

24
00:04:15,850 --> 00:04:17,850
ohne dort anzuhalten. 

25
00:04:25,810 --> 00:04:28,914
Das Schönste, was Sie mit Ihrem Körper tun können, ist Zazen. 

26
00:04:28,914 --> 00:04:30,419
Dies ist der Glanz der Natur. 

27
00:04:30,849 --> 00:04:33,728
Es inspiriert Ruhe, Harmonie, Kraft. 

28
00:04:33,728 --> 00:04:36,539
Alles ist in der Haltung von Zazen enthalten 

29
00:04:39,969 --> 00:04:43,447
Im Sommercamp versuche ich, das Üben zu fördern. 

30
00:04:43,447 --> 00:04:46,111
Die Atmosphäre im Dojo, die Körperhaltungen. 

31
00:04:46,111 --> 00:04:47,909
Eine Person, die das stört 

32
00:04:50,540 --> 00:04:53,940
Mit ihrer persönlichen Geschichte bin ich bereit, sie zu töten. 

33
00:04:57,120 --> 00:04:59,120
Es ist die einzige Regel, die ich kenne: Mitgefühl. 

34
00:05:00,600 --> 00:05:02,477
Sensei zeigte sein Kyosaku: 

35
00:05:02,477 --> 00:05:04,160
"der Stab des Mitgefühls!" 

36
00:05:04,660 --> 00:05:11,080
Manchmal traf er 20 Schüsse hintereinander. 

37
00:05:14,440 --> 00:05:19,640
Er würde sagen: "Sensei, sehr mitfühlend!" 

38
00:05:26,280 --> 00:05:32,940
Wenn einem Schüler bei seiner Meditation geholfen werden soll, indem er mit einem Stock auf die Schulter geschlagen wird, bittet er darum. 

39
00:05:33,180 --> 00:05:36,800
Weil er spürt, wie seine Aufmerksamkeit nachlässt. 

40
00:05:37,140 --> 00:05:41,360
Wenn Sie sich schläfrig fühlen. 

41
00:05:41,580 --> 00:05:46,300
Oder wenn Sie Komplikationen spüren, die Ihr Gehirn trüben. 

42
00:05:46,540 --> 00:05:49,520
Nicht mehr so ​​leise. 

43
00:05:49,940 --> 00:05:54,600
Wenn Sie sich schläfrig fühlen oder wenn Ihr Geist unruhig ist … 

44
00:06:01,240 --> 00:06:03,240
Gasshô! 

45
00:06:21,580 --> 00:06:23,580
Zazen fährt während fort 

46
00:06:23,840 --> 00:06:25,233
für mehrere Stunden jeden Tag. 

47
00:06:25,233 --> 00:06:25,929
Und manchmal Tag und Nacht. 

48
00:06:26,180 --> 00:06:30,291
Aber er wird beim Gehen durch eine Meditation unterbrochen. 

49
00:06:30,291 --> 00:06:33,489
Das heißt, der innere Zustand bleibt, 

50
00:06:34,280 --> 00:06:38,799
aber die Haltung ist anders. 

51
00:06:39,940 --> 00:06:44,040
Die ganze Aufmerksamkeit richtet sich auf den Daumen. 

52
00:06:44,600 --> 00:06:47,500
Jeder Atemzug entspricht einem halben Schritt. 

53
00:06:58,880 --> 00:07:05,020
Die Augen sind 3 Meter vor dem Boden. 

54
00:07:13,340 --> 00:07:18,580
Es ist das gleiche Atmen wie in Zazen während der Meditation in sitzender Haltung. 

55
00:07:20,740 --> 00:07:28,300
Zen-Atmung ist das Gegenteil von normaler Atmung. 

56
00:07:28,560 --> 00:07:30,940
Wenn Sie jemanden zum Atmen bitten, 

57
00:07:31,240 --> 00:07:34,600
Der Reflex ist, tief durchzuatmen. 

58
00:07:35,060 --> 00:07:38,860
Im Zen werden wir uns konzentrieren 

59
00:07:39,080 --> 00:07:42,740
vor allem beim Ausatmen. 

60
00:07:43,040 --> 00:07:46,780
Das Ausatmen ist lang, ruhig und tief. 

61
00:07:54,360 --> 00:07:56,160
Es ist im Boden verankert. 

62
00:07:56,160 --> 00:07:58,860
Es kann bis zu ein, zwei Minuten dauern. 

63
00:07:59,080 --> 00:08:02,940
Abhängig von seiner Konzentration und Ruhe. 

64
00:08:03,280 --> 00:08:09,600
Es kann sehr weich und unmerklich sein. 

65
00:08:09,860 --> 00:08:15,640
Wir untersuchen diese Atmung zuerst, indem wir darauf bestehen, wie eine Kuh, die muht. 

66
00:08:38,020 --> 00:08:44,640
Am Ende der Ausatmung lassen wir die Inspiration automatisch erfolgen. 

67
00:08:45,880 --> 00:08:49,640
Wir entspannen alles, öffnen die Lunge. 

68
00:08:50,300 --> 00:08:55,380
Wir lassen die Inspiration automatisch kommen. 

69
00:08:55,820 --> 00:08:58,689
Wir setzen nicht viel Absicht in die Inspiration. 

70
00:08:59,600 --> 00:09:03,129
Wenn Sie dies tun, werden Sie verstehen 

71
00:09:04,550 --> 00:09:06,550
Das Atmen ist alles. 

72
00:09:07,910 --> 00:09:09,910
Es ist ein Kreis. 

73
00:09:10,070 --> 00:09:12,070
Oft zeichnen Meister einen Kreis. 

74
00:09:12,560 --> 00:09:14,560
Es geht ums Atmen. 

75
00:09:15,040 --> 00:09:18,880
Diese Atmung hatte viel Einfluss, 

76
00:09:19,340 --> 00:09:23,200
vor allem in den Kampfkünsten. 

77
00:09:23,480 --> 00:09:29,160
In den japanischen Kampfkünsten wird Zen-Atmung verwendet. 

78
00:09:29,580 --> 00:09:34,840
Viele große Kampfkunstmeister wie Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
praktizierte Zen und wurde Schüler großer Meister. 

80
00:09:39,940 --> 00:09:45,480
Und haben die Lehre von Zen und Atmung an ihre Kampfkunst angepasst. 

81
00:09:45,820 --> 00:09:53,860
Am Ende ist die Atmung jenseits der Techniken und völlig frei. 

82
00:09:58,080 --> 00:10:03,340
Die Lehre Buddhas an seine Jünger ist sehr einfach. 

83
00:10:03,640 --> 00:10:09,440
Es basiert auf den grundlegenden Handlungen des Menschen. 

84
00:10:09,660 --> 00:10:11,260
Wie man sich bewegt. 

85
00:10:11,480 --> 00:10:14,120
Wie man isst, wie man atmet. 

86
00:10:14,440 --> 00:10:15,836
Wie man denkt. 

87
00:10:15,836 --> 00:10:20,860
Und schließlich, wie man sich hinsetzt, um seine Göttlichkeit zu erkennen. 

88
00:11:24,940 --> 00:11:28,702
Genmai ist seit Shakyamuni Buddha das Essen der Mönche. 

89
00:11:28,702 --> 00:11:31,800
Wir mischen das Gemüse, das wir zur Hand haben, mit Reis. 

90
00:11:32,280 --> 00:11:39,440
Wir kochen sehr lange, mit viel Aufmerksamkeit und Konzentration. 

91
00:11:39,780 --> 00:11:42,730
Es ist ein Essen, das vom Buddha weitergegeben wurde. 

92
00:11:42,730 --> 00:11:45,920
In China und Japan essen wir das gleiche Essen. 

93
00:11:47,660 --> 00:11:49,980
Es ist wirklich der Körper des Buddha. 

94
00:11:50,280 --> 00:11:53,080
Wenn wir gleich essen gehen, 

95
00:11:53,440 --> 00:11:55,560
Sie müssen sich die Frage stellen: 

96
00:11:55,900 --> 00:11:59,460
"Warum werde ich essen?" 

97
00:11:59,680 --> 00:12:01,580
Ein Tier oder ein Lümmel 

98
00:12:01,960 --> 00:12:05,680
Ich werde mich nicht wundern und auf seinem Teller auf und ab springen. 

99
00:12:06,040 --> 00:12:09,820
Natürlich ist es ums Überleben, aber wofür überleben? 

100
00:12:10,060 --> 00:12:13,520
Das steht in den Sutras, die während der Mahlzeiten gesungen werden. 

101
00:12:13,780 --> 00:12:15,240
"Warum essen wir?" 

102
00:12:15,520 --> 00:12:17,591
"Woher kommt dieses Essen? 

103
00:12:17,800 --> 00:12:18,860
Wer hat es vorbereitet? " 

104
00:12:19,180 --> 00:12:21,140
"Wer hat Reis und Gemüse angebaut?" 

105
00:12:21,400 --> 00:12:26,140
"Wer hat gekocht?" 

106
00:12:26,500 --> 00:12:29,960
Wir widmen das Essen und danken all diesen Menschen. 

107
00:12:30,240 --> 00:12:32,140
Und bei vollem Bewusstsein, 

108
00:12:32,620 --> 00:12:37,340
Ich beschließe, dieses Essen für zu essen 

109
00:12:37,780 --> 00:12:44,620
Die Evolution, die ich auf dieser Erde, in dieser materiellen Welt, vollbracht habe. 

110
00:12:52,360 --> 00:12:56,900
EMS ist um 10:30 Uhr. 6 Personen für die Küche. 

111
00:13:01,080 --> 00:13:04,080
Zwei Leute für den großen Tauchgang. 

112
00:13:05,940 --> 00:13:10,340
Drei Personen für den Dienst. 

113
00:13:19,600 --> 00:13:23,580
Samu macht nicht besonders Mauerwerk, fegt … 

114
00:13:24,190 --> 00:13:27,989
oder den Abwasch machen. 

115
00:13:28,540 --> 00:13:30,540
Samu ist alles Action 

116
00:13:30,760 --> 00:13:32,939
(Ein Buch zu schreiben ist ein Samu) 

117
00:13:35,320 --> 00:13:37,320
durchgeführt ohne 

118
00:13:37,640 --> 00:13:38,860
Absicht 

119
00:13:39,180 --> 00:13:40,160
versteckt 

120
00:13:44,950 --> 00:13:47,249
keine geheimen Motive. 

121
00:13:47,980 --> 00:13:50,048
Es ist wie dein Job im Alltag. 

122
00:13:50,048 --> 00:13:52,289
Sie können es als persönliche Bereicherung nehmen 

123
00:13:52,839 --> 00:13:57,749
oder als eine Art Geschenk, das Sie sich und allen anderen gleichzeitig geben. 

124
00:13:59,320 --> 00:14:02,460
Manchmal denke ich darüber nach 

125
00:14:03,400 --> 00:14:05,459
Einige Leute nehmen es vielleicht als lästige Pflicht. 

126
00:14:05,920 --> 00:14:09,390
Aber ich wette, die in Zazen verletzt sind. 

127
00:14:16,400 --> 00:14:19,550
Als Zen-Nonne 

128
00:14:23,040 --> 00:14:26,180
Welche Haltung sollten Sie bei der Arbeit einnehmen? 

129
00:14:27,080 --> 00:14:30,100
Ich weiß nicht, ich habe nie gearbeitet! 

130
00:14:33,960 --> 00:14:37,060
Müssen wir aus ihm einen Samu machen? 

131
00:14:38,500 --> 00:14:39,520
Ja, das ist der Punkt. 

132
00:14:39,740 --> 00:14:41,100
Ich habe Mist geredet, oder? 

133
00:14:41,300 --> 00:14:47,500
Als ich jünger war, hatte ich einen Job als Hausmeister. 

134
00:14:48,090 --> 00:14:54,920
Ich putzte die Treppe und holte jeden Morgen um 5 Uhr den Müll raus. 

135
00:14:55,140 --> 00:15:00,619
Die Treppe war ein Hurra. Da war Hundescheiße. 

136
00:15:00,920 --> 00:15:05,360
Ich dachte: "Aber das ist nicht das Dojo oder Zen." 

137
00:15:05,760 --> 00:15:09,760
"Macht nichts. Für mich ist es wie ein Tempel." 

138
00:15:10,320 --> 00:15:14,160
Ich habe es wie ein Samurai gemacht. 

139
00:15:14,480 --> 00:15:17,040
Und Sensei fragte mich: 

140
00:15:17,420 --> 00:15:20,080
"Stéphane, ich möchte, dass du das Dojo sauber machst." 

141
00:15:20,400 --> 00:15:23,660
"Ja, das werde ich. Freitag bin ich frei." 

142
00:15:25,440 --> 00:15:27,920
"Ich werde es nächsten Freitag tun." 

143
00:15:28,580 --> 00:15:29,660
Er sagte zu mir: 

144
00:15:29,940 --> 00:15:32,500
"Nein, ich möchte, dass du es jeden Tag machst." 

145
00:15:33,120 --> 00:15:34,272
Dann sagte er: 

146
00:15:34,520 --> 00:15:37,440
"Ein Tag ohne Arbeit, ein Tag ohne Essen." 

147
00:15:37,860 --> 00:15:40,720
"Aber es hat nichts mit Essen zu tun!" 

148
00:15:41,000 --> 00:15:44,440
"Ein Tag ohne Arbeit, ein Tag ohne Mushotoku zu essen!" 

149
00:15:44,780 --> 00:15:47,380
Es war ein sehr origineller Ausdruck: 

150
00:15:47,700 --> 00:15:50,380
"Du musst Mushotoku essen!" 

151
00:15:54,760 --> 00:15:57,540
Es bedeutete nichts! 

152
00:15:58,180 --> 00:16:02,500
Zu sagen, du bist ein Mönch, Samu zu praktizieren ist Revolution. 

153
00:16:02,880 --> 00:16:05,880
Wir müssen die Revolution machen, das habe ich immer gesagt! 

154
00:16:06,920 --> 00:16:10,657
Aber ich weiß nicht, wie man Molotow-Cocktails benutzt! 

155
00:16:11,000 --> 00:16:14,100
In seiner Arbeit muss man also Samu üben. 

156
00:16:14,400 --> 00:16:19,780
Um alle Probleme der Welt zu lösen, müssen Sie dies nur finden. 

157
00:16:20,100 --> 00:16:22,060
Die Menschen haben nicht mehr den Geist der Samu. 

158
00:16:22,520 --> 00:16:23,920
Sie essen kein Mushotoku mehr. 

159
00:16:24,160 --> 00:16:25,580
Sie essen Hamburger und trinken Cola. 

160
00:16:26,180 --> 00:16:28,880
Also, wer auch immer zu dir sagt: 

161
00:16:29,580 --> 00:16:31,580
Aber es ist halb sieben, du bist der einzige, der noch übrig ist! 

162
00:16:31,880 --> 00:16:33,140
Du sagst ihm: 

163
00:16:33,360 --> 00:16:36,160
"Ich, ich übe Samu!" 

164
00:16:45,640 --> 00:16:47,640
"Weil ich Mushotoku essen will!" 

165
00:16:49,780 --> 00:16:51,200
"Was ist Mushotoku?" 

166
00:16:51,440 --> 00:16:52,780
"Ich will auch was!" 

167
00:16:57,040 --> 00:17:01,200
Wir müssen Menschen unterrichten, ohne Angst zu haben. 

168
00:17:03,180 --> 00:17:06,660
Sei stolz, meine Jünger und Mönche zu sein! 

169
00:17:07,000 --> 00:17:09,000
Und Jünger von Deshimaru zu sein. 

