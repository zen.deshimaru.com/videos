1
00:00:00,080 --> 00:00:00,920
Question :

2
00:00:01,140 --> 00:00:05,380
Je ressens parfois en méditation zazen une sensation très agréable.

3
00:00:05,680 --> 00:00:09,880
De picotements un peu dans tout le corps.

4
00:00:10,340 --> 00:00:15,040
Comme de petites décharges électriques.

5
00:00:15,440 --> 00:00:17,880
Réponse de Maître Kosen :

6
00:00:18,400 --> 00:00:22,420
Toutes les sensations peuvent être bonnes ou mauvaises.

7
00:00:23,080 --> 00:00:28,760
Une même sensation peut paraître bonne ou mauvaise.

8
00:00:29,220 --> 00:00:33,520
L’important c’est que ce soit agréable.

9
00:00:34,180 --> 00:00:37,240
Que ça vous détende.

10
00:00:37,500 --> 00:00:42,000
C’est en général lié à la circulation du sang.

11
00:00:42,340 --> 00:00:50,640
En posture du lotus, le sang circule peu dans les jambes.

12
00:00:51,020 --> 00:00:55,960
Il circule beaucoup plus dans la partie abdominale.

13
00:00:56,400 --> 00:01:00,840
La partie supérieure du corps, y compris le cerveau, est mieux irriguée.

14
00:01:01,100 --> 00:01:05,240
Ça peut provoquer une sensation agréable.

15
00:01:05,500 --> 00:01:09,660
C’est bien, parce que c’est une pensée positive.

16
00:01:10,060 --> 00:01:13,580
Quand on se sent bien, c’est une une conscience positive.

17
00:01:13,820 --> 00:01:16,240
Ça influence également notre vie.

18
00:01:16,480 --> 00:01:18,160
Maître Deshimaru disait toujours :

19
00:01:18,380 --> 00:01:20,500
« Le zazen n’est pas une mortification. »

20
00:01:20,840 --> 00:01:26,260
Mais quand on doit faire face à son corps,

21
00:01:26,620 --> 00:01:31,480
certains moments sont désagréables ou douloureux.

22
00:01:31,860 --> 00:01:34,940
Quand on se sent bien c’est super !

