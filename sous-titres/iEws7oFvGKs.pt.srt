1
00:00:06,180 --> 00:00:11,550
Tenho praticado zazen por mais da metade da minha vida.

2
00:00:14,890 --> 00:00:20,170
E é muito raro que eu esteja em perfeito equilíbrio.

3
00:00:25,650 --> 00:00:29,445
Portanto, há muito poucas chances

4
00:00:29,605 --> 00:00:33,910
que eu percebo totalmente o Buda?

5
00:00:38,110 --> 00:00:40,910
O Buda também tinha as costas ruins!

6
00:00:47,340 --> 00:00:54,790
Estamos tentando nos harmonizar com algo perfeito e vivo.

7
00:00:55,110 --> 00:00:59,740
O que é perfeito em nós é que estamos vivos.

8
00:01:01,860 --> 00:01:06,660
Há pessoas que estão pensando em fazer
de modificação genética.

9
00:01:13,480 --> 00:01:22,120
Diz-se em fábulas que o Buda tinha
as trinta e duas marcas de perfeição.

10
00:01:23,400 --> 00:01:27,440
Ou seja, não tinha falhas.

11
00:01:41,780 --> 00:01:48,180
É bom o suficiente para se aproximar,
para imitar seu deus.

12
00:01:51,530 --> 00:01:55,780
Mas eu acho que o Buda era um homem como nós.
e que ele tinha falhas.

13
00:02:02,690 --> 00:02:05,685
A primeira falha é que ele está morto.

14
00:02:05,685 --> 00:02:08,710
Se ele não tivesse tido uma falha, não teria morrido.

15
00:02:11,630 --> 00:02:15,510
Ele morreu, ele fez chorar todos os seus discípulos.

16
00:02:18,733 --> 00:02:21,406
Estamos fazendo zazen com um corpo imperfeito.

17
00:02:21,686 --> 00:02:24,100
Como um ser imperfeito.

18
00:02:26,130 --> 00:02:30,650
Mas o que é ótimo o suficiente é nos dar a direção

19
00:02:32,670 --> 00:02:39,020
celular, muscular, mental.

20
00:02:39,670 --> 00:02:43,320
A direção a ser seguida ou imitada.

21
00:02:48,450 --> 00:02:51,840
Os muçulmanos, por exemplo, não usam Buda.

22
00:02:51,910 --> 00:02:55,900
Eles dizem: "Não temos uma estátua,
nenhuma imagem de Deus".

23
00:02:57,440 --> 00:02:59,165
Eu acho que é muito bom.

24
00:03:02,695 --> 00:03:07,520
Mas sabemos a direção a seguir para imitar a Deus.

25
00:03:11,630 --> 00:03:18,510
Para fazer tudo o que pudermos para tornar nosso ser divino.

26
00:03:22,120 --> 00:03:24,730
Mas nós não somos perfeitos
e nós vamos morrer.

27
00:03:27,000 --> 00:03:29,375
E há alguns que não são perfeitos…

28
00:03:29,375 --> 00:03:31,932
e outras que NÃO SÃO PERFEITAS.

29
00:03:34,552 --> 00:03:39,830
Muitas vezes, as pessoas perfeitas não precisam…

30
00:03:40,390 --> 00:03:43,060
Eu, quando estava fazendo música…

31
00:03:46,120 --> 00:03:49,685
Eu conto esta história com freqüência.

32
00:03:50,445 --> 00:03:53,590
…eu tinha um produtor.

33
00:03:54,070 --> 00:03:58,640
Ele era um multimilionário italiano…

34
00:04:00,860 --> 00:04:04,415
Não importava o que ele estava fazendo.

35
00:04:05,265 --> 00:04:10,450
Ele era muito rico e queria fundar uma editora.

36
00:04:13,480 --> 00:04:17,123
Ele se ofereceu para produzir meu registro.

37
00:04:19,480 --> 00:04:23,820
E eu não sei porquê, ele passou por aqui
no Templo de la Gendronnière.

38
00:04:24,780 --> 00:04:28,520
Durante os dias de preparação para o acampamento de verão.

39
00:04:28,750 --> 00:04:33,050
Ele disse: "Eu queria ver o que você está fazendo!"

40
00:04:33,520 --> 00:04:36,215
Mostramos a ele as regras do dojo:

41
00:04:36,425 --> 00:04:39,417
"Você entra assim, depois se senta."

42
00:04:39,877 --> 00:04:42,420
Ele se senta: lótus completo direto.

43
00:04:46,690 --> 00:04:48,450
Lótus impecável.

44
00:04:48,570 --> 00:04:51,190
"Bem, estou vendo que você já tem tudo resolvido!"

45
00:04:51,190 --> 00:04:54,080
"Zazen vai estar dentro de uma hora."

46
00:04:55,870 --> 00:04:58,580
"Você pode visitar o templo."

47
00:04:58,760 --> 00:05:02,870
Ele fez seu zazen por uma hora.

48
00:05:04,300 --> 00:05:06,060
- "Não foi difícil?"

49
00:05:06,190 --> 00:05:08,720
- "Não, não, foi muito bom!"

50
00:05:11,590 --> 00:05:14,655
Eu juro que é verdade!

51
00:05:15,314 --> 00:05:18,080
E ele nunca mais fez zazen em sua vida.

52
00:05:19,420 --> 00:05:21,920
Foi ele quem me ensinou uma lição.

53
00:05:27,150 --> 00:05:32,332
Há dois ou três anos, meu neto, sobre este alto…

54
00:05:32,632 --> 00:05:37,460
disse-me: "Avô, mostre-me zazen!"

55
00:05:40,840 --> 00:05:45,540
Eu o trouxe para a sala
onde eu faço zazen online através do Zoom.

56
00:05:47,140 --> 00:05:50,780
Eu disse: "Sente-se aqui, ponha suas pernas assim"."

57
00:05:51,000 --> 00:05:52,019
Ele está enfatizando.

58
00:05:52,115 --> 00:05:55,950
Ele se parece com Stephen Zeisler [o discípulo de Deshimaru].

59
00:05:58,050 --> 00:06:00,410
- "Você se coloca assim."

60
00:06:00,570 --> 00:06:04,130
Estou corrigindo sua postura.

61
00:06:04,400 --> 00:06:06,485
Ele tem uma ótima postura.

62
00:06:06,575 --> 00:06:08,750
Eu disse: "É isso aí, é isso aí!"

63
00:06:08,910 --> 00:06:10,250
- "Obrigado, vovô!"

64
00:06:15,755 --> 00:06:17,270
Ele o tem.
65
00:06:17,350 --> 00:06:19,645
Ele sabe o que é zazen.

66
00:06:22,430 --> 00:06:25,660
Eu não sei que pergunta eu estava respondendo…

67
00:06:27,345 --> 00:06:30,995
Existe a plena realização de Buda?

68
00:06:33,520 --> 00:06:37,090
Tudo é realização total do Buda!

69
00:06:38,090 --> 00:06:42,240
Mesmo que sejamos retorcidos, mesmo que não façamos zazen.

70
00:06:43,130 --> 00:06:47,830
É por isso que às vezes é bom dizer que o zazen é inútil.

71
00:06:49,410 --> 00:06:55,340
Porque quando você faz zazen em shorts em casa

72
00:06:55,760 --> 00:07:01,180
sem kesa, sem cerimônia, sem amigos,
sem um mestre, sem discípulos,

73
00:07:01,980 --> 00:07:07,360
Você diz para si mesmo "São os mesmos zazens
que eu costumava fazer quando era um novato!"

74
00:07:07,970 --> 00:07:12,030
No começo, quando eu estava em casa e pensava:

75
00:07:12,030 --> 00:07:14,410
"Ah, apetece-me fazer zazen!"

76
00:07:16,521 --> 00:07:17,751
Eu estava pensando…
