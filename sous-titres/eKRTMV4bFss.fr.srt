1
00:00:08,980 --> 00:00:16,230
Bonjour ! Je vais vous expliquer comment vous asseoir pour pratiquer la méditation zen,

2
00:00:17,200 --> 00:00:19,180
appelée zazen.

3
00:00:19,180 --> 00:00:22,829
Le matériel nécessaire c’est très simple, un être humain

4
00:00:23,320 --> 00:00:28,349
et un coussin de méditation. Si vous avez un coussin

5
00:00:29,230 --> 00:00:33,689
plutôt rembourré, c’est très bien et sinon, vous en faites un avec une couverture

6
00:00:35,320 --> 00:00:39,060
ou un sac de couchage par exemple, ce doit être suffisamment ferme.

7
00:00:57,490 --> 00:01:04,120
Je vous explique la posture de méditation zen, la méditation zen.

8
00:01:05,560 --> 00:01:08,940
Avant tout à savoir, que c’est une posture

9
00:01:09,440 --> 00:01:10,880
assise

10
00:01:11,240 --> 00:01:13,440
immobile et silencieuse.

11
00:01:14,120 --> 00:01:16,660
La particularité de cette posture

12
00:01:17,300 --> 00:01:19,300
est que nous la pratiquons

13
00:01:19,400 --> 00:01:21,400
face au mur.

14
00:01:21,800 --> 00:01:27,460
Ici évidemment je me mets face à la caméra pour vous l’enseigner.

15
00:01:27,960 --> 00:01:34,640
Ce qu’il faut faire c’est s’asseoir sur un coussin, tel que je l’expliquais, qui doit être

16
00:01:35,000 --> 00:01:37,000
suffisamment rembourré, suffisamment dur.

17
00:01:37,300 --> 00:01:44,060
Vous asseyez bien au fond du coussin, ne vous asseyez pas sur le bord vous pouvez glisser,

18
00:01:44,440 --> 00:01:48,360
et vous devez, dans la mesure de votre possible, croiser les jambes.

19
00:01:48,820 --> 00:01:56,220
Vous pouvez les croiser comme ceci, comme ceci, comme ceci, ça s’appelle le demi-lotus,

20
00:01:56,660 --> 00:01:59,410
si vous mettez l’autre jambe ici ça s’appelle le lotus.

21
00:02:02,860 --> 00:02:07,620
L’important c’est que vos genoux aillent vers le sol,

22
00:02:08,220 --> 00:02:09,660
d’appuyer,

23
00:02:10,789 --> 00:02:13,420
d’exercer une pression des genoux sur le sol.

24
00:02:13,880 --> 00:02:20,079
Alors, comment faire cela ? Le faire sans aucun effort physique,

25
00:02:20,720 --> 00:02:25,240
rien du tout de muscles. La posture de méditation zen, zazen,

26
00:02:25,900 --> 00:02:27,780
c’est une posture où l’on doit lâcher.

27
00:02:28,300 --> 00:02:33,000
Pour que vos genoux aillent vers le sol

28
00:02:33,920 --> 00:02:37,360
vous allez basculer le bassin légèrement

29
00:02:38,440 --> 00:02:42,220
vers l’avant, le haut du bassin va vers l’avant,

30
00:02:42,760 --> 00:02:46,300
le coccyx va un peu derrière.

31
00:02:46,760 --> 00:02:48,760
Je vous le montre de profil

32
00:02:55,700 --> 00:03:03,040
Mon bassin n’est pas ainsi mis, comme si par exemple, je suis en train de conduire une voiture,

33
00:03:03,260 --> 00:03:08,700
vous voyez là mes genoux montent derrière parce que mon centre de gravité

34
00:03:08,900 --> 00:03:15,700
est passé derrière mon corps. Je veux que mon centre de gravité aille devant et je fais cela avec mon bassin.

35
00:03:15,940 --> 00:03:24,300
Naturellement, le centre de gravité passe devant avec la pesanteur, mes genoux vont vers le sol.

36
00:03:24,300 --> 00:03:30,860
Voilà, c’est ce que vous devez commencer par établir dans la posture,

37
00:03:31,160 --> 00:03:37,040
une assise dans laquelle vos genoux vont vers le sol.

38
00:03:37,480 --> 00:03:40,200
Quand vous avez réalisé cette assise

39
00:03:40,800 --> 00:03:45,500
depuis le bassin qui est légèrement basculé vers l’avant

40
00:03:45,900 --> 00:03:49,280
vous allez redresser le plus possible la colonne vertébrale

41
00:03:50,200 --> 00:03:52,860
jusqu’au sommet de la tête.

42
00:03:54,580 --> 00:04:02,780
On va considérer que la colonne va jusqu’en haut. Comme si je mettais un crochet

43
00:04:05,060 --> 00:04:10,500
et hop, je me redresse. Ce qui veut dire que j’étire la nuque, je rentre le menton,

44
00:04:11,150 --> 00:04:15,699
Voyez je n’ai pas la tête qui tombe devant, je n’ai pas le dos courbé

45
00:04:16,849 --> 00:04:18,849
je suis le plus droit possible.

46
00:04:19,520 --> 00:04:22,000
je vous remontre de profil

47
00:04:23,300 --> 00:04:28,040
Je ne suis pas comme ceci, je ne suis pas comme ceci

48
00:04:28,560 --> 00:04:35,780
ou penché vers l’avant ou je ne sais quoi, je suis le plus droit possible.

49
00:04:38,800 --> 00:04:43,820
Quand vous avez réussi à vous installer bien droit, vous ne bougez plus.

50
00:04:45,320 --> 00:04:47,160
Je continue

51
00:04:47,380 --> 00:04:49,280
la main gauche

52
00:04:49,540 --> 00:04:51,540
les doigts sont réunis

53
00:04:52,250 --> 00:04:57,220
la main droite les doigts sont réunis et je vais superposer mes doigts de la main gauche

54
00:04:58,070 --> 00:05:00,070
sur les doigts de la main droite

55
00:05:01,490 --> 00:05:03,680
Mes pouces se rejoignent

56
00:05:05,730 --> 00:05:08,329
Superposés aux doigts de la main gauche,

57
00:05:09,630 --> 00:05:11,630
je forme une ligne horizontale.

58
00:05:12,690 --> 00:05:20,059
Le tranchant de mes mains depuis tout le poignet jusque là, le tranchant de mes mains

59
00:05:21,030 --> 00:05:23,929
vous allez l’appliquer à la base de l’abdomen

60
00:05:25,280 --> 00:05:27,820
pour ce faire,

61
00:05:29,580 --> 00:05:33,440
utilisez quelque chose qui puisse vous permettre de caler vos mains

62
00:05:34,160 --> 00:05:36,280
de telle façon que vous puissiez

63
00:05:36,700 --> 00:05:38,620
relâcher complètement

64
00:05:38,880 --> 00:05:43,300
la tension des épaules la posture est une posture où il faut lâcher

65
00:05:44,280 --> 00:05:50,089
relâcher les tensions, vous voyez mes coudes sont libres je suis longtemps comme ça ou comme ça,

66
00:05:51,380 --> 00:05:53,940
tranquillement. Mais je ne fais pas d’effort

67
00:05:54,540 --> 00:06:00,940
pour maintenir mes mains, l’effort il est pour maintenir les pouces horizontaux.

68
00:06:01,220 --> 00:06:03,220
une fois que j’ai réalisé

69
00:06:03,750 --> 00:06:05,750
ces points de la posture

70
00:06:06,600 --> 00:06:08,600
je ne bouge plus,

71
00:06:09,160 --> 00:06:11,160
ça c’est la posture physique.

72
00:06:20,060 --> 00:06:27,420
Après cette première partie sur la posture physique, deuxième partie : l’attitude mentale.

73
00:06:28,380 --> 00:06:32,340
Une fois que je suis en posture de méditation zen, zazen

74
00:06:33,620 --> 00:06:37,960
face au mur, qu’est ce que je fais, que faut-il faire?

75
00:06:39,720 --> 00:06:42,260
Dans le silence de cette posture,

76
00:06:43,770 --> 00:06:48,380
vous allez vous rendre compte que vous pensez,

77
00:06:49,350 --> 00:06:51,769
voire que vous n’arrêtez pas de penser

78
00:06:52,290 --> 00:06:57,469
et qu’il y a des milliers de pensées qui viennent, qui viennent, qui viennent.

79
00:06:57,980 --> 00:07:02,420
Vous allez me rendre compte que c’est un phénomène naturel

80
00:07:03,500 --> 00:07:05,500
Vous ne pouvez pas contrôler.

81
00:07:05,940 --> 00:07:11,900
Ce que l’on va faire, pendant la méditation zen, on va laisser passer nos pensées.

82
00:07:12,740 --> 00:07:15,580
c’est à dire que, dans cette immobilité,

83
00:07:16,040 --> 00:07:20,440
on va pouvoir observer que nous pensons,

84
00:07:21,040 --> 00:07:25,960
vous allez observer vos pensées et ces pensées au lieu de les entretenir,

85
00:07:26,360 --> 00:07:32,860
au lieu de les faire croître, vous allez les laisser passer.

86
00:07:33,160 --> 00:07:39,220
Donc il ne s’agit pas de refuser quoi que ce soit, là encore il s’agit de lâcher, laisser passer.

87
00:07:40,080 --> 00:07:42,400
Comment laisser passer à travers de

88
00:07:43,260 --> 00:07:45,260
l’attention, de la concentration,

89
00:07:46,080 --> 00:07:47,860
ici, maintenant,

90
00:07:48,240 --> 00:07:50,240
sur votre respiration.

91
00:07:55,930 --> 00:07:58,680
Respiration dans le zen est une respiration abdominale.

92
00:08:00,460 --> 00:08:08,320
On respire à travers, par la narine et vous respirez doucement,

93
00:08:09,640 --> 00:08:13,780
lentement, évidemment sans bruit, méditation silencieuse.

94
00:08:15,180 --> 00:08:20,180
La caractéristique cette respiration est que vous allez porter votre attention

95
00:08:20,540 --> 00:08:24,120
sur la face de l’expiration.

96
00:08:24,420 --> 00:08:26,420
On inspire

97
00:08:26,620 --> 00:08:33,680
par le nez, tranquillement, soyez généreux, remplissez

98
00:08:34,300 --> 00:08:41,880
et la face de l’expiration quand vous allez sortir tout l’air vous le faites lentement,

99
00:08:42,340 --> 00:08:46,940
lentement, comme pour devenir de plus en plus long

100
00:08:56,700 --> 00:09:00,140
Quand vous êtes à la fin de votre expiration

101
00:09:00,880 --> 00:09:02,380
vous aspirez

102
00:09:02,850 --> 00:09:06,340
et de nouveau vous expirez

103
00:09:07,520 --> 00:09:12,020
et vous mettez plus de temps, lentement, plus profond,

104
00:09:12,200 --> 00:09:15,760
une expiration plus profonde et de nouveau comme les vagues

105
00:09:16,280 --> 00:09:18,280
sur un océan

106
00:09:18,560 --> 00:09:20,560
qui arrivent sur la plage

107
00:09:22,580 --> 00:09:24,580
Tout ceci avec le nez.

108
00:09:30,060 --> 00:09:33,100
Si vous vous concentrez ici et maintenant

109
00:09:33,400 --> 00:09:40,880
sur les points de la posture, les points physiques donc l’attention, une attention ici et maintenant dans votre corps

110
00:09:41,420 --> 00:09:48,960
si vous vous concentrez ici et maintenant, dans le même temps, à ne pas suivre vos pensées,

111
00:09:49,820 --> 00:09:52,640
revenant sur les points que la posture,

112
00:09:52,980 --> 00:09:55,880
si vous vous concentrez ici et maintenant

113
00:09:56,460 --> 00:10:02,320
sur établir une respiration, un va et vient harmonieux et petit à petit

114
00:10:02,900 --> 00:10:06,180
que le temps de l’expiration,

115
00:10:06,800 --> 00:10:10,620
vous sortez l’air devient de plus en plus long

116
00:10:11,600 --> 00:10:16,240
Vous n’aurez plus le temps de penser à ... ce que vous voulez!

117
00:10:16,800 --> 00:10:22,940
Vous êtes ici et maintenant en posture de zazen.

118
00:10:23,520 --> 00:10:30,360
En posture, ni le corps la matière, en posture. Je vous rappelle vous appuyez,

119
00:10:30,660 --> 00:10:34,420
vous exercez une pression des genoux vers le sol,

120
00:10:34,680 --> 00:10:38,840
vous poussez la terre avec les genoux

121
00:10:38,900 --> 00:10:40,980
et vous redressez

122
00:10:41,500 --> 00:10:43,500
le plus possible la colonne vertébrale

123
00:10:44,089 --> 00:10:48,999
jusqu’en haut du crâne comme pour pousser le ciel avec la tête.

124
00:10:49,760 --> 00:10:55,270
La main gauche dans la main droite les pouces horizontaux le tranchant des mains

125
00:10:55,780 --> 00:10:58,440
contre le bas ventre.

126
00:10:58,840 --> 00:11:00,820
Je suis immobile

127
00:11:01,190 --> 00:11:03,160
face au mur

128
00:11:03,170 --> 00:11:05,170
face au mur il n’y a rien à voir spécialement,

129
00:11:05,360 --> 00:11:11,169
ce que je vais plutôt faire c’est tourner mon regard à l’intérieur,

130
00:11:11,959 --> 00:11:13,959
donc les sens,

131
00:11:14,410 --> 00:11:16,959
je ne vais plus les utiliser pendant cette méditation.

132
00:11:17,900 --> 00:11:23,360
Je suis face au mur, il n’y a rien donc à voir, donc c’est comme si

133
00:11:24,080 --> 00:11:28,640
ma vue, ma vision je la prends, je la pose devant moi, je ne m’en occupe plus.

134
00:11:29,020 --> 00:11:33,180
Pendant la méditation, vous allez méditer avec la vue,

135
00:11:34,160 --> 00:11:37,240
45° en diagonale vers le bas

136
00:11:38,300 --> 00:11:46,920
Vous pouvez méditer les yeux fermés, mais si vous gardez les yeux semi-ouverts vous gardez un contact avec la réalité.

137
00:11:47,340 --> 00:11:49,980
Ne partez pas spécialement dans vos pensées.

138
00:11:50,320 --> 00:11:54,380
Comme nous sommes en silence, nous n’utilisons pas non plus l’ouïe,

139
00:11:54,600 --> 00:11:58,980
donc l’audition, il n’y en a pas besoin, il n’y a pas de musique, il n’y a rien

140
00:11:59,260 --> 00:12:03,740
et on a vu déjà que nous sommes immobiles.

141
00:12:04,260 --> 00:12:09,960
Petit à petit, d’un coup on coupe avec les sollicitations du monde extérieur

142
00:12:10,400 --> 00:12:12,680
on fait le vide de tout ça.

143
00:12:13,140 --> 00:12:18,920
La méditation, vous êtes assis physiquement, c’est le corps qui est en posture,

144
00:12:19,910 --> 00:12:26,350
vous vous coupez des sollicitations avec l’extérieur, vous mettez tout en mode avion.

145
00:12:26,920 --> 00:12:33,740
Ne méditez pas avec votre téléphone portable à côté de vous.

146
00:13:09,580 --> 00:13:12,000
Pour terminer un conseil aux débutants,

147
00:13:12,660 --> 00:13:17,740
ce n’est pas évident de se retrouver comme ça, confiné maintenant,

148
00:13:18,140 --> 00:13:21,720
méditer face à un mur,

149
00:13:22,820 --> 00:13:25,940
mais, c’est vraiment une bonne aide

150
00:13:26,220 --> 00:13:28,640
vraiment je vous conseille de le pratiquer,

151
00:13:28,900 --> 00:13:36,940
de l’essayer et vous allez voir qu’au lieu d’entretenir des pensées qui ne sont peut-être pas très lumineuses,

152
00:13:37,260 --> 00:13:43,920
pas très joyeuses, au lieu d’être de mauvaise humeur, au lieu de sentir de la tension, du stress,

153
00:13:44,560 --> 00:13:48,569
tout ceci c’est normal, avec ce travail sur le corps

154
00:13:49,200 --> 00:13:55,500
l’attitude de l’esprit vous lâchez, vous prenez de la distance avec tout ce qui vous arrive

155
00:13:55,940 --> 00:13:58,240
vous allez voir que vous allez vous calmer

156
00:13:58,560 --> 00:13:59,900
physiquement

157
00:14:00,100 --> 00:14:01,480
émotionnellement

158
00:14:01,660 --> 00:14:04,820
la respiration, tout ça, ça va vous aider à lâcher

159
00:14:05,280 --> 00:14:09,880
ça va vous aider profondément, vous et votre entourage si vous vivez accompagné.

160
00:14:10,340 --> 00:14:14,480
Alors ne doutez pas à le pratiquer. Merci.
