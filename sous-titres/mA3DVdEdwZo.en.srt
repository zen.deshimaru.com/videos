1
00:00:02,555 --> 00:00:04,935
We have talked here in the kusen…

2
00:00:05,365 --> 00:00:08,125
in Europe…

3
00:00:14,485 --> 00:00:17,635
about Volition

4
00:00:18,965 --> 00:00:21,545
It’s not Willingness

5
00:00:22,975 --> 00:00:25,785
Volition is something that triggers…

6
00:00:43,405 --> 00:00:46,395
Unconsciously

7
00:00:56,045 --> 00:00:58,085
Then I have explained…

8
00:01:00,835 --> 00:01:03,645
Here in the West

9
00:01:14,945 --> 00:01:16,280
First of all, good day to all

10
00:01:21,100 --> 00:01:23,915
Sharing this zazen at this moment

11
00:01:27,795 --> 00:01:29,465
with great pleasure

12
00:01:41,215 --> 00:01:44,275
We  have managed to organize

13
00:01:44,275 --> 00:01:47,235
and practice in a Summer Camp

14
00:01:50,295 --> 00:01:53,145
despite the complicated events

15
00:01:56,085 --> 00:01:59,065
We are a limited number because you have not come

16
00:02:05,140 --> 00:02:08,085
because of the safety space

17
00:02:16,705 --> 00:02:19,585
We are obliged to keep a distance in the Dojo

18
00:02:22,745 --> 00:02:25,825
We did everything seriously, we followed all the rules

19
00:02:48,715 --> 00:02:50,205
It’s…

20
00:02:52,155 --> 00:02:54,855
a specification of Zen

21
00:02:54,955 --> 00:02:57,875
to follow the rules

22
00:03:02,375 --> 00:03:05,425
"I was not educated like that…I’m not like that…"

23
00:03:12,340 --> 00:03:15,180
In Zen… we follow the rules

24
00:03:20,685 --> 00:03:23,535
The rules that they are where we are

25
00:03:33,205 --> 00:03:35,075
So I have explained that Volition

26
00:03:38,655 --> 00:03:41,555
is a Yin or Yang

27
00:03:47,765 --> 00:03:50,855
When I say to relax the legs, it’s the Yin volition

28
00:03:53,835 --> 00:03:56,215
We release

29
00:04:06,125 --> 00:04:08,765
And by relaxing we even anchor a resonance there

30
00:04:20,465 --> 00:04:23,345
The Yang Volition…

31
00:04:23,345 --> 00:04:26,525
is pressing the earth with your knees

32
00:04:40,595 --> 00:04:43,625
I have told the story

33
00:04:46,475 --> 00:04:47,475
and the reflection

34
00:04:50,305 --> 00:04:53,155
The Yang method from Baso

35
00:04:53,155 --> 00:04:55,895
And the Yin method from Sekito

36
00:05:02,085 --> 00:05:04,855
I will share to you this kusen

37
00:05:22,145 --> 00:05:25,305
This a very popular story

38
00:05:28,265 --> 00:05:31,285
but seen from another point of view than the usual

39
00:05:33,965 --> 00:05:36,905
We talk about Baso who had already received the transmission of the Dharma

40
00:05:42,415 --> 00:05:43,415
Of his master

41
00:05:49,815 --> 00:05:52,885
He continued his master’s teachings and sesshins

42
00:06:05,180 --> 00:06:06,660
He had a curious fixation

43
00:06:12,775 --> 00:06:16,025
He was going to do zazen alone in the Dojo even though it was not the time for zazen

44
00:06:18,775 --> 00:06:21,745
He always wanted to do zazen

45
00:06:28,325 --> 00:06:31,475
One day his master goes to see him

46
00:06:31,475 --> 00:06:34,715
while he was sitted

47
00:06:34,715 --> 00:06:35,715
and he asks him

48
00:06:40,475 --> 00:06:42,915
Great monk,

49
00:06:43,425 --> 00:06:46,645
-Baso was very big-

50
00:06:48,405 --> 00:06:51,495
What are you looking for sitting in zazen?

51
00:06:57,065 --> 00:06:58,325
In other words,

52
00:06:58,865 --> 00:07:01,455
what are you looking for? Do you have a goal?

53
00:07:07,565 --> 00:07:08,565
Do you have a goal beyond…

54
00:07:08,875 --> 00:07:11,805
that is more important than simply sitting down?

55
00:07:20,045 --> 00:07:22,545
This story is very popular

56
00:07:25,745 --> 00:07:28,875
It’s the Yang aspect of volition

57
00:07:31,705 --> 00:07:34,625
I invented the dialog

58
00:07:41,045 --> 00:07:44,175
"-So, do you have a goal?"

59
00:07:44,565 --> 00:07:47,525
"-Yes, I do have a goal." Baso says.

60
00:07:50,435 --> 00:07:53,175
I want to become Buda. I want to be Buda.

61
00:07:56,345 --> 00:07:59,545
So…

62
00:07:59,545 --> 00:08:02,195
I aim

63
00:08:02,195 --> 00:08:05,195
for the centre of the target

64
00:08:08,525 --> 00:08:11,655
and I hit it at every shot

65
00:08:21,915 --> 00:08:22,955
It’s the Yang aspect of zazen

66
00:08:23,355 --> 00:08:26,725
that I spoke about yesterday

67
00:08:33,620 --> 00:08:35,080
I told

68
00:08:39,275 --> 00:08:42,355
how Deshimaru master has taught us when he was young

69
00:08:51,595 --> 00:08:53,855
He created a lot of tension in the Dojo with his kyosaku

70
00:08:53,855 --> 00:08:56,695
It’s the Yang method

71
00:08:59,385 --> 00:09:02,365
As soon as someone moved

72
00:09:02,365 --> 00:09:05,395
or fell asleep

73
00:09:06,385 --> 00:09:09,835
he would hit him

74
00:09:10,035 --> 00:09:12,765
So we were in the summer camp

75
00:09:12,765 --> 00:09:15,915
It was too hot

76
00:09:15,915 --> 00:09:16,945
we were sweating

77
00:09:20,515 --> 00:09:22,925
there was a water stain on the tatami after zazen

78
00:09:32,265 --> 00:09:35,255
I sit down to become a Buddha and that’s it. I put all my energy there

79
00:09:41,125 --> 00:09:43,755
That’s Deshimaru’s method

80
00:09:47,835 --> 00:09:50,825
Practice with all his strength for pleasure

81
00:09:51,435 --> 00:09:54,475
That was what Baso was doing

82
00:10:00,985 --> 00:10:03,985
So the Yin method,

83
00:10:07,255 --> 00:10:10,245
I wanted to say that Baso didn’t have any doubts

84
00:10:13,375 --> 00:10:16,385
It was the same for him to have a goal or not, whether he was Mushotoku

85
00:10:27,485 --> 00:10:30,515
It was the volition. He was in the mood

86
00:10:30,515 --> 00:10:31,515
He had the willingness

87
00:10:37,535 --> 00:10:40,345
Sensei told

88
00:10:41,185 --> 00:10:43,855
it’s better to do less time zazen

89
00:10:52,525 --> 00:10:53,525
but do a strong zazen, like Baso

90
00:10:58,875 --> 00:11:04,825
better than doing a lot of zazen all day,

91
00:11:04,825 --> 00:11:05,825
sleep or think

92
00:11:18,425 --> 00:11:19,425
20 minutes

93
00:11:19,425 --> 00:11:22,335
with all your strength

94
00:11:22,335 --> 00:11:25,315
is very efficient

95
00:11:32,245 --> 00:11:33,245
When we like zazen,

96
00:11:33,415 --> 00:11:36,545
for the pleasure…

97
00:11:42,395 --> 00:11:43,395
we do not proselytize

98
00:11:57,805 --> 00:12:01,235
We only share for pleasure

99
00:12:41,325 --> 00:12:44,205
Let’s go back to the Yang example

100
00:12:44,205 --> 00:12:47,535
relax the legs

101
00:12:47,815 --> 00:12:50,445
when we relax the legs we relax the pelvis

102
00:12:50,445 --> 00:12:53,595
I have the impression of having my weight on the ground

103
00:13:10,585 --> 00:13:13,035
The example I have given

104
00:13:14,165 --> 00:13:17,215
is the time we are going to take a nap

105
00:13:26,135 --> 00:13:29,145
So we relax everything

106
00:13:30,775 --> 00:13:33,425
to go to sleep

107
00:13:35,935 --> 00:13:39,145
Obviously the example of the nap is a metaphor

108
00:13:42,045 --> 00:13:45,005
which refers to zazen

109
00:13:59,545 --> 00:14:02,295
7 centuries before Jesus Christ

110
00:14:09,240 --> 00:14:12,240
a scientist named Pitagoras

111
00:14:14,595 --> 00:14:16,545
said:

112
00:14:18,965 --> 00:14:21,305
keep an eye at night

113
00:14:30,485 --> 00:14:33,115
when you don’t control the night

114
00:14:36,115 --> 00:14:37,455
be careful when you go to sleep

115
00:14:46,755 --> 00:14:48,235
do not fall asleep without checking that moment before falling asleep

116
00:15:02,695 --> 00:15:04,735
zazen is made up of these two aspects

117
00:15:05,885 --> 00:15:08,785
the ying and the yang

118
00:15:11,405 --> 00:15:14,485
expressed by the masters Sekito and Baso

119
00:15:29,195 --> 00:15:32,195
One can be called "Kill the Buda" and the other "Let go the Buda"

120
00:15:41,635 --> 00:15:44,895
The monk Sekito was contemporary. They were monks who lived at the same time.

121
00:15:55,240 --> 00:15:57,825
Baso and Sekito lived at the same time

122
00:16:03,100 --> 00:16:05,420
Many times one’s disciples went to see one and vice versa

123
00:16:10,560 --> 00:16:13,280
At that time, Sekito didn’t have any disciples yet

124
00:16:19,555 --> 00:16:22,115
He was living in the forest…in the mountain

125
00:16:22,445 --> 00:16:25,385
He had himself built

126
00:16:25,385 --> 00:16:28,405
a little house

127
00:16:34,335 --> 00:16:35,335
with wood and straw

128
00:16:44,315 --> 00:16:47,255
in the same mountain that Nangaku master had taught

129
00:16:54,435 --> 00:16:57,825
Sekito told

130
00:16:58,545 --> 00:17:01,475
"I feel protected in this house"

131
00:17:04,635 --> 00:17:07,355
I built it by myself.  It’s my only wealth

132
00:17:10,965 --> 00:17:12,745
But when I finish my meal,

133
00:17:16,045 --> 00:17:19,015
I calmly prepare the nap time

134
00:17:23,655 --> 00:17:26,825
and I repeat it every day

135
00:17:26,825 --> 00:17:27,825
every day

136
00:17:31,695 --> 00:17:34,525
every single day

137
00:17:40,625 --> 00:17:43,415
The experience of the meal and the nap,

138
00:17:43,415 --> 00:17:46,235
is a power of the patriarchs

139
00:17:50,035 --> 00:17:53,055
the one who has not yet finished his meal,

140
00:17:53,055 --> 00:17:56,285
didn’t get to the end of the experience.

141
00:17:56,285 --> 00:17:58,085
He is not yet satisfied.

