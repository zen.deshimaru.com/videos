1
00:00:11,160 --> 00:00:13,720
Olá, como você está?

2
00:00:16,560 --> 00:00:21,280
A postura, ensinada pelo Mestre Deshimaru...

3
00:00:25,000 --> 00:00:28,460
Chamou-lhe o método Zen...

4
00:00:31,920 --> 00:00:35,640
Comece por ter uma base estável.

5
00:00:39,600 --> 00:00:43,160
O que significa que ambos os joelhos estão tocando o chão.

6
00:00:47,480 --> 00:00:50,540
E você coloca o períneo sobre o zafu [almofada].

7
00:00:53,860 --> 00:00:58,940
Se você fizer o lótus completo, você coloca uma perna primeiro.

8
00:01:04,220 --> 00:01:07,300
Depois o outro.

9
00:01:07,900 --> 00:01:12,140
Se você tiver boa estabilidade, você pode realmente soltar o corpo.

10
00:01:12,170 --> 00:01:15,220
Como se costuma dizer, soltar a mente-corpo.

11
00:01:22,460 --> 00:01:24,320
disse o Mestre Deshimaru:

12
00:01:25,020 --> 00:01:28,640
"Zen é o novo humanismo para o século 21".

13
00:01:29,560 --> 00:01:31,440
"Deve ser sempre fresco".

14
00:01:31,480 --> 00:01:34,000
"Se você praticar o verdadeiro zazen,"

15
00:01:34,060 --> 00:01:36,600
"queixo enfiado".

16
00:01:37,060 --> 00:01:39,980
A "espinha esticada".

17
00:01:40,580 --> 00:01:42,580
"apenas zazen","

18
00:01:43,620 --> 00:01:46,270
"o resto já não importa mais tanto".

19
00:01:46,320 --> 00:01:49,600
No início do zazen, há três sinos.

20
00:01:52,040 --> 00:01:56,060
E então, quando há parentes, a meditação está em andamento,

21
00:02:00,530 --> 00:02:03,580
Há dois sinos.

22
00:02:15,980 --> 00:02:18,940
Esticar bem o joelho da perna dianteira.

23
00:02:22,900 --> 00:02:28,340
Pressione o chão com a raiz do dedo grande do pé da frente.

24
00:02:34,580 --> 00:02:36,580
Relaxe seus ombros.

25
00:02:38,260 --> 00:02:41,480
No final da expiração,

26
00:02:44,100 --> 00:02:46,100
respiramos,

27
00:02:47,560 --> 00:02:51,900
e damos um pequeno passo à frente.

28
00:02:56,820 --> 00:03:00,060
Recomeçar, exalar...

29
00:03:01,360 --> 00:03:05,180
Você coloca todo seu peso corporal sobre a perna dianteira.

30
00:03:10,920 --> 00:03:13,760
O polegar da mão esquerda

31
00:03:16,120 --> 00:03:19,960
estamos cerrando-o em um punho.

32
00:03:22,400 --> 00:03:25,300
Então tomamos a mão direita

33
00:03:26,960 --> 00:03:29,480
e nós cobrimos a mão esquerda.

34
00:03:36,140 --> 00:03:40,040
Fazer zazen por zooming é muito prático...

35
00:03:40,260 --> 00:03:43,710
especialmente nestes tempos difíceis.

36
00:03:48,280 --> 00:03:53,440
Mas normalmente, ela complementa a prática em um verdadeiro dojo.

37
00:03:58,860 --> 00:04:03,340
Obviamente, nós dedicamos esta prática que estamos fazendo juntos

38
00:04:12,540 --> 00:04:19,560
A todas as pessoas que se dedicam a curar e salvar os outros.

39
00:04:34,720 --> 00:04:36,720
Oi, Toshi. Olá.

40
00:04:36,780 --> 00:04:38,720
Bom dia, mestre.

41
00:04:40,680 --> 00:04:43,940
Ah, aí está! Olá !

42
00:04:46,100 --> 00:04:48,100
Olá, Ingrid.

43
00:04:51,840 --> 00:04:54,640
Esse é Grabriela, mas qual deles?

44
00:04:57,460 --> 00:05:00,520
Ah, eu não o reconheci com seus óculos!

45
00:05:06,020 --> 00:05:09,160
Eu sou o tradutor, você não me reconhece?
