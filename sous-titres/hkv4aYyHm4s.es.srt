1
00:00:13,360 --> 00:00:15,750
Un cuento contado por el Maestro Deshimaru

2
00:00:17,320 --> 00:00:19,320
que se llama "Paseando por la Montaña".

3
00:00:19,980 --> 00:00:21,980
Así que ya sólo el título,

4
00:00:22,560 --> 00:00:24,080
es maravilloso,

5
00:00:24,609 --> 00:00:30,360
poder pasearse por la montaña. Un maestro solía ir a dar un paseo por la montaña y cuando volvía

6
00:00:31,179 --> 00:00:33,179
uno de sus discípulos le preguntó:

7
00:00:33,700 --> 00:00:35,700
"Maestro, ¿a dónde fue a dar un paseo?"

8
00:00:35,980 --> 00:00:40,420
"A la montaña" respondió el maestro y el discípulo insistió, "Pero qué camino

9
00:00:40,900 --> 00:00:43,380
ha tomado, ¿qué ha visto?"

10
00:00:45,260 --> 00:00:52,940
El maestro respondió: "Seguí el olor de las flores y deambulé por los jóvenes brotes de primavera".

11
00:00:55,480 --> 00:00:57,480
El Maestro Deshimaru continuó:

12
00:00:58,540 --> 00:01:02,180
"Uno debe dejarse guiar por el Dharma del Buda...

13
00:01:02,380 --> 00:01:03,820
tener confianza

14
00:01:04,140 --> 00:01:08,040
en las hierbas y flores que crecen sin meta,

15
00:01:08,600 --> 00:01:10,220
sin egoísmo,

16
00:01:10,440 --> 00:01:15,640
naturalmente, inconscientemente. Esa respuesta venía de la fuente de la sabiduría

17
00:01:16,900 --> 00:01:20,560
la verdadera sabiduría debe ser creada más allá del conocimiento...

18
00:01:21,100 --> 00:01:26,240
y de la memoria". Esa es una enseñanza muy poderosa.

19
00:01:26,700 --> 00:01:29,160
En un kusen,

20
00:01:29,340 --> 00:01:37,040
La enseñanza dada en el dojo, el maestro Kosen enseña: "La filosofía de Shikantaza",

21
00:01:37,900 --> 00:01:40,580
la filosofía de "sólo zazen".

22
00:01:40,800 --> 00:01:47,380
debe guiar nuestras vidas, es la verdadera esencia del budismo, el método más perfecto

23
00:01:48,249 --> 00:01:50,699
El zazen mismo es el satori, el despertar.

24
00:01:51,520 --> 00:01:53,520
Estamos cometiendo un error

25
00:01:54,159 --> 00:01:56,368
si creemos que hay un zazen antes

26
00:01:57,100 --> 00:02:02,460
y que con la acumulación de varios zazen, zazen, quince días, algo sucede, el despertar.

27
00:02:05,300 --> 00:02:11,520
Inmediatamente cualquiera que practique zazen desencadena el proceso de despertar.

28
00:02:13,020 --> 00:02:15,800
Bueno, este proceso no encaja en nuestras categorías.

29
00:02:16,900 --> 00:02:18,900
a veces la gente dice

30
00:02:18,959 --> 00:02:20,959
"El zazen fue muy, muy duro.

31
00:02:21,420 --> 00:02:24,949
fue un "mal zazen" o a veces

32
00:02:25,860 --> 00:02:27,860
Es muy cómodo.

33
00:02:28,409 --> 00:02:35,929
y el proceso que desencadena el zazen está más allá de nuestras categorías o de nuestra conciencia personal.

34
00:02:36,660 --> 00:02:42,169
Así que el método de Dogen, el método de otros Maestros, es genial, es único.

35
00:02:43,799 --> 00:02:48,769
Son enseñanzas que parecen muy simples las que recibimos,

36
00:02:50,190 --> 00:02:52,190
que recibimos durante el zazen

37
00:02:53,400 --> 00:02:55,400
y que vamos a

38
00:02:58,530 --> 00:03:02,750
integrarlas, lo que significa que vamos a hacerlas nuestras,

39
00:03:04,019 --> 00:03:09,319
suavemente, profundamente, inconscientemente, son lecciones valiosas.
