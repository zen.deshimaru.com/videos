1
00:00:08,660 --> 00:00:11,260
Buona giornata a tutti 

2
00:00:11,560 --> 00:00:19,400
Per sapere come sedersi nella meditazione zen, zazen .. 

3
00:00:19,800 --> 00:00:22,600
Ti darò subito una spiegazione. 

4
00:00:22,700 --> 00:00:27,880
Prima di tutto, presta attenzione che l’unica cosa di cui hai bisogno .. 

5
00:00:28,340 --> 00:00:33,020
è un essere umano e un cuscino per la meditazione. 

6
00:00:33,020 --> 00:00:36,680
Se hai un cuscino per la meditazione, molto bene. 

7
00:00:37,100 --> 00:00:40,560
E se no puoi farne uno con dei panni. 

8
00:00:43,060 --> 00:00:44,320
Eccoci qui. 

9
00:00:45,885 --> 00:00:48,855
Quindi, la meditazione zen .. 

10
00:00:49,620 --> 00:00:53,420
Prima di tutto, devi capire chiaramente che .. 

11
00:00:53,720 --> 00:00:57,580
La meditazione Zen è una meditazione seduta. 

12
00:00:57,940 --> 00:01:00,820
Immobile e silenzioso. 

13
00:01:01,295 --> 00:01:04,425
E la particolarità di questa meditazione è … 

14
00:01:04,425 --> 00:01:07,445
Che ci sediamo davanti al muro. 

15
00:01:08,400 --> 00:01:11,040
Come ora, mi siedo davanti alla telecamera ma .. 

16
00:01:11,380 --> 00:01:15,900
Ma durante la meditazione ti siedi di fronte al muro. 

17
00:01:16,520 --> 00:01:21,940
Quindi esamineremo tre diversi aspetti. 

18
00:01:24,860 --> 00:01:27,060
Ma noterai che .. 

19
00:01:27,300 --> 00:01:32,680
La meditazione sta praticando questi tre aspetti allo stesso tempo. 

20
00:01:33,420 --> 00:01:36,955
Il primo tema è la postura fisica .. 

21
00:01:36,955 --> 00:01:38,435
Con il corpo. 

22
00:01:39,175 --> 00:01:40,625
Il secondo tema è … 

23
00:01:41,020 --> 00:01:42,900
La respirazione 

24
00:01:42,960 --> 00:01:45,580
Respiriamo durante la meditazione zen. 

25
00:01:46,300 --> 00:01:48,720
E il terzo aspetto .. 

26
00:01:48,880 --> 00:01:50,320
L’atteggiamento mentale. 

27
00:01:50,620 --> 00:01:56,180
Significato, cosa facciamo mentre siamo seduti immobili .. 

28
00:01:56,180 --> 00:01:59,080
E in silenzio davanti al muro. 

29
00:02:00,040 --> 00:02:04,560
La postura del corpo 

30
00:02:06,160 --> 00:02:09,600
La postura del corpo, la postura fisica .. 

31
00:02:09,635 --> 00:02:12,585
La postura concreta della meditazione zen. 

32
00:02:12,585 --> 00:02:17,340
Quindi sei seduto su un cuscino. 

33
00:02:17,760 --> 00:02:21,440
Non sul bordo del cuscino ma ben in cima. 

34
00:02:21,440 --> 00:02:24,640
E dovresti prestare attenzione ad incrociare le gambe .. 

35
00:02:24,640 --> 00:02:28,160
In modo che le tue ginocchia possano … 

36
00:02:28,220 --> 00:02:32,540
Cadere o spingere il pavimento. 

37
00:02:32,540 --> 00:02:35,140
O che scendono. 

38
00:02:35,440 --> 00:02:37,220
Come si fa a farlo? 

39
00:02:37,840 --> 00:02:41,120
Prendi la tua gamba, come puoi. 

40
00:02:41,380 --> 00:02:44,020
Puoi dirlo in questo modo o .. 

41
00:02:44,685 --> 00:02:47,435
Meno rigido, potresti metterlo qui. 

42
00:02:47,435 --> 00:02:50,585
La posizione che chiamiamo loto medio. 

43
00:02:50,585 --> 00:02:54,980
E ovviamente puoi anche cambiare la parte superiore della gamba. 

44
00:02:55,240 --> 00:02:59,040
Posizionalo comunque in modo da farti sentire più a tuo agio. 

45
00:02:59,260 --> 00:03:02,240
Ricorda, non ci trasferiremo. 

46
00:03:02,500 --> 00:03:07,120
Quindi dovremmo trovare una posizione che non sia troppo difficile. 

47
00:03:07,120 --> 00:03:10,600
Che ci impedirà di .. 

48
00:03:10,600 --> 00:03:13,940
E stare zitto. Così.. 

49
00:03:14,160 --> 00:03:17,460
Le ginocchia vanno a terra. 

50
00:03:17,655 --> 00:03:19,655
Come facciamo questo? 

51
00:03:19,660 --> 00:03:23,800
Il pelvico si inclina leggermente in avanti. 

52
00:03:24,240 --> 00:03:27,415
Allora, cosa sta succedendo? 

53
00:03:27,420 --> 00:03:31,000
Se il bacino è così davanti. 

54
00:03:32,020 --> 00:03:36,180
A causa della gravità dietro la mia schiena .. 

55
00:03:36,180 --> 00:03:39,020
O come quando sto conducendo una macchina .. 

56
00:03:39,020 --> 00:03:41,025
E come puoi vedere .. 

57
00:03:41,025 --> 00:03:43,860
Le mie ginocchia sono in aria. 

58
00:03:43,860 --> 00:03:47,060
E voglio che le mie ginocchia si abbassino. 

59
00:03:47,060 --> 00:03:51,700
In zen diciamo: "premi la terra con le tue ginocchia". 

60
00:03:51,900 --> 00:03:54,940
E questo movimento, con la gravità .. 

61
00:03:55,040 --> 00:03:57,300
Andando verso il basso .. 

62
00:03:57,300 --> 00:04:03,120
Con questa leggera inclinazione del bacino vediamo .. 

63
00:04:03,460 --> 00:04:06,995
Non c’è alcun intervento con i muscoli. 

64
00:04:06,995 --> 00:04:10,355
Guardandolo dal profilo .. 

65
00:04:12,355 --> 00:04:15,315
Nel caso in cui tu sia così .. 

66
00:04:15,780 --> 00:04:19,160
Dove il bacino è rivolto in avanti .. 

67
00:04:20,220 --> 00:04:21,700
Giralo un po ’.. 

68
00:04:22,720 --> 00:04:23,780
Proprio verso qui. 

69
00:04:24,960 --> 00:04:27,040
Perfettamente dritto. 

70
00:04:27,040 --> 00:04:30,080
Se ti siedi così naturalmente … 

71
00:04:30,080 --> 00:04:31,740
Il centro di gravità è .. 

72
00:04:31,740 --> 00:04:34,875
Venendo più in primo piano .. 

73
00:04:34,875 --> 00:04:37,555
E le tue ginocchia cadranno sul pavimento. 

74
00:04:39,555 --> 00:04:42,595
Da questo bacino inclinato .. 

75
00:04:42,945 --> 00:04:45,725
Raddrizzi la schiena .. 

76
00:04:46,165 --> 00:04:48,885
Come puoi. 

77
00:04:49,600 --> 00:04:52,100
A questo proposito parliamo di .. 

78
00:04:52,100 --> 00:04:55,460
Spingendo il cielo 

79
00:04:55,460 --> 00:04:58,700
Con la cima della nostra testa. 

80
00:05:04,620 --> 00:05:06,680
In zen la colonna vertebrale .. 

81
00:05:07,560 --> 00:05:10,720
Inizia o si ferma (come vuoi tu) qui. 

82
00:05:10,720 --> 00:05:14,020
Se raddrizziamo la schiena .. 

83
00:05:14,040 --> 00:05:17,100
Ciò significa che prova ad allungarlo fino a lì. 

84
00:05:17,380 --> 00:05:18,740
Quindi non abbiamo intenzione di .. 

85
00:05:19,260 --> 00:05:21,400
Medita con la testa .. 

86
00:05:22,335 --> 00:05:25,525
Cadere, né cadere all’indietro. 

87
00:05:26,015 --> 00:05:28,255
Il mento dovrebbe essere .. 

88
00:05:29,080 --> 00:05:31,240
Il più dritto possibile. 

89
00:05:31,960 --> 00:05:35,920
Parliamo di mantenere una linea orizzontale diritta. 

90
00:05:36,280 --> 00:05:38,320
Gli occhi.. 

91
00:05:39,285 --> 00:05:40,345
La bocca.. 

92
00:05:41,015 --> 00:05:42,905
Una linea verticale .. 

93
00:05:43,305 --> 00:05:45,125
Tra gli occhi, la bocca e .. 

94
00:05:45,605 --> 00:05:46,695
L’ombelico. 

95
00:05:48,075 --> 00:05:49,075
Buona. 

96
00:05:49,880 --> 00:05:53,840
Una volta che mi siedo il più dritto possibile .. 

97
00:05:54,060 --> 00:05:56,365
La postura delle mani .. 

98
00:05:56,365 --> 00:05:58,495
La mano sinistra 

99
00:05:59,765 --> 00:06:01,175
Dita insieme .. 

100
00:06:01,575 --> 00:06:04,725
Hai messo nella mano destra. 

101
00:06:04,920 --> 00:06:06,380
Dita insieme. 

102
00:06:06,600 --> 00:06:09,740
Perché le dita si sovrappongono. 

103
00:06:11,765 --> 00:06:13,975
Una linea orizzontale con .. 

104
00:06:13,975 --> 00:06:17,115
I pollici si toccano. 

105
00:06:17,360 --> 00:06:21,280
Si toccano ad esempio per contenere una piuma. 

106
00:06:21,280 --> 00:06:22,480
Quindi non c’è bisogno di .. 

107
00:06:22,885 --> 00:06:24,925
Pressione muscolare 

108
00:06:25,915 --> 00:06:29,125
E l’interno delle nostre mani. 

109
00:06:29,905 --> 00:06:33,295
Abbiamo messo alla base, qui .. 

110
00:06:33,675 --> 00:06:36,545
Più o meno nel mezzo del ventre. 

111
00:06:36,760 --> 00:06:40,900
Quindi, al fine di mantenere questa postura .. 

112
00:06:41,620 --> 00:06:47,740
Devi prendere qualsiasi pezzo di stoffa 

113
00:06:48,140 --> 00:06:51,800
Che ti aiuterà a .. 

114
00:06:53,680 --> 00:06:56,675
Lascia andare le tue mani 

115
00:06:56,680 --> 00:07:00,920
In modo libero contro la pancia .. 

116
00:07:01,180 --> 00:07:06,080
Senza sentirsi obbligati a usare le dita e le spalle .. 

117
00:07:06,400 --> 00:07:09,600
Al contrario, puoi lasciar andare tutte le tensioni. 

118
00:07:10,180 --> 00:07:12,780
I gomiti sono liberi .. 

119
00:07:12,995 --> 00:07:14,985
Mi metterò di profilo .. 

120
00:07:15,355 --> 00:07:17,065
Quindi puoi vedere bene. 

121
00:07:17,485 --> 00:07:19,105
Ci riprendiamo 

122
00:07:20,125 --> 00:07:22,925
Dritto.. 

123
00:07:23,820 --> 00:07:27,460
Le mani, a sinistra in alto a destra. 

124
00:07:27,740 --> 00:07:31,000
I pollici. E l’interno delle mani .. 

125
00:07:31,000 --> 00:07:34,480
Alla base dell’addome. 

126
00:07:35,140 --> 00:07:36,460
Gomiti dritti. 

127
00:07:37,985 --> 00:07:38,985
Buona. 

128
00:07:40,120 --> 00:07:42,540
Questa è la postura .. 

129
00:07:43,220 --> 00:07:47,000
In parole povere, diciamo .. 

130
00:07:47,640 --> 00:07:49,840
Del materiale, il corpo. 

131
00:07:49,840 --> 00:07:53,240
Una volta che ho messo il mio corpo .. 

132
00:07:54,345 --> 00:07:57,225
Nella posizione di meditazione .. 

133
00:07:57,225 --> 00:08:00,455
Ricordi, in silenzio davanti a un muro. 

134
00:08:01,280 --> 00:08:02,680
Che succede? 

135
00:08:02,960 --> 00:08:04,520
Quello che accadrà è che .. 

136
00:08:04,760 --> 00:08:08,160
Di fronte al muro, non succede nulla di interessante. 

137
00:08:08,640 --> 00:08:12,380
Parliamo di rivolgere la tua attenzione verso l’interno. 

138
00:08:14,360 --> 00:08:17,200
Quindi riguardo al tuo sguardo durante lo zazen: 

139
00:08:17,975 --> 00:08:20,795
Gli occhi sono semi-aperti .. 

140
00:08:21,085 --> 00:08:24,345
45 gradi in diagonale .. 

141
00:08:24,345 --> 00:08:25,820
Verso il basso. 

142
00:08:26,000 --> 00:08:28,985
È come prendere la tua visione .. 

143
00:08:28,985 --> 00:08:32,155
E mettendolo di fronte a te. 

144
00:08:32,245 --> 00:08:34,785
Non c’è niente di interessante da vedere. 

145
00:08:35,160 --> 00:08:38,920
Quindi faremo lo stesso con gli altri sensi. 

146
00:08:39,160 --> 00:08:43,180
Se parliamo di una meditazione silenziosa .. 

147
00:08:44,440 --> 00:08:48,780
Non hai bisogno di niente. Niente musica, niente. 

148
00:08:49,080 --> 00:08:51,580
Un silenzio interiore. 

149
00:08:52,220 --> 00:08:55,680
È una meditazione silenziosa, quindi … 

150
00:08:56,475 --> 00:08:58,315
Non è inoltre necessario parlare. 

151
00:08:59,235 --> 00:09:00,235
Conclusione.. 

152
00:09:00,900 --> 00:09:02,880
Quando sono immobile .. 

153
00:09:02,880 --> 00:09:06,760
La mia visione non serve a niente .. 

154
00:09:06,760 --> 00:09:09,000
Né è il mio udito 

155
00:09:09,280 --> 00:09:10,820
o le mie parole. 

156
00:09:10,980 --> 00:09:14,280
Quindi la conclusione è che .. 

157
00:09:14,500 --> 00:09:17,920
I nostri sensi con cui percepiamo il mondo che ci circonda .. 

158
00:09:17,920 --> 00:09:21,300
Li mettiamo in "modalità aereo". 

159
00:09:21,300 --> 00:09:23,980
Potremmo dirlo così. In standby. 

160
00:09:24,300 --> 00:09:27,400
E questo è il principio della postura .. 

161
00:09:27,400 --> 00:09:30,140
Attraverso la creazione di .. 

162
00:09:30,740 --> 00:09:33,675
Questa postura fisica e .. 

163
00:09:33,675 --> 00:09:36,775
Per tagliare l’esterno .. 

164
00:09:36,775 --> 00:09:39,915
Per tagliarlo con i nostri sentimenti. 

165
00:09:40,060 --> 00:09:43,940
Quindi possiamo iniziare a meditare davvero. 

166
00:09:51,140 --> 00:09:55,440
Ora che abbiamo una postura più tranquilla .. 

167
00:09:56,040 --> 00:09:59,380
Sono di fronte a un muro, meditando .. 

168
00:09:59,685 --> 00:10:01,975
E con questo silenzio .. 

169
00:10:02,495 --> 00:10:04,675
Sto notando il fatto che … 

170
00:10:05,480 --> 00:10:09,440
La mia testa è molto attiva. 

171
00:10:09,780 --> 00:10:12,880
Quindi prendo in considerazione questo qui … 

172
00:10:13,120 --> 00:10:17,080
Ho molti pensieri .. 

173
00:10:17,080 --> 00:10:22,740
Che svanirà con il nostro sguardo, potresti dirlo così. 

174
00:10:23,040 --> 00:10:26,240
Pensieri, immagini visive .. 

175
00:10:26,245 --> 00:10:28,235
Suoni .. 

176
00:10:28,865 --> 00:10:29,945
Molte cose. 

177
00:10:31,420 --> 00:10:33,520
Quindi cosa faremo .. 

178
00:10:33,720 --> 00:10:37,075
Nella nostra meditazione zen è … 

179
00:10:37,080 --> 00:10:40,205
Per lasciar andare questi pensieri. 

180
00:10:40,205 --> 00:10:43,180
Questo è un punto fondamentale .. 

181
00:10:43,180 --> 00:10:45,500
Della posizione di meditazione zen. 

182
00:10:45,640 --> 00:10:49,200
Mentre sono così in zazen .. 

183
00:10:49,200 --> 00:10:53,380
Non sto passando il mio tempo, riflettendo su .. 

184
00:10:53,380 --> 00:10:57,240
Qualunque cosa, forse interessante, forse no .. 

185
00:10:57,420 --> 00:10:59,340
Non si tratta di questo. 

186
00:10:59,580 --> 00:11:03,700
Si tratta dello stesso modo in cui abbiamo messo il nostro corpo .. 

187
00:11:03,715 --> 00:11:06,685
In una situazione tranquilla .. 

188
00:11:06,825 --> 00:11:10,215
Metterò la mia mente e la mente .. 

189
00:11:10,505 --> 00:11:13,415
Che è sempre agitato .. 

190
00:11:13,415 --> 00:11:16,035
Lo metterò .. 

191
00:11:16,035 --> 00:11:19,035
Nello stesso modo.. 

192
00:11:19,040 --> 00:11:20,500
In un tranquillo stato d’essere. 

193
00:11:20,680 --> 00:11:23,640
Come detto semplicemente .. 

194
00:11:23,640 --> 00:11:26,620
Lascia andare i miei pensieri. 

195
00:11:27,760 --> 00:11:31,800
I nostri pensieri spuntano dall’esterno. 

196
00:11:32,680 --> 00:11:35,200
Per dimostrarlo; se ci chiediamo .. 

197
00:11:35,200 --> 00:11:39,040
Cosa penseremo tra 5 minuti o tra mezz’ora .. 

198
00:11:39,305 --> 00:11:42,575
O domani? Nessuno può sapere .. 

199
00:11:43,440 --> 00:11:46,780
Quindi, anche mentre dormiamo .. 

200
00:11:46,960 --> 00:11:50,080
Abbiamo dei sogni. Quindi continuiamo sempre .. 

201
00:11:50,540 --> 00:11:53,440
Un’attività mentale, consapevolmente o meno. 

202
00:11:53,660 --> 00:11:57,000
Durante la meditazione zen questa attività mentale .. 

203
00:11:57,165 --> 00:12:00,445
Lo abbatteremo e lo calmeremo. 

204
00:12:00,760 --> 00:12:04,760
Come diciamo; è lasciar passare i tuoi pensieri. 

205
00:12:05,100 --> 00:12:07,520
Ma poi dirai, ok molto bene ma .. 

206
00:12:07,520 --> 00:12:08,900
Come fare questo? 

207
00:12:09,080 --> 00:12:12,920
Questo possiamo farlo attraverso la respirazione. 

208
00:12:13,240 --> 00:12:16,400
Questo mi porta al terzo punto; 

209
00:12:16,440 --> 00:12:19,400
Come respirare durante lo zazen? 

210
00:12:19,400 --> 00:12:23,920
Respirazione Zen 

211
00:12:26,740 --> 00:12:29,660
Respirazione durante la meditazione zen. 

212
00:12:29,880 --> 00:12:32,560
È un respiro addominale. 

213
00:12:34,415 --> 00:12:36,265
Noi inspiriamo.. 

214
00:12:36,705 --> 00:12:39,985
Riempiamo la nostra capacità polmonare 

215
00:12:39,985 --> 00:12:42,185
Con un po ’di generosità. 

216
00:12:42,925 --> 00:12:45,245
E ci concentreremo .. 

217
00:12:45,245 --> 00:12:46,740
Particolarmente.. 

218
00:12:47,120 --> 00:12:49,900
Alla nostra scadenza 

219
00:12:50,200 --> 00:12:52,560
Per restituire l’aria. 

220
00:12:52,740 --> 00:12:55,875
Questo modo di espirare, a poco a poco .. 

221
00:12:55,875 --> 00:12:59,165
Cerchiamo di prolungarlo. 

222
00:12:59,225 --> 00:13:02,275
Un lungo periodo di espirazione. 

223
00:13:02,345 --> 00:13:05,135
Significato, per espirare l’aria .. 

224
00:13:05,140 --> 00:13:06,720
Attraverso il naso 

225
00:13:07,000 --> 00:13:10,360
Morbidamente senza fare rumore. 

226
00:13:10,700 --> 00:13:13,080
Siamo in una meditazione silenziosa .. 

227
00:13:13,300 --> 00:13:16,240
Espiriamo l’aria, attraverso il naso .. 

228
00:13:17,160 --> 00:13:21,260
Prova di estendere l’espirazione. 

229
00:13:21,620 --> 00:13:23,880
E alla fine della tua espirazione .. 

230
00:13:24,625 --> 00:13:27,785
Inspiriamo e di nuovo .. 

231
00:13:36,465 --> 00:13:39,565
Lasciamo andare l’aria e di nuovo … 

232
00:13:43,440 --> 00:13:46,980
Quindi se focalizzi la tua concentrazione su … 

233
00:13:47,240 --> 00:13:50,420
l’andare e venire, si stabilisce .. 

234
00:13:50,820 --> 00:13:53,500
calma la tua respirazione .. 

235
00:13:54,540 --> 00:13:57,495
Dovresti lasciarlo tranquillamente .. 

236
00:13:57,500 --> 00:14:02,600
Dovresti ingrandire l’espirazione. 

237
00:14:03,160 --> 00:14:07,180
E questo fa sì che il tuo diaframma scenda. 

238
00:14:07,480 --> 00:14:08,740
Scende. 

239
00:14:08,980 --> 00:14:11,980
Darà un massaggio interno all’intestino .. 

240
00:14:11,980 --> 00:14:13,980
Che li farà bene. 

241
00:14:14,160 --> 00:14:17,700
E lentamente puoi sentire .. 

242
00:14:18,160 --> 00:14:20,820
Come se qualcosa si stesse gonfiando. 

243
00:14:21,145 --> 00:14:23,720
Nella zona in cui abbiamo messo le mani. 

244
00:14:24,020 --> 00:14:27,700
Quindi, è qui che stanno accadendo le cose. 

245
00:14:28,040 --> 00:14:30,520
Quindi non è qui, ma è .. 

246
00:14:31,900 --> 00:14:34,925
Laggiù. 

247
00:14:34,925 --> 00:14:39,360
Respirazione attraverso il naso e in silenzio. 

248
00:14:46,440 --> 00:14:49,900
La postura fisica: incrocia le gambe. 

249
00:14:50,020 --> 00:14:52,860
Le ginocchia scendono a terra. 

250
00:14:53,420 --> 00:14:56,140
Il bacino leggermente attorcigliato nella parte anteriore. 

251
00:14:56,700 --> 00:15:00,760
Raddrizziamo la spina dorsale il meglio che possiamo. 

252
00:15:01,160 --> 00:15:02,840
Fino alla corona della nostra testa .. 

253
00:15:02,840 --> 00:15:08,040
Spingiamo il cielo con la testa. 

254
00:15:08,540 --> 00:15:11,800
La mano sinistra dentro la mano destra. 

255
00:15:11,925 --> 00:15:13,925
I pollici in una linea orizzontale .. 

256
00:15:13,925 --> 00:15:17,700
Non come una valle, non come una montagna, una cima. 

257
00:15:17,960 --> 00:15:20,100
Dritto. 

258
00:15:20,100 --> 00:15:24,140
Quando metti le mani a contatto con la pancia. 

259
00:15:24,500 --> 00:15:26,640
Usi qualcosa sotto … 

260
00:15:26,665 --> 00:15:28,675
Per avere una posizione più comoda. 

261
00:15:29,125 --> 00:15:31,805
Hai i gomiti dritti. 

262
00:15:31,805 --> 00:15:32,885
Questa è la nostra postura fisica. 

263
00:15:33,645 --> 00:15:37,065
In secondo luogo, il nostro stato mentale. 

264
00:15:37,280 --> 00:15:41,760
Ci tagliamo dall’esterno. 

265
00:15:42,160 --> 00:15:44,060
Dai suoni .. 

266
00:15:44,060 --> 00:15:46,760
Non mediteremo con una radio. 

267
00:15:46,760 --> 00:15:48,755
O con il cellulare acceso. 

268
00:15:48,755 --> 00:15:51,635
Quindi disattivali, ma è logico. 

269
00:15:52,040 --> 00:15:54,620
Tagliamo tutti i suoni. 

270
00:15:54,620 --> 00:15:58,295
Portiamo il nostro sguardo verso l’interno e torniamo a .. 

271
00:15:58,300 --> 00:16:02,040
Essere l’osservatore di ciò che sta accadendo dall’interno. 

272
00:16:02,120 --> 00:16:05,000
E cosa sta succedendo dentro? 

273
00:16:05,380 --> 00:16:07,180
Notiamo che c’è un’attività mentale. 

274
00:16:07,660 --> 00:16:10,720
I pensieri stanno bruciando .. 

275
00:16:10,725 --> 00:16:12,665
Non possiamo controllarli. 

276
00:16:13,155 --> 00:16:16,415
I pensieri sorgono automaticamente. 

277
00:16:16,440 --> 00:16:21,000
Quindi non si tratta di prevenire questa eccitazione. 

278
00:16:21,380 --> 00:16:24,740
Meglio dirlo, si tratta di .. 

279
00:16:24,740 --> 00:16:29,740
Non saltare sul treno dei nostri pensieri. 

280
00:16:29,960 --> 00:16:33,420
E parliamo di lasciarli passare. 

281
00:16:33,500 --> 00:16:36,385
Come possiamo lasciarli passare? 

282
00:16:36,385 --> 00:16:41,380
Prima di tutto, con la concentrazione del qui e dell’ora .. 

283
00:16:41,540 --> 00:16:43,260
Sulla nostra postura fisica. 

284
00:16:43,795 --> 00:16:46,835
E con questa concentrazione .. 

285
00:16:46,840 --> 00:16:51,640
Qui e ora, sul mio respiro. 

286
00:16:52,080 --> 00:16:54,960
Sono pienamente consapevole del mio respiro .. 

287
00:16:55,265 --> 00:16:58,505
Della mia respirazione addominale. 

288
00:16:58,540 --> 00:17:02,580
Quindi respiriamo attraverso il naso .. 

289
00:17:03,080 --> 00:17:04,520
Inspiriamo .. 

290
00:17:05,095 --> 00:17:08,385
Cerchiamo di estendere l’espirazione. 

291
00:17:08,900 --> 00:17:10,460
Ogni volta.. 

292
00:17:10,700 --> 00:17:14,040
Più morbido, più a lungo. 

293
00:17:14,740 --> 00:17:17,735
E alla fine dell’espirazione ricominciamo. 

294
00:17:17,740 --> 00:17:19,800
Va e viene come .. 

295
00:17:20,080 --> 00:17:23,920
Un’onda nell’oceano. Le onde. 

296
00:17:24,360 --> 00:17:28,340
Ma .. lungo, lungo, lungo. 

297
00:17:29,180 --> 00:17:32,605
Se ti stai concentrando sul qui e ora .. 

298
00:17:32,605 --> 00:17:36,620
Sui punti della postura 

299
00:17:36,920 --> 00:17:41,140
Significa che non stiamo dormendo, durante la meditazione .. 

300
00:17:41,680 --> 00:17:44,880
E se ti concentri allo stesso tempo .. 

301
00:17:45,040 --> 00:17:48,500
Stabilendo il tuo respiro .. 

302
00:17:48,680 --> 00:17:51,140
In modo armonizzato e silenzioso. 

303
00:17:51,140 --> 00:17:53,780
E con una scadenza estesa .. 

304
00:17:53,940 --> 00:17:57,680
Non avrai tempo di pensare in te stesso o .. 

305
00:17:57,800 --> 00:18:00,060
O in qualsiasi altra cosa, per quanto interessante possa essere. 

306
00:18:00,295 --> 00:18:03,365
Quindi la conclusione è … 

307
00:18:03,365 --> 00:18:05,665
Mediteremo 20 minuti, mezz’ora, un’ora .. 

308
00:18:06,820 --> 00:18:08,540
Tu decidi. 

309
00:18:08,720 --> 00:18:12,380
Ma questo momento di meditazione, di concentrazione .. 

310
00:18:12,455 --> 00:18:15,635
Ti darà benefici immediati. 

311
00:18:15,635 --> 00:18:17,255
Ti sentirai .. 

312
00:18:18,405 --> 00:18:21,405
Un po ’più tranquillo. 

313
00:18:21,405 --> 00:18:24,665
Un po ’più leggero. 

314
00:18:24,665 --> 00:18:27,355
E ancora meglio .. 

315
00:18:27,355 --> 00:18:29,355
Creerai una distanza tra .. 

316
00:18:29,355 --> 00:18:32,445
te stesso e tutte le emozioni che sorgono nella tua vita. 

317
00:18:32,885 --> 00:18:34,395
E questo.. 

318
00:18:34,785 --> 00:18:37,725
È davvero un buon strumento 

319
00:18:38,135 --> 00:18:41,015
La maggior parte delle volte.. 

320
00:18:41,015 --> 00:18:44,055
Ma soprattutto ora .. 

321
00:18:44,060 --> 00:18:48,420
Quella metà della nostra popolazione è confinata. 

322
00:19:29,920 --> 00:19:34,960
Consigli per un principiante 

323
00:19:35,300 --> 00:19:40,380
Invece di arrabbiarti o fare storie … 

324
00:19:40,380 --> 00:19:45,380
In destrieri di diffondere pensieri più o meno .. 

325
00:19:45,380 --> 00:19:48,160
Di dimensione inferiore 

326
00:19:48,420 --> 00:19:49,840
Siediti. 

327
00:19:49,995 --> 00:19:53,325
Di fronte a un muro. Adotta questa meditazione. 

328
00:19:54,440 --> 00:19:58,320
Lavora su te stesso in questo modo. 

329
00:19:58,760 --> 00:20:00,620
E noterai presto .. 

330
00:20:00,625 --> 00:20:02,925
Quanto bene ti farà. 
