1
00:00:00,000 --> 00:00:02,240
Sono un monaco Zen. 

2
00:00:04,540 --> 00:00:09,960
Sono anche un calligrafo. 

3
00:00:10,980 --> 00:00:18,660
La calligrafia cinese è apprezzata dalle persone che praticano lo zazen. 

4
00:00:19,280 --> 00:00:24,980
Non è facile parlare di Zen. 

5
00:00:25,400 --> 00:00:28,820
Per esprimerlo, per manifestarlo. 

6
00:00:29,240 --> 00:00:34,480
Attraverso la calligrafia, vediamo lo spirito di una persona che fa lo zazen. 

7
00:00:35,000 --> 00:00:39,100
Chi pratica l’atteggiamento del risveglio. 

8
00:00:40,020 --> 00:00:46,260
Il maestro Deshimaru spiega che la calligrafia non è solo una tecnica. 

9
00:00:46,980 --> 00:00:52,700
Non solo calligrafia secondo tecniche, proprietà … 

10
00:00:53,360 --> 00:00:58,400
Callichiamo con la mente, con lo stinco. 

11
00:00:58,880 --> 00:01:09,440
Con la mente, con tutto il corpo e la mente in uno. 

12
00:01:10,720 --> 00:01:18,400
La calligrafia lo stampa su carta. È una presenza. 

13
00:01:18,960 --> 00:01:24,320
Si sente nella calligrafia di grandi maestri come Deshimaru e Niwa Zenji. 

14
00:01:25,760 --> 00:01:30,000
Il valore della calligrafia non è solo nella forma. 

15
00:01:31,440 --> 00:01:36,560
Ma anche nel momento, la presenza, l’energia. 

16
00:01:37,280 --> 00:01:46,240
Nell’attività che si presenta …. 

17
00:01:47,040 --> 00:01:52,080
E che sentiamo e percepiamo. 

18
00:01:52,500 --> 00:01:55,800
Partiamo da uno slancio che deriva dall’immobilità. 

19
00:01:56,950 --> 00:01:58,853
Zen, calma, calma, silenzio. 

20
00:02:00,160 --> 00:02:06,093
Per non far andare nulla. 

21
00:02:06,100 --> 00:02:07,910
Lasciare passare i pensieri. 

22
00:02:07,950 --> 00:02:10,750
Non essere monopolizzato. 

23
00:02:10,840 --> 00:02:15,040
Concentrarsi in un atteggiamento. 

24
00:02:15,920 --> 00:02:21,329
Gambe incrociate. 

25
00:02:21,329 --> 00:02:26,880
Il cuscino consente di inclinare il bacino in avanti. 

26
00:02:26,880 --> 00:02:31,280
Per cancellare l’intera gabbia toracica. 

27
00:02:32,240 --> 00:02:37,040
In questa posa, riportiamo le mani indietro. 

28
00:02:38,160 --> 00:02:43,680
Ci permette di unire e centrare. 

29
00:02:44,400 --> 00:02:51,040
È l’atteggiamento della meditazione Zen, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
È da questo, e dallo slancio, dall’attività, che emergerà la calligrafia. 

31
00:02:58,480 --> 00:03:05,200
Non dalle complicazioni, dal voler fare del bene, dal voler fare del bene, dal voler fare del bel tempo … 

32
00:03:06,240 --> 00:03:11,520
Solo questo slancio della vita. 

33
00:03:12,560 --> 00:03:22,160
La calligrafia emergerà da questo impulso Zen, questo impulso, questo impulso. 

34
00:03:23,280 --> 00:03:33,200
Ecco di cosa parla la calligrafia Zen. 

35
00:03:34,720 --> 00:03:59,600
Considerando la fase di civilizzazione che la Cina aveva raggiunto durante la dinastia Tang … 

36
00:04:01,120 --> 00:04:15,520
Nel VII, VIII e IX secolo, i paesi circostanti, Giappone, Corea e Vietnam, furono attratti da questa cultura e dalla lingua, dai caratteri cinesi. 

37
00:04:16,400 --> 00:04:27,200
Quindi la sceneggiatura cinese si è diffusa in questi paesi. 

38
00:04:28,080 --> 00:04:34,480
Ma tutti lo hanno registrato secondo i propri sentimenti, la propria lingua. 

39
00:04:35,280 --> 00:04:44,560
Ognuno di loro ha sviluppato il proprio modo di calligrafia. 

40
00:04:45,520 --> 00:04:56,080
Hanno anche introdotto programmi di studio, personaggi simili a un alfabeto mescolati a caratteri cinesi. 

41
00:04:58,320 --> 00:05:04,480
La calligrafia giapponese ha differenze minori rispetto alla calligrafia cinese. 

42
00:05:05,680 --> 00:05:13,440
Tutti usano pennelli, disegnano caratteri cinesi, ma usano anche sistemi sillabici. 

43
00:05:15,040 --> 00:05:23,280
I vietnamiti, precedentemente influenzati dalla Cina, erano ancora più creativi. 

44
00:05:24,800 --> 00:05:31,600
Hanno inventato il loro sistema ideografico per trascrivere la loro lingua sul modello cinese. 

45
00:05:33,440 --> 00:05:38,400
È scritto: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
È il sutra dello Spirito della Grande Saggezza che ci fa avanzare. 

48
00:05:59,840 --> 00:06:06,160
Questo è l’insegnamento universale del Buddha, specifico per tutte le scuole buddiste. 

49
00:06:07,360 --> 00:06:16,800
È cantato in cinese antico, giapponese antico, in trascrizione fonetica dal sanscrito. 

50
00:06:18,080 --> 00:06:24,000
Questo lo rende un testo universale, perché nessuno lo capisce o lo legge nella propria lingua. 

51
00:06:25,360 --> 00:06:39,120
I giapponesi, i cinesi, gli indiani …. Lo pronunciamo noi stessi con alcuni piccoli aggiustamenti …. 

52
00:06:40,980 --> 00:06:47,460
È qualcosa che appartiene a tutti e nessuno in particolare. 

53
00:06:48,220 --> 00:06:53,440
Questa è la frase che ho scritto qui, in uno stile di scrittura chiamato corsivo. 

54
00:06:53,680 --> 00:06:58,520
Non sta scrivendo il dizionario. 

55
00:06:58,560 --> 00:07:03,680
Non è una scrittura normale. 

56
00:07:05,740 --> 00:07:09,100
È stato sviluppato per la scrittura veloce. 

57
00:07:09,480 --> 00:07:15,960
Una specie di stenografia. 

58
00:07:16,240 --> 00:07:28,560
Manteniamo le linee principali del personaggio semplificando, attraverso le curvature, i punti, dove c’era una linea … 

59
00:07:29,600 --> 00:07:39,440
Il personaggio è stato semplificato all’estremo, assume una forma diversa dalla sua forma originale. 

60
00:07:39,760 --> 00:07:47,680
Ci muoviamo verso un’astrazione, preservando l’essenza della forma originale. 

61
00:07:48,800 --> 00:07:55,920
Ti permette di scrivere più velocemente. 

62
00:07:58,080 --> 00:08:05,240
Nella trama troviamo questo impulso, questo impulso, questo impulso di zazen. 

63
00:08:05,640 --> 00:08:13,280
Perché colleghiamo l’intero personaggio insieme. 

64
00:08:13,660 --> 00:08:18,479
Subito. 

65
00:08:18,479 --> 00:08:23,470
Possiamo anche collegare i personaggi insieme. 

66
00:08:24,000 --> 00:08:31,840
Colleghiamo ciascuno dei personaggi, ciascuna delle proprietà nel personaggio. 

67
00:08:33,120 --> 00:08:38,520
Se vengono disegnati più caratteri, possono essere collegati insieme. 

68
00:08:38,940 --> 00:08:44,960
Lo facciamo sul respiro, sull’impulso. 

69
00:08:45,400 --> 00:08:51,540
Al momento, l’energia, la dinamica, l’attività di tutto il corpo, l’intera mente in uno. 

70
00:08:51,540 --> 00:08:55,200
In un solo movimento, dall’inizio alla fine. 

71
00:08:55,200 --> 00:08:59,200
Non possiamo tornare indietro, non possiamo aggiornarlo. 

72
00:08:59,200 --> 00:09:02,900
Non possiamo correggerlo. 

73
00:09:03,000 --> 00:09:05,120
Lo facciamo in coscienza. 

74
00:09:05,920 --> 00:09:11,600
Ci lasciamo andare anche con gioia. 

75
00:09:12,060 --> 00:09:18,220
Ma non possiamo correggere nulla. 

76
00:09:18,560 --> 00:09:22,160
Quando viene tracciato, viene tracciato. 

77
00:09:22,200 --> 00:09:25,529
È il qui e ora. 

78
00:09:25,529 --> 00:09:29,000
È qualcosa che non tornerà mai più. 

79
00:09:29,100 --> 00:09:33,800
Questo esiste completamente, eternamente, nel momento. 

80
00:09:33,900 --> 00:09:36,200
Anche quello è Zen. 

81
00:09:36,250 --> 00:09:39,640
Ogni volta, ogni respiro di zazen è unico. 

82
00:09:42,520 --> 00:09:44,640
Lei non tornerà. 

83
00:09:45,340 --> 00:09:52,540
È un punto, un trattino dell’atteggiamento. 

84
00:09:52,600 --> 00:09:58,320
Nell’umore, nella respirazione e nell’attività totale. 

85
00:09:58,420 --> 00:10:03,160
È un tempo di vita, di coscienza, assoluto, universale. 

86
00:10:03,720 --> 00:10:06,760
Chi non torna. 

87
00:10:07,120 --> 00:10:11,000
È molto importante in calligrafia. 

88
00:10:11,000 --> 00:10:17,500
Se calligrafia come un robot senza metterti tutti i pensieri, è una tecnica. 

89
00:10:17,540 --> 00:10:18,920
Zazen è lo stesso. 

90
00:10:19,000 --> 00:10:25,880
Se pratichi lo zazen come una tecnica di benessere, non è la dimensione dello Zen. 

