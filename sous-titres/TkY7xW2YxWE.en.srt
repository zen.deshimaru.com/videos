1
00:00:00,200 --> 00:00:03,460
Sometimes you can only find that in Zen, 

2
00:00:03,780 --> 00:00:06,680
when we’re together for a while, 

3
00:00:06,920 --> 00:00:08,960
that people lack compassion. 

4
00:00:09,180 --> 00:00:11,600
I think sometimes you have to change the way you look at things. 

5
00:00:11,980 --> 00:00:16,400
Instead of thinking, "Hey, that’s the other one that robbed me!" 

6
00:00:17,000 --> 00:00:20,680
thinking, "what the hell am I supposed to do with this?" 

7
00:00:21,180 --> 00:00:23,160
And try to turn it over. 

8
00:00:24,360 --> 00:00:27,900
That is very sincere. 

9
00:00:28,300 --> 00:00:30,340
Because people are themselves. 

10
00:00:30,600 --> 00:00:32,560
They try not to play a role: 

11
00:00:32,760 --> 00:00:36,900
"I’m fine, I’m fine, I’m fine …" 

12
00:00:38,240 --> 00:00:41,760
There are certainly times when we get upset. 

13
00:00:42,040 --> 00:00:45,280
And where our reactions stand out. 

14
00:00:45,600 --> 00:00:50,780
It is important to me that we do not settle there. 

15
00:00:51,040 --> 00:00:52,880
Not to wallow in the idea: 

16
00:00:53,200 --> 00:00:55,700
"In a sesshin, there are no nice people." 

17
00:00:55,960 --> 00:00:59,320
When I get to Sesshin, 

18
00:00:59,880 --> 00:01:04,060
my idea is to turn everything into a positive one. 

19
00:01:04,600 --> 00:01:07,340
I don’t know if I can do it all the time! 

20
00:01:08,360 --> 00:01:10,960
For me, all the difficulties that arise 

21
00:01:11,180 --> 00:01:16,960
his invitations to continue in his state of mind. 

22
00:01:17,360 --> 00:01:24,680
I would even say that if we are unfairly criticized, 

23
00:01:24,980 --> 00:01:27,600
we can make something positive out of it. 

24
00:01:27,900 --> 00:01:33,580
For example, it forces you to take yourself less seriously. 

25
00:01:37,620 --> 00:01:40,340
What I like about this sangha, 

26
00:01:40,640 --> 00:01:42,860
is that we try to listen to each other a little more. 

27
00:01:43,140 --> 00:01:45,720
Because it takes time to listen to each other more, 

28
00:01:46,040 --> 00:01:50,460
to take into account some other points of view. 

29
00:01:50,760 --> 00:01:53,580
We know things are wrong, 

30
00:01:53,980 --> 00:01:58,120
but we always strive for improvement. 

31
00:01:59,400 --> 00:02:07,120
If you liked this video, feel free to like it and subscribe to the channel! 

