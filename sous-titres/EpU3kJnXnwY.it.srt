1
00:00:12,840 --> 00:00:20,310
In una poesia di Yoka Daishi,
un maestro cinese che ha scritto

2
00:00:20,310 --> 00:00:25,100
Il Cantico di Satori Immediato,

3
00:00:25,100 --> 00:00:27,100
lo Shôdôka

4
00:00:27,800 --> 00:00:35,760
ed era un discepolo di Maestro Eno, 
di cui ha ricevuto la trasmissione

5
00:00:35,760 --> 00:00:37,210
in un incontro durato un giorno e una notte.

6
00:00:37,210 --> 00:00:43,100
In seguito ha continuato a insegnare.

7
00:00:44,040 --> 00:00:50,040
La sua poesia: "Abbandonate i quattro elementi, non cercate più di accumulare,

8
00:00:50,740 --> 00:00:57,880
in pace e in assoluto completamento,

9
00:00:58,500 --> 00:01:01,940
Bevete e mangiate come volete.

10
00:01:02,360 --> 00:01:08,660
Tutti i fenomeni sono impermanenti, tutto è KU, senza noumenon, senza alcuna sostanza propria...

11
00:01:09,080 --> 00:01:14,080
e questo è proprio il grande
e completare il Buddha satori".

12
00:01:14,560 --> 00:01:17,820
Il maestro Deshimaru dice che qui la poesia tratta di

13
00:01:18,260 --> 00:01:23,620
dei Tre Tesori del Buddismo l’abbandono dei quattro elementi,

14
00:01:23,900 --> 00:01:30,300
i quattro elementi
è acqua, terra, fuoco, aria.

15
00:01:30,880 --> 00:01:35,440
Il secondo tesoro è la pace e la tranquillità e il
tranquillità di zazen, nirvana,

16
00:01:36,060 --> 00:01:44,400
il terzo l’impermanenza dei fenomeni, la consapevolezza, l’assenza di noumenon delle esistenze.

17
00:01:44,740 --> 00:01:50,120
Nella prima tesoreria, lasciate cadere i quattro elementi

18
00:01:50,120 --> 00:01:56,720
e non cerca più di catturarli, trattenerli, catturarli.

19
00:01:56,720 --> 00:02:04,300
Il maestro Deshimaru dice che non dobbiamo cercare il noumenon o l’ego.

20
00:02:04,300 --> 00:02:09,980
Tutti cercano l’ego del proprio corpo.
vuole possedere il suo corpo

21
00:02:10,700 --> 00:02:15,580
e da lì nascono sentimenti come la gelosia, l’egoismo, l’invidia.

22
00:02:15,580 --> 00:02:21,940
così nel primo tesoro,
impari ad arrenderti, a lasciarti andare,

23
00:02:21,940 --> 00:02:28,150
il suo corpo, il suo ego, la sua mente.
E’ interessante perché si può anche vedere

24
00:02:28,150 --> 00:02:33,580
che questi quattro elementi, abbandonando la
quattro elementi e non cercano più di

25
00:02:33,580 --> 00:02:39,560
per catturarli, per catturarli
in relazione al nostro mondo.

26
00:02:39,560 --> 00:02:44,000
Acqua, terra, fuoco e aria.

27
00:02:44,000 --> 00:02:48,840
Negli insegnamenti
dei Maestri Zen c’è sempre

28
00:02:48,850 --> 00:02:53,560
una profonda intuizione, e questo è
interessante, perché questa intuizione

29
00:02:53,560 --> 00:03:00,370
si unisce alla scienza moderna, la
scienze della vita

30
00:03:00,370 --> 00:03:07,120
e sempre più scienziati della vita ci dicono anche

31
00:03:07,120 --> 00:03:13,630
di abbandonare i quattro elementi,
Smettila di cercare di prenderli

32
00:03:13,630 --> 00:03:18,490
oceanologi, idrologi
spiegaci che gli oceani

33
00:03:18,490 --> 00:03:24,250
sono i veri polmoni della terra e
che dobbiamo smettere di acidificarli,

34
00:03:24,250 --> 00:03:30,280
per ucciderli, riempirli di plastica.
Specialisti della Terra,

35
00:03:30,280 --> 00:03:34,590
con la sua vita vegetale e animale, con quello che ha nelle sue viscere...

36
00:03:34,590 --> 00:03:40,900
le sue risorse, i suoi combustibili fossili,
le sue terre rare, dobbiamo lasciarle,

37
00:03:40,900 --> 00:03:48,060
dobbiamo lasciarli andare, gli agronomi, i geologi, i biochimici ci dicono...

38
00:03:48,060 --> 00:03:53,290
vulcanologi. Dobbiamo trovare un
altre funzioni.

39
00:03:53,290 --> 00:03:56,740
Non sono i Maestri Zen qui, è
davvero scienziati

40
00:03:56,740 --> 00:04:03,760
Anche loro hanno ora una profonda comprensione del principio di interdipendenza.

41
00:04:03,760 --> 00:04:10,810
Fuoco che gli umani usano senza cautela, senza sapere come controllarlo,

42
00:04:10,810 --> 00:04:15,959
fusione nucleare atomica,
il fuoco del sole,

43
00:04:15,959 --> 00:04:21,669
dobbiamo lasciarci andare, non lo sappiamo, è...
troppo complicato.

44
00:04:21,669 --> 00:04:27,340
Dobbiamo trovare un altro modo più semplice di lavorare e infine l’aria, che rendiamo irrespirabile...

45
00:04:27,340 --> 00:04:33,580
con un’atmosfera satura di CO2
metano, gas tossici,

46
00:04:33,960 --> 00:04:41,160
climatologi, meteorologi
glaciologi e molti altri,

47
00:04:41,160 --> 00:04:47,760
ci dicono: "Fermi, lasciatemi andare",
tenetevi fuori dai piedi

48
00:04:47,770 --> 00:04:52,330
così insegnano anche a tutti noi,
di smettere di cercare di afferrare, di smettere di afferrare...

49
00:04:52,330 --> 00:04:59,100
i quattro elementi. In shôdôka
"abbandona" il carattere cinese è "放 fang"

50
00:04:59,100 --> 00:05:06,070
"放 fang" significa anche libero significa
di’: "Arrenditi, lasciati andare, codardo".

52
00:05:12,600 --> 00:05:20,139
pascolare l’erba, in silenzio.
Lasciare le risorse fossili nella terra,

53
00:05:20,139 --> 00:05:27,190
Lasciate riposare la terra. Il maestro Deshimaru nel suo commento ha detto

54
00:05:27,190 --> 00:05:32,300
che non dovremmo cercare l’ego nel
corpo, tutti vogliono avere e possedere quel corpo.

55
00:05:32,300 --> 00:05:36,450
ma è lo stesso tra noi e
il pianeta.

56
00:05:36,450 --> 00:05:43,479
Lo consideriamo il nostro
corpo, ma non ne siamo a conoscenza.

57
00:05:43,479 --> 00:05:51,970
la sostanza divina,
spirituale, è una sciocchezza,

58
00:05:51,970 --> 00:05:59,420
ci comportiamo con il pianeta
come con i nostri corpi.

59
00:05:59,760 --> 00:06:07,680
Con zazen possiamo capirlo molto esattamente e se lasciamo andare i quattro elementi

60
00:06:08,080 --> 00:06:14,340
è la Terra del Buddha, la pace, il satori, che si realizza.

61
00:06:14,780 --> 00:06:22,280
in un’operazione semplice e armoniosa che non ferisce né uccide la vita.

62
00:06:22,460 --> 00:06:27,920
il seme di Buddha, la possibilità
per elevarsi al di sopra di esso.

63
00:06:28,260 --> 00:06:35,020
Quello che mi interessa qui è che ora gli scienziati stessi

64
00:06:35,700 --> 00:06:44,060
stanno iniziando a scoprire cosa stiamo facendo,
percepiamo e pratichiamo in modo naturale.

65
00:06:47,560 --> 00:06:55,560
Gli scienziati spiegano anche
che noi stessi siamo fatti di questi quattro elementi.

66
00:06:55,900 --> 00:07:02,920
ma anche che siamo un corpo vivente in cui

67
00:07:03,400 --> 00:07:09,379
ci sono più batteri, più virus, 
microbico

68
00:07:09,379 --> 00:07:18,439
abbiamo più virus di quanti ne abbiamo, credo, anche
di cellule viventi,

69
00:07:18,439 --> 00:07:23,649
funziona perché c’è un equilibrio, una protezione.

70
00:07:23,649 --> 00:07:29,019
Naturalmente, quando non c’è equilibrio, non c’è protezione...

71
00:07:29,019 --> 00:07:35,959
Per fortuna ci siamo potuti incontrare,
per conoscere zazen, per praticarlo,

72
00:07:35,959 --> 00:07:42,979
per farlo conoscere. È molto prezioso, non solo per noi, ma per tutte le esistenze.
