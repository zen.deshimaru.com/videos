1
00:00:00,180 --> 00:00:04,420
Ik begon met Zen te oefenen toen ik 16 was.

2
00:00:04,740 --> 00:00:07,660
Ik word 49.

3
00:00:07,860 --> 00:00:14,360
Ik had geprobeerd de houding van zazen alleen aan te nemen, op mijn bed.

4
00:00:14,960 --> 00:00:19,460
Ik hield het maximaal 2, 3, 4 minuten vol, het was echt heel pijnlijk.

5
00:00:19,700 --> 00:00:21,700
Ik kon het helemaal niet.

6
00:00:21,840 --> 00:00:30,800
Daar lieten ze me de houding zien en ik hield de normale zazen vast, ongeveer twee keer een half uur.

7
00:00:31,120 --> 00:00:33,420
We maken ons klaar om te vertrekken, en hier:

8
00:00:33,640 --> 00:00:36,100
“Ah, vergeet niet te betalen!”

9
00:00:36,540 --> 00:00:38,500
Ik dacht dat het gratis was…

10
00:00:38,880 --> 00:00:42,580
Ik vertrok zonder mijn toelage…

11
00:00:45,460 --> 00:00:48,680
Ik oefen in de Zen Dojo van Lyon, in Croix-Rousse.

12
00:00:49,280 --> 00:00:56,940
Het is een oude dojo gemaakt door Meester Deshimaru.

13
00:00:59,380 --> 00:01:02,340
Mijn naam is Christophe Desmurs.

14
00:01:02,700 --> 00:01:04,800
Mijn monniksnaam is Ryurin.

15
00:01:05,200 --> 00:01:09,700
Ik ben al 25 jaar een discipel van Meester Kosen.

16
00:01:10,000 --> 00:01:13,000
Het kostte me een tijdje om Meester Kosen te ontmoeten.

17
00:01:13,320 --> 00:01:17,480
Ik kende hem, ik deed vroeger sesshins met hem…

18
00:01:17,760 --> 00:01:21,000
Maar ik was niet erg close.

19
00:01:21,340 --> 00:01:26,980
Door omstandigheden die buiten mijn macht liggen, ben ik dichterbij gekomen.

20
00:01:27,260 --> 00:01:31,800
En ik was in staat om zijn leer te ontdekken.

21
00:01:32,160 --> 00:01:36,120
Dus ik heb tot vandaag met hem geoefend met zazen.

22
00:01:36,280 --> 00:01:39,800
Meester Kosen gaf me ongeveer tien jaar geleden,

23
00:01:40,060 --> 00:01:42,920
zoals het met veel van zijn vroegere volgelingen deed,

24
00:01:43,260 --> 00:01:46,900
de shiho, de transmissie van Dharma.

25
00:01:47,740 --> 00:01:50,740
Zenmeester, het is een samu, het is geen titel.

26
00:01:51,020 --> 00:01:57,420
Zoals we zeggen “meester”, het suggereert het idee dat men iets onder de knie heeft, maar hij is een samu.

27
00:01:58,240 --> 00:02:01,480
“Samu” is een Chinees woord, Japans.

28
00:02:01,740 --> 00:02:03,840
Dit zijn de taken die moeten worden uitgevoerd.

29
00:02:04,100 --> 00:02:06,380
Hij geeft ook les, hij is een samu.

30
00:02:06,560 --> 00:02:08,720
De Zen-meester doet zijn samu…

31
00:02:09,000 --> 00:02:11,020
Als hij klaar is met zijn samoerai, gaat hij naar huis.

32
00:02:11,300 --> 00:02:12,980
Hij gaat werken of niet…

33
00:02:13,280 --> 00:02:15,560
Hij heeft een vrouw en familie of niet…

34
00:02:15,980 --> 00:02:17,240
Het hangt af van elke persoon…

35
00:02:18,060 --> 00:02:21,180
En nu is hij bezig met iets anders…

36
00:02:21,500 --> 00:02:23,120
Dat is het leven!

37
00:02:23,640 --> 00:02:26,660
Maar de Zen-meester, hij is een discipel.

38
00:02:27,060 --> 00:02:35,860
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

