1
00:00:11,160 --> 00:00:13,720
Hello, how are you?

2
00:00:16,560 --> 00:00:21,280
The posture, taught by Master Deshimaru...

3
00:00:25,000 --> 00:00:28,460
He called it the Zen method...

4
00:00:31,920 --> 00:00:35,640
Start by having a stable base.

5
00:00:39,600 --> 00:00:43,160
This means that both knees touch the ground.

6
00:00:47,480 --> 00:00:50,540
And you place the perineum on the zafu [cushion].

7
00:00:53,860 --> 00:00:58,940
If you do the full lotus, you place one leg first.

8
00:01:04,220 --> 00:01:07,300
Then the other one.

9
00:01:07,900 --> 00:01:12,140
If you have good stability, you can really let go of the body.

10
00:01:12,170 --> 00:01:15,220
As they say: let go of the mind-body.

11
00:01:22,460 --> 00:01:24,320
Master Deshimaru said:

12
00:01:25,020 --> 00:01:28,640
"Zen is the new humanism for the 21st century."

13
00:01:29,560 --> 00:01:31,440
"It must always be fresh."

14
00:01:31,480 --> 00:01:34,000
"If you practice true zazen,"

15
00:01:34,060 --> 00:01:36,600
"chin tucked in,"

16
00:01:37,060 --> 00:01:39,980
The "stretched spine,"

17
00:01:40,580 --> 00:01:42,580
"only zazen,"

18
00:01:43,620 --> 00:01:46,270
"the rest doesn’t matter so much anymore."

19
00:01:46,320 --> 00:01:49,600
At the beginning of zazen, there are three bells.

20
00:01:52,040 --> 00:01:56,060
And then, when there is kin-hin, meditation is on,

21
00:02:00,530 --> 00:02:03,580
There are two bells.

22
00:02:15,980 --> 00:02:18,940
Stretch the knee of the front leg well.

23
00:02:22,900 --> 00:02:28,340
Press the ground with the root of the big toe of the front foot.

24
00:02:34,580 --> 00:02:36,580
Relax your shoulders well.

25
00:02:38,260 --> 00:02:41,480
At the end of expiration,

26
00:02:44,100 --> 00:02:46,100
we inspire,

27
00:02:47,560 --> 00:02:51,900
and we take a small step forward.

28
00:02:56,820 --> 00:03:00,060
Start again, exhale...

29
00:03:01,360 --> 00:03:05,180
The entire weight of the body is put on the front leg.

30
00:03:10,920 --> 00:03:13,760
The thumb of the left hand

31
00:03:16,120 --> 00:03:19,960
one encloses it in the fist.

32
00:03:22,400 --> 00:03:25,300
Then we take the right hand

33
00:03:26,960 --> 00:03:29,480
and we cover the left hand.

34
00:03:36,140 --> 00:03:40,040
Doing zazen by zooming is very practical...

35
00:03:40,260 --> 00:03:43,710
especially in these difficult times.

36
00:03:48,280 --> 00:03:53,440
But normally, it completes the practice in a real dojo.

37
00:03:58,860 --> 00:04:03,340
Obviously, we dedicate this practice that we do together.

38
00:04:12,540 --> 00:04:19,560
To all the people who are dedicated to healing and saving others.

39
00:04:34,720 --> 00:04:36,720
Hi Toshi. Hello.

40
00:04:36,780 --> 00:04:38,720
Good morning, master.

41
00:04:40,680 --> 00:04:43,940
Ah, here it is! Hello !

42
00:04:46,100 --> 00:04:48,100
Hello, Ingrid.

43
00:04:51,840 --> 00:04:54,640
Here is Grabriela, but which one?

44
00:04:57,460 --> 00:05:00,520
Ah, I didn’t recognize you with your glasses!

45
00:05:06,020 --> 00:05:09,160
I’m the translator, don’t you recognize me?
