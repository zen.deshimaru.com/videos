1
00:00:03,420 --> 00:00:07,900
Wat is geest?

2
00:00:08,220 --> 00:00:16,200
In het Chinees en Japans worden verschillende ideogrammen vertaald als “geest”.

3
00:00:16,880 --> 00:00:19,340
Soms wordt het vertaald als “hart”.

4
00:00:21,960 --> 00:00:23,560
Het is elk bestaan.

5
00:00:23,820 --> 00:00:25,240
Het is de levende gemeenschap.

6
00:00:25,500 --> 00:00:27,400
Dit is de gemeenschappelijke geest van de levende gemeenschap.

7
00:00:27,740 --> 00:00:28,900
Dat is de geest.

8
00:00:29,120 --> 00:00:30,780
Er is maar één geest.

9
00:00:32,000 --> 00:00:34,960
Wat schept de geest?

10
00:00:35,740 --> 00:00:37,780
Vanuit boeddhistisch oogpunt schept niets de geest.

11
00:00:38,100 --> 00:00:40,540
Omdat de geest zonder geboorte is.

12
00:00:40,880 --> 00:00:46,780
Het is zonder oorzaak of verdwijning.

13
00:00:47,060 --> 00:00:50,020
De geest is geschapen, maar niet geschapen.

14
00:00:50,500 --> 00:00:54,220
Is de geest de eerste oorzaak?

15
00:00:54,460 --> 00:00:57,820
Er kan geen eerste oorzaak zijn.

16
00:00:58,100 --> 00:00:59,940
Omdat de oorsprong zuiver is.

17
00:01:02,280 --> 00:01:05,420
Wat zou de eerste oorzaak zijn geweest?

18
00:01:05,840 --> 00:01:11,200
We begrijpen, zelfs intellectueel, dat als we in het verleden gaan, we de eerste oorzaak niet kunnen vinden.

19
00:01:11,660 --> 00:01:13,300
Dus de eerste oorzaak bestaat niet.

20
00:01:13,660 --> 00:01:15,640
De eerste oorzaak is het effect.

21
00:01:16,060 --> 00:01:16,900
Nu.

22
00:01:17,440 --> 00:01:20,940
Waar verblijft de geest?

23
00:01:21,280 --> 00:01:23,120
De geest bestaat altijd en verblijft overal.

24
00:01:23,420 --> 00:01:25,200
Of we nu in Zazen zijn of ergens anders.

25
00:01:25,400 --> 00:01:30,140
Er is geen speciale meditatie die de structuur van de geest verandert.

26
00:01:30,460 --> 00:01:32,240
De geest is altijd aanwezig.

27
00:01:32,520 --> 00:01:35,780
Wat is “geest-lichaam”?

28
00:01:36,520 --> 00:01:38,920
Materie en energie staan niet los van elkaar.

29
00:01:39,280 --> 00:01:44,600
Het zijn twee verschillende uitdrukkingen van hetzelfde.

30
00:01:47,080 --> 00:01:50,360
Lichaam en geest zijn dus twee verschillende uitdrukkingen van hetzelfde.

31
00:01:50,760 --> 00:01:52,280
Je moet het lichaam niet denigreren.

32
00:01:52,640 --> 00:01:53,980
Het lichaam is de geest.

33
00:01:54,340 --> 00:01:55,720
Materie is de geest.

34
00:01:56,220 --> 00:02:00,520
Wat is het effect van de gedachte op het geestelijk lichaam?

35
00:02:01,240 --> 00:02:06,120
Het denken materialiseert zich in de werkelijkheid.

36
00:02:07,020 --> 00:02:09,100
In de droom van de materie.

37
00:02:09,580 --> 00:02:11,040
Dromen gebeurt niet alleen als je slaapt.

38
00:02:11,480 --> 00:02:15,580
De droom van de materie is wat we “de werkelijkheid” noemen.

39
00:02:15,900 --> 00:02:17,880
Materiële realiteit.

40
00:02:18,400 --> 00:02:21,980
Wat kunnen we hieruit concluderen?

41
00:02:22,340 --> 00:02:27,080
De geest ziet altijd in de geest.

