1
00:00:00,180 --> 00:00:04,420
Comecei a praticar Zen aos 16 anos. 

2
00:00:04,740 --> 00:00:07,660
Estou fazendo 49 anos. 

3
00:00:07,860 --> 00:00:14,360
Eu tentei tomar a posição de zazen sozinho na minha cama. 

4
00:00:14,960 --> 00:00:19,460
Eu mantive por 2, 3, 4 minutos, foi realmente muito doloroso. 

5
00:00:19,700 --> 00:00:21,700
Eu não poderia fazer isso. 

6
00:00:21,840 --> 00:00:30,800
Lá eles me mostraram a pose e eu segurei o zazen normal por cerca de meia hora. 

7
00:00:31,120 --> 00:00:33,420
Estamos nos preparando para sair, e aqui: 

8
00:00:33,640 --> 00:00:36,100
"Ah, não se esqueça de pagar!" 

9
00:00:36,540 --> 00:00:38,500
Eu pensei que era grátis … 

10
00:00:38,880 --> 00:00:42,580
Saí sem a minha mesada … 

11
00:00:45,460 --> 00:00:48,680
Eu pratico no Zen Dojo de Lyon, em Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
É um antigo dojo feito pelo Mestre Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
Meu nome é Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
Meu nome de monge é Ryurin. 

15
00:01:05,200 --> 00:01:09,700
Sou discípulo do Mestre Kosen há 25 anos. 

16
00:01:10,000 --> 00:01:13,000
Levei um tempo para conhecer o Mestre Kosen. 

17
00:01:13,320 --> 00:01:17,480
Eu o conhecia, costumava fazer sessões com ele … 

18
00:01:17,760 --> 00:01:21,000
Mas eu não estava muito perto. 

19
00:01:21,340 --> 00:01:26,980
Cheguei mais perto devido a circunstâncias fora do meu controle. 

20
00:01:27,260 --> 00:01:31,800
E eu pude descobrir seus ensinamentos. 

21
00:01:32,160 --> 00:01:36,120
Então, eu pratiquei zazen com ele até hoje. 

22
00:01:36,280 --> 00:01:39,800
Mestre Kosen me deu cerca de dez anos atrás, 

23
00:01:40,060 --> 00:01:42,920
como fez com muitos de seus ex-seguidores, 

24
00:01:43,260 --> 00:01:46,900
o shiho, a transmissão do dharma. 

25
00:01:47,740 --> 00:01:50,740
Mestre Zen, é um samu, não é um título. 

26
00:01:51,020 --> 00:01:57,420
Como dizemos "mestre", sugere a idéia de que alguém dominou alguma coisa, mas ele é um samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" é uma palavra chinesa, japonesa. 

28
00:02:01,740 --> 00:02:03,840
Essas são as tarefas a serem executadas. 

29
00:02:04,100 --> 00:02:06,380
Ele também ensina, ele é um samu. 

30
00:02:06,560 --> 00:02:08,720
O mestre Zen faz sua samu … 

31
00:02:09,000 --> 00:02:11,020
Quando ele termina seu samurai, ele volta para casa. 

32
00:02:11,300 --> 00:02:12,980
Ele vai trabalhar ou não … 

33
00:02:13,280 --> 00:02:15,560
Ele tem esposa e família ou não … 

34
00:02:15,980 --> 00:02:17,240
Depende de cada pessoa … 

35
00:02:18,060 --> 00:02:21,180
E agora ele está trabalhando em outra coisa … 

36
00:02:21,500 --> 00:02:23,120
Essa é a vida! 

37
00:02:23,640 --> 00:02:26,660
Mas o mestre zen, ele é um discípulo. 

38
00:02:27,060 --> 00:02:35,860
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

