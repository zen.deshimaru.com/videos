1
00:00:18,400 --> 00:00:23,380
Als je aan iemand wilt uitleggen hoe je
mediteert dan leg je hem het beste de

2
00:00:23,380 --> 00:00:29,619
houding uit waarin je zit. Nog sterker
mediteren wordt eigenlijk wel genoemd

3
00:00:29,619 --> 00:00:37,360
het beoefenen van de houding. Je beoefent
de Boeddhahouding. En dat dat is omdat je

4
00:00:37,360 --> 00:00:43,239
als je mediteert, dan zijn je lichaam en je
geest eigenlijk één.

5
00:00:43,239 --> 00:00:48,190
Dus die lichaamshouding, de houding
waarin je zit, beïnvloedt je geest. En

6
00:00:48,190 --> 00:00:51,070
vice versa:
jouw geesteshouding beïnvloedt ook je

7
00:00:51,070 --> 00:00:53,369
lichaamshouding.

8
00:00:53,470 --> 00:00:58,000
Dus laten we nu die lichaamshouding
bestuderen. Maar je ziet, als eerste

9
00:00:58,000 --> 00:01:01,989
ik zit op een kussen. Dit kussen maakt
mogelijk mogelijk

10
00:01:01,989 --> 00:01:11,830
dat mijn bekken deze positie inneemt: mijn
bekken eigenlijk ietsje naar vorige gekanteld is, bijna verticaal is, en dat ik mijn buik kan ontspannen.

11
00:01:11,830 --> 00:01:14,880
Dus je zit op een kussen.

12
00:01:16,180 --> 00:01:20,590
Zo’n kussen wordt een zafu
genoemd. De hoogte van een kussen is dus

13
00:01:20,590 --> 00:01:25,360
best belangrijk:
Het kussen zo zou zo moeten zijn

14
00:01:25,360 --> 00:01:29,650
dat precies het bekken uit zichzelf naar
voren kantelt,

15
00:01:29,650 --> 00:01:32,140
zonder dat je daar moeite voor te doen
zonder je spierspanning vor hoeft te

16
00:01:32,140 --> 00:01:36,010
gebruiken.
Dus als je een groot lichaam hebt is het

17
00:01:36,010 --> 00:01:42,010
handig om een groot kussen te hebben,
een goed gevuld kussen en als je wat

18
00:01:42,010 --> 00:01:46,570
kleiner van postuur bent, kun je een
dunner kussen nemen

19
00:01:46,570 --> 00:01:49,540
Er zijn kussens in de handel, die hebben
maar een maat.

20
00:01:49,540 --> 00:01:52,840
Dit is een kussen daar zit kapok in en
dat kun je bijvullen,

21
00:01:52,840 --> 00:01:57,570
dus die kun je precies op de goede
hoogte voor jou brengen.

22
00:01:59,350 --> 00:02:05,290
Ik ga in deze uitleg uit van de
van de houding waarin je zit

23
00:02:05,290 --> 00:02:09,039
op een kussen met knieën op de grond,
dit wordt de halve-lotushouding

24
00:02:09,039 --> 00:02:13,000
genoemd. Sommige mensen hebben
moeite om deze houding aan te nemen,

25
00:02:13,000 --> 00:02:17,739
die zitten liever op een bankje. Dat kan,
ook dan kun je

26
00:02:17,739 --> 00:02:20,470
je rug in de goede houding
hebben

27
00:02:20,470 --> 00:02:24,640
Je kunt ook op een stoel zitten, dat ga
ik allemaal niet uitleggen in deze in

28
00:02:24,640 --> 00:02:27,549
deze sessie
ik ga hier uit van de houding op een

29
00:02:27,549 --> 00:02:30,870
kussen beide knieën op de grond

30
00:02:31,400 --> 00:02:36,090
Als je de houding aanneemt,
het eerste wat je doet is: je brengt een

31
00:02:36,090 --> 00:02:41,700
voet tegen je kussen aan, knie op de
grond en

32
00:02:41,700 --> 00:02:46,590
van het andere been breng je voet op je
dijbeen en deze knie hier. Dus je zit

33
00:02:46,590 --> 00:02:51,600
niet met
gekruiste benen. Deze voet tegen het

34
00:02:51,600 --> 00:02:55,250
kussen aan, deze omhoog

35
00:02:55,470 --> 00:03:01,680
dit is de halve lotus houding. Als die te
moeilijk voor je is, als je al een beetje

36
00:03:01,680 --> 00:03:05,070
stroef bent in je heupgewrichten

37
00:03:05,070 --> 00:03:09,660
zazen, de meditatie zal je
helpen om daar ook losser in te worden.

38
00:03:09,660 --> 00:03:14,970
Als je nu nog een beetje stroef bent, kun
je eventueel dit been verder naar voren

39
00:03:14,970 --> 00:03:17,670
leggen, 
deze voet verder naar voorleggen en ook

40
00:03:17,670 --> 00:03:22,800
dan heb je nog steeds je twee knieën op
de grond. Gaat dan nog steeds

41
00:03:22,800 --> 00:03:27,780
je knie een beetje omhoog, dan
kun je in dat geval een handdoek nemen

42
00:03:27,780 --> 00:03:31,740
en dan probeer je hem zo dun mogelijk onder je
knie te leggen,

43
00:03:31,740 --> 00:03:35,610
een zo dun mogelijke handdoek, net wat je
nodig hebt om de knie

44
00:03:35,610 --> 00:03:40,970
steun te geven. Ook dan vormen zich de
twee knieën op de grond.

45
00:03:44,080 --> 00:03:50,480
Dus die knieën op de grond, je bekken op het kussen,

46
00:03:50,480 --> 00:03:54,650
dit vormt een driehoek en die driehoek
dat zou je kunnen noemen dat is de

47
00:03:54,650 --> 00:03:59,360
sokkel van de houding, een stevige stabiele sokkel.

48
00:03:59,360 --> 00:04:03,590
Je bekken strekt zich, je bekken
kantelt iets naar voren, het is bijna

49
00:04:03,590 --> 00:04:12,920
verticaal,
en je wervelkolom is precies in balans bovenop je bekken en de wervelkolom die strekt

50
00:04:12,920 --> 00:04:16,190
zich naar boven toe
en die strekking gaat door tot aan de

51
00:04:16,190 --> 00:04:19,030
kruin van je hoofd.

52
00:04:19,520 --> 00:04:22,970
Je kunt dus waarnemen, je kunt dus voelen
dat enerzijds je knieën op de grond

53
00:04:22,970 --> 00:04:28,160
drukken en anderzijds je kruin naar
boven reikt, de kruin van je hoofd strekt

54
00:04:28,160 --> 00:04:30,580
zich naar boven.

55
00:04:32,540 --> 00:04:37,500
Vervolgens, en dat is een belangrijk
detail

56
00:04:37,500 --> 00:04:41,300
van de houding, je
kin is ingetrokken, zonder verkramping te

57
00:04:41,300 --> 00:04:45,320
veroorzaken. Dus je zit niet zo maar je zit
ook niet zo.

58
00:04:45,320 --> 00:04:47,780
De kin is ingetrokken en je nek is
gestrekt,

59
00:04:47,780 --> 00:04:51,980
je kunt het eigenlijk heel goed voelen we
zeggen ook wel: je nek opent zich naar

60
00:04:51,980 --> 00:04:56,060
achteren toe, het is alsof je je hoofd
precies in het verlengde van je

61
00:04:56,060 --> 00:05:02,770
wervelkolom plaatst en ook dat maakt dat
de houding wakker is.

62
00:05:06,360 --> 00:05:11,669
Een ander belangrijk aspect van de
houding is de positie van je ogen.

63
00:05:11,669 --> 00:05:14,280
Je ogen zijn tijdens de meditatie niet
gesloten,

64
00:05:14,280 --> 00:05:17,599
ze zijn halfopen halfdicht, je laat je
ogen zakken

65
00:05:17,599 --> 00:05:21,629
45 graden naar beneden toe. Je kunt ook
zeggen

66
00:05:21,629 --> 00:05:26,060
je ogen zijn gericht ongeveer een meter
voor je op de grond.

67
00:05:30,750 --> 00:05:35,280
En daarbij ben je niet aan het
kijken, je bent niet actief aan het kijken

68
00:05:35,280 --> 00:05:41,270
Je kunt wel alles om je heen waarnemen
zonder dat je er aandacht aan geeft.

69
00:05:44,189 --> 00:05:48,509
De positie van je handen: de handen
vormen eigenlijk gezamenlijk een soort

70
00:05:48,509 --> 00:05:54,990
van mini-houding. Je rechterhand is onder
je linkerhand is boven en ze vormen een

71
00:05:54,990 --> 00:05:58,710
schaaltje.
Je duimen raken elkaar horizontaal en

72
00:05:58,710 --> 00:06:01,830
zijn precies boven je middelvingers.
Je ziet, de zijkant van de handen is

73
00:06:01,830 --> 00:06:08,400
tegen mijn buik aan. Nog preciezer kun je
zien dat de twee middelvingers precies

74
00:06:08,400 --> 00:06:12,449
boven elkaar zijn, dus je schuift je
handen niet over elkaar heen

75
00:06:12,449 --> 00:06:22,100
maar precies hier en die duimen raken
elkaar heel zachtjes.

76
00:06:22,200 --> 00:06:25,860
Om die positie van de handen goed te kunnen laten rusten ...

77
00:06:25,860 --> 00:06:30,300
Kijk als iemand volledige lotus doet, dus
ook nog het andere been naar boven

78
00:06:30,300 --> 00:06:34,409
gevouwen is, dan kun je handen op je
hielen laten rusten.

79
00:06:34,409 --> 00:06:39,330
Als dat niet het geval is, is het handig
om een handdoek te gebruiken die over je

80
00:06:39,330 --> 00:06:46,080
dijbenen legt en dit vormt een soort van
hangmatje voor je handen.

81
00:06:46,080 --> 00:06:49,250
Hierop kun je handen laten rusten.

82
00:06:53,289 --> 00:07:00,460
En nu richt je je aandacht op je houding,
op de diverse punten van de houding:

83
00:07:00,770 --> 00:07:03,550
ingetrokken kin,

84
00:07:06,050 --> 00:07:15,219
de positie van je ogen, je duimen,
je handen.

85
00:07:18,249 --> 00:07:22,709
Je ontspant je mond en je kaken.

86
00:07:23,229 --> 00:07:27,279
Je tong is tegen je gehemelte aan en het
puntje van je tong precies achter je

87
00:07:27,279 --> 00:07:29,459
voortanden.

88
00:07:33,699 --> 00:07:36,689
Je ontspant je schouders.

89
00:07:37,359 --> 00:07:48,329
Je ellebogen staan ietsje van je lichaam
af en je ontspant je buik.

90
00:07:57,740 --> 00:08:02,960
Als je nu een tijdje zo zit dan ga je
een paar dingen op vallen

91
00:08:02,960 --> 00:08:06,889
waar je je normaal niet bewust van bent.

92
00:08:06,889 --> 00:08:16,069
Het eerste wat je opvalt is je
ademhaling. De aanwijzing voor de

93
00:08:16,069 --> 00:08:21,770
ademhaling is: 
hij is al goed zoals die is.

94
00:08:21,770 --> 00:08:27,139
Dus we raden niet aan om de ademhaling
kunstmatig te beïnvloeden. De grootste

95
00:08:27,139 --> 00:08:31,370
invloed die de ademhaling krijgt, gaat
uit van de houding: als je houding goed

96
00:08:31,370 --> 00:08:36,080
in balans is, als je goed kunt ontspannen, 
strekken en ontspannen tegelijkertijd,

97
00:08:36,200 --> 00:08:39,409
dan zal die ademhaling langzamerhand uit zichzelf

98
00:08:39,409 --> 00:08:43,070
steeds meer dieper worden, steeds meer

99
00:08:43,070 --> 00:08:47,360
gebruik maken van je buik.
Je uitademing wordt waarschijnlijk langer

100
00:08:47,360 --> 00:08:53,060
en je inademing is kort en spontaan. Maar
dat hoef je niet te beïnvloeden, het is een

101
00:08:53,060 --> 00:08:58,580
proces dat uit zichzelf plaatsvindt.
Met andere woorden de ademhaling is nu

102
00:08:58,580 --> 00:09:02,350
al goed zoals die is.

103
00:09:02,350 --> 00:09:09,100
Concentreer je op de ademhaling.

104
00:09:09,560 --> 00:09:15,620
Een tweede ding wat je gaat opvallen,
is dat je je gedachten waarneemt,

105
00:09:15,620 --> 00:09:19,210
je neemt waar dat je aan het denken bent.

106
00:09:20,230 --> 00:09:25,300
De aanwijzing voor de gedachtes is de
volgende:

107
00:09:25,300 --> 00:09:32,000
eigenlijk, je richt je aandacht op de houding en op de ademhaling, op het volgen van je ademhaling

108
00:09:32,000 --> 00:09:33,590
met andere woorden: je richt je aandacht

109
00:09:33,590 --> 00:09:38,420
niet op je gedachten, 
de gedachten mogen gerust langs komen,

110
00:09:38,420 --> 00:09:45,590
je gaat niet doordenken op een gedachte. We gebruiken ook wel de uitdrukking:

111
00:09:45,590 --> 00:09:50,000
de gedachtes trekken voorbij zoals
wolken aan de hemel.

112
00:09:50,000 --> 00:09:53,200
In de praktijk ziet het er zo uit:

113
00:09:55,940 --> 00:10:00,070
je mediteert en er komt een gedachte op...

114
00:10:01,250 --> 00:10:05,870
... op dit moment merk je op dat je in een
gedachte zit, dan laat je hem vriendelijk

115
00:10:05,870 --> 00:10:12,580
los en je brengt je aandacht terug naar
de houding en de ademhaling.

116
00:10:14,240 --> 00:10:18,370
Eventjes later komt er een nieuwe
gedachte op ...

117
00:10:19,730 --> 00:10:28,010
... nu merk je hem op, je laat hem los, je laat hem vriendelijk los en je keert

118
00:10:28,010 --> 00:10:30,880
terug tot de houding.

119
00:10:33,250 --> 00:10:37,220
Het is niet belangrijk hoeveel
gedachtes of

120
00:10:37,220 --> 00:10:43,279
hoe weinig gedachtes je hebt. Misschien
heb je net een heel druk leven, heb je

121
00:10:43,279 --> 00:10:47,420
veel spanning om je heen,
heb je heel veel gedachtes en op een

122
00:10:47,420 --> 00:10:51,940
ander moment zul je weinig gedachtes
hebben, dat maakt niet uit.

123
00:10:52,010 --> 00:10:56,540
We vergelijken wel het proces met de
gedachtes met een fles met water, een

124
00:10:56,540 --> 00:11:02,930
glas met water, waarin een handje zand is,
een handje aarde, en als je zo’n glas

125
00:11:02,930 --> 00:11:06,100
schudt en je zet het neer dan is het
water is troebel

126
00:11:06,100 --> 00:11:10,730
als je nu een tijdje wacht, gaat die
aarde langzamerhand naar de bodem en wordt

127
00:11:10,730 --> 00:11:14,560
het glas helder. Dat is wat gebeurt met je
geest.

128
00:11:14,560 --> 00:11:20,180
Dus in het begin van de meditatie zul je
waarschijnlijk veel gedachtes hebben of

129
00:11:20,180 --> 00:11:24,740
onrustige gedachtes en als je wat
langer zit, als je de meditatie langer

130
00:11:24,740 --> 00:11:28,610
volhoudt en je keert iedere keer weer
terug tot de houding en de ademhaling,

131
00:11:28,610 --> 00:11:33,050
dan langzamerhand worden de gedachtes, krijg je waarschijnlijk wat minder gedachtes of

132
00:11:33,050 --> 00:11:41,680
ze zijn minder dwingend en en ontstaat
er dus helderheid in je hoofd.

133
00:11:46,860 --> 00:11:53,220
Dus je ziet de houding is een samenspel
van strekking, van aanwezigheid, van strekking

134
00:11:53,220 --> 00:11:59,850
in je wervelkolom, 
van aanwezigheid en van ontspanning.

135
00:11:59,850 --> 00:12:04,230
Het is een combinatie van
strekking en ontspanning van

136
00:12:04,230 --> 00:12:09,540
aanwezigheid en loslaten. En diezelfde
combinatie die vind je ook terug in de

137
00:12:09,600 --> 00:12:12,540
geesteshouding.
in de geesteshouding is het een

138
00:12:12,620 --> 00:12:15,700
combinatie van van aandacht geven

139
00:12:19,700 --> 00:12:23,760
en observeren, loslaten.

140
00:12:28,960 --> 00:12:34,660
Bij een meditatie: probeer niet
te denken in termen van goed of slecht

141
00:12:34,660 --> 00:12:38,860
van ik had een goede meditatie of deze
meditatie was niet goed,

142
00:12:38,860 --> 00:12:42,730
eigenlijk juist de crux is dat je dat
loslaat:

143
00:12:42,730 --> 00:12:47,070
de meditatie is goed zoals die is.

144
00:12:49,300 --> 00:12:53,410
De diepste beoefening die je kunt doen
is eigenlijk

145
00:12:53,410 --> 00:12:59,350
in de zen noemen we dat mushotoku, en dat betekent zonder doel

146
00:12:59,350 --> 00:13:05,140
zonder winstoogmerk. Alleen maar zitten,
laat je doel los. Natuurlijk heb je in

147
00:13:05,140 --> 00:13:10,140
begin een doel en wil je iets en
zo maar tijdens de meditatie zelf:

148
00:13:10,140 --> 00:13:15,520
alleen de meditatie, laat je doel los.
Probeer niet iets te bereiken, stilte

149
00:13:15,520 --> 00:13:19,900
te bereiken of concentratie of weet ik
veel wat je zou willen bereiken.

150
00:13:19,900 --> 00:13:22,260
Laat het los

151
00:13:23,380 --> 00:13:29,350
Als je begint te mediteren, hoeveel tijd
is handig? Nou, ik raad sowieso aan als je

152
00:13:29,350 --> 00:13:34,660
in de buurt van een dojo bent, ga
regelmatig een keer zo’n dojo bezoeken,

153
00:13:34,660 --> 00:13:39,430
een plek waar andere beoefenaars zitten,
laat je houding corrigeren door oudere

154
00:13:39,430 --> 00:13:45,430
beoefenaars. En als je thuis beoefent, dan
stel jezelf een bepaalde tijd,

155
00:13:45,430 --> 00:13:50,560
bijvoorbeeld 15 of 20 minuten. Die stel
je in op je telefoon, je zet je telefoon

156
00:13:50,560 --> 00:13:54,760
op vliegtuigstand en je stelt je timer in
op 15 minuten, en dan onafhankelijk of je

157
00:13:54,820 --> 00:13:59,100
meditatie nu gemakkelijk is, of je
gemakkelijk concentreert of dat je het lastig vind,

158
00:13:59,100 --> 00:14:04,580
je zit gewoon 15 minuten of 20 minuten,
het is mooie tijd als je alleen zit.

159
00:14:04,580 --> 00:14:09,060
Een, twee keer per week en dan kun je het
langzamerhand opbouwen. Je kunt het

160
00:14:09,070 --> 00:14:13,560
opbouwen tot iets langer zitten of
misschien meer dagen zitten. Doe het zo ...

161
00:14:13,560 --> 00:14:16,690
Het lastigste met meditatie is niet
beginnen,

162
00:14:16,690 --> 00:14:20,650
het lastigste met meditatie is het
volhouden, is ermee doorgaan

163
00:14:20,650 --> 00:14:24,270
Dus kies een ritme uit dat je kunt
volhouden

164
00:14:25,330 --> 00:14:29,160
Oké, veel succes!
