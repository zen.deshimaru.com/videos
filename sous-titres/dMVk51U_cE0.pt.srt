1
00:00:07,600 --> 00:00:09,910
Eu farei a iniciação.

2
00:00:10,080 --> 00:00:13,570
Há pelo menos uma pessoa registrada.

3
00:00:13,850 --> 00:00:17,860
Então passaremos pela vizinhança!

4
00:00:29,760 --> 00:00:32,305
As medidas de proteção são:

5
00:00:33,225 --> 00:00:36,380
álcool e gel, várias vezes dentro e fora.

6
00:00:36,530 --> 00:00:39,390
A distância social de um metro e meio.

7
00:00:39,550 --> 00:00:44,960
No lobby e no vestiário, no dojo.

8
00:00:45,140 --> 00:00:48,740
Nós usamos a máscara o tempo todo.

9
00:00:48,910 --> 00:00:52,370
Nós o tiramos para fazer zazen, nós o mantemos no quimono.

10
00:00:52,450 --> 00:00:55,175
E colocá-lo de volta durante a caminhada dos parentes.

11
00:00:55,325 --> 00:00:58,940
Mantemos as janelas abertas, apesar do barulho ou do frio.

12
00:01:00,330 --> 00:01:03,280
Bom dia, são 6:15 da manhã.

13
00:01:03,360 --> 00:01:08,110
Vamos para o dojo para chegar com tempo de preparar tudo.

14
00:01:08,330 --> 00:01:11,490
Aproveite o City Tour de Barcelona!

15
00:01:35,120 --> 00:01:37,935
Nosso dojo na Rua Tordera está em construção.

16
00:01:38,005 --> 00:01:44,940
Estamos nos preparando para o meio dia de zazen.
dirigido pelo maître Pierre Soko no final do mês.

17
00:01:45,020 --> 00:01:50,790
É uma sala em Barcelona onde são praticadas atividades relacionadas ao budismo.

18
00:01:50,900 --> 00:01:56,587
Alugamo-lo para continuar praticando zazen.

19
00:01:56,700 --> 00:02:00,730
Com a covida, desprogramamos todas as atividades.

20
00:02:00,990 --> 00:02:08,090
Não podemos fazer sesshin, nem de dia, nem de classe.

21
00:02:08,470 --> 00:02:14,610
Eu não posso fazer nada de especial para difundir a prática: estamos presos.

22
00:02:14,880 --> 00:02:19,040
Mantemos uma prática o melhor que podemos.

23
00:02:19,143 --> 00:02:25,460
Mas não podemos fazer propaganda ou desenvolver o dojo Zen.

24
00:02:25,760 --> 00:02:30,375
A única publicidade que podemos fazer é na web e nas redes sociais.

25
00:02:30,536 --> 00:02:34,905
Mas nós não colocamos cartazes na rua,
algo que fazemos há 20 anos.

26
00:02:35,521 --> 00:02:38,825
Muito bem, então. Mais um dia. Pandemia.

27
00:02:38,955 --> 00:02:43,280
Tivemos muitos praticantes hoje, estamos felizes.

28
00:02:50,000 --> 00:03:00,060
A cobiça tem perturbado minha vida familiar e profissional.

29
00:03:02,300 --> 00:03:03,950
Eu sou francês.

30
00:03:04,041 --> 00:03:12,920
Para o coronavírus, estou seguindo as precauções usuais:
a máscara, lavando as mãos...

31
00:03:13,440 --> 00:03:15,390
Olá, meu nome é Helena.

32
00:03:15,500 --> 00:03:18,140
Há 15 anos eu pratico no Ryokan Dojo.

33
00:03:18,200 --> 00:03:25,850
É muito difícil para mim não poder praticar no dojo.

34
00:03:26,160 --> 00:03:30,660
Mas você tem que ter cuidado, aceitar as coisas como elas são.

35
00:03:30,760 --> 00:03:34,080
Estamos no meio de uma segunda onda.

36
00:03:34,350 --> 00:03:38,468
Faço parte do grupo de risco e tenho que ficar em casa.

37
00:03:39,100 --> 00:03:42,200
Comecei a praticar Zen no "Zen y playa".

38
00:03:42,440 --> 00:03:48,610
Sou atriz e o mundo da cultura é muito incerto...

39
00:03:48,810 --> 00:03:53,970
Olá, meu nome é David e sou um praticante no dojo.

40
00:03:54,070 --> 00:03:59,110
Apesar da cobiça, eu me sinto seguro aqui.

41
00:03:59,230 --> 00:04:01,877
Eu realmente quero praticar zazen.

42
00:04:02,000 --> 00:04:06,200
Vou voltar para a iniciação, porque eu costumava praticar no dojo da Rua Tordera.

43
00:04:06,520 --> 00:04:09,140
Eu tive que refazer uma iniciação.

44
00:04:09,260 --> 00:04:16,910
Ultimamente, tenho sentido muito medo ultimamente.

45
00:04:20,100 --> 00:04:23,890
Quero tanto cuidar de mim quanto continuar a viver.

46
00:04:44,720 --> 00:04:49,120
Crescer espiritualmente.

47
00:04:51,800 --> 00:04:59,050
Mestre Dogen nos convida para uma prática altruísta e abnegada.

48
00:05:16,070 --> 00:05:17,650
Olá.

49
00:05:40,030 --> 00:05:42,400
Aí está. Fim do zazen.

50
00:05:42,660 --> 00:05:46,460
Eu pegarei o metrô por 40 minutos.

51
00:05:46,690 --> 00:05:51,340
E o faremos novamente amanhã de manhã, às 7 horas, para o primeiro zazen.

52
00:05:52,015 --> 00:05:56,015
São 21 horas e, como você pode ver, Barcelona está deserta.

53
00:05:57,239 --> 00:05:58,639
É assim que as coisas são.

54
00:05:58,729 --> 00:06:01,239
Tenha uma boa noite, nos vemos em breve!
