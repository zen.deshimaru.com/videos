1
00:00:07,600 --> 00:00:09,910
Voy a hacer la introducción.

2
00:00:10,080 --> 00:00:13,570
Hay al menos una persona registrada.

3
00:00:13,850 --> 00:00:17,860
¡Vamos a atravesar el municipio!

4
00:00:29,760 --> 00:00:32,305
Las medidas de protección son:

5
00:00:33,225 --> 00:00:36,380
alcohol y gel, varias veces, al entrar y al salir.

6
00:00:36,530 --> 00:00:39,390
La distancia social de un metro y medio.

7
00:00:39,550 --> 00:00:44,960
Al circular, en los vestuarios, y en el dojo.

8
00:00:45,140 --> 00:00:48,740
Usamos la mascarilla todo el tiempo.

9
00:00:48,910 --> 00:00:52,370
La sacamos para hacer zazen, la paremos en el kimono.

10
00:00:52,450 --> 00:00:55,175
Y la ponemos por la caminata de kin-hin.

11
00:00:55,325 --> 00:00:58,940
Mantenemos las ventanas abiertas, aunque haya frío o ruido.

12
00:01:00,330 --> 00:01:03,280
Buen día, son las 6:15 de la mañana.

13
00:01:03,360 --> 00:01:08,110
No dirigimos para el dojo para llegar con tiempo y preparar todo.

14
00:01:08,330 --> 00:01:11,490
¡Que disfruten del Barcelona City Tour!

15
00:01:35,120 --> 00:01:37,935
Nuestro dojo en la calle Tordera está en obras de renovación.

16
00:01:38,005 --> 00:01:44,940
Nos estamos preparando para el medio día de zazen...
dirigida por el maestro Pierre Soko a finales de mes.

17
00:01:45,020 --> 00:01:50,790
Es una sala de Barcelona donde se practican actividades relacionadas con el budismo.

18
00:01:50,900 --> 00:01:56,587
La alquilamos para seguir practicando zazen.

19
00:01:56,700 --> 00:02:00,730
Con el covid, hemos desprogramado todas las actividades.

20
00:02:00,990 --> 00:02:08,090
No podemos hacer de sesshin, ni de día, ni de conferencia.

21
00:02:08,470 --> 00:02:14,610
No puedo hacer nada especial para difundir la práctica: estamos atascados.

22
00:02:14,880 --> 00:02:19,040
Mantenemos una práctica lo mejor posible.

23
00:02:19,143 --> 00:02:25,460
Pero no podemos promover o desarrollar el dojo Zen.

24
00:02:25,760 --> 00:02:30,375
La única publicidad que podemos hacer es en la web y en las redes sociales.

25
00:02:30,536 --> 00:02:34,905
Pero no ponemos carteles en la calle,
algo que hemos estado haciendo durante 20 años.

26
00:02:35,521 --> 00:02:38,825
Bueno. Un día más. Pandemia.

27
00:02:38,955 --> 00:02:43,280
Hemos tenido bastante practicantes hoy,
estamos contentos.

28
00:02:50,000 --> 00:03:00,060
El covid ha perturbado mi vida familiar y profesional.

29
00:03:02,300 --> 00:03:03,950
Soy francés.

30
00:03:04,041 --> 00:03:12,920
Para el coronavirus, sigo las precauciones habituales:
mascarilla, lavado de manos...

31
00:03:13,440 --> 00:03:15,390
Hola, soy Helena.

32
00:03:15,500 --> 00:03:18,140
He estado practicando en el Dojo Ryokan durante 15 años.

33
00:03:18,200 --> 00:03:25,850
Es muy difícil para mí no poder practicar en el dojo.

34
00:03:26,160 --> 00:03:30,660
Pero hay que ser prudente,
aceptar las cosas como son.

35
00:03:30,760 --> 00:03:34,080
Estamos en plena segunda ola.

36
00:03:34,350 --> 00:03:38,468
Soy parte del grupo de riesgo y tengo que quedarme en casa.

37
00:03:39,100 --> 00:03:42,200
Empecé a practicar Zen en "Zen y playa".

38
00:03:42,440 --> 00:03:48,610
Soy actriz y el mundo de la cultura
está sujeto a mucha incertidumbre...

39
00:03:48,810 --> 00:03:53,970
Hola, me llamo David y soy practicante en el dojo.

40
00:03:54,070 --> 00:03:59,110
A pesar de esta situación, me siento seguro aquí.

41
00:03:59,230 --> 00:04:01,877
Y tengo muchas ganas de practicar zazen.

42
00:04:02,000 --> 00:04:06,200
Vuelvo a la introducción porque antes practicaba
en el dojo de la calle Tordera.

43
00:04:06,520 --> 00:04:09,140
Y me hace falta tomar un poco de introducción.

44
00:04:09,260 --> 00:04:18,470
Últimamente, he estado sintiendo mucho miedo.

45
00:04:19,660 --> 00:04:23,890
Quiero cuidarme y vivir.

46
00:04:44,720 --> 00:04:49,120
Para crecer espiritualmente.

47
00:04:51,800 --> 00:04:59,050
El Maestro Dogen nos invita a una práctica altruista, desinteresada.

48
00:05:16,070 --> 00:05:17,650
Buenos días!

49
00:05:40,030 --> 00:05:42,400
Aquí es. Fin del zazen.

50
00:05:42,660 --> 00:05:46,460
Voy a tomar el metro durante 40 minutos.

51
00:05:46,690 --> 00:05:51,340
Y lo haremos de nuevo mañana por la mañana
a las 7 en punto para el primer zazen.

52
00:05:52,015 --> 00:05:56,015
Son las 9 de la noche y como puedes ver,
Barcelona está desierta.

53
00:05:57,239 --> 00:05:58,639
Así son las cosas.

54
00:05:58,729 --> 00:06:01,899
Que tengas una buena noche, ¡hasta pronto!
