1
00:00:09,675 --> 00:00:13,085
Hello. Today, I would like to explain to you

2
00:00:13,280 --> 00:00:15,940
how to practice a meditation walk

3
00:00:16,760 --> 00:00:21,800
that we practice in Zen, this walk is called kin-hin.

4
00:00:22,180 --> 00:00:25,640
It’s a fairly short walk,

5
00:00:25,920 --> 00:00:30,740
that one practices between two moments of sitting meditation

6
00:00:30,920 --> 00:00:32,220
zazen

7
00:00:32,460 --> 00:00:35,380
as I explained in a previous video.

8
00:00:35,760 --> 00:00:39,220
In general, a whole meditation session

9
00:00:39,440 --> 00:00:43,380
includes a first part of sitting meditation, zazen,

10
00:00:43,660 --> 00:00:49,800
then this little meditative walk that I will explain to you

11
00:00:50,060 --> 00:00:52,360
which is called kin-hin,

12
00:00:52,640 --> 00:00:54,820
and after this kin-hin

13
00:00:54,820 --> 00:00:57,580
we return to where our meditation cushion is

14
00:00:57,580 --> 00:01:00,600
for a second part

15
00:01:00,600 --> 00:01:04,460
of zazen, facing the wall, in silence.

16
00:01:12,340 --> 00:01:15,640
The meditative walk, kin-hin,

17
00:01:16,445 --> 00:01:17,915
it is a walk,

18
00:01:18,480 --> 00:01:21,380
you’re going to put your feet

19
00:01:21,760 --> 00:01:26,320
if you look at the position of my feet like this,

20
00:01:26,600 --> 00:01:30,900
that is, they are spaced, more or less, the width of your fist,

21
00:01:31,780 --> 00:01:36,100
without distorting your pelvis.

22
00:01:37,580 --> 00:01:41,180
And you’re going to take steps

23
00:01:41,360 --> 00:01:44,480
the width of half a foot

24
00:01:44,800 --> 00:01:47,540
a foot is more or less 30-33 cm

25
00:01:47,880 --> 00:01:51,160
so it’s going to be steps of 15 centimeters or a little bit more.

26
00:01:51,660 --> 00:01:54,480
If you look,

27
00:01:54,680 --> 00:01:57,660
See, I take a half-foot step.

28
00:01:58,040 --> 00:02:03,580
So at one point I have one leg in front and one leg behind.

29
00:02:03,840 --> 00:02:12,240
the back leg will pass in front by taking a step the width of half a foot.

30
00:02:12,240 --> 00:02:15,880
Make sure your feet are

31
00:02:16,240 --> 00:02:18,560
right and not so.

32
00:02:27,700 --> 00:02:30,875
Now the posture of the hands.

33
00:02:30,875 --> 00:02:33,685
While we walk, as I just explained,

34
00:02:33,685 --> 00:02:36,820
we also have an activity with our hands.

35
00:02:37,520 --> 00:02:40,300
You remember, the Zen meditation posture,

36
00:02:40,620 --> 00:02:42,520
we had our hands like this.

37
00:02:43,140 --> 00:02:45,660
I remove my right hand,

38
00:02:45,980 --> 00:02:49,180
with my left hand, I bend the thumb of my left hand

39
00:02:49,500 --> 00:02:55,000
and I hold it with all my fingers glued together,

40
00:02:55,320 --> 00:03:01,700
I let the root of my left thumb stick out.

41
00:03:02,900 --> 00:03:05,140
My right hand that wrapped

42
00:03:05,480 --> 00:03:08,765
wraps the left hand

43
00:03:08,765 --> 00:03:14,600
fingers well joined, thumb also, all glued together

44
00:03:14,800 --> 00:03:18,640
I bend my wrists slightly

45
00:03:19,380 --> 00:03:22,320
see, the root of my left thumb comes out

46
00:03:22,940 --> 00:03:25,980
and this root

47
00:03:26,340 --> 00:03:29,700
I will apply it here, at the base of the sternum.

48
00:03:30,320 --> 00:03:35,180
All human beings, at the base of the sternum have two-three small holes.

49
00:03:35,720 --> 00:03:44,200
This is where you will pose, making contact between the root of your left thumb and the plexus.

50
00:03:44,780 --> 00:03:55,440
As you can see, I bend my wrists slightly so that my forearms are in the same line.

51
00:03:55,860 --> 00:03:59,160
I don’t have one arm above the other.

52
00:03:59,600 --> 00:04:03,020
The palms of my hands are parallel to the ground,

53
00:04:03,540 --> 00:04:07,340
I mean I’m not like this

54
00:04:07,680 --> 00:04:11,140
I don’t have my arms falling down

55
00:04:11,520 --> 00:04:16,700
and I don’t hold my arms up with physical strength by raising my elbows.

56
00:04:17,800 --> 00:04:21,040
What’s up?

57
00:04:21,560 --> 00:04:30,260
Remember when you exhale, you put the pressure, all the weight of the body on the front leg.

58
00:04:30,560 --> 00:04:33,640
During this phase,

59
00:04:34,200 --> 00:04:40,720
In the same way, we will press our hands together,

60
00:04:41,080 --> 00:04:49,220
Apply light pressure and at the same time press the root of the left thumb against the plexus.

61
00:04:49,580 --> 00:04:54,640
Obviously, delicately, it is not a question of being violent.

62
00:04:54,840 --> 00:04:59,700
You will see that if you are a little too low you touch a point of the stomach.

63
00:05:00,080 --> 00:05:04,340
you’ll soon realize that it’s not pleasant.

64
00:05:04,820 --> 00:05:09,720
so naturally you will get up to find the right posture.

65
00:05:13,600 --> 00:05:16,880
When I breathe in, I put my leg in front of it,

66
00:05:17,160 --> 00:05:18,460
everything is released.

67
00:05:18,860 --> 00:05:21,880
Everything is in posture, but it’s loose.

68
00:05:22,300 --> 00:05:26,300
During the expiration phase,

69
00:05:27,080 --> 00:05:34,440
I put the weight on the front leg, press lightly against the plexus, and both hands against each other.

70
00:05:35,080 --> 00:05:40,920
I put myself in profile, as you can see, it’s like during zazen,

71
00:05:41,200 --> 00:05:49,180
I keep the spine as straight as possible, the neck stretched out, the chin tucked in.

72
00:05:49,520 --> 00:05:53,840
my gaze 45 degrees diagonally downwards,

73
00:05:54,460 --> 00:05:59,560
I am totally focused, here and now on what I do.

74
00:06:00,200 --> 00:06:03,180
And what I do, I am a meditative walk.

75
00:06:03,580 --> 00:06:08,420
You are not following your thoughts, you are not looking outside,

76
00:06:08,640 --> 00:06:14,260
You continue with the concentration of zazen, the concentration of the sitting posture.

77
00:06:22,020 --> 00:06:27,600
The rhythm of the walk will be the rhythm of your breathing.

78
00:06:28,120 --> 00:06:31,380
You remember, during sitting meditation,

79
00:06:31,760 --> 00:06:35,840
we are focused here and now on the physical points of the posture,

80
00:06:36,500 --> 00:06:41,380
the state of mind, the mental attitude, we let our thoughts pass,

81
00:06:41,640 --> 00:06:44,660
they are not maintained

82
00:06:44,920 --> 00:06:48,000
and we focus here and now on our breathing...

83
00:06:48,340 --> 00:06:51,560
by establishing a harmonious coming and going

84
00:06:51,960 --> 00:06:55,780
and with a special focus on exhalation.

85
00:06:57,160 --> 00:07:01,260
longer and longer, deeper and deeper,

86
00:07:01,580 --> 00:07:04,540
abdominal, which goes all the way down to the lower abdomen.

87
00:07:05,860 --> 00:07:15,440
Breathing and mental attitude are the same whether we are sitting, facing the wall, in silence, motionless, in zazen.

88
00:07:16,280 --> 00:07:24,660
or by doing this meditation while walking. The only thing that changes is that we are going to move.

89
00:07:30,320 --> 00:07:33,880
Walk according to the rhythm of your breathing.

90
00:07:34,160 --> 00:07:41,660
During the inspiration you pass one leg in front

91
00:07:42,520 --> 00:07:45,000
half a foot

92
00:07:45,180 --> 00:07:50,220
and during the expiration phase,

93
00:07:52,440 --> 00:07:55,420
through the nose, in silence,

94
00:07:56,100 --> 00:07:59,820
you will gradually place all the weight of the body

95
00:08:00,100 --> 00:08:03,240
on the front leg

96
00:08:03,520 --> 00:08:06,880
the front leg is well stretched

97
00:08:07,180 --> 00:08:11,480
the soles of the feet completely in contact with the ground

98
00:08:12,080 --> 00:08:15,680
the leg behind it, remains relaxed,

99
00:08:16,180 --> 00:08:20,440
but also the entire sole of the foot in contact with the ground.

100
00:08:20,740 --> 00:08:24,320
If you see the foot, I’m not with a heel up.

101
00:08:24,700 --> 00:08:29,860
I have the heels of both feet resting on the ground.

102
00:08:30,480 --> 00:08:33,280
Therefore, during the expiration phase,

103
00:08:33,740 --> 00:08:40,180
gradually I put all the weight of my body on this front leg.

104
00:08:40,460 --> 00:08:43,520
to the root of the big toe

105
00:08:43,920 --> 00:08:46,820
without tension

106
00:08:47,380 --> 00:08:51,380
it’s simply the weight that makes me press down on the floor.

107
00:08:51,760 --> 00:08:57,960
As if you were on soft ground such as sand or clay

108
00:08:58,280 --> 00:09:05,800
and that you would want to leave your mark in the sand or in the clay.

109
00:09:06,700 --> 00:09:10,420
I show you in profile.

110
00:09:10,920 --> 00:09:14,580
I breathe in and pass the leg in front,

111
00:09:15,100 --> 00:09:20,280
I exhale, all the weight of my body on the front leg.

112
00:09:20,640 --> 00:09:23,420
I’m at the end of the expiration period,

113
00:09:23,680 --> 00:09:28,940
I breathe in and put my leg behind in front of it.

114
00:09:29,960 --> 00:09:31,120
I expire

115
00:09:32,620 --> 00:09:37,340
all the weight of my body on the front leg.

116
00:09:37,780 --> 00:09:44,240
At the end of the exhalation, I breathe in, the leg behind passes in front.

117
00:09:51,240 --> 00:09:55,380
Try it and you’ll see that the kin-hin walk

118
00:09:55,640 --> 00:10:01,920
it is a walk that you can then use in many circumstances in your life.

119
00:10:02,360 --> 00:10:12,360
5 minutes of waiting, you are stressed and well, practice this, 3-4 minutes of this walk.

120
00:10:12,660 --> 00:10:18,980
and you will see how you will calm down and feel more in harmony with you.

121
00:10:19,860 --> 00:10:21,240
Good practice
