1
00:00:03,300 --> 00:00:07,060
Che ore sono 

2
00:00:07,240 --> 00:00:11,780
Il tempo è solo adesso. 

3
00:00:12,080 --> 00:00:15,080
Lo Zen è "qui e ora". 

4
00:00:15,340 --> 00:00:16,820
È importante. 

5
00:00:17,220 --> 00:00:25,160
Ma comprendere il passato è un trucco straordinario per comprendere il presente. 

6
00:00:25,580 --> 00:00:31,640
E anche la comprensione del futuro è molto importante. 

7
00:00:32,460 --> 00:00:34,780
Come passi il tempo? 

8
00:00:35,360 --> 00:00:38,400
Il tempo è una questione di frequenza. 

9
00:00:38,700 --> 00:00:43,640
Maggiore è la frequenza, più veloce è il tempo, tutto qui. 

10
00:00:44,100 --> 00:00:49,760
La coscienza si evolve anche in base alle frequenze. 

11
00:00:59,700 --> 00:01:03,440
Costoso è solo un’impressione materiale. 

12
00:01:03,820 --> 00:01:06,000
È una delizia. 

13
00:01:06,400 --> 00:01:09,100
Che dire di zazen? 

14
00:01:09,400 --> 00:01:16,700
Per un amante dello zazen, il tempo di zazen è molto prezioso. 

15
00:01:17,120 --> 00:01:22,560
Arriviamo al dojo e abbiamo solo un’ora e mezza, fino a due ore. 

16
00:01:23,020 --> 00:01:27,720
Essere in grado di vivere in questa dimensione. 

17
00:01:28,000 --> 00:01:31,520
E metti le cose al loro posto. 

18
00:01:31,780 --> 00:01:35,440
Devi risparmiare tempo zazen. 

19
00:01:35,540 --> 00:01:38,760
Ci vuole un po ’per arrivare in fondo. 

20
00:01:39,000 --> 00:01:40,760
E di essere in una buona postura. 

21
00:01:41,060 --> 00:01:43,960
Il tempo è legato al karma? 

22
00:01:44,720 --> 00:01:46,920
Il passato esiste solo ora. 

23
00:01:47,280 --> 00:01:57,240
Ogni "ora" corrisponde a un tempo passato specifico con il tuo "ora". 

24
00:01:57,620 --> 00:02:00,900
Il tuo passato è in continua evoluzione. 

25
00:02:01,000 --> 00:02:10,700
Ma quando vai nel tuo passato, dimentichi il tuo presente. 

26
00:02:11,040 --> 00:02:13,560
E il tuo presente diventa il passato. 

27
00:02:13,900 --> 00:02:17,720
E vai in qualcosa che diventa sempre più complicato. 

28
00:02:17,940 --> 00:02:19,340
Nel karma. 

29
00:02:19,820 --> 00:02:24,160
"È colpa del mio karma …" 

30
00:02:25,240 --> 00:02:27,960
"I miei genitori, le mie cose …" 

31
00:02:28,280 --> 00:02:30,440
"La mia vita è complicata …" 

32
00:02:30,700 --> 00:02:32,380
"I miei antenati erano alcolisti …" 

33
00:02:32,660 --> 00:02:35,420
Eccetera E ti perdi lì dentro. 

34
00:02:35,780 --> 00:02:40,060
Ti identifichi con questo personaggio. 

35
00:02:40,340 --> 00:02:41,720
Ti stai indebolendo. 

36
00:02:42,060 --> 00:02:44,140
Non sei più dio. 

37
00:02:44,420 --> 00:02:45,720
Sei confuso 

38
00:02:46,080 --> 00:02:47,760
Ti sei perso nella materializzazione. 

39
00:02:48,060 --> 00:02:53,300
Con il nostro stato attuale, ci sono sempre cause in passato. 

40
00:02:53,680 --> 00:02:58,540
Se cambiamo la situazione attuale, anche il passato cambierà. 

41
00:02:59,160 --> 00:03:01,940
E il futuro? 

42
00:03:02,340 --> 00:03:05,300
Il futuro è una storia diversa … 

