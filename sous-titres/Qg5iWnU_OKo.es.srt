1
00:00:00,100 --> 00:00:01,460
Mi nombre es Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
Mi monje zen se llama Ryurin. 

3
00:00:03,880 --> 00:00:05,580
También soy un calígrafo. 

4
00:00:05,900 --> 00:00:09,560
Enseño caligrafía china, que descubrí en el zen. 

5
00:00:09,840 --> 00:00:13,860
Lo aprendí de un monje zen. 

6
00:00:14,320 --> 00:00:18,260
Me gusta compartir caligrafía con todos. 

7
00:00:18,860 --> 00:00:22,540
Hago mucho trabajo en las escuelas. 

8
00:00:22,900 --> 00:00:27,360
Hago caligrafía con niños en talleres. 

9
00:00:27,700 --> 00:00:31,020
También para adultos y personas con discapacidad. 

10
00:00:31,380 --> 00:00:33,300
A ellos les gusta mucho. 

11
00:00:33,780 --> 00:00:39,220
Mis estudiantes más jóvenes tienen 6 años y mi estudiante más viejo tiene 90. 

12
00:00:39,760 --> 00:00:42,800
También enseño chino. 

13
00:00:43,180 --> 00:00:44,840
Ese es mi negocio principal. 

14
00:00:45,140 --> 00:00:48,020
Esta caligrafía es simplemente "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
Es una caligrafía realizada en el estilo semicursivo actual. 

16
00:00:54,580 --> 00:01:06,760
El pincel es corto, va de puntos a líneas, para formar un carácter chino o japonés. 

17
00:01:08,420 --> 00:01:11,320
Esto es "Reiki" (靈氣), la energía espiritual. 

18
00:01:11,620 --> 00:01:13,880
Es una técnica curativa. 

19
00:01:14,220 --> 00:01:20,260
Continuamos esta tradición porque proviene de un maestro que ha practicado mucha caligrafía. 

20
00:01:20,500 --> 00:01:23,920
También practicó sumi-e, (墨 絵), era pintor. 

21
00:01:24,280 --> 00:01:27,580
Realmente aprecio las pinturas del Maestro Deshimaru, él era un artista. 

22
00:01:27,840 --> 00:01:31,020
Pintó e hizo muy bien la caligrafía. 

23
00:01:31,440 --> 00:01:37,860
Su caligrafía es muy fuerte, densa y enérgica. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) es el nombre de un sutra que se canta. 

25
00:01:43,700 --> 00:01:52,480
Lo rastreé en rizado: algo así como pequeños espaguetis. 

26
00:01:52,780 --> 00:01:56,440
Los personajes están muy vinculados y simplificados. 

27
00:01:56,780 --> 00:02:00,800
También nos gusta eso en la práctica Zen. 

28
00:02:01,240 --> 00:02:08,040
A los practicantes de Zen les gustó mucho este escrito, que es bastante gratuito y expresivo. 

29
00:02:09,260 --> 00:02:10,740
Es un arte enérgico. 

30
00:02:11,020 --> 00:02:14,840
Se practica con todo el cuerpo, en relación con la respiración. 

31
00:02:15,240 --> 00:02:21,900
En relación con su disponibilidad de la mente, su conciencia. 

32
00:02:22,360 --> 00:02:24,720
Entonces es un arte energético. 

33
00:02:25,000 --> 00:02:28,120
Vamos a calmarnos, reorientar algo dentro de nosotros mismos. 

34
00:02:28,360 --> 00:02:33,240
Es una metamorfosis completa, una alquimia del equilibrio. 

35
00:02:33,560 --> 00:02:40,040
Horizontalidad, verticalidad, centrado, acoplamiento al dibujar los signos. 

36
00:02:40,540 --> 00:02:43,120
Disponibilidad a mano. 

37
00:02:43,460 --> 00:02:48,080
En la meditación zen, la presencia en las manos es muy importante. 

38
00:02:48,420 --> 00:02:55,600
Ya sea estacionario o en ejecución. 

39
00:02:55,940 --> 00:02:57,760
El gesto es diferente en caligrafía. 

40
00:02:58,180 --> 00:03:00,120
Pero hay una presencia por las manos. 

41
00:03:00,340 --> 00:03:02,320
Es la relación mano a cerebro. 

42
00:03:02,600 --> 00:03:04,140
La caligrafía tiene una vida. 

43
00:03:04,440 --> 00:03:05,440
¡Incluso si no lo sabes! 

44
00:03:05,680 --> 00:03:07,980
Algunas caligrafías te tocarán, otras no. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) es chino, y es más taoísta. 

46
00:03:11,740 --> 00:03:15,820
Significa "nutrir el principio vital". 

47
00:03:16,540 --> 00:03:23,440
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

