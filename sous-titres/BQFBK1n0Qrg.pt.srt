1
00:00:09,675 --> 00:00:13,085
Olá. Hoje, eu gostaria de explicar a vocês

2
00:00:13,280 --> 00:00:15,940
como praticar uma caminhada de meditação

3
00:00:16,760 --> 00:00:21,800
que praticamos no Zen, esta caminhada é chamada de caminhada de parentesco.

4
00:00:22,180 --> 00:00:25,640
É uma caminhada bem curta,

5
00:00:25,920 --> 00:00:30,740
que se pratica entre dois momentos de meditação sentada...

6
00:00:30,920 --> 00:00:32,220
zazen

7
00:00:32,460 --> 00:00:35,380
como expliquei em um vídeo anterior.

8
00:00:35,760 --> 00:00:39,220
Em geral, toda uma sessão de meditação

9
00:00:39,440 --> 00:00:43,380
inclui uma primeira parte da meditação sentada, zazen,

10
00:00:43,660 --> 00:00:49,800
e depois esta pequena caminhada de meditação que vou explicar-lhes.

11
00:00:50,060 --> 00:00:52,360
chamado kin-hin,

12
00:00:52,640 --> 00:00:54,820
e depois deste parentesco

13
00:00:54,820 --> 00:00:57,580
voltamos para onde está nossa almofada de meditação.

14
00:00:57,580 --> 00:01:00,600
em segundo lugar

15
00:01:00,600 --> 00:01:04,460
de zazen, de frente para a parede, em silêncio.

16
00:01:12,340 --> 00:01:15,640
A caminhada de meditação, parentes e afins,

17
00:01:16,445 --> 00:01:17,915
é um passeio,

18
00:01:18,480 --> 00:01:21,380
você vai colocar seus pés

19
00:01:21,760 --> 00:01:26,320
se você olhar assim para a posição dos meus pés,

20
00:01:26,600 --> 00:01:30,900
ou seja, eles estão espaçados, mais ou menos, a largura de seu punho,

21
00:01:31,780 --> 00:01:36,100
sem distorcer sua pélvis.

22
00:01:37,580 --> 00:01:41,180
E você vai tomar medidas

23
00:01:41,360 --> 00:01:44,480
meio pé de largura

24
00:01:44,800 --> 00:01:47,540
um pé é mais ou menos 30-33 cm.

25
00:01:47,880 --> 00:01:51,160
portanto, serão degraus de cerca de seis polegadas.

26
00:01:51,660 --> 00:01:54,480
Se você estiver observando,

27
00:01:54,680 --> 00:01:57,660
Veja, eu estou dando um passo de meio pé.

28
00:01:58,040 --> 00:02:03,580
Então, em algum momento eu tenho uma perna para cima e uma perna para baixo.

29
00:02:03,840 --> 00:02:12,240
a perna atrás passará na frente, dando um passo com a largura de meio pé.

30
00:02:12,240 --> 00:02:15,880
Assegure-se de que seus pés estão

31
00:02:16,240 --> 00:02:18,560
reto e não como este.

32
00:02:27,700 --> 00:02:30,875
Agora a postura das mãos.

33
00:02:30,875 --> 00:02:33,685
Enquanto caminhamos, como acabo de explicar,

34
00:02:33,685 --> 00:02:36,820
também temos uma atividade com nossas mãos.

35
00:02:37,520 --> 00:02:40,300
Você se lembra, a postura de meditação Zen,

36
00:02:40,620 --> 00:02:42,520
tínhamos nossas mãos assim.

37
00:02:43,140 --> 00:02:45,660
Estou tirando minha mão direita,

38
00:02:45,980 --> 00:02:49,180
Curvo o polegar da minha mão esquerda

39
00:02:49,500 --> 00:02:55,000
e eu o agarro com todos os dedos colados,

40
00:02:55,320 --> 00:03:01,700
Eu deixei a raiz do meu polegar esquerdo saltar para fora.

41
00:03:02,900 --> 00:03:05,140
Minha mão direita que embrulhou

42
00:03:05,480 --> 00:03:08,765
envolve a mão esquerda

43
00:03:08,765 --> 00:03:14,600
dedos juntos, polegares juntos, todos colados...

44
00:03:14,800 --> 00:03:18,640
Dobro ligeiramente os pulsos

45
00:03:19,380 --> 00:03:22,320
Veja, a raiz do meu polegar esquerdo está saindo...

46
00:03:22,940 --> 00:03:25,980
e esta raiz

47
00:03:26,340 --> 00:03:29,700
Vou aplicá-lo aqui, na base do esterno.

48
00:03:30,320 --> 00:03:35,180
Todos os seres humanos, na base do esterno, têm dois ou três pequenos orifícios.

49
00:03:35,720 --> 00:03:44,200
É aqui que você vai posar, fazendo contato entre a raiz de seu polegar esquerdo e o plexo.

50
00:03:44,780 --> 00:03:55,440
Como você pode ver, eu dobro ligeiramente meus pulsos, de modo que meus antebraços estão na mesma linha.

51
00:03:55,860 --> 00:03:59,160
Eu não tenho um braço sobre o outro.

52
00:03:59,600 --> 00:04:03,020
As palmas das minhas mãos estão paralelas ao chão,

53
00:04:03,540 --> 00:04:07,340
Quero dizer, não sou assim.

54
00:04:07,680 --> 00:04:11,140
Eu não tenho meus braços caindo.

55
00:04:11,520 --> 00:04:16,700
e não levanto meus braços com força física quando levanto os cotovelos.

56
00:04:17,800 --> 00:04:21,040
O que está acontecendo?

57
00:04:21,560 --> 00:04:30,260
Lembre-se que quando você expira, você pressiona, todo o seu peso corporal sobre a perna dianteira.

58
00:04:30,560 --> 00:04:33,640
Durante esta fase,

59
00:04:34,200 --> 00:04:40,720
da mesma forma, vamos pressionar nossas mãos juntas,

60
00:04:41,080 --> 00:04:49,220
aplicar uma leve pressão e ao mesmo tempo pressionar a raiz do polegar esquerdo contra o plexo.

61
00:04:49,580 --> 00:04:54,640
Obviamente, delicadamente, não se trata de violência.

62
00:04:54,840 --> 00:04:59,700
Você verá que se você estiver um pouco baixo demais você vai acertar um ponto no estômago

63
00:05:00,080 --> 00:05:04,340
você logo perceberá que não é agradável

64
00:05:04,820 --> 00:05:09,720
por isso, naturalmente, você vai se levantar para encontrar a postura correta.

65
00:05:13,600 --> 00:05:16,880
Quando respiro, coloco minha perna na frente dela,

66
00:05:17,160 --> 00:05:18,460
tudo está solto.

67
00:05:18,860 --> 00:05:21,880
Tudo está em posição, mas está solto.

68
00:05:22,300 --> 00:05:26,300
Durante a fase de exalação,

69
00:05:27,080 --> 00:05:34,440
Coloco o peso na perna dianteira, pressiono levemente contra o plexo, e as duas mãos uma contra a outra.

70
00:05:35,080 --> 00:05:40,920
Eu me coloco em perfil, como você pode ver, é como durante o zazen,

71
00:05:41,200 --> 00:05:49,180
Mantenho minha coluna o mais reta possível, pescoço esticado, queixo enfiado...

72
00:05:49,520 --> 00:05:53,840
meu olhar 45 graus diagonalmente para baixo,

73
00:05:54,460 --> 00:05:59,560
Estou totalmente concentrado, aqui e agora, no que estou fazendo.

74
00:06:00,200 --> 00:06:03,180
E o que eu faço, sou uma caminhada meditativa.

75
00:06:03,580 --> 00:06:08,420
Você não está seguindo seus pensamentos, você não está olhando para fora,

76
00:06:08,640 --> 00:06:14,260
você continua com a concentração de zazen, a concentração da postura sentada.

77
00:06:22,020 --> 00:06:27,600
O ritmo da caminhada vai ser o ritmo de sua respiração.

78
00:06:28,120 --> 00:06:31,380
Você se lembra, durante a meditação sentada,

79
00:06:31,760 --> 00:06:35,840
estamos focados aqui e agora nos pontos físicos de postura,

80
00:06:36,500 --> 00:06:41,380
o estado de espírito, a atitude mental, você deixa seus pensamentos ir,

81
00:06:41,640 --> 00:06:44,660
não os atendemos

82
00:06:44,920 --> 00:06:48,000
e nós nos concentramos aqui e agora em nossa respiração.

83
00:06:48,340 --> 00:06:51,560
estabelecendo um ir e vir harmoniosamente

84
00:06:51,960 --> 00:06:55,780
e concentrando-se especialmente na exalação...

85
00:06:57,160 --> 00:07:01,260
cada vez mais longo, mais e mais profundo,

86
00:07:01,580 --> 00:07:04,540
abdominal, que vai até a parte inferior do abdômen.

87
00:07:05,860 --> 00:07:15,440
A respiração e a atitude mental são as mesmas quer estejamos sentados, de frente para a parede, em silêncio, sem movimento, em zazen.

88
00:07:16,280 --> 00:07:24,660
ou fazendo esta meditação enquanto caminhava. A única coisa que muda é que nós vamos nos mudar.

89
00:07:30,320 --> 00:07:33,880
Caminhe de acordo com o ritmo de sua respiração.

90
00:07:34,160 --> 00:07:41,660
Durante a inspiração você passa uma perna na frente

91
00:07:42,520 --> 00:07:45,000
meio pé

92
00:07:45,180 --> 00:07:50,220
e durante a fase de expiração,

93
00:07:52,440 --> 00:07:55,420
através do nariz, silenciosamente,

94
00:07:56,100 --> 00:07:59,820
você vai gradualmente colocar o peso total do corpo

95
00:08:00,100 --> 00:08:03,240
na perna dianteira

96
00:08:03,520 --> 00:08:06,880
a perna dianteira está bem esticada

97
00:08:07,180 --> 00:08:11,480
as solas dos pés completamente em contato com o solo

98
00:08:12,080 --> 00:08:15,680
a perna atrás dela, fique solta,

99
00:08:16,180 --> 00:08:20,440
mas também toda a sola do pé em contato com o solo.

100
00:08:20,740 --> 00:08:24,320
Se você vê o pé, eu não estou com o calcanhar para cima.

101
00:08:24,700 --> 00:08:29,860
Eu tenho os calcanhares de ambos os pés descansando no chão.

102
00:08:30,480 --> 00:08:33,280
Portanto, durante a fase de exalação,

103
00:08:33,740 --> 00:08:40,180
Estou gradualmente deslocando todo o meu peso corporal para esta perna dianteira.

104
00:08:40,460 --> 00:08:43,520
até a raiz do dedo grande do pé

105
00:08:43,920 --> 00:08:46,820
livremente

106
00:08:47,380 --> 00:08:51,380
é apenas o peso que me faz bater no chão.

107
00:08:51,760 --> 00:08:57,960
Como se você estivesse em solo macio como areia ou barro...

108
00:08:58,280 --> 00:09:05,800
e você ia querer deixar sua marca na areia ou no barro.

109
00:09:06,700 --> 00:09:10,420
Eu lhe mostrarei em perfil.

110
00:09:10,920 --> 00:09:14,580
Inspiro e coloco minha perna por cima,

111
00:09:15,100 --> 00:09:20,280
Eu exalo, todo o peso do meu corpo na minha perna dianteira.

112
00:09:20,640 --> 00:09:23,420
Estou no final da minha exalação,

113
00:09:23,680 --> 00:09:28,940
Inspiro e coloco minha perna atrás da frente.

114
00:09:29,960 --> 00:09:31,120
Eu expiro

115
00:09:32,620 --> 00:09:37,340
todo o peso do meu corpo na minha perna dianteira.

116
00:09:37,780 --> 00:09:44,240
Fim da exalação, respiro, a perna de trás vai para a frente.

117
00:09:51,240 --> 00:09:55,380
Experimente e você verá que os parentes andam

118
00:09:55,640 --> 00:10:01,920
é uma caminhada que você pode então usar em muitas circunstâncias em sua vida.

119
00:10:02,360 --> 00:10:12,360
5 minutos de espera, você está estressado e bem, pratique isto, 3-4 minutos desta caminhada.

120
00:10:12,660 --> 00:10:18,980
e você verá como vai se acalmar, se sentir mais em sintonia consigo mesmo.

121
00:10:19,860 --> 00:10:21,240
Boas práticas
