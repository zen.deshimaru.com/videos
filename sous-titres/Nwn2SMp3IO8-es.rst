
1
00:00:00,280 --> 00:00:02,560
No sé mucho sobre sofrología.

2
00:00:02,600 --> 00:00:12,140
Sé que es una aplicación de ciertas prácticas de bienestar, de desarrollo personal,

3
00:00:12,540 --> 00:00:17,160
y que hay una sofrología llamada "orientalista",

4
00:00:17,440 --> 00:00:24,080
que está muy inspirada en las prácticas yóguicas, budistas o indias.

5
00:00:24,540 --> 00:00:27,940
El Zen es muy simple.

6
00:00:28,380 --> 00:00:38,400
Se basa en posturas humanas fundamentales.

7
00:00:39,820 --> 00:00:48,480
Voy a mostrarle la primera postura fundamental del Zen.

8
00:01:01,500 --> 00:01:06,120
Los seres humanos solían ser monos.

9
00:01:06,460 --> 00:01:08,580
Así es como caminaban.

10
00:01:09,200 --> 00:01:20,360
Poco a poco, se sentaron derechos, como los maestros Zen.

11
00:01:22,900 --> 00:01:24,900
Luego se enderezaron.

12
00:01:25,180 --> 00:01:28,600
Así es como caminaron.

13
00:01:29,680 --> 00:01:33,760
Y la conciencia humana ha evolucionado

14
00:01:35,180 --> 00:01:39,980
junto con la postura.

15
00:01:40,340 --> 00:01:46,000
La postura del cuerpo es un pensamiento fundamental.

16
00:01:46,340 --> 00:01:52,200
Por ejemplo, Usain Bolt, el corredor más rápido del mundo,

17
00:01:52,540 --> 00:01:55,160
hace ese gesto.

18
00:01:55,440 --> 00:02:02,300
Todo el mundo entiende lo que significa, y sin embargo no hay palabras.

19
00:02:02,640 --> 00:02:05,440
Es únicamente una postura.

20
00:02:05,900 --> 00:02:11,120
La forma en que nos sentamos y caminamos…

21
00:02:11,520 --> 00:02:13,400
lo que hacemos con nuestras manos,

22
00:02:13,700 --> 00:02:16,500
todo es fundamentalmente importante.

23
00:02:18,160 --> 00:02:21,440
En el zen, está la postura de Buda.

24
00:02:22,980 --> 00:02:31,120
Existió antes que el Buda, se menciona en el Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
Se sentaban en un cojín de hierba y doblaban las piernas.

26
00:02:41,560 --> 00:02:47,140
En la naturaleza, te comunicas

27
00:02:47,440 --> 00:02:53,220
con tus oídos, con tu nariz, con tus ojos, para ver lejos.

28
00:02:54,920 --> 00:02:59,000
Así que  querían mantenerse erguidos.

29
00:02:59,400 --> 00:03:04,220
Y a medida que se enderezaban, su conciencia se agudizaba, se despertaba.

30
00:03:06,040 --> 00:03:08,720
Esta es la primera postura.

31
00:03:08,980 --> 00:03:11,580
Normalmente, tenemos que hacer un loto completo.

32
00:03:12,060 --> 00:03:14,260
Así es como se cruzan las piernas.

33
00:03:14,700 --> 00:03:18,460
Se llama "pretzel".

34
00:03:24,960 --> 00:03:30,980
Es muy importante adoptar esta postura.

35
00:03:31,380 --> 00:03:37,380
El orador anterior habló sobre el dinamismo y la relajación.

36
00:03:37,780 --> 00:03:42,340
Tenemos un sistema nervioso simpático y parasimpático.

37
00:03:42,600 --> 00:03:45,800
Tenemos los músculos externos y los músculos profundos.

38
00:03:46,320 --> 00:03:51,400
Tenemos una respiración superficial y una respiración profunda.

39
00:03:51,820 --> 00:03:54,620
Tenemos una conciencia superficial, cortical

40
00:03:54,920 --> 00:03:59,760
y profunda, el cerebro reptil o medio.

41
00:04:00,340 --> 00:04:04,160
Para mantenerse erguido en esta postura,

42
00:04:04,440 --> 00:04:06,800
hay que empujar la tierra con las rodillas.

43
00:04:10,240 --> 00:04:15,640
Se requiere un esfuerzo para ser dinámico y tener una buena postura.

44
00:04:16,000 --> 00:04:22,180
Mi maestro [Deshimaru] insistió en la belleza y la fuerza de la postura.

45
00:04:22,460 --> 00:04:31,280
Pero en el loto, cuanto más te relajas, más empujan tus pies en los muslos,

46
00:04:31,520 --> 00:04:35,100
y cuanto más empujan las rodillas en el suelo.

47
00:04:35,620 --> 00:04:40,280
Cuanto más te relajas, más dinámico eres, más recto y más fuerte eres.

48
00:04:40,560 --> 00:04:46,560
Esto no tiene nada que ver con relajarse en una silla o una cama.

49
00:04:46,940 --> 00:04:57,280
Es una forma de abandonar el cuerpo a sí mismo.

50
00:05:04,380 --> 00:05:09,700
Mientras esté perfectamente despierto.

51
00:05:10,120 --> 00:05:16,240
Esta es la primera postura, la postura sentada del Buda.

52
00:05:16,680 --> 00:05:20,280
Las manos también son muy importantes.

53
00:05:20,640 --> 00:05:23,600
Siempre estás haciendo algo con tus manos:

54
00:05:23,860 --> 00:05:26,560
trabajando, protegiéndote…

55
00:05:26,960 --> 00:05:28,740
Las manos son muy expresivas

56
00:05:29,040 --> 00:05:31,040
y están conectadas al cerebro.

57
00:05:31,500 --> 00:05:41,040
Tomar este mudra con las manos cambia la conciencia.

58
00:05:41,680 --> 00:05:47,680
La concentración está en nuestras manos, cuerpo, sistema nervioso y cerebro.

59
00:05:48,100 --> 00:05:53,360
Otra cosa importante es el pensamiento, la conciencia.

60
00:05:53,820 --> 00:05:57,680
Si eres como el pensador de Rodin,

61
00:05:58,000 --> 00:06:01,100
tienes una conciencia muy diferente

62
00:06:09,960 --> 00:06:12,240
que cuando tu postura es perfectamente recta,

63
00:06:12,580 --> 00:06:14,640
especialmente con el cuello estirado,

64
00:06:15,700 --> 00:06:18,400
cabeza erguida, abierta al cielo,

65
00:06:18,760 --> 00:06:22,020
nariz y ombligo alineados,

66
00:06:25,060 --> 00:06:27,220
y estás respirando con todo tu ser.

67
00:06:29,020 --> 00:06:31,860
El pensamiento ya no está disociado del cuerpo.

68
00:06:32,240 --> 00:06:36,100
Ya no hay más disociación entre nuestra conciencia

69
00:06:36,300 --> 00:06:39,080
y cualquiera célula de nuestro cuerpo.

70
00:06:39,500 --> 00:06:45,520
En el Zen, llamamos a esto "mente-cuerpo", en una palabra.

71
00:06:46,360 --> 00:06:50,540
Esta es la postura fundamental: zazen.

72
00:06:56,380 --> 00:07:00,200
La segunda postura es la postura de caminata.

73
00:07:00,680 --> 00:07:06,340
En zazen, abandonamos todo y no nos movemos en absoluto.

74
00:07:06,560 --> 00:07:08,280
Así que la conciencia es especial.

75
00:07:08,720 --> 00:07:13,800
Pero hay otra meditación que se hace mientras se camina.

76
00:07:16,180 --> 00:07:18,160
Tenemos que movernos.

77
00:07:18,540 --> 00:07:20,540
Si hacemos algo, avanzamos.

78
00:07:21,180 --> 00:07:24,180
No avanzas para atrapar algo.

79
00:07:24,580 --> 00:07:28,380
El ritmo de la respiración se mide por la forma en que camina.

80
00:07:29,020 --> 00:07:30,740
El coreógrafo Maurice Béjart, 

81
00:07:31,040 --> 00:07:33,560
que fue discípulo de mi maestro Deshimaru, 

82
00:07:34,060 --> 00:07:38,040
hizo que sus bailarines practicaran eso todos los días.

83
00:07:39,060 --> 00:07:45,340
La segunda postura es el paseo "kin-hin".

84
00:07:45,880 --> 00:07:51,040
Fíjate en lo que haces con tus manos.

85
00:07:51,680 --> 00:07:57,840
Un boxeador que se hace una foto hace esto con las manos.

86
00:07:58,240 --> 00:08:05,220
Cuando pones las manos así, automáticamente el cerebro expresa una fuerte agresividad.

87
00:08:05,820 --> 00:08:10,360
Puedes meter las manos en los bolsillos.

88
00:08:10,840 --> 00:08:13,140
"¿Dónde puse mi billetera?" "

89
00:08:13,640 --> 00:08:16,360
¡Alguien me ha robado el móvil!"

90
00:08:16,700 --> 00:08:19,580
Podemos meternos los dedos en la nariz.

91
00:08:21,660 --> 00:08:25,820
Todo lo que se ha construido aquí ha sido construido por las manos.

92
00:08:26,260 --> 00:08:28,820
Es extraordinaria, la mano.

93
00:08:29,160 --> 00:08:31,860
Somos los únicos animales que tienen manos, 

94
00:08:32,260 --> 00:08:35,340
están conectados a nuestra evolución.

95
00:08:37,700 --> 00:08:40,660
Las manos también meditan.

96
00:08:40,940 --> 00:08:43,580
Pones tu pulgar en tu puño.

97
00:08:43,820 --> 00:08:49,420
Pongas la raíz de tu pulgar debajo del esternón, pongas la otra mano sobre la primera.

98
00:08:49,760 --> 00:08:55,060
Estos detalles posturales han existido desde el Buda.

99
00:08:55,380 --> 00:09:02,520
Los textos antiguos explican que fueron transmitidos a China desde la India.

100
00:09:02,960 --> 00:09:08,920
Es una transmisión oral de persona a persona.

101
00:09:09,480 --> 00:09:12,500
En el budismo, hay muchos aspectos.

102
00:09:12,780 --> 00:09:17,320
Pero el aspecto meditativo y postural es muy importante.

103
00:09:17,820 --> 00:09:28,540
Hubo dos grandes discípulos del Buda, Śāriputra y Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Su maestro espiritual no estaba seguro:

105
00:09:37,220 --> 00:09:43,680
"No estoy seguro de tener el truco para salir de la vida y la muerte."

106
00:09:43,940 --> 00:09:52,860
Si encuentras un maestro que enseñe esto, házmelo saber, y yo iré contigo.

107
00:09:53,620 --> 00:10:04,320
Un día ven a un monje, como yo, ya ves, caminando por el camino.

108
00:10:04,780 --> 00:10:10,000
Están asombrados por la belleza de este hombre.

109
00:10:10,420 --> 00:10:16,200
Entonces este monje se detiene en una arboleda para orinar.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, que es considerado una de las mayores inteligencias de la India de la época, le dice:

111
00:10:34,180 --> 00:10:43,940
Estamos sorprendidos por tu presencia, ¿quién es tu maestro?

112
00:10:46,560 --> 00:10:49,540
- "Mi maestro es el Buda Shakyamuni."

113
00:10:49,860 --> 00:10:53,520
- "¡Explíquenos su doctrina!"

114
00:10:53,780 --> 00:10:58,220
- "No puedo explicártelo, soy demasiado nuevo en esto."

115
00:10:59,100 --> 00:11:04,440
Y solo siguieron a este chico por su forma de caminar.

116
00:11:04,800 --> 00:11:06,980
Su forma de expresar algo a través del cuerpo.

117
00:11:07,440 --> 00:11:11,680
Así que la segunda postura es caminar.

118
00:11:12,020 --> 00:11:15,280
Cuando la gente hace zazen, es muy fuerte.

119
00:11:15,500 --> 00:11:21,580
Incluso los practicantes de artes marciales están impresionados por el poder que esto da.

120
00:11:21,940 --> 00:11:25,860
Por el poder de la respiración.

121
00:11:26,240 --> 00:11:35,520
¿Pero cómo encontrar esta energía, esta plenitud, en la vida cotidiana?

122
00:11:35,840 --> 00:11:39,220
Esa calma, esa concentración.

123
00:11:39,600 --> 00:11:42,960
Es un punto difícil.

124
00:11:43,280 --> 00:11:47,140
Porque la postura de zazen es extremadamente precisa.

125
00:11:47,600 --> 00:11:55,100
Al principio, nos centramos en el aspecto técnico de la caminata kin-hin.

126
00:11:55,540 --> 00:11:58,800
Pero una vez que tengamos todos los detalles,

127
00:11:59,300 --> 00:12:02,160
en cualquier momento de nuestra vida diaria

128
00:12:02,440 --> 00:12:08,400
(aunque siempre me olvide de hacerlo), podemos practicarlo.

129
00:12:10,600 --> 00:12:16,740
Puedes reenfocarte en tu ser espiritual.

130
00:12:17,740 --> 00:12:22,760
En tu ser que no hace tonterías.

131
00:12:23,320 --> 00:12:31,340
Ya te dije lo de las manos.

132
00:12:31,840 --> 00:12:39,980
La postura religiosa fundamental es esta.

133
00:12:40,560 --> 00:12:44,240
Cuando tienes las manos juntas así,

134
00:12:45,080 --> 00:12:48,260
solo hacer eso,

135
00:12:48,580 --> 00:12:51,280
tiene un efecto en el cerebro, 

136
00:12:51,620 --> 00:12:55,340
y construye un puente entre los dos hemisferios cerebrales.

137
00:12:55,740 --> 00:12:59,960
Esta postura se utiliza en todas las religiones.

138
00:13:00,280 --> 00:13:02,180
Es muy simple.

139
00:13:02,560 --> 00:13:05,640
¡Vean la importancia de las manos!

140
00:13:06,060 --> 00:13:13,280
La tercera postura…

141
00:13:13,860 --> 00:13:16,520
(Creía que había cuatro…)

142
00:13:16,880 --> 00:13:20,580
Oh, sí, está la postura al acostarse, pero no vamos a hablar de eso.

143
00:13:21,000 --> 00:13:25,520
La postura al acostarse, cómo dormir.

144
00:13:26,700 --> 00:13:31,980
Le he explicado cómo los hombres prehistóricos solían arrastrarse a cuatro patas,

145
00:13:32,260 --> 00:13:34,440
Poco a poco se fueron enderezando.

146
00:13:34,700 --> 00:13:38,760
Y nuestra evolución viene de nuestra postura.

147
00:13:39,060 --> 00:13:42,580
De la irrigación desde la columna vertebral hasta el cerebro.

148
00:13:42,940 --> 00:13:46,400
Y el cerebro, que es un instrumento extraordinario.

149
00:13:46,700 --> 00:13:51,440
De los cuales solo se conoce entre el 10 y el 15% de su utilidad total.

150
00:13:51,760 --> 00:13:57,740
Tenemos algo grande entre las orejas que estamos infrautilizando.

151
00:14:08,640 --> 00:14:12,380
¡Todo el mundo tiene su propio cerebro!

152
00:14:19,020 --> 00:14:22,140
Como una dirección IP.

153
00:14:22,400 --> 00:14:25,200
Comme une adresse IP.

154
00:14:30,220 --> 00:14:33,220
¡Todo el mundo tiene su propio cerebro!

155
00:14:33,940 --> 00:14:35,900
¡Y estamos solos!

156
00:14:36,700 --> 00:14:41,940
Es muy importante para lo que estoy a punto de decirle.

157
00:14:44,760 --> 00:14:50,040
Nuestros pies (vamos a volver a los pies…).

158
00:14:51,720 --> 00:14:56,420
Sabes que vivimos en una Tierra pequeña.

159
00:14:57,040 --> 00:15:01,420
¿Viste la película "Gravity"?

160
00:15:02,120 --> 00:15:07,060
Es una de las películas que realmente despierta la conciencia humana.

161
00:15:07,560 --> 00:15:13,380
En Hollywood, a veces hacen algún trabajo de manipulación.

162
00:15:13,680 --> 00:15:17,220
Pero también a veces de información y evolución espiritual.

163
00:15:17,760 --> 00:15:19,660
Esta película es una de ellas.

164
00:15:20,040 --> 00:15:26,920
Y entendemos, cuando la vemos, la fragilidad de nuestra tierra.

165
00:15:29,200 --> 00:15:33,400
Normalmente, cuando miras al cielo, despotricas: "¡Es infinitamente azul!"

166
00:15:33,760 --> 00:15:38,940
No. No infinitamente. Hay 30 kilómetros de atmósfera.

167
00:15:39,360 --> 00:15:44,660
El final de la atmósfera está entre 30 y 1.000 kilómetros.

168
00:15:52,260 --> 00:15:56,700
Incluso 1.000 no son nada.

169
00:15:58,800 --> 00:16:04,780
Si te pongo a 20 millas de distancia, no sobrevivirías ni un minuto.

170
00:16:05,380 --> 00:16:15,940
Incluso aparte de los impuestos, nuestras condiciones de vida son precarias.

171
00:16:17,420 --> 00:16:20,440
La Tierra es un pequeño y frágil ser viviente.

172
00:16:21,120 --> 00:16:30,660
Nuestra biosfera es pequeña y frágil.

173
00:16:30,940 --> 00:16:35,100
Siempre debemos preocuparnos por preservar esta cosa.

174
00:16:36,860 --> 00:16:39,940
Porque nacimos de la tierra.

175
00:16:40,220 --> 00:16:45,700
Este milagro de la vida en la Tierra es efímero.

176
00:16:46,240 --> 00:16:47,840
Nacimos de la tierra.

177
00:16:48,220 --> 00:16:53,320
Y todos tenemos nuestros pies apuntando al mismo centro, el centro de la tierra.

178
00:16:53,900 --> 00:16:57,760
Así que nuestros pies están en comunidad.

179
00:17:01,860 --> 00:17:08,680
Pero nuestra cabeza es única, nadie apunta en la misma dirección.

180
00:17:18,400 --> 00:17:23,500
De ahí la tercera postura que voy a mostrarte.

181
00:17:23,800 --> 00:17:28,140
Es común a muchas religiones, se llama "postración".

182
00:17:28,380 --> 00:17:31,240
Así es como lo hacemos.

183
00:17:40,640 --> 00:17:42,780
¡Ah, eso se siente bien!

184
00:17:43,060 --> 00:17:49,940
Cuando hacemos esta práctica, es como cuando hacemos el amor:

185
00:17:50,260 --> 00:17:58,460
A veces no tienes sexo durante 10 años, 20 años, 6 meses…

186
00:17:58,780 --> 00:18:07,460
Y cuando hacemos el amor de nuevo decimos: "¡Ah, qué bien se siente, había olvidado lo bien que estaba!".

187
00:18:08,220 --> 00:18:12,200
Cuando haces esa postración, es lo mismo.

188
00:18:13,460 --> 00:18:15,740
No estoy bromeando.

189
00:18:16,240 --> 00:18:20,260
Estamos reformateando nuestros cerebros.

190
00:18:21,060 --> 00:18:26,280
Cuando pones tu cerebro en línea con la tierra.

191
00:18:26,620 --> 00:18:29,080
No se trata solo de humildad 

192
00:18:29,360 --> 00:18:32,720
o de inclinarse ante las estatuas o lo que sea.

193
00:18:34,600 --> 00:18:38,880
Es algo mágico, fundamental, chamánico, tal vez…

194
00:18:39,320 --> 00:18:41,860
Dès le moment où vous faites cette prosternation…

195
00:18:42,180 --> 00:18:43,340
Mi maestro solía decir:

196
00:18:43,660 --> 00:18:47,440
"Si los jefes de estado hicieran esta postración una vez al día, "

197
00:18:47,800 --> 00:18:50,200
"cambiaría la faz del mundo."

198
00:18:50,640 --> 00:18:54,560
No es bla, bla, ni misticismo.

199
00:18:55,280 --> 00:18:58,780
Debe ser científicamente medible.

200
00:19:00,780 --> 00:19:05,280
Reformatea el cerebro, poner la cabeza en el suelo.

201
00:19:05,740 --> 00:19:09,280
Está constantemente en su línea individual.

202
00:19:09,620 --> 00:19:17,300
Y allí te conectas a tu raíz, a tus hermanos, al centro de la tierra.

203
00:19:19,940 --> 00:19:25,380
A veces lo hago y siento algo fuerte en mi cabeza.

204
00:19:25,660 --> 00:19:27,540
Se siente tan bien.

205
00:19:27,820 --> 00:19:39,420
Al mismo tiempo, relativiza el mal que involuntariamente hacemos a nuestro alrededor.

206
00:19:40,560 --> 00:19:42,960
Es una buena medicina.

207
00:19:43,180 --> 00:19:47,040
Todas estas posturas son medicamentos fundamentales.

208
00:19:47,760 --> 00:19:53,260
Había preparado un texto un tanto intelectual.

209
00:19:53,520 --> 00:19:58,140
Pero parece que no puedo pasar de un tema a otro.

210
00:19:58,440 --> 00:20:02,300
¿Qué estaba diciendo?

211
00:20:06,860 --> 00:20:12,040
Voy a volver al cuerpo y a la prehistoria.

212
00:20:12,780 --> 00:20:28,200
Está probado que los hombres de Neandertal tenían una práctica religiosa.

213
00:20:30,680 --> 00:20:36,340
Encontramos algunas tumbas.

214
00:20:36,860 --> 00:20:40,440
Trajeron ofrendas a sus muertos.

215
00:20:40,760 --> 00:20:43,680
Solían realizar una ceremonia religiosa para sus muertos.

216
00:20:48,420 --> 00:20:52,180
Ce qui est intéressant, ce n’est pas seulement ça !

217
00:20:52,720 --> 00:20:56,260
Cuando solían hacer estas ceremonias religiosas…

218
00:20:56,740 --> 00:21:01,700
- religiosas en el sentido de que se comunicaban con lo invisible…

219
00:21:02,540 --> 00:21:06,940
- con la vida eterna, con los muertos -

220
00:21:08,800 --> 00:21:11,020
- se conectaban con lo invisible.

221
00:21:11,420 --> 00:21:18,140
Sin embargo, en esa época, el lenguaje articulado no existía todavía.

222
00:21:18,560 --> 00:21:24,380
Así que la religión existía antes del lenguaje articulado.

223
00:21:24,840 --> 00:21:30,140
(Estoy dispuesto a cambiar a un lenguaje articulado.)

224
00:21:31,480 --> 00:21:36,500
(Es muy interesante, me llevó cuatro días escribirlo…)

225
00:21:43,820 --> 00:21:45,660
Mi maestro solía decir :

226
00:21:45,920 --> 00:21:48,880
"El zazen es la religión antes que la religión".

227
00:21:49,380 --> 00:21:54,380
Decíamos: "Sí, siempre quieres ser el primero…"

228
00:21:55,860 --> 00:21:59,700
Pero esa es la religión antes que la religión tal como la conocemos.

229
00:21:59,960 --> 00:22:02,720
Donde se hace referencia a los libros.

230
00:22:03,080 --> 00:22:06,280
Los libros vienen después de la religión, no antes.

231
00:22:06,720 --> 00:22:11,940
Antes, el hombre ni siquiera podía hablar.

232
00:22:12,360 --> 00:22:15,060
Así que ya estaba en esa postura.

233
00:22:15,480 --> 00:22:19,620
Son posturas prehistóricas.

234
00:22:21,460 --> 00:22:23,740
(¿Cuánto tiempo me queda?)

235
00:22:23,960 --> 00:22:28,320
(¿Solo me quedan tres minutos? ¡Muy bien, entonces!

236
00:22:31,020 --> 00:22:34,460
Entonces, sobre la sofrología…

237
00:22:36,500 --> 00:22:39,280
Quería hablarle sobre el zazen tradicional.

238
00:22:39,740 --> 00:22:43,660
Estoy vestido como un monje tradicional.

239
00:22:47,440 --> 00:22:51,700
El zazen tradicional no se practica solo.

240
00:22:52,100 --> 00:22:54,500
No es una técnica de bienestar.

241
00:22:58,260 --> 00:23:05,060
Te leeré lo que dijo el Buda, ¡creo que es genial!

242
00:23:08,520 --> 00:23:12,780
El Buda Shakyamuni dijo una vez a su audiencia:

243
00:23:13,120 --> 00:23:18,540
El estudio de los sutras y las enseñanzas - textos -

244
00:23:18,900 --> 00:23:28,260
la observancia de los preceptos - en las religiones, es importante, no matar, no robar, etc. - es importante,

245
00:23:29,300 --> 00:23:34,960
e incluso la práctica del zazen, la meditación sentada

246
00:23:35,900 --> 00:23:43,060
son incapaces de extinguir los deseos, miedos y ansiedades humanas.

247
00:23:43,340 --> 00:23:45,080
Eso es lo que dijo el Buda Shakyamuni.

248
00:23:45,340 --> 00:23:51,660
Así que soy un fanático, y él está arruinando mis planes…

249
00:23:52,080 --> 00:23:56,800
Zazen, incluso el Buda dice que es inútil.

250
00:23:57,600 --> 00:24:01,680
Entonces, ¿qué vamos a hacer?

251
00:24:14,440 --> 00:24:17,480
Por supuesto, hay una secuela…

252
00:24:23,360 --> 00:24:29,360
Mi maestro [Deshimaru], cuando conoció a su maestro [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
este último dijo en la conferencia: "Zazen es inútil, no tiene ningún mérito".

254
00:24:34,680 --> 00:24:38,800
Mi maestro dijo, "¡Pero es realmente interesante, si es inútil!"

255
00:24:39,140 --> 00:24:41,940
¡Siempre estamos haciendo cosas que son buenas para algo!

256
00:24:42,280 --> 00:24:43,760
¿Algo inútil?

257
00:24:44,120 --> 00:24:47,400
¡Quiero hacerlo!
