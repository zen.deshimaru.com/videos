1
00:00:00,010 --> 00:00:11,610
The key points are: the pelvis,

2
00:00:11,740 --> 00:00:17,895
the fifth lumbar vertebra,

3
00:00:18,635 --> 00:00:24,900
the first dorsal vertebra, the one that stands out.

4
00:00:26,940 --> 00:00:31,820
And then we see the morphology, everyone is different.

5
00:00:31,960 --> 00:00:38,620
Let’s say that I think she should stretch her neck a little more.

6
00:00:41,210 --> 00:00:48,145
One must always touch two points.

7
00:00:48,645 --> 00:00:52,262
Except when it is the top of the skull.

8
00:00:52,602 --> 00:00:55,460
One yin and one yang.

9
00:00:55,640 --> 00:00:58,675
I make myself comfortable, I get down on my knees.

10
00:00:59,855 --> 00:01:03,020
I’m going to touch this very vertebra.

11
00:01:08,280 --> 00:01:09,815
I will touch the chin with two fingers.

12
00:01:09,975 --> 00:01:11,790
I don’t do that to her.

13
00:01:14,170 --> 00:01:16,825
Indications are given.

14
00:01:16,855 --> 00:01:19,540
But when they are really well given,

15
00:01:19,620 --> 00:01:21,635
if the person is allowed to maintain concentration,

16
00:01:21,865 --> 00:01:24,132
it has a very profound effect.

17
00:01:25,602 --> 00:01:30,670
So I’m going to push the two antagonistic points.

18
00:01:30,753 --> 00:01:35,976
We can also do it this way.

19
00:01:36,156 --> 00:01:39,650
I know she had an operation…

20
00:01:41,560 --> 00:01:43,680
We can also do it this way.

21
00:01:43,785 --> 00:01:46,570
Sensei [Deshimaru] often did so.

22
00:01:48,030 --> 00:01:53,480
A specific point can be reached where the person has weaknesses.

23
00:01:53,740 --> 00:01:56,860
For example, to the stomach or liver.

24
00:02:00,470 --> 00:02:03,900
You should always look at the camber.

25
00:02:06,820 --> 00:02:10,385
Often, it is over- or under-emphasized.

26
00:02:10,575 --> 00:02:15,140
Don’t forget that you have a kyosaku in your hand.

27
00:02:18,340 --> 00:02:23,950
There is no question of starting to tinker.

28
00:02:25,680 --> 00:02:30,245
You must keep your kyosaku.

29
00:02:32,525 --> 00:02:36,740
We can do that with the kyosaku, it doesn’t bother.

30
00:02:36,875 --> 00:02:39,895
We can also do that.

31
00:02:40,010 --> 00:02:45,140
If the person is too cambered.

32
00:02:48,195 --> 00:02:50,880
We can also do that.

33
00:02:52,790 --> 00:02:56,313
You have to generate a correction that the person will take.

34
00:02:56,405 --> 00:02:59,930
It’s important to do that as well.

35
00:03:04,960 --> 00:03:08,450
It’s very delicate and I don’t disturb the meditation.

36
00:03:11,440 --> 00:03:13,570
One can also use the kyosaku.

37
00:03:16,235 --> 00:03:19,825
We take the wide side down.

38
00:03:20,330 --> 00:03:23,325
We put one knee on the ground.

39
00:03:24,216 --> 00:03:27,264
And we will place it at the place of the arch.

40
00:03:29,334 --> 00:03:31,587
This will give an indication.

41
00:03:32,747 --> 00:03:37,940
Very negative, because you feel like you’re not straight at all.

42
00:03:38,150 --> 00:03:41,930
Because the stick is straight, and the spine is not straight.

43
00:03:48,600 --> 00:03:50,800
The spine is never straight.

44
00:03:51,130 --> 00:03:52,995
It has a slight curvature.

45
00:03:53,175 --> 00:03:59,240
When you put that on, you’re bound to feel twisted.

46
00:03:59,400 --> 00:04:06,705
If you place it well on your back…

47
00:04:06,955 --> 00:04:09,490
It’s a bit of a Spartan method.

48
00:04:13,940 --> 00:04:16,660
You have a beautiful posture, Cristina!

49
00:04:22,280 --> 00:04:25,430
He has a very nice posture.

50
00:04:28,960 --> 00:04:32,320
There are little things you don’t realize.

51
00:04:32,480 --> 00:04:34,340
For example, the thumb there.

52
00:04:36,040 --> 00:04:39,820
It must be straight.

53
00:04:41,850 --> 00:04:44,710
It’s something he doesn’t realize.

54
00:04:44,890 --> 00:04:50,520
But you’re going to go on for a year, two years, and your thumbs are going to be fine.

55
00:04:50,855 --> 00:04:53,735
That’s the first thing I see.

56
00:04:55,180 --> 00:04:58,900
Put your thumbs right up against your belly button.

57
00:05:10,260 --> 00:05:12,600
A little too much in thought.

58
00:05:14,440 --> 00:05:20,300
The head goes a little forward.

59
00:05:28,920 --> 00:05:32,970
I’m a pure osteopath.

60
00:05:34,420 --> 00:05:36,260
Very good!

61
00:05:38,315 --> 00:05:42,875
Relax a little there.

62
00:05:53,292 --> 00:05:54,862
Very beautiful!

63
00:05:56,060 --> 00:06:00,290
When you do gassho, do it well like this.

64
00:06:04,605 --> 00:06:06,335
The posture is beautiful.

65
00:06:08,920 --> 00:06:11,120
A little too cambered.

66
00:06:11,225 --> 00:06:14,765
Decamber a little.

67
00:06:24,537 --> 00:06:28,737
At that moment, because we want to hold on,

68
00:06:35,023 --> 00:06:37,393
No, no, quiet!

69
00:06:37,591 --> 00:06:38,591
Voilà!

70
00:06:38,780 --> 00:06:39,780
Perfect!

71
00:06:45,450 --> 00:06:49,410
Don’t forget that it is the left hand in the right hand.

72
00:06:51,200 --> 00:06:52,950
Relax your hands!

73
00:06:53,010 --> 00:06:55,540
Relax your fingers.

74
00:06:56,150 --> 00:06:58,530
You put them on top of each other.

75
00:07:03,395 --> 00:07:06,015
It’s a little tense, but still…

76
00:07:08,797 --> 00:07:09,947
Here.

77
00:07:16,500 --> 00:07:19,185
What I will do in this case…

78
00:07:19,275 --> 00:07:21,870
I see a lot of tension, that’s normal.

79
00:07:21,910 --> 00:07:24,435
Because in the beginning, you have to do it in force.

80
00:07:24,585 --> 00:07:27,130
You do that.

81
00:07:30,560 --> 00:07:33,810
The hands are not bad now.

82
00:07:34,930 --> 00:07:38,150
Relax, no need for them to be too tense.

83
00:07:39,690 --> 00:07:40,470
Flexible.

84
00:07:40,560 --> 00:07:42,110
That’s good, great!

85
00:07:48,975 --> 00:07:54,395
Me, I would accentuate a little on the fifth vertebra.

86
00:08:03,997 --> 00:08:07,027
You see, I feel some tension.

87
00:08:10,253 --> 00:08:12,099
Very good!

88
00:08:12,829 --> 00:08:15,839
Loosen the elbows well.

89
00:08:19,849 --> 00:08:27,759
Sensei, not on beginners, but he often did that.

90
00:08:27,894 --> 00:08:31,684
He was definitely pushing hard.

91
00:08:33,682 --> 00:08:37,282
For the moment, it is not worth it.

92
00:08:37,941 --> 00:08:39,471
Very good.

93
00:08:40,660 --> 00:08:42,680
You can start to breathe.

94
00:08:42,790 --> 00:08:44,875
Take a breath.

95
00:08:45,065 --> 00:08:46,577
Follow your breathing.

96
00:08:46,727 --> 00:08:48,010
Take pleasure.

97
00:08:54,425 --> 00:08:55,625
That’s very good.

98
00:08:55,717 --> 00:08:58,987
Is this your first sesshin?

99
00:09:00,103 --> 00:09:02,613
Bravo!

100
00:09:02,853 --> 00:09:04,530
I have a question:

101
00:09:04,608 --> 00:09:07,268
For breathing,

102
00:09:07,380 --> 00:09:12,640
sometimes I suck in strength and I don’t know how to stop?

103
00:09:20,830 --> 00:09:22,820
As long as you feel good…

104
00:09:26,310 --> 00:09:29,775
This is my point of view, there are other methods…

105
00:09:29,935 --> 00:09:32,860
But in zazen, for breathing.

106
00:09:33,060 --> 00:09:36,780
You have to feel good, don’t force yourself.

107
00:09:37,130 --> 00:09:40,530
As long as you feel good, you keep going.

108
00:09:45,380 --> 00:09:48,570
You catch your breath when you feel it.

109
00:09:48,731 --> 00:09:51,461
It is your body that must dictate it.

110
00:10:25,390 --> 00:10:27,310
Posture in general

111
00:11:05,935 --> 00:11:08,115
Very good!

112
00:11:15,740 --> 00:11:18,100
You have to look at the main points:

113
00:11:18,190 --> 00:11:21,200
fifth lumbar vertebra, first dorsal,

114
00:11:21,266 --> 00:11:24,790
head, arms not too tight.

115
00:11:26,070 --> 00:11:31,060
The person can be helped by a position of the hands.

116
00:11:31,240 --> 00:11:34,280
I would like to see a kin-hin posture.

117
00:11:41,400 --> 00:11:44,925
There are only good postures.

118
00:11:46,485 --> 00:11:50,500
He has a very beautiful posture, very natural.

119
00:11:51,230 --> 00:11:59,200
Except for one detail: the little finger must be very tight.

120
00:11:59,785 --> 00:12:01,855
People often forget to tighten it.

121
00:12:01,967 --> 00:12:03,822
Sensei always said:

122
00:12:03,982 --> 00:12:08,977
the two little fingers must be tightly together.

123
00:12:15,043 --> 00:12:17,623
I even corrected you the other time…

124
00:12:19,766 --> 00:12:22,006
Very good, bravo!

125
00:12:24,018 --> 00:12:27,238
There are often people with postures like that.

126
00:12:27,424 --> 00:12:28,974
Should they be corrected?

127
00:12:29,047 --> 00:12:30,387
This should be corrected.

128
00:12:30,510 --> 00:12:33,615
For example Pierre, take the posture…

129
00:12:33,920 --> 00:12:38,170
I’ve noticed something, let’s see if you do…

130
00:12:46,127 --> 00:12:50,227
The hand is a bit like that…

131
00:12:55,366 --> 00:12:57,176
Loosen up a bit.

132
00:12:59,735 --> 00:13:04,130
It really needs to be layered.

133
00:13:13,740 --> 00:13:15,870
Sometimes you can make a mistake.

134
00:13:15,935 --> 00:13:18,195
If it is corrected…

135
00:13:18,317 --> 00:13:20,887
Tighten the little fingers a little…

136
00:13:23,093 --> 00:13:27,443
Sensei always showed energy in both fingers.

137
00:13:27,821 --> 00:13:30,621
We do it like this, then like this.

138
00:13:32,250 --> 00:13:35,010
And then this one is put on top.

139
00:13:36,000 --> 00:13:38,040
Well perpendicular.

140
00:13:38,310 --> 00:13:42,130
When you open your hands, they are parallel to the ground.

141
00:13:44,370 --> 00:13:46,580
We must avoid ending up like this…

142
00:13:47,610 --> 00:13:48,830
It’s good

143
00:13:48,990 --> 00:13:50,410
It’s perfect.
