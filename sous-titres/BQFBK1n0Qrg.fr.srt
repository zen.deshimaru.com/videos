1
00:00:09,675 --> 00:00:13,085
Bonjour. Aujourd’hui, je voudrais vous expliquer

2
00:00:13,280 --> 00:00:15,940
comment pratiquer une marche méditative

3
00:00:16,760 --> 00:00:21,800
que nous pratiquons dans le zen, cette marche s’appelle kin-hin.

4
00:00:22,180 --> 00:00:25,640
C’est une marche assez courte,

5
00:00:25,920 --> 00:00:30,740
que l’on pratique entre deux moments de méditation assise

6
00:00:30,920 --> 00:00:32,220
le zazen

7
00:00:32,460 --> 00:00:35,380
tel que je l’ai expliqué dans une vidéo précédente.

8
00:00:35,760 --> 00:00:39,220
En général, une séance de méditation entière

9
00:00:39,440 --> 00:00:43,380
comprend une première partie de méditation assise, zazen,

10
00:00:43,660 --> 00:00:49,800
puis cette petite marche méditative que je vais vous expliquer

11
00:00:50,060 --> 00:00:52,360
qui s’appelle kin-hin,

12
00:00:52,640 --> 00:00:54,820
et après ce kin-hin

13
00:00:54,820 --> 00:00:57,580
nous revenons là où est notre coussin de méditation

14
00:00:57,580 --> 00:01:00,600
pour une deuxième partie

15
00:01:00,600 --> 00:01:04,460
de zazen, face au mur, en silence.

16
00:01:12,340 --> 00:01:15,640
La marche méditative, kin-hin,

17
00:01:16,445 --> 00:01:17,915
il s’agit d’une marche,

18
00:01:18,480 --> 00:01:21,380
vous allez mettre vos pieds

19
00:01:21,760 --> 00:01:26,320
si vous regardez la position de mes pieds, de cette façon,

20
00:01:26,600 --> 00:01:30,900
c’est-à-dire qu’ils sont espacés, plus ou moins, de la largeur de votre poing,

21
00:01:31,780 --> 00:01:36,100
sans distorsion de votre bassin.

22
00:01:37,580 --> 00:01:41,180
Et vous allez faire des pas

23
00:01:41,360 --> 00:01:44,480
de la largeur d’un demi-pied

24
00:01:44,800 --> 00:01:47,540
un pied c’est plus ou moins 30-33 cm

25
00:01:47,880 --> 00:01:51,160
donc ça va faire des pas de 15 centimètres ou un peu plus.

26
00:01:51,660 --> 00:01:54,480
Si vous regardez,

27
00:01:54,680 --> 00:01:57,660
voyez, je fais un pas d’un demi-pied.

28
00:01:58,040 --> 00:02:03,580
J’ai donc à un moment une jambe devant et une jambe derrière

29
00:02:03,840 --> 00:02:12,240
la jambe derrière passera devant en faisant un pas de la largeur d’un demi-pied.

30
00:02:12,240 --> 00:02:15,880
Veillez bien à ce que vos pieds soient

31
00:02:16,240 --> 00:02:18,560
bien droits et non ainsi.

32
00:02:27,700 --> 00:02:30,875
Maintenant la posture des mains.

33
00:02:30,875 --> 00:02:33,685
Pendant que nous marchons, tel que je viens de l’expliquer,

34
00:02:33,685 --> 00:02:36,820
nous avons également une activité avec nos mains.

35
00:02:37,520 --> 00:02:40,300
Vous vous souvenez, la posture de méditation zen,

36
00:02:40,620 --> 00:02:42,520
nous avions les mains comme cela.

37
00:02:43,140 --> 00:02:45,660
J’enlève ma main droite,

38
00:02:45,980 --> 00:02:49,180
avec ma main gauche, je plie le pouce de la main gauche

39
00:02:49,500 --> 00:02:55,000
et je l’enserre avec tous mes doigts bien collés,

40
00:02:55,320 --> 00:03:01,700
je laisse dépasser la racine de mon pouce gauche.

41
00:03:02,900 --> 00:03:05,140
Ma main droite qui enveloppait

42
00:03:05,480 --> 00:03:08,765
enveloppe la main gauche

43
00:03:08,765 --> 00:03:14,600
les doigts bien joints, le pouce également, tout collés les uns aux autres

44
00:03:14,800 --> 00:03:18,640
je plie légèrement les poignets

45
00:03:19,380 --> 00:03:22,320
voyez, la racine de mon pouce gauche sort

46
00:03:22,940 --> 00:03:25,980
et cette racine

47
00:03:26,340 --> 00:03:29,700
je vais l’appliquer ici, à la base du sternum.

48
00:03:30,320 --> 00:03:35,180
Tous les êtres humains, à la base du sternum ont deux-trois petits trous.

49
00:03:35,720 --> 00:03:44,200
C’est ici que vous allez poser, faire un contact entre la racine de votre pouce gauche et le plexus.

50
00:03:44,780 --> 00:03:55,440
Comme vous le voyez, je plie légèrement les poignets, de telle façon que mes avant-bras sont dans la même ligne.

51
00:03:55,860 --> 00:03:59,160
Je n’ai pas un bras au-dessus de l’autre.

52
00:03:59,600 --> 00:04:03,020
Les paumes de mes mains sont bien parallèles au sol,

53
00:04:03,540 --> 00:04:07,340
je veux dire par là que je ne suis pas comme ceci

54
00:04:07,680 --> 00:04:11,140
je n’ai pas les bras qui tombent

55
00:04:11,520 --> 00:04:16,700
et je ne maintiens pas les bras avec la force physique en levant mes coudes.

56
00:04:17,800 --> 00:04:21,040
Que se passe-t-il?

57
00:04:21,560 --> 00:04:30,260
Vous vous souvenez quand on expire, on met la pression, tout le poids du corps sur la jambe avant.

58
00:04:30,560 --> 00:04:33,640
Pendant cette phase,

59
00:04:34,200 --> 00:04:40,720
de la même façon, nous allons appuyer les mains l’une contre l’autre,

60
00:04:41,080 --> 00:04:49,220
exercer une légère pression et en même temps une pression de la racine du pouce gauche contre le plexus.

61
00:04:49,580 --> 00:04:54,640
Évidemment, délicatement, il ne s’agit pas de se faire violence.

62
00:04:54,840 --> 00:04:59,700
Vous verrez que si vous êtes un peu trop bas vous touchez un point de l’estomac

63
00:05:00,080 --> 00:05:04,340
vous allez vite vous rendre compte que ce n’est pas agréable

64
00:05:04,820 --> 00:05:09,720
donc naturellement vous allez lever pour trouver la bonne posture.

65
00:05:13,600 --> 00:05:16,880
À l’inspiration je passe la jambe devant,

66
00:05:17,160 --> 00:05:18,460
tout est relâché.

67
00:05:18,860 --> 00:05:21,880
Tout est en posture, mais c’est relâché.

68
00:05:22,300 --> 00:05:26,300
Pendant la phase de l’expiration,

69
00:05:27,080 --> 00:05:34,440
je mets le poids sur la jambe avant, j’appuie légèrement contre le plexus, et les deux mains l’une contre l’autre.

70
00:05:35,080 --> 00:05:40,920
Je me mets de profil, comme vous le voyez, c’est comme pendant zazen,

71
00:05:41,200 --> 00:05:49,180
je garde la colonne le plus droit possible, la nuque étirée, le menton rentré

72
00:05:49,520 --> 00:05:53,840
mon regard 45 degrés en diagonale vers le bas,

73
00:05:54,460 --> 00:05:59,560
je suis concentré totalement, ici et maintenant sur ce que je fais.

74
00:06:00,200 --> 00:06:03,180
Et ce que je fais, je suis une marche méditative.

75
00:06:03,580 --> 00:06:08,420
Vous ne suivez pas vos pensées, vous n’êtes pas en train de regarder à l’extérieur,

76
00:06:08,640 --> 00:06:14,260
vous continuez avec la concentration du zazen, la concentration de la posture assise.

77
00:06:22,020 --> 00:06:27,600
Le rythme de la marche va être le rythme de votre respiration.

78
00:06:28,120 --> 00:06:31,380
Vous vous souvenez, pendant la méditation assise,

79
00:06:31,760 --> 00:06:35,840
on est concentré ici et maintenant sur les points physiques de la posture,

80
00:06:36,500 --> 00:06:41,380
l’état d’esprit, l’attitude mentale, on laisse passer ses pensées,

81
00:06:41,640 --> 00:06:44,660
on ne les entretient pas

82
00:06:44,920 --> 00:06:48,000
et on se concentre ici et maintenant sur notre respiration

83
00:06:48,340 --> 00:06:51,560
en établissant un va et vient harmonieux

84
00:06:51,960 --> 00:06:55,780
et en se concentrant spécialement sur l’expiration

85
00:06:57,160 --> 00:07:01,260
de plus en plus longue, profonde,

86
00:07:01,580 --> 00:07:04,540
abdominale, qui va jusqu’en bas du ventre.

87
00:07:05,860 --> 00:07:15,440
La respiration et l’attitude mentale sont la même que nous soyons assis, face au mur, en silence, immobile, en zazen

88
00:07:16,280 --> 00:07:24,660
ou en faisant cette méditation en marchant. La seule chose qui change c’est que nous allons bouger.

89
00:07:30,320 --> 00:07:33,880
Marchez en fonction du rythme de votre respiration.

90
00:07:34,160 --> 00:07:41,660
Pendant l’inspiration vous passez une jambe devant

91
00:07:42,520 --> 00:07:45,000
un demi-pied

92
00:07:45,180 --> 00:07:50,220
et pendant la phase d’expiration,

93
00:07:52,440 --> 00:07:55,420
par le nez, en silence,

94
00:07:56,100 --> 00:07:59,820
vous allez progressivement placer tout le poids du corps

95
00:08:00,100 --> 00:08:03,240
sur la jambe avant

96
00:08:03,520 --> 00:08:06,880
la jambe avant est bien tendue

97
00:08:07,180 --> 00:08:11,480
la plante des pieds complètement en contact avec le sol

98
00:08:12,080 --> 00:08:15,680
la jambe derrière elle, reste relâchée,

99
00:08:16,180 --> 00:08:20,440
mais également toute la plante du pied en contact avec le sol.

100
00:08:20,740 --> 00:08:24,320
Si vous voyez le pied, je ne suis pas avec un talon en l’air.

101
00:08:24,700 --> 00:08:29,860
J’ai les talons des deux pieds appuyés sur le sol.

102
00:08:30,480 --> 00:08:33,280
Donc, pendant la phase de l’expiration,

103
00:08:33,740 --> 00:08:40,180
progressivement je passe tout le poids de mon corps sur cette jambe avant

104
00:08:40,460 --> 00:08:43,520
jusqu’à la racine du gros orteil

105
00:08:43,920 --> 00:08:46,820
sans crisper

106
00:08:47,380 --> 00:08:51,380
c’est simplement le poids qui fait que j’appuie sur le sol.

107
00:08:51,760 --> 00:08:57,960
Comme si vous étiez sur un terrain meuble comme du sable ou de l’argile

108
00:08:58,280 --> 00:09:05,800
et que alliez vouloir laisser votre empreinte dans le sable ou dans l’argile.

109
00:09:06,700 --> 00:09:10,420
Je vous montre de profil.

110
00:09:10,920 --> 00:09:14,580
J’inspire et je passe la jambe devant,

111
00:09:15,100 --> 00:09:20,280
j’expire, tout le poids de mon corps sur la jambe avant.

112
00:09:20,640 --> 00:09:23,420
Je suis à la fin de l’expiration,

113
00:09:23,680 --> 00:09:28,940
j’inspire et je passe la jambe derrière devant

114
00:09:29,960 --> 00:09:31,120
j’expire

115
00:09:32,620 --> 00:09:37,340
tout le poids de mon corps sur la jambe avant.

116
00:09:37,780 --> 00:09:44,240
Fin de l’expiration, j’inspire, la jambe derrière passe devant.

117
00:09:51,240 --> 00:09:55,380
Essayez-le et vous allez voir que la marche kin-hin

118
00:09:55,640 --> 00:10:01,920
c’est une marche que vous pourrez après utiliser dans beaucoup de circonstances de votre vie

119
00:10:02,360 --> 00:10:12,360
5 minutes d’attente, vous êtes stressé et bien, pratiquez cela, 3-4 minutes de cette marche

120
00:10:12,660 --> 00:10:18,980
et vous allez voir comme déjà vous allez vous calmer, vous sentir plus en harmonie avec vous.

121
00:10:19,860 --> 00:10:21,240
Bonne pratique
