1
00:00:00,100 --> 00:00:01,460
My name is Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
My Zen monk is called Ryurin. 

3
00:00:03,880 --> 00:00:05,580
I am also a calligrapher. 

4
00:00:05,900 --> 00:00:09,560
I teach Chinese calligraphy, which I discovered in Zen. 

5
00:00:09,840 --> 00:00:13,860
I learned it from a Zen monk. 

6
00:00:14,320 --> 00:00:18,260
I like to share calligraphy with everyone. 

7
00:00:18,860 --> 00:00:22,540
I do a lot of work in schools. 

8
00:00:22,900 --> 00:00:27,360
I do calligraphy with children in workshops. 

9
00:00:27,700 --> 00:00:31,020
Also for adults and disabled people. 

10
00:00:31,380 --> 00:00:33,300
They like it a lot. 

11
00:00:33,780 --> 00:00:39,220
My youngest students are 6 years old, and my oldest student is 90. 

12
00:00:39,760 --> 00:00:42,800
I also teach Chinese. 

13
00:00:43,180 --> 00:00:44,840
That is my core business. 

14
00:00:45,140 --> 00:00:48,020
This calligraphy is simply "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
It is a calligraphy made in the current semi-cursive style. 

16
00:00:54,580 --> 00:01:06,760
The brush is short, it goes from dots to lines, to form a Chinese or Japanese character. 

17
00:01:08,420 --> 00:01:11,320
This is "Reiki" (靈氣), the spiritual energy. 

18
00:01:11,620 --> 00:01:13,880
It is a healing technique. 

19
00:01:14,220 --> 00:01:20,260
We continue this tradition because it comes from a master who has practiced a lot of calligraphy. 

20
00:01:20,500 --> 00:01:23,920
He also practiced sumi-e, (墨 絵), he was a painter. 

21
00:01:24,280 --> 00:01:27,580
I really appreciate Master Deshimaru’s paintings, he was an artist. 

22
00:01:27,840 --> 00:01:31,020
He painted and made calligraphy very well. 

23
00:01:31,440 --> 00:01:37,860
His calligraphy is very strong, dense and energetic. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) is the name of a sutra that is sung. 

25
00:01:43,700 --> 00:01:52,480
I traced it in curly: kind of like little spaghetti. 

26
00:01:52,780 --> 00:01:56,440
The characters are very linked and simplified. 

27
00:01:56,780 --> 00:02:00,800
We also like that in Zen practice. 

28
00:02:01,240 --> 00:02:08,040
Zen practitioners really liked this writing, which is quite free and expressive. 

29
00:02:09,260 --> 00:02:10,740
It is an energetic art. 

30
00:02:11,020 --> 00:02:14,840
It is practiced with the whole body, in connection with the breathing. 

31
00:02:15,240 --> 00:02:21,900
In connection with his availability of the mind, his consciousness. 

32
00:02:22,360 --> 00:02:24,720
So it is an energy art. 

33
00:02:25,000 --> 00:02:28,120
We are going to calm down, reorient something within ourselves. 

34
00:02:28,360 --> 00:02:33,240
It is a complete metamorphosis, an alchemy of balance. 

35
00:02:33,560 --> 00:02:40,040
Horizontality, verticality, centering, coupling when drawing the signs. 

36
00:02:40,540 --> 00:02:43,120
Availability by hand. 

37
00:02:43,460 --> 00:02:48,080
In Zen meditation, the presence in the hands is very important. 

38
00:02:48,420 --> 00:02:55,600
Whether it is stationary or running. 

39
00:02:55,940 --> 00:02:57,760
The gesture is different in calligraphy. 

40
00:02:58,180 --> 00:03:00,120
But there is a presence by the hands. 

41
00:03:00,340 --> 00:03:02,320
It is the hand-to-brain relationship. 

42
00:03:02,600 --> 00:03:04,140
Calligraphy has a life. 

43
00:03:04,440 --> 00:03:05,440
Even if you don’t know about it! 

44
00:03:05,680 --> 00:03:07,980
Some calligraphy will touch you, others will not. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) is Chinese, and it is more Taoist. 

46
00:03:11,740 --> 00:03:15,820
It means "nourishing the vital principle." 

47
00:03:16,540 --> 00:03:23,440
If you liked this video, feel free to like it and subscribe to the channel! 

