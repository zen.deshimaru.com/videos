1
00:00:00,080 --> 00:00:00,920
domanda: 

2
00:00:01,140 --> 00:00:05,380
A volte sento nella meditazione zazen una sensazione molto piacevole. 

3
00:00:05,680 --> 00:00:09,880
Un po ’di formicolio su tutto il corpo. 

4
00:00:10,340 --> 00:00:15,040
Come piccole scosse elettriche. 

5
00:00:15,440 --> 00:00:17,880
Risposta del Maestro Kosen: 

6
00:00:18,400 --> 00:00:22,420
Tutti i sentimenti possono essere buoni o cattivi. 

7
00:00:23,080 --> 00:00:28,760
La stessa sensazione può sembrare buona o cattiva. 

8
00:00:29,220 --> 00:00:33,520
L’importante è che sia piacevole. 

9
00:00:34,180 --> 00:00:37,240
Che ti possa rilassare. 

10
00:00:37,500 --> 00:00:42,000
Di solito è correlato alla circolazione del sangue. 

11
00:00:42,340 --> 00:00:50,640
Nella posa del loto, c’è poca circolazione sanguigna nelle gambe. 

12
00:00:51,020 --> 00:00:55,960
Circola molto di più nella parte addominale. 

13
00:00:56,400 --> 00:01:00,840
La parte superiore del corpo, compreso il cervello, è meglio irrigata. 

14
00:01:01,100 --> 00:01:05,240
Può causare una sensazione piacevole. 

15
00:01:05,500 --> 00:01:09,660
È buono, perché è un pensiero positivo. 

16
00:01:10,060 --> 00:01:13,580
Quando ti senti bene, è una coscienza positiva. 

17
00:01:13,820 --> 00:01:16,240
Inoltre influenza le nostre vite. 

18
00:01:16,480 --> 00:01:18,160
Il maestro Deshimaru diceva sempre: 

19
00:01:18,380 --> 00:01:20,500
“Zazen non è una mortificazione. " 

20
00:01:20,840 --> 00:01:26,260
Ma quando devi affrontare il tuo corpo, 

21
00:01:26,620 --> 00:01:31,480
alcune volte sono spiacevoli o dolorose. 

22
00:01:31,860 --> 00:01:34,940
Quando ti senti bene è fantastico! 

