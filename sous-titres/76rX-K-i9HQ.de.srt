1
00:00:00,280 --> 00:00:03,640
Alltag 

2
00:00:03,840 --> 00:00:06,460
Eines Tages … 

3
00:00:07,200 --> 00:00:12,320
Tagsüber müssen wir aufhören, was wir regelmäßig tun. 

4
00:00:13,040 --> 00:00:15,320
Stoppen Sie seinen Job. 

5
00:00:15,780 --> 00:00:18,780
Und komm schon. 

6
00:00:19,020 --> 00:00:22,220
In seiner Arbeit, im Alltag findet man den Geist. 

7
00:00:22,560 --> 00:00:24,720
Es dauert nur zwei Atemzüge. 

8
00:00:25,060 --> 00:00:26,340
Sie arbeiten an Ihrem Schreibtisch … 

9
00:00:26,680 --> 00:00:28,820
Richten Sie es einfach aus. 

10
00:00:29,340 --> 00:00:32,760
Und atme. 

11
00:00:41,120 --> 00:00:49,260
Und um sofort zu seinem Sein zurückzukehren. 

12
00:00:50,960 --> 00:00:54,140
Ich kannte eine Gemeinde namens Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
Sie arbeiten sehr hart. 

14
00:00:57,580 --> 00:01:00,400
Sie bearbeiten das Land. 

15
00:01:00,660 --> 00:01:02,660
Sie bauen ihr Haus. 

16
00:01:02,880 --> 00:01:04,540
Sie machen viel Handarbeit. 

17
00:01:04,740 --> 00:01:06,040
Sie arbeiten den ganzen Tag. 

18
00:01:06,300 --> 00:01:07,500
Sie stehen um 5 Uhr morgens auf. 

19
00:01:07,720 --> 00:01:08,820
Sie sagen ihre Gebete. 

20
00:01:09,080 --> 00:01:10,840
Sie frühstücken. 

21
00:01:11,160 --> 00:01:12,420
Sie gehen zur Arbeit. 

22
00:01:12,780 --> 00:01:15,820
Jede Stunde läutet eine Glocke. 

23
00:01:16,180 --> 00:01:18,060
Jeder muss aufhören zu arbeiten. 

24
00:01:18,720 --> 00:01:25,520
Und konzentrieren Sie sich für eine Minute neu. 

25
00:01:26,060 --> 00:01:28,960
Jeder auf seine Weise. 

26
00:01:34,080 --> 00:01:36,040
Und dann gehen sie zurück. 

27
00:01:36,560 --> 00:01:39,160
Es gibt also nicht nur Zazen. 

28
00:01:39,540 --> 00:01:42,100
Es gibt viele Möglichkeiten, zu sich selbst zurückzukehren. 

29
00:01:42,640 --> 00:01:45,220
Das tägliche Leben der Zen-Mönche … 

30
00:01:45,540 --> 00:01:48,820
Wir leben unser Leben für Zazen. 

31
00:01:49,080 --> 00:01:52,150
Um dieses Bewusstsein, dieses Wissen zu sammeln. 

32
00:01:52,440 --> 00:01:53,580
Diese Energie. 

33
00:01:54,880 --> 00:01:58,780
Zum Zeitpunkt der Zazen-Erfahrung. 

34
00:01:58,900 --> 00:02:03,120
Es geht nicht darum, dein Leben für Zazen aufzugeben. 

35
00:02:03,380 --> 00:02:05,400
In einem sektiererischen Geist. 

36
00:02:05,740 --> 00:02:07,680
Es ist keine Ideologie. 

37
00:02:07,900 --> 00:02:10,380
Es ist kein "-ismus". 

38
00:02:10,840 --> 00:02:14,780
Es ist: "Ich lebe mein Leben so, dass ich diese Erfahrung machen kann." 

39
00:02:15,060 --> 00:02:16,560
"So tief wie möglich." 

40
00:02:16,900 --> 00:02:18,980
Dies ist eine Erfahrung der Zazen-Meditation. 

41
00:02:19,220 --> 00:02:25,060
Wenn Sie in Zazen sind, haben Sie die Möglichkeit, Ihr ganzes Leben zu verändern. 

42
00:02:25,400 --> 00:02:27,900
Darum geht es bei Zazen! 

43
00:02:28,340 --> 00:02:31,980
Es ist kein geringer Weg, sich gut zu fühlen. 

44
00:02:32,500 --> 00:02:35,040
Zen im Alltag … 

45
00:02:35,640 --> 00:02:39,200
Auch in Profit und Arbeit … 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku" (ohne Ziel oder Profitgeist) … 

47
00:02:41,760 --> 00:02:43,140
das ist das geheimnis. 

48
00:02:43,340 --> 00:02:46,320
Mit anderen Worten, konzentrieren Sie sich auf Ihre Essenz. 

49
00:02:46,540 --> 00:02:48,880
Dann können wir alles bekommen. 

50
00:02:49,080 --> 00:02:50,360
Wir haben alles. 

51
00:02:50,880 --> 00:02:55,280
Was ist der Unterschied zwischen dem Leben eines Meisters und dem eines gewöhnlichen Wesens? 

52
00:02:55,660 --> 00:02:59,520
Es gibt kein gewöhnliches. 

53
00:02:59,840 --> 00:03:05,780
Es gibt nur außergewöhnliche Kreaturen. 

54
00:03:06,660 --> 00:03:14,900
Das Schlimme ist, dass die Leute nicht wissen, dass sie außergewöhnlich sind. 

55
00:03:16,420 --> 00:03:22,260
Ich mag den Satz "sei einfach" nicht! 

