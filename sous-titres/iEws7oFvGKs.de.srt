1
00:00:06,180 --> 00:00:11,550
Ich praktiziere Zazen seit mehr als der Hälfte meines Lebens.

2
00:00:14,890 --> 00:00:20,170
Und es ist sehr selten, dass ich in perfekter Balance bin.

3
00:00:25,650 --> 00:00:29,445
Es besteht also nur eine sehr geringe Chance

4
00:00:29,605 --> 00:00:33,910
dass mir der Buddha völlig klar ist?

5
00:00:38,110 --> 00:00:40,910
Der Buddha hatte auch einen schlimmen Rücken!

6
00:00:47,340 --> 00:00:54,790
Wir versuchen, mit etwas Vollkommenem und Lebendigem zu harmonisieren.

7
00:00:55,110 --> 00:00:59,740
Das Perfekte an uns ist, dass wir am Leben sind.

8
00:01:01,860 --> 00:01:06,660
Es gibt Leute, die darüber nachdenken
der genetischen Veränderung.

9
00:01:13,480 --> 00:01:22,120
In Fabeln heißt es, dass der Buddha
die zweiunddreißig Zeichen der Perfektion.

10
00:01:23,400 --> 00:01:27,440
Das heißt, es hatte keine Mängel.

11
00:01:41,780 --> 00:01:48,180
Es ist gut genug, nah heranzukommen,
um seinen Gott nachzuahmen.

12
00:01:51,530 --> 00:01:55,780
Aber ich glaube, der Buddha war ein Mann wie wir.
und dass er Fehler hatte.

13
00:02:02,690 --> 00:02:05,685
Der erste Makel ist, dass er tot ist.

14
00:02:05,685 --> 00:02:08,710
Hätte er keinen Makel gehabt, wäre er nicht gestorben.

15
00:02:11,630 --> 00:02:15,510
Er starb, er brachte alle seine Jünger zum Weinen.

16
00:02:18,733 --> 00:02:21,406
Wir machen Zazen mit einem unvollkommenen Körper.

17
00:02:21,686 --> 00:02:24,100
Wie ein unvollkommenes Wesen.

18
00:02:26,130 --> 00:02:30,650
Aber was groß genug ist, gibt uns die Richtung vor

19
00:02:32,670 --> 00:02:39,020
zellulär, muskulär, geistig.

20
00:02:39,670 --> 00:02:43,320
Die Richtung, der man sich nähern oder sie nachahmen sollte.

21
00:02:48,450 --> 00:02:51,840
Muslime tragen zum Beispiel keinen Buddha.

22
00:02:51,910 --> 00:02:55,900
Sie sagen: "Wir haben keine Statue,
kein Abbild Gottes".

23
00:02:57,440 --> 00:02:59,165
Ich denke, es ist ziemlich gut.

24
00:03:02,695 --> 00:03:07,520
Aber wir kennen die Richtung, Gott nachzuahmen.

25
00:03:11,630 --> 00:03:18,510
Alles zu tun, was wir tun können, um unser Sein göttlich zu machen.

26
00:03:22,120 --> 00:03:24,730
Aber wir sind nicht perfekt
und wir werden sterben.

27
00:03:27,000 --> 00:03:29,375
Und es gibt einige, die nicht perfekt sind…

28
00:03:29,375 --> 00:03:31,932
und andere, die NICHT VOLLSTÄNDIG sind.

29
00:03:34,552 --> 00:03:39,830
Oftmals müssen perfekte Menschen nicht…

30
00:03:40,390 --> 00:03:43,060
Ich, als ich Musik gemacht habe…

31
00:03:46,120 --> 00:03:49,685
Ich erzähle diese Geschichte oft.

32
00:03:50,445 --> 00:03:53,590
…hatte ich einen Produzenten.

33
00:03:54,070 --> 00:03:58,640
Er war ein italienischer Multimillionär…

34
00:04:00,860 --> 00:04:04,415
Egal, was er getan hat.

35
00:04:05,265 --> 00:04:10,450
Er war sehr reich und wollte einen Verlag gründen.

36
00:04:13,480 --> 00:04:17,123
Er bot mir an, meine Platte zu produzieren.

37
00:04:19,480 --> 00:04:23,820
Und ich weiß nicht, warum, er kam vorbei
im Temple de la Gendronnière.

38
00:04:24,780 --> 00:04:28,520
Während der Tage der Vorbereitung auf das Sommercamp.

39
00:04:28,750 --> 00:04:33,050
Er sagte: "Ich wollte sehen, was ihr da macht!"

40
00:04:33,520 --> 00:04:36,215
Wir zeigten ihm die Regeln des Dojos:

41
00:04:36,425 --> 00:04:39,417
"Sie kommen so rein, dann setzen Sie sich."

42
00:04:39,877 --> 00:04:42,420
Er setzt sich hin: voller Lotus direkt.

43
00:04:46,690 --> 00:04:48,450
Tadelloser Lotus.

44
00:04:48,570 --> 00:04:51,190
"Nun, wie ich sehe, haben Sie sich alles gut überlegt!"

45
00:04:51,190 --> 00:04:54,080
"Zazen wird in einer Stunde stattfinden."

46
00:04:55,870 --> 00:04:58,580
"Sie können den Tempel besuchen."

47
00:04:58,760 --> 00:05:02,870
Er machte eine Stunde lang sein Zazen.

48
00:05:04,300 --> 00:05:06,060
- "War das nicht schwer?"

49
00:05:06,190 --> 00:05:08,720
- "Nein, nein, es war sehr schön!"

50
00:05:11,590 --> 00:05:14,655
Ich schwöre, es ist wahr!

51
00:05:15,314 --> 00:05:18,080
Und er hat nie wieder in seinem Leben Zazen gemacht.

52
00:05:19,420 --> 00:05:21,920
Er ist derjenige, der mir eine Lektion erteilt hat.

53
00:05:27,150 --> 00:05:32,332
Vor zwei oder drei Jahren, mein Enkel, ungefähr so hoch…

54
00:05:32,632 --> 00:05:37,460
sagte zu mir: "Großvater, zeig mir Zazen!"

55
00:05:40,840 --> 00:05:45,540
Ich brachte ihn in den Raum.
wo ich online Zazen durch Zoom mache.

56
00:05:47,140 --> 00:05:50,780
Ich sagte: "Setzen Sie sich hier hin, legen Sie Ihre Beine so hin."

57
00:05:51,000 --> 00:05:52,019
Er ist gestresst.

58
00:05:52,115 --> 00:05:55,950
Er sieht aus wie Stephen Zeisler [Schüler von Deshimaru].

59
00:05:58,050 --> 00:06:00,410
- "Du stellst dich selbst so dar."

60
00:06:00,570 --> 00:06:04,130
Ich korrigiere seine Körperhaltung.

61
00:06:04,400 --> 00:06:06,485
Er hat eine tolle Körperhaltung.

62
00:06:06,575 --> 00:06:08,750
Ich sagte: "Das war's, das war's!"

63
00:06:08,910 --> 00:06:10,250
- "Danke, Opa!"

64
00:06:15,755 --> 00:06:17,270
Er hat's drauf.

65
00:06:17,350 --> 00:06:19,645
Er weiß, was Zazen ist.

66
00:06:22,430 --> 00:06:25,660
Ich weiß nicht, auf welche Frage ich geantwortet habe…

67
00:06:27,345 --> 00:06:30,995
Gibt es die volle Verwirklichung des Buddha?

68
00:06:33,520 --> 00:06:37,090
Alles ist die totale Verwirklichung des Buddha!

69
00:06:38,090 --> 00:06:42,240
Auch wenn wir verdreht sind, auch wenn wir kein Zazen machen.

70
00:06:43,130 --> 00:06:47,830
Deshalb ist es manchmal gut zu sagen, dass Zazen nutzlos ist.

71
00:06:49,410 --> 00:06:55,340
Denn wenn Sie zu Hause Zazen in kurzen Hosen machen

72
00:06:55,760 --> 00:07:01,180
kein Kesa, keine Zeremonie, keine Freunde,
ohne Meister, ohne Jünger,

73
00:07:01,980 --> 00:07:07,360
Sie sagen sich: "Es sind die gleichen Zazen
das ich als Neuling immer gemacht habe!"

74
00:07:07,970 --> 00:07:12,030
Zuerst, als ich zu Hause war und nachgedacht habe:

75
00:07:12,030 --> 00:07:14,410
"Ah, ich habe Lust, Zazen zu machen!"

76
00:07:16,521 --> 00:07:17,751
Ich dachte…
