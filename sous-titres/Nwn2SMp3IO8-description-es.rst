Zen y sofrología: posturas fundamentales y su influencia en el cerebro

El maestro Kosen ha sido invitado al congreso de 2013 de la Federación
de Escuelas Profesionales de Sofrología, en Francia (www.sophro.fr).

Explica las cuatro posturas fundamentales del Zen y su influencia en
el cerebro. Vuelve a las fuentes de relajación dinámica. Insiste en la
importancia de los tres pilares de la postura, la respiración y el
estado de ánimo en la meditación budista, especialmente en la práctica
del Zen, el zazen.
