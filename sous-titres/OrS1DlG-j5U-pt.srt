1
00:00:00,000 --> 00:00:08,750
Eu faço zazen com uma kesa da Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
Seda.

3
00:00:15,330 --> 00:00:19,215
Estou muito feliz.

4
00:00:20,235 --> 00:00:22,830
Eu gosto muito da Barbara.

5
00:00:24,580 --> 00:00:27,210
Ela é uma grande santa.

6
00:00:33,770 --> 00:00:38,190
Felizmente, meus discípulos em geral são melhores do que eu.

7
00:00:42,910 --> 00:00:45,830
Especialmente Bárbara.

8
00:00:48,850 --> 00:00:51,270
E Ariadna também.

9
00:00:55,710 --> 00:00:58,515
Os três tesouros.

10
00:01:01,015 --> 00:01:03,817
Buda, dharma, sangha.

11
00:01:06,267 --> 00:01:10,440
são inseparáveis.

12
00:01:24,820 --> 00:01:28,470
Eu o preparei como um kusen…

13
00:01:35,340 --> 00:01:41,560
…a continuação do que fiz no acampamento de verão.

14
00:01:45,170 --> 00:01:50,930
Sobre os quatro fundamentos dos poderes mágicos.

15
00:01:56,750 --> 00:02:01,770
Fiz isto no acampamento de verão…

16
00:02:02,130 --> 00:02:10,530
Dizem que as quatro fundações são como os quatro cascos de um cavalo.

17
00:02:35,940 --> 00:02:40,120
A primeira é a força de vontade, a volição.

18
00:02:44,460 --> 00:02:50,300
A volição é algo mais forte do que você.

19
00:02:52,290 --> 00:02:55,400
E isso o impulsiona a agir.

20
00:02:58,920 --> 00:03:02,400
É como uma questão de sobrevivência.

21
00:03:06,430 --> 00:03:08,350
É muito forte.

22
00:03:09,410 --> 00:03:12,160
Eu falei sobre isso no acampamento de verão.

23
00:03:15,530 --> 00:03:18,550
A segunda é o espírito.

24
00:03:20,745 --> 00:03:23,015
O segundo casco.

25
00:03:27,730 --> 00:03:33,390
O terceiro é o progresso, avançando.

26
00:03:38,440 --> 00:03:40,810
O quarto é pensado.

27
00:03:43,810 --> 00:03:46,855
Isso é o que estou pensando.

28
00:03:48,135 --> 00:03:50,020
A segunda.

29
00:03:54,970 --> 00:03:57,515
Eu falei sobre isso no acampamento de verão,

30
00:03:57,625 --> 00:04:00,860
mas eu ainda não tinha terminado.

31
00:04:03,270 --> 00:04:09,595
Vou contar três histórias Zen…

32
00:04:11,995 --> 00:04:16,060
que fazem com que a natureza da mente seja melhor compreendida.

33
00:04:19,990 --> 00:04:25,860
Mestre Eka, um discípulo de Bodhidharma.

34
00:04:32,370 --> 00:04:35,760
…de pé na neve…

35
00:04:39,330 --> 00:04:41,145
…gelado…

36
00:04:44,635 --> 00:04:47,564
… dirigiu-se ao seu mestre!

37
00:04:53,470 --> 00:04:57,240
"Mestre, minha mente não é pacífica. »

38
00:05:08,460 --> 00:05:12,130
"Peço-vos, dai-lhe paz! »

39
00:05:18,940 --> 00:05:25,380
Eka havia matado muitos homens porque tinha estado no exército.

40
00:05:33,130 --> 00:05:37,370
Ele se sentiu extremamente culpado por tudo isso.

41
00:05:40,780 --> 00:05:44,100
Ele não conseguia se livrar da culpa.

42
00:05:49,410 --> 00:05:54,485
Ele está na neve e pergunta a Bodhidharma: "Aceite-me! »

43
00:06:00,245 --> 00:06:02,050
"Como um discípulo! »

44
00:06:06,000 --> 00:06:09,410
Bodhidharma não está respondendo.

45
00:06:17,770 --> 00:06:19,615
Ele insiste.

46
00:06:23,185 --> 00:06:25,790
O Bodhidharma não está se movendo.

47
00:06:30,960 --> 00:06:35,420
No final, ele corta seu braço com sua espada.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma diz: "Bem…"

49
00:06:58,695 --> 00:07:02,350
Grunhidos do Bodhidharma: "Han…"

50
00:07:06,620 --> 00:07:09,980
E ele continua zazen.

51
00:07:14,280 --> 00:07:17,145
Eka insiste, implora:

52
00:07:20,910 --> 00:07:24,630
"Peço-lhes que pacificem minha mente! »

53
00:07:29,190 --> 00:07:31,335
"É insuportável! »

54
00:07:32,305 --> 00:07:34,400
"Estou ficando louco! »

55
00:07:44,290 --> 00:07:48,140
Finalmente, Bodhidharma lhe dá uma olhada:

56
00:07:54,430 --> 00:07:58,595
"Traga-me seu espírito". »

57
00:08:10,745 --> 00:08:13,680
"E eu lhe darei a paz". »

58
00:08:20,300 --> 00:08:22,590
Mestre Eka lhe respondeu:

59
00:08:26,210 --> 00:08:29,105
"Mestre, eu o procurei, meu espírito! »

60
00:08:33,665 --> 00:08:37,750
"E permaneceu inacessível, fugidio! »

61
00:08:46,240 --> 00:08:49,020
Eka está em busca de paz de espírito.

62
00:08:52,630 --> 00:08:55,050
Ele está em sofrimento.

63
00:08:56,520 --> 00:08:59,340
Ele quer paz, sossego.

64
00:09:04,085 --> 00:09:05,490
Não importa quantas vezes lhe digamos:

65
00:09:05,550 --> 00:09:07,845
"É a sua mente! »

66
00:09:13,950 --> 00:09:16,360
"Onde está, minha mente? »

67
00:09:17,805 --> 00:09:19,655
"Onde está ele, meu espírito? »

68
00:09:22,452 --> 00:09:25,182
"Onde está a raiz deste espírito? »

69
00:09:29,400 --> 00:09:32,480
"Qual é a essência deste espírito? »

70
00:09:35,925 --> 00:09:38,528
"Está no cérebro? »

71
00:09:46,670 --> 00:09:48,510
"Está no coração? »

72
00:09:56,655 --> 00:10:00,025
Mesmo que nem todos cortem o braço,

73
00:10:03,270 --> 00:10:07,120
na neve, no frio…

74
00:10:08,810 --> 00:10:12,880
Todo mundo quer encontrar algo em sua mente.

75
00:10:16,040 --> 00:10:18,570
Todos querem ser pacíficos.

76
00:10:21,970 --> 00:10:23,691
Pacifique sua mente.

77
00:10:27,930 --> 00:10:31,515
Eka teve uma resposta notável quando disse:

78
00:10:31,645 --> 00:10:35,920
"Tenho andado à procura dele, mas ele é esquivo! »

79
00:10:43,990 --> 00:10:46,030
"Não consegui encontrá-lo. »

80
00:10:48,615 --> 00:10:53,235
Em japonês, é chamado de "Shin fukatoku".

81
00:11:07,460 --> 00:11:09,885
"Fu" é a negação, "nada".

82
00:11:13,960 --> 00:11:15,870
"Ta": Apreender.

83
00:11:19,270 --> 00:11:21,100
Apreender, encontrar.

84
00:11:24,230 --> 00:11:28,260
"Toku" é como "Mushotoku".

85
00:11:29,550 --> 00:11:33,470
"Toku" significa "obter".

86
00:11:35,910 --> 00:11:37,930
"Alcançando seu objetivo".

87
00:11:40,370 --> 00:11:43,320
"Shin fukatoku",

88
00:11:47,710 --> 00:11:51,310
É a mente completamente indetectável e esquiva.

89
00:11:57,710 --> 00:11:58,955
Eka diz:

90
00:11:59,085 --> 00:12:01,650
"Procurei, mas não consegui encontrá-lo. »

91
00:12:04,480 --> 00:12:06,670
Essa é uma excelente resposta.

92
00:12:09,470 --> 00:12:13,340
Mas o Bodhidharma’s é ainda mais bonito.

93
00:12:18,060 --> 00:12:22,330
"Traga-me seu espírito e eu o pacificarei". »

94
00:12:29,700 --> 00:12:30,660
Eka disse:

95
00:12:30,720 --> 00:12:32,670
"Eu não posso! »

96
00:12:34,760 --> 00:12:37,320
"Não só não posso pegá-lo".

97
00:12:37,400 --> 00:12:40,320
"mas não consigo nem encontrá-lo! »

98
00:12:44,610 --> 00:12:46,270
diz-lhe Bodhidharma:

99
00:12:48,910 --> 00:12:52,340
"Nesse caso, ele já está pacificado! »

100
00:13:07,610 --> 00:13:09,520
Segunda história:

101
00:13:11,955 --> 00:13:14,575
Eu gosto de histórias.

102
00:13:16,890 --> 00:13:18,750
Mestre Obaku.

103
00:13:20,240 --> 00:13:22,330
O mestre de Baso.

104
00:13:25,480 --> 00:13:30,240
Dê seu ensinamento, na frente dos monges.

105
00:13:33,405 --> 00:13:35,605
Alguém chama por ele:

106
00:13:41,550 --> 00:13:46,580
"O mestre deve ter uma grande faculdade de concentração. »

107
00:13:52,200 --> 00:13:53,985
O Mestre Obaku responde:

108
00:13:58,735 --> 00:14:02,110
"Concentração".

109
00:14:03,950 --> 00:14:07,594
"…ou pelo contrário, distração".

110
00:14:10,310 --> 00:14:14,610
"…você está dizendo que essas coisas existem? »

111
00:14:18,300 --> 00:14:20,600
"Eu lhes digo:"

112
00:14:23,770 --> 00:14:27,100
"Procure-os e você não os encontrará em nenhum lugar. »

113
00:14:32,840 --> 00:14:36,340
"Mas se você me disser que eles não existem"…

114
00:14:40,390 --> 00:14:42,490
"Eu lhe direi:"

115
00:14:45,510 --> 00:14:47,390
"o que quer que você pense".

116
00:14:53,490 --> 00:14:58,050
"você está constantemente presente onde você está! »

117
00:15:08,040 --> 00:15:11,650
Mesmo quando se pensa que se está distraído,

118
00:15:16,330 --> 00:15:20,610
olhar para o lugar onde a distração se originou.

119
00:15:24,850 --> 00:15:27,950
Você vai ver que finalmente não tem origem.

120
00:15:33,360 --> 00:15:37,260
O estado mental da distração que aparece…

121
00:15:40,870 --> 00:15:43,490
…não vai a lugar algum.

122
00:15:44,920 --> 00:15:46,695
Em todas as dez direções.

123
00:15:48,305 --> 00:15:50,690
E ele também não vai a nenhum outro lugar.

124
00:16:10,270 --> 00:16:13,490
Em zazen, não há distração ou concentração.

125
00:16:21,040 --> 00:16:23,470
Fique quieto.

126
00:16:30,800 --> 00:16:33,090
O resto são técnicas.

127
00:16:35,820 --> 00:16:40,168
Isso nos afasta do autêntico zazen.

128
00:16:45,129 --> 00:16:46,769
Terceira história.

129
00:16:47,119 --> 00:16:49,719
- Sim, eu te estraguei! -

130
00:17:00,600 --> 00:17:03,750
É da época do Baso na China.

131
00:17:05,760 --> 00:17:18,250
Baso, este é o discípulo do mestre de que eu estava falando, Obaku.

132
00:17:24,410 --> 00:17:28,330
Naquela época, vivia um estudioso

133
00:17:30,950 --> 00:17:34,280
…cujo nome era Ryo.

134
00:17:36,560 --> 00:17:41,090
Ele era famoso por seu conhecimento do budismo.

135
00:17:45,320 --> 00:17:48,840
Ele passou sua vida dando palestras.

136
00:17:53,410 --> 00:17:58,790
Acontece que, um dia, ele teve uma entrevista com Baso.

137
00:18:04,890 --> 00:18:07,160
Baso lhe perguntou:

138
00:18:09,190 --> 00:18:12,770
- Quais são os temas sobre os quais você faz palestra? »

139
00:18:15,850 --> 00:18:20,040
- "No Spirit Sutra, o Hannya Shingyo". »

140
00:18:25,360 --> 00:18:28,220
Também é chamado de Sutra do Coração.

141
00:18:32,420 --> 00:18:36,750
Não está claro se a mente no cérebro ou no coração.

142
00:18:41,320 --> 00:18:47,240
Em "Shingyo", "Shin" significa mente, ou coração.

143
00:18:52,600 --> 00:18:55,460
"Gyô" significa Sutra.

144
00:18:58,090 --> 00:19:00,285
Então Baso lhe pergunta:

145
00:19:03,825 --> 00:19:07,260
- "O que você usa para suas palestras? »

146
00:19:09,950 --> 00:19:11,755
Ryo respondeu:

147
00:19:14,525 --> 00:19:17,527
- "Com o espírito". »

148
00:19:19,530 --> 00:19:21,355
Baso diz a ele:

149
00:19:24,685 --> 00:19:30,210
- "O espírito, ele é como um ator muito bom! »

150
00:19:34,480 --> 00:19:39,390
"O significado de seu pensamento é como a extravagância de um palhaço. »

151
00:19:45,690 --> 00:19:51,580
"Quanto aos seis sentidos que nos fazem senti-los como num filme no cinema"…

152
00:20:07,270 --> 00:20:10,420
"Estes seis sentidos estão vazios! »

153
00:20:12,420 --> 00:20:16,970
"Como uma onda sem início ou fim real. »

154
00:20:21,260 --> 00:20:26,340
"Como a mente poderia dar uma palestra sobre um sutra?"

155
00:20:33,590 --> 00:20:38,200
Ryo respondeu, pensando que era esperto:

156
00:20:42,150 --> 00:20:47,880
- "Se o espírito não pode fazer isso, talvez o não-mente possa? »

157
00:20:51,630 --> 00:20:53,315
Baso diz a ele:

158
00:20:55,055 --> 00:20:58,607
- "Sim, exatamente! »

159
00:21:00,167 --> 00:21:04,150
"O não-espírito pode dar lições totalmente". »

160
00:21:09,050 --> 00:21:13,230
Ryo, acreditando estar satisfeito com sua resposta…

161
00:21:16,350 --> 00:21:19,730
limpou as mangas…

162
00:21:25,070 --> 00:21:28,100
…e se preparou para partir.

163
00:21:31,390 --> 00:21:33,820
O Mestre Baso o chama:

164
00:21:35,920 --> 00:21:38,605
- "Professor! »

165
00:21:39,305 --> 00:21:41,160
Ryo virou sua cabeça.

166
00:21:47,460 --> 00:21:49,115
Baso diz a ele:

167
00:21:49,185 --> 00:21:51,560
- "O que você está fazendo? »

168
00:21:55,630 --> 00:21:58,225
Ryo teve uma profunda revelação.

169
00:22:03,905 --> 00:22:07,490
Ele se deu conta de sua falta de espírito.

170
00:22:10,940 --> 00:22:12,440
E Baso diz a ele:

171
00:22:12,550 --> 00:22:16,000
"Desde o nascimento até a morte, é assim". »

172
00:22:21,750 --> 00:22:24,976
Nós pensamos, nós fazemos coisas,

173
00:22:28,400 --> 00:22:30,670
espontaneamente.

174
00:22:33,320 --> 00:22:36,315
Ryo queria se curvar a Baso,

175
00:22:42,215 --> 00:22:44,840
mas este lhe diz:

176
00:22:46,660 --> 00:22:51,220
"Não há necessidade de fazer pretensões tolas! »

177
00:23:02,150 --> 00:23:07,280
Todo o corpo de Ryo estava coberto de suor.

178
00:23:16,080 --> 00:23:19,360
Ele voltou ao seu templo.

179
00:23:21,260 --> 00:23:24,295
E ele disse a seus discípulos:

180
00:23:27,575 --> 00:23:31,645
"Eu estava pensando que quando eu morresse, nós poderíamos dizer,"

181
00:23:37,670 --> 00:23:41,440
"Que eu tinha dado as melhores palestras…"

182
00:23:44,650 --> 00:23:47,170
"…no Zen. »

183
00:23:52,430 --> 00:23:55,830
"Hoje, fiz uma pergunta a Baso. »

184
00:24:00,250 --> 00:24:03,710
"E ele limpou a névoa da minha vida inteira. »

185
00:24:10,790 --> 00:24:14,400
Ryo desistiu de dar palestras.

186
00:24:18,180 --> 00:24:21,125
E se retirou para o coração das montanhas.

187
00:24:24,475 --> 00:24:28,290
E nunca mais ouvimos falar dele.

188
00:25:15,750 --> 00:25:21,950
Por que a mente é tão difícil de capturar?

189
00:25:24,620 --> 00:25:25,755
Por quê?

190
00:25:35,090 --> 00:25:39,490
Porque o pensamento, a consciência…

191
00:25:44,230 --> 00:25:46,680
…e até a realidade…

192
00:26:00,830 --> 00:26:02,620
…às vezes aparece…

193
00:26:06,380 --> 00:26:08,210
…às vezes desaparece.

194
00:26:10,690 --> 00:26:16,570
Às vezes aparece, às vezes desaparece…

195
00:26:26,840 --> 00:26:30,275
Quando ela desaparece, não sabemos onde ela está.

196
00:26:32,895 --> 00:26:35,532
Mas onde ela está…

197
00:26:37,512 --> 00:26:39,260
…ela está lá.

198
00:26:42,620 --> 00:26:44,475
Ela está lá.

199
00:26:53,205 --> 00:26:56,900
Onde ela não está, ela também está lá.

200
00:27:04,820 --> 00:27:09,140
O tempo e a consciência não são lineares.

201
00:28:08,350 --> 00:28:10,574
Quanto à distração…

202
00:28:19,220 --> 00:28:24,070
Quando eu era criança, eles sempre me disseram
que eu não estava concentrado.

203
00:28:37,820 --> 00:28:39,365
Distraído.

204
00:28:42,805 --> 00:28:45,670
Mas quando eu joguei…

205
00:28:47,257 --> 00:28:50,930
Quando eu brincava com meus carrinhos…

206
00:28:55,110 --> 00:28:58,880
…eu estava completamente concentrado.

207
00:29:06,020 --> 00:29:11,980
O Hannya Shingyo e a cerimônia são dedicados a Jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
E Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra da grande sabedoria que permite ir além

210
00:30:12,312 --> 00:30:17,006
O Bodissatva da Verdadeira Liberdade,

211
00:30:17,196 --> 00:30:21,144
pela prática profunda da Grande Sabedoria,

212
00:30:21,264 --> 00:30:25,022
entende que o corpo e os cinco skandah não são nada além de vacuums.

213
00:30:25,152 --> 00:30:30,151
e através desse entendimento, ele ajuda a todos aqueles que sofrem.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, os fenômenos não são diferentes do ku.

215
00:30:34,970 --> 00:30:39,527
Ku não é diferente de anormais.

216
00:30:39,987 --> 00:30:42,996
Fenômenos tornam-se ku.

217
00:30:43,196 --> 00:30:51,305
Ku se torna um fenômeno (forma é vazio, vazio é forma),

218
00:30:51,575 --> 00:30:56,155
os cinco skanda também são fenômenos.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, toda a existência tem o caráter de ku,

220
00:31:03,451 --> 00:31:08,429
não há nascimento e não há início,

221
00:31:08,989 --> 00:31:14,707
sem pureza, sem mácula, sem crescimento, sem decadência.

222
00:31:14,977 --> 00:31:20,130
É por isso que no ku, não há forma, não há skanda,

223
00:31:20,939 --> 00:31:26,080
sem olhos, sem orelhas, sem nariz, sem língua, sem corpo, sem consciência;

224
00:31:26,491 --> 00:31:32,397
não há cor, som, cheiro, paladar, tato ou pensamento;

225
00:31:32,527 --> 00:31:37,915
não há conhecimento, não há ignorância, não há ilusão, não há cessação de sofrimento,

226
00:31:38,225 --> 00:31:43,283
não há conhecimento, não há lucro, não há fins lucrativos.

227
00:31:43,413 --> 00:31:48,649
Para o Bodissatva, graças a esta Sabedoria que leva além,

228
00:31:48,969 --> 00:31:54,622
não existe tal coisa como medo ou pavor.

229
00:31:54,882 --> 00:32:02,024
Toda ilusão e fixação são removidas

230
00:32:02,504 --> 00:32:11,116
e ele pode aproveitar o fim último da vida, o Nirvana.

231
00:32:12,325 --> 00:32:16,407
Todos os Budas do passado, presente e futuro.

232
00:32:16,497 --> 00:32:21,489
pode alcançar a compreensão desta Sabedoria Suprema...

233
00:32:21,569 --> 00:32:25,021
que proporciona sofrimento, o Satori,

234
00:32:25,081 --> 00:32:30,329
por este encantamento, incomparável e inigualável, autêntico,

235
00:32:30,432 --> 00:32:36,163
que remove todo sofrimento e permite que você encontre a realidade, o verdadeiro ku:

236
00:32:38,113 --> 00:32:52,170
"Vão, vão, vão juntos para além do além no banco do satori". »

237
00:33:01,240 --> 00:33:14,420
Não importa quantos seres haja, prometo salvar a todos eles.

238
00:33:14,600 --> 00:33:23,095
Não importa quantas paixões existam, eu prometo derrotá-las a todas.

239
00:33:25,635 --> 00:33:34,292
Não importa quantos Dharmas existam, eu prometo adquirir todos eles.

240
00:33:35,112 --> 00:33:43,700
Não importa quão perfeito seja um Buda, eu prometo ser um.

241
00:33:44,085 --> 00:33:48,247
Que os méritos desta recitação

242
00:33:48,247 --> 00:33:52,768
penetrem todos os seres em todo o lado,

243
00:33:52,768 --> 00:33:57,538
para que todos nós, seres sencientes,

244
00:33:57,538 --> 00:34:03,129
possamos realizar juntos o caminho de Buda.

245
00:34:05,332 --> 00:34:16,597
Todos os Budas passado, presente e futuro nas dez direções.

246
00:34:17,517 --> 00:34:26,629
Todos os Bodissatvas e patriarcas

247
00:34:27,869 --> 00:34:38,142
O Sutra da "Sabedoria que vai além"

248
00:34:53,546 --> 00:34:58,216
Sanpaï!

249
00:37:25,590 --> 00:37:28,060
Ufa!

250
00:37:46,750 --> 00:37:48,965
Olá a todos!

251
00:37:57,955 --> 00:38:03,310
Espero que você esteja feliz por a pandemia ter acabado!

252
00:38:32,700 --> 00:38:34,600
Hola Paola!
