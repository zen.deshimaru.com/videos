Zen e sofrologia: posturas fundamentais e sua influência sobre o cérebro

O Mestre Kosen foi convidado para o congresso de 2013 da Federação das Escolas Profissionais de Sofrologia (www.sophro.fr).

Ele explica as quatro posturas fundamentais do Zen e sua influência sobre o cérebro. Ele retorna às fontes de relaxamento dinâmico. Ele insiste na importância dos três pilares de postura, respiração e estado de espírito na meditação budista, especialmente a prática do Zen, zazen.
