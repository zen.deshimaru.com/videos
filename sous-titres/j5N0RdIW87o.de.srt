1
00:00:00,000 --> 00:00:05,180
Ich übte mit Schwester Deshimaru in Sesshin- und Sommerlagern. 

2
00:00:05,500 --> 00:00:08,360
Mir wurde gesagt: "Du wirst sehen, er ist ein Meister!" 

3
00:00:08,700 --> 00:00:13,040
Ich wagte es nicht einmal ihn anzusehen, ich war so eingeschüchtert. 

4
00:00:13,540 --> 00:00:16,780
Als ich herausfand, dass er krank war, dachte ich mir: 

5
00:00:17,120 --> 00:00:20,340
"Soll ich Zen fortsetzen, wenn er stirbt?" 

6
00:00:20,620 --> 00:00:26,180
Ich hatte den Eindruck, dass er mir etwas beigebracht hatte, das mir den Mut gab, weiterzumachen. 

7
00:00:26,620 --> 00:00:30,400
Ich heiße Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
Ich hätte vielleicht vorher gedacht, dass ich eine katholische Nonne sein könnte … 

9
00:00:35,400 --> 00:00:39,320
Aber wie gut ist es, im Berufsleben eine Zen-Nonne zu sein! 

10
00:00:39,620 --> 00:00:43,200
Ich folgte weiterhin dem Dojo von Lyon mit André Meissner. 

11
00:00:43,440 --> 00:00:49,000
Indem ich Meissner folgte, der später Stéphane folgte, folgte ich natürlich Meister Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen ist ein enger Schüler von Meister Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
Ich wurde 1980 von Meister Deshimaru zum Bodhisattva geweiht. 

14
00:00:59,680 --> 00:01:03,560
Der Bodhisattva ist einer, der allen Wesen helfen will. 

15
00:01:03,780 --> 00:01:07,960
Ordination ist die Erfüllung dieses Gelübdes. 

16
00:01:08,200 --> 00:01:10,560
Ich habe nie versucht, Zen zu üben. 

17
00:01:10,860 --> 00:01:12,460
Ich wusste nicht was es war. 

18
00:01:12,740 --> 00:01:15,180
Auf einer Konferenz in Lyon gab es ein Plakat. 

19
00:01:15,400 --> 00:01:18,180
Es war eine Konferenz mit Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
Er sagt, du würdest etwas vermissen, wenn du nicht gehen würdest. 

21
00:01:26,320 --> 00:01:33,860
Während seines Vortrags waren Leute in Zazen, aber ich war nicht einmal beeindruckt von der Einstellung. 

22
00:01:34,280 --> 00:01:37,780
Es war ein intellektueller Schachzug von meiner Seite. 

23
00:01:38,020 --> 00:01:42,220
Als ich anfing zu trainieren, war es eine Möglichkeit, die Zeit anzuhalten. 

24
00:01:42,500 --> 00:01:44,640
Es geht alles so schnell! 

25
00:01:44,940 --> 00:01:50,140
Ich dachte auch, dass es alle Probleme auf der Basis lösen könnte. 

26
00:01:50,460 --> 00:01:55,260
Es löste ein Problem, weil ich mich aus der Karriere meines Mathematiklehrers ausgeschlossen fühlte. 

27
00:01:55,540 --> 00:02:00,280
Ich mochte den ersten Satz des Gedichts Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"Die große Straße ist nicht schwierig, vermeiden Sie einfach eine Wahl." 

29
00:02:05,860 --> 00:02:11,100
Ich sagte mir: "Ja, ich bin sehr verfügbar für das, was passiert!" 

30
00:02:11,420 --> 00:02:16,560
Während ich übte, wurde mir klar, wie sehr ich mich immer noch in einem Zustand der Wahl und Ablehnung befand. 

31
00:02:16,940 --> 00:02:23,900
Das Üben von Zen hilft uns auch, unsere Dunkelheit zu sehen. 

32
00:02:25,100 --> 00:02:32,740
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

