1
00:00:00,000 --> 00:00:03,380
È inutile andare in Giappone per praticare lo Zen. 

2
00:00:03,640 --> 00:00:07,360
Quando le persone mi chiedono informazioni a Digione, dico loro che: 

3
00:00:07,620 --> 00:00:09,240
"Non parleremo di Zen." 

4
00:00:09,500 --> 00:00:10,180
"Vieni nel dojo." 

5
00:00:10,440 --> 00:00:14,040
"Solo allora sentirai se ti va bene o no." 

6
00:00:14,360 --> 00:00:18,100
Dalla prima volta che ti alleni, puoi sentire cosa ti porta. 

7
00:00:18,680 --> 00:00:21,800
Se ne hai voglia, è segno che stai bene. 

8
00:00:22,000 --> 00:00:26,480
Non sono necessarie spiegazioni teoriche per sapere perché pratichiamo. 

9
00:00:26,780 --> 00:00:35,280
Quando ci sistemiamo a Zazen, immediatamente … senti che la postura ha qualcosa che dona riposo, che si calma. 

10
00:00:35,580 --> 00:00:40,660
L’atteggiamento fisico è davvero importante per sentirlo circolare. 

11
00:00:40,940 --> 00:00:46,960
Penso che sia principalmente legato a questo equilibrio, che ci consente di arrenderci. 

12
00:00:47,240 --> 00:00:49,280
Zazen è anche un’esperienza di silenzio. 

13
00:00:49,620 --> 00:00:59,800
Quando siamo in silenzio insieme, quando ascolti quel silenzio, viene trasmesso qualcosa di profondo. 

14
00:01:00,140 --> 00:01:03,140
Sto pensando a un’infermiera che viene completamente stressata: 

15
00:01:03,340 --> 00:01:05,840
"Non riesco a calmare la mente!" 

16
00:01:06,120 --> 00:01:08,840
È uscita completamente calma. 

17
00:01:09,120 --> 00:01:14,620
Se possiamo rilevare gradualmente la respirazione profonda, 

18
00:01:15,140 --> 00:01:22,300
cancella tutti quei pensieri fastidiosi, tutte le complicazioni. 

19
00:01:22,720 --> 00:01:29,720
È persino difficile dire che sono buddista perché suona come una chiesa. 

20
00:01:30,280 --> 00:01:33,720
Se non ti serve questo per praticare lo Zen … 

21
00:01:34,020 --> 00:01:38,800
È un peccato in questi giorni, le persone vogliono la meditazione secolare. 

22
00:01:39,220 --> 00:01:42,800
a Digione ci sono molti tipi di meditazioni. 

23
00:01:43,140 --> 00:01:47,360
Non li conosco, non posso parlarne. 

24
00:01:47,560 --> 00:01:52,940
Ma personalmente mi piace esercitarmi come è fatto. 

25
00:01:53,180 --> 00:01:54,340
Con alcune semplici regole: 

26
00:01:54,700 --> 00:01:56,780
Saluta quando torniamo a casa. 

27
00:01:57,080 --> 00:01:58,880
Saluta le altre persone con cui fai pratica. 

28
00:01:59,120 --> 00:02:00,620
Facciamo la nostra meditazione. 

29
00:02:00,880 --> 00:02:06,040
Quindi recitiamo un sutra praticamente per l’intero universo. 

30
00:02:06,300 --> 00:02:08,200
È una questione completa. 

31
00:02:08,520 --> 00:02:12,480
È un modo di esercitare non solo per te stesso. 

32
00:02:13,500 --> 00:02:17,180
Non ascoltiamo sempre il nostro conforto. 

33
00:02:17,460 --> 00:02:24,300
Mentre le persone che vogliono una qualche forma di meditazione, stanno già facendo una scelta. 

34
00:02:24,560 --> 00:02:29,720
Mi piace questa nozione di "inutilità", in questo momento in cui tutto tende a trarre profitto. 

35
00:02:30,000 --> 00:02:32,180
Siamo nello zazen, e basta. 

36
00:02:32,420 --> 00:02:35,660
Ho ricevuto il messaggio dal Maestro Kosen. 

37
00:02:35,960 --> 00:02:38,180
Mi sento onorato e intimidito. 

38
00:02:38,520 --> 00:02:42,800
Le persone immaginano che essere un maestro debba essere pienamente realizzato. 

39
00:02:43,080 --> 00:02:45,180
Non mi sento così. 

40
00:02:45,440 --> 00:02:50,720
Vivo questo trasferimento come il desiderio di continuare a trasferire il Sangha e di aiutarlo a crescere. 

41
00:02:51,040 --> 00:02:56,300
I nostri Sangha sono tutte persone che praticano con il Maestro Kosen. 

42
00:02:56,980 --> 00:03:04,360
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

