1
00:00:00,280 --> 00:00:02,560
Я мало что знаю о софрологии.

2
00:00:02,800 --> 00:00:12,140
Я знаю, что это применение определенных практик оздоровления, развития личности,

3
00:00:12,540 --> 00:00:17,160
и есть софрология, которая называется "востоковед",

4
00:00:17,440 --> 00:00:24,080
который очень вдохновлен йогическими, буддийскими или индийскими практиками.

5
00:00:24,540 --> 00:00:27,940
Дзен очень прост.

6
00:00:28,380 --> 00:00:38,400
Это основано на фундаментальных человеческих позах.

7
00:00:39,820 --> 00:00:48,480
Я покажу тебе первую фундаментальную позу Дзен.

8
00:01:01,500 --> 00:01:06,120
Раньше люди были обезьянами.

9
00:01:06,460 --> 00:01:08,580
Вот как они ходили.

10
00:01:09,200 --> 00:01:20,360
Мало-помалу они сидели прямо, как дзен-мастера.

11
00:01:22,900 --> 00:01:24,900
Потом они выпрямились.

12
00:01:25,180 --> 00:01:28,600
Вот как они ходили.

13
00:01:29,680 --> 00:01:33,760
И человеческое сознание эволюционировало

14
00:01:35,180 --> 00:01:39,980
вместе с осанкой.

15
00:01:40,340 --> 00:01:46,000
Поза тела - это фундаментальная мысль.

16
00:01:46,340 --> 00:01:52,200
Например, Усэйн Болт, самый быстрый бегун в мире,

17
00:01:52,540 --> 00:01:55,160
сделал этот жест.

18
00:01:55,440 --> 00:02:02,300
Все понимают, что это значит, но нет слов.

19
00:02:02,640 --> 00:02:05,440
Это просто поза.

20
00:02:05,900 --> 00:02:11,120
То, как мы сидим и ходим,

21
00:02:11,520 --> 00:02:13,400
что ты делаешь руками,

22
00:02:13,700 --> 00:02:16,500
все это принципиально важно.

23
00:02:18,160 --> 00:02:21,440
В Дзен есть поза Будды.

24
00:02:22,980 --> 00:02:31,120
Он существовал до Будды, упоминается в Бхагавад-Гите.

25
00:02:32,400 --> 00:02:39,880
Ты сидишь на травяной подушке и сгибаешь ноги.

26
00:02:41,560 --> 00:02:47,140
В дикой природе доисторический человек общался...

27
00:02:47,440 --> 00:02:53,220
с ушами, с носом, с глазами, чтобы видеть далеко.

28
00:02:54,920 --> 00:02:59,000
Так что они хотели выпрямиться.

29
00:02:59,400 --> 00:03:04,220
И когда они выпрямились, их сознание заострилось, пробудилось.

30
00:03:06,040 --> 00:03:08,720
Это первая поза.

31
00:03:08,980 --> 00:03:11,580
Обычно мы должны сделать полный лотос.

32
00:03:12,060 --> 00:03:14,260
Вот как ты скрещиваешь ноги.

33
00:03:14,700 --> 00:03:18,460
Это называется "крендель".

34
00:03:24,960 --> 00:03:30,980
Очень важно занять такую позу.

35
00:03:31,380 --> 00:03:37,380
Предыдущий оратор говорил о динамизме и отдыхе.

36
00:03:37,780 --> 00:03:42,340
У нас симпатическая и парасимпатическая нервная система.

37
00:03:42,600 --> 00:03:45,800
У нас есть внешние и глубокие мышцы.

38
00:03:46,320 --> 00:03:51,400
У нас поверхностное и глубокое дыхание.

39
00:03:51,820 --> 00:03:54,620
У нас поверхностное, кортикальное сознание,

40
00:03:54,920 --> 00:03:59,760
и глубокое сознание, рептилия или средний мозг.

41
00:04:00,340 --> 00:04:04,160
Чтобы стоять прямо в этой позе,

42
00:04:04,440 --> 00:04:06,800
ты должен толкать землю коленями.

43
00:04:10,240 --> 00:04:15,640
Нужно стараться быть динамичным и иметь хорошую осанку.

44
00:04:16,000 --> 00:04:22,180
Мой господин [Дешимару] настаивал на красоте и силе осанки.

45
00:04:22,460 --> 00:04:31,280
Но в лотосе, чем больше ты расслабляешься, тем больше растут ноги на бедрах,

46
00:04:31,520 --> 00:04:35,100
и чем больше колени растут на земле.

47
00:04:35,620 --> 00:04:40,280
Чем больше вы расслабляетесь, тем динамичнее вы становитесь, тем вы прямее и сильнее.

48
00:04:40,560 --> 00:04:46,560
Это не имеет ничего общего с отдыхом в кресле или кровати.

49
00:04:46,940 --> 00:04:57,280
Это способ оставить тело самому себе,

50
00:05:04,380 --> 00:05:09,700
пока не проснулся.

51
00:05:10,120 --> 00:05:16,240
Это первая поза, сидячее положение Будды.

52
00:05:16,680 --> 00:05:20,280
Руки также очень важны.

53
00:05:20,640 --> 00:05:23,600
Ты всегда что-то делаешь руками:

54
00:05:23,860 --> 00:05:26,560
работая, защищая себя...

55
00:05:26,960 --> 00:05:28,740
Руки очень выразительные

56
00:05:29,040 --> 00:05:31,040
и связаны с мозгом.

57
00:05:31,500 --> 00:05:41,040
Принятие этой мудры руками меняет сознание.

58
00:05:41,680 --> 00:05:47,680
Концентрация в наших руках, теле, нервной системе и мозге.

59
00:05:48,100 --> 00:05:53,360
Другая важная вещь - это мысль, сознание.

60
00:05:53,820 --> 00:05:57,680
Если ты как Мыслитель Родена,

61
00:05:58,000 --> 00:06:01,100
у тебя совсем нет совести.

62
00:06:09,960 --> 00:06:12,240
что когда ваша осанка абсолютно ровная,

63
00:06:12,580 --> 00:06:14,640
особенно с вытянутой шеей,

64
00:06:15,700 --> 00:06:18,400
с поднятой головой, открытой к небу,

65
00:06:18,760 --> 00:06:22,020
выравнивание носа и пупка

66
00:06:25,060 --> 00:06:27,220
и ты дышишь всем своим существом.

67
00:06:29,020 --> 00:06:31,860
Мысль больше не отделяется от тела.

68
00:06:32,240 --> 00:06:36,100
Больше нет разобщенности между нашим сознанием.

69
00:06:36,300 --> 00:06:39,080
и каждую клетку в нашем теле.

70
00:06:39,500 --> 00:06:45,520
В Дзен мы называем это "мозговое тело", одним словом.

71
00:06:46,360 --> 00:06:50,540
Это фундаментальная поза: Зазен.

72
00:06:56,380 --> 00:07:00,200
Вторая поза - ходячая поза.

73
00:07:00,680 --> 00:07:06,340
В Зазене мы бросаем все и вообще не двигаемся.

74
00:07:06,560 --> 00:07:08,280
Значит, сознание - это особенное.

75
00:07:08,720 --> 00:07:13,800
Но есть еще одна медитация, которая делается во время ходьбы.

76
00:07:16,180 --> 00:07:18,160
Мы должны двигаться.

77
00:07:18,540 --> 00:07:20,540
Мы делаем что-то, мы двигаемся дальше.

78
00:07:21,180 --> 00:07:24,180
Ты не идешь вперед, чтобы поймать что-нибудь.

79
00:07:24,580 --> 00:07:28,380
Ты ускоряешь дыхание ходьбой.

80
00:07:29,020 --> 00:07:30,740
Хореограф Морис Бежар,

81
00:07:31,040 --> 00:07:33,560
который был учеником моего господина, Дезимару,

82
00:07:34,060 --> 00:07:38,040
заставил своих танцоров репетировать это каждый день.

83
00:07:39,060 --> 00:07:45,340
Вторая поза - ходьба "родня".

84
00:07:45,880 --> 00:07:51,040
Заметь, что ты делаешь руками.

85
00:07:51,680 --> 00:07:57,840
Боксер, который фотографируется, делает это руками.

86
00:07:58,240 --> 00:08:05,220
Когда вы кладете руки вот так, мозг автоматически выражает сильную агрессивность.

87
00:08:05,820 --> 00:08:10,360
Можешь положить руки в карманы.

88
00:08:10,840 --> 00:08:13,140
"Куда я положил свой бумажник? »

89
00:08:13,640 --> 00:08:16,360
"Кто-то украл мой мобильник! »

90
00:08:16,700 --> 00:08:19,580
Мы можем засунуть пальцы себе в нос.

91
00:08:21,660 --> 00:08:25,820
Все, что здесь было построено, было построено вручную.

92
00:08:26,260 --> 00:08:28,820
Это необычно, рука.

93
00:08:29,160 --> 00:08:31,860
Мы единственные животные с руками,

94
00:08:32,260 --> 00:08:35,340
они связаны с нашей эволюцией.

95
00:08:37,700 --> 00:08:40,660
Руки также медитируют.

96
00:08:40,940 --> 00:08:43,580
Ты засунул большой палец в кулак.

97
00:08:43,820 --> 00:08:49,420
Положите корень большого пальца под грудную кость, положите другую руку на него.

98
00:08:49,760 --> 00:08:55,060
Эти постуральные детали существовали еще со времен Будды.

99
00:08:55,380 --> 00:09:02,520
Древние тексты объясняют, что они были переданы в Китай из Индии.

100
00:09:02,960 --> 00:09:08,920
Это оральная передача от человека к человеку.

101
00:09:09,480 --> 00:09:12,500
В буддизме есть много аспектов.

102
00:09:12,780 --> 00:09:17,320
Но медитативный и постуральный аспект очень важен.

103
00:09:17,820 --> 00:09:28,540
Были два великих ученика Будды - Сарипутра и Мокурен (Мудгалаяна).

104
00:09:28,940 --> 00:09:36,840
Их духовный наставник не был уверен:

105
00:09:37,220 --> 00:09:43,680
"Я не уверен, что у меня есть трюк, чтобы выбраться из жизни и смерти. »

106
00:09:43,940 --> 00:09:52,860
"Если найдешь мастера, который научит этому, дай мне знать, и я пойду с тобой". »

107
00:09:53,620 --> 00:10:04,320
Однажды они видят монаха, как и я, идущего по дороге.

108
00:10:04,780 --> 00:10:10,000
Они поражены красотой этого человека.

109
00:10:10,420 --> 00:10:16,200
Потом этот монах останавливается в роще, чтобы пописать.

110
00:10:24,440 --> 00:10:33,240
Ему рассказывает Сарипутра, которая считается одной из величайших интеллигентностей Индии того времени:

111
00:10:34,180 --> 00:10:43,940
"Мы поражены твоим присутствием, кто твой хозяин? »

112
00:10:46,560 --> 00:10:49,540
- "Мой хозяин - Будда Шакьямуни".

113
00:10:49,860 --> 00:10:53,520
- "Объясни нам свою доктрину! »

114
00:10:53,780 --> 00:10:58,220
- "Я не могу объяснить тебе, я слишком новичок в этом. »

115
00:10:59,100 --> 00:11:04,440
И они последовали за этим мальчиком только из-за того, как он ходит.

116
00:11:04,800 --> 00:11:06,980
То, как он что-то из нее извлекает.

117
00:11:07,440 --> 00:11:11,680
Значит, вторая поза - ходьба.

118
00:11:12,020 --> 00:11:15,280
Когда люди делают дзазен, он очень сильный.

119
00:11:15,500 --> 00:11:21,580
Даже боевики впечатлены властью, которую это дает.

120
00:11:21,940 --> 00:11:25,860
Силой дыхания.

121
00:11:26,240 --> 00:11:35,520
Но как найти эту энергию, эту полноту, в повседневной жизни?

122
00:11:35,840 --> 00:11:39,220
Это спокойствие, эта концентрация.

123
00:11:39,600 --> 00:11:42,960
Это трудный момент.

124
00:11:43,280 --> 00:11:47,140
Потому что поза Зазена чрезвычайно точна.

125
00:11:47,600 --> 00:11:55,100
В начале мы концентрируемся на технических аспектах ходьбы "родня".

126
00:11:55,540 --> 00:11:58,800
Но как только мы получим все детали,

127
00:11:59,300 --> 00:12:02,160
в любое время в нашей повседневной жизни

128
00:12:02,440 --> 00:12:08,400
(даже если я всегда забываю это делать), мы можем потренироваться.

129
00:12:10,600 --> 00:12:16,740
Ты можешь переориентироваться на своё духовное существо.

130
00:12:17,740 --> 00:12:22,760
О твоем существовании, которое ни хрена не делает.

131
00:12:23,320 --> 00:12:31,340
Я говорил тебе о руках.

132
00:12:31,840 --> 00:12:39,980
Фундаментальная религиозная позиция - вот эта.

133
00:12:40,560 --> 00:12:44,240
Когда ты так держишь руки вместе,

134
00:12:45,080 --> 00:12:48,260
просто делая это,

135
00:12:48,580 --> 00:12:51,280
это влияет на мозг,

136
00:12:51,620 --> 00:12:55,340
он строит мост между двумя полушариями головного мозга.

137
00:12:55,740 --> 00:12:59,960
Эта поза используется во всех религиях.

138
00:13:00,280 --> 00:13:02,180
Это довольно просто.

139
00:13:02,560 --> 00:13:05,640
Видишь, как важны руки!

140
00:13:06,060 --> 00:13:13,280
Третья поза...

141
00:13:13,860 --> 00:13:16,520
(Я думал, их было четверо...)

142
00:13:16,880 --> 00:13:20,580
О, да, есть склонность к осанке, но мы не будем об этом говорить.

143
00:13:21,000 --> 00:13:25,520
Лежащее положение, как спать.

144
00:13:26,700 --> 00:13:31,980
Я объяснил тебе, как доисторические мужчины ползали на четвереньках,

145
00:13:32,260 --> 00:13:34,440
понемногу, они выпрямились,

146
00:13:34,700 --> 00:13:38,760
и что наша эволюция исходит из нашей позы,

147
00:13:39,060 --> 00:13:42,580
орошения позвоночника, идущего к мозгу,

148
00:13:42,940 --> 00:13:46,400
и мозг, который является экстраординарным инструментом,

149
00:13:46,700 --> 00:13:51,440
из которых известно только 10-15% от его общей полезности.

150
00:13:51,760 --> 00:13:57,740
У нас между ушами большая штука, которую мы недоиспользуют.

151
00:14:08,640 --> 00:14:12,380
У каждого свой мозг!

152
00:14:19,020 --> 00:14:22,140
У каждого свой мозг!

153
00:14:22,400 --> 00:14:25,200
Как IP-адрес.

154
00:14:30,220 --> 00:14:33,220
У каждого свой мозг!

155
00:14:33,940 --> 00:14:35,900
И мы одни!

156
00:14:36,700 --> 00:14:41,940
Это очень важно для того, что я собираюсь тебе сказать.

157
00:14:44,760 --> 00:14:50,040
Наши ноги (мы возвращаемся к нашим ногам...).

158
00:14:51,720 --> 00:14:56,420
Ты знаешь, что мы живем на маленькой Земле.

159
00:14:57,040 --> 00:15:01,420
Ты видел фильм "Гравитация"?

160
00:15:02,120 --> 00:15:07,060
Это один из фильмов, которые действительно пробуждают человеческое сознание.

161
00:15:07,560 --> 00:15:13,380
В Голливуде они иногда занимаются манипуляциями.

162
00:15:13,680 --> 00:15:17,220
Но также иногда информации и духовной эволюции.

163
00:15:17,760 --> 00:15:19,660
Этот фильм один из них.

164
00:15:20,040 --> 00:15:26,920
И вы сами видите, насколько хрупка наша Земля.

165
00:15:29,200 --> 00:15:33,400
Обычно, когда мы смотрим на небо, мы бредим: "Оно бесконечно голубое! ».

166
00:15:33,760 --> 00:15:38,940
Нет. Не навсегда. 30 километров атмосферы.

167
00:15:39,360 --> 00:15:44,660
Окончание атмосферы находится между 30 и 1000 километров.

168
00:15:52,260 --> 00:15:56,700
Даже тысяча миль - это ерунда.

169
00:15:58,800 --> 00:16:04,780
Если я посажу тебя за 20 миль, ты не выживешь и минуты.

170
00:16:05,380 --> 00:16:15,940
Даже за исключением налогов, наши условия жизни опасны.

171
00:16:17,420 --> 00:16:20,440
Земля - маленькое, хрупкое, живое существо.

172
00:16:21,120 --> 00:16:30,660
Наша биосфера крошечная и хрупкая, она должна быть сохранена.

173
00:16:30,940 --> 00:16:35,100
Мы всегда должны заботиться о сохранении этой штуки.

174
00:16:36,860 --> 00:16:39,940
Потому что мы родились на Земле.

175
00:16:40,220 --> 00:16:45,700
Это чудо жизни, которое произошло на Земле, эфемерно.

176
00:16:46,240 --> 00:16:47,840
Мы родились на Земле.

177
00:16:48,220 --> 00:16:53,320
И у всех нас ноги указывают на один и тот же центр, центр Земли.

178
00:16:53,900 --> 00:16:57,760
Так что наши ноги в обществе.

179
00:17:01,860 --> 00:17:08,680
Но наша голова уникальна, никто не указывает в одном направлении.

180
00:17:18,400 --> 00:17:23,500
Отсюда и третья поза, которую я тебе покажу.

181
00:17:23,800 --> 00:17:28,140
Это общее для многих религий, это называется "прострация".

182
00:17:28,380 --> 00:17:31,240
Вот как мы это делаем.

183
00:17:40,640 --> 00:17:42,780
Ах, как приятно!

184
00:17:43,060 --> 00:17:49,940
Когда мы делаем эту практику, это как когда мы занимаемся любовью:

185
00:17:50,260 --> 00:17:58,460
иногда мы не занимаемся сексом 10 лет, 20 лет, 6 месяцев...

186
00:17:58,780 --> 00:18:07,460
и когда мы снова занимаемся любовью, мы говорим: "Ах, как приятно, я забыл, как приятно! ».

187
00:18:08,220 --> 00:18:12,200
Когда ты делаешь прострацию, это одно и то же.

188
00:18:13,460 --> 00:18:15,740
Я не шучу.

189
00:18:16,240 --> 00:18:20,260
Мы реформируем наш мозг.

190
00:18:21,060 --> 00:18:26,280
Когда ты ставишь свой мозг в соответствие с центром Земли.

191
00:18:26,620 --> 00:18:29,080
Дело не только в смирении,

192
00:18:29,360 --> 00:18:32,720
или преклониться перед статуями или чем угодно.

193
00:18:34,600 --> 00:18:38,880
Это волшебная, фундаментальная, шаманская штука, может быть...

194
00:18:39,320 --> 00:18:41,860
С того момента, как ты раскладываешься...

195
00:18:42,180 --> 00:18:43,340
Мой хозяин говорил:

196
00:18:43,660 --> 00:18:47,440
"Если бы главы государств раскладывались один раз в день."

197
00:18:47,800 --> 00:18:50,200
"это изменит мир. »

198
00:18:50,640 --> 00:18:54,560
Это не болтовня или мистика.

199
00:18:55,280 --> 00:18:58,780
Это должно быть научно измеримо.

200
00:19:00,780 --> 00:19:05,280
Он переформатирует мозг, чтобы прикоснуться.

201
00:19:05,740 --> 00:19:09,280
Он постоянно в своей индивидуальной линии.

202
00:19:09,620 --> 00:19:17,300
И там ты соединяешься с твоим корнем, с братьями, с центром Земли.

203
00:19:19,940 --> 00:19:25,380
Иногда я делаю это и чувствую что-то сильное в своей голове.

204
00:19:25,660 --> 00:19:27,540
Это так приятно.

205
00:19:27,820 --> 00:19:39,420
В то же время он релятивизирует зло, которое мы непреднамеренно совершаем вокруг нас.

206
00:19:40,560 --> 00:19:42,960
Это хорошее лекарство.

207
00:19:43,180 --> 00:19:47,040
Все эти позы - фундаментальные лекарства.

208
00:19:47,760 --> 00:19:53,260
Я подготовил немного интеллектуальный текст.

209
00:19:53,520 --> 00:19:58,140
Но я, кажется, не могу перейти от одного предмета к другому.

210
00:19:58,440 --> 00:20:02,300
О чем я говорил?

211
00:20:06,860 --> 00:20:12,040
Я возвращаюсь к телу и предыстории.

212
00:20:12,780 --> 00:20:28,200
Было доказано, что у неандертальцев была религиозная практика.

213
00:20:30,680 --> 00:20:36,340
Мы нашли несколько могил.

214
00:20:36,860 --> 00:20:40,440
Они принесли подношения своим мертвым.

215
00:20:40,760 --> 00:20:43,680
Они проводили религиозную церемонию для своих умерших.

216
00:20:48,420 --> 00:20:52,180
Интересно не только это!

217
00:20:52,720 --> 00:20:56,260
В те времена, когда они проводили эти религиозные церемонии -

218
00:20:56,740 --> 00:21:01,700
- религиозными в том смысле, что они общались с невидимым -

219
00:21:02,540 --> 00:21:06,940
- с вечной жизнью, с мертвыми -

220
00:21:08,800 --> 00:21:11,020
- они соединялись с невидимым.

221
00:21:11,420 --> 00:21:18,140
В то время, однако, артикулированного языка еще не существовало.

222
00:21:18,560 --> 00:21:24,380
Итак, религия существовала до артикулированного языка.

223
00:21:24,840 --> 00:21:30,140
(Я готов перейти к артикулированному языку...)

224
00:21:31,480 --> 00:21:36,500
(Это очень интересно, мне понадобилось четыре дня, чтобы написать это...)

225
00:21:43,820 --> 00:21:45,660
Мой хозяин говорил:

226
00:21:45,920 --> 00:21:48,880
"Зазен" - это религия перед религией. »

227
00:21:49,380 --> 00:21:54,380
Мы бы сказали: "Да, ты всегда хочешь быть первым..."

228
00:21:55,860 --> 00:21:59,700
Но это религия перед религией, как мы ее знаем.

229
00:21:59,960 --> 00:22:02,720
Там, где есть ссылки на книги.

230
00:22:03,080 --> 00:22:06,280
Книги приходят после религии, а не раньше.

231
00:22:06,720 --> 00:22:11,940
Раньше он даже не мог говорить.

232
00:22:12,360 --> 00:22:15,060
Значит, он уже был в таком положении.

233
00:22:15,480 --> 00:22:19,620
Это доисторические позы.

234
00:22:21,460 --> 00:22:23,740
(Сколько у меня осталось времени?)


235
00:22:23,960 --> 00:22:28,320
(У меня осталось всего 3 минуты? Ну ладно!)

236
00:22:31,020 --> 00:22:34,460
Итак, насчет софрологии...

237
00:22:36,500 --> 00:22:39,280
Я хотел поговорить с тобой о традиционном дзазене.

238
00:22:39,740 --> 00:22:43,660
Я одет как традиционный монах.

239
00:22:47,440 --> 00:22:51,700
Традиционный дзазен практикуется не в одиночку.

240
00:22:52,100 --> 00:22:54,500
Это не оздоровительная техника.

241
00:22:58,260 --> 00:23:05,060
Я прочитаю тебе, что сказал Будда, я думаю, это так здорово!

242
00:23:08,520 --> 00:23:12,780
Будда Шакьямуни сказал своим слушателям:

243
00:23:13,120 --> 00:23:18,540
"Изучение сутр и учения" - тексты -

244
00:23:18,900 --> 00:23:28,260
соблюдение наставлений" - в религиях это важно, не убивать, не воровать и т.д. - это очень важная часть "соблюдения наставлений". -

245
00:23:29,300 --> 00:23:34,960
"и даже практика дзазен, сидячая медитация."

246
00:23:35,900 --> 00:23:43,060
"неспособны погасить человеческие желания, страхи и тревоги". »

247
00:23:43,340 --> 00:23:45,080
Так сказал Будда Шакьямуни.

248
00:23:45,340 --> 00:23:51,660
Так что я фанатик, а он заноза в заднице...

249
00:23:52,080 --> 00:23:56,800
Зазен, даже Будда говорит, что это бесполезно!

250
00:23:57,600 --> 00:24:01,680
И что мы будем с этим делать?

251
00:24:14,440 --> 00:24:17,480
Конечно, есть люкс...

252
00:24:23,360 --> 00:24:29,360
Мой хозяин [Дешимару], когда встретил своего хозяина [Кодо Саваки],

253
00:24:29,820 --> 00:24:34,280
Последний на конференции сказал: "Зазен бесполезен, он не приносит никакой пользы! ».

254
00:24:34,680 --> 00:24:38,800
Мой хозяин подумал: "Но это действительно интересно, если это бесполезно! »

255
00:24:39,140 --> 00:24:41,940
"Мы всегда делаем то, что хорошо для чего-то! »

256
00:24:42,280 --> 00:24:43,760
"Что-то бесполезное? »

257
00:24:44,120 --> 00:24:47,400
"Я хочу уйти, я хочу сделать это! »
