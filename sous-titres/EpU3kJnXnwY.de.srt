1
00:00:12,840 --> 00:00:20,310
In einem Gedicht von Yoka Daishi,
ein chinesischer Meister, der schrieb

2
00:00:20,310 --> 00:00:25,100
Das Lied des unmittelbaren Satori,

3
00:00:25,100 --> 00:00:27,100
der Shôdôka

4
00:00:27,800 --> 00:00:35,760
und war ein Schüler von Meister Eno, 
dessen Übertragung er empfangen hat

5
00:00:35,760 --> 00:00:37,210
in einer Sitzung, die einen Tag und eine Nacht dauerte.

6
00:00:37,210 --> 00:00:43,100
Danach setzte er seine Lehrtätigkeit fort.

7
00:00:44,040 --> 00:00:50,040
Sein Gedicht: "Verzichte auf die vier Elemente, suche nicht länger zu horten,

8
00:00:50,740 --> 00:00:57,880
in Frieden und absoluter Vollendung,

9
00:00:58,500 --> 00:01:01,940
Trinken und essen Sie, wie Sie wollen.

10
00:01:02,360 --> 00:01:08,660
Alle Phänomene sind vergänglich, alles ist KU, ohne Noumenon, ohne eigene Substanz...

11
00:01:09,080 --> 00:01:14,080
und das ist einfach das große
und Buddha satori vervollständigen".

12
00:01:14,560 --> 00:01:17,820
Meister Deshimaru sagt, dass sich das Gedicht hier mit

13
00:01:18,260 --> 00:01:23,620
der Drei Schätze des Buddhismus der Verzicht auf die vier Elemente,

14
00:01:23,900 --> 00:01:30,300
die vier Elemente
Es ist Wasser, Erde, Feuer, Luft.

15
00:01:30,880 --> 00:01:35,440
Der zweite Schatz ist der Frieden und die Ruhe und die
Ruhe von Zazen, Nirvana,

16
00:01:36,060 --> 00:01:44,400
die dritte die Vergänglichkeit der Phänomene, das Bewusstsein, die Abwesenheit des Noumenons der Existenzen.

17
00:01:44,740 --> 00:01:50,120
Lassen Sie in der ersten Schatzkammer die vier Elemente fallen

18
00:01:50,120 --> 00:01:56,720
und nicht mehr versucht, sie zu fangen, festzuhalten, zu ergreifen.

19
00:01:56,720 --> 00:02:04,300
Meister Deshimaru sagt, dass wir weder das Noumenon noch das Ego suchen dürfen.

20
00:02:04,300 --> 00:02:09,980
Jeder sucht nach dem Ego seines Körpers.
er will seinen Körper besitzen

21
00:02:10,700 --> 00:02:15,580
und daraus werden Gefühle wie Eifersucht, Egoismus, Neid geboren.

22
00:02:15,580 --> 00:02:21,940
also in den ersten Schatz,
lernt man aufzugeben, loszulassen,

23
00:02:21,940 --> 00:02:28,150
seinen Körper, sein Ego, seinen Geist.
Es ist interessant, weil man auch sehen kann

24
00:02:28,150 --> 00:02:33,580
dass diese vier Elemente, die Verzicht auf die
vier Elemente und versuchen nicht mehr

25
00:02:33,580 --> 00:02:39,560
sie zu fangen, sie zu beschlagnahmen
in Verbindung mit unserer Welt.

26
00:02:39,560 --> 00:02:44,000
Wasser, Erde, Feuer und Luft.

27
00:02:44,000 --> 00:02:48,840
In den Lehren
der Zen-Meister gibt es immer

28
00:02:48,850 --> 00:02:53,560
eine tiefe Intuition, und das ist
interessant, denn diese Intuition

29
00:02:53,560 --> 00:03:00,370
es verbindet sich mit der modernen Wissenschaft, der
Biowissenschaften

30
00:03:00,370 --> 00:03:07,120
und immer mehr dieser Biowissenschaftler sagen uns auch

31
00:03:07,120 --> 00:03:13,630
die vier Elemente aufzugeben,
nicht mehr versuchen, sie zu fangen

32
00:03:13,630 --> 00:03:18,490
Ozeanologen, Hydrologen
erklären Sie uns, dass die Ozeane

33
00:03:18,490 --> 00:03:24,250
sie sind die wahren Lungen der Erde und
dass wir aufhören müssen, sie zu versauern,

34
00:03:24,250 --> 00:03:30,280
um sie zu töten, sie mit Plastik zu füllen.
Erdspezialisten,

35
00:03:30,280 --> 00:03:34,590
mit seiner Pflanzen- und Tierwelt, mit dem, was in seinen Eingeweiden steckt...

36
00:03:34,590 --> 00:03:40,900
seine Ressourcen, seine fossilen Brennstoffe,
seine seltenen Erden, wir müssen sie verlassen,

37
00:03:40,900 --> 00:03:48,060
wir müssen sie gehen lassen, sagen uns Agronomen, Geologen, Biochemiker...

38
00:03:48,060 --> 00:03:53,290
Vulkanologen. Wir müssen eine
andere Funktionen.

39
00:03:53,290 --> 00:03:56,740
Es geht hier nicht um die Zen-Meister, es geht um
wirklich Wissenschaftler

40
00:03:56,740 --> 00:04:03,760
Auch sie haben nun ein tiefes Verständnis des Prinzips der Interdependenz.

41
00:04:03,760 --> 00:04:10,810
Feuer, das Menschen ohne Vorsicht benutzen, ohne zu wissen, wie man es kontrolliert,

42
00:04:10,810 --> 00:04:15,959
Atomkernfusion,
das Feuer der Sonne,

43
00:04:15,959 --> 00:04:21,669
wir müssen loslassen, wir wissen es nicht, es ist...
zu kompliziert.

44
00:04:21,669 --> 00:04:27,340
Wir müssen einen anderen, einfacheren Weg finden, um zu arbeiten und schließlich die Luft, die wir unatembar machen...

45
00:04:27,340 --> 00:04:33,580
mit einer mit CO2 gesättigten Atmosphäre
Methan, giftige Gase,

46
00:04:33,960 --> 00:04:41,160
Klimatologen, Meteorologen
Glaziologen und viele andere,

47
00:04:41,160 --> 00:04:47,760
sagen uns: Aufhören, loslassen,
aus dem Weg gehen

48
00:04:47,770 --> 00:04:52,330
also lehren sie uns auch alle,
aufhören zu versuchen zu grabschen, aufhören zu grabschen...

49
00:04:52,330 --> 00:04:59,100
die vier Elemente. In Shôdôka
"verzichtet" auf das chinesische Zeichen "放 fang".

50
00:04:59,100 --> 00:05:06,070
"放 fang" bedeutet auch frei es bedeutet
sagen: "Gib auf, lass los, Feigling".

51
00:05:06,070 --> 00:05:12,600
Es ist wie wenn man
Bringen wir die Schafe auf die Weide, lassen wir sie frei laufen,

52
00:05:12,600 --> 00:05:20,139
weide das Gras ab, ganz ruhig.
Lassen Sie die fossilen Ressourcen in der Erde,

53
00:05:20,139 --> 00:05:27,190
Lasst die Erde ruhen. Meister Deshimaru sagte in seinem Kommentar

54
00:05:27,190 --> 00:05:32,300
dass wir nicht nach dem Ego in der
Körper, den jeder haben und besitzen möchte.

55
00:05:32,300 --> 00:05:36,450
aber es ist dasselbe zwischen uns und
den Planeten.

56
00:05:36,450 --> 00:05:43,479
Wir betrachten es als unser
Körper, aber wir wissen es einfach nicht.

57
00:05:43,479 --> 00:05:51,970
die göttliche Substanz,
spirituell, das ist Unsinn,

58
00:05:51,970 --> 00:05:59,420
wir verhalten uns mit dem Planeten
wie mit unseren Körpern.

59
00:05:59,760 --> 00:06:07,680
Unter Zazen können wir es sehr genau verstehen, und wenn wir die vier Elemente loslassen

60
00:06:08,080 --> 00:06:14,340
es ist Buddhas Land, Frieden, Satori, die verwirklicht werden.

61
00:06:14,780 --> 00:06:22,280
in einer einfachen, harmonischen Operation, die kein Leben verletzt oder tötet.

62
00:06:22,460 --> 00:06:27,920
der Buddha-Samen, die Möglichkeit
sich darüber zu erheben.

63
00:06:28,260 --> 00:06:35,020
Was mich hier interessiert, ist, dass jetzt die Wissenschaftler selbst

64
00:06:35,700 --> 00:06:44,060
fangen an, herauszufinden, was wir vorhaben,
die wir natürlich wahrnehmen und praktizieren.

65
00:06:47,560 --> 00:06:55,560
Die Wissenschaftler erklären auch
dass wir selbst aus diesen vier Elementen bestehen.

66
00:06:55,900 --> 00:07:02,920
sondern auch, dass wir ein lebendiger Körper sind, in dem

67
00:07:03,400 --> 00:07:09,379
Es gibt mehr Bakterien, mehr Viren, 
mikrobielle

68
00:07:09,379 --> 00:07:18,439
wir haben, glaube ich, mehr Viren als
von lebenden Zellen,

69
00:07:18,439 --> 00:07:23,649
Es funktioniert, weil es ein Gleichgewicht, einen Schutz gibt.

70
00:07:23,649 --> 00:07:29,019
Natürlich, wenn es kein Gleichgewicht, keinen Schutz gibt...

71
00:07:29,019 --> 00:07:35,959
Glücklicherweise konnten wir uns treffen,
Zazen zu kennen, es zu praktizieren,

72
00:07:35,959 --> 00:07:42,979
um ihn bekannt zu machen. Es ist sehr kostbar, nicht nur für uns, sondern für die Existenzen.