1
00:00:12,840 --> 00:00:20,310
In a poem by Yoka Daishi,
a Chinese master who wrote

2
00:00:20,310 --> 00:00:25,100
The Song of Immediate Satori,

3
00:00:25,100 --> 00:00:27,100
the Shôdôka

4
00:00:27,800 --> 00:00:35,760
and was a disciple of Master Eno, 
whose transmission he received

5
00:00:35,760 --> 00:00:37,210
in a meeting that lasted a day and a night.

6
00:00:37,210 --> 00:00:43,100
Afterwards he continued teaching.

7
00:00:44,040 --> 00:00:50,040
His poem: "Abandon the four elements, no longer seek to hoard,

8
00:00:50,740 --> 00:00:57,880
in peace and absolute completion,

9
00:00:58,500 --> 00:01:01,940
Drink and eat as you wish.

10
00:01:02,360 --> 00:01:08,660
All phenomena are impermanent, everything is KU, without noumenon, without any substance of its own...

11
00:01:09,080 --> 00:01:14,080
and that’s just the great
and complete Buddha satori."

12
00:01:14,560 --> 00:01:17,820
Master Deshimaru says that here the poem deals with

13
00:01:18,260 --> 00:01:23,620
of the Three Treasures of Buddhism the abandonment of the four elements,

14
00:01:23,900 --> 00:01:30,300
the four elements
it’s water, earth, fire, air.

15
00:01:30,880 --> 00:01:35,440
The second treasure is peace and quiet and the
tranquility of zazen, nirvana,

16
00:01:36,060 --> 00:01:44,400
the third the impermanence of the phenomena, the awareness, the absence of noumenon of existences.

17
00:01:44,740 --> 00:01:50,120
In the first treasury, drop the four elements

18
00:01:50,120 --> 00:01:56,720
and no longer seeks to catch them, hold them, seize them.

19
00:01:56,720 --> 00:02:04,300
Master Deshimaru says that we must not seek the noumenon or the ego.

20
00:02:04,300 --> 00:02:09,980
Everybody’s looking for their body’s ego.
he wants to own his body

21
00:02:10,700 --> 00:02:15,580
and from there feelings like jealousy, selfishness, envy are born.

22
00:02:15,580 --> 00:02:21,940
so in the first treasure,
you learn to give up, let go,

23
00:02:21,940 --> 00:02:28,150
his body, his ego, his mind.
It’s interesting because you can also see

24
00:02:28,150 --> 00:02:33,580
that these four elements, abandoning the
four elements and no longer seek to

25
00:02:33,580 --> 00:02:39,560
to catch them, to seize them
in connection with our world.

26
00:02:39,560 --> 00:02:44,000
Water, earth, fire and air.

27
00:02:44,000 --> 00:02:48,840
In the teachings
of the Zen Masters there’s always

28
00:02:48,850 --> 00:02:53,560
a deep intuition, and this is
interesting, because this intuition

29
00:02:53,560 --> 00:03:00,370
it joins modern science, the
life sciences

30
00:03:00,370 --> 00:03:07,120
and more and more of these life scientists are also telling us

31
00:03:07,120 --> 00:03:13,630
to abandon the four elements,
quit trying to catch them

32
00:03:13,630 --> 00:03:18,490
oceanologists, hydrologists
explain to us that the oceans

33
00:03:18,490 --> 00:03:24,250
they are the true lungs of the earth and
that we have to stop acidifying them,

34
00:03:24,250 --> 00:03:30,280
to kill them, fill them with plastic.
Earth specialists,

35
00:03:30,280 --> 00:03:34,590
with its plant and animal life, with what’s in its entrails...

36
00:03:34,590 --> 00:03:40,900
its resources, its fossil fuels,
his rare earths, we must leave them,

37
00:03:40,900 --> 00:03:48,060
we have to let them go, agronomists, geologists, biochemists tell us...

38
00:03:48,060 --> 00:03:53,290
volcanologists. We need to find a
other functioning.

39
00:03:53,290 --> 00:03:56,740
It’s not the Zen Masters here, it’s
really scientists

40
00:03:56,740 --> 00:04:03,760
They too, now have a deep understanding of the principle of interdependence.

41
00:04:03,760 --> 00:04:10,810
Fire that humans use without caution, without knowing how to control it,

42
00:04:10,810 --> 00:04:15,959
atomic nuclear fusion,
the fire of the sun,

43
00:04:15,959 --> 00:04:21,669
we have to let go, we don’t know, it’s...
too complicated.

44
00:04:21,669 --> 00:04:24,417
We have to find another, simpler way of workin.

45
00:04:24,417 --> 00:04:27,340
And finally the air, which we make unbreathable...

46
00:04:27,340 --> 00:04:33,580
with an atmosphere saturated with CO2
methane, toxic gases,

47
00:04:33,960 --> 00:04:41,160
climatologists, meteorologists
glaciologists and many others,

48
00:04:41,160 --> 00:04:47,760
are telling us, stop, let go,
keep out of the way

49
00:04:47,770 --> 00:04:52,330
so they teach us all, too,
to stop trying to grab, to stop grabbing...

50
00:04:52,330 --> 00:04:59,100
the four elements. In shôdôka
"abandons" the Chinese character it’s "放 fang"

51
00:04:59,100 --> 00:05:06,070
"放 fang" also means free it means
say give up, let go, coward.

52
00:05:06,070 --> 00:05:12,600
It’s like when you
bring the sheep to pasture, we let them go free,

53
00:05:12,600 --> 00:05:20,139
graze the grass, quietly.
Leave the fossil resources in the earth,

54
00:05:20,139 --> 00:05:27,190
let the earth rest. Master Deshimaru in his commentary said

55
00:05:27,190 --> 00:05:32,300
that we shouldn’t look for ego in the
body, everyone wants to have and possess that body.

56
00:05:32,300 --> 00:05:36,450
but it’s the same between us and
the planet.

57
00:05:36,450 --> 00:05:43,479
We consider it our
body but we just don’t know about it.

58
00:05:43,479 --> 00:05:51,970
the divine substance,
spiritual, it’s nonsense,

59
00:05:51,970 --> 00:05:59,420
we behave with the planet
like with our bodies.

60
00:05:59,760 --> 00:06:07,680
By zazen we can understand it very exactly and if we let go of the four elements

61
00:06:08,080 --> 00:06:14,340
it is the Buddha’s Land, peace, satori, which are realized.

62
00:06:14,780 --> 00:06:22,280
in a simple, harmonious operation that does not hurt or kill life.

63
00:06:22,460 --> 00:06:27,920
the Buddha seed, the possibility
to rise above it.

64
00:06:28,260 --> 00:06:35,020
What I’m interested in here is that now the scientists themselves

65
00:06:35,700 --> 00:06:44,060
are starting to find out what we’re up to,
we perceive and practice naturally.

66
00:06:47,560 --> 00:06:55,560
The scientists also explain
that we’re made of these four elements ourselves.

67
00:06:55,900 --> 00:07:02,920
but also that we are a living body in which

68
00:07:03,400 --> 00:07:09,379
there’s more bacteria, more viruses, 
microbial

69
00:07:09,379 --> 00:07:18,439
we have more, I think, viruses than even
of living cells,

70
00:07:18,439 --> 00:07:23,649
it works because there’s a balance, a protection.

71
00:07:23,649 --> 00:07:29,019
Of course, when there’s no balance, no protection...

72
00:07:29,019 --> 00:07:35,959
fortunately we were able to meet,
to know zazen, to practice it,

73
00:07:35,959 --> 00:07:42,979
to make him known. It’s very precious, not only for us but for all life.
