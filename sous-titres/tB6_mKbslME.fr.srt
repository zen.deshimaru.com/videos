1
00:00:00,000 --> 00:00:04,660
Vous avez dit que si les chefs d’État effectuent des prostations, 

2
00:00:05,080 --> 00:00:08,560
le monde serait plus paisible. 

3
00:00:08,880 --> 00:00:13,040
Je me demandais … 

4
00:00:13,340 --> 00:00:18,340
par rapport aux autres religions. 

5
00:00:18,620 --> 00:00:23,080
Dans la religion musulmane, on parle de prosternation. 

6
00:00:23,460 --> 00:00:34,540
Pourtant, certains pays du Moyen-Orient se déchirent. 

7
00:00:35,180 --> 00:00:42,020
Je voulais savoir pourquoi. 

8
00:00:42,320 --> 00:00:46,000
Parce que la prosternation est la même dans le zen et dans l’islam. 

9
00:00:46,340 --> 00:00:52,660
Je pense qu’il y a du bon et du mauvais dans toutes les religions. 

10
00:00:53,080 --> 00:00:58,240
Mais la prosternation est une bonne chose, dans la religion musulmane. 

11
00:00:58,400 --> 00:01:04,620
Les chrétiens joignent également leurs mains de la même manière que dans le zen, à Gasshô. 

12
00:01:05,100 --> 00:01:08,520
Ça va. 

13
00:01:08,700 --> 00:01:14,040
Il y a aussi du fanatisme, il a toujours été là. 

14
00:01:14,300 --> 00:01:18,820
Il y a toujours eu des guerres de religion. 

15
00:01:21,320 --> 00:01:26,920
En zen, la prosternation n’est pas faite dans un but précis. 

16
00:01:27,280 --> 00:01:33,440
Ni pour adorer la statue sur l’autel, ni pour prier Bouddha. 

17
00:01:33,960 --> 00:01:39,640
C’est la pratique de la posture avec tout votre corps. 

18
00:01:40,020 --> 00:01:46,140
Au moment où vous touchez le sol, il y a une prise de conscience. 

19
00:01:46,500 --> 00:01:50,860
Habituellement, le cerveau ne touche jamais le sol. 

20
00:01:51,100 --> 00:01:55,820
Il est toujours dans le ciel, pointant toujours les étoiles. 

21
00:01:56,100 --> 00:02:01,480
Donc ça fait juste du bien. Pour vous reconnecter, penchez-vous, touchez. 

22
00:02:01,740 --> 00:02:06,300
Quand les musulmans font cela, c’est parce qu’il y a de très bonnes choses dans l’islam. 

23
00:02:06,660 --> 00:02:13,000
Les familles élèvent bien leurs enfants. 

24
00:02:15,500 --> 00:02:21,620
Les vrais musulmans sont pleins d’amour. 

25
00:02:24,200 --> 00:02:30,020
Ne jetez pas le bébé avec l’eau du bain! 

26
00:02:30,550 --> 00:02:34,450
C’est bien qu’ils se prosternent! 

