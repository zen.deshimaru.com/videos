1
00:00:13,650 --> 00:00:17,340
então eu nasci em Montpellier, na França 

2
00:00:17,340 --> 00:00:22,510
mas agora eu vivo em um som meus pais são dois países diferentes 

3
00:00:22,510 --> 00:00:26,950
de repente, viajo frequentemente entre o único na França e também em outros países 

4
00:00:26,950 --> 00:00:32,169
porque eles vêem lá, eu vejo lá com o trabalho deles, mas todo verão chegamos 

5
00:00:32,169 --> 00:00:36,370
aqui o templo na montanha desde a usinagem quando eu tinha dois 

6
00:00:36,370 --> 00:00:44,960
semanas eu acho que temos tempo integral crescendo aqui eu nunca sempre 

7
00:00:44,960 --> 00:00:50,390
venha aqui o que eu e meus amigos sabemos o tempo todo, sim, vamos 

8
00:00:50,390 --> 00:00:55,130
continue vindo aqui, mesmo quando você não é um adulto, mas você disse a si mesmo 

9
00:00:55,130 --> 00:00:58,430
nunca que nós teríamos que conversar não tinha sido, não era realmente 

10
00:00:58,430 --> 00:01:03,080
algo que pensamos nas pessoas que vêm aqui, elas vêm aqui para 

11
00:01:03,080 --> 00:01:08,960
Zen me quando eu não existia para ver as pernas para a comida jogar 

12
00:01:08,960 --> 00:01:15,560
e de repente eu descobri lá a sangha antes do Zen me e minha opinião que você tem é sobre 

13
00:01:15,560 --> 00:01:19,850
também tinha outro amigo vivien que tinha mais um vento do que ele e nós 

14
00:01:19,850 --> 00:01:25,729
para nós, é um pouco como colocá-lo no Doot Rue Godot que nos fez 

15
00:01:25,729 --> 00:01:32,090
acreditar em magia, ele nos fez acreditar em todos os tipos de coisas por 

16
00:01:32,090 --> 00:01:37,700
exemplo, uma mulher quando estava na floresta, ele nos disse que meio caminho andado 

17
00:01:37,700 --> 00:01:42,530
cavar no chão e porque sempre há diamantes no subsolo 

18
00:01:42,530 --> 00:01:48,140
não importa o pé, há pelo menos um diamante a cada metro do consulado 

19
00:01:48,140 --> 00:01:54,680
cavou e cavou e cavou e que, em qualquer caso, nós dois tivemos os dois lados 

20
00:01:54,680 --> 00:01:59,270
escondido dele, ele tirou um diamante do bolso e fingiu sair do 

21
00:01:59,270 --> 00:02:04,640
chão de um enorme diamante emprestado de sua mãe, em seguida, ficaram deslumbrados 

22
00:02:04,640 --> 00:02:12,230
nós temos que ter algo como 30 assim e durante o tipo muito dado depois 

23
00:02:12,230 --> 00:02:15,860
às vezes eu cavava um pouco atrevido com frequência se encontrasse um 

24
00:02:15,860 --> 00:02:21,110
diamante se eu já percebi que posso ter dez anos 

25
00:02:21,110 --> 00:02:24,250
algo se na minha pele eu te contar tudo 

26
00:02:24,250 --> 00:02:28,130
James é diferente para todos eu acredito em minha mãe quando o cansaço de 

27
00:02:28,130 --> 00:02:31,840
seus fãs, ela imediatamente sentiu que era azul pharsal pelo resto de sua vida 

28
00:02:31,840 --> 00:02:38,440
mas acredito que para mim, depois de cada Zen, começo a amar mais 

29
00:02:38,690 --> 00:02:43,950
em mais ou menos bem o segundo tiro eu digo a mim mesmo que eu teria 

30
00:02:43,950 --> 00:02:48,709
arrebatamento, além disso, não acabou 

31
00:02:49,280 --> 00:02:54,740
Eu sempre soube os anúncios que sempre tive, desde quando 

32
00:02:54,740 --> 00:02:59,840
mãe estava grávida, ela já praticava zazen de repente, é um pouco como 

33
00:02:59,840 --> 00:03:06,890
Eu nasci de repente, eu sempre conheci a sangha em diferentes templos 

34
00:03:06,890 --> 00:03:13,819
que tenho muitas lembranças que conheci amigos ou até cabanas 

35
00:03:13,819 --> 00:03:17,750
coisa que sempre foi de diferentes demandas e quando eu era pequeno eu 

36
00:03:17,750 --> 00:03:21,470
Eu não estava me escondendo, estava apenas dizendo aos meus amigos sim, eu estou indo para as montanhas e 

37
00:03:21,470 --> 00:03:25,850
tudo, mas eu nunca disse a eles que eu vou fazer eu vou para o zen eu disse a eles 

38
00:03:25,850 --> 00:03:28,400
não é porque eu tentei uma vez tudo a mesma coisa 

39
00:03:28,400 --> 00:03:31,190
todos eles me olharam muito estranhamente eu disse a mim mesmo ei 

40
00:03:31,190 --> 00:03:36,110
nunca fale de novo e, de outro modo, nunca me pareceu estranho, me pareceu 

41
00:03:36,110 --> 00:03:41,590
ainda mais normal ir de fato, eu sabia com antecedência se eu vim 

42
00:03:41,590 --> 00:03:46,010
Eu tinha que fazer um zen por dia, mas não era nada contra essa ideia 

43
00:03:46,010 --> 00:03:50,750
Francamente, para o meu jogo, eu queria descobrir precisamente que era minha escolha 

44
00:03:50,750 --> 00:03:54,709
venha aqui primeiro para ver toda a saga que eu não vi em dois 

45
00:03:54,709 --> 00:04:01,820
três anos depois o lugar a comida e todas as memórias que voltam 

46
00:04:01,820 --> 00:04:06,380
feito assim que chego a primeira vez que faço hazen eu não percebi 

47
00:04:06,380 --> 00:04:09,850
conta que eu estava fazendo isso realmente dá que era bastante estranho no sentido de que 

48
00:04:09,850 --> 00:04:15,320
Na verdade, não percebi muitas mudanças, não percebi porque não percebi 

49
00:04:15,320 --> 00:04:19,310
a utilidade do zen, mas parece tão normal para mim que eu não vi nenhum 

50
00:04:19,310 --> 00:04:23,660
mudar, na verdade, eu acho estranho, mas eu gosto, sinto 

51
00:04:23,660 --> 00:04:28,490
quanto mais sortudo Jean prat, mais eu gosto e fofinho, então eles me 

52
00:04:28,490 --> 00:04:31,090
parece diferente 

53
00:04:33,470 --> 00:04:41,410
a senhora validada na frança youtube em 

54
00:04:45,910 --> 00:04:57,220
França, de forma simples e eficiente 

55
00:05:05,560 --> 00:05:13,809
[Música] 

56
00:05:22,860 --> 00:05:25,929
[Música] 

57
00:07:03,510 --> 00:07:06,840
1 [música] 

58
00:07:06,840 --> 00:07:08,840
e 

