1
00:00:08,480 --> 00:00:13,559
hallo iedereen, ik zal uitleggen hoe het met je gaat 

2
00:00:13,559 --> 00:00:20,760
ga zitten om zenmeditatie te beoefenen, zazen genoemd, het benodigde materiaal 

3
00:00:20,760 --> 00:00:27,090
het is heel eenvoudig een mens en een meditatiekuiken als je een hebt 

4
00:00:27,090 --> 00:00:30,869
kussen eerder hamburg en dat is prima 

5
00:00:30,869 --> 00:00:35,730
en zo niet maak je er een met een deken waar een slaapzak doorheen gaat 

6
00:00:35,730 --> 00:00:41,060
het moet bijvoorbeeld stevig genoeg zijn 

7
00:00:57,490 --> 00:01:06,070
Dus volgens jou legt de zen-meditatiehouding zen-meditatie uit 

8
00:01:06,070 --> 00:01:13,620
vooral om te weten dat het een onbeweeglijke en stille zithouding is 

9
00:01:13,620 --> 00:01:22,630
en de bijzonderheid van deze houding en dat we het hier naar de muur oefenen 

10
00:01:22,630 --> 00:01:26,620
Uiteraard kijk ik naar de camera om je te informeren 

11
00:01:26,620 --> 00:01:32,560
dus wat je moet doen is bij elkaar zitten kussen degene die ik 

12
00:01:32,560 --> 00:01:37,479
leg uit dat u voldoende hard moet worden opgevuld 

13
00:01:37,479 --> 00:01:42,520
goed zitten aan de onderkant van het kuiken, niet op de rand zitten als je dat hebt 

14
00:01:42,520 --> 00:01:49,330
uitgegleden en je moet zo ver mogelijk je benen over elkaar steken 

15
00:01:49,330 --> 00:01:55,600
kruis ze zo als deze, zoals alleen, en het heet de 2010 als 

16
00:01:55,600 --> 00:02:04,270
je zet je andere mensen hier, het heet lotus, het belangrijkste is dat 

17
00:02:04,270 --> 00:02:13,480
je knieën gaan naar de grond om te drukken, druk op de knieën op de grond 

18
00:02:13,480 --> 00:02:19,780
dus hoe je dat moet doen om het te doen zonder enige organisatie of zonder enige inspanning 

19
00:02:19,780 --> 00:02:27,810
fysiek goede spieren bij alle zen meditatie houdingen dan loslaten 

20
00:02:27,810 --> 00:02:37,950
zodat je knieën naar beneden gaan, kantel je het bekken een beetje 

21
00:02:37,950 --> 00:02:46,180
naar voren gaat de bovenkant van het bekken naar voren als 6 tot een beetje laatste ik 

22
00:02:46,180 --> 00:02:49,440
laat het je zien in profiel 

23
00:02:55,200 --> 00:03:02,769
mijn bekken is niet cynisch alsof ik bijvoorbeeld aan het rijden ben 

24
00:03:02,769 --> 00:03:07,629
een auto zie daar maar ik laat ons het laatst zien 

25
00:03:07,629 --> 00:03:11,860
omdat mijn zwaartepunt achter mijn lichaam ligt wil ik 

26
00:03:11,860 --> 00:03:19,420
ziet er van hoge kwaliteit uit aan de voorkant en ik doe dit natuurlijk bij dassin bridges 

27
00:03:19,420 --> 00:03:24,970
wat we zouden willen en met zwaartekracht ervoor passeren, gaan mijn knieën naar de grond 

28
00:03:24,970 --> 00:03:32,970
dit is wat je moet beginnen door in de houding een zitplaats vast te stellen 

29
00:03:32,970 --> 00:03:42,609
waarbij de knie in staking gaat als je 7 tot 6 vanuit het bekken hebt gepresteerd 

30
00:03:42,609 --> 00:03:44,920
die een beetje schuin stonden om het verzoek te doen 

31
00:03:44,920 --> 00:03:51,940
je maakt de wervelkolom zoveel mogelijk recht 

32
00:03:51,940 --> 00:03:57,000
top van het hoofd van mening dat we van mening zijn dat 

33
00:03:57,000 --> 00:04:05,889
Keulen gaat zo ver als ik een spel zou plaatsen, is geen haak die ik herlees 

34
00:04:05,889 --> 00:04:12,280
wat betekent dat ik de nek van de nek trok. Ik paste mijn kin, ik zie dat ik het niet heb 

35
00:04:12,280 --> 00:04:16,349
hoofd draait vooraan Ik heb geen gebogen rug 

36
00:04:16,349 --> 00:04:24,610
Ik ben zo eerlijk mogelijk Ik laat je twee profielen zien die ik niet ben 

37
00:04:24,610 --> 00:04:33,910
alsof ik niet eerder op een veiling ben of wat ik het meest ben 

38
00:04:33,910 --> 00:04:43,659
van kruis, ook als je erin bent geslaagd rechtop te zitten, beweeg je niet 

39
00:04:43,659 --> 00:04:53,080
hoe meer ik de linkerhand vervolg, de vingers worden samengevoegd met de rechterhand 

40
00:04:53,080 --> 00:04:58,690
de vingers zijn gezegend en ik ga de linkervingers bovenop de 

41
00:04:58,690 --> 00:05:08,030
sporen van de rechterhand maar duwt elkaar overlappen de vinger van de 

42
00:05:08,030 --> 00:05:14,300
linkerhand vormt een horizontale lijn de rand van mijn handen 

43
00:05:14,300 --> 00:05:17,840
dus we konden de provincie over de hele pols 

44
00:05:17,840 --> 00:05:23,480
tot dan de rand van mijn handen morgen breng je het aan op de basis 

45
00:05:23,480 --> 00:05:31,370
van de buik om dit te doen, gebruikte iets dat 

46
00:05:31,370 --> 00:05:37,460
kan je laten staan ​​door terrasson die je kunt loslaten 

47
00:05:37,460 --> 00:05:43,780
volledige aandacht vanaf de schouders de houding van een houding waarbij je moet loslaten 

48
00:05:43,780 --> 00:05:47,990
laat de spanning los, je ziet dat mijn ellebogen vrij zijn, ik ben een lange tijd 

49
00:05:47,990 --> 00:05:56,090
zoals dit o zo rustig maar ik doe geen moeite om te onderhouden 

50
00:05:56,090 --> 00:06:01,070
mijn handen de moeite ideeën om de push in het restaurant te houden 

51
00:06:01,070 --> 00:06:08,650
zodra ik me realiseer dat dit het punt van houding is, raak ik het aan 

52
00:06:08,650 --> 00:06:13,120
dit is een fysieke houding 

53
00:06:20,000 --> 00:06:25,460
na dit eerste deel over fysieke houding tweede deel 

54
00:06:25,460 --> 00:06:30,860
de mentale houding zodra ik in de houding van ben 

55
00:06:30,860 --> 00:06:39,220
zen meditatie naar de muur gericht wat moet ik doen wat moet ik doen 

56
00:06:39,220 --> 00:06:46,190
in de stilte van deze houding ga je anderen 

57
00:06:46,190 --> 00:06:53,390
besef dat je denkt dat je ziet dat je blijft denken en dat er zijn 

58
00:06:53,390 --> 00:06:58,490
Duizenden gedachtenborstels die komen en zeggen, komen naar u toe 

59
00:06:58,490 --> 00:07:03,010
besef dat dit een natuurlijk fenomeen is 

60
00:07:03,010 --> 00:07:08,840
je hebt geen controle over wat we zullen doen tijdens zenmeditatie 

61
00:07:08,840 --> 00:07:16,790
we zullen onze gedachten laten passeren, dat wil zeggen in deze onbeweeglijkheid 

62
00:07:16,790 --> 00:07:23,150
we zullen kunnen waarnemen dat je met losgeld je gedachten zult observeren en 

63
00:07:23,150 --> 00:07:29,419
gedachten in plaats van ze te koesteren in plaats van ze te laten groeien, ga je 

64
00:07:29,419 --> 00:07:34,610
laat ze passeren, dus het gaat er niet om wat te weigeren 

65
00:07:34,610 --> 00:07:39,580
laat het er weer zijn, dit is de machine die je voorbij laat gaan 

66
00:07:39,580 --> 00:07:46,190
hoe je aandacht focus doorlaat 

67
00:07:46,190 --> 00:07:51,400
hier nu op je adem 

68
00:07:55,430 --> 00:07:59,960
ademen in zen is buikademhaling 

69
00:07:59,960 --> 00:08:10,400
u ademt in door uw neusgat en u ademt langzaam in 

70
00:08:10,400 --> 00:08:14,270
geruisloze uitsparing stille meditatie 

71
00:08:14,270 --> 00:08:20,090
het kenmerk dat ademen en je gaat je aandacht trekken 

72
00:08:20,090 --> 00:08:30,930
op het gezicht van de verhoging inspireren we door rustig in zijn te komen 

73
00:08:30,930 --> 00:08:37,620
genereuze vulling en geconfronteerd met de natie wanneer 

74
00:08:37,620 --> 00:08:44,070
je gaat alle lucht uit, je doet het langzaam en langzaam 

75
00:08:44,070 --> 00:08:47,720
langer en langer worden 

76
00:08:56,540 --> 00:09:03,889
als je aan het einde van je relatie bent, verlang je steeds weer 

77
00:09:03,889 --> 00:09:12,709
je ademt uit en je was niet langer langzaam dieper naakt hoe langer 

78
00:09:12,709 --> 00:09:18,069
diep nieuw zoals de golven op een oceaan 

79
00:09:18,069 --> 00:09:31,639
omdat ze het strand zijn en al deze dingen spreken met de v6 die jij focust 

80
00:09:31,639 --> 00:09:36,440
hier nu op de punten van de fysieke puntenhouding 

81
00:09:36,440 --> 00:09:42,259
dus kijk uit, kijk hier nu naar je lichaam als je 

82
00:09:42,259 --> 00:09:47,660
gefocust hier nu om je niet te volgen 

83
00:09:47,660 --> 00:09:55,040
gedachten laten we teruggaan naar de punten die houding zijn als je hier focust 

84
00:09:55,040 --> 00:10:03,139
nu bij het vestigen van een ademhaling in dappere harmonieus en beetje bij beetje 

85
00:10:03,139 --> 00:10:11,110
haal de togo de adem uit krijg langer en langer 

86
00:10:11,110 --> 00:10:18,860
maar de flippers hebben geen tijd meer om na te denken over wat je hier bent 

87
00:10:18,860 --> 00:10:27,860
nu in zaza-houding is hier in houding en het lichaam is er niet in 

88
00:10:27,860 --> 00:10:35,569
houding Ik herinner je eraan dat je een drukknie naar het ijs drukt 

89
00:10:35,569 --> 00:10:38,680
aarde duw je de aarde 

90
00:10:38,680 --> 00:10:44,920
en strek je rug zo veel mogelijk naar de top van de 

91
00:10:44,920 --> 00:10:49,260
schedel alsof hij de lucht met het hoofd wil duwen 

92
00:10:49,260 --> 00:10:55,290
de linkerhand zoals rechts de horizontale duimen het snijvlak van de handen 

93
00:10:55,290 --> 00:11:05,020
tegen de bal nou ik ben nog steeds in saumur naar de muur gericht, er is niets te doen 

94
00:11:05,020 --> 00:11:11,459
vooral wat ik ga doen is mijn blik naar binnen richten 

95
00:11:11,459 --> 00:11:16,330
dus de zintuigen ik zal ze niet langer gebruiken tijdens 

96
00:11:16,330 --> 00:11:22,060
dit feit meditatie Ik sta voor de muur, dus er is niets te zien dus het is 

97
00:11:22,060 --> 00:11:29,890
alsof ik mijn visie voor me neerzet op mijn drukke dus tijdens 

98
00:11:29,890 --> 00:11:38,620
meditatie was er het uitzicht van 45 ° diagonaal richting Europa je kunt 

99
00:11:38,620 --> 00:11:43,839
mediteer met je ogen dicht maar als je je ogen open houdt of de laatste 

100
00:11:43,839 --> 00:11:50,100
contact met de werkelijkheid gaat niet speciaal in je gedachten 

101
00:11:50,100 --> 00:11:56,110
zoals in stilte door gebruiken we ook het water daar niet 

102
00:11:56,110 --> 00:12:00,880
horen dat het geen muziek nodig heeft en we hebben al gezien dat we dat zijn 

103
00:12:00,880 --> 00:12:09,040
gémonies zo beetje bij beetje snijden we plotseling met de verzoeken van de wereld 

104
00:12:09,040 --> 00:12:15,880
buiten doen we de meditatie shot die je zit 

105
00:12:15,880 --> 00:12:21,970
fysiek is het het lichaam dat in de houding zit die je jezelf snijdt 

106
00:12:21,970 --> 00:12:28,360
verzoeken met de kraampjes die je in instant vliegtuigmodus zet, vertel het me niet 

107
00:12:28,360 --> 00:12:34,340
mediteer niet met je mobiel ernaast 

108
00:13:09,540 --> 00:13:14,399
dus om een ​​tip voor beginners in te vullen is het niet makkelijk 

109
00:13:14,399 --> 00:13:23,519
op die manier vertrouwen vinden is nu niet hetzelfde als harnas, maar dat is het wel 

110
00:13:23,519 --> 00:13:28,579
echt een goede een goede hulp je echt raad ik je aan 

111
00:13:28,579 --> 00:13:33,959
oefen het uitproberen en je zult zien in plaats van vermakelijke gedachten 

112
00:13:33,959 --> 00:13:38,790
dat ze misschien nog niet klaar zijn, maar misschien niet helemaal vrolijk 

113
00:13:38,790 --> 00:13:43,380
dit alles is om in een slecht humeur te zijn of in het middelpunt van de aandacht te staan 

114
00:13:43,380 --> 00:13:52,620
normaal hier met dit werk op het goud de houding van de geest die je loslaat 

115
00:13:52,620 --> 00:13:57,810
je distantieert je van alles wat ons overkomt om te gaan zien dat jij 

116
00:13:57,810 --> 00:14:03,329
zul je dit alles fysiek en emotioneel kalmeren 

117
00:14:03,329 --> 00:14:08,310
het zal je helpen los te laten het zal je en je diep helpen 

118
00:14:08,310 --> 00:14:13,079
entourage als je wilt was vergezeld wanneer niet twijfelde aan de 

119
00:14:13,079 --> 00:14:15,529
oefenen 

