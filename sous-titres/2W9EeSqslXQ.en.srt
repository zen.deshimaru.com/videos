1
00:00:00,000 --> 00:00:04,799
How do you synchronize kin-hin walking and breathing slowly? 

2
00:00:04,799 --> 00:00:08,550
You said we would go a step further with every exhalation. 

3
00:00:08,550 --> 00:00:15,690
But most people breathe twice in one step. 

4
00:00:15,690 --> 00:00:19,470
Of course I want to do two. 

5
00:00:19,470 --> 00:00:24,180
People double their breathing because they do not take a break between breaths. 

6
00:00:24,180 --> 00:00:28,650
When walking child-hin slowly, the little break is fundamental. 

7
00:00:28,650 --> 00:00:33,059
If you rest a little on top of the exhalation …. 

8
00:00:33,059 --> 00:00:38,210
You are in the rhythm. 

9
00:00:38,210 --> 00:00:44,309
Dogen and his master Nyojo say …. 

10
00:00:44,309 --> 00:00:50,219
… that the Buddha himself said that you don’t double your breath. 

11
00:00:50,219 --> 00:00:55,050
To do this, at the end of the exhalation, it is necessary to take a small break … 

12
00:00:55,050 --> 00:01:03,239
…. in which we will go half a step forward … 

13
00:01:03,239 --> 00:01:09,299
… after a short pause before inhaling and we are in good rhythm. 

14
00:01:09,299 --> 00:01:14,610
We get fed up …. much less. 

15
00:01:14,610 --> 00:01:20,580
During sesshin, I expanded the family hins. 

16
00:01:20,580 --> 00:01:26,159
I had already lived this in another exercise with another master … 

17
00:01:26,159 --> 00:01:32,670
You had to walk long with your eyes blindfolded. 

18
00:01:32,670 --> 00:01:38,280
We must walk without seeing anything. 

19
00:01:38,280 --> 00:01:42,270
It is tiring because you also have to put your hands in a position …. 

20
00:01:42,270 --> 00:01:45,540
… less comfortable than in kind-hin. 

21
00:01:45,540 --> 00:01:51,180
If you do family hin for a long time it will get tired …. 

22
00:01:51,180 --> 00:01:54,659
… another aspect of ourselves. 

23
00:01:54,659 --> 00:01:59,360
It is an active aspect. I learned in Chinese medicine …. 

24
00:01:59,360 --> 00:02:06,920
…. that to cure excessive activity, you need rest. 

25
00:02:13,340 --> 00:02:18,170
If you do family hin for a long time, you are exhausted. 

26
00:02:18,170 --> 00:02:23,230
You have back pain, shoulder pain, pain in your arms … 

27
00:02:23,230 --> 00:02:29,330
If you walk like this for an hour, people are exhausted. 

28
00:02:29,330 --> 00:02:33,290
They will only want one thing: to be in zazen. 

29
00:02:33,290 --> 00:02:41,470
What a relief when you are in zazen: it is complementary. 

30
00:02:41,470 --> 00:02:46,549
You say to yourself: what a relaxation, what a surrender! 

31
00:02:46,549 --> 00:02:51,650
You really want to be in Zazen. 

32
00:02:51,650 --> 00:02:57,920
After an hour in Zazen, you will have the feeling …. 

33
00:02:57,920 --> 00:03:02,720
….. unpleasant tingling … 

34
00:03:02,720 --> 00:03:07,040
You will want to move and wish it would end because of the pain. 

35
00:03:07,040 --> 00:03:11,630
Zazen and slow walking are completely complementary. 

36
00:03:11,630 --> 00:03:14,870
But because we practice child hin for a short time, we don’t realize it. 

37
00:03:14,870 --> 00:03:19,430
I will now do the child hins longer during sesshinsµ. 

38
00:03:19,430 --> 00:03:25,130
So that people like to sit down and let go of certain things in zazen. 

39
00:03:25,130 --> 00:03:29,350
Your question is very good and not new. 

40
00:03:29,350 --> 00:03:38,920
Master Dogen posed the problem 1,400 years ago. 

