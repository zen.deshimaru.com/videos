1
00:00:02,555 --> 00:00:04,935
Wir unterhielten uns im Kusen...

2
00:00:05,365 --> 00:00:08,125
hier in Europa...

3
00:00:14,475 --> 00:00:17,625
...Wille.

4
00:00:18,965 --> 00:00:21,545
Es ist nicht der Wille.

5
00:00:22,980 --> 00:00:27,800
Der Wille ist etwas, das ausgelöst wird

6
00:00:42,860 --> 00:00:44,860
...unbewusst.

7
00:00:56,040 --> 00:00:58,740
Danach erklärte ich...

8
00:01:00,840 --> 00:01:03,660
...hier im Westen...

9
00:01:11,580 --> 00:01:16,280
Zunächst einmal, allen einen guten Morgen!

10
00:01:21,100 --> 00:01:23,915
Lassen Sie uns dieses Zazen jetzt teilen.

11
00:01:27,795 --> 00:01:29,465
mit großer Freude

12
00:01:41,220 --> 00:01:46,880
Es ist uns gelungen, ein Sommercamp zu organisieren und durchzuführen.

13
00:01:49,540 --> 00:01:53,520
trotz komplizierter Ereignisse

14
00:01:56,080 --> 00:01:59,870
Unsere Zahl ist begrenzt, weil Sie nicht gekommen sind.

15
00:02:05,140 --> 00:02:08,080
wegen des Sicherheitsraums

16
00:02:13,600 --> 00:02:19,900
Wir sind gezwungen, im Dojo mehr Abstand zu halten

17
00:02:20,140 --> 00:02:25,820
Wir haben alles ernsthaft gemacht, wir haben die Regeln befolgt...

18
00:02:48,715 --> 00:02:52,160
Sie ist eine der...

19
00:02:52,160 --> 00:02:54,860
Die Besonderheiten des Zen

20
00:02:58,940 --> 00:03:01,860
als der Regel zu folgen

21
00:03:02,380 --> 00:03:07,780
"Ja, aber ich wurde nicht so erzogen... Ich bin nicht so, ich!"

22
00:03:12,340 --> 00:03:16,840
Im Zen... folgen wir der Regel...

23
00:03:20,685 --> 00:03:23,535
Die Regel, wo wir sind.

24
00:03:33,200 --> 00:03:36,580
Also erklärte ich, dass der Wille

25
00:03:39,480 --> 00:03:42,380
ist Yin oder Yang

26
00:03:47,760 --> 00:03:52,500
Wenn ich sage, entspanne deine Beine, dann ist das Yin-Wille...

27
00:03:53,840 --> 00:03:56,220
Wir lassen los

28
00:04:06,125 --> 00:04:08,765
Wenn du loslässt, löst du eine Resonanz aus...

29
00:04:19,460 --> 00:04:23,340
der Wille des Yang...

30
00:04:23,640 --> 00:04:26,820
es drückt die Erde mit den Knien.

31
00:04:40,600 --> 00:04:43,640
Ich erzählte die Geschichte

32
00:04:46,480 --> 00:04:48,380
und Gedanken

33
00:04:50,300 --> 00:04:52,760
Zur Baso’schen Yang-Methode

34
00:04:54,760 --> 00:04:57,500
Und Sekitos Yin-Methode

35
00:04:59,440 --> 00:05:03,380
Ich werde dieses Kusen mit Ihnen teilen.

36
00:05:22,140 --> 00:05:25,300
Es ist eine sehr berühmte Geschichte

37
00:05:27,420 --> 00:05:31,780
aber aus einem anderen Blickwinkel als gewöhnlich betrachtet.

38
00:05:33,960 --> 00:05:39,300
Wir sprechen von Baso, der die Dharma-Übertragung bereits empfangen hatte...

39
00:05:40,620 --> 00:05:42,680
Von seinem Meister

40
00:05:49,820 --> 00:05:54,400
Er fuhr fort, den Lehren seines Meisters zu folgen und sesshins...

41
00:06:03,270 --> 00:06:08,320
Er hatte eine komische Art

42
00:06:11,580 --> 00:06:18,180
Er wollte Zazen allein im Dojo machen, auch wenn es nicht an der Zeit war...

43
00:06:19,740 --> 00:06:23,730
Er wollte schon immer Zazen machen

44
00:06:28,320 --> 00:06:34,360
Eines Tages kam sein Herr zu ihm, als er sich setzte...

45
00:06:36,360 --> 00:06:38,400
und er fragt sie

46
00:06:40,480 --> 00:06:42,920
Großer Mönch,

47
00:06:43,425 --> 00:06:46,645
-Baso war sehr groß-

48
00:06:48,400 --> 00:06:53,700
Was suchen Sie, wenn Sie in Zazen sitzen?

49
00:06:57,060 --> 00:06:58,640
Mit anderen Worten:

50
00:06:58,860 --> 00:07:03,920
Was suchen Sie? Haben Sie ein Ziel?

51
00:07:04,300 --> 00:07:08,560
Haben Sie ein Ziel jenseits...

52
00:07:10,580 --> 00:07:15,000
das wichtiger ist als sich nur hinzusetzen?

53
00:07:20,040 --> 00:07:22,540
Diese Geschichte ist sehr bekannt

54
00:07:24,325 --> 00:07:28,875
Es ist der Yang-Aspekt des Wollens...

55
00:07:31,705 --> 00:07:34,625
Danach erfand ich die Dialoge

56
00:07:41,045 --> 00:07:44,175
"- Haben Sie ein Ziel?"

57
00:07:44,565 --> 00:07:47,525
"-Ja, ich habe ein Ziel", sagt Baso.

58
00:07:50,435 --> 00:07:54,045
Ich möchte Buddha werden. Ich möchte Buddha werden.

59
00:07:56,345 --> 00:07:59,445
Also... (seufzt)

60
00:07:59,535 --> 00:08:02,035
Ich ziele

61
00:08:02,225 --> 00:08:05,195
das Zentrum des Ziels

62
00:08:08,525 --> 00:08:11,655
und ich erreiche ihn jedes Mal.

63
00:08:21,920 --> 00:08:25,000
Dies ist der Yang-Aspekt von Zazen.

64
00:08:25,240 --> 00:08:26,720
die ich gestern erwähnt habe

65
00:08:33,620 --> 00:08:35,080
Ich sagte

66
00:08:36,800 --> 00:08:42,360
wie Meister Deshimaru uns lehrte, als wir jung waren...

67
00:08:49,120 --> 00:08:53,785
Er sorgte mit seinem Kyosaku für viel Spannung im Dojo.

68
00:08:55,045 --> 00:08:57,875
Es ist die Yang-Methode

69
00:08:59,385 --> 00:09:02,385
Sobald jemand umgezogen ist

70
00:09:03,105 --> 00:09:05,395
oder eingeschlafen sind

71
00:09:06,385 --> 00:09:09,345
er schlug ihn

72
00:09:09,545 --> 00:09:12,765
Wir waren im Sommercamp

73
00:09:12,875 --> 00:09:15,405
Es war sehr heiß.

74
00:09:15,915 --> 00:09:17,455
wir waren klatschnass.

75
00:09:19,765 --> 00:09:22,925
Nach Zazen gab es auf den Tatami eine Pfütze.

76
00:09:32,260 --> 00:09:37,340
"Ich setze mich hin, um ein Buddha zu werden, und das war’s. Ich habe meine ganze Energie darauf verwendet.

77
00:09:40,910 --> 00:09:43,550
Das ist die Methode von Deshimaru.

78
00:09:45,080 --> 00:09:48,490
Mit aller Kraft zum Vergnügen üben

79
00:09:51,435 --> 00:09:54,475
Genau das hat Baso getan.

80
00:10:00,980 --> 00:10:03,980
Also die Yin-Methode,

81
00:10:05,960 --> 00:10:10,240
Was ich sagen wollte, war, dass Baso keinen Zweifel daran hatte

82
00:10:13,420 --> 00:10:19,180
Ihm war es egal, ob er einen Zweck hatte oder nicht, ob er Mushotoku war oder nicht.

83
00:10:24,460 --> 00:10:28,020
Es war der Wille. Er hatte den Wunsch, er hatte den Willen.

84
00:10:37,535 --> 00:10:40,345
sagte Sensei:

85
00:10:41,185 --> 00:10:44,415
es ist besser, Zazen weniger Zeit zu machen

86
00:10:48,985 --> 00:10:51,825
aber ein starkes Zazen zu machen, wie Baso...

87
00:10:58,875 --> 00:11:02,865
Anstatt Zazen lange Zeit zu machen, den ganzen Tag lang,

88
00:11:06,525 --> 00:11:10,005
und zu schlafen oder zu denken

89
00:11:18,425 --> 00:11:19,425
20 Minuten.

90
00:11:19,565 --> 00:11:22,445
mit aller Macht

91
00:11:24,055 --> 00:11:26,785
es ist sehr effektiv

92
00:11:31,355 --> 00:11:33,725
Wenn Sie Zazen lieben,

93
00:11:35,995 --> 00:11:38,475
zum Spaß...

94
00:11:42,375 --> 00:11:45,085
Wir sind keine Bekehrer.

95
00:11:56,515 --> 00:12:01,235
Wir teilen nur, einfach so zum Spaß.

96
00:12:41,325 --> 00:12:44,085
Lassen Sie uns auf das Yin-Beispiel zurückkommen,

97
00:12:44,205 --> 00:12:45,865
Lockern Sie sich

98
00:12:47,820 --> 00:12:51,610
Wenn ich die Beine loslasse, gibt es das ganze Becken frei...

99
00:12:54,180 --> 00:12:57,340
Ich habe das Gefühl, schwerelos zu sein.

100
00:13:10,580 --> 00:13:13,040
Das angeführte Beispiel

101
00:13:14,165 --> 00:13:18,815
ist es, den lang ersehnten Moment zu erreichen, in dem wir ein Nickerchen machen werden

102
00:13:26,135 --> 00:13:29,145
Wo Sie alles fallen lassen

103
00:13:30,775 --> 00:13:33,425
schlafen

104
00:13:35,935 --> 00:13:39,845
Das Beispiel des Mittagsschlafs ist offensichtlich eine Metapher...

105
00:13:42,045 --> 00:13:45,005
die sich auf Zazen bezieht

106
00:13:59,545 --> 00:14:02,295
Jedoch 7 Jahrhunderte vor Christus.

107
00:14:09,240 --> 00:14:12,240
ein Wissenschaftler namens Pythagoras

108
00:14:14,595 --> 00:14:16,545
sagte:

109
00:14:18,960 --> 00:14:21,300
achten Sie auf die Nacht

110
00:14:27,500 --> 00:14:33,120
da Sie die Nacht nicht kontrollieren können

111
00:14:34,600 --> 00:14:39,980
Seien Sie beim Einschlafen vorsichtig.

112
00:14:43,600 --> 00:14:48,240
Schlafen Sie nicht ein, ohne den letzten Moment vor dem Einschlafen zu überprüfen.

113
00:15:02,695 --> 00:15:05,735
Zazen besteht aus diesen beiden Aspekten

114
00:15:05,885 --> 00:15:08,785
Yin und Yang

115
00:15:11,440 --> 00:15:16,260
die ihrerseits von den großen Meistern Baso und Sekito

116
00:15:21,370 --> 00:15:36,380
Das eine kann als "Tötet den Buddha" und das andere als "Lasst den Buddha frei" bezeichnet werden.

117
00:15:41,680 --> 00:15:49,020
Der große Meister Sekito war sein Zeitgenosse. Es waren Mönche, die zur gleichen Zeit lebten.

118
00:15:52,300 --> 00:15:56,300
Baso und Sekito stammten aus der gleichen Zeit.

119
00:16:00,980 --> 00:16:04,560
Oft gingen die Jünger des einen zum anderen.

120
00:16:10,570 --> 00:16:14,000
Zu dieser Zeit hatte Sekito noch keine Schüler...

121
00:16:17,455 --> 00:16:20,865
Er lebte in den Wäldern... in den Bergen...

122
00:16:22,445 --> 00:16:24,945
Er hatte sich selbst aufgebaut

123
00:16:25,385 --> 00:16:28,405
eine kleine Hütte

124
00:16:34,335 --> 00:16:36,915
mit Holz und Stroh

125
00:16:44,315 --> 00:16:48,895
auf demselben Berg, auf dem Meister Nangaku gelehrt hatte...

126
00:16:54,435 --> 00:16:57,825
sagt Sekito:

127
00:16:58,545 --> 00:17:01,475
"Ich fühle mich in dieser Hütte geschützt."

128
00:17:02,865 --> 00:17:07,355
Ich habe es selbst gebaut. Es ist mein einziger Reichtum.

129
00:17:10,965 --> 00:17:13,395
Aber wenn ich meine Mahlzeit beendet habe,

130
00:17:16,045 --> 00:17:19,015
Ich bereite in aller Ruhe den Moment des Mittagsschlafs vor...

131
00:17:23,655 --> 00:17:26,825
und ich sage das jeden Tag

132
00:17:29,295 --> 00:17:34,525
und jeden Tag aufs Neue

133
00:17:38,875 --> 00:17:41,665
Das Erlebnis Essen und Mittagsschlaf,

134
00:17:44,135 --> 00:17:46,955
ist eine Macht der Patriarchen

135
00:17:50,035 --> 00:17:53,055
derjenige, der seine Mahlzeit noch nicht beendet hat,

136
00:17:56,345 --> 00:18:03,075
ist noch nicht abgeschlossen.

137
00:18:08,215 --> 00:18:10,015
Er ist noch nicht zufrieden.
