1
00:00:00,100 --> 00:00:01,460
Ich heiße Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
Mein Zen-Mönch heißt Ryurin. 

3
00:00:03,880 --> 00:00:05,580
Ich bin auch ein Kalligraph. 

4
00:00:05,900 --> 00:00:09,560
Ich unterrichte chinesische Kalligraphie, die ich im Zen entdeckt habe. 

5
00:00:09,840 --> 00:00:13,860
Ich habe es von einem Zen-Mönch gelernt. 

6
00:00:14,320 --> 00:00:18,260
Ich teile gerne Kalligraphie mit allen. 

7
00:00:18,860 --> 00:00:22,540
Ich arbeite viel in Schulen. 

8
00:00:22,900 --> 00:00:27,360
Ich mache Kalligraphie mit Kindern in Workshops. 

9
00:00:27,700 --> 00:00:31,020
Auch für Erwachsene und Behinderte. 

10
00:00:31,380 --> 00:00:33,300
Sie mögen es sehr. 

11
00:00:33,780 --> 00:00:39,220
Meine jüngsten Schüler sind 6 Jahre alt und mein ältester Schüler ist 90 Jahre alt. 

12
00:00:39,760 --> 00:00:42,800
Ich unterrichte auch Chinesisch. 

13
00:00:43,180 --> 00:00:44,840
Das ist mein Kerngeschäft. 

14
00:00:45,140 --> 00:00:48,020
Diese Kalligraphie ist einfach "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
Es ist eine Kalligraphie, die im aktuellen halbkursiven Stil hergestellt wurde. 

16
00:00:54,580 --> 00:01:06,760
Der Pinsel ist kurz, er geht von Punkten zu Linien, um ein chinesisches oder japanisches Zeichen zu bilden. 

17
00:01:08,420 --> 00:01:11,320
Dies ist "Reiki" (靈氣), die spirituelle Energie. 

18
00:01:11,620 --> 00:01:13,880
Es ist eine Heilmethode. 

19
00:01:14,220 --> 00:01:20,260
Wir setzen diese Tradition fort, weil sie von einem Meister stammt, der viel Kalligraphie praktiziert hat. 

20
00:01:20,500 --> 00:01:23,920
Er praktizierte auch Sumi-e (墨 絵), er war Maler. 

21
00:01:24,280 --> 00:01:27,580
Ich schätze die Bilder von Meister Deshimaru sehr, er war Künstler. 

22
00:01:27,840 --> 00:01:31,020
Er malte und machte Kalligraphie sehr gut. 

23
00:01:31,440 --> 00:01:37,860
Seine Kalligraphie ist sehr stark, dicht und energisch. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) ist der Name eines Sutra, das gesungen wird. 

25
00:01:43,700 --> 00:01:52,480
Ich zeichnete es lockig nach: ein bisschen wie kleine Spaghetti. 

26
00:01:52,780 --> 00:01:56,440
Die Charaktere sind sehr verknüpft und vereinfacht. 

27
00:01:56,780 --> 00:02:00,800
Das gefällt uns auch in der Zen-Praxis. 

28
00:02:01,240 --> 00:02:08,040
Zen-Praktizierende mochten dieses Schreiben sehr, das ziemlich frei und ausdrucksstark ist. 

29
00:02:09,260 --> 00:02:10,740
Es ist eine energetische Kunst. 

30
00:02:11,020 --> 00:02:14,840
Es wird mit dem ganzen Körper in Verbindung mit der Atmung geübt. 

31
00:02:15,240 --> 00:02:21,900
In Verbindung mit seiner Verfügbarkeit des Geistes, seines Bewusstseins. 

32
00:02:22,360 --> 00:02:24,720
Es ist also eine Energiekunst. 

33
00:02:25,000 --> 00:02:28,120
Wir werden uns beruhigen, etwas in uns neu ausrichten. 

34
00:02:28,360 --> 00:02:33,240
Es ist eine vollständige Metamorphose, eine Alchemie des Gleichgewichts. 

35
00:02:33,560 --> 00:02:40,040
Horizontalität, Vertikalität, Zentrierung, Kopplung beim Zeichnen der Zeichen. 

36
00:02:40,540 --> 00:02:43,120
Verfügbarkeit von Hand. 

37
00:02:43,460 --> 00:02:48,080
In der Zen-Meditation ist die Präsenz in den Händen sehr wichtig. 

38
00:02:48,420 --> 00:02:55,600
Ob stationär oder laufend. 

39
00:02:55,940 --> 00:02:57,760
Die Geste ist in der Kalligraphie anders. 

40
00:02:58,180 --> 00:03:00,120
Aber es gibt eine Präsenz durch die Hände. 

41
00:03:00,340 --> 00:03:02,320
Es ist die Hand-zu-Gehirn-Beziehung. 

42
00:03:02,600 --> 00:03:04,140
Kalligraphie hat ein Leben. 

43
00:03:04,440 --> 00:03:05,440
Auch wenn Sie nichts davon wissen! 

44
00:03:05,680 --> 00:03:07,980
Einige Kalligraphien werden Sie berühren, andere nicht. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) ist chinesisch und eher taoistisch. 

46
00:03:11,740 --> 00:03:15,820
Es bedeutet "das Lebensprinzip nähren". 

47
00:03:16,540 --> 00:03:23,440
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

