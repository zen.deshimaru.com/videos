1
00:01:31,170 --> 00:01:35,220
No Soto Zen, que praticamos, dizemos: "Não busque grande iluminação". 

2
00:01:35,560 --> 00:01:38,407
A iluminação, por si só, não existe. 

3
00:01:38,407 --> 00:01:40,170
Por que ela existiria? 

4
00:01:40,780 --> 00:01:45,720
Você tem que olhar a natureza e as coisas como elas são para ver a verdade. 

5
00:01:46,200 --> 00:01:49,663
A iluminação é absolutamente natural. 

6
00:01:49,663 --> 00:01:52,780
Não é uma condição especial. 

7
00:01:53,320 --> 00:01:59,400
No Zen, não há condição especial na iluminação, a menos que haja uma doença da mente. 

8
00:01:59,700 --> 00:02:04,760
Não há iluminação especial em si e não se deve procurar uma. 

9
00:02:05,540 --> 00:02:07,433
Muitos mestres insistem nisso. 

10
00:02:07,433 --> 00:02:08,681
Isto é extremamente importante. 

11
00:02:08,681 --> 00:02:11,000
A iluminação está a todo momento, a todo zazen. 

12
00:02:11,320 --> 00:02:15,580
Eu estava conversando com um especialista em drogas psicotrópicas … 

13
00:02:15,790 --> 00:02:20,700
projetado para alcançar estados especiais de consciência. 

14
00:02:21,130 --> 00:02:24,686
Ele sustentou que o zazen era um meio de alcançar a iluminação. 

15
00:02:24,686 --> 00:02:25,889
Isso definitivamente não é verdade. 

16
00:02:26,350 --> 00:02:31,379
Zazen é a própria iluminação. 

17
00:02:32,410 --> 00:02:33,254
Dogen disse: 

18
00:02:33,254 --> 00:02:36,269
"Fui à China e conheci um verdadeiro mestre". 

19
00:02:37,390 --> 00:02:41,729
"O que aprendi com isso é que os olhos são horizontais e o nariz é vertical". 

20
00:02:42,370 --> 00:02:45,810
"E esse não deve ser enganado, nem por si nem pelos outros." 

21
00:03:44,750 --> 00:03:46,750
e todos os músculos da barriga 

22
00:03:47,269 --> 00:03:52,358
deve ser relaxado para dar a impressão de que o intestino está pesando. 

23
00:04:09,010 --> 00:04:15,420
se nos concentrarmos completamente na postura naquele momento, podemos deixar os pensamentos 

24
00:04:15,850 --> 00:04:17,850
sem parar por aí. 

25
00:04:25,810 --> 00:04:28,914
A coisa mais bonita que você pode fazer com seu corpo é o zazen. 

26
00:04:28,914 --> 00:04:30,419
Este é o esplendor da natureza. 

27
00:04:30,849 --> 00:04:33,728
Inspira calma, harmonia, força. 

28
00:04:33,728 --> 00:04:36,539
Tudo está contido na postura do zazen 

29
00:04:39,969 --> 00:04:43,447
No acampamento de verão, tento incentivar a prática. 

30
00:04:43,447 --> 00:04:46,111
A atmosfera no dojo, as posturas. 

31
00:04:46,111 --> 00:04:47,909
Uma pessoa que perturba isso 

32
00:04:50,540 --> 00:04:53,940
com sua história pessoal, estou disposto a matá-la. 

33
00:04:57,120 --> 00:04:59,120
É a única regra que conheço: compaixão. 

34
00:05:00,600 --> 00:05:02,477
Sensei estava exibindo seu kyosaku: 

35
00:05:02,477 --> 00:05:04,160
"o cajado da compaixão!" 

36
00:05:04,660 --> 00:05:11,080
Às vezes, ele acertava 20 tiros seguidos. 

37
00:05:14,440 --> 00:05:19,640
Ele dizia: "Sensei, muito compassivo!" 

38
00:05:26,280 --> 00:05:32,940
Quando um discípulo quer ser ajudado em sua meditação ao ser atingido no ombro com um graveto, ele pede. 

39
00:05:33,180 --> 00:05:36,800
Porque ele sente sua atenção começando a diminuir. 

40
00:05:37,140 --> 00:05:41,360
Quando você sente sono. 

41
00:05:41,580 --> 00:05:46,300
Ou quando você sentir complicações nublando seu cérebro. 

42
00:05:46,540 --> 00:05:49,520
Não é mais tão quieto. 

43
00:05:49,940 --> 00:05:54,600
Se você sentir sono ou se sua mente estiver inquieta … 

44
00:06:01,240 --> 00:06:03,240
Gasshô! 

45
00:06:21,580 --> 00:06:23,580
Zazen continua durante 

46
00:06:23,840 --> 00:06:25,233
por várias horas todos os dias. 

47
00:06:25,233 --> 00:06:25,929
E às vezes dia e noite. 

48
00:06:26,180 --> 00:06:30,291
Mas ele é interrompido por uma meditação enquanto caminha. 

49
00:06:30,291 --> 00:06:33,489
Ou seja, o estado interno permanece, 

50
00:06:34,280 --> 00:06:38,799
mas a postura é diferente. 

51
00:06:39,940 --> 00:06:44,040
Toda a atenção está focada no polegar. 

52
00:06:44,600 --> 00:06:47,500
Cada respiração corresponde a meio passo. 

53
00:06:58,880 --> 00:07:05,020
Os olhos estão no chão a 3 metros à frente. 

54
00:07:13,340 --> 00:07:18,580
É a mesma respiração que no zazen, durante a meditação na postura sentada. 

55
00:07:20,740 --> 00:07:28,300
A respiração zen é o oposto da respiração normal. 

56
00:07:28,560 --> 00:07:30,940
Quando você pede que alguém respire, 

57
00:07:31,240 --> 00:07:34,600
o reflexo é respirar fundo. 

58
00:07:35,060 --> 00:07:38,860
No zen, vamos nos concentrar 

59
00:07:39,080 --> 00:07:42,740
especialmente na expiração. 

60
00:07:43,040 --> 00:07:46,780
A expiração é longa, calma e profunda. 

61
00:07:54,360 --> 00:07:56,160
Está ancorado no chão. 

62
00:07:56,160 --> 00:07:58,860
Pode durar até um, dois minutos. 

63
00:07:59,080 --> 00:08:02,940
Dependendo do seu estado de concentração e calma. 

64
00:08:03,280 --> 00:08:09,600
Pode ser muito macio, imperceptível. 

65
00:08:09,860 --> 00:08:15,640
Primeiro estudamos essa respiração insistindo, como uma vaca gemendo. 

66
00:08:38,020 --> 00:08:44,640
No final da expiração, deixamos que a inspiração ocorra automaticamente. 

67
00:08:45,880 --> 00:08:49,640
Relaxamos tudo, abrimos os pulmões. 

68
00:08:50,300 --> 00:08:55,380
Deixamos a inspiração vir automaticamente. 

69
00:08:55,820 --> 00:08:58,689
Não colocamos muita intenção em inspiração. 

70
00:08:59,600 --> 00:09:03,129
Ao fazer isso, você entenderá 

71
00:09:04,550 --> 00:09:06,550
essa respiração é tudo. 

72
00:09:07,910 --> 00:09:09,910
É um círculo. 

73
00:09:10,070 --> 00:09:12,070
Muitas vezes, os mestres desenham um círculo. 

74
00:09:12,560 --> 00:09:14,560
É sobre respirar. 

75
00:09:15,040 --> 00:09:18,880
Essa respiração teve muita influência, 

76
00:09:19,340 --> 00:09:23,200
especialmente nas artes marciais. 

77
00:09:23,480 --> 00:09:29,160
Nas artes marciais japonesas, a respiração zen é usada. 

78
00:09:29,580 --> 00:09:34,840
Muitos grandes mestres em artes marciais, como Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
praticou o Zen e se tornou discípulo de grandes mestres. 

80
00:09:39,940 --> 00:09:45,480
E adaptaram o ensino do Zen e a respiração à sua arte marcial. 

81
00:09:45,820 --> 00:09:53,860
No final, a respiração está além das técnicas e é totalmente livre. 

82
00:09:58,080 --> 00:10:03,340
O ensino do Buda para seus discípulos é muito simples. 

83
00:10:03,640 --> 00:10:09,440
É baseado nos atos fundamentais do ser humano. 

84
00:10:09,660 --> 00:10:11,260
Como se mover 

85
00:10:11,480 --> 00:10:14,120
Como comer, como respirar. 

86
00:10:14,440 --> 00:10:15,836
Como pensar. 

87
00:10:15,836 --> 00:10:20,860
E, finalmente, como se sentar para realizar sua divindade. 

88
00:11:24,940 --> 00:11:28,702
Genmai é o alimento dos monges desde o Buda Shakyamuni. 

89
00:11:28,702 --> 00:11:31,800
Misturamos os vegetais que temos à mão com arroz. 

90
00:11:32,280 --> 00:11:39,440
Cozinhamos por muito tempo, com muita atenção e concentração. 

91
00:11:39,780 --> 00:11:42,730
É uma comida passada do Buda. 

92
00:11:42,730 --> 00:11:45,920
Na China e no Japão, comemos a mesma comida. 

93
00:11:47,660 --> 00:11:49,980
Realmente é o corpo do Buda. 

94
00:11:50,280 --> 00:11:53,080
Quando estamos prestes a fazer a nossa refeição, 

95
00:11:53,440 --> 00:11:55,560
você deve se perguntar: 

96
00:11:55,900 --> 00:11:59,460
"Por que eu vou comer?" 

97
00:11:59,680 --> 00:12:01,580
Um animal ou um lout 

98
00:12:01,960 --> 00:12:05,680
não vai se perguntar e pular para cima e para baixo em seu prato. 

99
00:12:06,040 --> 00:12:09,820
Obviamente, é pela sobrevivência, mas sobrevivência pelo quê? 

100
00:12:10,060 --> 00:12:13,520
É o que diz nos sutras que são cantados durante as refeições. 

101
00:12:13,780 --> 00:12:15,240
"Por que estamos comendo?" 

102
00:12:15,520 --> 00:12:17,591
"De onde vem essa comida? 

103
00:12:17,800 --> 00:12:18,860
Quem o preparou? " 

104
00:12:19,180 --> 00:12:21,140
"Quem cultivou arroz e legumes?" 

105
00:12:21,400 --> 00:12:26,140
"Quem cozinhou?" 

106
00:12:26,500 --> 00:12:29,960
Dedicamos a refeição e agradecemos a todas essas pessoas. 

107
00:12:30,240 --> 00:12:32,140
E em plena consciência, 

108
00:12:32,620 --> 00:12:37,340
Eu decido comer esse alimento por 

109
00:12:37,780 --> 00:12:44,620
a evolução que vim realizar nesta terra, neste mundo material. 

110
00:12:52,360 --> 00:12:56,900
O EMS é às 10:30. 6 pessoas para a cozinha. 

111
00:13:01,080 --> 00:13:04,080
Duas pessoas para o grande mergulho. 

112
00:13:05,940 --> 00:13:10,340
Três pessoas pelo serviço. 

113
00:13:19,600 --> 00:13:23,580
Samu não está especialmente fazendo alvenaria, varrendo … 

114
00:13:24,190 --> 00:13:27,989
ou lavar a louça. 

115
00:13:28,540 --> 00:13:30,540
Samu é tudo ação 

116
00:13:30,760 --> 00:13:32,939
(Escrever um livro é um samu) 

117
00:13:35,320 --> 00:13:37,320
realizada sem 

118
00:13:37,640 --> 00:13:38,860
intenção 

119
00:13:39,180 --> 00:13:40,160
escondido 

120
00:13:44,950 --> 00:13:47,249
sem motivos secretos. 

121
00:13:47,980 --> 00:13:50,048
É como o seu trabalho na vida cotidiana. 

122
00:13:50,048 --> 00:13:52,289
Você pode tomá-lo como enriquecimento pessoal 

123
00:13:52,839 --> 00:13:57,749
ou como um tipo de presente que você dá a si mesmo e a todos os outros ao mesmo tempo. 

124
00:13:59,320 --> 00:14:02,460
As vezes considero 

125
00:14:03,400 --> 00:14:05,459
Algumas pessoas podem encarar isso como uma tarefa. 

126
00:14:05,920 --> 00:14:09,390
Mas aposto que aqueles que se machucam no zazen. 

127
00:14:16,400 --> 00:14:19,550
Como freira zen, 

128
00:14:23,040 --> 00:14:26,180
que atitude você deve tomar no trabalho? 

129
00:14:27,080 --> 00:14:30,100
Não sei, nunca trabalhei! 

130
00:14:33,960 --> 00:14:37,060
Temos que fazer dele um samu? 

131
00:14:38,500 --> 00:14:39,520
Sim, esse é o ponto. 

132
00:14:39,740 --> 00:14:41,100
Eu estava falando besteira, hein. 

133
00:14:41,300 --> 00:14:47,500
Quando eu era mais jovem, tinha um emprego como zelador. 

134
00:14:48,090 --> 00:14:54,920
Limpei as escadas e tirei o lixo todas as manhãs às 5:00. 

135
00:14:55,140 --> 00:15:00,619
As escadas, era um viva. Houve merda de cachorro. 

136
00:15:00,920 --> 00:15:05,360
Eu pensei: "Mas este não é o dojo ou o zen". 

137
00:15:05,760 --> 00:15:09,760
"Não importa. Para mim, é como um templo." 

138
00:15:10,320 --> 00:15:14,160
Eu fiz isso como um samurai. 

139
00:15:14,480 --> 00:15:17,040
E o Sensei me perguntou: 

140
00:15:17,420 --> 00:15:20,080
"Stéphane, eu gostaria que você limpasse o dojo." 

141
00:15:20,400 --> 00:15:23,660
"Sim, eu vou. Sexta-feira eu estou livre." 

142
00:15:25,440 --> 00:15:27,920
"Farei na próxima sexta-feira." 

143
00:15:28,580 --> 00:15:29,660
Ele me disse: 

144
00:15:29,940 --> 00:15:32,500
"Não, eu quero que você faça isso todos os dias." 

145
00:15:33,120 --> 00:15:34,272
Então ele disse: 

146
00:15:34,520 --> 00:15:37,440
"Um dia sem trabalho, um dia sem comida." 

147
00:15:37,860 --> 00:15:40,720
"Mas não tem nada a ver com comida!" 

148
00:15:41,000 --> 00:15:44,440
"Um dia sem trabalhar, um dia sem comer mushotoku!" 

149
00:15:44,780 --> 00:15:47,380
Era uma expressão muito original: 

150
00:15:47,700 --> 00:15:50,380
"Você deve comer mushotoku!" 

151
00:15:54,760 --> 00:15:57,540
Não significou nada! 

152
00:15:58,180 --> 00:16:02,500
Dizendo que você é um monge, praticar samu é revolução. 

153
00:16:02,880 --> 00:16:05,880
Nós devemos fazer a revolução, eu sempre disse isso! 

154
00:16:06,920 --> 00:16:10,657
Mas não sei como usar coquetéis molotov! 

155
00:16:11,000 --> 00:16:14,100
Então, no trabalho dele, você precisa praticar o samu. 

156
00:16:14,400 --> 00:16:19,780
Para resolver todos os problemas do mundo, tudo o que você precisa fazer é encontrar isso. 

157
00:16:20,100 --> 00:16:22,060
As pessoas não têm mais o espírito do samu. 

158
00:16:22,520 --> 00:16:23,920
Eles não comem mais mushotoku. 

159
00:16:24,160 --> 00:16:25,580
Eles comem hambúrgueres e bebem coca-cola. 

160
00:16:26,180 --> 00:16:28,880
Então, quem te disser: 

161
00:16:29,580 --> 00:16:31,580
Mas são 6:30, você é o único que resta! 

162
00:16:31,880 --> 00:16:33,140
Você diz a ele: 

163
00:16:33,360 --> 00:16:36,160
"Eu pratico samu!" 

164
00:16:45,640 --> 00:16:47,640
"Porque eu quero comer mushotoku!" 

165
00:16:49,780 --> 00:16:51,200
"O que é mushotoku?" 

166
00:16:51,440 --> 00:16:52,780
"Eu também quero um pouco!" 

167
00:16:57,040 --> 00:17:01,200
Temos que ensinar as pessoas sem ter medo. 

168
00:17:03,180 --> 00:17:06,660
Tenha orgulho de ser meus discípulos e de ser monges! 

169
00:17:07,000 --> 00:17:09,000
E ser discípulos de Deshimaru. 

