1
00:00:00,800 --> 00:00:06,680
Kin-hin "El paseo meditativo del Zen"

2
00:00:07,600 --> 00:00:09,140
Buenos días.

3
00:00:09,400 --> 00:00:12,060
Hoy me gustaría explicar

4
00:00:12,060 --> 00:00:16,600
cómo se practica la meditación Zen al caminar

5
00:00:16,820 --> 00:00:20,300
que llamamos "kin hin"

6
00:00:20,400 --> 00:00:23,340
Este paseo está hecho

7
00:00:23,340 --> 00:00:26,515
entre dos períodos de meditación sentada.

8
00:00:26,520 --> 00:00:31,140
En general, una sesión de meditación Zen

9
00:00:31,140 --> 00:00:36,060
comienza con treinta o cuarenta minutos de zazen.

10
00:00:36,360 --> 00:00:38,920
En la postura expliqué 
(en otro video):

11
00:00:38,920 --> 00:00:41,740
quieto, silencioso,

12
00:00:41,740 --> 00:00:43,500
frente a la pared.

13
00:00:43,660 --> 00:00:46,240
Entonces, este paseo

14
00:00:46,240 --> 00:00:49,860
se hace durante 5 o 10 minutos.

15
00:00:50,140 --> 00:00:53,420
y al final de este paseo,

16
00:00:53,480 --> 00:00:58,480
volvemos a donde tenemos nuestra almohada de meditación

17
00:00:58,700 --> 00:01:04,160
para sentarse en zazen, de pie frente a la pared.

18
00:01:04,620 --> 00:01:08,580
¿Cómo caminas cuando haces "Kin hin"?

19
00:01:08,900 --> 00:01:11,480
Ya que estamos caminando,

20
00:01:11,480 --> 00:01:13,780
Vamos a movernos.

21
00:01:14,000 --> 00:01:16,820
¿Cómo nos movemos?

22
00:01:17,660 --> 00:01:20,740
Ponemos los pies en su sitio.

23
00:01:21,380 --> 00:01:23,500
No es así.

24
00:01:23,800 --> 00:01:25,500
No es así.

25
00:01:25,500 --> 00:01:28,480
Paralelo y alineado.

26
00:01:28,480 --> 00:01:34,580
Separados el uno del otro, la distancia de un puño.

27
00:01:35,300 --> 00:01:40,420
Movamos el equivalente a medio pie.

28
00:01:40,880 --> 00:01:47,180
Un pie mide aproximadamente 30 o 35 centímetros.

29
00:01:47,640 --> 00:01:50,820
Así que, vamos a hacer pasos de 6 u 8 pulgadas

30
00:01:51,000 --> 00:01:52,300
como mucho.

31
00:01:52,600 --> 00:01:55,555
Si me miras los pies,

32
00:01:55,560 --> 00:01:59,100
verás mis pasos ir así.

33
00:01:59,240 --> 00:02:02,120
Decimos que medio pie.

34
00:02:08,120 --> 00:02:09,120
Bueno...

35
00:02:09,520 --> 00:02:12,425
¿Cómo vamos a caminar?

36
00:02:12,425 --> 00:02:16,620
Caminemos con la respiración.

37
00:02:17,540 --> 00:02:23,840
Nuestra respiración nos da el ritmo.

38
00:02:25,100 --> 00:02:28,320
Inspirar.

39
00:02:28,320 --> 00:02:31,720
Es el mismo tipo de respiración que hacemos durante el zazen.

40
00:02:31,980 --> 00:02:34,760
Inspiramos...

41
00:02:34,760 --> 00:02:38,720
Durante este tiempo cuando tomamos aire,

42
00:02:38,720 --> 00:02:42,040
una pierna va hacia adelante

43
00:02:44,955 --> 00:02:47,875
Durante la exhalación,

44
00:02:47,875 --> 00:02:51,075
lento y profundo,

45
00:02:51,075 --> 00:02:53,725
empujando hacia abajo

46
00:02:54,100 --> 00:02:56,960
empujando los intestinos hacia abajo

47
00:02:57,265 --> 00:02:59,440
Durante la exhalación

48
00:02:59,780 --> 00:03:02,120
por la nariz,

49
00:03:02,660 --> 00:03:07,340
pondremos todo el peso en el cuerpo

50
00:03:07,520 --> 00:03:10,800
en la pierna delantera.

51
00:03:11,140 --> 00:03:15,160
Pierna delantera estirada.

52
00:03:15,160 --> 00:03:18,700
toda la planta del pie descansando en el suelo,

53
00:03:18,700 --> 00:03:21,220
incluyendo el talón.

54
00:03:21,620 --> 00:03:23,980
La pata trasera,

55
00:03:23,980 --> 00:03:26,660
que no tiene peso,

56
00:03:27,060 --> 00:03:31,880
se mantiene suelto.

57
00:03:31,880 --> 00:03:34,300
Pero también,

58
00:03:34,300 --> 00:03:36,340
toda la planta del pie,

59
00:03:36,360 --> 00:03:42,660
incluyendo el talón, permanece descansando en el suelo.

60
00:03:42,660 --> 00:03:46,540
no levantamos los talones.

61
00:03:47,465 --> 00:03:48,595
Así que..,

62
00:03:48,920 --> 00:03:53,260
Yo inspiro y doy el paso.

63
00:03:53,460 --> 00:03:57,460
Larga y profunda exhalación.

64
00:03:57,660 --> 00:04:00,500
Durante la exhalación

65
00:04:00,720 --> 00:04:06,100
transferir el peso del cuerpo a la pierna delantera,

66
00:04:06,100 --> 00:04:11,420
hasta la raíz del dedo gordo de enfrente,

67
00:04:11,520 --> 00:04:15,140
como si quisiera dejar una marca en el suelo.

68
00:04:15,300 --> 00:04:21,380
Imagina que el suelo es arcilla o arena.

69
00:04:22,380 --> 00:04:27,620
Entonces la huella de su pie sería registrada

70
00:04:27,880 --> 00:04:30,900
Inspiración: Doy el paso.

71
00:04:31,340 --> 00:04:37,740
Exhalación: transferir el peso del cuerpo a la pierna delantera

72
00:04:37,940 --> 00:04:40,420
He llegado al final de mi exhalación.

73
00:04:40,660 --> 00:04:45,600
No tengo más remedio que volver a tomar un poco de aire.

74
00:04:45,640 --> 00:04:52,060
Pasaré la pierna que está más atrás, adelante.

75
00:04:52,300 --> 00:04:55,600
Transfiero el peso corporal

76
00:04:55,600 --> 00:04:58,680
en la pierna que hizo el movimiento

77
00:04:58,680 --> 00:05:02,260
y otra vez en la otra pierna.

78
00:05:07,340 --> 00:05:10,620
El ritmo de nuestra respiración

79
00:05:10,640 --> 00:05:15,300
establecer el ritmo de la caminata

80
00:05:15,860 --> 00:05:17,800
Obviamente,

81
00:05:17,800 --> 00:05:25,100
mantenemos la misma actitud mental que durante la meditación sentada

82
00:05:25,360 --> 00:05:27,920
Me concentré aquí y ahora,

83
00:05:27,920 --> 00:05:30,000
en lo que estoy haciendo.

84
00:05:30,000 --> 00:05:32,400
No estoy siguiendo mis pensamientos.

85
00:05:32,400 --> 00:05:35,305
El mismo aliento.

86
00:05:35,305 --> 00:05:39,960
Sólo la actividad física ha cambiado.

87
00:05:39,960 --> 00:05:45,060
"La postura de la mano"

88
00:05:46,360 --> 00:05:48,380
Mientras caminamos,

89
00:05:48,540 --> 00:05:51,660
¿qué hacemos con nuestras manos?

90
00:05:52,355 --> 00:05:55,455
cuando estamos frente a la pared,

91
00:05:55,765 --> 00:05:58,545
tenemos esta actitud

92
00:06:00,020 --> 00:06:02,935
Suelta tu mano derecha.

93
00:06:02,940 --> 00:06:05,800
Deja tu mano izquierda.

94
00:06:05,920 --> 00:06:08,760
Me envolveré el pulgar con los dedos.

95
00:06:08,940 --> 00:06:11,920
Todos los dedos juntos,

96
00:06:12,100 --> 00:06:17,020
dejando que la raíz del pulgar izquierdo sobresalga.

97
00:06:17,260 --> 00:06:19,400
Mano derecha.

98
00:06:19,540 --> 00:06:22,580
Todos los dedos juntos

99
00:06:22,580 --> 00:06:28,800
Los dedos de la mano derecha involucran a la mano izquierda.

100
00:06:29,560 --> 00:06:33,180
Voy a apretar un poco las muñecas.

101
00:06:33,760 --> 00:06:36,720
para que los dos antebrazos

102
00:06:36,720 --> 00:06:39,720
están en la misma línea.

103
00:06:39,720 --> 00:06:45,780
No tengo un antebrazo más alto que el otro.

104
00:06:47,300 --> 00:06:51,860
La raíz del pulgar izquierdo

105
00:06:52,640 --> 00:06:57,260
está en contacto con el plexo solar.

106
00:06:57,820 --> 00:06:59,740
La base del esternón.

107
00:06:59,980 --> 00:07:03,860
Todo ser humano tiene un pequeño agujero aquí,

108
00:07:03,880 --> 00:07:05,760
en la base del esternón.

109
00:07:05,780 --> 00:07:14,160
ahí es donde necesitas hacer contacto con la raíz de tu pulgar.

110
00:07:15,060 --> 00:07:18,640
Mis antebrazos paralelos al suelo.

111
00:07:18,700 --> 00:07:22,420
Mis manos también.

112
00:07:22,820 --> 00:07:25,020
No así.

113
00:07:25,480 --> 00:07:30,080
Mantengo esta postura con los hombros relajados.

114
00:07:30,200 --> 00:07:33,360
No mantengo esta postura con los hombros así:

115
00:07:33,400 --> 00:07:35,360
con los codos hacia arriba.

116
00:07:35,500 --> 00:07:38,260
pero tampoco así.

117
00:07:40,820 --> 00:07:42,880
Como en zazen:

118
00:07:42,940 --> 00:07:44,600
columna recta,

119
00:07:44,680 --> 00:07:46,020
...con la nuca extendida,

120
00:07:46,360 --> 00:07:48,320
La barbilla entró,

121
00:07:48,340 --> 00:07:51,920
Mira la diagonal 45.

122
00:07:54,520 --> 00:07:57,320
Ahora, ¿cómo voy a caminar?

123
00:07:57,900 --> 00:08:02,600
Durante la inspiración,

124
00:08:02,600 --> 00:08:04,120
estamos dando un paso adelante.

125
00:08:06,800 --> 00:08:09,660
En la exhalación

126
00:08:10,840 --> 00:08:15,740
...transfieres el peso del cuerpo de una pierna a la otra.

127
00:08:15,820 --> 00:08:18,060
tal como lo expliqué.

128
00:08:18,060 --> 00:08:20,240
Al mismo tiempo,

129
00:08:20,240 --> 00:08:25,740
vas a dar la mano ligeramente.

130
00:08:25,740 --> 00:08:30,500
por eso la flexibilidad en las muñecas es importante.

131
00:08:31,980 --> 00:08:33,560
Al mismo tiempo,

132
00:08:33,680 --> 00:08:38,235
una ligera presión de la raíz del pulgar izquierdo

133
00:08:38,235 --> 00:08:41,405
contra el plexo solar.

134
00:08:42,020 --> 00:08:45,120
Todo eso durante la exhalación.

135
00:08:46,120 --> 00:08:51,540
Resumen

136
00:08:53,040 --> 00:08:56,500
Mano izquierda, pulgar, dedos juntos,

137
00:08:56,520 --> 00:08:58,760
mano derecha,

138
00:08:58,760 --> 00:08:59,580
en la parte superior.

139
00:08:59,780 --> 00:09:04,000
La raíz del pulgar izquierdo en el esternón.

140
00:09:04,160 --> 00:09:08,880
Yo inspiro.

141
00:09:09,760 --> 00:09:13,680
Mi pierna va hacia adelante

142
00:09:14,140 --> 00:09:16,120
Fase de exhalación:

143
00:09:16,300 --> 00:09:17,340
En cuánto

144
00:09:17,480 --> 00:09:22,120
Muevo todo el peso de mi cuerpo sobre esta pierna,

145
00:09:22,120 --> 00:09:24,480
bien estirado,

146
00:09:24,800 --> 00:09:27,920
prensa de mano

147
00:09:27,920 --> 00:09:30,340
y contra el plexo solar.

148
00:09:31,280 --> 00:09:32,620
Inspiración.

149
00:09:32,860 --> 00:09:35,620
Un paso adelante.

150
00:09:35,620 --> 00:09:37,840
Una larga y profunda exhalación

151
00:09:38,020 --> 00:09:41,300
el peso en la pierna delantera.

152
00:09:41,320 --> 00:09:45,940
Presión entre las manos y contra el plexo solar.

153
00:09:48,440 --> 00:09:51,180
Fin de la exhalación

154
00:09:51,320 --> 00:09:55,640
Daré un paso y aprovecharé para relajarme.

155
00:09:55,740 --> 00:09:57,500
Voy de nuevo...

156
00:09:57,540 --> 00:10:00,660
Presiono el suelo

157
00:10:00,820 --> 00:10:03,815
con el peso de mi cuerpo.

158
00:10:03,820 --> 00:10:05,040
la presión entre las manos

159
00:10:05,240 --> 00:10:07,380
y contra el plexo.

160
00:10:07,420 --> 00:10:09,460
Las presiones son suaves.

161
00:10:09,460 --> 00:10:13,000
No se trata de salir herido.

162
00:10:14,140 --> 00:10:15,840
Todo esto,

163
00:10:15,840 --> 00:10:20,340
guiados por el ritmo de nuestra propia respiración,

164
00:10:20,340 --> 00:10:22,460
durante 5 o 10 minutos

165
00:10:22,660 --> 00:10:24,920
es que volvamos entonces,

166
00:10:24,920 --> 00:10:28,980
a donde está nuestra almohada de meditación.

167
00:10:29,280 --> 00:10:30,280
En la vida cotidiana,

168
00:10:30,560 --> 00:10:34,740
este paseo meditativo llamado "Kin hin"

169
00:10:35,560 --> 00:10:37,940
es muy práctico, muy eficiente.

170
00:10:38,960 --> 00:10:43,040
Tal vez estás en un momento de espera

171
00:10:43,520 --> 00:10:46,075
o en momentos de estrés.

172
00:10:46,075 --> 00:10:47,805
Y puedes aislarte

173
00:10:48,200 --> 00:10:50,860
durante 2, 3 o 5 minutos.

174
00:10:50,980 --> 00:10:54,340
¿Dónde se conectará con el aquí y ahora

175
00:10:54,815 --> 00:10:57,965
En la conciencia de tu respiración

176
00:10:57,965 --> 00:11:03,840
y teniendo en cuenta los puntos que he explicado.

177
00:11:04,620 --> 00:11:07,755
Practicar este paseo de meditación

178
00:11:07,760 --> 00:11:15,480
verás lo rápido que te sientes menos estresado

179
00:11:15,480 --> 00:11:21,880
más relajado y en armonía.

180
00:11:22,880 --> 00:11:27,820
Espero que puedas hacer un zazen completo ahora.

181
00:11:27,820 --> 00:11:31,060
Es decir, una primera parte de la sesión,

182
00:11:31,060 --> 00:11:32,055
entonces,

183
00:11:32,055 --> 00:11:34,855
5 o 10 minutos de esta caminata

184
00:11:35,360 --> 00:11:38,960
y volver a una segunda parte de la meditación.

185
00:11:39,140 --> 00:11:40,300
Muchas gracias.