1
00:00:00,010 --> 00:00:11,610
Los puntos clave son: la pelvis,

2
00:00:11,740 --> 00:00:17,895
la quinta vértebra lumbar,

3
00:00:18,635 --> 00:00:24,900
la primera vértebra dorsal, la que sobresale.

4
00:00:26,940 --> 00:00:31,820
Y luego ves la morfología, todos son diferentes.

5
00:00:31,960 --> 00:00:38,620
Digamos que creo que debería estirar su cuello un poco más.

6
00:00:41,210 --> 00:00:48,145
Siempre tienes que tocar dos puntos.

7
00:00:48,645 --> 00:00:52,262
Excepto cuando es la parte superior del cráneo.

8
00:00:52,602 --> 00:00:55,460
Un yin y un yang.

9
00:00:55,640 --> 00:00:58,675
Me pongo cómodo, me pongo de rodillas.

10
00:00:59,855 --> 00:01:03,020
Voy a tocar esta misma vértebra.

11
00:01:08,280 --> 00:01:09,815
Voy a tocar la barbilla con dos dedos.

12
00:01:09,975 --> 00:01:11,790
No le voy a hacer esto.

13
00:01:14,170 --> 00:01:16,825
Estamos dando indicaciones.

14
00:01:16,855 --> 00:01:19,540
Pero cuando están realmente bien dados,

15
00:01:19,620 --> 00:01:21,635
si dejamos que la persona mantenga su enfoque,

16
00:01:21,865 --> 00:01:24,132
tiene un efecto muy profundo.

17
00:01:25,602 --> 00:01:30,670
Así que voy a empujar los dos puntos opuestos.

18
00:01:30,753 --> 00:01:35,976
También podemos hacerlo así.

19
00:01:36,156 --> 00:01:39,650
Sé que tuvo una operación…

20
00:01:41,560 --> 00:01:43,680
También podemos hacerlo así.

21
00:01:43,785 --> 00:01:46,570
El Sensei [Deshimaru] solía hacer eso mucho.

22
00:01:48,030 --> 00:01:53,480
Puedes golpear un punto específico donde la persona tiene debilidades.

23
00:01:53,740 --> 00:01:56,860
Por ejemplo, al estómago o al hígado.

24
00:02:00,470 --> 00:02:03,900
Siempre mira la curvatura.

25
00:02:06,820 --> 00:02:10,385
A menudo, se le da demasiada o poca importancia.

26
00:02:10,575 --> 00:02:15,140
No olvides que tienes un kyosaku en la mano.

27
00:02:18,340 --> 00:02:23,950
No hay forma de que empecemos a hacer ajustes.

28
00:02:25,680 --> 00:02:30,245
Tienes que mantener tu kyosaku.

29
00:02:32,525 --> 00:02:36,740
Podemos hacer eso con el kyosaku, no molesta.

30
00:02:36,875 --> 00:02:39,895
También podemos hacer eso.

31
00:02:40,010 --> 00:02:45,140
Si la persona está demasiado encorvada.

32
00:02:48,195 --> 00:02:50,880
También podemos hacer eso.

33
00:02:52,790 --> 00:02:56,313
Tienes que generar una corrección que la persona tomará.

34
00:02:56,405 --> 00:02:59,930
Es importante hacer eso también.

35
00:03:04,960 --> 00:03:08,450
Es muy delicado y no perturbo la meditación.

36
00:03:11,440 --> 00:03:13,570
También podemos usar kyosaku.

37
00:03:16,235 --> 00:03:19,825
Bajaremos el lado ancho.

38
00:03:20,330 --> 00:03:23,325
Arrodíllate.

39
00:03:24,216 --> 00:03:27,264
Y vamos a colocarlo en el arco.

40
00:03:29,334 --> 00:03:31,587
Eso te dará una indicación.

41
00:03:32,747 --> 00:03:37,940
Muy negativo, porque sientes que no eres nada recto.

42
00:03:38,150 --> 00:03:41,930
Porque el palo es recto, y la columna no es recta.

43
00:03:48,600 --> 00:03:50,800
La columna nunca es recta.

44
00:03:51,130 --> 00:03:52,995
Tiene una ligera curvatura.

45
00:03:53,175 --> 00:03:59,240
Cuando te pones esto, te sentirás un poco retorcido.

46
00:03:59,400 --> 00:04:06,705
Si lo colocas en tu espalda…

47
00:04:06,955 --> 00:04:09,490
Es un poco espartano.

48
00:04:13,940 --> 00:04:16,660
¡Tienes una postura muy bonita, Cristina!

49
00:04:22,280 --> 00:04:25,430
Tiene una gran postura.

50
00:04:28,960 --> 00:04:32,320
Hay pequeñas cosas de las que no te das cuenta.

51
00:04:32,480 --> 00:04:34,340
Por ejemplo, el pulgar allí.

52
00:04:36,040 --> 00:04:39,820
Debe estar derecho.

53
00:04:41,850 --> 00:04:44,710
Es algo de lo que no se da cuenta.

54
00:04:44,890 --> 00:04:50,520
Pero vas a seguir durante un año, dos años, y tus pulgares estarán bien.

55
00:04:50,855 --> 00:04:53,735
Es lo primero que veo.

56
00:04:55,180 --> 00:04:58,900
Asegúrate de que tus pulgares están justo contra tu ombligo.

57
00:05:10,260 --> 00:05:12,600
Un poco demasiado en el fondo de mi mente.

58
00:05:14,440 --> 00:05:20,300
La cabeza va un poco hacia adelante.

59
00:05:28,920 --> 00:05:32,970
Estoy totalmente metido en la osteopatía.

60
00:05:34,420 --> 00:05:36,260
¡Está bien!

61
00:05:38,315 --> 00:05:42,875
Afloja un poco ahí.

62
00:05:53,292 --> 00:05:54,862
¡Muy bien!

63
00:05:56,060 --> 00:06:00,290
Cuando hagas gassho, hazlo bien así.

64
00:06:04,605 --> 00:06:06,335
La postura es hermosa.

65
00:06:08,920 --> 00:06:11,120
Un poco demasiado abombado.

66
00:06:11,225 --> 00:06:14,765
Salga de aquí.

67
00:06:24,537 --> 00:06:28,737
En ese punto, porque queremos aguantar,

68
00:06:35,023 --> 00:06:37,393
No, no, ¡tranquilo!

69
00:06:37,591 --> 00:06:38,591
¡Aquí!

70
00:06:38,780 --> 00:06:39,780
¡Perfecto!

71
00:06:45,450 --> 00:06:49,410
No olvides que es mano izquierda en mano derecha.

72
00:06:51,200 --> 00:06:52,950
¡Relaja las manos!

73
00:06:53,010 --> 00:06:55,540
Relaja los dedos.

74
00:06:56,150 --> 00:06:58,530
Se las pone una a la otra.

75
00:07:03,395 --> 00:07:06,015
Es un poco tenso, pero aún así…

76
00:07:08,797 --> 00:07:09,947
Aquí.

77
00:07:16,500 --> 00:07:19,185
Lo que haré entonces…

78
00:07:19,275 --> 00:07:21,870
Estoy viendo mucha tensión. Es normal.

79
00:07:21,910 --> 00:07:24,435
Porque al principio, tenemos que hacerlo con fuerza.

80
00:07:24,585 --> 00:07:27,130
Hazlo tú.

81
00:07:30,560 --> 00:07:33,810
Las manos están bastante bien ahora.

82
00:07:34,930 --> 00:07:38,150
Relájate, no tienen que estar muy tensos.

83
00:07:39,690 --> 00:07:40,470
Flexible.

84
00:07:40,560 --> 00:07:42,110
¡Eso es bueno, genial!

85
00:07:48,975 --> 00:07:54,395
Yo pondría un poco de énfasis en la quinta vértebra.

86
00:08:03,997 --> 00:08:07,027
Verás, me siento un poco tenso.

87
00:08:10,253 --> 00:08:12,099
¡Está bien!

88
00:08:12,829 --> 00:08:15,839
Afloja tus codos.

89
00:08:19,849 --> 00:08:27,759
Sensei, no en los principiantes, pero solía hacerlo mucho.

90
00:08:27,894 --> 00:08:31,684
Definitivamente, estaba presionando mucho.

91
00:08:33,682 --> 00:08:37,282
Por ahora, no hay necesidad.

92
00:08:37,941 --> 00:08:39,471
Está bien. Está bien.

93
00:08:40,660 --> 00:08:42,680
Puedes empezar a respirar.

94
00:08:42,790 --> 00:08:44,875
Toma un respiro.

95
00:08:45,065 --> 00:08:46,577
Sigue tu respiración.

96
00:08:46,727 --> 00:08:48,010
Disfrútalo.

97
00:08:54,425 --> 00:08:55,625
Eso es muy bueno.

98
00:08:55,717 --> 00:08:58,987
¿Esta es tu primera sesión?

99
00:09:00,103 --> 00:09:02,613
¡Bravo!

100
00:09:02,853 --> 00:09:04,530
Déjame preguntarte algo:

101
00:09:04,608 --> 00:09:07,268
Para respirar,

102
00:09:07,380 --> 00:09:12,640
a veces me lo trago y no sé cómo parar…

103
00:09:20,830 --> 00:09:22,820
Mientras te sientas bien…

104
00:09:26,310 --> 00:09:29,775
Ese es mi punto, hay otros métodos…

105
00:09:29,935 --> 00:09:32,860
Pero en zazen, para respirar.

106
00:09:33,060 --> 00:09:36,780
Tienes que sentirte bien, no lo fuerces.

107
00:09:37,130 --> 00:09:40,530
Mientras te sientas bien, sigue adelante.

108
00:09:45,380 --> 00:09:48,570
Recuperas el aliento en el momento en que lo sientes.

109
00:09:48,731 --> 00:09:51,461
Es tu cuerpo el que tiene que dictarlo.

110
00:10:25,390 --> 00:10:27,310
La postura en general

111
00:11:05,935 --> 00:11:08,115
¡Está bien!

112
00:11:15,740 --> 00:11:18,100
Tienes que mirar los puntos principales:

113
00:11:18,190 --> 00:11:21,200
quinta vértebra lumbar, primera dorsal,

114
00:11:21,266 --> 00:11:24,790
cabeza, brazos no muy apretados.

115
00:11:26,070 --> 00:11:31,060
Podemos ayudar a la persona con la posición de las manos.

116
00:11:31,240 --> 00:11:34,280
Me gustaría ver una postura de kin-hin.

117
00:11:41,400 --> 00:11:44,925
No hay nada más que una buena postura.

118
00:11:46,485 --> 00:11:50,500
Tiene una postura muy agradable, muy natural.

119
00:11:51,230 --> 00:11:59,200
Excepto por un detalle: el dedo meñique debe estar apretado.

120
00:11:59,785 --> 00:12:01,855
La gente a menudo se olvida de apretarlo.

121
00:12:01,967 --> 00:12:03,822
El Sensei siempre dijo:

122
00:12:03,982 --> 00:12:08,977
los dos dedos pequeños tienen que estar muy apretados.

123
00:12:15,043 --> 00:12:17,623
Incluso te corregí la última vez…

124
00:12:19,766 --> 00:12:22,006
¡Muy bien, bravo!

125
00:12:24,018 --> 00:12:27,238
A menudo hay personas con posturas como esa.

126
00:12:27,424 --> 00:12:28,974
¿Deberían ser corregidos?

127
00:12:29,047 --> 00:12:30,387
Debería ser corregido.

128
00:12:30,510 --> 00:12:33,615
Por ejemplo, Pierre, toma la postura…

129
00:12:33,920 --> 00:12:38,170
He notado algo, veremos si lo haces…

130
00:12:46,127 --> 00:12:50,227
La mano es un poco como esto…

131
00:12:55,366 --> 00:12:57,176
Afloja un poco.

132
00:12:59,735 --> 00:13:04,130
Realmente necesita ser estratificada.

133
00:13:13,740 --> 00:13:15,870
A veces puedes cometer un error.

134
00:13:15,935 --> 00:13:18,195
Si lo arreglamos…

135
00:13:18,317 --> 00:13:20,887
Aprieta un poco los dedos meñiques…

136
00:13:23,093 --> 00:13:27,443
El Sensei siempre mostró energía en ambos dedos.

137
00:13:27,821 --> 00:13:30,621
Lo hacemos así, y luego así.

138
00:13:32,250 --> 00:13:35,010
Y luego este lo ponemos encima.

139
00:13:36,000 --> 00:13:38,040
Bonito y perpendicular.

140
00:13:38,310 --> 00:13:42,130
Cuando abre las manos, están paralelas al suelo.

141
00:13:44,370 --> 00:13:46,580
No quieres terminar así…

142
00:13:47,610 --> 00:13:48,830
Eso es bueno.

143
00:13:48,990 --> 00:13:50,410
Es perfecto.
