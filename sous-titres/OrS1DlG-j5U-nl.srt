1
00:00:00,000 --> 00:00:08,750
Ik doe zazen met een kesa van Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
Zijde.

3
00:00:15,330 --> 00:00:19,215
Ik ben erg blij.

4
00:00:20,235 --> 00:00:22,830
Ik vind Barbara erg leuk.

5
00:00:24,580 --> 00:00:27,210
Ze is een grote heilige.

6
00:00:33,770 --> 00:00:38,190
Gelukkig zijn mijn discipelen in het algemeen beter dan ik.

7
00:00:42,910 --> 00:00:45,830
Vooral Barbara.

8
00:00:48,850 --> 00:00:51,270
En Ariadna ook.

9
00:00:55,710 --> 00:00:58,515
De drie schatten.

10
00:01:01,015 --> 00:01:03,817
Boeddha, dharma, sangha.

11
00:01:06,267 --> 00:01:10,440
zijn onafscheidelijk.

12
00:01:24,820 --> 00:01:28,470
Ik heb je voorbereid als een kusen...

13
00:01:35,340 --> 00:01:41,560
...de voortzetting van wat ik op zomerkamp heb gedaan.

14
00:01:45,170 --> 00:01:50,930
Op de vier fundamenten van magische krachten.

15
00:01:56,750 --> 00:02:01,770
Ik deed dit op zomerkamp...

16
00:02:02,130 --> 00:02:10,530
Ze zeggen dat de vier funderingen als de vier hoeven van een paard zijn.

17
00:02:35,940 --> 00:02:40,120
De eerste is de wilskracht, de wilskracht.

18
00:02:44,460 --> 00:02:50,300
Volitie is iets sterkers dan jij.

19
00:02:52,290 --> 00:02:55,400
En dat dwingt je om te handelen.

20
00:02:58,920 --> 00:03:02,400
Het is een kwestie van overleven.

21
00:03:06,430 --> 00:03:08,350
Het is erg sterk.

22
00:03:09,410 --> 00:03:12,160
Ik heb er op zomerkamp over gesproken.

23
00:03:15,530 --> 00:03:18,550
De tweede is de geest.

24
00:03:20,745 --> 00:03:23,015
De tweede hoef.

25
00:03:27,730 --> 00:03:33,390
Het derde punt is de vooruitgang die wordt geboekt.

26
00:03:38,440 --> 00:03:40,810
De vierde is gedacht.

27
00:03:43,810 --> 00:03:46,855
Dat is wat ik denk.

28
00:03:48,135 --> 00:03:50,020
De tweede.

29
00:03:54,970 --> 00:03:57,515
Ik heb er op zomerkamp over gesproken,

30
00:03:57,625 --> 00:04:00,860
maar ik was nog niet klaar.

31
00:04:03,270 --> 00:04:09,595
Ik ga je drie zen-verhalen vertellen.

32
00:04:11,995 --> 00:04:16,060
Dat maakt de aard van de geest beter te begrijpen.

33
00:04:19,990 --> 00:04:25,860
Meester Eka, een leerling van Bodhidharma...

34
00:04:32,370 --> 00:04:35,760
...staande in de sneeuw...

35
00:04:39,330 --> 00:04:41,145
...ijskoud...

36
00:04:44,635 --> 00:04:47,564
... richtte zich tot zijn meester!

37
00:04:53,470 --> 00:04:57,240
"Meester, mijn geest is niet vredig."

38
00:05:08,460 --> 00:05:12,130
"Ik smeek u, geef hem rust!"

39
00:05:18,940 --> 00:05:25,380
Eka had veel mannen gedood omdat hij in het leger zat.

40
00:05:33,130 --> 00:05:37,370
Hij voelde zich erg schuldig over dit alles.

41
00:05:40,780 --> 00:05:44,100
Hij kon zich niet van zijn schuldgevoel ontdoen.

42
00:05:49,410 --> 00:05:54,485
Hij is in de sneeuw en vraagt Bodhidharma: "Accepteer mij!"

43
00:06:00,245 --> 00:06:02,050
"Als een discipel!"

44
00:06:06,000 --> 00:06:09,410
Bodhidharma geeft geen antwoord.

45
00:06:17,770 --> 00:06:19,615
Hij staat erop.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma beweegt niet.

47
00:06:30,960 --> 00:06:35,420
Uiteindelijk snijdt hij zijn arm af met zijn zwaard.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma zegt, "Nou..."

49
00:06:58,695 --> 00:07:02,350
Bodhidharma grunts: "Han..."

50
00:07:06,620 --> 00:07:09,980
En hij gaat verder met zazen.

51
00:07:14,280 --> 00:07:17,145
Eka staat erop, bedelt:

52
00:07:20,910 --> 00:07:24,630
"Ik smeek u om mijn geest te kalmeren!"

53
00:07:29,190 --> 00:07:31,335
"Het is ondraaglijk!"

54
00:07:32,305 --> 00:07:34,400
"Ik word gek!"

55
00:07:44,290 --> 00:07:48,140
Eindelijk, Bodhidharma geeft hem een kijkje:

56
00:07:54,430 --> 00:07:58,595
"Breng me je geest."

57
00:08:10,745 --> 00:08:13,680
"En ik zal hem vrede geven."

58
00:08:20,300 --> 00:08:22,590
Meester Eka antwoordde hem:

59
00:08:26,210 --> 00:08:29,105
"Meester, ik heb hem gezocht, mijn geest!"

60
00:08:33,665 --> 00:08:37,750
"En hij bleef ontoegankelijk, ongrijpbaar!"

61
00:08:46,240 --> 00:08:49,020
Eka is op zoek naar gemoedsrust.

62
00:08:52,630 --> 00:08:55,050
Hij heeft pijn.

63
00:08:56,520 --> 00:08:59,340
Hij wil rust, stilte.

64
00:09:04,085 --> 00:09:05,490
Het maakt niet uit hoe vaak we het hem vertellen:

65
00:09:05,550 --> 00:09:07,845
"Het is je geest!"

66
00:09:13,950 --> 00:09:16,360
"Waar is het, mijn geest?"

67
00:09:17,805 --> 00:09:19,655
"Waar is hij, mijn geest?"

68
00:09:22,452 --> 00:09:25,182
"Waar is de wortel van deze geest?"

69
00:09:29,400 --> 00:09:32,480
"Wat is de essentie van deze geest?"

70
00:09:35,925 --> 00:09:38,528
"Zit het in de hersenen?"

71
00:09:46,670 --> 00:09:48,510
"Zit het in het hart?"

72
00:09:56,655 --> 00:10:00,025
Zelfs als niet iedereen zijn arm eraf snijdt,

73
00:10:03,270 --> 00:10:07,120
in de sneeuw, in de kou...

74
00:10:08,810 --> 00:10:12,880
Iedereen wil iets vinden over zijn geest.

75
00:10:16,040 --> 00:10:18,570
Iedereen wil vreedzaam zijn.

76
00:10:21,970 --> 00:10:23,691
Verzacht zijn geest.

77
00:10:27,930 --> 00:10:31,515
Eka had een opmerkelijke reactie toen hij zei:

78
00:10:31,645 --> 00:10:35,920
"Ik heb naar hem gezocht, maar hij is ongrijpbaar!"

79
00:10:43,990 --> 00:10:46,030
"Ik kon het niet vinden."

80
00:10:48,615 --> 00:10:53,235
In het Japans heet het "Shin fukatoku".

81
00:11:07,460 --> 00:11:09,885
"Fu" is de ontkenning, "niets".

82
00:11:13,960 --> 00:11:15,870
"Ta": Grijp.

83
00:11:19,270 --> 00:11:21,100
Grijp, vind.

84
00:11:24,230 --> 00:11:28,260
"Toku" is als "Mushotoku".

85
00:11:29,550 --> 00:11:33,470
"Toku" betekent "krijgen".

86
00:11:35,910 --> 00:11:37,930
"Het bereiken van zijn doel."

87
00:11:40,370 --> 00:11:43,320
"Shin fukatoku",

88
00:11:47,710 --> 00:11:51,310
Het is de volledig onvindbare, ongrijpbare geest.

89
00:11:57,710 --> 00:11:58,955
Eka zegt:

90
00:11:59,085 --> 00:12:01,650
"Ik zocht het, maar ik kon het niet vinden."

91
00:12:04,480 --> 00:12:06,670
Dat is een uitstekend antwoord.

92
00:12:09,470 --> 00:12:13,340
Maar Bodhidharma’s is nog mooier.

93
00:12:18,060 --> 00:12:22,330
"Breng me je geest en ik zal hem kalmeren."

94
00:12:29,700 --> 00:12:30,660
Eka zei:

95
00:12:30,720 --> 00:12:32,670
"Ik kan het niet!"

96
00:12:34,760 --> 00:12:37,320
"Niet alleen kan ik hem niet vangen,"

97
00:12:37,400 --> 00:12:40,320
"maar ik kan het niet eens vinden!"

98
00:12:44,610 --> 00:12:46,270
Bodhidharma zegt tegen hem:

99
00:12:48,910 --> 00:12:52,340
"In dat geval is hij al gepacificeerd!"

100
00:13:07,610 --> 00:13:09,520
Verhaal twee:

101
00:13:11,955 --> 00:13:14,575
Ik hou van verhalen.

102
00:13:16,890 --> 00:13:18,750
Meester Obaku.

103
00:13:20,240 --> 00:13:22,330
Baso’s meester.

104
00:13:25,480 --> 00:13:30,240
Geef zijn onderwijs, in het bijzijn van de monniken.

105
00:13:33,405 --> 00:13:35,605
Iemand roept hem:

106
00:13:41,550 --> 00:13:46,580
"De meester moet een groot concentratievermogen hebben."

107
00:13:52,200 --> 00:13:53,985
Meester Obaku antwoordt:

108
00:13:58,735 --> 00:14:02,110
"Concentratie,"

109
00:14:03,950 --> 00:14:07,594
"...of integendeel, afleiding,"

110
00:14:10,310 --> 00:14:14,610
"...je zegt dat deze dingen bestaan?"

111
00:14:18,300 --> 00:14:20,600
"Ik zeg u:"

112
00:14:23,770 --> 00:14:27,100
"Zoek ze, en je zult ze nergens vinden."

113
00:14:32,840 --> 00:14:36,340
"Maar als je me vertelt dat ze niet bestaan,"

114
00:14:40,390 --> 00:14:42,490
"Ik zal je zeggen:"

115
00:14:45,510 --> 00:14:47,390
"wat je ook denkt,"

116
00:14:53,490 --> 00:14:58,050
"je bent constant aanwezig waar je bent!"

117
00:15:08,040 --> 00:15:11,650
Zelfs als je denkt dat je afgeleid bent,

118
00:15:16,330 --> 00:15:20,610
kijk naar de plaats waar de afleiding vandaan komt.

119
00:15:24,850 --> 00:15:27,950
Je zult zien dat het eindelijk geen oorsprong meer heeft.

120
00:15:33,360 --> 00:15:37,260
De mentale toestand van de afleiding die verschijnt...

121
00:15:40,870 --> 00:15:43,490
...gaat nergens heen.

122
00:15:44,920 --> 00:15:46,695
In alle tien de richtingen.

123
00:15:48,305 --> 00:15:50,690
En hij gaat ook nergens anders heen.

124
00:16:10,270 --> 00:16:13,490
In zazen is er geen afleiding of concentratie.

125
00:16:21,040 --> 00:16:23,470
Blijf gewoon stil zitten.

126
00:16:30,800 --> 00:16:33,090
De rest zijn technieken.

127
00:16:35,820 --> 00:16:40,168
Het neemt ons weg van authentieke zazen.

128
00:16:45,129 --> 00:16:46,769
Verhaal drie.

129
00:16:47,119 --> 00:16:49,719
- Ja, ik heb je verwend! -

130
00:17:00,600 --> 00:17:03,750
Het is uit Baso’s tijd in China.

131
00:17:05,760 --> 00:17:18,250
Baso, dit is de leerling van de meester waar ik het net over had, Obaku.

132
00:17:24,410 --> 00:17:28,330
In die tijd leefde er een geleerde

133
00:17:30,950 --> 00:17:34,280
...wiens naam Ryo was.

134
00:17:36,560 --> 00:17:41,090
Hij was beroemd om zijn kennis van het boeddhisme.

135
00:17:45,320 --> 00:17:48,840
Hij heeft zijn leven lang lezingen gegeven.

136
00:17:53,410 --> 00:17:58,790
Het blijkt dat hij op een dag een interview had met Baso.

137
00:18:04,890 --> 00:18:07,160
Baso vroeg het hem:

138
00:18:09,190 --> 00:18:12,770
- Over welke onderwerpen geeft u een lezing?"

139
00:18:15,850 --> 00:18:20,040
- "Op de Geest Sutra, de Hannya Shingyo."

140
00:18:25,360 --> 00:18:28,220
Het wordt ook wel de Hart Soetra genoemd.

141
00:18:32,420 --> 00:18:36,750
Het is niet duidelijk of de geest in de hersenen of het hart.

142
00:18:41,320 --> 00:18:47,240
In "Shingyo" betekent "Shin" Geest, of Hart.

143
00:18:52,600 --> 00:18:55,460
"Gyô" betekent Sutra.

144
00:18:58,090 --> 00:19:00,285
Dus Baso vraagt het hem:

145
00:19:03,825 --> 00:19:07,260
- "Wat gebruik je voor je lezingen?"

146
00:19:09,950 --> 00:19:11,755
Ryo antwoordde:

147
00:19:14,525 --> 00:19:17,527
- "Met de Geest."

148
00:19:19,530 --> 00:19:21,355
Baso zegt tegen hem:

149
00:19:24,685 --> 00:19:30,210
- "De geest, hij is als een echte goede acteur!"

150
00:19:34,480 --> 00:19:39,390
"De betekenis van zijn denken is als de extravagantie van een lolbroek."

151
00:19:45,690 --> 00:19:51,580
"Wat betreft de zes zintuigen die ons laten voelen als in een film in de bioscoop..."

152
00:20:07,270 --> 00:20:10,420
"Deze zes zintuigen zijn leeg!"

153
00:20:12,420 --> 00:20:16,970
"Als een golf zonder echt begin of einde."

154
00:20:21,260 --> 00:20:26,340
"Hoe kon de Geest een lezing geven over een soetra?"

155
00:20:33,590 --> 00:20:38,200
Ryo antwoordde, denkend dat hij slim was:

156
00:20:42,150 --> 00:20:47,880
- "Als de geest het niet kan, kan de niet-geest het misschien wel?"

157
00:20:51,630 --> 00:20:53,315
Baso zegt tegen hem:

158
00:20:55,055 --> 00:20:58,607
- "Ja, precies!"

159
00:21:00,167 --> 00:21:04,150
"De niet-spirit kan totaal de les lezen."

160
00:21:09,050 --> 00:21:13,230
Ryo, die gelooft dat hij tevreden is met zijn antwoord...

161
00:21:16,350 --> 00:21:19,730
...heeft zijn mouwen afgestoft...

162
00:21:25,070 --> 00:21:28,100
...en maakte zich klaar om te vertrekken.

163
00:21:31,390 --> 00:21:33,820
Meester Baso roept hem:

164
00:21:35,920 --> 00:21:38,605
- "Professor!"

165
00:21:39,305 --> 00:21:41,160
Ryo draaide zijn hoofd om.

166
00:21:47,460 --> 00:21:49,115
Baso zegt tegen hem:

167
00:21:49,185 --> 00:21:51,560
- "Wat ben je aan het doen?"

168
00:21:55,630 --> 00:21:58,225
Ryo had een diepe openbaring.

169
00:22:03,905 --> 00:22:07,490
Hij is zich bewust geworden van zijn gebrek aan geest.

170
00:22:10,940 --> 00:22:12,440
En Baso zegt tegen hem:

171
00:22:12,550 --> 00:22:16,000
"Van de geboorte tot de dood, het is zo."

172
00:22:21,750 --> 00:22:24,976
We denken, we doen dingen,

173
00:22:28,400 --> 00:22:30,670
spontaan.

174
00:22:33,320 --> 00:22:36,315
Ryo wilde buigen voor Baso,

175
00:22:42,215 --> 00:22:44,840
maar deze zegt tegen hem:

176
00:22:46,660 --> 00:22:51,220
"Geen behoefte om dwaze pretenties te maken!"

177
00:23:02,150 --> 00:23:07,280
Ryo’s hele lichaam was bedekt met zweet.

178
00:23:16,080 --> 00:23:19,360
Hij ging terug naar zijn tempel.

179
00:23:21,260 --> 00:23:24,295
En hij zei tegen zijn discipelen:

180
00:23:27,575 --> 00:23:31,645
"Ik dacht dat als ik sterf, we zouden kunnen zeggen,"

181
00:23:37,670 --> 00:23:41,440
"Dat ik de beste lezingen had gegeven..."

182
00:23:44,650 --> 00:23:47,170
"...op Zen."

183
00:23:52,430 --> 00:23:55,830
"Vandaag stelde ik Baso een vraag."

184
00:24:00,250 --> 00:24:03,710
"En hij heeft de mist van mijn hele leven opgeruimd."

185
00:24:10,790 --> 00:24:14,400
Ryo gaf het lesgeven op.

186
00:24:18,180 --> 00:24:21,125
En trok zich terug in het hart van de bergen.

187
00:24:24,475 --> 00:24:28,290
En we hebben nooit meer iets van hem gehoord.

188
00:25:15,750 --> 00:25:21,950
Waarom is de geest zo moeilijk te vangen?

189
00:25:24,620 --> 00:25:25,755
Waarom?

190
00:25:35,090 --> 00:25:39,490
Omdat gedachte, bewustzijn...

191
00:25:44,230 --> 00:25:46,680
...en zelfs de realiteit...

192
00:26:00,830 --> 00:26:02,620
...verschijnt soms...

193
00:26:06,380 --> 00:26:08,210
...verdwijnt soms.

194
00:26:10,690 --> 00:26:16,570
Soms verschijnt, soms verdwijnt...

195
00:26:26,840 --> 00:26:30,275
Als ze verdwijnt, weten we niet waar ze is.

196
00:26:32,895 --> 00:26:35,532
Maar waar ze is...

197
00:26:37,512 --> 00:26:39,260
...ze is daar.

198
00:26:42,620 --> 00:26:44,475
Ze is daar.

199
00:26:53,205 --> 00:26:56,900
Waar zij niet is, is zij er ook.

200
00:27:04,820 --> 00:27:09,140
Tijd en bewustzijn zijn niet lineair.

201
00:28:08,350 --> 00:28:10,574
Wat de afleiding betreft...

202
00:28:19,220 --> 00:28:24,070
Toen ik een kind was, vertelden ze me altijd...
dat ik niet gefocust was.

203
00:28:37,820 --> 00:28:39,365
Afgeleid.

204
00:28:42,805 --> 00:28:45,670
Maar toen ik speelde...

205
00:28:47,257 --> 00:28:50,930
Toen ik met mijn kleine auto’s speelde...

206
00:28:55,110 --> 00:28:58,880
...ik was volledig gefocust.

207
00:29:06,020 --> 00:29:11,980
De Hannya Shingyo en de ceremonie zijn opgedragen aan jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
En Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra van de grote wijsheid die het mogelijk maakt om verder te gaan dan

210
00:30:12,312 --> 00:30:17,006
De Bodhisattva van de Ware Vrijheid,

211
00:30:17,196 --> 00:30:21,144
door de diepgaande praktijk van Grote Wijsheid,

212
00:30:21,264 --> 00:30:25,022
begrijpt dat het lichaam en de vijf skandah niets anders zijn dan zuigers.

213
00:30:25,152 --> 00:30:30,151
en door dat begrip, helpt hij iedereen die lijdt.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, verschijnselen zijn niet anders dan ku.

215
00:30:34,970 --> 00:30:39,527
Ku is niet anders dan abnormaal.

216
00:30:39,987 --> 00:30:42,996
Fenomenen worden ku.

217
00:30:43,196 --> 00:30:51,305
Ku wordt een fenomeen (vorm is leegte, leegte is vorm),

218
00:30:51,575 --> 00:30:56,155
de vijf skanda zijn ook fenomenen.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, alle bestaan heeft het karakter van ku,

220
00:31:03,451 --> 00:31:08,429
er is geen geboorte en geen begin,

221
00:31:08,989 --> 00:31:14,707
geen puurheid, geen vlekje, geen groei, geen verval.

222
00:31:14,977 --> 00:31:20,130
Daarom is er in ku geen vorm, geen skanda,

223
00:31:20,939 --> 00:31:26,080
geen ogen, geen oren, geen neus, geen tong, geen lichaam, geen geweten;

224
00:31:26,491 --> 00:31:32,397
er is geen kleur, geluid, geur, smaak, aanraking of gedachte;

225
00:31:32,527 --> 00:31:37,915
er is geen kennis, geen onwetendheid, geen illusie, geen stopzetting van het lijden,

226
00:31:38,225 --> 00:31:43,283
er is geen kennis, geen winst, geen non-profit.

227
00:31:43,413 --> 00:31:48,649
Voor de Bodhisattva, dankzij deze Wijsheid die verder gaat,

228
00:31:48,969 --> 00:31:54,622
Er bestaat niet zoiets als angst of vrees.

229
00:31:54,882 --> 00:32:02,024
Alle illusies en bevestigingen worden verwijderd

230
00:32:02,504 --> 00:32:11,116
en hij kan het ultieme einde van het leven in beslag nemen, Nirvana.

231
00:32:12,325 --> 00:32:16,407
Alle Boeddha’s van het verleden, het heden en de toekomst...

232
00:32:16,497 --> 00:32:21,489
kan bereiken tot het begrip van deze Opperste Wijsheid...

233
00:32:21,569 --> 00:32:25,021
die lijden bezorgt, de Satori,

234
00:32:25,081 --> 00:32:30,329
door deze bezwering, onvergelijkbaar en ongeëvenaard, authentiek,

235
00:32:30,432 --> 00:32:36,163
die al het lijden wegneemt en je in staat stelt om de realiteit te vinden, de ware ku:

236
00:32:38,113 --> 00:32:52,170
"Ga, ga, ga samen verder dan het hiernamaals op de oever van de satori."

237
00:33:01,240 --> 00:33:14,420
Hoeveel wezens er ook zijn, ik beloof dat ik ze allemaal zal redden.

238
00:33:14,600 --> 00:33:23,095
Hoeveel hartstochten er ook zijn, ik beloof dat ik ze allemaal zal verslaan.

239
00:33:25,635 --> 00:33:34,292
Het maakt niet uit hoeveel Dharmas er zijn, ik beloof dat ik ze allemaal zal verwerven.

240
00:33:35,112 --> 00:33:43,700
Hoe perfect een Boeddha ook is, ik beloof dat ik er een zal worden.

241
00:33:44,085 --> 00:33:48,247
Dat de verdiensten van deze voordracht

242
00:33:48,247 --> 00:33:52,768
doordringen tot alle wezens overal,

243
00:33:52,768 --> 00:33:57,538
zodat we allemaal voelende wezens zijn,

244
00:33:57,538 --> 00:34:03,129
kunnen we samen het pad van de Boeddha realiseren.

245
00:34:05,332 --> 00:34:16,597
Alle Boeddha’s verleden, heden en toekomst in de tien richtingen.

246
00:34:17,517 --> 00:34:26,629
Alle Bodhisattva’s en patriarchen

247
00:34:27,869 --> 00:34:38,142
De Soetra van de ’Wijsheid die voorbij gaat’...

248
00:34:53,546 --> 00:34:58,216
Sanpaï!

249
00:37:25,590 --> 00:37:28,060
Whew!

250
00:37:46,750 --> 00:37:48,965
Hallo, iedereen!

251
00:37:57,955 --> 00:38:03,310
Ik hoop dat je blij bent dat de pandemie voorbij is!

252
00:38:32,700 --> 00:38:34,600
Hola Paola!
