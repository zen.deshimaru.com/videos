1
00:00:08,980 --> 00:00:16,230
Hello ! I’m going to explain to you how to sit down to practice Zen meditation,
2
00:00:17,200 --> 00:00:19,180
called zazen.

3
00:00:19,180 --> 00:00:22,829
The material needed is very simple, a human being

4
00:00:23,320 --> 00:00:28,349
and a meditation cushion. If you have a cushion

5
00:00:29,230 --> 00:00:33,689
rather padded, it’s very good and if not, you make one with a cover

6
00:00:35,320 --> 00:00:39,060
or a sleeping bag for example, it must be firm enough.

7
00:00:57,490 --> 00:01:04,120
Let me explain the Zen meditation posture, Zen meditation.

8
00:01:05,560 --> 00:01:08,940
Above all to know, that it is a posture

9
00:01:09,440 --> 00:01:10,880
assise

10
00:01:11,240 --> 00:01:13,440
motionless and silent.

11
00:01:14,120 --> 00:01:16,660
The particularity of this posture

12
00:01:17,300 --> 00:01:19,300
is that we practice it

13
00:01:19,400 --> 00:01:21,400
facing the wall.

14
00:01:21,800 --> 00:01:27,460
Here, of course, I stand in front of the camera to teach it to you.

15
00:01:27,960 --> 00:01:34,640
What you have to do is sit on a cushion, as I explained, which must be

16
00:01:35,000 --> 00:01:37,000
sufficiently padded, sufficiently hard.

17
00:01:37,300 --> 00:01:44,060
You sit well at the bottom of the cushion, do not sit on the edge you can slide,

18
00:01:44,440 --> 00:01:48,360
and you should cross your legs as much as possible.

19
00:01:48,820 --> 00:01:56,220
You can cross them like this, like this, like this, it’s called half-lotus,

20
00:01:56,660 --> 00:01:59,410
if you put the other leg here it’s called the lotus.

21
00:02:02,860 --> 00:02:07,620
The important thing is that your knees go to the ground,

22
00:02:08,220 --> 00:02:09,660
to support,

23
00:02:10,789 --> 00:02:13,420
to exert knee pressure on the floor.

24
00:02:13,880 --> 00:02:20,079
So, how do we do that? Do it without any physical effort,

25
00:02:20,720 --> 00:02:25,240
no muscles at all. The posture of Zen meditation, zazen,

26
00:02:25,900 --> 00:02:27,780
it is a posture where one must let go.

27
00:02:28,300 --> 00:02:33,000
So that your knees go to the ground

28
00:02:33,920 --> 00:02:37,360
you will tilt the pelvis slightly

29
00:02:38,440 --> 00:02:42,220
forward, the top of the pelvis goes forward,

30
00:02:42,760 --> 00:02:46,300
the coccyx goes a little bit behind.

31
00:02:46,760 --> 00:02:48,760
I show it to you in profile

32
00:02:55,700 --> 00:03:03,040
My pelvis is not put like this, as if I am driving a car for example,

33
00:03:03,260 --> 00:03:08,700
you see there my knees go up behind because my center of gravity

34
00:03:08,900 --> 00:03:15,700
went behind my body. I want my center of gravity to go in front and I do that with my pelvis.

35
00:03:15,940 --> 00:03:24,300
Naturally, the center of gravity passes in front with gravity, my knees go towards the ground.

36
00:03:24,300 --> 00:03:30,860
That’s what you have to start by establishing in the posture,

37
00:03:31,160 --> 00:03:37,040
a seat in which your knees go to the floor.

38
00:03:37,480 --> 00:03:40,200
When you made this seating

39
00:03:40,800 --> 00:03:45,500
from the pelvis which is tilted slightly forward

40
00:03:45,900 --> 00:03:49,280
you will straighten the spine as much as possible

41
00:03:50,200 --> 00:03:52,860
to the top of the head.

42
00:03:54,580 --> 00:04:02,780
We will consider that the column goes all the way to the top. As if I put a hook

43
00:04:05,060 --> 00:04:10,500
and hop, I stand up again. That means I stretch the neck, I tuck in my chin,

44
00:04:11,150 --> 00:04:15,699
See I don’t have my head falling down in front of me, I don’t have a bent back.

45
00:04:16,849 --> 00:04:18,849
I am as straight as possible.

46
00:04:19,520 --> 00:04:22,000
I show you in profile

47
00:04:23,300 --> 00:04:28,040
I’m not like this, I’m not like this

48
00:04:28,560 --> 00:04:35,780
or leaning forward or whatever, I’m as straight as I can be.

49
00:04:38,800 --> 00:04:43,820
When you have managed to settle down straight, you don’t move.

50
00:04:45,320 --> 00:04:47,160
I continue

51
00:04:47,380 --> 00:04:49,280
the left hand

52
00:04:49,540 --> 00:04:51,540
the fingers are joined together

53
00:04:52,250 --> 00:04:57,220
the fingers of the right hand are joined together and I will superimpose my fingers of the left hand.

54
00:04:58,070 --> 00:05:00,070
on the fingers of the right hand

55
00:05:01,490 --> 00:05:03,680
My thumbs meet

56
00:05:05,730 --> 00:05:08,329
Superimposed on the fingers of the left hand,

57
00:05:09,630 --> 00:05:11,630
I form a horizontal line.

58
00:05:12,690 --> 00:05:20,059
The sharpness of my hands from the wrist to there, the edge of my hands

59
00:05:21,030 --> 00:05:23,929
you will apply it to the base of the abdomen.

60
00:05:25,280 --> 00:05:27,820
to do so,

61
00:05:29,580 --> 00:05:33,440
use something that will allow you to wedge your hands together

62
00:05:34,160 --> 00:05:36,280
in such a way that you can

63
00:05:36,700 --> 00:05:38,620
release completely

64
00:05:38,880 --> 00:05:43,300
shoulder tension the posture is a posture where you have to let go

65
00:05:44,280 --> 00:05:50,089
release the tension, you see my elbows are free I’ve been like this or that for a long time,

66
00:05:51,380 --> 00:05:53,940
quietly. But I don’t make an effort

67
00:05:54,540 --> 00:06:00,940
to maintain my hands, the effort it is to maintain the horizontal thumbs.

68
00:06:01,220 --> 00:06:03,220
once I realized

69
00:06:03,750 --> 00:06:05,750
these points of posture

70
00:06:06,600 --> 00:06:08,600
I don’t move anymore,

71
00:06:09,160 --> 00:06:11,160
This is the physical posture.

72
00:06:20,060 --> 00:06:27,420
After this first part on the physical posture, the second part: the mental attitude.

73
00:06:28,380 --> 00:06:32,340
Once I am in a Zen meditation posture, zazen

74
00:06:33,620 --> 00:06:37,960
facing the wall, what do I do, what should I do?

75
00:06:39,720 --> 00:06:42,260
In the silence of this posture,

76
00:06:43,770 --> 00:06:48,380
you’re going to realize that you’re thinking,

77
00:06:49,350 --> 00:06:51,769
or even that you can’t stop thinking

78
00:06:52,290 --> 00:06:57,469
and that there are thousands of thoughts coming, coming, coming.

79
00:06:57,980 --> 00:07:02,420
You’re going to realize that this is a natural phenomenon.

80
00:07:03,500 --> 00:07:05,500
You can’t control.

81
00:07:05,940 --> 00:07:11,900
What we are going to do, during Zen meditation, we are going to let our thoughts pass.

82
00:07:12,740 --> 00:07:15,580
that is to say, in this immobility,

83
00:07:16,040 --> 00:07:20,440
we will be able to observe what we think,

84
00:07:21,040 --> 00:07:25,960
you will observe your thoughts and these thoughts instead of maintaining them,

85
00:07:26,360 --> 00:07:32,860
instead of growing them, you’re going to let them pass.

86
00:07:33,160 --> 00:07:39,220
So it’s not a question of refusing anything, again it’s a question of letting go, letting pass.

87
00:07:40,080 --> 00:07:42,400
How to let go through

88
00:07:43,260 --> 00:07:45,260
attention, concentration,

89
00:07:46,080 --> 00:07:47,860
right here, right now,

90
00:07:48,240 --> 00:07:50,240
on your breathing.

91
00:07:55,930 --> 00:07:58,680
Breathing in Zen is abdominal breathing.

92
00:08:00,460 --> 00:08:08,320
You breathe through, through the nostril and you breathe in softly,

93
00:08:09,640 --> 00:08:13,780
slowly, obviously without noise, silent meditation.

94
00:08:15,180 --> 00:08:20,180
The characteristic of this breathing is that you will focus your attention

95
00:08:20,540 --> 00:08:24,120
on the exhalation side.

96
00:08:24,420 --> 00:08:26,420
We inspire

97
00:08:26,620 --> 00:08:33,680
by the nose, quietly, be generous, fill up

98
00:08:34,300 --> 00:08:41,880
and the exhalation side when you’re going to exhale all the air you do it slowly,

99
00:08:42,340 --> 00:08:46,940
slowly, as if to become longer and longer

100
00:08:56,700 --> 00:09:00,140
When you are at the end of your expiration

101
00:09:00,880 --> 00:09:02,380
you aspire

102
00:09:02,850 --> 00:09:06,340
and again you expire

103
00:09:07,520 --> 00:09:12,020
and you put more time, slowly, deeper,

104
00:09:12,200 --> 00:09:15,760
a deeper exhalation and again like the waves

105
00:09:16,280 --> 00:09:18,280
on an ocean

106
00:09:18,560 --> 00:09:20,560
arriving on the beach

107
00:09:22,580 --> 00:09:24,580
All this with the nose.

108
00:09:30,060 --> 00:09:33,100
If you focus here and now

109
00:09:33,400 --> 00:09:40,880
on the points of the posture, the physical points therefore the attention, an attention here and now in your body

110
00:09:41,420 --> 00:09:48,960
if you concentrate here and now, at the same time, on not following your thoughts,

111
00:09:49,820 --> 00:09:52,640
returning to the points that the posture,

112
00:09:52,980 --> 00:09:55,880
if you concentrate here and now

113
00:09:56,460 --> 00:10:02,320
on establishing a breathing, a harmonious coming and going and little by little

114
00:10:02,900 --> 00:10:06,180
than the time of expiration,

115
00:10:06,800 --> 00:10:10,620
you go out the air becomes longer and longer

116
00:10:11,600 --> 00:10:16,240
You won’t have time to think about ... what you want!

117
00:10:16,800 --> 00:10:22,940
You are here and now in a zazen posture.

118
00:10:23,520 --> 00:10:30,360
In posture, neither the body nor the material. I remind you to press,

119
00:10:30,660 --> 00:10:34,420
you apply pressure from the knees to the floor,

120
00:10:34,680 --> 00:10:38,840
you push the earth with your knees

121
00:10:38,900 --> 00:10:40,980
and you straighten up

122
00:10:41,500 --> 00:10:43,500
as much as possible the spine

123
00:10:44,089 --> 00:10:48,999
to the top of the skull as if to push the sky with the head.

124
00:10:49,760 --> 00:10:55,270
The left hand in the right hand the horizontal thumbs the edge of the hands

125
00:10:55,780 --> 00:10:58,440
against the lower abdomen.

126
00:10:58,840 --> 00:11:00,820
I am motionless

127
00:11:01,190 --> 00:11:03,160
facing the wall

128
00:11:03,170 --> 00:11:05,170
in front of the wall there is nothing to see especially,

129
00:11:05,360 --> 00:11:11,169
What I’m going to do instead is turn my gaze inward,

130
00:11:11,959 --> 00:11:13,959
so the senses,

131
00:11:14,410 --> 00:11:16,959
I will no longer use them during this meditation.

132
00:11:17,900 --> 00:11:23,360
I’m facing the wall, so there’s nothing to see, so it’s like

133
00:11:24,080 --> 00:11:28,640
I take my sight, my vision, I take it, I put it in front of me, I don’t take care of it anymore.

134
00:11:29,020 --> 00:11:33,180
During meditation, you will meditate with sight,

135
00:11:34,160 --> 00:11:37,240
45° diagonally downwards

136
00:11:38,300 --> 00:11:46,920
You can meditate with your eyes closed, but if you keep your eyes half-open you keep in touch with reality.

137
00:11:47,340 --> 00:11:49,980
Don’t go especially in your thoughts.

138
00:11:50,320 --> 00:11:54,380
Since we are in silence, we do not use our hearing either,

139
00:11:54,600 --> 00:11:58,980
so there’s no need for hearing, there’s no music, there’s nothing.

140
00:11:59,260 --> 00:12:03,740
and we’ve already seen that we’re immobile.

141
00:12:04,260 --> 00:12:09,960
Little by little, suddenly we cut with the demands of the outside world.

142
00:12:10,400 --> 00:12:12,680
we’re getting rid of it all.

143
00:12:13,140 --> 00:12:18,920
In meditation, you are sitting physically, it is the body that is in posture,

144
00:12:19,910 --> 00:12:26,350
you cut yourself off from the outside, you put everything in airplane mode.

145
00:12:26,920 --> 00:12:33,740
Do not meditate with your cell phone next to you.

146
00:13:09,580 --> 00:13:12,000
To finish a tip for beginners,

147
00:13:12,660 --> 00:13:17,740
it’s not easy to be like this, confined now,

148
00:13:18,140 --> 00:13:21,720
meditate in front of a wall,

149
00:13:22,820 --> 00:13:25,940
but, it’s really a good help

150
00:13:26,220 --> 00:13:28,640
I really advise you to practice it,

151
00:13:28,900 --> 00:13:36,940
to try it and you will see that instead of having thoughts that may not be very bright,

152
00:13:37,260 --> 00:13:43,920
not very cheerful, instead of being in a bad mood, instead of feeling tension, stress,

153
00:13:44,560 --> 00:13:48,569
all this is normal, with this work on the body

154
00:13:49,200 --> 00:13:55,500
the attitude of the mind you let go, you take distance from everything that happens to you

155
00:13:55,940 --> 00:13:58,240
you will see that you will calm down

156
00:13:58,560 --> 00:13:59,900
physically

157
00:14:00,100 --> 00:14:01,480
emotionally

158
00:14:01,660 --> 00:14:04,820
the breathing, all that, it will help you to let go.

159
00:14:05,280 --> 00:14:09,880
It will help you and your family deeply if you live with someone.

160
00:14:10,340 --> 00:14:14,480
So don’t hesitate to practice it. Thank you.
