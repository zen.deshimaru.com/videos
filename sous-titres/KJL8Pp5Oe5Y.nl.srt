1
00:01:31,170 --> 00:01:35,220
In Soto Zen, dat we oefenen, zeggen we: "Zoek geen grote verlichting". 

2
00:01:35,560 --> 00:01:38,407
Verlichting bestaat op zichzelf niet. 

3
00:01:38,407 --> 00:01:40,170
Waarom zou ze bestaan? 

4
00:01:40,780 --> 00:01:45,720
Je moet naar de natuur en de dingen kijken zoals ze zijn om de waarheid te zien. 

5
00:01:46,200 --> 00:01:49,663
Verlichting is absoluut natuurlijk. 

6
00:01:49,663 --> 00:01:52,780
Het is geen bijzondere voorwaarde. 

7
00:01:53,320 --> 00:01:59,400
In Zen is er geen speciale voorwaarde voor verlichting, tenzij er een ziekte van de geest is. 

8
00:01:59,700 --> 00:02:04,760
Er is op zich geen speciale verlichting en men moet er geen zoeken. 

9
00:02:05,540 --> 00:02:07,433
Veel meesters staan ​​erop. 

10
00:02:07,433 --> 00:02:08,681
Dit is buitengewoon belangrijk. 

11
00:02:08,681 --> 00:02:11,000
Verlichting is op elk moment, bij elke zazen. 

12
00:02:11,320 --> 00:02:15,580
Ik sprak met een specialist in psychotrope drugs … 

13
00:02:15,790 --> 00:02:20,700
ontworpen om speciale bewustzijnstoestanden te bereiken. 

14
00:02:21,130 --> 00:02:24,686
Hij beweerde dat zazen een middel was om verlichting te bereiken. 

15
00:02:24,686 --> 00:02:25,889
Dat klopt helemaal niet. 

16
00:02:26,350 --> 00:02:31,379
Zazen is zelf de verlichting. 

17
00:02:32,410 --> 00:02:33,254
Dogen zegt: 

18
00:02:33,254 --> 00:02:36,269
’Ik ging naar China en ontmoette een echte meester.’

19
00:02:37,390 --> 00:02:41,729
’Wat ik ervan heb geleerd, is dat de ogen horizontaal zijn en de neus verticaal.’

20
00:02:42,370 --> 00:02:45,810
’En dat je niet voor de gek gehouden moet worden, door jezelf of door anderen.’

21
00:03:44,750 --> 00:03:46,750
en elke spier in de buik 

22
00:03:47,269 --> 00:03:52,358
moet ontspannen zijn om de indruk te wekken dat de darmen bezwaren. 

23
00:04:09,010 --> 00:04:15,420
als we ons op dat moment volledig in de houding concentreren, kunnen we de gedachten laten 

24
00:04:15,850 --> 00:04:17,850
zonder daar te stoppen. 

25
00:04:25,810 --> 00:04:28,914
Het mooiste wat je met je lichaam kunt doen is zazen. 

26
00:04:28,914 --> 00:04:30,419
Dit is de pracht van de natuur. 

27
00:04:30,849 --> 00:04:33,728
Het inspireert rust, harmonie, kracht. 

28
00:04:33,728 --> 00:04:36,539
Alles zit vervat in de houding van zazen 

29
00:04:39,969 --> 00:04:43,447
Tijdens het zomerkamp probeer ik de praktijk aan te moedigen. 

30
00:04:43,447 --> 00:04:46,111
De sfeer in de dojo, de houdingen. 

31
00:04:46,111 --> 00:04:47,909
Iemand die dat verstoort 

32
00:04:50,540 --> 00:04:53,940
met haar persoonlijke geschiedenis ben ik bereid haar te vermoorden. 

33
00:04:57,120 --> 00:04:59,120
Het is de enige regel die ik ken: medeleven. 

34
00:05:00,600 --> 00:05:02,477
Sensei liet zijn kyosaku zien: 

35
00:05:02,477 --> 00:05:04,160
"de staf van mededogen!" 

36
00:05:04,660 --> 00:05:11,080
Soms schoot hij twintig keer achter elkaar. 

37
00:05:14,440 --> 00:05:19,640
Hij zei dan: ’Sensei, heel medelevend!’

38
00:05:26,280 --> 00:05:32,940
Als een discipel geholpen wil worden in zijn meditatie door met een stok op de schouder geslagen te worden, vraagt ​​hij erom. 

39
00:05:33,180 --> 00:05:36,800
Omdat hij voelt dat zijn aandacht begint af te nemen. 

40
00:05:37,140 --> 00:05:41,360
Als u zich slaperig voelt. 

41
00:05:41,580 --> 00:05:46,300
Of wanneer u complicaties voelt die uw hersenen vertroebelen. 

42
00:05:46,540 --> 00:05:49,520
Niet zo stil meer. 

43
00:05:49,940 --> 00:05:54,600
Als u zich slaperig voelt of als uw geest rusteloos is … 

44
00:06:01,240 --> 00:06:03,240
Gasshô! 

45
00:06:21,580 --> 00:06:23,580
Zazen gaat door tijdens 

46
00:06:23,840 --> 00:06:25,233
meerdere uren per dag. 

47
00:06:25,233 --> 00:06:25,929
En soms dag en nacht. 

48
00:06:26,180 --> 00:06:30,291
Maar tijdens het lopen wordt hij onderbroken door een meditatie. 

49
00:06:30,291 --> 00:06:33,489
Dat wil zeggen, de innerlijke staat blijft, 

50
00:06:34,280 --> 00:06:38,799
maar de houding is anders. 

51
00:06:39,940 --> 00:06:44,040
Alle aandacht is gericht op de duim. 

52
00:06:44,600 --> 00:06:47,500
Elke ademhaling komt overeen met een halve stap. 

53
00:06:58,880 --> 00:07:05,020
Ogen zijn 3 meter vooraan op de grond. 

54
00:07:13,340 --> 00:07:18,580
Het is dezelfde ademhaling als bij zazen, tijdens de meditatie in zithouding. 

55
00:07:20,740 --> 00:07:28,300
Zen-ademhaling is het tegenovergestelde van normale ademhaling. 

56
00:07:28,560 --> 00:07:30,940
Als je iemand vraagt ​​om te ademen, 

57
00:07:31,240 --> 00:07:34,600
de reflex is om diep adem te halen. 

58
00:07:35,060 --> 00:07:38,860
In zen gaan we focussen 

59
00:07:39,080 --> 00:07:42,740
vooral op de uitademing. 

60
00:07:43,040 --> 00:07:46,780
De uitademing is lang, kalm en diep. 

61
00:07:54,360 --> 00:07:56,160
Het is verankerd in de grond. 

62
00:07:56,160 --> 00:07:58,860
Het kan tot één, twee minuten duren. 

63
00:07:59,080 --> 00:08:02,940
Afhankelijk van zijn concentratie en kalmte. 

64
00:08:03,280 --> 00:08:09,600
Het kan heel zacht, onmerkbaar zijn. 

65
00:08:09,860 --> 00:08:15,640
We bestuderen deze ademhaling eerst door erop te staan, als een koe die loeit. 

66
00:08:38,020 --> 00:08:44,640
Aan het einde van de uitademing laten we de inspiratie automatisch plaatsvinden. 

67
00:08:45,880 --> 00:08:49,640
We ontspannen alles, openen de longen. 

68
00:08:50,300 --> 00:08:55,380
We laten de inspiratie automatisch komen. 

69
00:08:55,820 --> 00:08:58,689
We zetten niet veel intenties in inspiratie. 

70
00:08:59,600 --> 00:09:03,129
Door dit te doen, zult u het begrijpen 

71
00:09:04,550 --> 00:09:06,550
die ademhaling is alles. 

72
00:09:07,910 --> 00:09:09,910
Het is een cirkel. 

73
00:09:10,070 --> 00:09:12,070
Meesters tekenen vaak een cirkel. 

74
00:09:12,560 --> 00:09:14,560
Het gaat over ademen. 

75
00:09:15,040 --> 00:09:18,880
Die ademhaling had veel invloed, 

76
00:09:19,340 --> 00:09:23,200
vooral in de vechtsport. 

77
00:09:23,480 --> 00:09:29,160
In Japanse vechtsporten wordt Zen-ademhaling gebruikt. 

78
00:09:29,580 --> 00:09:34,840
Veel grote vechtsportmeesters zoals Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
beoefende Zen en werd discipelen van grote meesters. 

80
00:09:39,940 --> 00:09:45,480
En hebben de leer van zen en ademhaling aangepast aan hun krijgskunst. 

81
00:09:45,820 --> 00:09:53,860
Uiteindelijk gaat de ademhaling de technieken te boven en is volledig gratis. 

82
00:09:58,080 --> 00:10:03,340
De leer van de Boeddha aan zijn discipelen is heel eenvoudig. 

83
00:10:03,640 --> 00:10:09,440
Het is gebaseerd op de fundamentele handelingen van de mens. 

84
00:10:09,660 --> 00:10:11,260
Hoe te bewegen. 

85
00:10:11,480 --> 00:10:14,120
Hoe te eten, hoe te ademen. 

86
00:10:14,440 --> 00:10:15,836
Hoe te denken. 

87
00:10:15,836 --> 00:10:20,860
En tot slot, hoe te gaan zitten om zijn goddelijkheid te realiseren. 

88
00:11:24,940 --> 00:11:28,702
Genmai is sinds Shakyamuni Boeddha het voedsel van monniken. 

89
00:11:28,702 --> 00:11:31,800
We mengen de groenten die we bij de hand hebben met rijst. 

90
00:11:32,280 --> 00:11:39,440
We koken heel lang, met veel aandacht en concentratie. 

91
00:11:39,780 --> 00:11:42,730
Het is een voedsel dat van de Boeddha is doorgegeven. 

92
00:11:42,730 --> 00:11:45,920
In China en Japan eten we hetzelfde voedsel. 

93
00:11:47,660 --> 00:11:49,980
Het is echt het lichaam van de Boeddha. 

94
00:11:50,280 --> 00:11:53,080
Als we op het punt staan ​​te eten, 

95
00:11:53,440 --> 00:11:55,560
je moet jezelf de vraag stellen: 

96
00:11:55,900 --> 00:11:59,460
’Waarom ga ik eten?’

97
00:11:59,680 --> 00:12:01,580
Een dier of een prut 

98
00:12:01,960 --> 00:12:05,680
zal het zich niet afvragen en op en neer springen op zijn bord. 

99
00:12:06,040 --> 00:12:09,820
Het is duidelijk om te overleven, maar waarvoor? 

100
00:12:10,060 --> 00:12:13,520
Dat staat in de soetra’s die tijdens de maaltijden worden gezongen. 

101
00:12:13,780 --> 00:12:15,240
’Waarom eten we?’

102
00:12:15,520 --> 00:12:17,591
’Waar komt dit voedsel vandaan? 

103
00:12:17,800 --> 00:12:18,860
Wie heeft het voorbereid? " 

104
00:12:19,180 --> 00:12:21,140
’Wie heeft de rijst en groenten verbouwd?’

105
00:12:21,400 --> 00:12:26,140
’Wie heeft er gekookt?’

106
00:12:26,500 --> 00:12:29,960
We wijden de maaltijd en danken al deze mensen. 

107
00:12:30,240 --> 00:12:32,140
En in volledig bewustzijn, 

108
00:12:32,620 --> 00:12:37,340
Ik besluit dit eten te eten 

109
00:12:37,780 --> 00:12:44,620
de evolutie die ik ben gaan volbrengen op deze aarde, in deze materiële wereld. 

110
00:12:52,360 --> 00:12:56,900
EMS is om 10:30 uur. 6 personen voor de keuken. 

111
00:13:01,080 --> 00:13:04,080
Twee mensen voor de grote duik. 

112
00:13:05,940 --> 00:13:10,340
Drie mensen voor de dienst. 

113
00:13:19,600 --> 00:13:23,580
Samu doet niet speciaal metselwerk, veegt op … 

114
00:13:24,190 --> 00:13:27,989
of doe de afwas. 

115
00:13:28,540 --> 00:13:30,540
Samu is allemaal actie 

116
00:13:30,760 --> 00:13:32,939
(Een boek schrijven is een samu) 

117
00:13:35,320 --> 00:13:37,320
uitgevoerd zonder 

118
00:13:37,640 --> 00:13:38,860
voornemen 

119
00:13:39,180 --> 00:13:40,160
verborgen 

120
00:13:44,950 --> 00:13:47,249
geen geheime motieven. 

121
00:13:47,980 --> 00:13:50,048
Het is net als je werk in het dagelijks leven. 

122
00:13:50,048 --> 00:13:52,289
Je kunt het als persoonlijke verrijking beschouwen 

123
00:13:52,839 --> 00:13:57,749
of als een soort geschenk dat je tegelijkertijd aan jezelf en aan iedereen geeft. 

124
00:13:59,320 --> 00:14:02,460
Soms overweeg ik 

125
00:14:03,400 --> 00:14:05,459
Sommige mensen beschouwen het misschien als een hele klus. 

126
00:14:05,920 --> 00:14:09,390
Maar ik wed dat degenen die pijn hebben in zazen. 

127
00:14:16,400 --> 00:14:19,550
Als zen-non, 

128
00:14:23,040 --> 00:14:26,180
welke houding moet je aannemen op het werk? 

129
00:14:27,080 --> 00:14:30,100
Ik weet het niet, ik heb nooit gewerkt! 

130
00:14:33,960 --> 00:14:37,060
Moeten we van hem een ​​samu maken? 

131
00:14:38,500 --> 00:14:39,520
Ja, dat is het punt. 

132
00:14:39,740 --> 00:14:41,100
Ik had het onzin, he. 

133
00:14:41,300 --> 00:14:47,500
Toen ik jonger was, had ik een baan als conciërge. 

134
00:14:48,090 --> 00:14:54,920
Ik maakte de trap schoon en haalde elke ochtend om vijf uur de vuilnis buiten. 

135
00:14:55,140 --> 00:15:00,619
De trap, het was een hoera. Er was hondenpoep. 

136
00:15:00,920 --> 00:15:05,360
Ik dacht: "Maar dit is niet de dojo of zen." 

137
00:15:05,760 --> 00:15:09,760
’Maakt niet uit. Voor mij is het net een tempel.’

138
00:15:10,320 --> 00:15:14,160
Ik deed het als een samoerai. 

139
00:15:14,480 --> 00:15:17,040
En Sensei vroeg me: 

140
00:15:17,420 --> 00:15:20,080
’Stéphane, ik wil graag dat je de dojo schoonmaakt.’

141
00:15:20,400 --> 00:15:23,660
’Ja, dat zal ik. Vrijdag ben ik vrij.’

142
00:15:25,440 --> 00:15:27,920
’Ik doe het volgende vrijdag.’

143
00:15:28,580 --> 00:15:29,660
Hij zei tegen me: 

144
00:15:29,940 --> 00:15:32,500
’Nee, ik wil dat je het elke dag doet.’

145
00:15:33,120 --> 00:15:34,272
Dan zei hij: 

146
00:15:34,520 --> 00:15:37,440
’Een dag zonder werk, een dag zonder eten.’

147
00:15:37,860 --> 00:15:40,720
’Maar het heeft niets met eten te maken!’

148
00:15:41,000 --> 00:15:44,440
"Een dag zonder werk, een dag zonder mushotoku!" 

149
00:15:44,780 --> 00:15:47,380
Het was een heel originele uitdrukking: 

150
00:15:47,700 --> 00:15:50,380
’Je moet Mushotoku eten!’

151
00:15:54,760 --> 00:15:57,540
Het betekende niets! 

152
00:15:58,180 --> 00:16:02,500
Zeggen dat je een monnik bent, het beoefenen van samu is een revolutie. 

153
00:16:02,880 --> 00:16:05,880
We moeten de revolutie maken, dat heb ik altijd gezegd! 

154
00:16:06,920 --> 00:16:10,657
Maar ik weet niet hoe ik molotovcocktails moet gebruiken! 

155
00:16:11,000 --> 00:16:14,100
Dus in zijn werk moet je samu oefenen. 

156
00:16:14,400 --> 00:16:19,780
Om alle problemen van de wereld op te lossen, hoeft u dit alleen maar te vinden. 

157
00:16:20,100 --> 00:16:22,060
Mensen hebben de geest van de samu niet meer. 

158
00:16:22,520 --> 00:16:23,920
Ze eten geen mushotoku meer. 

159
00:16:24,160 --> 00:16:25,580
Ze eten hamburger en drinken cola. 

160
00:16:26,180 --> 00:16:28,880
Dus wie tegen je zegt: 

161
00:16:29,580 --> 00:16:31,580
Maar het is half zes, jij bent de enige die nog over is! 

162
00:16:31,880 --> 00:16:33,140
Zeg jij het tegen hem: 

163
00:16:33,360 --> 00:16:36,160
’Ik oefen samu!’

164
00:16:45,640 --> 00:16:47,640
’Omdat ik Mushotoku wil eten!’

165
00:16:49,780 --> 00:16:51,200
’Wat is mushotoku?’

166
00:16:51,440 --> 00:16:52,780
’Ik wil er ook een paar!’

167
00:16:57,040 --> 00:17:01,200
We moeten mensen lesgeven zonder bang te zijn. 

168
00:17:03,180 --> 00:17:06,660
Wees trots om mijn discipelen te zijn en monniken te zijn! 

169
00:17:07,000 --> 00:17:09,000
En om discipelen van Deshimaru te zijn. 

