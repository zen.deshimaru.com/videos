1
00:00:00,290 --> 00:00:08,820
Sono la suora Gyu Ji, ordinata dal mio maestro quasi 40 anni fa

2
00:00:08,820 --> 00:00:16,440
e sono anche certificato dal mio Maestro, il Maestro Kosen, dal quale ho ricevuto quello che si chiama Shihô.

3
00:00:16,720 --> 00:00:21,580
che mi permette di
aprirmi all’insegnamento

4
00:00:21,880 --> 00:00:25,200
per aiutare a trasmettere lo Zen.

5
00:00:25,520 --> 00:00:29,880
Io vivo nel tempio, il che significa che sono io che organizzo

6
00:00:30,220 --> 00:00:35,400
fuori da sesshin la vita di questo tempio,

7
00:00:35,680 --> 00:00:40,680
che si assicura che sia mantenuta, che possa ospitare persone al di fuori di sesshin...

8
00:00:40,680 --> 00:00:43,700
in modo che possa vivere tutta la sua vita
l’anno.

9
00:00:43,700 --> 00:00:49,950
Siamo nel Caroux, è a un’ora e mezza da Montpellier, a un’ora da Béziers.

10
00:00:49,950 --> 00:00:55,370
Siamo in mezzo alla natura, è un tempio...
che esiste da dieci anni ormai.

11
00:00:55,370 --> 00:01:03,390
Così, la vita del Tempio di Yujo Nyusanji è...
semplice, è ritmico.

12
00:01:03,390 --> 00:01:09,600
Ritmati dalla pratica dello zazen, ci alziamo la mattina, andiamo al dojo, facciamo zazen, quello che ci viene insegnato.

13
00:01:09,600 --> 00:01:15,450
il lignaggio dei Maestri, qualunque esso sia 
Il Maestro Kosen che è all’origine

14
00:01:15,450 --> 00:01:21,180
di questo tempio, il suo maestro Deshimaru e il suo maestro
andiamo su, andiamo su, torniamo su, torniamo su, torniamo su

15
00:01:21,180 --> 00:01:26,700
Shakyamuni Buddha. Zazen è
il centro della pratica di questo tempio

16
00:01:26,700 --> 00:01:33,960
è una meditazione seduta e silenziosa, una meditazione in tranquillità

17
00:01:33,960 --> 00:01:42,570
dove, postura e condizione
di spirito sono in armonia.

18
00:01:42,570 --> 00:01:47,820
La vita del tempio è anche vita in comune con gli altri,

19
00:01:48,140 --> 00:01:52,220
i momenti in cui facciamo il samu, cioè la condivisione dei compiti...

20
00:01:52,500 --> 00:02:00,140
che si tratti di cucinare, fare giardinaggio, dipingere un muro, tutto quello che c’è da fare da
affinché questo tempio possa vivere.

21
00:02:00,420 --> 00:02:06,960
Ma è un luogo aperto, non è un luogo
dove sei destinato ad essere fidanzato

22
00:02:06,960 --> 00:02:12,680
nel buddismo, dove sei destinato a diventare una persona religiosa...

23
00:02:12,680 --> 00:02:18,109
è un posto per tutti. Inoltre, il Maestro Kosen una volta ci ha detto

24
00:02:18,109 --> 00:02:23,390
siamo monaci laici, è importante per noi confrontarci

25
00:02:23,390 --> 00:02:28,579
alla vita di tutti, compreso il nostro studio,

26
00:02:28,579 --> 00:02:33,280
la nostra pratica è la forza trainante che ci porta a vivere una vita per tutti
i giorni.

27
00:02:33,280 --> 00:02:40,010
Il tempio è dove stiamo andando.
passare un po’ di tempo per approfondire la sua pratica,

28
00:02:40,010 --> 00:02:45,900
fare un passo indietro
in relazione alla sua vita quotidiana,

29
00:02:46,440 --> 00:02:51,280
Potrebbe essere solo un weekend. Potrebbe essere...
potrebbe essere un mese, potrebbero essere tre mesi.

30
00:02:51,780 --> 00:02:59,500
E’ un passaggio il tempio, è un
momento della sua vita, del suo anno.

31
00:03:00,580 --> 00:03:05,180
La vita del tempio comprende anche i ritiri, i cosiddetti sesshin.

32
00:03:05,180 --> 00:03:11,540
Ora siamo in minoranza,
è un periodo più intenso

33
00:03:11,540 --> 00:03:16,820
di praticare lo zazen anche con samu, naturalmente.

34
00:03:17,340 --> 00:03:27,520
La particolarità di questo tempio è che non abbiamo l’ottica per imitare tutto ciò che già esiste.

35
00:03:27,820 --> 00:03:33,300
è uno zen come l’ha insegnato il maestro Deshimaru, molto creativo,

36
00:03:33,840 --> 00:03:37,800
con quello che siamo, con quello che abbiamo.

37
00:03:38,260 --> 00:03:46,120
Anche i rituali sono praticati in questo tempio, ma con parsimonia,

38
00:03:46,440 --> 00:03:51,360
come direbbe il Maestro Kosen, la giusta dose non è né troppo né troppo poca.

39
00:03:51,900 --> 00:03:56,820
Vivo sul posto e vi do il benvenuto.
