1
00:00:00,100 --> 00:00:02,400
Nulle part ailleurs on nous dit 

2
00:00:02,780 --> 00:00:06,380
comme c’est génial de s’asseoir sans rien faire 

3
00:00:06,780 --> 00:00:09,100
Les gens ne sont pas là pour «faire semblant». 

4
00:00:09,380 --> 00:00:10,520
C’est très vrai. 

5
00:00:10,780 --> 00:00:12,320
C’est ce que j’aime dans cette pratique. 

6
00:00:12,760 --> 00:00:16,420
J’avais déjà vécu une première méditation … 

7
00:00:16,800 --> 00:00:18,700
tout seul dans ma chambre quand j’étais senior. 

8
00:00:19,060 --> 00:00:21,280
Il m’est arrivé de voir qu’il y avait un dojo zen … 

9
00:00:21,740 --> 00:00:24,140
… juste à côté de ma maison à La Croix-Rousse à Lyon. 

10
00:00:24,540 --> 00:00:25,520
Et je me suis retrouvé là-bas! 

11
00:00:25,840 --> 00:00:28,240
Ce n’est qu’après que j’ai réalisé que ça faisait longtemps … 

12
00:00:28,560 --> 00:00:30,600
que c’était quelque chose de préparatoire. 

13
00:00:31,500 --> 00:00:32,660
Je m’appelle Gautier. 

14
00:00:32,920 --> 00:00:34,740
J’ai 28 ans. 

15
00:00:35,000 --> 00:00:37,320
Je pratique le Zen depuis près de 3 ans maintenant. 

16
00:00:38,620 --> 00:00:41,180
Je pratique au dojo à Lyon, à Croix-Rousse. 

17
00:00:41,560 --> 00:00:44,500
Avec Christophe Desmur, maître Ryurin. 

18
00:00:44,860 --> 00:00:48,280
Et tous les sanghas de Maître Kosen. 

19
00:00:48,860 --> 00:00:52,460
C’est une grande Sangha répartie sur différents continents. 

20
00:00:52,720 --> 00:00:57,160
On se retrouve pendant les sesshins pour une pratique plus intensive. 

21
00:00:57,460 --> 00:01:00,620
Il y a un équilibre entre ma vie professionnelle, 

22
00:01:00,960 --> 00:01:03,300
ma vie personnelle et zen. 

23
00:01:03,580 --> 00:01:05,460
Tu n’as pas à te forcer, 

24
00:01:05,720 --> 00:01:07,640
au lieu de simplement tomber amoureux. 

25
00:01:08,280 --> 00:01:13,060
Si j’ai envie d’aller à une session, j’y vais. 

26
00:01:15,100 --> 00:01:18,140
Il en va de même pour la pratique quotidienne au dojo. 

27
00:01:18,520 --> 00:01:20,480
Vous entendez dans un dojo: 

28
00:01:20,780 --> 00:01:23,180
"Vous devez tout oublier, même le bouddhisme!" 

29
00:01:23,720 --> 00:01:24,940
Ce n’est jamais ennuyeux. 

30
00:01:25,200 --> 00:01:26,460
C’est parfois très difficile. 

31
00:01:26,740 --> 00:01:29,060
Nous prenons conscience de nos conflits intérieurs. 

32
00:01:30,060 --> 00:01:32,240
Je vois comment cela peut faire une impression. 

33
00:01:32,540 --> 00:01:35,260
Vous pouvez les voir tous en noir, 

34
00:01:35,600 --> 00:01:37,160
certains ont la tête rasée … 

35
00:01:37,500 --> 00:01:39,780
Ils sont tous devant le mur … 

36
00:01:41,160 --> 00:01:43,240
En Zen, vous ne pratiquez pas par vous-même. 

37
00:01:43,480 --> 00:01:45,440
Ne pas mieux dormir, travailler plus efficacement, 

38
00:01:45,780 --> 00:01:48,780
pour aller mieux dans ses baskets, 

39
00:01:49,020 --> 00:01:51,540
mieux dans ses relations … 

40
00:01:51,880 --> 00:01:53,200
C’est autre chose qui motive! 

41
00:01:53,480 --> 00:01:56,740
Si vous avez aimé cette vidéo, n’hésitez pas à l’aimer et à vous abonner à la chaîne! 

42
00:01:57,060 --> 00:02:00,980
Et au revoir, j’espère! 

