1
00:00:09,675 --> 00:00:13,085
Salve. Oggi vorrei spiegarvi

2
00:00:13,280 --> 00:00:15,940
come praticare una passeggiata di meditazione

3
00:00:16,760 --> 00:00:21,800
che pratichiamo nello Zen, questa passeggiata si chiama kin-hin.

4
00:00:22,180 --> 00:00:25,640
È una passeggiata piuttosto breve,

5
00:00:25,920 --> 00:00:30,740
che si pratica tra due momenti di meditazione seduti...

6
00:00:30,920 --> 00:00:32,220
zazen

7
00:00:32,460 --> 00:00:35,380
come ho spiegato in un video precedente.

8
00:00:35,760 --> 00:00:39,220
In generale, un’intera sessione di meditazione

9
00:00:39,440 --> 00:00:43,380
include una prima parte di meditazione seduta, zazen,

10
00:00:43,660 --> 00:00:49,800
e poi questa piccola passeggiata di meditazione che ti spiegherò...

11
00:00:50,060 --> 00:00:52,360
chiamato kin-hin,

12
00:00:52,640 --> 00:00:54,820
e dopo questo kin-hin

13
00:00:54,820 --> 00:00:57,580
torniamo al punto in cui si trova il nostro cuscino per la meditazione.

14
00:00:57,580 --> 00:01:00,600
in secondo luogo

15
00:01:00,600 --> 00:01:04,460
di zazen, di fronte al muro, in silenzio.

16
00:01:12,340 --> 00:01:15,640
La passeggiata di meditazione, kin-hin,

17
00:01:16,445 --> 00:01:17,915
è una passeggiata,

18
00:01:18,480 --> 00:01:21,380
metterai i piedi

19
00:01:21,760 --> 00:01:26,320
se si guarda la posizione dei miei piedi in questo modo,

20
00:01:26,600 --> 00:01:30,900
cioè sono distanziati, più o meno, dalla larghezza del pugno,

21
00:01:31,780 --> 00:01:36,100
senza distorcere il bacino.

22
00:01:37,580 --> 00:01:41,180
E tu prenderai provvedimenti

23
00:01:41,360 --> 00:01:44,480
mezzo metro di larghezza

24
00:01:44,800 --> 00:01:47,540
un piede è più o meno 30-33 cm.

25
00:01:47,880 --> 00:01:51,160
quindi saranno circa 15 centimetri.

26
00:01:51,660 --> 00:01:54,480
Se stai guardando,

27
00:01:54,680 --> 00:01:57,660
Vedi, sto facendo un passo di mezzo metro.

28
00:01:58,040 --> 00:02:03,580
Così a un certo punto ho una gamba in alto e una in basso...

29
00:02:03,840 --> 00:02:12,240
la gamba dietro passerà davanti facendo un passo della larghezza di mezzo metro.

30
00:02:12,240 --> 00:02:15,880
Assicuratevi che i vostri piedi siano

31
00:02:16,240 --> 00:02:18,560
dritto e non così.

32
00:02:27,700 --> 00:02:30,875
Ora la postura della mano.

33
00:02:30,875 --> 00:02:33,685
Mentre camminiamo, come ho appena spiegato,

34
00:02:33,685 --> 00:02:36,820
abbiamo anche un’attività con le mani.

35
00:02:37,520 --> 00:02:40,300
Ricordate, la postura di meditazione Zen,

36
00:02:40,620 --> 00:02:42,520
avevamo le mani così.

37
00:02:43,140 --> 00:02:45,660
Mi tolgo la mano destra,

38
00:02:45,980 --> 00:02:49,180
Piego il pollice della mano sinistra

39
00:02:49,500 --> 00:02:55,000
e lo afferro con tutte le dita incollate,

40
00:02:55,320 --> 00:03:01,700
Ho lasciato sporgere la radice del mio pollice sinistro.

41
00:03:02,900 --> 00:03:05,140
La mia mano destra che avvolgeva

42
00:03:05,480 --> 00:03:08,765
avvolge la mano sinistra

43
00:03:08,765 --> 00:03:14,600
le dita unite, i pollici uniti, tutti incollati insieme...

44
00:03:14,800 --> 00:03:18,640
Piego leggermente i polsi

45
00:03:19,380 --> 00:03:22,320
Vedi, la radice del mio pollice sinistro sta uscendo...

46
00:03:22,940 --> 00:03:25,980
e questa radice

47
00:03:26,340 --> 00:03:29,700
Lo applicherò qui, alla base dello sterno.

48
00:03:30,320 --> 00:03:35,180
Tutti gli esseri umani, alla base dello sterno, hanno due o tre piccoli fori.

49
00:03:35,720 --> 00:03:44,200
Qui è dove si poserà, facendo contatto tra la radice del pollice sinistro e il plesso.

50
00:03:44,780 --> 00:03:55,440
Come potete vedere, piego leggermente i polsi, in modo che gli avambracci siano nella stessa linea.

51
00:03:55,860 --> 00:03:59,160
Non ho un braccio sopra l’altro.

52
00:03:59,600 --> 00:04:03,020
I palmi delle mie mani sono paralleli al suolo,

53
00:04:03,540 --> 00:04:07,340
Voglio dire, io non sono così.

54
00:04:07,680 --> 00:04:11,140
Non mi cadono le braccia.

55
00:04:11,520 --> 00:04:16,700
e non alzo le braccia con forza fisica quando alzo i gomiti.

56
00:04:17,800 --> 00:04:21,040
Che succede? Che succede?

57
00:04:21,560 --> 00:04:30,260
Ricordate quando espirate, fate pressione, tutto il vostro peso corporeo sulla gamba anteriore.

58
00:04:30,560 --> 00:04:33,640
Durante questa fase,

59
00:04:34,200 --> 00:04:40,720
allo stesso modo, premeremo le mani insieme,

60
00:04:41,080 --> 00:04:49,220
applicare una leggera pressione e contemporaneamente premere la radice del pollice sinistro contro il plesso.

61
00:04:49,580 --> 00:04:54,640
Ovviamente, delicatamente, non si tratta di violenza.

62
00:04:54,840 --> 00:04:59,700
Vedrai che se sei un po’ troppo basso, ti colpirà un punto nello stomaco

63
00:05:00,080 --> 00:05:04,340
vi renderete presto conto che non è piacevole

64
00:05:04,820 --> 00:05:09,720
quindi naturalmente ti alzerai per trovare la giusta postura.

65
00:05:13,600 --> 00:05:16,880
Quando inspiro, metto la gamba davanti ad essa,

66
00:05:17,160 --> 00:05:18,460
tutto è sciolto.

67
00:05:18,860 --> 00:05:21,880
Tutto è in posizione, ma è sciolto.

68
00:05:22,300 --> 00:05:26,300
Durante la fase di espirazione,

69
00:05:27,080 --> 00:05:34,440
Metto il peso sulla gamba anteriore, premo leggermente contro il plesso ed entrambe le mani l’una contro l’altra.

70
00:05:35,080 --> 00:05:40,920
Mi sono messo di profilo, come potete vedere, è come durante lo zazen,

71
00:05:41,200 --> 00:05:49,180
Tengo la colonna vertebrale il più dritta possibile, il collo allungato, il mento infilato...

72
00:05:49,520 --> 00:05:53,840
il mio sguardo 45 gradi in diagonale verso il basso,

73
00:05:54,460 --> 00:05:59,560
Sono totalmente concentrato, proprio qui, proprio ora, su quello che sto facendo.

74
00:06:00,200 --> 00:06:03,180
E quello che faccio è una passeggiata meditativa.

75
00:06:03,580 --> 00:06:08,420
Non stai seguendo i tuoi pensieri, non stai guardando fuori,

76
00:06:08,640 --> 00:06:14,260
si continua con la concentrazione di zazen, la concentrazione della postura seduta.

77
00:06:22,020 --> 00:06:27,600
Il ritmo della camminata sarà il ritmo del tuo respiro.

78
00:06:28,120 --> 00:06:31,380
Ricordate, durante la meditazione seduta,

79
00:06:31,760 --> 00:06:35,840
siamo concentrati qui e ora sui punti fisici della postura,

80
00:06:36,500 --> 00:06:41,380
lo stato d’animo, l’atteggiamento mentale, si lasciano andare i pensieri,

81
00:06:41,640 --> 00:06:44,660
non li serviamo

82
00:06:44,920 --> 00:06:48,000
e ci concentriamo qui e ora sul nostro respiro.

83
00:06:48,340 --> 00:06:51,560
stabilendo un armonioso avanti e indietro

84
00:06:51,960 --> 00:06:55,780
e concentrandosi soprattutto sull’espirazione...

85
00:06:57,160 --> 00:07:01,260
sempre più a lungo, sempre più a fondo,

86
00:07:01,580 --> 00:07:04,540
addominale, che arriva fino al basso addome.

87
00:07:05,860 --> 00:07:15,440
Il respiro e l’atteggiamento mentale sono gli stessi sia che siamo seduti, di fronte al muro, in silenzio, immobili, in zazen.

88
00:07:16,280 --> 00:07:24,660
o fare questa meditazione mentre si cammina. L’unica cosa che cambia è che ci muoveremo.

89
00:07:30,320 --> 00:07:33,880
Cammina secondo il ritmo del tuo respiro.

90
00:07:34,160 --> 00:07:41,660
Durante l’ispirazione si passa una gamba davanti

91
00:07:42,520 --> 00:07:45,000
mezzo metro

92
00:07:45,180 --> 00:07:50,220
e durante la fase di scadenza,

93
00:07:52,440 --> 00:07:55,420
attraverso il naso, in silenzio,

94
00:07:56,100 --> 00:07:59,820
si sta andando a posizionare gradualmente il peso totale del corpo

95
00:08:00,100 --> 00:08:03,240
sulla gamba anteriore

96
00:08:03,520 --> 00:08:06,880
la gamba anteriore è ben tesa

97
00:08:07,180 --> 00:08:11,480
le piante dei piedi completamente a contatto con il suolo

98
00:08:12,080 --> 00:08:15,680
la gamba dietro di lei, stai sciolto,

99
00:08:16,180 --> 00:08:20,440
ma anche l’intera pianta del piede a contatto con il suolo.

100
00:08:20,740 --> 00:08:24,320
Se vedete il piede, non sono con il tallone alzato.

101
00:08:24,700 --> 00:08:29,860
Ho i tacchi di entrambi i piedi appoggiati a terra.

102
00:08:30,480 --> 00:08:33,280
Quindi, durante la fase di espirazione,

103
00:08:33,740 --> 00:08:40,180
Sto spostando gradualmente tutto il mio peso corporeo su questa gamba anteriore...

104
00:08:40,460 --> 00:08:43,520
fino alla radice dell’alluce

105
00:08:43,920 --> 00:08:46,820
vagamente

106
00:08:47,380 --> 00:08:51,380
è solo il peso che mi fa cadere a terra.

107
00:08:51,760 --> 00:08:57,960
Come se fossi su un terreno morbido come la sabbia o l’argilla...

108
00:08:58,280 --> 00:09:05,800
e tu avresti voluto lasciare il tuo segno nella sabbia o nell’argilla.

109
00:09:06,700 --> 00:09:10,420
Te lo mostro di profilo.

110
00:09:10,920 --> 00:09:14,580
Inspiro e metto la gamba in alto,

111
00:09:15,100 --> 00:09:20,280
Espiro, tutto il peso del mio corpo sulla gamba anteriore.

112
00:09:20,640 --> 00:09:23,420
Sono alla fine della mia espirazione,

113
00:09:23,680 --> 00:09:28,940
Inspiro e metto la gamba dietro la parte anteriore...

114
00:09:29,960 --> 00:09:31,120
Scadenza

115
00:09:32,620 --> 00:09:37,340
tutto il peso del mio corpo sulla gamba anteriore.

116
00:09:37,780 --> 00:09:44,240
Fine dell’espirazione, inspiro, la gamba posteriore va davanti.

117
00:09:51,240 --> 00:09:55,380
Provatelo e vedrete che la camminata del kin-hin

118
00:09:55,640 --> 00:10:01,920
è una passeggiata che si può poi utilizzare in molte circostanze nella vita.

119
00:10:02,360 --> 00:10:12,360
5 minuti di attesa, sei stressato e bene, pratica questo, 3-4 minuti di questa passeggiata.

120
00:10:12,660 --> 00:10:18,980
e vedrai come ti calmerai, ti sentirai più in sintonia con te stesso.

121
00:10:19,860 --> 00:10:21,240
Buona pratica
