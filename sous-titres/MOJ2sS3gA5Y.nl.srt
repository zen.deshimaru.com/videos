1
00:00:04,240 --> 00:00:07,040
Wat is Zen?

2
00:00:07,320 --> 00:00:10,120
Zen gaat over het begrijpen van jezelf.

3
00:00:10,400 --> 00:00:11,800
Je eigen geest.

4
00:00:12,080 --> 00:00:14,140
Het zijn de zaken van iedereen.

5
00:00:14,420 --> 00:00:16,500
Dat is wat Deshimaru wilde uitzenden.

6
00:00:16,800 --> 00:00:19,460
Dat is wat ik wil doorgeven en realiseren.

7
00:00:19,720 --> 00:00:22,460
Ik had een discipel van Kodo Sawaki gezien.

8
00:00:22,840 --> 00:00:24,720
Hij zei tegen me:

9
00:00:25,120 --> 00:00:27,620
“Je moet naar jezelf kijken!”

10
00:00:27,960 --> 00:00:29,960
Hij had gelijk!

11
00:00:30,380 --> 00:00:33,300
Wat betekent een spiritueel pad?

12
00:00:33,640 --> 00:00:41,880
De betekenis van een spiritueel pad is “terug te komen naar de dromer”.

13
00:00:46,020 --> 00:00:48,260
Dogen zegt:

14
00:00:48,520 --> 00:00:51,260
“Het boeddhisme bestuderen is jezelf bestuderen.”

15
00:00:51,540 --> 00:00:53,980
Bestudeer de dromer.

16
00:00:54,380 --> 00:00:57,300
Waarom droomt hij zo?

17
00:00:58,260 --> 00:01:00,240
Hoe kan hij zijn droom veranderen?

18
00:01:00,540 --> 00:01:02,020
Hoe kan hij zijn droom onder controle houden?

19
00:01:02,840 --> 00:01:04,000
Als de Boeddha zegt:

20
00:01:04,440 --> 00:01:06,300
“Alles is een droom.”

21
00:01:06,620 --> 00:01:09,820
Dat wil niet zeggen “Niets doet er toe”.

22
00:01:10,100 --> 00:01:13,580
Het is niet de droom die belangrijk is, het is de dromer.

23
00:01:13,900 --> 00:01:16,540
Stop met dromen, dat is wat we “ontwaken” noemen…

24
00:01:17,180 --> 00:01:21,040
Ontwaken is: de dromer worden.

25
00:01:21,340 --> 00:01:22,980
Het beheersen van je droom.

26
00:01:24,000 --> 00:01:26,120
Dat is het spirituele pad.

27
00:01:26,420 --> 00:01:27,980
Het is erg moeilijk.

28
00:01:28,560 --> 00:01:31,760
Hoe kan ik beginnen?

29
00:01:32,660 --> 00:01:45,080
De primaire motivatie voor het beoefenen van een spiritueel pad is: het kunnen beheersen van je droom.

30
00:01:46,700 --> 00:01:49,340
Wat is belangrijk in Zen?

31
00:01:49,920 --> 00:01:55,880
Op deze manier is het je geest die belangrijk is.

32
00:01:56,280 --> 00:02:01,280
Zazen is geen voorwerp van aanbidding.

33
00:02:01,540 --> 00:02:03,980
Het tweede belangrijke punt van zijn onderwijs:

34
00:02:04,320 --> 00:02:06,320
Mushotoku.

35
00:02:07,180 --> 00:02:10,780
Wat betekent: “zonder winstoogmerk”.

36
00:02:11,340 --> 00:02:23,340
Het is de deur die ons van de geest van God naar de materiële wereld leidt.

37
00:02:23,640 --> 00:02:27,000
Het is erg belangrijk.

38
00:02:27,360 --> 00:02:37,420
Zelfs in de alledaagse wereld, zelfs in fenomenen, verlies je je concentratie niet.

39
00:02:37,800 --> 00:02:42,920
Je verliest je ware aard niet.

40
00:02:43,180 --> 00:02:45,100
Dat is Mushotoku.

41
00:02:45,400 --> 00:02:49,660
Maar wat is gemakkelijk?

42
00:02:55,780 --> 00:03:00,260
Mensen nemen altijd de makkelijke weg.

43
00:03:00,620 --> 00:03:03,780
We vallen in de makkelijke dingen.

44
00:03:10,640 --> 00:03:13,600
Macht is makkelijk.

45
00:03:14,060 --> 00:03:19,220
Het is gemakkelijk om bij mensen te zijn die zich altijd hetzelfde voelen.

46
00:03:21,160 --> 00:03:26,400
Van zen een kerk maken is gemakkelijk.

47
00:03:26,800 --> 00:03:32,080
Aan een kerk vastmaken is eenvoudig.

48
00:03:32,360 --> 00:03:37,300
Mensen gaan in iets veiligs.

49
00:03:37,600 --> 00:03:41,060
Zonder enige moeite.

50
00:03:41,420 --> 00:03:44,620
Waar ze nooit echt met zichzelf geconfronteerd worden.

51
00:03:44,920 --> 00:03:47,080
Waar ze nooit alleen zijn.

52
00:03:47,500 --> 00:03:52,040
Hoe kijkt u naar uw voormalige mede-volgelingen?

53
00:03:52,360 --> 00:03:58,720
Ik bewonder veel van Meester Deshimaru’s discipelen.

54
00:03:58,980 --> 00:04:07,980
Die zijn onderwijs hebben ontvangen en alleen blijven oefenen.

55
00:04:08,240 --> 00:04:14,320
Het is beter om samen te oefenen.

56
00:04:14,700 --> 00:04:19,140
Maar ze doen tenminste hun eigen dingen.

57
00:04:19,660 --> 00:04:23,980
Ze zijn alleen.

