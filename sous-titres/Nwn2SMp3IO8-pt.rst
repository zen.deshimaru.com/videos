1
00:00:00,280 --> 00:00:02,560
Eu não sei muito sobre a sofrologia.

2
00:00:02,800 --> 00:00:12,140
Eu sei que é uma aplicação de certas práticas de bem-estar, de desenvolvimento pessoal,

3
00:00:12,540 --> 00:00:17,160
e há uma sofrologia que é chamada de "orientalista",

4
00:00:17,440 --> 00:00:24,080
que é muito inspirada pelas práticas iogues, budistas ou indianas.

5
00:00:24,540 --> 00:00:27,940
O Zen é muito simples.

6
00:00:28,380 --> 00:00:38,400
É baseado em posturas humanas fundamentais.

7
00:00:39,820 --> 00:00:48,480
Vou lhes mostrar a primeira postura fundamental do Zen.

8
00:01:01,500 --> 00:01:06,120
Os seres humanos costumavam ser macacos.

9
00:01:06,460 --> 00:01:08,580
Foi assim que eles andaram.

10
00:01:09,200 --> 00:01:20,360
Pouco a pouco, eles se sentaram retos, como mestres Zen.

11
00:01:22,900 --> 00:01:24,900
Depois eles se endireitaram.

12
00:01:25,180 --> 00:01:28,600
Foi assim que eles andaram.

13
00:01:29,680 --> 00:01:33,760
E a consciência humana tem evoluído

14
00:01:35,180 --> 00:01:39,980
junto com a postura.

15
00:01:40,340 --> 00:01:46,000
A postura do corpo é um pensamento fundamental.

16
00:01:46,340 --> 00:01:52,200
Por exemplo, Usain Bolt, o corredor mais rápido do mundo,

17
00:01:52,540 --> 00:01:55,160
fez esse gesto.

18
00:01:55,440 --> 00:02:02,300
Todos entendem o que isso significa e, no entanto, não há palavras.

19
00:02:02,640 --> 00:02:05,440
É apenas uma postura.

20
00:02:05,900 --> 00:02:11,120
A maneira como nos sentamos e caminhamos,

21
00:02:11,520 --> 00:02:13,400
o que você faz com suas mãos,

22
00:02:13,700 --> 00:02:16,500
tudo isso é de fundamental importância.

23
00:02:18,160 --> 00:02:21,440
Em zen, há a postura de Buda.

24
00:02:22,980 --> 00:02:31,120
Ela existia antes do Buda, é mencionada no Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
Você se senta em uma almofada de grama e dobra suas pernas.

26
00:02:41,560 --> 00:02:47,140
Na natureza, o homem pré-histórico comunicou

27
00:02:47,440 --> 00:02:53,220
com as orelhas, com o nariz, com os olhos, para ver longe.

28
00:02:54,920 --> 00:02:59,000
Por isso, eles queriam se endireitar.

29
00:02:59,400 --> 00:03:04,220
E à medida que se endireitavam, sua consciência se aguçava, despertava.

30
00:03:06,040 --> 00:03:08,720
Esta é a primeira postura.

31
00:03:08,980 --> 00:03:11,580
Normalmente, temos que fazer um lótus completo.

32
00:03:12,060 --> 00:03:14,260
É assim que você cruza suas pernas.

33
00:03:14,700 --> 00:03:18,460
É chamado de "pretzel".

34
00:03:24,960 --> 00:03:30,980
É muito importante tomar esta postura.

35
00:03:31,380 --> 00:03:37,380
O orador anterior falou sobre dinamismo e relaxamento.

36
00:03:37,780 --> 00:03:42,340
Temos um sistema nervoso simpático e parassimpático.

37
00:03:42,600 --> 00:03:45,800
Temos os músculos externos e os músculos profundos.

38
00:03:46,320 --> 00:03:51,400
Temos respiração rasa e profunda.

39
00:03:51,820 --> 00:03:54,620
Temos uma consciência superficial, cortical,

40
00:03:54,920 --> 00:03:59,760
e consciência profunda, o réptil ou cérebro médio.

41
00:04:00,340 --> 00:04:04,160
Para ficar de pé nesta postura,

42
00:04:04,440 --> 00:04:06,800
você tem que empurrar a terra com seus joelhos.

43
00:04:10,240 --> 00:04:15,640
É preciso um esforço para ser dinâmico e ter uma boa postura.

44
00:04:16,000 --> 00:04:22,180
Meu mestre [Deshimaru] insistiu na beleza e na força da postura.

45
00:04:22,460 --> 00:04:31,280
Mas no lótus, quanto mais você relaxa, mais seus pés crescem sobre suas coxas,

46
00:04:31,520 --> 00:04:35,100
e quanto mais os joelhos crescem no chão.

47
00:04:35,620 --> 00:04:40,280
Quanto mais você relaxa, mais dinâmico você é, mais reto e mais forte você é.

48
00:04:40,560 --> 00:04:46,560
Isto não tem nada a ver com relaxar em uma cadeira ou em uma cama.

49
00:04:46,940 --> 00:04:57,280
É uma forma de abandonar o corpo a si mesmo,

50
00:05:04,380 --> 00:05:09,700
enquanto está bem desperto.

51
00:05:10,120 --> 00:05:16,240
Esta é a primeira postura, a postura sentada de Buda.

52
00:05:16,680 --> 00:05:20,280
As mãos também são muito importantes.

53
00:05:20,640 --> 00:05:23,600
Você está sempre fazendo algo com suas mãos:

54
00:05:23,860 --> 00:05:26,560
trabalhando, protegendo-se...

55
00:05:26,960 --> 00:05:28,740
As mãos são muito expressivas

56
00:05:29,040 --> 00:05:31,040
e estão conectados ao cérebro.

57
00:05:31,500 --> 00:05:41,040
Tomar este mudra com suas mãos muda a consciência.

58
00:05:41,680 --> 00:05:47,680
A concentração está em nossas mãos, corpo, sistema nervoso e cérebro.

59
00:05:48,100 --> 00:05:53,360
Outra coisa importante é o pensamento, a consciência.

60
00:05:53,820 --> 00:05:57,680
Se você é como Rodin’s Thinker,

61
00:05:58,000 --> 00:06:01,100
você não tem a mesma consciência.

62
00:06:09,960 --> 00:06:12,240
que quando sua postura é perfeitamente reta,

63
00:06:12,580 --> 00:06:14,640
especialmente com seu pescoço esticado,

64
00:06:15,700 --> 00:06:18,400
com a cabeça erguida, aberta para o céu,

65
00:06:18,760 --> 00:06:22,020
nariz e umbigo alinhados

66
00:06:25,060 --> 00:06:27,220
e você está respirando com todo o seu ser.

67
00:06:29,020 --> 00:06:31,860
O pensamento então não é mais dissociado do corpo.

68
00:06:32,240 --> 00:06:36,100
Não há mais uma dissociação entre nossa consciência

69
00:06:36,300 --> 00:06:39,080
e cada célula de nosso corpo.

70
00:06:39,500 --> 00:06:45,520
Em Zen, chamamos isto de "mente-corpo", em uma palavra.

71
00:06:46,360 --> 00:06:50,540
Esta é a postura fundamental: zazen.

72
00:06:56,380 --> 00:07:00,200
A segunda postura é a postura de caminhar.

73
00:07:00,680 --> 00:07:06,340
Em zazen, abandonamos tudo e não nos movemos de forma alguma.

74
00:07:06,560 --> 00:07:08,280
Portanto, a consciência é especial.

75
00:07:08,720 --> 00:07:13,800
Mas há outra meditação que é feita durante a caminhada.

76
00:07:16,180 --> 00:07:18,160
Temos que nos mover.

77
00:07:18,540 --> 00:07:20,540
Fazemos algo, seguimos em frente.

78
00:07:21,180 --> 00:07:24,180
Não se avança para pegar algo.

79
00:07:24,580 --> 00:07:28,380
Você acelera sua respiração com sua caminhada.

80
00:07:29,020 --> 00:07:30,740
O coreógrafo Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
que foi discípulo de meu mestre, Deshimaru,

82
00:07:34,060 --> 00:07:38,040
fez seus dançarinos praticarem isso todos os dias.

83
00:07:39,060 --> 00:07:45,340
A segunda postura é a caminhada "kin-hin".

84
00:07:45,880 --> 00:07:51,040
Observe o que você faz com suas mãos.

85
00:07:51,680 --> 00:07:57,840
Um pugilista que tira sua foto, o faz com as mãos.

86
00:07:58,240 --> 00:08:05,220
Quando você coloca suas mãos assim, automaticamente o cérebro expressa uma forte agressividade.

87
00:08:05,820 --> 00:08:10,360
Você pode colocar suas mãos em seus bolsos.

88
00:08:10,840 --> 00:08:13,140
"Onde eu coloquei minha carteira? »

89
00:08:13,640 --> 00:08:16,360
"Alguém roubou meu telefone celular! »

90
00:08:16,700 --> 00:08:19,580
Podemos enfiar nossos dedos no nariz.

91
00:08:21,660 --> 00:08:25,820
Tudo o que foi construído aqui foi construído pelas mãos.

92
00:08:26,260 --> 00:08:28,820
É extraordinário, a mão.

93
00:08:29,160 --> 00:08:31,860
Somos os únicos animais com mãos,

94
00:08:32,260 --> 00:08:35,340
eles estão relacionados à nossa evolução.

95
00:08:37,700 --> 00:08:40,660
As mãos também meditam.

96
00:08:40,940 --> 00:08:43,580
Você coloca seu polegar no punho.

97
00:08:43,820 --> 00:08:49,420
Coloque a raiz do polegar sob o esterno, coloque a outra mão sobre ele.

98
00:08:49,760 --> 00:08:55,060
Estes detalhes posturais existem desde o Buda.

99
00:08:55,380 --> 00:09:02,520
Textos antigos explicam que eles foram transmitidos à China a partir da Índia.

100
00:09:02,960 --> 00:09:08,920
É uma transmissão oral de pessoa para pessoa.

101
00:09:09,480 --> 00:09:12,500
No budismo, há muitos aspectos.

102
00:09:12,780 --> 00:09:17,320
Mas o aspecto meditativo e postural é muito importante.

103
00:09:17,820 --> 00:09:28,540
Havia dois grandes discípulos do Buda, Śāriputra e Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Seu mestre espiritual não tinha certeza:

105
00:09:37,220 --> 00:09:43,680
"Não tenho certeza se tenho o truque para sair da vida e da morte". »

106
00:09:43,940 --> 00:09:52,860
"Se você encontrar um mestre que ensine isto, me avise e eu irei com você". »

107
00:09:53,620 --> 00:10:04,320
Um dia, eles vêem um monge, como eu, você vê, andando pela estrada.

108
00:10:04,780 --> 00:10:10,000
Eles estão maravilhados com a beleza deste homem.

109
00:10:10,420 --> 00:10:16,200
Então este monge pára em um bosque para fazer xixi.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, que é considerada uma das maiores inteligências da Índia na época, diz-lhe:

111
00:10:34,180 --> 00:10:43,940
"Estamos maravilhados com sua presença, quem é seu mestre? »

112
00:10:46,560 --> 00:10:49,540
- "Meu mestre é Shakyamuni Buddha".

113
00:10:49,860 --> 00:10:53,520
- "Explique sua doutrina para nós! »

114
00:10:53,780 --> 00:10:58,220
- "Não posso explicar, sou muito novo nisto". »

115
00:10:59,100 --> 00:11:04,440
E eles só seguiram este menino por causa da maneira como ele anda.

116
00:11:04,800 --> 00:11:06,980
A maneira como ele está tirando algo dela.

117
00:11:07,440 --> 00:11:11,680
Portanto, a segunda postura é caminhar.

118
00:11:12,020 --> 00:11:15,280
Quando as pessoas fazem zazen, é muito forte.

119
00:11:15,500 --> 00:11:21,580
Até mesmo os praticantes de artes marciais ficam impressionados com o poder que isso dá.

120
00:11:21,940 --> 00:11:25,860
Pelo poder da respiração.

121
00:11:26,240 --> 00:11:35,520
Mas como encontrar essa energia, essa plenitude, na vida cotidiana?

122
00:11:35,840 --> 00:11:39,220
Aquela calma, aquela concentração.

123
00:11:39,600 --> 00:11:42,960
É um ponto difícil.

124
00:11:43,280 --> 00:11:47,140
Porque a postura do zazen é extremamente precisa.

125
00:11:47,600 --> 00:11:55,100
No início, concentramo-nos no aspecto técnico da caminhada dos parentes.

126
00:11:55,540 --> 00:11:58,800
Mas assim que tivermos todos os detalhes,

127
00:11:59,300 --> 00:12:02,160
em qualquer momento de nossa vida diária

128
00:12:02,440 --> 00:12:08,400
(mesmo que eu sempre me esqueça de fazê-lo), podemos praticá-lo.

129
00:12:10,600 --> 00:12:16,740
Você pode se concentrar novamente em seu ser espiritual.

130
00:12:17,740 --> 00:12:22,760
Sobre seu ser que não faz merda alguma.

131
00:12:23,320 --> 00:12:31,340
Eu lhe falei sobre as mãos.

132
00:12:31,840 --> 00:12:39,980
A postura religiosa fundamental é esta.

133
00:12:40,560 --> 00:12:44,240
Quando você tem as mãos juntas assim,

134
00:12:45,080 --> 00:12:48,260
apenas fazendo isto,

135
00:12:48,580 --> 00:12:51,280
tem um efeito sobre o cérebro,

136
00:12:51,620 --> 00:12:55,340
constrói uma ponte entre os dois hemisférios cerebrais.

137
00:12:55,740 --> 00:12:59,960
Esta postura é utilizada em todas as religiões.

138
00:13:00,280 --> 00:13:02,180
É bastante simples.

139
00:13:02,560 --> 00:13:05,640
Veja a importância das mãos!

140
00:13:06,060 --> 00:13:13,280
A terceira postura...

141
00:13:13,860 --> 00:13:16,520
(eu pensei que eram quatro...)

142
00:13:16,880 --> 00:13:20,580
Ah, sim, há a postura propensa, mas não vamos falar sobre isso.

143
00:13:21,000 --> 00:13:25,520
A postura deitada, como dormir.

144
00:13:26,700 --> 00:13:31,980
Já expliquei a vocês como os homens pré-históricos costumavam se arrastar de quatro em quatro,

145
00:13:32,260 --> 00:13:34,440
pouco a pouco, eles se endireitaram,

146
00:13:34,700 --> 00:13:38,760
e que nossa evolução vem de nossa postura,

147
00:13:39,060 --> 00:13:42,580
da irrigação da coluna vertebral que vai até o cérebro,

148
00:13:42,940 --> 00:13:46,400
e o cérebro, que é um instrumento extraordinário,

149
00:13:46,700 --> 00:13:51,440
dos quais apenas 10-15% de sua utilidade total é conhecida.

150
00:13:51,760 --> 00:13:57,740
Temos uma grande coisa entre nossos ouvidos que estamos subutilizando.

151
00:14:08,640 --> 00:14:12,380
Cada um tem seu próprio cérebro!

152
00:14:19,020 --> 00:14:22,140
Cada um tem seu próprio cérebro!

153
00:14:22,400 --> 00:14:25,200
Como um endereço IP.

154
00:14:30,220 --> 00:14:33,220
Cada um tem seu próprio cérebro!

155
00:14:33,940 --> 00:14:35,900
E estamos sozinhos!

156
00:14:36,700 --> 00:14:41,940
É muito importante para o que estou prestes a lhes dizer.

157
00:14:44,760 --> 00:14:50,040
Nossos pés (estamos voltando aos nossos pés...).

158
00:14:51,720 --> 00:14:56,420
Você sabe que vivemos em uma pequena Terra.

159
00:14:57,040 --> 00:15:01,420
Você viu o filme Gravity?

160
00:15:02,120 --> 00:15:07,060
É um dos filmes que realmente despertam a consciência humana.

161
00:15:07,560 --> 00:15:13,380
Em Hollywood, eles fazem às vezes algum trabalho manipulador.

162
00:15:13,680 --> 00:15:17,220
Mas também às vezes de informação e evolução espiritual.

163
00:15:17,760 --> 00:15:19,660
Este filme é um deles.

164
00:15:20,040 --> 00:15:26,920
E você pode ver por si mesmo como nossa Terra é frágil.

165
00:15:29,200 --> 00:15:33,400
Normalmente, quando olhamos para o céu, deliramos: "É infinitamente azul! ».

166
00:15:33,760 --> 00:15:38,940
Não. Não para sempre. 30 quilômetros de atmosfera.

167
00:15:39,360 --> 00:15:44,660
O final da atmosfera está entre 30 e 1.000 quilômetros.

168
00:15:52,260 --> 00:15:56,700
Mesmo mil milhas não é nada.

169
00:15:58,800 --> 00:16:04,780
Se eu o colocasse a 20 milhas de distância, você não sobreviveria nem um minuto.

170
00:16:05,380 --> 00:16:15,940
Mesmo fora dos impostos, nossas condições de vida são precárias.

171
00:16:17,420 --> 00:16:20,440
A Terra é um ser pequeno, frágil e vivo.

172
00:16:21,120 --> 00:16:30,660
Nossa biosfera é minúscula e frágil, ela deve ser preservada.

173
00:16:30,940 --> 00:16:35,100
Devemos estar sempre preocupados em preservar esta coisa.

174
00:16:36,860 --> 00:16:39,940
Porque nascemos da Terra.

175
00:16:40,220 --> 00:16:45,700
Este milagre da vida que se realizou na Terra é efêmero.

176
00:16:46,240 --> 00:16:47,840
Nós nascemos da Terra.

177
00:16:48,220 --> 00:16:53,320
E todos nós temos nossos pés apontando para o mesmo centro, o centro da Terra.

178
00:16:53,900 --> 00:16:57,760
Portanto, nossos pés estão em comunidade.

179
00:17:01,860 --> 00:17:08,680
Mas nossa cabeça é única, ninguém está apontando na mesma direção.

180
00:17:18,400 --> 00:17:23,500
Daí a terceira postura que vou mostrar a vocês.

181
00:17:23,800 --> 00:17:28,140
É comum a muitas religiões, é chamada de "prostração".

182
00:17:28,380 --> 00:17:31,240
É assim que nós fazemos.

183
00:17:40,640 --> 00:17:42,780
Ah, isso sabe bem!

184
00:17:43,060 --> 00:17:49,940
Quando fazemos esta prática, é como quando fazemos amor:

185
00:17:50,260 --> 00:17:58,460
às vezes não fazemos sexo por 10 anos, 20 anos, 6 meses...

186
00:17:58,780 --> 00:18:07,460
e quando voltamos a fazer amor, dizemos: "Ah, isso sabe bem, eu esqueci como era bom! ».

187
00:18:08,220 --> 00:18:12,200
Quando você faz essa prostração, é a mesma coisa.

188
00:18:13,460 --> 00:18:15,740
Não estou brincando.

189
00:18:16,240 --> 00:18:20,260
Estamos reformando nossos cérebros.

190
00:18:21,060 --> 00:18:26,280
Quando você coloca seu cérebro alinhado com o centro da Terra.

191
00:18:26,620 --> 00:18:29,080
Não se trata apenas de humildade,

192
00:18:29,360 --> 00:18:32,720
ou curvar-se diante de estátuas ou o que quer que seja.

193
00:18:34,600 --> 00:18:38,880
É uma coisa mágica, fundamental, xamânica, talvez...

194
00:18:39,320 --> 00:18:41,860
Desde o momento em que você se prostra...

195
00:18:42,180 --> 00:18:43,340
Meu mestre costumava dizer:

196
00:18:43,660 --> 00:18:47,440
"Se os chefes de estado se prostrassem uma vez por dia,"

197
00:18:47,800 --> 00:18:50,200
"mudaria o mundo". »

198
00:18:50,640 --> 00:18:54,560
Não se trata de jibber-jabber ou misticismo.

199
00:18:55,280 --> 00:18:58,780
Deve ser cientificamente mensurável.

200
00:19:00,780 --> 00:19:05,280
Reformata o cérebro, para tocar para baixo.

201
00:19:05,740 --> 00:19:09,280
Ele está constantemente em sua linha individual.

202
00:19:09,620 --> 00:19:17,300
E lá você se conecta com sua raiz, com seus irmãos, com o centro da Terra.

203
00:19:19,940 --> 00:19:25,380
Às vezes eu o faço e sinto algo forte na minha cabeça.

204
00:19:25,660 --> 00:19:27,540
É uma sensação tão boa.

205
00:19:27,820 --> 00:19:39,420
Ao mesmo tempo, ela relativiza o mal que involuntariamente fazemos ao nosso redor.

206
00:19:40,560 --> 00:19:42,960
É um bom remédio.

207
00:19:43,180 --> 00:19:47,040
Todas essas posturas são medicamentos fundamentais.

208
00:19:47,760 --> 00:19:53,260
Eu tinha preparado um texto um tanto intelectual.

209
00:19:53,520 --> 00:19:58,140
Mas parece que não consigo passar de um assunto para outro.

210
00:19:58,440 --> 00:20:02,300
O que eu estava dizendo?

211
00:20:06,860 --> 00:20:12,040
Vou voltar para o corpo e para a pré-história.

212
00:20:12,780 --> 00:20:28,200
Foi provado que os homens de Neanderthal tinham uma prática religiosa.

213
00:20:30,680 --> 00:20:36,340
Encontramos algumas sepulturas.

214
00:20:36,860 --> 00:20:40,440
Eles trouxeram oferendas aos seus mortos.

215
00:20:40,760 --> 00:20:43,680
Eles costumavam realizar uma cerimônia religiosa para seus mortos.

216
00:20:48,420 --> 00:20:52,180
O que é interessante não é só isso!

217
00:20:52,720 --> 00:20:56,260
Quando costumavam fazer estas cerimônias religiosas.

218
00:20:56,740 --> 00:21:01,700
- religiosos, no sentido de que se comunicavam com os invisíveis...

219
00:21:02,540 --> 00:21:06,940
- com vida eterna, com os mortos...

220
00:21:08,800 --> 00:21:11,020
- eles estavam se conectando ao invisível.

221
00:21:11,420 --> 00:21:18,140
Naquela época, porém, ainda não existia uma linguagem articulada.

222
00:21:18,560 --> 00:21:24,380
Portanto, a religião existia antes da linguagem articulada.

223
00:21:24,840 --> 00:21:30,140
(Estou disposto a passar à linguagem articulada...)

224
00:21:31,480 --> 00:21:36,500
(É muito interessante, levei quatro dias para escrevê-lo...)

225
00:21:43,820 --> 00:21:45,660
Meu mestre costumava dizer:

226
00:21:45,920 --> 00:21:48,880
"Zazen é a religião antes da religião". »

227
00:21:49,380 --> 00:21:54,380
Nós dizíamos: "Sim, você sempre quer ser o primeiro..."

228
00:21:55,860 --> 00:21:59,700
Mas essa é a religião antes da religião, como a conhecemos.

229
00:21:59,960 --> 00:22:02,720
Onde é feita referência a livros.

230
00:22:03,080 --> 00:22:06,280
Os livros vêm depois da religião, não antes.

231
00:22:06,720 --> 00:22:11,940
Antes, o homem não conseguia nem falar.

232
00:22:12,360 --> 00:22:15,060
Portanto, ele já estava nessa postura.

233
00:22:15,480 --> 00:22:19,620
Estas são posturas pré-históricas.

234
00:22:21,460 --> 00:22:23,740
(Quanto tempo me resta?)

235
00:22:23,960 --> 00:22:28,320
(Só me restam 3 minutos? Tudo bem, então!)

236
00:22:31,020 --> 00:22:34,460
Então, sobre a sofrologia...

237
00:22:36,500 --> 00:22:39,280
Eu queria falar com você sobre o zazen tradicional.

238
00:22:39,740 --> 00:22:43,660
Estou vestido como um monge tradicional.

239
00:22:47,440 --> 00:22:51,700
O zazen tradicional não é praticado sozinho.

240
00:22:52,100 --> 00:22:54,500
Não é uma técnica de bem-estar.

241
00:22:58,260 --> 00:23:05,060
Vou ler o que o Buda disse, eu acho ótimo!

242
00:23:08,520 --> 00:23:12,780
Shakyamuni Buda disse ao seu público:

243
00:23:13,120 --> 00:23:18,540
"O Estudo de Sutras e Ensinamentos" - textos -

244
00:23:18,900 --> 00:23:28,260
observância de preceitos" - nas religiões isto é importante, não matar, não roubar, etc. - é uma parte muito importante da "observância de preceitos". -

245
00:23:29,300 --> 00:23:34,960
"e até a prática do zazen, meditação sentada".

246
00:23:35,900 --> 00:23:43,060
"são incapazes de extinguir os desejos, medos e ansiedades humanas. »

247
00:23:43,340 --> 00:23:45,080
Isso foi o que disse Shakyamuni Buda.

248
00:23:45,340 --> 00:23:51,660
Então eu sou um fanático e ele é um chato...

249
00:23:52,080 --> 00:23:56,800
Zazen, até mesmo o Buda diz que é inútil!

250
00:23:57,600 --> 00:24:01,680
Então, o que vamos fazer a respeito disso?

251
00:24:14,440 --> 00:24:17,480
É claro, há uma suíte...

252
00:24:23,360 --> 00:24:29,360
Meu mestre [Deshimaru], quando conheceu seu mestre [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
Este último disse em uma conferência "Zazen é inútil, não traz nenhum mérito! ».

254
00:24:34,680 --> 00:24:38,800
Meu mestre pensou: "Mas é realmente interessante se for inútil! »

255
00:24:39,140 --> 00:24:41,940
"Estamos sempre fazendo coisas que são boas para alguma coisa! »

256
00:24:42,280 --> 00:24:43,760
"Algo que é inútil? »

257
00:24:44,120 --> 00:24:47,400
"Eu quero ir, eu quero fazer isso! »

