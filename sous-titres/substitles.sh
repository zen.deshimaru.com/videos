#! /bin/bash

youtube-dl --id --write-auto-sub --write-description  --sub-lang fr --skip-download https://www.youtube.com/channel/UC0gI9NJJsfRI3fymYgm9z3g
youtube-dl --all-subs --skip-download https://www.youtube.com/channel/UC0gI9NJJsfRI3fymYgm9z3g
for i in *.vtt; do ffmpeg -i $i $(basename $i .vtt).srt -y; done
git add *.srt
git commit -am "Add subtitles"
git push origin master
rm *.vtt
