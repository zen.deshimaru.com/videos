1
00:00:00,000 --> 00:00:06,820
Mostro un piccolo dettaglio della postura zazen,

2
00:00:07,200 --> 00:00:12,720
di André Lemort con il quale ho iniziato lo zazen nel 1969 con Monsieur Lambert,

3
00:00:13,120 --> 00:00:17,440
lo ha spiegato bene nel suo video e credo che
che è molto importante,

4
00:00:17,640 --> 00:00:21,540
è l’apertura dell’anca.

5
00:00:21,860 --> 00:00:26,880
Quando riporti la gamba indietro - io farò il loto a destra -

6
00:00:27,560 --> 00:00:34,840
così quando si fa il mezzo loto si fa scivolare un piede contro lo zafu,

7
00:00:35,180 --> 00:00:40,340
e tu metti l’altro contro l’inguine.

8
00:00:41,200 --> 00:00:48,600
L’importante è allentare l’anca.

9
00:00:49,160 --> 00:00:52,920
Lo stiamo già facendo più o meno, ma non abbastanza.

10
00:00:53,560 --> 00:00:58,640
Dobbiamo farlo dritto, a tutto gas!

11
00:00:59,200 --> 00:01:15,080
Basta sbloccarlo [l’anca] e prendere l’ischio in mano sotto le natiche e tirarsi fuori.

12
00:01:15,720 --> 00:01:25,440
In modo che apra il
bacino e questo cambia tutto, nel loto siamo davvero a nostro agio, siamo ben incastrati.

13
00:01:26,680 --> 00:01:32,840
Poi l’altra gamba.

14
00:01:35,000 --> 00:01:41,040
La gamba destra che sta sopra, faccio la stessa cosa.

15
00:01:41,480 --> 00:01:51,160
Mi alzerò in piedi e aprirò questo. Prendo l’ischio in mano e lo tiro fuori,

16
00:01:51,640 --> 00:01:58,960
Sparo a una natica e all’altra. Se tiri fuori l’osso, tiri fuori la pelle,

17
00:01:59,600 --> 00:02:05,600
così sei a posto e ora stai davvero tenendo duro.

18
00:02:05,720 --> 00:02:09,640
Siamo in una posizione davvero buona

19
00:02:09,720 --> 00:02:18,320
ti senti come se i perni fossero così [ben posizionati] sei davvero a tuo agio.

20
00:02:18,480 --> 00:02:21,160
Buon zazen a tutti!
