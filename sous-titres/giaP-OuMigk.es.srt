1
00:00:00,080 --> 00:00:00,920
Pregunta : 

2
00:00:01,140 --> 00:00:05,380
A veces siento en zazen meditación una sensación muy agradable. 

3
00:00:05,680 --> 00:00:09,880
Un pequeño hormigueo en todo el cuerpo. 

4
00:00:10,340 --> 00:00:15,040
Como pequeñas descargas eléctricas. 

5
00:00:15,440 --> 00:00:17,880
Respuesta del Maestro Kosen: 

6
00:00:18,400 --> 00:00:22,420
Todos los sentimientos pueden ser buenos o malos. 

7
00:00:23,080 --> 00:00:28,760
El mismo sentimiento puede parecer bueno o malo. 

8
00:00:29,220 --> 00:00:33,520
Lo importante es que es agradable. 

9
00:00:34,180 --> 00:00:37,240
Que te relaje. 

10
00:00:37,500 --> 00:00:42,000
Suele estar relacionado con la circulación de la sangre. 

11
00:00:42,340 --> 00:00:50,640
En la postura del loto, hay poca circulación sanguínea en las piernas. 

12
00:00:51,020 --> 00:00:55,960
Circula mucho más en la parte abdominal. 

13
00:00:56,400 --> 00:01:00,840
La parte superior del cuerpo, incluido el cerebro, está mejor irrigada. 

14
00:01:01,100 --> 00:01:05,240
Puede causar una sensación agradable. 

15
00:01:05,500 --> 00:01:09,660
Es bueno, porque es un pensamiento positivo. 

16
00:01:10,060 --> 00:01:13,580
Cuando te sientes bien, es una conciencia positiva. 

17
00:01:13,820 --> 00:01:16,240
También influye en nuestras vidas. 

18
00:01:16,480 --> 00:01:18,160
El maestro Deshimaru siempre decía: 

19
00:01:18,380 --> 00:01:20,500
“Zazen no es una mortificación. " 

20
00:01:20,840 --> 00:01:26,260
Pero cuando tienes que enfrentar tu cuerpo, 

21
00:01:26,620 --> 00:01:31,480
Algunas veces son desagradables o dolorosas. 

22
00:01:31,860 --> 00:01:34,940
Cuando te sientes bien, ¡es genial! 

