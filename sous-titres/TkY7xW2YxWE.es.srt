1
00:00:00,200 --> 00:00:03,460
A veces solo puedes encontrar eso en Zen, 

2
00:00:03,780 --> 00:00:06,680
cuando estamos juntos por un tiempo 

3
00:00:06,920 --> 00:00:08,960
que la gente carece de compasión 

4
00:00:09,180 --> 00:00:11,600
Creo que a veces hay que cambiar la forma de ver las cosas. 

5
00:00:11,980 --> 00:00:16,400
En lugar de pensar: "¡Oye, ese es el otro que me robó!" 

6
00:00:17,000 --> 00:00:20,680
pensando, "¿qué demonios se supone que debo hacer con esto?" 

7
00:00:21,180 --> 00:00:23,160
Y trata de darle la vuelta. 

8
00:00:24,360 --> 00:00:27,900
Eso es muy sincero. 

9
00:00:28,300 --> 00:00:30,340
Porque las personas son ellas mismas. 

10
00:00:30,600 --> 00:00:32,560
Intentan no jugar un papel: 

11
00:00:32,760 --> 00:00:36,900
"Estoy bien, estoy bien, estoy bien …" 

12
00:00:38,240 --> 00:00:41,760
Ciertamente hay momentos en los que nos enojamos. 

13
00:00:42,040 --> 00:00:45,280
Y donde nuestras reacciones se destacan. 

14
00:00:45,600 --> 00:00:50,780
Para mí es importante que no nos establezcamos allí. 

15
00:00:51,040 --> 00:00:52,880
No revolcarse en la idea: 

16
00:00:53,200 --> 00:00:55,700
"En un sesshin, no hay gente agradable". 

17
00:00:55,960 --> 00:00:59,320
Cuando llego a Sesshin, 

18
00:00:59,880 --> 00:01:04,060
mi idea es convertir todo en positivo. 

19
00:01:04,600 --> 00:01:07,340
¡No sé si puedo hacerlo todo el tiempo! 

20
00:01:08,360 --> 00:01:10,960
Para mi, todas las dificultades que surgen 

21
00:01:11,180 --> 00:01:16,960
sus invitaciones para continuar en su estado de ánimo. 

22
00:01:17,360 --> 00:01:24,680
Incluso diría que si somos injustamente criticados, 

23
00:01:24,980 --> 00:01:27,600
podemos sacar algo positivo de eso. 

24
00:01:27,900 --> 00:01:33,580
Por ejemplo, te obliga a tomarte menos en serio. 

25
00:01:37,620 --> 00:01:40,340
Lo que me gusta de esta sangha, 

26
00:01:40,640 --> 00:01:42,860
es que tratamos de escucharnos un poco más. 

27
00:01:43,140 --> 00:01:45,720
Porque lleva tiempo escucharse más, 

28
00:01:46,040 --> 00:01:50,460
para tener en cuenta algunos otros puntos de vista. 

29
00:01:50,760 --> 00:01:53,580
Sabemos que las cosas están mal 

30
00:01:53,980 --> 00:01:58,120
pero siempre nos esforzamos por mejorar. 

31
00:01:59,400 --> 00:02:07,120
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

