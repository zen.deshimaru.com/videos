1
00:00:00,000 --> 00:00:03,380
Om Zen te beoefenen is het nutteloos om naar Japan te gaan.

2
00:00:03,640 --> 00:00:07,360
Als mensen mij om informatie vragen in Dijon, vertel ik ze dat:

3
00:00:07,620 --> 00:00:09,240
“We gaan niet over Zen praten.”

4
00:00:09,500 --> 00:00:10,180
“Kom naar de dojo.”

5
00:00:10,440 --> 00:00:14,040
“Alleen dan zul je voelen of het je uitkomt of niet.”

6
00:00:14,360 --> 00:00:18,100
Vanaf de eerste keer dat je oefent,
je kunt voelen wat het brengt.

7
00:00:18,680 --> 00:00:21,800
Als je er zin in hebt, is dat een teken dat je het goed vindt.

8
00:00:22,000 --> 00:00:26,480
Er is geen behoefte aan theoretische verklaringen om te weten waarom we oefenen.

9
00:00:26,780 --> 00:00:35,280
Als we ons in Zazen vestigen, meteen…
je voelt dat de houding iets heeft dat rust geeft, dat kalmeert.

10
00:00:35,580 --> 00:00:40,660
De fysieke houding is echt belangrijk om het te voelen circuleren.

11
00:00:40,940 --> 00:00:46,960
Ik denk dat het vooral in verband staat met
deze balans, die ons in staat stelt ons over te geven.

12
00:00:47,240 --> 00:00:49,280
Zazen is ook een ervaring van stilte.

13
00:00:49,620 --> 00:00:59,800
Als we samen in stilte zijn,
Als je naar die stilte luistert, wordt er iets diepzinnigs overgebracht.

14
00:01:00,140 --> 00:01:03,140
Ik denk aan een verpleegster die volledig gestrest binnenkomt:

15
00:01:03,340 --> 00:01:05,840
“Ik kan de geest niet kalmeren!”

16
00:01:06,120 --> 00:01:08,840
Ze kwam er volledig kalm uit.

17
00:01:09,120 --> 00:01:14,620
Als we geleidelijk aan een diepe ademhaling kunnen vaststellen,

18
00:01:15,140 --> 00:01:22,300
het veegt al die vervelende gedachten, alle complicaties weg.

19
00:01:22,720 --> 00:01:29,720
Het is zelfs moeilijk te zeggen dat ik een boeddhist ben, want dat klinkt als een kerk.

20
00:01:30,280 --> 00:01:33,720
Als je dit niet nodig hebt om Zen te oefenen…

21
00:01:34,020 --> 00:01:38,800
In deze tijd is dat jammer,
mensen willen een seculiere meditatie.

22
00:01:39,220 --> 00:01:42,800
in Dijon zijn er vele soorten meditaties.

23
00:01:43,140 --> 00:01:47,360
Ik ken ze niet, ik kan er niet over praten.

24
00:01:47,560 --> 00:01:52,940
Maar persoonlijk vind ik het leuk om te oefenen zoals het wordt gedaan.

25
00:01:53,180 --> 00:01:54,340
Met een paar eenvoudige regels:

26
00:01:54,700 --> 00:01:56,780
Zeg hallo als we thuiskomen.

27
00:01:57,080 --> 00:01:58,880
Groet andere mensen waarmee je oefent.

28
00:01:59,120 --> 00:02:00,620
We doen onze meditatie.

29
00:02:00,880 --> 00:02:06,040
Daarna reciteren we een soetra om de
praktisch voor het hele universum.

30
00:02:06,300 --> 00:02:08,200
Het is een complete zaak.

31
00:02:08,520 --> 00:02:12,480
Het is een manier om niet alleen voor jezelf te oefenen.

32
00:02:13,500 --> 00:02:17,180
We luisteren niet altijd naar ons comfort.

33
00:02:17,460 --> 00:02:24,300
Terwijl mensen die een of andere vorm van meditatie willen, zijn ze al een keuze aan het maken.

34
00:02:24,560 --> 00:02:29,720
Ik hou van deze notie van “doelloosheid”, in deze tijd waarin alles de neiging heeft om winst te maken.

35
00:02:30,000 --> 00:02:32,180
We zitten in zazen, en dat is het.

36
00:02:32,420 --> 00:02:35,660
Ik heb het bericht van Meester Kosen ontvangen.

37
00:02:35,960 --> 00:02:38,180
Ik voel me vereerd, en ook geïntimideerd.

38
00:02:38,520 --> 00:02:42,800
Mensen stellen zich voor dat een meester zijn volledig moet worden gerealiseerd.

39
00:02:43,080 --> 00:02:45,180
Ik voel me niet zo.

40
00:02:45,440 --> 00:02:50,720
Ik ervaar deze overdracht als de wens om de sangha te blijven overbrengen en te helpen groeien.

41
00:02:51,040 --> 00:02:56,300
Onze sangha’s zijn alle
mensen die oefenen met Meester Kosen.

42
00:02:56,980 --> 00:03:04,360
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

