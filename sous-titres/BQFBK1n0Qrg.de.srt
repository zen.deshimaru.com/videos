1
00:00:09,675 --> 00:00:13,085
Hallo. Heute möchte ich Ihnen erklären

2
00:00:13,280 --> 00:00:15,940
wie man einen Meditationsspaziergang übt

3
00:00:16,760 --> 00:00:21,800
die wir im Zen praktizieren, wird dieser Gang kin-hin genannt.

4
00:00:22,180 --> 00:00:25,640
Es ist ein ziemlich kurzer Spaziergang,

5
00:00:25,920 --> 00:00:30,740
dass man zwischen zwei Momenten sitzender Meditation übt...

6
00:00:30,920 --> 00:00:32,220
zazen

7
00:00:32,460 --> 00:00:35,380
wie ich in einem früheren Video erklärt habe.

8
00:00:35,760 --> 00:00:39,220
Im Allgemeinen eine ganze Meditationssitzung

9
00:00:39,440 --> 00:00:43,380
beinhaltet einen ersten Teil der Sitzmeditation, Zazen,

10
00:00:43,660 --> 00:00:49,800
und dann dieser kleine Meditationsspaziergang, den ich Ihnen erklären werde...

11
00:00:50,060 --> 00:00:52,360
genannt kin-hin,

12
00:00:52,640 --> 00:00:54,820
und nach diesem kin-hin

13
00:00:54,820 --> 00:00:57,580
gehen wir dorthin zurück, wo unser Meditationskissen ist.

14
00:00:57,580 --> 00:01:00,600
zweitens

15
00:01:00,600 --> 00:01:04,460
von Zazen, mit dem Gesicht zur Wand, in Stille.

16
00:01:12,340 --> 00:01:15,640
Der Meditationsgang, kin-hin,

17
00:01:16,445 --> 00:01:17,915
Es ist ein Spaziergang,

18
00:01:18,480 --> 00:01:21,380
Sie werden Ihre Füße

19
00:01:21,760 --> 00:01:26,320
wenn man sich die Position meiner Füße so anschaut,

20
00:01:26,600 --> 00:01:30,900
d.h. sie haben einen Abstand, der mehr oder weniger der Breite Ihrer Faust entspricht,

21
00:01:31,780 --> 00:01:36,100
ohne Ihr Becken zu verzerren.

22
00:01:37,580 --> 00:01:41,180
Und Sie werden Schritte unternehmen

23
00:01:41,360 --> 00:01:44,480
einen halben Fuß breit

24
00:01:44,800 --> 00:01:47,540
ein Fuß ist plus oder minus 30-33 cm.

25
00:01:47,880 --> 00:01:51,160
also werden es etwa 15 Zentimeter Schritte oder so sein.

26
00:01:51,660 --> 00:01:54,480
Wenn Sie zusehen,

27
00:01:54,680 --> 00:01:57,660
Sehen Sie, ich mache einen Schritt von einem halben Fuß.

28
00:01:58,040 --> 00:02:03,580
Irgendwann habe ich also ein Bein oben und ein Bein unten...

29
00:02:03,840 --> 00:02:12,240
Das hintere Bein geht mit einem Schritt von der Breite eines halben Fußes nach vorn.

30
00:02:12,240 --> 00:02:15,880
Stellen Sie sicher, dass Ihre Füße

31
00:02:16,240 --> 00:02:18,560
gerade und nicht so.

32
00:02:27,700 --> 00:02:30,875
Jetzt die Handhaltung.

33
00:02:30,875 --> 00:02:33,685
Während wir gehen, wie ich gerade erklärt habe,

34
00:02:33,685 --> 00:02:36,820
wir haben auch eine Aktivität mit unseren Händen.

35
00:02:37,520 --> 00:02:40,300
Sie erinnern sich, die Zen-Meditationshaltung,

36
00:02:40,620 --> 00:02:42,520
wir hatten unsere Hände so.

37
00:02:43,140 --> 00:02:45,660
Ich nehme meine rechte Hand weg,

38
00:02:45,980 --> 00:02:49,180
Ich beuge den Daumen meiner linken Hand

39
00:02:49,500 --> 00:02:55,000
und ich umklammere ihn mit allen Fingern zusammengeklebt,

40
00:02:55,320 --> 00:03:01,700
Ich habe die Wurzel meines linken Daumens herausstehen lassen.

41
00:03:02,900 --> 00:03:05,140
Meine rechte Hand, die

42
00:03:05,480 --> 00:03:08,765
wickelt die linke Hand

43
00:03:08,765 --> 00:03:14,600
Finger zusammen, Daumen zusammen, alles zusammengeklebt...

44
00:03:14,800 --> 00:03:18,640
Ich beuge meine Handgelenke leicht

45
00:03:19,380 --> 00:03:22,320
Sehen Sie, die Wurzel meines linken Daumens kommt heraus...

46
00:03:22,940 --> 00:03:25,980
und diese Wurzel

47
00:03:26,340 --> 00:03:29,700
Ich werde es hier an der Basis des Brustbeins anwenden.

48
00:03:30,320 --> 00:03:35,180
Alle Menschen haben an der Basis des Brustbeins zwei oder drei kleine Löcher.

49
00:03:35,720 --> 00:03:44,200
An dieser Stelle werden Sie posieren und Kontakt zwischen der Wurzel Ihres linken Daumens und dem Plexus herstellen.

50
00:03:44,780 --> 00:03:55,440
Wie Sie sehen können, biege ich meine Handgelenke leicht ab, so dass meine Unterarme auf der gleichen Linie liegen.

51
00:03:55,860 --> 00:03:59,160
Ich habe nicht einen Arm über dem anderen.

52
00:03:59,600 --> 00:04:03,020
Die Handflächen meiner Hände sind parallel zum Boden,

53
00:04:03,540 --> 00:04:07,340
Ich meine, ich bin nicht so.

54
00:04:07,680 --> 00:04:11,140
Mir fallen die Arme nicht ab.

55
00:04:11,520 --> 00:04:16,700
und ich halte meine Arme nicht mit körperlicher Gewalt hoch, wenn ich meine Ellbogen anhebe.

56
00:04:17,800 --> 00:04:21,040
Was ist los? Was ist los?

57
00:04:21,560 --> 00:04:30,260
Denken Sie beim Ausatmen daran, dass Sie mit Ihrem gesamten Körpergewicht Druck auf das vordere Bein ausüben.

58
00:04:30,560 --> 00:04:33,640
Während dieser Phase,

59
00:04:34,200 --> 00:04:40,720
Auf die gleiche Weise werden wir unsere Hände zusammendrücken,

60
00:04:41,080 --> 00:04:49,220
leichten Druck ausüben und gleichzeitig die Wurzel des linken Daumens gegen das Plexus drücken.

61
00:04:49,580 --> 00:04:54,640
Offensichtlich, delikat, geht es nicht um Gewalt.

62
00:04:54,840 --> 00:04:59,700
Sie werden sehen, dass Sie, wenn Sie etwas zu niedrig sind, einen Punkt im Magen treffen

63
00:05:00,080 --> 00:05:04,340
werden Sie bald feststellen, dass es nicht angenehm ist

64
00:05:04,820 --> 00:05:09,720
also werden Sie natürlich aufstehen, um die richtige Körperhaltung zu finden.

65
00:05:13,600 --> 00:05:16,880
Wenn ich einatme, stelle ich mein Bein davor,

66
00:05:17,160 --> 00:05:18,460
Alles ist lose.

67
00:05:18,860 --> 00:05:21,880
Alles ist in Position, aber es ist lose.

68
00:05:22,300 --> 00:05:26,300
Während der Ausatmungsphase,

69
00:05:27,080 --> 00:05:34,440
Ich lege das Gewicht auf das vordere Bein, drücke leicht gegen den Plexus und beide Hände gegeneinander.

70
00:05:35,080 --> 00:05:40,920
Ich versetze mich ins Profil, wie Sie sehen können, es ist wie bei Zazen,

71
00:05:41,200 --> 00:05:49,180
Ich halte meine Wirbelsäule so gerade wie möglich, den Hals gestreckt, das Kinn eingezogen...

72
00:05:49,520 --> 00:05:53,840
mein Blick 45 Grad schräg nach unten,

73
00:05:54,460 --> 00:05:59,560
Ich bin total konzentriert, genau hier, genau jetzt, auf das, was ich tue.

74
00:06:00,200 --> 00:06:03,180
Und was ich tue, ich bin ein meditativer Spaziergang.

75
00:06:03,580 --> 00:06:08,420
Sie folgen nicht Ihren Gedanken, Sie schauen nicht nach draußen,

76
00:06:08,640 --> 00:06:14,260
fahren Sie mit der Konzentration von Zazen, der Konzentration der Sitzhaltung, fort.

77
00:06:22,020 --> 00:06:27,600
Der Rhythmus des Gehens wird der Rhythmus Ihrer Atmung sein.

78
00:06:28,120 --> 00:06:31,380
Sie erinnern sich, während der Sitzmeditation,

79
00:06:31,760 --> 00:06:35,840
Wir konzentrieren uns hier und jetzt auf die physischen Punkte der Körperhaltung,

80
00:06:36,500 --> 00:06:41,380
der Geisteszustand, die geistige Einstellung, Sie lassen Ihre Gedanken gehen,

81
00:06:41,640 --> 00:06:44,660
wir bedienen sie nicht

82
00:06:44,920 --> 00:06:48,000
und wir konzentrieren uns hier und jetzt auf unsere Atmung.

83
00:06:48,340 --> 00:06:51,560
durch die Schaffung eines harmonischen Hin und Her

84
00:06:51,960 --> 00:06:55,780
und sich besonders auf die Ausatmung zu konzentrieren...

85
00:06:57,160 --> 00:07:01,260
länger und länger, tiefer und tiefer,

86
00:07:01,580 --> 00:07:04,540
Bauch, der bis in den Unterbauch reicht.

87
00:07:05,860 --> 00:07:15,440
Die Atmung und die geistige Haltung sind die gleichen, ob wir sitzen, mit dem Gesicht zur Wand, in Stille, bewegungslos, in Zazen.

88
00:07:16,280 --> 00:07:24,660
oder diese Meditation beim Gehen durchzuführen. Das Einzige, was sich ändert, ist, dass wir umziehen werden.

89
00:07:30,320 --> 00:07:33,880
Gehen Sie nach dem Rhythmus Ihrer Atmung.

90
00:07:34,160 --> 00:07:41,660
Während der Inspiration gehen Sie mit einem Bein nach vorne

91
00:07:42,520 --> 00:07:45,000
einen halben Fuß

92
00:07:45,180 --> 00:07:50,220
und während der Auslaufphase,

93
00:07:52,440 --> 00:07:55,420
durch die Nase, lautlos,

94
00:07:56,100 --> 00:07:59,820
Sie werden nach und nach das volle Gewicht des Körpers

95
00:08:00,100 --> 00:08:03,240
auf dem Vorderbein

96
00:08:03,520 --> 00:08:06,880
das Vorderbein gut gestreckt ist

97
00:08:07,180 --> 00:08:11,480
die Fußsohlen, die den Boden vollständig berühren

98
00:08:12,080 --> 00:08:15,680
das Bein hinter ihr, bleiben Sie locker,

99
00:08:16,180 --> 00:08:20,440
sondern auch die gesamte mit dem Boden in Kontakt stehende Fußsohle.

100
00:08:20,740 --> 00:08:24,320
Wenn Sie den Fuß sehen, bin ich nicht mit der Ferse oben.

101
00:08:24,700 --> 00:08:29,860
Ich habe die Fersen beider Füße auf dem Boden ruhen.

102
00:08:30,480 --> 00:08:33,280
Also, während der Ausatmungsphase,

103
00:08:33,740 --> 00:08:40,180
Ich verlagere allmählich mein ganzes Körpergewicht auf dieses Vorderbein...

104
00:08:40,460 --> 00:08:43,520
bis zur Wurzel der Großzehe

105
00:08:43,920 --> 00:08:46,820
locker

106
00:08:47,380 --> 00:08:51,380
es ist nur das Gewicht, das mich auf den Boden schlägt.

107
00:08:51,760 --> 00:08:57,960
Als stünden Sie auf weichem Boden wie Sand oder Lehm...

108
00:08:58,280 --> 00:09:05,800
und Sie wollten Ihre Spuren im Sand oder im Lehm hinterlassen.

109
00:09:06,700 --> 00:09:10,420
Ich werde es Ihnen im Profil zeigen.

110
00:09:10,920 --> 00:09:14,580
Ich atme ein und lege mein Bein um,

111
00:09:15,100 --> 00:09:20,280
Ich atme aus, das ganze Gewicht meines Körpers auf meinem Vorderbein.

112
00:09:20,640 --> 00:09:23,420
Ich bin am Ende meiner Ausatmung,

113
00:09:23,680 --> 00:09:28,940
Ich atme ein und lege mein Bein hinter die Front...

114
00:09:29,960 --> 00:09:31,120
Ich verfalle

115
00:09:32,620 --> 00:09:37,340
das ganze Gewicht meines Körpers auf meinem Vorderbein.

116
00:09:37,780 --> 00:09:44,240
Ende der Ausatmung, ich atme ein, das hintere Bein geht nach vorne.

117
00:09:51,240 --> 00:09:55,380
Versuchen Sie es, und Sie werden sehen, dass der Kinnhin-Gang

118
00:09:55,640 --> 00:10:01,920
Es ist ein Spaziergang, den Sie dann unter vielen Umständen in Ihrem Leben nutzen können.

119
00:10:02,360 --> 00:10:12,360
5 Minuten warten, Sie sind gestresst und gut, üben Sie das, 3-4 Minuten dieses Spaziergangs.

120
00:10:12,660 --> 00:10:18,980
und Sie werden sehen, wie Sie sich beruhigen, sich mehr im Einklang mit sich selbst fühlen werden.

121
00:10:19,860 --> 00:10:21,240
Gute Praxis
