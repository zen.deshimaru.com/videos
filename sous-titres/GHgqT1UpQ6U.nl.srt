1
00:00:10,260 --> 00:00:23,160
Elke dag, over de hele wereld, beoefenen de leden van de Kosen sangha samen zazen .

2
00:00:24,360 --> 00:00:28,520
Ik heb jullie verteld over Meester Deshimaru’s jeugd.

3
00:00:28,820 --> 00:00:30,820
Van zijn twijfels, zijn onderzoek..

4
00:00:30,820 --> 00:00:35,760
Tot zijn ontmoeting met Kodo Sawaki en het vinden van zijn zazenhouding.

5
00:00:36,040 --> 00:00:39,100
Het vinden van je eigen zazenhouding:

6
00:00:40,240 --> 00:00:41,860
"Wat een genot!"

7
00:00:42,240 --> 00:00:46,100
Ik geloof dat degenen die hun zazenhouding hebben leren kennen…

8
00:00:46,100 --> 00:00:49,120
Hun tijd op deze planeet niet hebben verspild.

9
00:00:49,500 --> 00:00:52,540
Het is een onmetelijk kado..

10
00:00:52,540 --> 00:00:54,239
Zelfs als in deze houding…

11
00:00:54,540 --> 00:00:59,920
Er ook lijden en angst is.

12
00:01:00,320 --> 00:01:01,960
In deze houding is alles aanwezig..

13
00:01:01,960 --> 00:01:06,160
Maar je bekijkt het van bovenaf.

14
00:01:06,500 --> 00:01:10,740
Voordat hij de lessen van Kodo Sawaki leerde kennen..

15
00:01:12,040 --> 00:01:15,980
Had Meester Deshimaru de Bijbel bestudeerd.

16
00:01:17,360 --> 00:01:21,020
Hij was een beetje verliefd op een Nederlands meisje..

17
00:01:23,020 --> 00:01:24,820
De dochter van een dominee.

18
00:01:24,960 --> 00:01:28,380
Hij maakte van de gelegenheid gebruik om de Bijbel en het Engels te bestuderen.

19
00:01:28,720 --> 00:01:32,140
Eigenlijk studeerde hij Engels vanuit de bestudering van de bijbel.

20
00:01:32,140 --> 00:01:37,060
Hij bezocht christelijke kerken en zong hymnen.

21
00:01:37,220 --> 00:01:41,360
Ik vertel je dit omdat het Pasen is.

22
00:01:41,660 --> 00:01:51,880
En nu zit Jezus tussen zijn graf en de vierde of vijfde dimensie.

23
00:01:52,340 --> 00:01:56,580
Sensei vertelde ons dat toen hij naar Frankrijk kwam..

24
00:01:57,000 --> 00:02:02,360
Hij zijn bijbel en de hymnen van zijn jeugd mee had gebracht.

25
00:02:02,680 --> 00:02:07,940
Tijdens een Paas sesshin, gaf hij ons een kusen over Christus.

26
00:02:08,280 --> 00:02:11,720
Volgens het evangelie van Lucas..

27
00:02:11,720 --> 00:02:15,620
Zodat Christus..

28
00:02:16,140 --> 00:02:20,240
Zich kon uitspreken voor  Pontius Pilatus om te worden beoordeeld..

29
00:02:20,600 --> 00:02:26,240
Riep Pontius Pilatus de overpriesters op..

30
00:02:26,240 --> 00:02:28,820
En ondervroeg Jezus in hun aanwezigheid.

31
00:02:29,180 --> 00:02:30,340
Hij zegt tegen hen:

32
00:02:30,600 --> 00:02:33,020
"Je hebt deze man aan mij voorgesteld als..

33
00:02:33,020 --> 00:02:39,820
Iemand die het volk in de verkeerde richting leidt."

34
00:02:40,280 --> 00:02:47,640
"Ik heb hem ondervraagd en niets slechts gevonden in deze man."

35
00:02:48,000 --> 00:02:50,260
"Ik heb geen van de motieven gevonden waarvan je hem beschuldigt."

36
00:02:50,580 --> 00:02:56,940
"Herodes heeft ook geen reden tot beschuldiging gevonden."

37
00:02:57,260 --> 00:03:02,620
"Ik concludeer dat deze man niets gedaan heeft, wat de doodstraf te verdient."

38
00:03:03,240 --> 00:03:06,480
Op dat moment riep het volk in 1 stem uit:

39
00:03:06,480 --> 00:03:15,100
"Dood aan Jezus, laat Barabbas vrij!"

40
00:03:16,240 --> 00:03:24,280
"Barabbas was gevangen gezet voor wanordelijk gedrag en moord."

41
00:03:24,620 --> 00:03:29,100
Pontius Pilatus bood aan ..

42
00:03:29,100 --> 00:03:32,200
Om één van de gevangenen vrij te laten.

43
00:03:32,800 --> 00:03:38,480
Maar het volk zei: "Bevrijd Barabbas!"

44
00:03:38,940 --> 00:03:44,260
Pontius Pilatus spreekt de mensen weer aan:

45
00:03:44,260 --> 00:03:49,640
En hij zegt ze opnieuw en spreekt tot het volk:

46
00:03:49,960 --> 00:03:53,820
"Wees redelijk, Jezus heeft niets verkeerd gedaan..

47
00:03:53,820 --> 00:03:55,800
Hij praatte alleen maar, dat is alles!"

48
00:03:55,800 --> 00:03:59,620
Iedereen schreeuwde: "Kruisig hem!"

49
00:03:59,860 --> 00:04:02,320
Pontius Pilatus zei eindelijk:

50
00:04:02,620 --> 00:04:04,340
"Zoals u wenst..

51
00:04:04,340 --> 00:04:06,300
Het is niet mijn probleem.."

52
00:04:07,200 --> 00:04:11,640
En in antwoord sprak hij de beroemde zin:

53
00:04:11,640 --> 00:04:13,500
"Ik was mijn handen in onschuld!"

54
00:04:13,560 --> 00:04:17,540
Deze uitdrukking betekent dat..

55
00:04:17,540 --> 00:04:19,960
U uw handen zo vaak mogelijk moet wassen..

56
00:04:20,140 --> 00:04:22,320
Om het coronavirus te vermijden.

57
00:04:22,400 --> 00:04:24,920
Je moet elke keer zeggen: "Ik was mijn handen in onschuld!"

58
00:04:25,240 --> 00:04:29,880
Dit is een onderwijzing van Deshimaru, hij zegt dit.

59
00:04:30,360 --> 00:04:35,760
"Zo werd Jezus veroordeeld door de publieke opinie."

60
00:04:36,020 --> 00:04:39,140
"Massa psychologie doodde Christus."

61
00:04:39,380 --> 00:04:44,200
"Mimicry, weigering om zichzelf te bevragen..

62
00:04:44,200 --> 00:04:46,160
Dat doodde Christus."

63
00:04:46,300 --> 00:04:48,940
De massa, de menigte ..

64
00:04:48,940 --> 00:04:54,800
Bevat in zichzelf een krachtige energie..

65
00:04:54,800 --> 00:04:57,720
Die zowel negatief als ook gevaarlijk kan zijn.

66
00:04:57,800 --> 00:05:02,900
Als we bij elkaar komen om zazen te doen, is het anders.

67
00:05:03,180 --> 00:05:05,640
Ieder van ons kijkt naar zichzelf..

68
00:05:05,640 --> 00:05:11,820
Terwijl we in harmonie zijn met de groep.

69
00:05:12,160 --> 00:05:16,260
Dit is vooral voor nu erg belangrijk met Zazoom!

70
00:05:16,680 --> 00:05:20,520
Ik heb bijvoorbeeld wierook in mijn kamer gebrand.

71
00:05:20,920 --> 00:05:25,260
Sensei zei vaak: "In zazen ben je alleen."

72
00:05:25,640 --> 00:05:28,220
Maar als je in een dojo zazen beoefent..

73
00:05:28,220 --> 00:05:31,460
Zijn we in een groep, zelfs als we alleen zitten.

74
00:05:31,820 --> 00:05:35,260
Maar dan is er geen emotionele verwarring.

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki zei hier altijd over:

76
00:05:38,320 --> 00:05:41,640
"We moeten niet in groepsdwaasheid leven."

77
00:05:42,020 --> 00:05:45,680
"En deze aberratie niet houden voor de echte ervaring."

78
00:05:45,940 --> 00:05:48,480
"Dit is het lot van de gewone mensen..

79
00:05:48,480 --> 00:05:50,540
Die geen andere manier kennen om er naar te kijken..

80
00:05:50,860 --> 00:05:54,180
Dan met de ogen van de collectieve imbeciliteit."

81
00:05:54,700 --> 00:06:02,080
De Boeddha gaf één keer per week een openbare les.

82
00:06:02,600 --> 00:06:05,620
Alle discipelen, monniken en leken..

83
00:06:05,620 --> 00:06:08,360
Verzamelden zich buiten..

84
00:06:08,360 --> 00:06:11,000
Vast en zeker op bevoorrechte plaatsen.

85
00:06:11,880 --> 00:06:14,960
De Boeddha gaf een kusen.

86
00:06:15,320 --> 00:06:16,980
En iedereen zat in zazen..

87
00:06:17,960 --> 00:06:19,880
En luisterde naar hem..

88
00:06:19,980 --> 00:06:23,260
En daarna ging ieder weer zijns weegs.

89
00:06:23,760 --> 00:06:25,560
Iedereen beoefende alleen..

90
00:06:25,560 --> 00:06:27,960
Een week, een maand ..

91
00:06:28,240 --> 00:06:30,100
En keerde dan regelmatig terug..

92
00:06:30,280 --> 00:06:32,680
om te luisteren naar de woorden van de Boeddha.

93
00:06:32,680 --> 00:06:37,840
Pas in China verschenen er dojos.

94
00:06:38,180 --> 00:06:40,900
En begonnen we als groep te beoefenen..

95
00:06:40,900 --> 00:06:42,560
Met een orde en regels.

96
00:06:44,580 --> 00:06:48,280
En de groep is dus zeer krachtigs..

97
00:06:48,800 --> 00:06:50,960
Zowel voor het goede als slechte.

98
00:06:51,200 --> 00:06:53,020
Sensei zei in werkelijkheid:

99
00:06:53,020 --> 00:06:57,240
"Jezus Christus gaf zijn lichaam aan de smaak van de dood."

100
00:06:57,620 --> 00:07:01,700
"Door het diepe meer van het niets en de wanhoop."

101
00:07:02,180 --> 00:07:06,420
"Hij is een symbool van het eeuwige leven geworden."

102
00:07:06,740 --> 00:07:09,180
"Zijn leven is identiek aan dat van een Zen-koan."

103
00:07:09,380 --> 00:07:11,380
Dit is wat Sensei heeft gezegd.

