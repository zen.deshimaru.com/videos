1
00:00:06,180 --> 00:00:11,550
Ho praticato lo zazen per più di metà della mia vita.

2
00:00:14,890 --> 00:00:20,170
Ed è molto raro che io sia in perfetto equilibrio.

3
00:00:25,650 --> 00:00:29,445
Quindi ci sono pochissime possibilità

4
00:00:29,605 --> 00:00:33,910
che mi rendo totalmente conto del Buddha?

5
00:00:38,110 --> 00:00:40,910
Anche il Buddha aveva mal di schiena!

6
00:00:47,340 --> 00:00:54,790
Stiamo cercando di armonizzarci con qualcosa di perfetto e vivo.

7
00:00:55,110 --> 00:00:59,740
Ciò che è perfetto in noi è che siamo vivi.

8
00:01:01,860 --> 00:01:06,660
Ci sono persone che stanno pensando di fare
di modificazione genetica.

9
00:01:13,480 --> 00:01:22,120
Nelle favole si dice che il Buddha aveva
i trentadue segni della perfezione.

10
00:01:23,400 --> 00:01:27,440
Vale a dire che non aveva difetti.

11
00:01:41,780 --> 00:01:48,180
E' abbastanza buono per avvicinarsi,
per imitare il suo dio.

12
00:01:51,530 --> 00:01:55,780
Ma credo che il Buddha fosse un uomo proprio come noi.
e che aveva dei difetti.

13
00:02:02,690 --> 00:02:05,685
Il primo difetto è che è morto.

14
00:02:05,685 --> 00:02:08,710
Se non avesse avuto un difetto, non sarebbe morto.

15
00:02:11,630 --> 00:02:15,510
Morì, fece piangere tutti i suoi discepoli.

16
00:02:18,733 --> 00:02:21,406
Stiamo facendo zazen con un corpo imperfetto.

17
00:02:21,686 --> 00:02:24,100
Come un essere imperfetto.

18
00:02:26,130 --> 00:02:30,650
Ma ciò che è abbastanza grande è darci la direzione

19
00:02:32,670 --> 00:02:39,020
cellulare, muscolare, mentale.

20
00:02:39,670 --> 00:02:43,320
La direzione da seguire per avvicinarsi o imitare.

21
00:02:48,450 --> 00:02:51,840
I musulmani, per esempio, non indossano Buddha.

22
00:02:51,910 --> 00:02:55,900
Dicono: "Non abbiamo una statua,
nessuna immagine di Dio".

23
00:02:57,440 --> 00:02:59,165
Penso che sia abbastanza buono.

24
00:03:02,695 --> 00:03:07,520
Ma noi conosciamo la direzione per imitare Dio.

25
00:03:11,630 --> 00:03:18,510
Fare tutto il possibile per rendere il nostro essere divino.

26
00:03:22,120 --> 00:03:24,730
Ma non siamo perfetti
e moriremo.

27
00:03:27,000 --> 00:03:29,375
E ce ne sono alcune che non sono perfette…

28
00:03:29,375 --> 00:03:31,932
e altri che non sono PERFETTI.

29
00:03:34,552 --> 00:03:39,830
Spesso, le persone perfette non devono…

30
00:03:40,390 --> 00:03:43,060
Io, quando facevo musica…

31
00:03:46,120 --> 00:03:49,685
Racconto spesso questa storia.

32
00:03:50,445 --> 00:03:53,590
…avevo un produttore.

33
00:03:54,070 --> 00:03:58,640
Era un multimilionario italiano…

34
00:04:00,860 --> 00:04:04,415
Non importa cosa stesse facendo.

35
00:04:05,265 --> 00:04:10,450
Era molto ricco e voleva fondare una casa editrice.

36
00:04:13,480 --> 00:04:17,123
Si è offerto di produrre il mio disco.

37
00:04:19,480 --> 00:04:23,820
E non so perché, è passato di qui
al Temple de la Gendronnière.

38
00:04:24,780 --> 00:04:28,520
Durante i giorni di preparazione al campo estivo.

39
00:04:28,750 --> 00:04:33,050
Ha detto: "Volevo vedere cosa stai facendo!"

40
00:04:33,520 --> 00:04:36,215
Gli abbiamo mostrato le regole del dojo:

41
00:04:36,425 --> 00:04:39,417
"Entri così, poi ti siedi."

42
00:04:39,877 --> 00:04:42,420
Si siede: loto pieno diretto.

43
00:04:46,690 --> 00:04:48,450
Loto impeccabile.

44
00:04:48,570 --> 00:04:51,190
"Bene, vedo che hai capito tutto!"

45
00:04:51,190 --> 00:04:54,080
"Zazen sarà tra un'ora."

46
00:04:55,870 --> 00:04:58,580
"Potete visitare il tempio."

47
00:04:58,760 --> 00:05:02,870
Ha fatto il suo zazen per un'ora.

48
00:05:04,300 --> 00:05:06,060
- "Non è stato difficile?"

49
00:05:06,190 --> 00:05:08,720
- "No, no, è stato molto bello!"

50
00:05:11,590 --> 00:05:14,655
Giuro che è vero!

51
00:05:15,314 --> 00:05:18,080
E non ha mai più fatto zazen in vita sua.

52
00:05:19,420 --> 00:05:21,920
È lui che mi ha dato una lezione.

53
00:05:27,150 --> 00:05:32,332
Due o tre anni fa, mio nipote, più o meno a questa altezza…

54
00:05:32,632 --> 00:05:37,460
mi disse: "Nonno, fammi vedere zazen!"

55
00:05:40,840 --> 00:05:45,540
L'ho portato nella stanza
dove faccio zazen online attraverso Zoom.

56
00:05:47,140 --> 00:05:50,780
Ho detto: "Siediti qui, metti le gambe così."

57
00:05:51,000 --> 00:05:52,019
E' stressato.

58
00:05:52,115 --> 00:05:55,950
Assomiglia a Stephen Zeisler [il discepolo di Deshimaru].

59
00:05:58,050 --> 00:06:00,410
- "Ti metti così."

60
00:06:00,570 --> 00:06:04,130
Sto correggendo la sua postura.

61
00:06:04,400 --> 00:06:06,485
Ha un'ottima postura.

62
00:06:06,575 --> 00:06:08,750
Ho detto: "Basta così, basta così!"

63
00:06:08,910 --> 00:06:10,250
- "Grazie, nonno!"

64
00:06:15,755 --> 00:06:17,270
Ce l'ha.

65
00:06:17,350 --> 00:06:19,645
Lui sa cos'è lo zazen.

66
00:06:22,430 --> 00:06:25,660
Non so a quale domanda rispondessi…

67
00:06:27,345 --> 00:06:30,995
Esiste la piena realizzazione del Buddha?

68
00:06:33,520 --> 00:06:37,090
Tutto è la realizzazione totale del Buddha!

69
00:06:38,090 --> 00:06:42,240
Anche se siamo contorti, anche se non facciamo zazen.

70
00:06:43,130 --> 00:06:47,830
Ecco perché a volte è bene dire che lo zazen è inutile.

71
00:06:49,410 --> 00:06:55,340
Perché quando fai zazen in pantaloncini a casa

72
00:06:55,760 --> 00:07:01,180
niente kesa, niente cerimonia, niente amici,
senza un maestro, senza discepoli,

73
00:07:01,980 --> 00:07:07,360
Ti dici: "Sono gli stessi zazen
che facevo quando ero un novellino!"

74
00:07:07,970 --> 00:07:12,030
All'inizio, quando ero a casa e pensavo:

75
00:07:12,030 --> 00:07:14,410
"Ah, ho voglia di fare zazen!"

76
00:07:16,521 --> 00:07:17,751
Stavo pensando…
