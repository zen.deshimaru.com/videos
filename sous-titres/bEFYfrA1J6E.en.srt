1
00:00:08,660 --> 00:00:11,260
Good day everybody

2
00:00:11,560 --> 00:00:19,400
In order to know how to sit in zen-meditation, zazen..

3
00:00:19,800 --> 00:00:22,600
I will give you an explanation right away.

4
00:00:22,700 --> 00:00:27,880
Before all this, pay attention that the only thing you need..

5
00:00:28,340 --> 00:00:33,020
isa human being and a meditation cushion.

6
00:00:33,020 --> 00:00:36,680
If you have a meditation cushion, very good.

7
00:00:37,100 --> 00:00:40,560
And if not you can make one with some cloths.

8
00:00:43,060 --> 00:00:44,320
Here we go.

9
00:00:45,885 --> 00:00:48,855
So, the zen meditation..

10
00:00:49,620 --> 00:00:53,420
Before anything, you need to understand clearly that..

11
00:00:53,720 --> 00:00:57,580
Zen meditation is a seated meditation.

12
00:00:57,940 --> 00:01:00,820
Motionless and silent.

13
00:01:01,295 --> 00:01:04,425
And the particularity of this meditation is..

14
00:01:04,425 --> 00:01:07,445
That we sit in front of the wall.

15
00:01:08,400 --> 00:01:11,040
Like now, I sit in front of the camera but..

16
00:01:11,380 --> 00:01:15,900
But during the meditation you sit in front of the wall.

17
00:01:16,520 --> 00:01:21,940
So we are going to look to three different aspects.

18
00:01:24,860 --> 00:01:27,060
But you will notice that..

19
00:01:27,300 --> 00:01:32,680
The meditation is practicing these three aspects at the same time.

20
00:01:33,420 --> 00:01:36,955
The first theme is the physical posture..

21
00:01:36,955 --> 00:01:38,435
With the body.

22
00:01:39,175 --> 00:01:40,625
The second theme is..

23
00:01:41,020 --> 00:01:42,900
The respiration.

24
00:01:42,960 --> 00:01:45,580
Ho we breath during zen meditation.

25
00:01:46,300 --> 00:01:48,720
And the third aspect..

26
00:01:48,880 --> 00:01:50,320
The mental attitude.

27
00:01:50,620 --> 00:01:56,180
Meaning, what do we do while we are sitting immobile..

28
00:01:56,180 --> 00:01:59,080
And in silence in front of the wall.

29
00:02:00,040 --> 00:02:04,560
The body posture

30
00:02:06,160 --> 00:02:09,600
The body posture, the physical posture..

31
00:02:09,635 --> 00:02:12,585
The concrete posture of zen meditation.

32
00:02:12,585 --> 00:02:17,340
So you are sitting on a cushion.

33
00:02:17,760 --> 00:02:21,440
Not on the edge of the pillow but well on top.

34
00:02:21,440 --> 00:02:24,640
And you should pay attention to cross your legs..

35
00:02:24,640 --> 00:02:28,160
In such a way that your knees can ..

36
00:02:28,220 --> 00:02:32,540
Fall to or push the floor.

37
00:02:32,540 --> 00:02:35,140
Or that they go down.

38
00:02:35,440 --> 00:02:37,220
How do you do that?

39
00:02:37,840 --> 00:02:41,120
You take your leg, the way you can.

40
00:02:41,380 --> 00:02:44,020
You can put it this way or..

41
00:02:44,685 --> 00:02:47,435
Less stiff, you could place it here.

42
00:02:47,435 --> 00:02:50,585
The position we call medium lotus.

43
00:02:50,585 --> 00:02:54,980
And obviously you can also change the upper leg.

44
00:02:55,240 --> 00:02:59,040
Anyhow place it so you feel the most comfortable.

45
00:02:59,260 --> 00:03:02,240
Remember, we are not going to move.

46
00:03:02,500 --> 00:03:07,120
So we should find a position which is not too hard.

47
00:03:07,120 --> 00:03:10,600
Which will prevent us from ..

48
00:03:10,600 --> 00:03:13,940
And keep is quiet. So..

49
00:03:14,160 --> 00:03:17,460
The knees go to the ground.

50
00:03:17,655 --> 00:03:19,655
How do we do this?

51
00:03:19,660 --> 00:03:23,800
The pelvic tilts a bit to the front.

52
00:03:24,240 --> 00:03:27,415
So, what is happening?

53
00:03:27,420 --> 00:03:31,000
If the pelvis is like this to the front..

54
00:03:32,020 --> 00:03:36,180
Because of the gravity behind my back..

55
00:03:36,180 --> 00:03:39,020
Or like when I am conducting a car..

56
00:03:39,020 --> 00:03:41,025
And like you can see..

57
00:03:41,025 --> 00:03:43,860
My knees are in the air.

58
00:03:43,860 --> 00:03:47,060
And I want that my knees go downwards.

59
00:03:47,060 --> 00:03:51,700
In zen we say: "press the earth with your knees."

60
00:03:51,900 --> 00:03:54,940
And this movement, with the  gravity..

61
00:03:55,040 --> 00:03:57,300
Going downwards..

62
00:03:57,300 --> 00:04:03,120
With this light tilt of the pelvis we see..

63
00:04:03,460 --> 00:04:06,995
There is no intervention with the muscles.

64
00:04:06,995 --> 00:04:10,355
Looking form it in profile..

65
00:04:12,355 --> 00:04:15,315
In case you ar like this..

66
00:04:15,780 --> 00:04:19,160
Where the pelvis is turned to the front..

67
00:04:20,220 --> 00:04:21,700
Twist it a little..

68
00:04:22,720 --> 00:04:23,780
Just towards here.

69
00:04:24,960 --> 00:04:27,040
Perfectly straight.

70
00:04:27,040 --> 00:04:30,080
If you sit like this naturally..

71
00:04:30,080 --> 00:04:31,740
The center of  gravity is..

72
00:04:31,740 --> 00:04:34,875
Coming more to the front..

73
00:04:34,875 --> 00:04:37,555
And your knees will fall to the floor.

74
00:04:39,555 --> 00:04:42,595
From this tilted pelvis..

75
00:04:42,945 --> 00:04:45,725
You straighten your spine..

76
00:04:46,165 --> 00:04:48,885
As well as you can.

77
00:04:49,600 --> 00:04:52,100
In this respect we speak about..

78
00:04:52,100 --> 00:04:55,460
Pushing the sky..

79
00:04:55,460 --> 00:04:58,700
With the top of our head.

80
00:05:04,620 --> 00:05:06,680
In zen the spine..

81
00:05:07,560 --> 00:05:10,720
Starts or stops (like you want) here.

82
00:05:10,720 --> 00:05:14,020
If we straighten our spine..

83
00:05:14,040 --> 00:05:17,100
That means, try to stretch it up till there.

84
00:05:17,380 --> 00:05:18,740
So we are not going to..

85
00:05:19,260 --> 00:05:21,400
Meditate with the head ..

86
00:05:22,335 --> 00:05:25,525
Falling down, neither falling backwards.

87
00:05:26,015 --> 00:05:28,255
The chin should be..

88
00:05:29,080 --> 00:05:31,240
The most straight possible.

89
00:05:31,960 --> 00:05:35,920
We speak about keeping a straight horizontal line.

90
00:05:36,280 --> 00:05:38,320
The eyes..

91
00:05:39,285 --> 00:05:40,345
The mouth..

92
00:05:41,015 --> 00:05:42,905
A vertical line..

93
00:05:43,305 --> 00:05:45,125
Between the eyes, the mouth and ..

94
00:05:45,605 --> 00:05:46,695
The belly button.

95
00:05:48,075 --> 00:05:49,075
Good.

96
00:05:49,880 --> 00:05:53,840
Once I sit the most straight possible..

97
00:05:54,060 --> 00:05:56,365
The posture of the hands..

98
00:05:56,365 --> 00:05:58,495
The left hand

99
00:05:59,765 --> 00:06:01,175
Fingers together..

100
00:06:01,575 --> 00:06:04,725
You put in your right hand.

101
00:06:04,920 --> 00:06:06,380
Fingers together.

102
00:06:06,600 --> 00:06:09,740
Because the fingers lay on top of each other.

103
00:06:11,765 --> 00:06:13,975
A horizontal line with..

104
00:06:13,975 --> 00:06:17,115
The thumbs, touching each other.

105
00:06:17,360 --> 00:06:21,280
They touch for example to hold a feather.

106
00:06:21,280 --> 00:06:22,480
So no need for..

107
00:06:22,885 --> 00:06:24,925
Muscle pressure.

108
00:06:25,915 --> 00:06:29,125
And the inside of our hands.

109
00:06:29,905 --> 00:06:33,295
We put at the base, here..

110
00:06:33,675 --> 00:06:36,545
More or less in the middle of the belly.

111
00:06:36,760 --> 00:06:40,900
So in order to maintain this posture..

112
00:06:41,620 --> 00:06:47,740
You have to get any piece of cloth

113
00:06:48,140 --> 00:06:51,800
Which will help you to..

114
00:06:53,680 --> 00:06:56,675
Let go your hands

115
00:06:56,680 --> 00:07:00,920
In a free way against your belly..

116
00:07:01,180 --> 00:07:06,080
Without feeling obliged to use your fingers and shoulders..

117
00:07:06,400 --> 00:07:09,600
On the contrary, you can let go all tensions.

118
00:07:10,180 --> 00:07:12,780
The elbows are free..

119
00:07:12,995 --> 00:07:14,985
I will put myself in profile..

120
00:07:15,355 --> 00:07:17,065
So you can see well.

121
00:07:17,485 --> 00:07:19,105
We recapture

122
00:07:20,125 --> 00:07:22,925
Straight..

123
00:07:23,820 --> 00:07:27,460
The hands, left on top of right.

124
00:07:27,740 --> 00:07:31,000
The thumbs. And the inside of the hands..

125
00:07:31,000 --> 00:07:34,480
At the base of the abdomen.

126
00:07:35,140 --> 00:07:36,460
Elbows straight.

127
00:07:37,985 --> 00:07:38,985
Good.

128
00:07:40,120 --> 00:07:42,540
This is the posture..

129
00:07:43,220 --> 00:07:47,000
Roughly, let’s say..

130
00:07:47,640 --> 00:07:49,840
Of the material, the body.

131
00:07:49,840 --> 00:07:53,240
Once I’ve put my body..

132
00:07:54,345 --> 00:07:57,225
In the meditation posture..

133
00:07:57,225 --> 00:08:00,455
You remember, in silence in front of a wall.

134
00:08:01,280 --> 00:08:02,680
What happens?

135
00:08:02,960 --> 00:08:04,520
What will happen is that..

136
00:08:04,760 --> 00:08:08,160
In front of the wall, nothing interesting happens.

137
00:08:08,640 --> 00:08:12,380
We speak about turning your attention inwards.

138
00:08:14,360 --> 00:08:17,200
So about your gaze during zazen:

139
00:08:17,975 --> 00:08:20,795
Eyes are semi-open..

140
00:08:21,085 --> 00:08:24,345
45 degrees in a diagonal..

141
00:08:24,345 --> 00:08:25,820
Downwards.

142
00:08:26,000 --> 00:08:28,985
It’s like taking your vision..

143
00:08:28,985 --> 00:08:32,155
And putting it in front of you.

144
00:08:32,245 --> 00:08:34,785
There is nothing interesting to see.

145
00:08:35,160 --> 00:08:38,920
Then we will do the same with the other senses.

146
00:08:39,160 --> 00:08:43,180
If we speak about a silent meditation..

147
00:08:44,440 --> 00:08:48,780
You don’t need anything. No music, nothing.

148
00:08:49,080 --> 00:08:51,580
An interior silence.

149
00:08:52,220 --> 00:08:55,680
It is a silent meditation, so..

150
00:08:56,475 --> 00:08:58,315
There is also no need to speak.

151
00:08:59,235 --> 00:09:00,235
Conclusion..

152
00:09:00,900 --> 00:09:02,880
When I am immobile..

153
00:09:02,880 --> 00:09:06,760
My vision is of no use..

154
00:09:06,760 --> 00:09:09,000
Nor is my hearing

155
00:09:09,280 --> 00:09:10,820
or my words.

156
00:09:10,980 --> 00:09:14,280
So the conclusion is that..

157
00:09:14,500 --> 00:09:17,920
Our senses with which we sense the world around us..

158
00:09:17,920 --> 00:09:21,300
We put them in ’airplane mode’.

159
00:09:21,300 --> 00:09:23,980
We could say it like that. In standbye.

160
00:09:24,300 --> 00:09:27,400
And this is the principle of the posture..

161
00:09:27,400 --> 00:09:30,140
Through establishing..

162
00:09:30,740 --> 00:09:33,675
This physical posture and..

163
00:09:33,675 --> 00:09:36,775
To cut the exterior..

164
00:09:36,775 --> 00:09:39,915
To cut it with our feelings.

165
00:09:40,060 --> 00:09:43,940
Then we can start to really meditate.

166
00:09:51,140 --> 00:09:55,440
Now that we are having a more quiet posture..

167
00:09:56,040 --> 00:09:59,380
I am in front of a wall, meditating..

168
00:09:59,685 --> 00:10:01,975
And with this silence..

169
00:10:02,495 --> 00:10:04,675
I am taking notice of the fact that..

170
00:10:05,480 --> 00:10:09,440
My head is very active.

171
00:10:09,780 --> 00:10:12,880
So I take into account that here..

172
00:10:13,120 --> 00:10:17,080
I have many thoughts..

173
00:10:17,080 --> 00:10:22,740
Which will vanish with our gaze, you could say it like that.

174
00:10:23,040 --> 00:10:26,240
Thoughts, visual images..

175
00:10:26,245 --> 00:10:28,235
Sounds..

176
00:10:28,865 --> 00:10:29,945
Many things.

177
00:10:31,420 --> 00:10:33,520
So what we are going to do..

178
00:10:33,720 --> 00:10:37,075
In our zen meditation is..

179
00:10:37,080 --> 00:10:40,205
To let go these thoughts.

180
00:10:40,205 --> 00:10:43,180
Which is a fundamental point..

181
00:10:43,180 --> 00:10:45,500
Of the zen meditation posture.

182
00:10:45,640 --> 00:10:49,200
While I am like this in zazen..

183
00:10:49,200 --> 00:10:53,380
I am not passing my time, reflecting on..

184
00:10:53,380 --> 00:10:57,240
Whatever, maybe interesting, maybe not..

185
00:10:57,420 --> 00:10:59,340
It’s not about that.

186
00:10:59,580 --> 00:11:03,700
It’s about, the same as we have put our body..

187
00:11:03,715 --> 00:11:06,685
In a quiet situation..

188
00:11:06,825 --> 00:11:10,215
I will put my mind and mental..

189
00:11:10,505 --> 00:11:13,415
Which is always agitated..

190
00:11:13,415 --> 00:11:16,035
I will put it..

191
00:11:16,035 --> 00:11:19,035
In the same way..

192
00:11:19,040 --> 00:11:20,500
In a quiet state of being.

193
00:11:20,680 --> 00:11:23,640
Like simply said..

194
00:11:23,640 --> 00:11:26,620
Letting my thoughts go.

195
00:11:27,760 --> 00:11:31,800
Our thoughts sprout from without.

196
00:11:32,680 --> 00:11:35,200
To demonstrate this; if we ask ourselves..

197
00:11:35,200 --> 00:11:39,040
What are we going to think in 5 minutes or in half an hour..

198
00:11:39,305 --> 00:11:42,575
Or tomorrow? Nobody can know..

199
00:11:43,440 --> 00:11:46,780
So, even while we are sleeping..

200
00:11:46,960 --> 00:11:50,080
We have dreams. So we always continue..

201
00:11:50,540 --> 00:11:53,440
A mental activity, consciously or not.

202
00:11:53,660 --> 00:11:57,000
During zen meditation this mental activity..

203
00:11:57,165 --> 00:12:00,445
We are going to bring it down and calm it.

204
00:12:00,760 --> 00:12:04,760
Like we say; it’s to let your thoughts pass.

205
00:12:05,100 --> 00:12:07,520
But then you will say, ok very good but..

206
00:12:07,520 --> 00:12:08,900
How to do this?

207
00:12:09,080 --> 00:12:12,920
This we can do through our breathing.

208
00:12:13,240 --> 00:12:16,400
That brings me to the third point;

209
00:12:16,440 --> 00:12:19,400
How to breathe during zazen?

210
00:12:19,400 --> 00:12:23,920
Zen breathing

211
00:12:26,740 --> 00:12:29,660
Breathing during zen meditation.

212
00:12:29,880 --> 00:12:32,560
It’s an abdominal breathing.

213
00:12:34,415 --> 00:12:36,265
We inspire..

214
00:12:36,705 --> 00:12:39,985
We refill our lung capacity

215
00:12:39,985 --> 00:12:42,185
With some generosity.

216
00:12:42,925 --> 00:12:45,245
And we will concentrate ourselves..

217
00:12:45,245 --> 00:12:46,740
Especially..

218
00:12:47,120 --> 00:12:49,900
On our expiration

219
00:12:50,200 --> 00:12:52,560
To return the air.

220
00:12:52,740 --> 00:12:55,875
This way of exhalation, little by little..

221
00:12:55,875 --> 00:12:59,165
We try to make it long.

222
00:12:59,225 --> 00:13:02,275
A long time of exhalation.

223
00:13:02,345 --> 00:13:05,135
Meaning, to exhale the air..

224
00:13:05,140 --> 00:13:06,720
Through the nose..

225
00:13:07,000 --> 00:13:10,360
Softly without making noise.

226
00:13:10,700 --> 00:13:13,080
We are in a silent meditation..

227
00:13:13,300 --> 00:13:16,240
We exhale the air, through the nose..

228
00:13:17,160 --> 00:13:21,260
Trying to extend the exhalation.

229
00:13:21,620 --> 00:13:23,880
And at the end of your exhalation..

230
00:13:24,625 --> 00:13:27,785
We breathe in and again..

231
00:13:36,465 --> 00:13:39,565
We let go the air and again…

232
00:13:43,440 --> 00:13:46,980
So if you focus your concentration on..

233
00:13:47,240 --> 00:13:50,420
the coming and going, you establish..

234
00:13:50,820 --> 00:13:53,500
you calm your respiration..

235
00:13:54,540 --> 00:13:57,495
You should leave it quietly..

236
00:13:57,500 --> 00:14:02,600
You should enlarge the exhalation..

237
00:14:03,160 --> 00:14:07,180
And that makes that your diaphragm is going down.

238
00:14:07,480 --> 00:14:08,740
It goes down.

239
00:14:08,980 --> 00:14:11,980
It will give an inner massage to the intestines..

240
00:14:11,980 --> 00:14:13,980
Which will do them good.

241
00:14:14,160 --> 00:14:17,700
And slowly you can feel..

242
00:14:18,160 --> 00:14:20,820
As if something is inflating.

243
00:14:21,145 --> 00:14:23,720
In the zone where we have put our hands.

244
00:14:24,020 --> 00:14:27,700
So, it’s here that the things are happening.

245
00:14:28,040 --> 00:14:30,520
So it is not here, but it is ..

246
00:14:31,900 --> 00:14:34,925
Down there.

247
00:14:34,925 --> 00:14:39,360
Breathing through the nose and in silence.

248
00:14:46,440 --> 00:14:49,900
The physical posture: cross your legs.

249
00:14:50,020 --> 00:14:52,860
The knees going down to the floor.

250
00:14:53,420 --> 00:14:56,140
The pelvis slightly twisted to the front.

251
00:14:56,700 --> 00:15:00,760
We straighten our spine the best we can.

252
00:15:01,160 --> 00:15:02,840
Up to the crown of our head..

253
00:15:02,840 --> 00:15:08,040
We push the sky with our head.

254
00:15:08,540 --> 00:15:11,800
The left hand inside the right hand.

255
00:15:11,925 --> 00:15:13,925
The thumbs in a horizontal line..

256
00:15:13,925 --> 00:15:17,700
Not as a valley, not as a mountain, a top.

257
00:15:17,960 --> 00:15:20,100
Just straight.

258
00:15:20,100 --> 00:15:24,140
When you put your hands in contact with the belly.

259
00:15:24,500 --> 00:15:26,640
You use something under..

260
00:15:26,665 --> 00:15:28,675
To have a more comfortable position.

261
00:15:29,125 --> 00:15:31,805
You have your elbows straight.

262
00:15:31,805 --> 00:15:32,885
This is our physical posture.

263
00:15:33,645 --> 00:15:37,065
Secondly, our mental state of mind.

264
00:15:37,280 --> 00:15:41,760
We cut ourselves from the exterior.

265
00:15:42,160 --> 00:15:44,060
From sounds..

266
00:15:44,060 --> 00:15:46,760
We are not going to meditate with a radio.

267
00:15:46,760 --> 00:15:48,755
Or with your mobile on.

268
00:15:48,755 --> 00:15:51,635
So turn them off, but that’s logic.

269
00:15:52,040 --> 00:15:54,620
We cut out all sounds.

270
00:15:54,620 --> 00:15:58,295
We bring our look inwards and we come back to..

271
00:15:58,300 --> 00:16:02,040
Being the observer of what is going on from the inside.

272
00:16:02,120 --> 00:16:05,000
And what is happening inside?

273
00:16:05,380 --> 00:16:07,180
We notice that there is a mental activity.

274
00:16:07,660 --> 00:16:10,720
Thoughts are burning..

275
00:16:10,725 --> 00:16:12,665
We can not control them.

276
00:16:13,155 --> 00:16:16,415
The thoughts arise automatically.

277
00:16:16,440 --> 00:16:21,000
So it is not about preventing this arousal.

278
00:16:21,380 --> 00:16:24,740
Better said, it is about..

279
00:16:24,740 --> 00:16:29,740
Not jumping on the train of our thoughts.

280
00:16:29,960 --> 00:16:33,420
And we speak of letting them pass.

281
00:16:33,500 --> 00:16:36,385
How can we let them pass?

282
00:16:36,385 --> 00:16:41,380
First of all, with the concentration of the here and the now..

283
00:16:41,540 --> 00:16:43,260
On our physical posture.

284
00:16:43,795 --> 00:16:46,835
And with this concentration..

285
00:16:46,840 --> 00:16:51,640
Here and now, on my breathing.

286
00:16:52,080 --> 00:16:54,960
I am fully aware of my breathing..

287
00:16:55,265 --> 00:16:58,505
Of my abdominal breathing.

288
00:16:58,540 --> 00:17:02,580
So we breathe through the nose..

289
00:17:03,080 --> 00:17:04,520
We inhale..

290
00:17:05,095 --> 00:17:08,385
We try to extend the exhalation.

291
00:17:08,900 --> 00:17:10,460
Each time..

292
00:17:10,700 --> 00:17:14,040
Softer, longer.

293
00:17:14,740 --> 00:17:17,735
And at the end of the exhalation we start again.

294
00:17:17,740 --> 00:17:19,800
It’s coming and going like..

295
00:17:20,080 --> 00:17:23,920
A wave in the ocean. The waves.

296
00:17:24,360 --> 00:17:28,340
But.. long, long, long.

297
00:17:29,180 --> 00:17:32,605
If you are concentrating on the here and now..

298
00:17:32,605 --> 00:17:36,620
On the points of the posture

299
00:17:36,920 --> 00:17:41,140
Meaning that we are not sleeping, during the meditation..

300
00:17:41,680 --> 00:17:44,880
And if you concentrate at the same time..

301
00:17:45,040 --> 00:17:48,500
On establishing your breathing..

302
00:17:48,680 --> 00:17:51,140
In a harmonised and quiet way.

303
00:17:51,140 --> 00:17:53,780
And with an extended exspiration..

304
00:17:53,940 --> 00:17:57,680
You will not have time to think in yourself or..

305
00:17:57,800 --> 00:18:00,060
Or in anything else, however interesting it may be.

306
00:18:00,295 --> 00:18:03,365
So the conclusion is..

307
00:18:03,365 --> 00:18:05,665
We will meditate 20 minutes, half an hour, one hour..

308
00:18:06,820 --> 00:18:08,540
You decide.

309
00:18:08,720 --> 00:18:12,380
But this moment of meditation, of concentration..

310
00:18:12,455 --> 00:18:15,635
Will give you immediate benefits.

311
00:18:15,635 --> 00:18:17,255
You will feel yourself..

312
00:18:18,405 --> 00:18:21,405
A little bit more quiet.

313
00:18:21,405 --> 00:18:24,665
A bit lighter.

314
00:18:24,665 --> 00:18:27,355
And even better..

315
00:18:27,355 --> 00:18:29,355
You will create a distance between ..

316
00:18:29,355 --> 00:18:32,445
yourself and all the emotions which arise in your life.

317
00:18:32,885 --> 00:18:34,395
And this..

318
00:18:34,785 --> 00:18:37,725
It’s really a good tool

319
00:18:38,135 --> 00:18:41,015
Most of the time..

320
00:18:41,015 --> 00:18:44,055
But especially now..

321
00:18:44,060 --> 00:18:48,420
That half of our population is confined.

322
00:19:29,920 --> 00:19:34,960
Advice for a beginner

323
00:19:35,300 --> 00:19:40,380
In stead of getting angry or making a fuss..

324
00:19:40,380 --> 00:19:45,380
In steeds of spreading thoughts more or less..

325
00:19:45,380 --> 00:19:48,160
Of a lower dimension..

326
00:19:48,420 --> 00:19:49,840
Sit down.

327
00:19:49,995 --> 00:19:53,325
In front of a wall. Adopt this meditation.

328
00:19:54,440 --> 00:19:58,320
Work on yourselves in this way.

329
00:19:58,760 --> 00:20:00,620
And you will soon notice..

330
00:20:00,625 --> 00:20:02,925
How well it will do you.

