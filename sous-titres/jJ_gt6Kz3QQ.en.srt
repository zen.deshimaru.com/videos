1
00:00:03,420 --> 00:00:07,900
What is spirit? 

2
00:00:08,220 --> 00:00:16,200
In Chinese and Japanese, various ideograms are translated as "ghost." 

3
00:00:16,880 --> 00:00:19,340
Sometimes it is translated as "heart". 

4
00:00:21,960 --> 00:00:23,560
It is every existence. 

5
00:00:23,820 --> 00:00:25,240
It is the living community. 

6
00:00:25,500 --> 00:00:27,400
This is the common spirit of the living community. 

7
00:00:27,740 --> 00:00:28,900
That’s the spirit. 

8
00:00:29,120 --> 00:00:30,780
There is only one mind. 

9
00:00:32,000 --> 00:00:34,960
What creates the mind? 

10
00:00:35,740 --> 00:00:37,780
From a Buddhist point of view, nothing creates the mind. 

11
00:00:38,100 --> 00:00:40,540
Because the mind is without birth. 

12
00:00:40,880 --> 00:00:46,780
It is without cause or disappearance. 

13
00:00:47,060 --> 00:00:50,020
The mind is created, but not created. 

14
00:00:50,500 --> 00:00:54,220
Is the mind the first cause? 

15
00:00:54,460 --> 00:00:57,820
There cannot be a first cause. 

16
00:00:58,100 --> 00:00:59,940
Because the origin is pure. 

17
00:01:02,280 --> 00:01:05,420
What would have been the first cause? 

18
00:01:05,840 --> 00:01:11,200
We understand, even intellectually, that if we go in the past, we cannot find the first cause. 

19
00:01:11,660 --> 00:01:13,300
So the first cause does not exist. 

20
00:01:13,660 --> 00:01:15,640
The first cause is the effect. 

21
00:01:16,060 --> 00:01:16,900
Now. 

22
00:01:17,440 --> 00:01:20,940
Where does the ghost reside? 

23
00:01:21,280 --> 00:01:23,120
The mind always exists and resides everywhere. 

24
00:01:23,420 --> 00:01:25,200
Whether we are in Zazen or elsewhere. 

25
00:01:25,400 --> 00:01:30,140
There is no special meditation that changes the structure of the mind. 

26
00:01:30,460 --> 00:01:32,240
The mind is always present. 

27
00:01:32,520 --> 00:01:35,780
What is "mind-body"? 

28
00:01:36,520 --> 00:01:38,920
Matter and energy are not separate from each other. 

29
00:01:39,280 --> 00:01:44,600
They are two different expressions of the same thing. 

30
00:01:47,080 --> 00:01:50,360
So body and mind are two different expressions of the same thing. 

31
00:01:50,760 --> 00:01:52,280
You should not denigrate the body. 

32
00:01:52,640 --> 00:01:53,980
The body is the mind. 

33
00:01:54,340 --> 00:01:55,720
Matter is the mind. 

34
00:01:56,220 --> 00:02:00,520
What is the effect of the thought on the spiritual body? 

35
00:02:01,240 --> 00:02:06,120
Thought materializes in reality. 

36
00:02:07,020 --> 00:02:09,100
In the dream of matter. 

37
00:02:09,580 --> 00:02:11,040
Dreaming doesn’t just happen when you’re sleeping. 

38
00:02:11,480 --> 00:02:15,580
The dream of matter is what we call “reality”. 

39
00:02:15,900 --> 00:02:17,880
Material reality. 

40
00:02:18,400 --> 00:02:21,980
What can we conclude from this? 

41
00:02:22,340 --> 00:02:27,080
The mind always sees in the mind. 

