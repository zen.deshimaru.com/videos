1
00:00:00,100 --> 00:00:02,020
Zen ist Zazen! 

2
00:00:02,220 --> 00:00:04,920
Zazen sitzt wie ein Buddha. 

3
00:00:05,160 --> 00:00:08,400
Nehmen Sie die ursprüngliche Buddha-Pose ein. 

4
00:00:08,620 --> 00:00:09,920
Nimm sein Herz. 

5
00:00:10,140 --> 00:00:12,360
Setzen Sie sich in diese Position. 

6
00:00:12,660 --> 00:00:16,000
Und vergiss allmählich, dass wir ein schmerzhafter Körper sind. 

7
00:00:16,320 --> 00:00:18,840
Alles verdreht, mit einem komplizierten Verstand. 

8
00:00:19,080 --> 00:00:23,120
Und lass die Transmutation geschehen, die Alchemie von Zazen. 

9
00:00:23,760 --> 00:00:25,700
Nur Menschen können trainieren. 

10
00:00:26,060 --> 00:00:33,200
Es ist unsere Entwicklung über Hunderttausende von Jahren, die es ermöglicht, diese Haltung zu praktizieren. 

11
00:00:33,520 --> 00:00:36,700
Gleichgewicht, Stabilität, Ruhe. 

12
00:00:37,000 --> 00:00:40,200
Dehnen, öffnen, vergessen. 

13
00:00:40,480 --> 00:00:46,960
Einheit mit allen Wesen, mit allem, mit dem gesamten Universum. 

14
00:00:47,740 --> 00:00:51,760
Meister Kosen sagt, dass Zazen das Welterbe der Menschheit ist. 

15
00:00:52,040 --> 00:00:54,600
Wir werden niemanden zwingen, Zazen zu machen. 

16
00:00:54,860 --> 00:00:56,220
Und manchmal ist es nicht der richtige Zeitpunkt. 

17
00:00:56,460 --> 00:00:57,960
Wir können uns irgendwann hinsetzen. 

18
00:00:58,220 --> 00:01:02,940
Und vermisse, was es für unser Leben bedeuten könnte. 

19
00:01:03,220 --> 00:01:07,460
Und dann, in einem anderen Moment, ist es plötzlich da. 

20
00:01:07,680 --> 00:01:09,560
Es gibt eine Frage und eine Antwort. 

21
00:01:09,760 --> 00:01:11,120
Genau das sollten wir üben. 

22
00:01:11,360 --> 00:01:17,460
Zazen nur einmal zu machen, auch wenn es nicht dein Ding ist, wenn du empfänglich bist, wenn du Lust dazu hast, gibt es dir Informationen. 

23
00:01:18,700 --> 00:01:21,140
Es gibt viele verschiedene Praktiken. 

24
00:01:21,380 --> 00:01:22,880
Welches ist auf jeden Fall gut. 

25
00:01:23,160 --> 00:01:25,700
Sie können nicht Dinge zusammenfügen wollen. 

26
00:01:26,000 --> 00:01:29,940
Zazen ist eine ziemlich anspruchsvolle Praxis. 

27
00:01:30,240 --> 00:01:32,700
Sie nehmen nicht nur Stellung. 

28
00:01:33,040 --> 00:01:38,440
Haltung und die dazugehörige Stimmung sind unerlässlich. 

29
00:01:39,160 --> 00:01:43,900
Aber Zazen hat sein eigenes Leben, sein eigenes Funktionieren. 

30
00:01:44,160 --> 00:01:46,220
Es wird auch "Meditation" genannt. 

31
00:01:46,420 --> 00:01:47,980
Aber es ist nur ein Wort. 

32
00:01:48,320 --> 00:01:51,580
Wir nennen es "Zazen", "Buddhas Haltung". 

33
00:01:51,800 --> 00:01:53,700
Es sind nur Worte. 

34
00:01:53,940 --> 00:01:56,180
Aber das ist die Haltung, die wir praktizieren. 

35
00:01:56,840 --> 00:01:59,220
Wir nennen uns nicht Lehrer oder Meister. 

36
00:01:59,500 --> 00:02:03,040
Auch wenn Sie 1, 2, 3 Kurse belegt haben. 

37
00:02:03,300 --> 00:02:06,140
Es gibt eine intime Beziehung, du folgst einem Meister. 

38
00:02:06,380 --> 00:02:09,460
Wir folgen ihm nicht überall hin. 

39
00:02:09,780 --> 00:02:12,920
Wir folgen seiner Lehre, seinem Verstand. 

40
00:02:13,200 --> 00:02:14,760
Es gibt Zazen selbst. 

41
00:02:15,000 --> 00:02:22,300
Dann folge nicht deinem eigenen Ding, deiner eigenen Idee. 

42
00:02:22,580 --> 00:02:25,120
Aber um etwas Größerem zu folgen. 

43
00:02:25,560 --> 00:02:29,480
Etwas, das Generationen umfasst. 

44
00:02:29,860 --> 00:02:34,060
Seit dem historischen Buddha und vor ihm. 

45
00:02:34,380 --> 00:02:36,960
Es gibt andere Buddhas, die ihm vorausgegangen sind. 

46
00:02:37,240 --> 00:02:40,020
Andere, die in der Pose saßen. 

47
00:02:40,360 --> 00:02:42,720
Andere, die den Weg erreicht haben. 

48
00:02:43,500 --> 00:02:45,520
Und es ist ernst! 

49
00:02:45,840 --> 00:02:59,580
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

