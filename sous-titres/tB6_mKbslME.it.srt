1
00:00:00,000 --> 00:00:04,660
Hai detto che se i capi di stato eseguono prostate, 

2
00:00:05,080 --> 00:00:08,560
il mondo sarebbe più pacifico. 

3
00:00:08,880 --> 00:00:13,040
Mi chiedevo … 

4
00:00:13,340 --> 00:00:18,340
rispetto ad altre religioni. 

5
00:00:18,620 --> 00:00:23,080
Nella religione musulmana si parla di prostrazione. 

6
00:00:23,460 --> 00:00:34,540
Tuttavia, alcuni paesi del Medio Oriente si stanno facendo a pezzi. 

7
00:00:35,180 --> 00:00:42,020
Volevo sapere perché. 

8
00:00:42,320 --> 00:00:46,000
Perché la prostrazione è la stessa nello Zen e nell’Islam. 

9
00:00:46,340 --> 00:00:52,660
Penso che ci sia del buono e del cattivo in tutte le religioni. 

10
00:00:53,080 --> 00:00:58,240
Ma la prostrazione è una buona cosa, nella religione musulmana. 

11
00:00:58,400 --> 00:01:04,620
Anche i cristiani si uniscono nello stesso modo dello Zen, a Gasshô. 

12
00:01:05,100 --> 00:01:08,520
Va bene 

13
00:01:08,700 --> 00:01:14,040
C’è anche il fanatismo, è sempre stato lì. 

14
00:01:14,300 --> 00:01:18,820
Ci sono sempre state guerre di religione. 

15
00:01:21,320 --> 00:01:26,920
Nello Zen, la prostrazione non viene eseguita per uno scopo specifico. 

16
00:01:27,280 --> 00:01:33,440
Né per adorare la statua sull’altare, né per pregare il Buddha. 

17
00:01:33,960 --> 00:01:39,640
È la pratica della postura con tutto il tuo corpo. 

18
00:01:40,020 --> 00:01:46,140
Nel momento in cui colpisci il suolo, c’è consapevolezza. 

19
00:01:46,500 --> 00:01:50,860
Di solito il cervello non tocca mai il suolo. 

20
00:01:51,100 --> 00:01:55,820
È sempre nel cielo, sempre puntando verso le stelle. 

21
00:01:56,100 --> 00:02:01,480
Quindi sembra giusto. Per riconnetterti, piegati, tocca. 

22
00:02:01,740 --> 00:02:06,300
Quando i musulmani fanno questo, è perché ci sono cose molto buone nell’Islam. 

23
00:02:06,660 --> 00:02:13,000
Le famiglie crescono bene i loro figli. 

24
00:02:15,500 --> 00:02:21,620
I veri musulmani sono pieni di amore. 

25
00:02:24,200 --> 00:02:30,020
Non buttare il bambino fuori con l’acqua del bagno! 

26
00:02:30,550 --> 00:02:34,450
È bello che si prostrino! 

