1
00:00:08,039 --> 00:00:12,389
Ovunque tu sia.

2
00:00:21,107 --> 00:00:23,587
Siediti tranquillamente.

3
00:00:23,995 --> 00:00:26,755
Siate totalmente presenti.

4
00:00:27,698 --> 00:00:32,578
Chiunque tu sia.

5
00:00:39,450 --> 00:00:41,700
Esci dalla tua routine quotidiana.

6
00:00:41,790 --> 00:00:43,254
Pratica lo zazen.

7
00:00:43,401 --> 00:00:45,881
con il Kosen Sangha.

8
00:00:51,238 --> 00:00:57,194
Eventi legati a questa pandemia

9
00:00:58,324 --> 00:01:02,861
certificare lo Zen di Dogen

10
00:01:03,081 --> 00:01:05,538
e lo Zen di Deshimaru.

11
00:01:05,862 --> 00:01:09,802
L’impermanenza, la viviamo.

12
00:01:11,770 --> 00:01:16,620
L’interdipendenza è qualcosa che anche noi sperimentiamo.

13
00:01:21,799 --> 00:01:24,759
L’unico momento che conta,

14
00:01:26,209 --> 00:01:29,069
è proprio ora.

15
00:01:29,259 --> 00:01:30,919
Qui e ora.

16
00:01:31,008 --> 00:01:33,698
Non importa cosa fai.

17
00:01:36,710 --> 00:01:40,610
Rivolgere l’attenzione verso l’interno.

18
00:01:40,908 --> 00:01:43,698
Siediti e basta.

19
00:01:44,461 --> 00:01:47,161
Dritto e silenzioso.

20
00:01:48,301 --> 00:01:49,821
Il Buddha,

21
00:01:50,388 --> 00:01:53,538
il nostro maestro interiore.

22
00:01:53,942 --> 00:01:56,332
Dharma, il cammino, la pratica.

23
00:01:56,671 --> 00:01:59,091
La sangha, il sangha virtuale.

24
00:02:03,066 --> 00:02:07,486
Quando puoi.

25
00:02:12,081 --> 00:02:16,081
Unitevi a noi qui e ora.

26
00:02:16,630 --> 00:02:20,630
Nessun ego.

27
00:02:20,769 --> 00:02:24,269
Senza scopo.

28
00:02:52,434 --> 00:02:54,384
Buongiorno a tutti!

29
00:02:54,904 --> 00:02:57,614
E grazie per aver fatto zazen insieme!
