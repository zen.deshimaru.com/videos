1
00:00:00,180 --> 00:00:04,420
J’ai commencé à pratiquer le zen à l’âge de 16 ans. 

2
00:00:04,740 --> 00:00:07,660
J’ai 49 ans. 

3
00:00:07,860 --> 00:00:14,360
J’avais essayé de prendre la position de zazen seul sur mon lit. 

4
00:00:14,960 --> 00:00:19,460
Je l’ai gardé pendant 2, 3, 4 minutes, c’était vraiment très douloureux. 

5
00:00:19,700 --> 00:00:21,700
Je ne pouvais pas du tout le faire. 

6
00:00:21,840 --> 00:00:30,800
Là, ils m’ont montré la pose et j’ai tenu le zazen normal pendant environ une demi-heure. 

7
00:00:31,120 --> 00:00:33,420
Nous nous préparons à partir, et voici: 

8
00:00:33,640 --> 00:00:36,100
"Ah, n’oubliez pas de payer!" 

9
00:00:36,540 --> 00:00:38,500
Je pensais que c’était gratuit… 

10
00:00:38,880 --> 00:00:42,580
Je suis parti sans mon allocation … 

11
00:00:45,460 --> 00:00:48,680
Je pratique au Zen Dojo de Lyon, à Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
C’est un vieux dojo fait par Maître Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
Je m’appelle Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
Mon nom de moine est Ryurin. 

15
00:01:05,200 --> 00:01:09,700
Je suis disciple de Maître Kosen depuis 25 ans. 

16
00:01:10,000 --> 00:01:13,000
Il m’a fallu un certain temps pour rencontrer Maître Kosen. 

17
00:01:13,320 --> 00:01:17,480
Je le connaissais, je faisais des sesshins avec lui… 

18
00:01:17,760 --> 00:01:21,000
Mais je n’étais pas très proche. 

19
00:01:21,340 --> 00:01:26,980
Je me suis rapproché en raison de circonstances indépendantes de ma volonté. 

20
00:01:27,260 --> 00:01:31,800
Et j’ai pu découvrir son enseignement. 

21
00:01:32,160 --> 00:01:36,120
J’ai donc pratiqué le zazen avec lui jusqu’à aujourd’hui. 

22
00:01:36,280 --> 00:01:39,800
Maître Kosen m’a donné il y a une dizaine d’années, 

23
00:01:40,060 --> 00:01:42,920
comme il l’a fait avec beaucoup de ses anciens disciples, 

24
00:01:43,260 --> 00:01:46,900
le shiho, la transmission du Dharma. 

25
00:01:47,740 --> 00:01:50,740
Maître zen, c’est un samu, ce n’est pas un titre. 

26
00:01:51,020 --> 00:01:57,420
Comme nous disons "maître", cela suggère l’idée que l’on a maîtrisé quelque chose, mais c’est un samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" est un mot chinois, japonais. 

28
00:02:01,740 --> 00:02:03,840
Ce sont les tâches à accomplir. 

29
00:02:04,100 --> 00:02:06,380
Il enseigne aussi, c’est un samu. 

30
00:02:06,560 --> 00:02:08,720
Le maître zen fait son samu … 

31
00:02:09,000 --> 00:02:11,020
Quand il a fini avec ses samouraïs, il rentre chez lui. 

32
00:02:11,300 --> 00:02:12,980
Il ira travailler ou non … 

33
00:02:13,280 --> 00:02:15,560
Il a une femme et une famille ou pas … 

34
00:02:15,980 --> 00:02:17,240
Cela dépend de chaque personne … 

35
00:02:18,060 --> 00:02:21,180
Et maintenant, il travaille sur autre chose… 

36
00:02:21,500 --> 00:02:23,120
C’est ça la vie! 

37
00:02:23,640 --> 00:02:26,660
Mais le maître zen, c’est un disciple. 

38
00:02:27,060 --> 00:02:35,860
Si vous avez aimé cette vidéo, n’hésitez pas à l’aimer et à vous abonner à la chaîne! 

