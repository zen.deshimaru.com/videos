1
00:00:00,280 --> 00:00:02,560
Je ne connais pas grand-chose à la sophrologie.

2
00:00:02,800 --> 00:00:12,140
Je sais que c’est une mise en application de certaines pratiques de mieux-être, de développement personnel,

3
00:00:12,540 --> 00:00:17,160
et qu’il y a une sophrologie qu’on dit « orientaliste »,

4
00:00:17,440 --> 00:00:24,080
qui s’inspire beaucoup des pratiques yogiques, bouddhiques ou indiennes.

5
00:00:24,540 --> 00:00:27,940
Le zen, c’est très simple.

6
00:00:28,380 --> 00:00:38,400
C’est basé sur des postures fondamentales de l’être humain.

7
00:00:39,820 --> 00:00:48,480
Je vais vous montrer la première posture fondamentale du zen.

8
00:01:01,500 --> 00:01:06,120
Avant, les êtres humains étaient singes.

9
00:01:06,460 --> 00:01:08,580
Ils marchaient comme ça.

10
00:01:09,200 --> 00:01:20,360
Petit à petit, ils se sont assis, bien droits, comme des maîtres zen.

11
00:01:22,900 --> 00:01:24,900
Puis ils se sont redressés.

12
00:01:25,180 --> 00:01:28,600
Ils ont marché comme ça.

13
00:01:29,680 --> 00:01:33,760
Et la conscience humaine a évolué

14
00:01:35,180 --> 00:01:39,980
en même temps que la posture.

15
00:01:40,340 --> 00:01:46,000
La posture du corps est une pensée fondamentale.

16
00:01:46,340 --> 00:01:52,200
Par exemple, Usain Bolt, le coureur le plus rapide de l’humanité,

17
00:01:52,540 --> 00:01:55,160
fait ce geste-là.

18
00:01:55,440 --> 00:02:02,300
Tout le monde comprend ce que ça veut dire, et pourtant, il n’y a pas de mots.

19
00:02:02,640 --> 00:02:05,440
C’est juste une posture.

20
00:02:05,900 --> 00:02:11,120
La manière dont on s’assoit et dont on marche,

21
00:02:11,520 --> 00:02:13,400
ce qu’on fait avec ses mains,

22
00:02:13,700 --> 00:02:16,500
tout ça a une importance fondamentale.

23
00:02:18,160 --> 00:02:21,440
Dans le zen, il y a la posture de Bouddha.

24
00:02:22,980 --> 00:02:31,120
Elle existait avant le Bouddha, on en parle dans la Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
On s’y assoit sur un coussin d’herbe, et on replie les jambes.

26
00:02:41,560 --> 00:02:47,140
Dans la nature, les hommes préhistoriques communiquaient

27
00:02:47,440 --> 00:02:53,220
avec les oreilles, avec le nez, avec les yeux, pour voir loin.

28
00:02:54,920 --> 00:02:59,000
Ils ont donc voulu se redresser.

29
00:02:59,400 --> 00:03:04,220
Et en se redressant, leur conscience s’est affûtée, éveillée.

30
00:03:06,040 --> 00:03:08,720
Voici la première posture.

31
00:03:08,980 --> 00:03:11,580
Normalement, on doit faire un lotus complet.

32
00:03:12,060 --> 00:03:14,260
On croise les jambes ainsi.

33
00:03:14,700 --> 00:03:18,460
On appelle ça le « bretzel ».

34
00:03:24,960 --> 00:03:30,980
C’est très important de prendre cette posture.

35
00:03:31,380 --> 00:03:37,380
La conférencière précédente parlait de dynamisme et de relaxation.

36
00:03:37,780 --> 00:03:42,340
On a le système nerveux sympathique et parasympathique.

37
00:03:42,600 --> 00:03:45,800
On a les muscles extérieurs et les muscles profonds.

38
00:03:46,320 --> 00:03:51,400
On a la respiration superficielle et la respiration profonde.

39
00:03:51,820 --> 00:03:54,620
On a la conscience superficielle, corticale,

40
00:03:54,920 --> 00:03:59,760
et la conscience profonde, le cerveau reptilien, ou moyen.

41
00:04:00,340 --> 00:04:04,160
Pour se tenir bien droit dans cette posture,

42
00:04:04,440 --> 00:04:06,800
il faut pousser la terre avec les genoux.

43
00:04:10,240 --> 00:04:15,640
Ça demande un effort, pour être dynamique et avoir une belle posture.

44
00:04:16,000 --> 00:04:22,180
Mon maître [Deshimaru] insistait beaucoup sur la beauté et la force de la posture.

45
00:04:22,460 --> 00:04:31,280
Mais en lotus, plus vous vous relâchez, plus les pieds poussent sur les cuisses,

46
00:04:31,520 --> 00:04:35,100
et plus les genoux poussent sur la terre.

47
00:04:35,620 --> 00:04:40,280
Plus vous vous relâchez, plus vous êtes dynamique, plus vous êtes droit et fort.

48
00:04:40,560 --> 00:04:46,560
Ça n’a rien à voir avec de la relaxation dans un fauteuil ou un lit.

49
00:04:46,940 --> 00:04:57,280
C’est une manière d’abandonner le corps à lui-même,

50
00:05:04,380 --> 00:05:09,700
tout en étant parfaitement éveillé.

51
00:05:10,120 --> 00:05:16,240
C’est la première posture, la posture assise du Bouddha.

52
00:05:16,680 --> 00:05:20,280
Les mains sont également très importantes.

53
00:05:20,640 --> 00:05:23,600
On est toujours en train de faire quelque chose avec ses mains :

54
00:05:23,860 --> 00:05:26,560
travailler, se protéger…

55
00:05:26,960 --> 00:05:28,740
Les mains sont très expressives

56
00:05:29,040 --> 00:05:31,040
et sont reliées au cerveau.

57
00:05:31,500 --> 00:05:41,040
Prendre cette mudra avec les mains change la conscience.

58
00:05:41,680 --> 00:05:47,680
La concentration est dans nos mains, notre corps, notre système nerveux et le cerveau.

59
00:05:48,100 --> 00:05:53,360
Une chose importante également, c’est la pensée, la conscience.

60
00:05:53,820 --> 00:05:57,680
Si vous êtes comme le Penseur de Rodin,

61
00:05:58,000 --> 00:06:01,100
vous n’avez pas du tout la même conscience

62
00:06:09,960 --> 00:06:12,240
que lorsque votre posture est parfaitement droite,

63
00:06:12,580 --> 00:06:14,640
spécialement avec la nuque tendue,

64
00:06:15,700 --> 00:06:18,400
la tête bien droite, ouverte vers le ciel,

65
00:06:18,760 --> 00:06:22,020
le nez et le nombril alignés

66
00:06:25,060 --> 00:06:27,220
et que vous respirez de tout votre être.

67
00:06:29,020 --> 00:06:31,860
La pensée n’est alors plus dissociée du corps.

68
00:06:32,240 --> 00:06:36,100
Il n’y a plus dissociation entre notre conscience

69
00:06:36,300 --> 00:06:39,080
et la moindre cellule de notre corps.

70
00:06:39,500 --> 00:06:45,520
Dans le zen, on appelle cela « corps-esprit », en un seul mot.

71
00:06:46,360 --> 00:06:50,540
Voilà la posture fondamentale : zazen.

72
00:06:56,380 --> 00:07:00,200
La deuxième posture, c’est la posture en marche.

73
00:07:00,680 --> 00:07:06,340
En zazen, on abandonne toute chose et on ne bouge absolument pas.

74
00:07:06,560 --> 00:07:08,280
Donc, la conscience est spéciale.

75
00:07:08,720 --> 00:07:13,800
Mais il y a une autre méditation qui se fait en marchant.

76
00:07:16,180 --> 00:07:18,160
On est obligé de bouger.

77
00:07:18,540 --> 00:07:20,540
On fait quelque chose, on avance.

78
00:07:21,180 --> 00:07:24,180
On n’avance pas pour attraper quelque chose.

79
00:07:24,580 --> 00:07:28,380
On rythme la respiration avec la marche.

80
00:07:29,020 --> 00:07:30,740
Le chorégraphe Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
qui a été disciple de mon maître, Deshimaru,

82
00:07:34,060 --> 00:07:38,040
faisait pratiquer ça tous les jours à ses danseurs.

83
00:07:39,060 --> 00:07:45,340
La deuxième posture, c’est la marche de « kin-hin ».

84
00:07:45,880 --> 00:07:51,040
Remarquez ce que l’on fait avec ses mains.

85
00:07:51,680 --> 00:07:57,840
Un boxeur qui se fait prendre en photo fait cela avec ses mains.

86
00:07:58,240 --> 00:08:05,220
Quand on met les mains comme ça, automatiquement le cerveau exprime une forte agressivité.

87
00:08:05,820 --> 00:08:10,360
On peut mettre ses mains dans les poches.

88
00:08:10,840 --> 00:08:13,140
« Où est-ce que j’ai mis mon portefeuille ? »

89
00:08:13,640 --> 00:08:16,360
« On m’a volé mon portable ! »

90
00:08:16,700 --> 00:08:19,580
On peut se mettre les doigts dans le nez.

91
00:08:21,660 --> 00:08:25,820
Tout ce qui a été construit ici l’a été par des mains.

92
00:08:26,260 --> 00:08:28,820
C’est extraordinaire, la main.

93
00:08:29,160 --> 00:08:31,860
Nous sommes les seuls animaux à avoir des mains,

94
00:08:32,260 --> 00:08:35,340
elles sont reliées à notre évolution.

95
00:08:37,700 --> 00:08:40,660
Les mains aussi méditent.

96
00:08:40,940 --> 00:08:43,580
On met le pouce dans le poing.

97
00:08:43,820 --> 00:08:49,420
On place la racine du pouce sous le sternum, on met l’autre main dessus.

98
00:08:49,760 --> 00:08:55,060
Ces détails posturaux existent depuis le Bouddha.

99
00:08:55,380 --> 00:09:02,520
Des textes anciens expliquent qu’ils ont été transmis en Chine depuis l’Inde.

100
00:09:02,960 --> 00:09:08,920
C’est une transmission orale de personne à personne.

101
00:09:09,480 --> 00:09:12,500
Dans le bouddhisme, il y a beaucoup d’aspects.

102
00:09:12,780 --> 00:09:17,320
Mais l’aspect méditatif et postural est très important.

103
00:09:17,820 --> 00:09:28,540
Il y avait deux grands disciples du Bouddha, Śāriputra et Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Leur maître spirituel n’était pas sûr de lui :

105
00:09:37,220 --> 00:09:43,680
« Je ne suis pas sûr d’avoir le truc pour sortir du problème de la vie et de la mort. »

106
00:09:43,940 --> 00:09:52,860
« Si vous trouvez un maître qui enseigne cela, prévenez-moi, et j’irai le suivre avec vous. »

107
00:09:53,620 --> 00:10:04,320
Un jour, ils voient un moine, comme moi, voyez, qui marche sur la route.

108
00:10:04,780 --> 00:10:10,000
Ils sont stupéfiés par la beauté de cet homme.

109
00:10:10,420 --> 00:10:16,200
Puis, ce moine s’arrête dans un bosquet pour faire pipi.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, qui est considéré comme l’une des plus grandes intelligences de l’Inde de l’époque, lui dit :

111
00:10:34,180 --> 00:10:43,940
« Nous sommes stupéfiés par votre prestance, quel est votre maître ? »

112
00:10:46,560 --> 00:10:49,540
- « Mon maître est Shakyamuni Bouddha ».

113
00:10:49,860 --> 00:10:53,520
- « Explique-nous ta doctrine ! »

114
00:10:53,780 --> 00:10:58,220
- « Je ne saurais vous l’expliquer, je suis trop novice. »

115
00:10:59,100 --> 00:11:04,440
Et ils ont suivi ce garçon seulement à cause de sa manière de marcher.

116
00:11:04,800 --> 00:11:06,980
De sa façon de dégager quelque chose.

117
00:11:07,440 --> 00:11:11,680
La deuxième posture, c’est donc la marche.

118
00:11:12,020 --> 00:11:15,280
Lorsque les gens font zazen, c’est très fort.

119
00:11:15,500 --> 00:11:21,580
Même des pratiquants d’arts martiaux sont impressionnés par la puissance que cela dégage.

120
00:11:21,940 --> 00:11:25,860
Par la puissance de la respiration.

121
00:11:26,240 --> 00:11:35,520
Mais comment retrouver cette énergie, cette plénitude, dans la vie quotidienne ?

122
00:11:35,840 --> 00:11:39,220
Ce calme, cette concentration.

123
00:11:39,600 --> 00:11:42,960
C’est un point difficile.

124
00:11:43,280 --> 00:11:47,140
Car la posture de zazen est extrêmement précise.

125
00:11:47,600 --> 00:11:55,100
Au début, on se concentre sur l’aspect technique de la marche kin-hin.

126
00:11:55,540 --> 00:11:58,800
Mais une fois qu’on en a intégré tous les détails,

127
00:11:59,300 --> 00:12:02,160
à tout moment de notre vie quotidienne

128
00:12:02,440 --> 00:12:08,400
(même si moi, j’oublie toujours de le faire), on peut la pratiquer.

129
00:12:10,600 --> 00:12:16,740
Vous pouvez vous recentrer sur votre être spirituel.

130
00:12:17,740 --> 00:12:22,760
Sur votre être qui ne fait pas de conneries.

131
00:12:23,320 --> 00:12:31,340
Je vous ai parlé des mains.

132
00:12:31,840 --> 00:12:39,980
La posture fondamentale religieuse, c’est celle-là.

133
00:12:40,560 --> 00:12:44,240
Quand vous avez les mains jointes comme ça,

134
00:12:45,080 --> 00:12:48,260
rien que de faire ça,

135
00:12:48,580 --> 00:12:51,280
ça a un effet sur le cerveau,

136
00:12:51,620 --> 00:12:55,340
ça construit un pont entre les deux hémisphères cérébraux.

137
00:12:55,740 --> 00:12:59,960
Cette posture est employée dans toutes les religions.

138
00:13:00,280 --> 00:13:02,180
Elle est toute simple.

139
00:13:02,560 --> 00:13:05,640
Voyez l’importance des mains !

140
00:13:06,060 --> 00:13:13,280
La troisième posture…

141
00:13:13,860 --> 00:13:16,520
(Je croyais qu’il y en avait quatre…)

142
00:13:16,880 --> 00:13:20,580
Ah oui, il y a la posture allongée, mais ça, on ne va pas en parler.

143
00:13:21,000 --> 00:13:25,520
La posture allongée, comment dormir.

144
00:13:26,700 --> 00:13:31,980
Je vous ai expliqué comment les hommes préhistoriques gambadaient à quatre pattes,

145
00:13:32,260 --> 00:13:34,440
petit à petit, ils se sont redressés,

146
00:13:34,700 --> 00:13:38,760
et que notre évolution vient de notre posture,

147
00:13:39,060 --> 00:13:42,580
de l’irrigation de la colonne vertébrale qui monte vers le cerveau,

148
00:13:42,940 --> 00:13:46,400
et du cerveau, qui est un instrument extraordinaire,

149
00:13:46,700 --> 00:13:51,440
dont on ne connaît l’utilité que de 10 à 15 % de sa totalité.

150
00:13:51,760 --> 00:13:57,740
On a un gros truc entre les oreilles, qu’on sous-emploie.

151
00:14:08,640 --> 00:14:12,380
Chacun a son cerveau !

152
00:14:19,020 --> 00:14:22,140
Chacun a son cerveau !

153
00:14:22,400 --> 00:14:25,200
Comme une adresse IP.

154
00:14:30,220 --> 00:14:33,220
Chacun a son cerveau !

155
00:14:33,940 --> 00:14:35,900
Et on est seuls !

156
00:14:36,700 --> 00:14:41,940
C’est très important pour ce que je vais vous dire.

157
00:14:44,760 --> 00:14:50,040
Nos pieds (on va revenir aux pieds…).

158
00:14:51,720 --> 00:14:56,420
Vous savez qu’on habite sur une petite Terre.

159
00:14:57,040 --> 00:15:01,420
Vous avez vu le film Gravity ?

160
00:15:02,120 --> 00:15:07,060
Il fait partie des films qui éveillent véritablement la conscience humaine.

161
00:15:07,560 --> 00:15:13,380
À Hollywood, ils font un certain travail de manipulation, parfois.

162
00:15:13,680 --> 00:15:17,220
Mais aussi parfois d’information et d’évolution spirituelle.

163
00:15:17,760 --> 00:15:19,660
Ce film en fait partie.

164
00:15:20,040 --> 00:15:26,920
Et on comprend en le voyant, la fragilité de notre Terre.

165
00:15:29,200 --> 00:15:33,400
D’habitude, quand on regarde le ciel, on s’extasie : « Il est bleu à l’infini ! ».

166
00:15:33,760 --> 00:15:38,940
Non. Pas à l’infini. 30 kilomètres d’atmosphère.

167
00:15:39,360 --> 00:15:44,660
La fin de l’atmosphère se situe entre 30 et 1 000 kilomètres.

168
00:15:52,260 --> 00:15:56,700
Même mille kilomètres, ce n’est rien du tout.

169
00:15:58,800 --> 00:16:04,780
Si je vous mets à 30 kilomètres, vous ne survivez même pas une minute.

170
00:16:05,380 --> 00:16:15,940
Même mis à part les impôts, nos conditions de vie sont précaires.

171
00:16:17,420 --> 00:16:20,440
La Terre est un petit être fragile, vivant.

172
00:16:21,120 --> 00:16:30,660
Notre biosphère est minuscule et fragile, il faut la préserver.

173
00:16:30,940 --> 00:16:35,100
On devrait toujours avoir le souci de préserver ce truc.

174
00:16:36,860 --> 00:16:39,940
Car nous sommes nés de la Terre.

175
00:16:40,220 --> 00:16:45,700
Ce miracle de la vie qui a eu lieu sur Terre est éphémère.

176
00:16:46,240 --> 00:16:47,840
Nous sommes nés de la Terre.

177
00:16:48,220 --> 00:16:53,320
Et nous avons tous les pieds qui pointent vers le même centre, celui de la Terre.

178
00:16:53,900 --> 00:16:57,760
Donc, nos pieds sont en communauté.

179
00:17:01,860 --> 00:17:08,680
Mais notre tête est unique, personne ne pointe dans la même direction.

180
00:17:18,400 --> 00:17:23,500
D’où la troisième posture que je vais vous montrer.

181
00:17:23,800 --> 00:17:28,140
Elle est commune à beaucoup de religions, on l’appelle la « prosternation ».

182
00:17:28,380 --> 00:17:31,240
Nous, on la fait comme ça.

183
00:17:40,640 --> 00:17:42,780
Ah, ça fait du bien !

184
00:17:43,060 --> 00:17:49,940
Quand on fait cette pratique, c’est comme quand on fait l’amour :

185
00:17:50,260 --> 00:17:58,460
parfois, on ne fait pas l’amour pendant 10 ans, 20 ans, 6 mois…

186
00:17:58,780 --> 00:18:07,460
et quand on fait à nouveau l’amour, on dit : « Ah, ça fait du bien, j’avais oublié comme c’était bon ! ».

187
00:18:08,220 --> 00:18:12,200
Quand on fait cette prosternation, c’est la même chose.

188
00:18:13,460 --> 00:18:15,740
Je ne plaisante pas.

189
00:18:16,240 --> 00:18:20,260
On reformate notre cerveau.

190
00:18:21,060 --> 00:18:26,280
Quand on met son cerveau dans l’alignement du centre de la Terre.

191
00:18:26,620 --> 00:18:29,080
Ce n’est pas seulement une question d’humilité,

192
00:18:29,360 --> 00:18:32,720
ou de se prosterner devant des statues ou devant je ne sais quoi.

193
00:18:34,600 --> 00:18:38,880
C’est un truc magique, fondamental, un truc de chamane, peut-être…

194
00:18:39,320 --> 00:18:41,860
Dès le moment où vous faites cette prosternation…

195
00:18:42,180 --> 00:18:43,340
Mon maître disait :

196
00:18:43,660 --> 00:18:47,440
« Si les chefs d’État faisaient cette prosternation une fois par jour,  »

197
00:18:47,800 --> 00:18:50,200
« ça changerait la face du monde. »

198
00:18:50,640 --> 00:18:54,560
Ce n’est ni du bla-bla ni de la mystique.

199
00:18:55,280 --> 00:18:58,780
On doit pouvoir le mesurer scientifiquement.

200
00:19:00,780 --> 00:19:05,280
Ça reformate le cerveau, de toucher en bas.

201
00:19:05,740 --> 00:19:09,280
Il est constamment dans sa ligne individuelle.

202
00:19:09,620 --> 00:19:17,300
Et là, on se relie à sa racine, à ses frères, au centre de la Terre.

203
00:19:19,940 --> 00:19:25,380
Il m’arrive de le faire et de ressentir quelque chose de fort dans la tête.

204
00:19:25,660 --> 00:19:27,540
Ça fait énormément de bien.

205
00:19:27,820 --> 00:19:39,420
En même temps, ça relativise le mal qu’on fait autour de nous sans le vouloir.

206
00:19:40,560 --> 00:19:42,960
C’est une bonne médecine.

207
00:19:43,180 --> 00:19:47,040
Toutes ces postures sont des médecines fondamentales.

208
00:19:47,760 --> 00:19:53,260
J’avais préparé un texte un peu intellectuel.

209
00:19:53,520 --> 00:19:58,140
Mais je n’arrive pas à passer d’un sujet à l’autre.

210
00:19:58,440 --> 00:20:02,300
Qu’est-ce que j’y disais ?

211
00:20:06,860 --> 00:20:12,040
Je reviens au corps et à la préhistoire.

212
00:20:12,780 --> 00:20:28,200
On a prouvé que les hommes de Néandertal avaient une pratique religieuse.

213
00:20:30,680 --> 00:20:36,340
On a retrouvé des tombeaux.

214
00:20:36,860 --> 00:20:40,440
Ils apportaient des offrandes à leurs morts.

215
00:20:40,760 --> 00:20:43,680
Ils faisaient une cérémonie religieuse pour leurs morts.

216
00:20:48,420 --> 00:20:52,180
Ce qui est intéressant, ce n’est pas seulement ça !

217
00:20:52,720 --> 00:20:56,260
À l’époque où ils faisaient ces cérémonies religieuses -

218
00:20:56,740 --> 00:21:01,700
- religieuses dans le sens où ils communiquaient avec l’invisible -

219
00:21:02,540 --> 00:21:06,940
- avec la vie éternelle, avec les morts -

220
00:21:08,800 --> 00:21:11,020
- ils se reliaient à l’invisible.

221
00:21:11,420 --> 00:21:18,140
Or, à cette époque, le langage articulé n’existait pas encore.

222
00:21:18,560 --> 00:21:24,380
Donc, la religion existait avant le langage articulé.

223
00:21:24,840 --> 00:21:30,140
(Je veux bien passer au langage articulé…)

224
00:21:31,480 --> 00:21:36,500
(C’est très intéressant, j’ai mis quatre jours à l’écrire…)

225
00:21:43,820 --> 00:21:45,660
Mon maître disait :

226
00:21:45,920 --> 00:21:48,880
« Le zazen, c’est la religion d’avant la religion. »

227
00:21:49,380 --> 00:21:54,380
On lui disait « Ouais, tu veux toujours être le premier… »

228
00:21:55,860 --> 00:21:59,700
Mais c’est la religion d’avant la religion telle qu’on la connaît.

229
00:21:59,960 --> 00:22:02,720
Où l’on se réfère à des livres.

230
00:22:03,080 --> 00:22:06,280
Les livres viennent après la religion, pas avant.

231
00:22:06,720 --> 00:22:11,940
Avant, l’homme ne savait même pas parler.

232
00:22:12,360 --> 00:22:15,060
Alors, il prenait déjà cette posture.

233
00:22:15,480 --> 00:22:19,620
Ce sont des postures préhistoriques.

234
00:22:21,460 --> 00:22:23,740
(Combien il me reste de temps ?)

235
00:22:23,960 --> 00:22:28,320
(Il ne me reste plus que 3 minutes ? C’est bon, alors !)

236
00:22:31,020 --> 00:22:34,460
Alors, par rapport à la sophrologie…

237
00:22:36,500 --> 00:22:39,280
Je voulais vous parler du zazen traditionnel.

238
00:22:39,740 --> 00:22:43,660
Je suis accoutré en moine traditionnel.

239
00:22:47,440 --> 00:22:51,700
Le zazen traditionnel ne se pratique pas seul.

240
00:22:52,100 --> 00:22:54,500
Ce n’est pas une technique de bien-être.

241
00:22:58,260 --> 00:23:05,060
Je vais vous lire ce que le Bouddha a dit, je trouve ça trop génial !

242
00:23:08,520 --> 00:23:12,780
Shakyamuni Bouddha dit à son auditoire :

243
00:23:13,120 --> 00:23:18,540
« L’étude des soutras et des enseignements » - des textes -

244
00:23:18,900 --> 00:23:28,260
« l’observance des préceptes » - dans les religions, c’est important, ne pas tuer, ne pas voler, etc. -

245
00:23:29,300 --> 00:23:34,960
« et même la pratique de zazen, la méditation assise »

246
00:23:35,900 --> 00:23:43,060
« sont incapables d’éteindre les désirs, les craintes et les angoisses humaines. »

247
00:23:43,340 --> 00:23:45,080
Voilà ce qu’a dit Shakyamuni Bouddha.

248
00:23:45,340 --> 00:23:51,660
Alors, moi qui suis un fanatique, il me casse la baraque…

249
00:23:52,080 --> 00:23:56,800
Zazen, même le Bouddha dit que ça ne sert à rien !

250
00:23:57,600 --> 00:24:01,680
Alors, qu’est-ce qu’on va faire?

251
00:24:14,440 --> 00:24:17,480
Bien sûr, il y a une suite…

252
00:24:23,360 --> 00:24:29,360
Mon maître [Deshimaru], quand il a rencontré son maître [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
ce dernier disait en conférence « Zazen ne sert à rien, ça n’apporte aucun mérite ! ».

254
00:24:34,680 --> 00:24:38,800
Mon maître s’est dit : « Mais c’est vachement intéressant, si ça ne sert à rien ! »

255
00:24:39,140 --> 00:24:41,940
« On fait toujours des trucs qui servent à quelque chose ! »

256
00:24:42,280 --> 00:24:43,760
« Un truc qui ne sert à rien ? »

257
00:24:44,120 --> 00:24:47,400
« Je veux y aller, je veux le faire ! »

