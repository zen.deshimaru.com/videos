1
00:00:13,650 --> 00:00:17,340
nací en Montpellier, así que en Francia 

2
00:00:17,340 --> 00:00:22,510
pero ahora vivo en un sonido mis padres son dos países diferentes 

3
00:00:22,510 --> 00:00:26,950
De repente, a menudo viajo entre el único en Francia y también en otros países. 

4
00:00:26,950 --> 00:00:32,169
porque ven allí, veo allí con su trabajo, pero todos los veranos venimos 

5
00:00:32,169 --> 00:00:36,370
aquí el templo en la montaña desde el mecanizado cuando tenía dos 

6
00:00:36,370 --> 00:00:44,960
semanas creo que tenemos tiempo completo creciendo aquí nunca siempre 

7
00:00:44,960 --> 00:00:50,390
ven aquí lo que mis amigos y yo sabemos todo el tiempo, sí, vamos 

8
00:00:50,390 --> 00:00:55,130
sigue viniendo aquí incluso cuando no eres un adulto pero te dijiste a ti mismo 

9
00:00:55,130 --> 00:00:58,430
nunca que íbamos a tener que hablar no había sido no era realmente 

10
00:00:58,430 --> 00:01:03,080
algo que pensamos de las personas que vienen aquí vienen a 

11
00:01:03,080 --> 00:01:08,960
Zen cuando no existía para ver las piernas para que la comida jugara 

12
00:01:08,960 --> 00:01:15,560
y de repente descubrí allí la sangha antes que Zen y mi opinión que tienes es sobre 

13
00:01:15,560 --> 00:01:19,850
También tenía otro amigo vivien que tenía un viento más que él y nosotros 

14
00:01:19,850 --> 00:01:25,729
para nosotros es un poco como ponerlo en la calle rue Godot nos hizo 

15
00:01:25,729 --> 00:01:32,090
creer en la magia nos hizo creer todo tipo de cosas por 

16
00:01:32,090 --> 00:01:37,700
ejemplo, una mujer cuando estaba en el bosque nos dijo a medio terminar 

17
00:01:37,700 --> 00:01:42,530
cavar en el suelo y porque siempre hay diamantes bajo tierra 

18
00:01:42,530 --> 00:01:48,140
no importa el pie hay al menos un diamante por metro del consulado 

19
00:01:48,140 --> 00:01:54,680
cavado y cavado y cavado y que en cualquier caso los dos teníamos ambos lados 

20
00:01:54,680 --> 00:01:59,270
escondido de él, sacó un diamante de su bolsillo y fingió salir del 

21
00:01:59,270 --> 00:02:04,640
tierra de un enorme diamante prestado de su madre y luego fueron deslumbrados 

22
00:02:04,640 --> 00:02:12,230
tenemos que tener algo así como 30 y durante el tipo mucho dado después 

23
00:02:12,230 --> 00:02:15,860
a veces cavaría un poco descarado si encuentro un 

24
00:02:15,860 --> 00:02:21,110
diamante si alguna vez me di cuenta de que podría tener diez años 

25
00:02:21,110 --> 00:02:24,250
algo si en mi piel te cuento todo 

26
00:02:24,250 --> 00:02:28,130
James es diferente para todos, creo que mi madre cuando la fatiga de 

27
00:02:28,130 --> 00:02:31,840
sus admiradores ella inmediatamente sintió que ella era azul pharsal por el resto de su vida 

28
00:02:31,840 --> 00:02:38,440
pero creo que para mí después de cada Zen empiezo a amar más 

29
00:02:38,690 --> 00:02:43,950
en más o menos bien el segundo tiro me digo que tendría 

30
00:02:43,950 --> 00:02:48,709
éxtasis además no ha terminado 

31
00:02:49,280 --> 00:02:54,740
Siempre supe los anuncios que siempre he tenido, de hecho, cuando mi 

32
00:02:54,740 --> 00:02:59,840
madre estaba embarazada, ella ya practicaba zazen de repente es un poco como 

33
00:02:59,840 --> 00:03:06,890
Nací de repente, siempre conocí la sangha en diferentes templos. 

34
00:03:06,890 --> 00:03:13,819
que tengo muchos recuerdos que conocí amigos o incluso chozas 

35
00:03:13,819 --> 00:03:17,750
Lo que siempre ha sido de diferentes demandas y cuando era pequeño yo 

36
00:03:17,750 --> 00:03:21,470
No me estaba escondiendo, solo les decía a mis amigos que sí, que voy a las montañas y 

37
00:03:21,470 --> 00:03:25,850
todo pero nunca les dije que voy a hacer voy al zen les dije 

38
00:03:25,850 --> 00:03:28,400
no eso porque intenté una vez de todos modos 

39
00:03:28,400 --> 00:03:31,190
todos me miraron muy extrañamente me dije oye 

40
00:03:31,190 --> 00:03:36,110
nunca vuelvas a hablar y, de lo contrario, nunca me pareció extraño, me pareció 

41
00:03:36,110 --> 00:03:41,590
aún más normal para ir, de hecho, sabía de antemano si venía 

42
00:03:41,590 --> 00:03:46,010
Tenía que hacer un Zen al día, pero no estaba en contra de esta idea. 

43
00:03:46,010 --> 00:03:50,750
francamente, para mi juego quería descubrir precisamente que era mi elección de 

44
00:03:50,750 --> 00:03:54,709
ven aquí primero para ver toda la saga que no he visto en dos 

45
00:03:54,709 --> 00:04:01,820
tres años entonces el lugar donde se encuentra la comida y todos los recuerdos que vuelven 

46
00:04:01,820 --> 00:04:06,380
hecho tan pronto como vengo la primera vez que hago Hazen no me di cuenta 

47
00:04:06,380 --> 00:04:09,850
cuenta que lo estaba haciendo en realidad da que era bastante extraño en el sentido de que 

48
00:04:09,850 --> 00:04:15,320
No vi demasiados cambios, de hecho no lo hice porque no vi 

49
00:04:15,320 --> 00:04:19,310
la utilidad del zen pero me parece tan normal que no he visto ninguna 

50
00:04:19,310 --> 00:04:23,660
cambio en realidad me parece extraño pero me gusta me siento como 

51
00:04:23,660 --> 00:04:28,490
cuanto más suertudo, Jean Prat, más me gusta y más esponjoso que ellos 

52
00:04:28,490 --> 00:04:31,090
verse diferente 

53
00:04:33,470 --> 00:04:41,410
la dama validada en francia youtube en 

54
00:04:45,910 --> 00:04:57,220
Francia tanto en simple como eficiente 

55
00:05:05,560 --> 00:05:13,809
[Musica] 

56
00:05:22,860 --> 00:05:25,929
[Musica] 

57
00:07:03,510 --> 00:07:06,840
1 [Música] 

58
00:07:06,840 --> 00:07:08,840
e 

