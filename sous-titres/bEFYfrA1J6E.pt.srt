1
00:00:08,660 --> 00:00:11,260
Bom Dia todo mundo 

2
00:00:11,560 --> 00:00:19,400
Para saber como se sentar em meditação zen, zazen .. 

3
00:00:19,800 --> 00:00:22,600
Vou lhe dar uma explicação imediatamente. 

4
00:00:22,700 --> 00:00:27,880
Antes de tudo isso, preste atenção que a única coisa que você precisa .. 

5
00:00:28,340 --> 00:00:33,020
é um ser humano e uma almofada de meditação. 

6
00:00:33,020 --> 00:00:36,680
Se você tem uma almofada de meditação, muito bom. 

7
00:00:37,100 --> 00:00:40,560
E se não, você pode fazer um com alguns panos. 

8
00:00:43,060 --> 00:00:44,320
Aqui vamos nós. 

9
00:00:45,885 --> 00:00:48,855
Então, a meditação zen .. 

10
00:00:49,620 --> 00:00:53,420
Antes de qualquer coisa, você precisa entender claramente que .. 

11
00:00:53,720 --> 00:00:57,580
A meditação zen é uma meditação sentada. 

12
00:00:57,940 --> 00:01:00,820
Imóvel e silencioso. 

13
00:01:01,295 --> 00:01:04,425
E a particularidade desta meditação é .. 

14
00:01:04,425 --> 00:01:07,445
Que nos sentamos em frente ao muro. 

15
00:01:08,400 --> 00:01:11,040
Como agora, eu sento na frente da câmera, mas .. 

16
00:01:11,380 --> 00:01:15,900
Mas durante a meditação você se senta em frente ao muro. 

17
00:01:16,520 --> 00:01:21,940
Então, vamos olhar para três aspectos diferentes. 

18
00:01:24,860 --> 00:01:27,060
Mas você notará que .. 

19
00:01:27,300 --> 00:01:32,680
A meditação está praticando esses três aspectos ao mesmo tempo. 

20
00:01:33,420 --> 00:01:36,955
O primeiro tema é a postura física. 

21
00:01:36,955 --> 00:01:38,435
Com o corpo. 

22
00:01:39,175 --> 00:01:40,625
O segundo tema é .. 

23
00:01:41,020 --> 00:01:42,900
A respiração. 

24
00:01:42,960 --> 00:01:45,580
Como respiramos durante a meditação zen. 

25
00:01:46,300 --> 00:01:48,720
E o terceiro aspecto .. 

26
00:01:48,880 --> 00:01:50,320
A atitude mental. 

27
00:01:50,620 --> 00:01:56,180
Significado, o que fazemos enquanto estamos sentados imóveis .. 

28
00:01:56,180 --> 00:01:59,080
E em silêncio em frente ao muro. 

29
00:02:00,040 --> 00:02:04,560
A postura corporal 

30
00:02:06,160 --> 00:02:09,600
A postura corporal, a postura física .. 

31
00:02:09,635 --> 00:02:12,585
A postura concreta da meditação zen. 

32
00:02:12,585 --> 00:02:17,340
Então você está sentado em uma almofada. 

33
00:02:17,760 --> 00:02:21,440
Não na borda do travesseiro, mas bem em cima. 

34
00:02:21,440 --> 00:02:24,640
E você deve prestar atenção para cruzar as pernas .. 

35
00:02:24,640 --> 00:02:28,160
De tal maneira que seus joelhos podem .. 

36
00:02:28,220 --> 00:02:32,540
Cair ou empurrar o chão. 

37
00:02:32,540 --> 00:02:35,140
Ou que eles caiam. 

38
00:02:35,440 --> 00:02:37,220
Como você faz isso? 

39
00:02:37,840 --> 00:02:41,120
Você pega sua perna, do jeito que você pode. 

40
00:02:41,380 --> 00:02:44,020
Você pode colocar desta forma ou .. 

41
00:02:44,685 --> 00:02:47,435
Menos rígido, você pode colocá-lo aqui. 

42
00:02:47,435 --> 00:02:50,585
A posição que chamamos de lótus médio. 

43
00:02:50,585 --> 00:02:54,980
E, obviamente, você também pode alterar a parte superior da perna. 

44
00:02:55,240 --> 00:02:59,040
De qualquer forma, coloque-o para que você se sinta mais confortável. 

45
00:02:59,260 --> 00:03:02,240
Lembre-se, não vamos nos mover. 

46
00:03:02,500 --> 00:03:07,120
Portanto, devemos encontrar uma posição que não seja muito difícil. 

47
00:03:07,120 --> 00:03:10,600
O que nos impedirá de .. 

48
00:03:10,600 --> 00:03:13,940
E fique quieto. Assim.. 

49
00:03:14,160 --> 00:03:17,460
Os joelhos vão para o chão. 

50
00:03:17,655 --> 00:03:19,655
Como vamos fazer isso? 

51
00:03:19,660 --> 00:03:23,800
A pélvica se inclina um pouco para a frente. 

52
00:03:24,240 --> 00:03:27,415
Então o que está acontecendo? 

53
00:03:27,420 --> 00:03:31,000
Se a pelve é assim para a frente .. 

54
00:03:32,020 --> 00:03:36,180
Por causa da gravidade nas minhas costas .. 

55
00:03:36,180 --> 00:03:39,020
Ou como quando estou dirigindo um carro .. 

56
00:03:39,020 --> 00:03:41,025
E como você pode ver .. 

57
00:03:41,025 --> 00:03:43,860
Meus joelhos estão no ar. 

58
00:03:43,860 --> 00:03:47,060
E eu quero que meus joelhos caiam. 

59
00:03:47,060 --> 00:03:51,700
No zen, dizemos: "pressione a terra com os joelhos". 

60
00:03:51,900 --> 00:03:54,940
E esse movimento, com a gravidade .. 

61
00:03:55,040 --> 00:03:57,300
Indo para baixo .. 

62
00:03:57,300 --> 00:04:03,120
Com esta leve inclinação da pelve, vemos .. 

63
00:04:03,460 --> 00:04:06,995
Não há intervenção com os músculos. 

64
00:04:06,995 --> 00:04:10,355
Procurando formar no perfil .. 

65
00:04:12,355 --> 00:04:15,315
Caso você esteja assim .. 

66
00:04:15,780 --> 00:04:19,160
Onde a pelve está virada para a frente .. 

67
00:04:20,220 --> 00:04:21,700
Torça um pouco .. 

68
00:04:22,720 --> 00:04:23,780
Apenas aqui. 

69
00:04:24,960 --> 00:04:27,040
Perfeitamente reto. 

70
00:04:27,040 --> 00:04:30,080
Se você se sente assim naturalmente .. 

71
00:04:30,080 --> 00:04:31,740
O centro de gravidade é .. 

72
00:04:31,740 --> 00:04:34,875
Vindo mais para a frente .. 

73
00:04:34,875 --> 00:04:37,555
E seus joelhos cairão no chão. 

74
00:04:39,555 --> 00:04:42,595
A partir desta pelve inclinada .. 

75
00:04:42,945 --> 00:04:45,725
Você endireita sua coluna .. 

76
00:04:46,165 --> 00:04:48,885
Assim como você pode. 

77
00:04:49,600 --> 00:04:52,100
A este respeito, falamos sobre .. 

78
00:04:52,100 --> 00:04:55,460
Empurrando o céu .. 

79
00:04:55,460 --> 00:04:58,700
Com o topo da nossa cabeça. 

80
00:05:04,620 --> 00:05:06,680
No zen da coluna vertebral .. 

81
00:05:07,560 --> 00:05:10,720
Inicia ou pára (como você deseja) aqui. 

82
00:05:10,720 --> 00:05:14,020
Se endireitarmos a espinha .. 

83
00:05:14,040 --> 00:05:17,100
Isso significa, tente esticá-lo até lá. 

84
00:05:17,380 --> 00:05:18,740
Então não vamos .. 

85
00:05:19,260 --> 00:05:21,400
Medite com a cabeça .. 

86
00:05:22,335 --> 00:05:25,525
Caindo, nem caindo para trás. 

87
00:05:26,015 --> 00:05:28,255
O queixo deve ser .. 

88
00:05:29,080 --> 00:05:31,240
O mais direto possível. 

89
00:05:31,960 --> 00:05:35,920
Falamos em manter uma linha horizontal reta. 

90
00:05:36,280 --> 00:05:38,320
Os olhos.. 

91
00:05:39,285 --> 00:05:40,345
A boca.. 

92
00:05:41,015 --> 00:05:42,905
Uma linha vertical .. 

93
00:05:43,305 --> 00:05:45,125
Entre os olhos, a boca e .. 

94
00:05:45,605 --> 00:05:46,695
O umbigo. 

95
00:05:48,075 --> 00:05:49,075
Boa. 

96
00:05:49,880 --> 00:05:53,840
Uma vez que eu me sento o mais reto possível .. 

97
00:05:54,060 --> 00:05:56,365
A postura das mãos .. 

98
00:05:56,365 --> 00:05:58,495
A mão esquerda 

99
00:05:59,765 --> 00:06:01,175
Dedos juntos .. 

100
00:06:01,575 --> 00:06:04,725
Você coloca na sua mão direita. 

101
00:06:04,920 --> 00:06:06,380
Dedos juntos. 

102
00:06:06,600 --> 00:06:09,740
Porque os dedos estavam em cima um do outro. 

103
00:06:11,765 --> 00:06:13,975
Uma linha horizontal com .. 

104
00:06:13,975 --> 00:06:17,115
Os polegares se tocando. 

105
00:06:17,360 --> 00:06:21,280
Eles tocam, por exemplo, para segurar uma pena. 

106
00:06:21,280 --> 00:06:22,480
Portanto, não há necessidade de .. 

107
00:06:22,885 --> 00:06:24,925
Pressão muscular. 

108
00:06:25,915 --> 00:06:29,125
E o interior de nossas mãos. 

109
00:06:29,905 --> 00:06:33,295
Colocamos na base, aqui .. 

110
00:06:33,675 --> 00:06:36,545
Mais ou menos no meio da barriga. 

111
00:06:36,760 --> 00:06:40,900
Então, para manter essa postura .. 

112
00:06:41,620 --> 00:06:47,740
Você tem que pegar qualquer pedaço de pano 

113
00:06:48,140 --> 00:06:51,800
O que irá ajudá-lo a .. 

114
00:06:53,680 --> 00:06:56,675
Solte suas mãos 

115
00:06:56,680 --> 00:07:00,920
De forma livre contra a sua barriga .. 

116
00:07:01,180 --> 00:07:06,080
Sem se sentir obrigado a usar os dedos e os ombros .. 

117
00:07:06,400 --> 00:07:09,600
Pelo contrário, você pode deixar de lado todas as tensões. 

118
00:07:10,180 --> 00:07:12,780
Os cotovelos são livres .. 

119
00:07:12,995 --> 00:07:14,985
Vou me colocar de perfil .. 

120
00:07:15,355 --> 00:07:17,065
Então você pode ver bem. 

121
00:07:17,485 --> 00:07:19,105
Nós recapturamos 

122
00:07:20,125 --> 00:07:22,925
Direto.. 

123
00:07:23,820 --> 00:07:27,460
As mãos, deixadas no canto superior direito. 

124
00:07:27,740 --> 00:07:31,000
Os polegares. E o interior das mãos .. 

125
00:07:31,000 --> 00:07:34,480
Na base do abdômen. 

126
00:07:35,140 --> 00:07:36,460
Cotovelos retos. 

127
00:07:37,985 --> 00:07:38,985
Boa. 

128
00:07:40,120 --> 00:07:42,540
Essa é a postura .. 

129
00:07:43,220 --> 00:07:47,000
Aproximadamente, digamos .. 

130
00:07:47,640 --> 00:07:49,840
Do material, o corpo. 

131
00:07:49,840 --> 00:07:53,240
Depois de colocar meu corpo .. 

132
00:07:54,345 --> 00:07:57,225
Na postura de meditação .. 

133
00:07:57,225 --> 00:08:00,455
Você se lembra, em silêncio em frente a uma parede. 

134
00:08:01,280 --> 00:08:02,680
O que acontece? 

135
00:08:02,960 --> 00:08:04,520
O que vai acontecer é que .. 

136
00:08:04,760 --> 00:08:08,160
Em frente ao muro, nada de interessante acontece. 

137
00:08:08,640 --> 00:08:12,380
Falamos sobre voltar sua atenção para dentro. 

138
00:08:14,360 --> 00:08:17,200
Então, sobre o seu olhar durante o zazen: 

139
00:08:17,975 --> 00:08:20,795
Os olhos estão semi-abertos .. 

140
00:08:21,085 --> 00:08:24,345
45 graus em uma diagonal .. 

141
00:08:24,345 --> 00:08:25,820
Para baixo. 

142
00:08:26,000 --> 00:08:28,985
É como ter sua visão .. 

143
00:08:28,985 --> 00:08:32,155
E colocando na sua frente. 

144
00:08:32,245 --> 00:08:34,785
Não há nada interessante para ver. 

145
00:08:35,160 --> 00:08:38,920
Então faremos o mesmo com os outros sentidos. 

146
00:08:39,160 --> 00:08:43,180
Se falamos de uma meditação silenciosa .. 

147
00:08:44,440 --> 00:08:48,780
Você não precisa de nada. Sem música, nada. 

148
00:08:49,080 --> 00:08:51,580
Um silêncio interior. 

149
00:08:52,220 --> 00:08:55,680
É uma meditação silenciosa, então .. 

150
00:08:56,475 --> 00:08:58,315
Também não há necessidade de falar. 

151
00:08:59,235 --> 00:09:00,235
Conclusão.. 

152
00:09:00,900 --> 00:09:02,880
Quando estou imóvel .. 

153
00:09:02,880 --> 00:09:06,760
Minha visão é inútil .. 

154
00:09:06,760 --> 00:09:09,000
Nem é minha audição 

155
00:09:09,280 --> 00:09:10,820
ou minhas palavras. 

156
00:09:10,980 --> 00:09:14,280
Então a conclusão é que .. 

157
00:09:14,500 --> 00:09:17,920
Nossos sentidos com os quais sentimos o mundo ao nosso redor .. 

158
00:09:17,920 --> 00:09:21,300
Nós os colocamos no ’modo avião’. 

159
00:09:21,300 --> 00:09:23,980
Poderíamos dizer assim. Em espera. 

160
00:09:24,300 --> 00:09:27,400
E este é o princípio da postura .. 

161
00:09:27,400 --> 00:09:30,140
Através do estabelecimento .. 

162
00:09:30,740 --> 00:09:33,675
Essa postura física e .. 

163
00:09:33,675 --> 00:09:36,775
Para cortar o exterior .. 

164
00:09:36,775 --> 00:09:39,915
Cortar com nossos sentimentos. 

165
00:09:40,060 --> 00:09:43,940
Então podemos começar a realmente meditar. 

166
00:09:51,140 --> 00:09:55,440
Agora que estamos tendo uma postura mais tranquila .. 

167
00:09:56,040 --> 00:09:59,380
Estou na frente de uma parede, meditando .. 

168
00:09:59,685 --> 00:10:01,975
E com esse silêncio .. 

169
00:10:02,495 --> 00:10:04,675
Estou percebendo o fato de que .. 

170
00:10:05,480 --> 00:10:09,440
Minha cabeça está muito ativa. 

171
00:10:09,780 --> 00:10:12,880
Então, eu levo em conta que aqui .. 

172
00:10:13,120 --> 00:10:17,080
Eu tenho muitos pensamentos .. 

173
00:10:17,080 --> 00:10:22,740
O que desaparecerá com o nosso olhar, você poderia dizer assim. 

174
00:10:23,040 --> 00:10:26,240
Pensamentos, imagens visuais .. 

175
00:10:26,245 --> 00:10:28,235
Soa .. 

176
00:10:28,865 --> 00:10:29,945
Muitas coisas. 

177
00:10:31,420 --> 00:10:33,520
Então, o que vamos fazer .. 

178
00:10:33,720 --> 00:10:37,075
Em nossa meditação zen é .. 

179
00:10:37,080 --> 00:10:40,205
Para deixar ir esses pensamentos. 

180
00:10:40,205 --> 00:10:43,180
Qual é um ponto fundamental .. 

181
00:10:43,180 --> 00:10:45,500
Da postura de meditação zen. 

182
00:10:45,640 --> 00:10:49,200
Enquanto eu sou assim no zazen .. 

183
00:10:49,200 --> 00:10:53,380
Não estou passando meu tempo refletindo sobre .. 

184
00:10:53,380 --> 00:10:57,240
Seja o que for, talvez interessante, talvez não .. 

185
00:10:57,420 --> 00:10:59,340
Não é sobre isso. 

186
00:10:59,580 --> 00:11:03,700
É sobre, o mesmo que colocamos nosso corpo .. 

187
00:11:03,715 --> 00:11:06,685
Em uma situação tranquila .. 

188
00:11:06,825 --> 00:11:10,215
Vou colocar minha mente e mental .. 

189
00:11:10,505 --> 00:11:13,415
O que é sempre agitado .. 

190
00:11:13,415 --> 00:11:16,035
Vou colocar .. 

191
00:11:16,035 --> 00:11:19,035
Do mesmo jeito.. 

192
00:11:19,040 --> 00:11:20,500
Em um estado silencioso de ser. 

193
00:11:20,680 --> 00:11:23,640
Como simplesmente disse .. 

194
00:11:23,640 --> 00:11:26,620
Deixando meus pensamentos irem. 

195
00:11:27,760 --> 00:11:31,800
Nossos pensamentos brotam de fora. 

196
00:11:32,680 --> 00:11:35,200
Para demonstrar isso; se nos perguntarmos .. 

197
00:11:35,200 --> 00:11:39,040
O que vamos pensar em 5 minutos ou em meia hora .. 

198
00:11:39,305 --> 00:11:42,575
Ou amanhã? Ninguém pode saber .. 

199
00:11:43,440 --> 00:11:46,780
Então, mesmo enquanto estamos dormindo .. 

200
00:11:46,960 --> 00:11:50,080
Nós temos sonhos. Então, nós sempre continuamos .. 

201
00:11:50,540 --> 00:11:53,440
Uma atividade mental, conscientemente ou não. 

202
00:11:53,660 --> 00:11:57,000
Durante a meditação zen, essa atividade mental .. 

203
00:11:57,165 --> 00:12:00,445
Nós vamos derrubá-lo e acalmá-lo. 

204
00:12:00,760 --> 00:12:04,760
Como dizemos; é deixar seus pensamentos passarem. 

205
00:12:05,100 --> 00:12:07,520
Mas aí você vai dizer, ok muito bom mas .. 

206
00:12:07,520 --> 00:12:08,900
Como fazer isso? 

207
00:12:09,080 --> 00:12:12,920
Isso podemos fazer através da nossa respiração. 

208
00:12:13,240 --> 00:12:16,400
Isso me leva ao terceiro ponto; 

209
00:12:16,440 --> 00:12:19,400
Como respirar durante o zazen? 

210
00:12:19,400 --> 00:12:23,920
Respiração zen 

211
00:12:26,740 --> 00:12:29,660
Respiração durante a meditação zen. 

212
00:12:29,880 --> 00:12:32,560
É uma respiração abdominal. 

213
00:12:34,415 --> 00:12:36,265
Nós inspiramos.. 

214
00:12:36,705 --> 00:12:39,985
Reabastecemos nossa capacidade pulmonar 

215
00:12:39,985 --> 00:12:42,185
Com alguma generosidade. 

216
00:12:42,925 --> 00:12:45,245
E vamos nos concentrar .. 

217
00:12:45,245 --> 00:12:46,740
Especialmente.. 

218
00:12:47,120 --> 00:12:49,900
Em nossa expiração 

219
00:12:50,200 --> 00:12:52,560
Para devolver o ar. 

220
00:12:52,740 --> 00:12:55,875
Esta forma de expiração, pouco a pouco .. 

221
00:12:55,875 --> 00:12:59,165
Tentamos demorar. 

222
00:12:59,225 --> 00:13:02,275
Um longo período de expiração. 

223
00:13:02,345 --> 00:13:05,135
Significado, para expirar o ar .. 

224
00:13:05,140 --> 00:13:06,720
Pelo nariz .. 

225
00:13:07,000 --> 00:13:10,360
Suavemente sem fazer barulho. 

226
00:13:10,700 --> 00:13:13,080
Estamos em uma meditação silenciosa .. 

227
00:13:13,300 --> 00:13:16,240
Nós exalamos o ar pelo nariz. 

228
00:13:17,160 --> 00:13:21,260
Tentando estender a expiração. 

229
00:13:21,620 --> 00:13:23,880
E no final da sua expiração .. 

230
00:13:24,625 --> 00:13:27,785
Nós respiramos e novamente .. 

231
00:13:36,465 --> 00:13:39,565
Soltamos o ar e novamente … 

232
00:13:43,440 --> 00:13:46,980
Então, se você concentrar sua concentração em .. 

233
00:13:47,240 --> 00:13:50,420
o ir e vir, você estabelece .. 

234
00:13:50,820 --> 00:13:53,500
você acalma sua respiração .. 

235
00:13:54,540 --> 00:13:57,495
Você deve deixá-lo em silêncio .. 

236
00:13:57,500 --> 00:14:02,600
Você deve aumentar a expiração .. 

237
00:14:03,160 --> 00:14:07,180
E isso faz com que seu diafragma esteja caindo. 

238
00:14:07,480 --> 00:14:08,740
Desce. 

239
00:14:08,980 --> 00:14:11,980
Dará uma massagem interior ao intestino. 

240
00:14:11,980 --> 00:14:13,980
O que lhes fará bem. 

241
00:14:14,160 --> 00:14:17,700
E lentamente você pode sentir .. 

242
00:14:18,160 --> 00:14:20,820
Como se algo estivesse inflando. 

243
00:14:21,145 --> 00:14:23,720
Na zona em que colocamos nossas mãos. 

244
00:14:24,020 --> 00:14:27,700
Então, é aqui que as coisas estão acontecendo. 

245
00:14:28,040 --> 00:14:30,520
Portanto, não está aqui, mas está .. 

246
00:14:31,900 --> 00:14:34,925
Lá em baixo. 

247
00:14:34,925 --> 00:14:39,360
Respirando pelo nariz e em silêncio. 

248
00:14:46,440 --> 00:14:49,900
A postura física: cruze as pernas. 

249
00:14:50,020 --> 00:14:52,860
Os joelhos caindo no chão. 

250
00:14:53,420 --> 00:14:56,140
A pelve torceu ligeiramente para a frente. 

251
00:14:56,700 --> 00:15:00,760
Endireitamos a coluna o melhor que podemos. 

252
00:15:01,160 --> 00:15:02,840
Até a coroa da nossa cabeça .. 

253
00:15:02,840 --> 00:15:08,040
Empurramos o céu com a cabeça. 

254
00:15:08,540 --> 00:15:11,800
A mão esquerda dentro da mão direita. 

255
00:15:11,925 --> 00:15:13,925
Os polegares em uma linha horizontal. 

256
00:15:13,925 --> 00:15:17,700
Não como um vale, não como uma montanha, um topo. 

257
00:15:17,960 --> 00:15:20,100
Apenas em linha reta. 

258
00:15:20,100 --> 00:15:24,140
Quando você coloca as mãos em contato com a barriga. 

259
00:15:24,500 --> 00:15:26,640
Você usa algo sob .. 

260
00:15:26,665 --> 00:15:28,675
Para ter uma posição mais confortável. 

261
00:15:29,125 --> 00:15:31,805
Você tem os cotovelos retos. 

262
00:15:31,805 --> 00:15:32,885
Esta é a nossa postura física. 

263
00:15:33,645 --> 00:15:37,065
Em segundo lugar, o nosso estado mental. 

264
00:15:37,280 --> 00:15:41,760
Nós nos cortamos do exterior. 

265
00:15:42,160 --> 00:15:44,060
De sons .. 

266
00:15:44,060 --> 00:15:46,760
Nós não vamos meditar com um rádio. 

267
00:15:46,760 --> 00:15:48,755
Ou com o seu celular ligado. 

268
00:15:48,755 --> 00:15:51,635
Então desligue-os, mas isso é lógico. 

269
00:15:52,040 --> 00:15:54,620
Cortamos todos os sons. 

270
00:15:54,620 --> 00:15:58,295
Trazemos nosso olhar para dentro e voltamos a .. 

271
00:15:58,300 --> 00:16:02,040
Ser o observador do que está acontecendo por dentro. 

272
00:16:02,120 --> 00:16:05,000
E o que está acontecendo lá dentro? 

273
00:16:05,380 --> 00:16:07,180
Percebemos que há uma atividade mental. 

274
00:16:07,660 --> 00:16:10,720
Pensamentos estão queimando .. 

275
00:16:10,725 --> 00:16:12,665
Nós não podemos controlá-los. 

276
00:16:13,155 --> 00:16:16,415
Os pensamentos surgem automaticamente. 

277
00:16:16,440 --> 00:16:21,000
Portanto, não se trata de impedir essa excitação. 

278
00:16:21,380 --> 00:16:24,740
Melhor dito, é sobre .. 

279
00:16:24,740 --> 00:16:29,740
Não pulando no trem dos nossos pensamentos. 

280
00:16:29,960 --> 00:16:33,420
E falamos de deixá-los passar. 

281
00:16:33,500 --> 00:16:36,385
Como podemos deixá-los passar? 

282
00:16:36,385 --> 00:16:41,380
Primeiro de tudo, com a concentração do aqui e do agora .. 

283
00:16:41,540 --> 00:16:43,260
Em nossa postura física. 

284
00:16:43,795 --> 00:16:46,835
E com essa concentração .. 

285
00:16:46,840 --> 00:16:51,640
Aqui e agora, na minha respiração. 

286
00:16:52,080 --> 00:16:54,960
Estou plenamente consciente da minha respiração .. 

287
00:16:55,265 --> 00:16:58,505
Da minha respiração abdominal. 

288
00:16:58,540 --> 00:17:02,580
Então respiramos pelo nariz .. 

289
00:17:03,080 --> 00:17:04,520
Nós inalamos .. 

290
00:17:05,095 --> 00:17:08,385
Tentamos estender a expiração. 

291
00:17:08,900 --> 00:17:10,460
Cada vez.. 

292
00:17:10,700 --> 00:17:14,040
Mais suave, mais longo. 

293
00:17:14,740 --> 00:17:17,735
E no final da expiração, começamos novamente. 

294
00:17:17,740 --> 00:17:19,800
Está indo e vindo como .. 

295
00:17:20,080 --> 00:17:23,920
Uma onda no oceano. As ondas. 

296
00:17:24,360 --> 00:17:28,340
Mas … muito, muito, muito. 

297
00:17:29,180 --> 00:17:32,605
Se você está se concentrando no aqui e agora .. 

298
00:17:32,605 --> 00:17:36,620
Nos pontos da postura 

299
00:17:36,920 --> 00:17:41,140
Significando que não estamos dormindo, durante a meditação .. 

300
00:17:41,680 --> 00:17:44,880
E se você se concentrar ao mesmo tempo .. 

301
00:17:45,040 --> 00:17:48,500
Ao estabelecer sua respiração .. 

302
00:17:48,680 --> 00:17:51,140
De forma harmonizada e silenciosa. 

303
00:17:51,140 --> 00:17:53,780
E com uma expiração prolongada .. 

304
00:17:53,940 --> 00:17:57,680
Você não terá tempo para pensar em si mesmo ou .. 

305
00:17:57,800 --> 00:18:00,060
Ou em qualquer outra coisa, por mais interessante que seja. 

306
00:18:00,295 --> 00:18:03,365
Então a conclusão é .. 

307
00:18:03,365 --> 00:18:05,665
Vamos meditar 20 minutos, meia hora, uma hora .. 

308
00:18:06,820 --> 00:18:08,540
Você decide. 

309
00:18:08,720 --> 00:18:12,380
Mas esse momento de meditação, de concentração .. 

310
00:18:12,455 --> 00:18:15,635
Vai lhe dar benefícios imediatos. 

311
00:18:15,635 --> 00:18:17,255
Você vai se sentir .. 

312
00:18:18,405 --> 00:18:21,405
Um pouco mais quieto. 

313
00:18:21,405 --> 00:18:24,665
Um pouco mais leve. 

314
00:18:24,665 --> 00:18:27,355
E melhor ainda .. 

315
00:18:27,355 --> 00:18:29,355
Você criará uma distância entre .. 

316
00:18:29,355 --> 00:18:32,445
você e todas as emoções que surgem em sua vida. 

317
00:18:32,885 --> 00:18:34,395
E isto.. 

318
00:18:34,785 --> 00:18:37,725
É realmente uma boa ferramenta 

319
00:18:38,135 --> 00:18:41,015
A maior parte do tempo.. 

320
00:18:41,015 --> 00:18:44,055
Mas especialmente agora .. 

321
00:18:44,060 --> 00:18:48,420
Essa metade da nossa população está confinada. 

322
00:19:29,920 --> 00:19:34,960
Conselho para iniciantes 

323
00:19:35,300 --> 00:19:40,380
Em vez de ficar com raiva ou fazer um barulho .. 

324
00:19:40,380 --> 00:19:45,380
Em corcéis de espalhar pensamentos mais ou menos .. 

325
00:19:45,380 --> 00:19:48,160
De menor dimensão .. 

326
00:19:48,420 --> 00:19:49,840
Sentar-se. 

327
00:19:49,995 --> 00:19:53,325
Em frente a uma parede. Adote esta meditação. 

328
00:19:54,440 --> 00:19:58,320
Trabalhem sozinhos dessa maneira. 

329
00:19:58,760 --> 00:20:00,620
E você logo notará .. 

330
00:20:00,625 --> 00:20:02,925
Quão bem isso vai fazer você. 

