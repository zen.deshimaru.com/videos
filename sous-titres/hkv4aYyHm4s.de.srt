1
00:00:13,360 --> 00:00:15,750
Eine Erzählung von Meister Deshimaru

2
00:00:17,320 --> 00:00:19,320
das "Walking in the Mountain" genannt wird.

3
00:00:19,980 --> 00:00:21,980
Also schon nur der Titel,

4
00:00:22,560 --> 00:00:24,080
Es ist wunderbar,

5
00:00:24,609 --> 00:00:30,360
in den Bergen wandern zu können. Ein Meister pflegte in den Bergen spazieren zu gehen, und als er zurückkam

6
00:00:31,179 --> 00:00:33,179
fragte ihn einer seiner Jünger

7
00:00:33,700 --> 00:00:35,700
"Meister, wo sind Sie spazieren gegangen?"

8
00:00:35,980 --> 00:00:40,420
"Im Berg", antwortete der Meister, und der Schüler bestand darauf: "Aber was für ein Weg

9
00:00:40,900 --> 00:00:43,380
haben Sie genommen, was haben Sie gesehen?"

10
00:00:45,260 --> 00:00:52,940
Der Meister antwortete: "Ich folgte dem Duft der Blumen und wanderte an den jungen Frühlingssprossen entlang".

11
00:00:55,480 --> 00:00:57,480
Meister Deshimaru fährt fort

12
00:00:58,540 --> 00:01:02,180
"Man muss sich von Buddhas Dharma leiten lassen...

13
00:01:02,380 --> 00:01:03,820
vertrauen

14
00:01:04,140 --> 00:01:08,040
mit Kräutern und Blumen, die ziellos wachsen,

15
00:01:08,600 --> 00:01:10,220
ohne Egoismus,

16
00:01:10,440 --> 00:01:15,640
natürlich, unbewusst. Diese Antwort kam aus der Quelle der Weisheit

17
00:01:16,900 --> 00:01:20,560
Wahre Weisheit muss jenseits des Wissens geschaffen werden...

18
00:01:21,100 --> 00:01:26,240
und Erinnerung". Das ist eine ziemlich kraftvolle Lehre.

19
00:01:26,700 --> 00:01:29,160
In einem Kusen,

20
00:01:29,340 --> 00:01:37,040
Meister Kosen lehrt im Dojo: "Die Philosophie von Shikantaza,

21
00:01:37,900 --> 00:01:40,580
die Philosophie von "nur Zazen".

22
00:01:40,800 --> 00:01:47,380
unser Leben leiten muss, ist das wahre Wesen des Buddhismus, die vollkommenste Methode

23
00:01:48,249 --> 00:01:50,699
zazen selbst ist satori, das Erwachen.

24
00:01:51,520 --> 00:01:53,520
Wir machen einen Fehler.

25
00:01:54,159 --> 00:01:56,368
wenn Sie glauben, dass es vorher ein Zazen gibt

26
00:01:57,100 --> 00:02:02,460
und dass mit der Anhäufung von Zazen, Zazen, fünfzehn Tage, etwas geschieht, das Erwachen.

27
00:02:05,300 --> 00:02:11,520
Sofort löst jeder, der Zazen praktiziert, den Prozess des Erwachens aus.

28
00:02:13,020 --> 00:02:15,800
Nun, dieser Prozess passt nicht in unsere Kategorien.

29
00:02:16,900 --> 00:02:18,900
manchmal sagen Leute

30
00:02:18,959 --> 00:02:20,959
’’Zazen war sehr sehr hart,

31
00:02:21,420 --> 00:02:24,949
es war schlechtes Zazen" oder manchmal

32
00:02:25,860 --> 00:02:27,860
Es ist sehr bequem.

33
00:02:28,409 --> 00:02:35,929
und der Prozess, der Zazen auslöst, liegt jenseits unserer Kategorien oder unseres persönlichen Bewusstseins".

34
00:02:36,660 --> 00:02:42,169
Also, Dogens Methode, die Methode der anderen Meister, sie ist großartig, sie ist einzigartig.

35
00:02:43,799 --> 00:02:48,769
Es sind Lehren, die einem sehr einfach erscheinen, die man erhält,

36
00:02:50,190 --> 00:02:52,190
die wir während Zazen erhalten

37
00:02:53,400 --> 00:02:55,400
und dass wir

38
00:02:58,530 --> 00:03:02,750
integrieren, was bedeutet, dass wir sie uns zu eigen machen werden,

39
00:03:04,019 --> 00:03:09,319
sanft, tief, unbewusst, es ist eine wertvolle Lektion.
