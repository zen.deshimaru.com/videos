1
00:00:13,650 --> 00:00:16,440

alors moi je suis né à montpellier donc

2
00:00:16,440 --> 00:00:16,450
alors moi je suis né à montpellier donc
 

3
00:00:16,450 --> 00:00:17,330
alors moi je suis né à montpellier donc
en france

4
00:00:17,330 --> 00:00:17,340
en france
 

5
00:00:17,340 --> 00:00:20,160
en france
mais maintenant j’habite un sound mes

6
00:00:20,160 --> 00:00:20,170
mais maintenant j’habite un sound mes
 

7
00:00:20,170 --> 00:00:22,500
mais maintenant j’habite un sound mes
parents sont deux pays différents

8
00:00:22,500 --> 00:00:22,510
parents sont deux pays différents
 

9
00:00:22,510 --> 00:00:24,660
parents sont deux pays différents
du coup je voyage souvent entre la seule

10
00:00:24,660 --> 00:00:24,670
du coup je voyage souvent entre la seule
 

11
00:00:24,670 --> 00:00:26,940
du coup je voyage souvent entre la seule
de la france et aussi d’autres pays

12
00:00:26,940 --> 00:00:26,950
de la france et aussi d’autres pays
 

13
00:00:26,950 --> 00:00:28,650
de la france et aussi d’autres pays
parce qu’ils y voient je vois là avec

14
00:00:28,650 --> 00:00:28,660
parce qu’ils y voient je vois là avec
 

15
00:00:28,660 --> 00:00:32,159
parce qu’ils y voient je vois là avec
leur travail mais chaque été on vient

16
00:00:32,159 --> 00:00:32,169
leur travail mais chaque été on vient
 

17
00:00:32,169 --> 00:00:33,810
leur travail mais chaque été on vient
ici le temple dans la montagne

18
00:00:33,810 --> 00:00:33,820
ici le temple dans la montagne
 

19
00:00:33,820 --> 00:00:36,360
ici le temple dans la montagne
depuis l’usiné quand j’avais deux

20
00:00:36,360 --> 00:00:36,370
depuis l’usiné quand j’avais deux
 

21
00:00:36,370 --> 00:00:40,780
depuis l’usiné quand j’avais deux
semaines je crois on a le temps plein

22
00:00:40,780 --> 00:00:40,790
semaines je crois on a le temps plein
 

23
00:00:40,790 --> 00:00:44,950
semaines je crois on a le temps plein
en grandissant ici j’ai jamais toujours

24
00:00:44,950 --> 00:00:44,960
en grandissant ici j’ai jamais toujours
 

25
00:00:44,960 --> 00:00:48,070
en grandissant ici j’ai jamais toujours
venir ici quoi puis moi et mes amis

26
00:00:48,070 --> 00:00:48,080
venir ici quoi puis moi et mes amis
 

27
00:00:48,080 --> 00:00:50,380
venir ici quoi puis moi et mes amis
on sait tout le temps ouais on va

28
00:00:50,380 --> 00:00:50,390
on sait tout le temps ouais on va
 

29
00:00:50,390 --> 00:00:53,170
on sait tout le temps ouais on va
continuer à venir ici pendant même quand

30
00:00:53,170 --> 00:00:53,180
continuer à venir ici pendant même quand
 

31
00:00:53,180 --> 00:00:55,120
continuer à venir ici pendant même quand
on est adulte pas mais on se disait

32
00:00:55,120 --> 00:00:55,130
on est adulte pas mais on se disait
 

33
00:00:55,130 --> 00:00:56,530
on est adulte pas mais on se disait
jamais qu’on allait devoir faire jaser

34
00:00:56,530 --> 00:00:56,540
jamais qu’on allait devoir faire jaser
 

35
00:00:56,540 --> 00:00:58,420
jamais qu’on allait devoir faire jaser
ne s’était pas c’était pas vraiment

36
00:00:58,420 --> 00:00:58,430
ne s’était pas c’était pas vraiment
 

37
00:00:58,430 --> 00:01:01,900
ne s’était pas c’était pas vraiment
quelque chose auquel on pensait les gens

38
00:01:01,900 --> 00:01:01,910
quelque chose auquel on pensait les gens
 

39
00:01:01,910 --> 00:01:03,070
quelque chose auquel on pensait les gens
qui viennent ici ils viennent ici pour

40
00:01:03,070 --> 00:01:03,080
qui viennent ici ils viennent ici pour
 

41
00:01:03,080 --> 00:01:05,860
qui viennent ici ils viennent ici pour
le zen moi quand je n’existais pour voir

42
00:01:05,860 --> 00:01:05,870
le zen moi quand je n’existais pour voir
 

43
00:01:05,870 --> 00:01:08,950
le zen moi quand je n’existais pour voir
les jambes pour la nourriture pour jouer

44
00:01:08,950 --> 00:01:08,960
les jambes pour la nourriture pour jouer
 

45
00:01:08,960 --> 00:01:12,730
les jambes pour la nourriture pour jouer
et du coup j’ai découvert là la sangha

46
00:01:12,730 --> 00:01:12,740
et du coup j’ai découvert là la sangha
 

47
00:01:12,740 --> 00:01:15,550
et du coup j’ai découvert là la sangha
avant le zen moi et mon avis tu as is on

48
00:01:15,550 --> 00:01:15,560
avant le zen moi et mon avis tu as is on
 

49
00:01:15,560 --> 00:01:17,590
avant le zen moi et mon avis tu as is on
avait aussi un autre ami vivien qui

50
00:01:17,590 --> 00:01:17,600
avait aussi un autre ami vivien qui
 

51
00:01:17,600 --> 00:01:19,840
avait aussi un autre ami vivien qui
avait un vent de plus que nous et lui

52
00:01:19,840 --> 00:01:19,850
avait un vent de plus que nous et lui
 

53
00:01:19,850 --> 00:01:22,320
avait un vent de plus que nous et lui
pour nous c’est un peu comme le mettre

54
00:01:22,320 --> 00:01:22,330
pour nous c’est un peu comme le mettre
 

55
00:01:22,330 --> 00:01:25,719
pour nous c’est un peu comme le mettre
dans le dojo rue godot il nous faisait

56
00:01:25,719 --> 00:01:25,729
dans le dojo rue godot il nous faisait
 

57
00:01:25,729 --> 00:01:30,430
dans le dojo rue godot il nous faisait
croire en la magie il nous faisait

58
00:01:30,430 --> 00:01:30,440
croire en la magie il nous faisait
 

59
00:01:30,440 --> 00:01:32,080
croire en la magie il nous faisait
croire toutes sortes de choses par

60
00:01:32,080 --> 00:01:32,090
croire toutes sortes de choses par
 

61
00:01:32,090 --> 00:01:34,270
croire toutes sortes de choses par
exemple une femme quand été sortie par

62
00:01:34,270 --> 00:01:34,280
exemple une femme quand été sortie par
 

63
00:01:34,280 --> 00:01:37,690
exemple une femme quand été sortie par
la forêt il nous a dit à moitié fait

64
00:01:37,690 --> 00:01:37,700
la forêt il nous a dit à moitié fait
 

65
00:01:37,700 --> 00:01:39,880
la forêt il nous a dit à moitié fait
creuser dans le sol et parce qu’il ya

66
00:01:39,880 --> 00:01:39,890
creuser dans le sol et parce qu’il ya
 

67
00:01:39,890 --> 00:01:42,520
creuser dans le sol et parce qu’il ya
toujours des diamants en sous sol

68
00:01:42,520 --> 00:01:42,530
toujours des diamants en sous sol
 

69
00:01:42,530 --> 00:01:44,530
toujours des diamants en sous sol
peu importe pied il ya au moins un

70
00:01:44,530 --> 00:01:44,540
peu importe pied il ya au moins un
 

71
00:01:44,540 --> 00:01:48,130
peu importe pied il ya au moins un
diamant chaque de mètres du consulat

72
00:01:48,130 --> 00:01:48,140
diamant chaque de mètres du consulat
 

73
00:01:48,140 --> 00:01:51,490
diamant chaque de mètres du consulat
creusé et creusé et creusé et qu’en tout

74
00:01:51,490 --> 00:01:51,500
creusé et creusé et creusé et qu’en tout
 

75
00:01:51,500 --> 00:01:54,670
creusé et creusé et creusé et qu’en tout
cas nous deux on avait les deux faces

76
00:01:54,670 --> 00:01:54,680
cas nous deux on avait les deux faces
 

77
00:01:54,680 --> 00:01:57,310
cas nous deux on avait les deux faces
cachées à lui il a sorti un diamant de

78
00:01:57,310 --> 00:01:57,320
cachées à lui il a sorti un diamant de
 

79
00:01:57,320 --> 00:01:59,260
cachées à lui il a sorti un diamant de
sa poche et a fait semblant de sortir du

80
00:01:59,260 --> 00:01:59,270
sa poche et a fait semblant de sortir du
 

81
00:01:59,270 --> 00:02:02,440
sa poche et a fait semblant de sortir du
sol d’un diamant énorme emprunté à sa

82
00:02:02,440 --> 00:02:02,450
sol d’un diamant énorme emprunté à sa
 

83
00:02:02,450 --> 00:02:04,630
sol d’un diamant énorme emprunté à sa
mère puis ont été éblouis

84
00:02:04,630 --> 00:02:04,640
mère puis ont été éblouis
 

85
00:02:04,640 --> 00:02:09,270
mère puis ont été éblouis
on doit avoir quoi qu’à 30 comme ça et

86
00:02:09,270 --> 00:02:09,280
on doit avoir quoi qu’à 30 comme ça et
 

87
00:02:09,280 --> 00:02:12,220
on doit avoir quoi qu’à 30 comme ça et
pendant genre beaucoup donné après

88
00:02:12,220 --> 00:02:12,230
pendant genre beaucoup donné après
 

89
00:02:12,230 --> 00:02:14,500
pendant genre beaucoup donné après
parfois je me mettais à creuser un peu

90
00:02:14,500 --> 00:02:14,510
parfois je me mettais à creuser un peu
 

91
00:02:14,510 --> 00:02:15,850
parfois je me mettais à creuser un peu
insolent de souvent si je trouve un

92
00:02:15,850 --> 00:02:15,860
insolent de souvent si je trouve un
 

93
00:02:15,860 --> 00:02:18,940
insolent de souvent si je trouve un
diamant si jamais je me suis pas rendu

94
00:02:18,940 --> 00:02:18,950
diamant si jamais je me suis pas rendu
 

95
00:02:18,950 --> 00:02:21,100
diamant si jamais je me suis pas rendu
compte que j’ai peut-être dix ans

96
00:02:21,100 --> 00:02:21,110
compte que j’ai peut-être dix ans
 

97
00:02:21,110 --> 00:02:23,080
compte que j’ai peut-être dix ans
quelque chose que si dans sa peau je te

98
00:02:23,080 --> 00:02:23,090
quelque chose que si dans sa peau je te
 

99
00:02:23,090 --> 00:02:24,240
quelque chose que si dans sa peau je te
dit tout

100
00:02:24,240 --> 00:02:24,250
dit tout
 

101
00:02:24,250 --> 00:02:26,259
dit tout
james est différent pour tout le monde

102
00:02:26,259 --> 00:02:26,269
james est différent pour tout le monde
 

103
00:02:26,269 --> 00:02:28,120
james est différent pour tout le monde
je crois ma mère quand la fatigue de

104
00:02:28,120 --> 00:02:28,130
je crois ma mère quand la fatigue de
 

105
00:02:28,130 --> 00:02:30,009
je crois ma mère quand la fatigue de
leurs fans elle a tout de suite senti

106
00:02:30,009 --> 00:02:30,019
leurs fans elle a tout de suite senti
 

107
00:02:30,019 --> 00:02:31,830
leurs fans elle a tout de suite senti
qu’elle bleus pharsale reste de sa vie

108
00:02:31,830 --> 00:02:31,840
qu’elle bleus pharsale reste de sa vie
 

109
00:02:31,840 --> 00:02:34,810
qu’elle bleus pharsale reste de sa vie
mais je crois que pour moi après chaque

110
00:02:34,810 --> 00:02:34,820
mais je crois que pour moi après chaque
 

111
00:02:34,820 --> 00:02:38,680
mais je crois que pour moi après chaque
zen je commence à aimer plus

112
00:02:38,680 --> 00:02:38,690

 

113
00:02:38,690 --> 00:02:41,589

sur plus ou moins bien

114
00:02:41,589 --> 00:02:41,599
sur plus ou moins bien
 

115
00:02:41,599 --> 00:02:43,940
sur plus ou moins bien
le deuxième coup je me dis que j’aurais

116
00:02:43,940 --> 00:02:43,950
le deuxième coup je me dis que j’aurais
 

117
00:02:43,950 --> 00:02:49,270
le deuxième coup je me dis que j’aurais
m’extasier en plus c’est pas fini

118
00:02:49,270 --> 00:02:49,280

 

119
00:02:49,280 --> 00:02:52,960

j’ai toujours connu les annonces j’ai

120
00:02:52,960 --> 00:02:52,970
j’ai toujours connu les annonces j’ai
 

121
00:02:52,970 --> 00:02:54,730
j’ai toujours connu les annonces j’ai
bien depuis toujours en fait quand ma

122
00:02:54,730 --> 00:02:54,740
bien depuis toujours en fait quand ma
 

123
00:02:54,740 --> 00:02:57,430
bien depuis toujours en fait quand ma
mère était enceinte elle pratiquait déjà

124
00:02:57,430 --> 00:02:57,440
mère était enceinte elle pratiquait déjà
 

125
00:02:57,440 --> 00:02:59,830
mère était enceinte elle pratiquait déjà
le zazen du coup c’est un peu comme si

126
00:02:59,830 --> 00:02:59,840
le zazen du coup c’est un peu comme si
 

127
00:02:59,840 --> 00:03:03,280
le zazen du coup c’est un peu comme si
j’étais né dans du coup j’ai toujours

128
00:03:03,280 --> 00:03:03,290
j’étais né dans du coup j’ai toujours
 

129
00:03:03,290 --> 00:03:06,880
j’étais né dans du coup j’ai toujours
connu la sangha dans différents temples

130
00:03:06,880 --> 00:03:06,890
connu la sangha dans différents temples
 

131
00:03:06,890 --> 00:03:09,370
connu la sangha dans différents temples
que j’ai beaucoup de souvenirs j’ai

132
00:03:09,370 --> 00:03:09,380
que j’ai beaucoup de souvenirs j’ai
 

133
00:03:09,380 --> 00:03:13,809
que j’ai beaucoup de souvenirs j’ai
rencontré des amis ou des cabanes même

134
00:03:13,809 --> 00:03:13,819
rencontré des amis ou des cabanes même
 

135
00:03:13,819 --> 00:03:15,520
rencontré des amis ou des cabanes même
chose quoi y’a toujours été de demandes

136
00:03:15,520 --> 00:03:15,530
chose quoi y’a toujours été de demandes
 

137
00:03:15,530 --> 00:03:17,740
chose quoi y’a toujours été de demandes
différents et quand j’étais petit je le

138
00:03:17,740 --> 00:03:17,750
différents et quand j’étais petit je le
 

139
00:03:17,750 --> 00:03:19,540
différents et quand j’étais petit je le
cachais je racontais pas je disais juste

140
00:03:19,540 --> 00:03:19,550
cachais je racontais pas je disais juste
 

141
00:03:19,550 --> 00:03:21,460
cachais je racontais pas je disais juste
à mes amis oui je vais à la montagne et

142
00:03:21,460 --> 00:03:21,470
à mes amis oui je vais à la montagne et
 

143
00:03:21,470 --> 00:03:23,800
à mes amis oui je vais à la montagne et
tout mais je leur disais jamais je vais

144
00:03:23,800 --> 00:03:23,810
tout mais je leur disais jamais je vais
 

145
00:03:23,810 --> 00:03:25,840
tout mais je leur disais jamais je vais
faire je vais dans le zen je leur disais

146
00:03:25,840 --> 00:03:25,850
faire je vais dans le zen je leur disais
 

147
00:03:25,850 --> 00:03:27,820
faire je vais dans le zen je leur disais
pas ça parce que j’avais essayé une fois

148
00:03:27,820 --> 00:03:27,830
pas ça parce que j’avais essayé une fois
 

149
00:03:27,830 --> 00:03:28,390
pas ça parce que j’avais essayé une fois
quand même

150
00:03:28,390 --> 00:03:28,400
quand même
 

151
00:03:28,400 --> 00:03:29,979
quand même
ils m’avaient tous regardé très

152
00:03:29,979 --> 00:03:29,989
ils m’avaient tous regardé très
 

153
00:03:29,989 --> 00:03:31,180
ils m’avaient tous regardé très
bizarrement je me suis dit bon j’en

154
00:03:31,180 --> 00:03:31,190
bizarrement je me suis dit bon j’en
 

155
00:03:31,190 --> 00:03:34,120
bizarrement je me suis dit bon j’en
parle plus jamais et mais sinon ça m’a

156
00:03:34,120 --> 00:03:34,130
parle plus jamais et mais sinon ça m’a
 

157
00:03:34,130 --> 00:03:36,100
parle plus jamais et mais sinon ça m’a
jamais paru bizarre ça me paraissait

158
00:03:36,100 --> 00:03:36,110
jamais paru bizarre ça me paraissait
 

159
00:03:36,110 --> 00:03:38,590
jamais paru bizarre ça me paraissait
même plus normal d’aller d’y aller en

160
00:03:38,590 --> 00:03:38,600
même plus normal d’aller d’y aller en
 

161
00:03:38,600 --> 00:03:41,580
même plus normal d’aller d’y aller en
fait je savais à l’avancé si je venais

162
00:03:41,580 --> 00:03:41,590
fait je savais à l’avancé si je venais
 

163
00:03:41,590 --> 00:03:44,500
fait je savais à l’avancé si je venais
je devais faire un zen par jour mais

164
00:03:44,500 --> 00:03:44,510
je devais faire un zen par jour mais
 

165
00:03:44,510 --> 00:03:46,000
je devais faire un zen par jour mais
j’étais pas du tout contre cette idée

166
00:03:46,000 --> 00:03:46,010
j’étais pas du tout contre cette idée
 

167
00:03:46,010 --> 00:03:48,370
j’étais pas du tout contre cette idée
franchement ça à mon jeu je voulais

168
00:03:48,370 --> 00:03:48,380
franchement ça à mon jeu je voulais
 

169
00:03:48,380 --> 00:03:50,740
franchement ça à mon jeu je voulais
découvrir justement c’était mon choix de

170
00:03:50,740 --> 00:03:50,750
découvrir justement c’était mon choix de
 

171
00:03:50,750 --> 00:03:53,500
découvrir justement c’était mon choix de
venir ici d’abord pour voir toute la

172
00:03:53,500 --> 00:03:53,510
venir ici d’abord pour voir toute la
 

173
00:03:53,510 --> 00:03:54,699
venir ici d’abord pour voir toute la
saga que j’avais pas vu depuis deux

174
00:03:54,699 --> 00:03:54,709
saga que j’avais pas vu depuis deux
 

175
00:03:54,709 --> 00:03:59,640
saga que j’avais pas vu depuis deux
trois ans puis le lieu la nourriture et

176
00:03:59,640 --> 00:03:59,650
trois ans puis le lieu la nourriture et
 

177
00:03:59,650 --> 00:04:01,810
trois ans puis le lieu la nourriture et
tous les souvenirs qui reviennent en

178
00:04:01,810 --> 00:04:01,820
tous les souvenirs qui reviennent en
 

179
00:04:01,820 --> 00:04:03,819
tous les souvenirs qui reviennent en
fait dès que je viens la première fois

180
00:04:03,819 --> 00:04:03,829
fait dès que je viens la première fois
 

181
00:04:03,829 --> 00:04:06,370
fait dès que je viens la première fois
que j’ai fais hazen je me suis pas rendu

182
00:04:06,370 --> 00:04:06,380
que j’ai fais hazen je me suis pas rendu
 

183
00:04:06,380 --> 00:04:07,630
que j’ai fais hazen je me suis pas rendu
compte que je faisais ça donne en fait

184
00:04:07,630 --> 00:04:07,640
compte que je faisais ça donne en fait
 

185
00:04:07,640 --> 00:04:09,840
compte que je faisais ça donne en fait
c’était assez bizarre dans le sens où

186
00:04:09,840 --> 00:04:09,850
c’était assez bizarre dans le sens où
 

187
00:04:09,850 --> 00:04:12,280
c’était assez bizarre dans le sens où
j’ai pas perçu trop de changements en

188
00:04:12,280 --> 00:04:12,290
j’ai pas perçu trop de changements en
 

189
00:04:12,290 --> 00:04:15,310
j’ai pas perçu trop de changements en
fait j’ai pas parce que j’ai pas perçu

190
00:04:15,310 --> 00:04:15,320
fait j’ai pas parce que j’ai pas perçu
 

191
00:04:15,320 --> 00:04:17,979
fait j’ai pas parce que j’ai pas perçu
l’utilité du zen mais ça me paraît

192
00:04:17,979 --> 00:04:17,989
l’utilité du zen mais ça me paraît
 

193
00:04:17,989 --> 00:04:19,300
l’utilité du zen mais ça me paraît
tellement normal que j’ai pas vu de

194
00:04:19,300 --> 00:04:19,310
tellement normal que j’ai pas vu de
 

195
00:04:19,310 --> 00:04:21,880
tellement normal que j’ai pas vu de
changement en fait je trouve ça étrange

196
00:04:21,880 --> 00:04:21,890
changement en fait je trouve ça étrange
 

197
00:04:21,890 --> 00:04:23,650
changement en fait je trouve ça étrange
mais j’aime bien j’ai l’impression que

198
00:04:23,650 --> 00:04:23,660
mais j’aime bien j’ai l’impression que
 

199
00:04:23,660 --> 00:04:26,710
mais j’aime bien j’ai l’impression que
plus jean prat plus chanceux et plus

200
00:04:26,710 --> 00:04:26,720
plus jean prat plus chanceux et plus
 

201
00:04:26,720 --> 00:04:28,480
plus jean prat plus chanceux et plus
j’aime et peluches des coups puis ils me

202
00:04:28,480 --> 00:04:28,490
j’aime et peluches des coups puis ils me
 

203
00:04:28,490 --> 00:04:33,460
j’aime et peluches des coups puis ils me
paraissent différents

204
00:04:33,460 --> 00:04:33,470

 

205
00:04:33,470 --> 00:04:45,900

la dame a validé en france youtube en

206
00:04:45,900 --> 00:04:45,910

 

207
00:04:45,910 --> 00:05:05,550

france tous deux en simple et efficace

208
00:05:05,550 --> 00:05:05,560

 

209
00:05:05,560 --> 00:05:22,850

[Musique]

210
00:05:22,850 --> 00:05:22,860

 

211
00:05:22,860 --> 00:07:03,500

[Musique]

212
00:07:03,500 --> 00:07:03,510

 

213
00:07:03,510 --> 00:07:04,780

1

214
00:07:04,780 --> 00:07:04,790
1
 

215
00:07:04,790 --> 00:07:06,830
1
[Musique]

216
00:07:06,830 --> 00:07:06,840
[Musique]
 

217
00:07:06,840 --> 00:07:08,840
[Musique]
e

