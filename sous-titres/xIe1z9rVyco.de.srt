1
00:00:00,180 --> 00:00:04,420
Ich begann mit 16 Zen zu üben. 

2
00:00:04,740 --> 00:00:07,660
Ich werde 49. 

3
00:00:07,860 --> 00:00:14,360
Ich hatte versucht, die Position von Zazen alleine auf meinem Bett einzunehmen. 

4
00:00:14,960 --> 00:00:19,460
Ich habe es 2, 3, 4 Minuten durchgehalten, es war wirklich sehr, sehr schmerzhaft. 

5
00:00:19,700 --> 00:00:21,700
Ich konnte es überhaupt nicht tun. 

6
00:00:21,840 --> 00:00:30,800
Dort zeigten sie mir die Pose und ich hielt den normalen Zazen ungefähr eine halbe Stunde lang. 

7
00:00:31,120 --> 00:00:33,420
Wir machen uns bereit zu gehen und hier: 

8
00:00:33,640 --> 00:00:36,100
"Ah, vergiss nicht zu bezahlen!" 

9
00:00:36,540 --> 00:00:38,500
Ich dachte es wäre kostenlos … 

10
00:00:38,880 --> 00:00:42,580
Ich ging ohne meine Erlaubnis … 

11
00:00:45,460 --> 00:00:48,680
Ich übe im Zen Dojo von Lyon in Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
Es ist ein altes Dojo von Meister Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
Ich heiße Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
Mein Mönch heißt Ryurin. 

15
00:01:05,200 --> 00:01:09,700
Ich bin seit 25 Jahren ein Schüler von Meister Kosen. 

16
00:01:10,000 --> 00:01:13,000
Ich habe eine Weile gebraucht, um Meister Kosen zu treffen. 

17
00:01:13,320 --> 00:01:17,480
Ich kannte ihn, ich habe mit ihm Sesshins gemacht … 

18
00:01:17,760 --> 00:01:21,000
Aber ich war nicht sehr nah. 

19
00:01:21,340 --> 00:01:26,980
Ich bin aufgrund von Umständen, die außerhalb meiner Kontrolle liegen, näher gekommen. 

20
00:01:27,260 --> 00:01:31,800
Und ich konnte seine Lehre entdecken. 

21
00:01:32,160 --> 00:01:36,120
Also habe ich bis heute mit ihm Zazen geübt. 

22
00:01:36,280 --> 00:01:39,800
Meister Kosen gab mir vor etwa zehn Jahren, 

23
00:01:40,060 --> 00:01:42,920
wie es mit vielen seiner ehemaligen Anhänger tat, 

24
00:01:43,260 --> 00:01:46,900
das Shiho, die Übertragung von Dharma. 

25
00:01:47,740 --> 00:01:50,740
Zen-Meister, es ist ein Samu, es ist kein Titel. 

26
00:01:51,020 --> 00:01:57,420
Wenn wir "Meister" sagen, deutet dies auf die Idee hin, dass man etwas gemeistert hat, aber er ist ein Samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" ist ein chinesisches Wort, japanisch. 

28
00:02:01,740 --> 00:02:03,840
Dies sind die auszuführenden Aufgaben. 

29
00:02:04,100 --> 00:02:06,380
Er unterrichtet auch, er ist ein Samu. 

30
00:02:06,560 --> 00:02:08,720
Der Zen-Meister macht sein Samu … 

31
00:02:09,000 --> 00:02:11,020
Wenn er mit seinem Samurai fertig ist, geht er nach Hause. 

32
00:02:11,300 --> 00:02:12,980
Er wird zur Arbeit gehen oder nicht … 

33
00:02:13,280 --> 00:02:15,560
Er hat eine Frau und eine Familie oder nicht … 

34
00:02:15,980 --> 00:02:17,240
Es kommt auf jede Person an … 

35
00:02:18,060 --> 00:02:21,180
Und jetzt arbeitet er an etwas anderem … 

36
00:02:21,500 --> 00:02:23,120
Das ist das Leben! 

37
00:02:23,640 --> 00:02:26,660
Aber der Zen-Meister ist ein Schüler. 

38
00:02:27,060 --> 00:02:35,860
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

