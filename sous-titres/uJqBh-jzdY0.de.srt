1
00:00:06,180 --> 00:00:09,540
Zen ist mir egal. 

2
00:00:09,840 --> 00:00:12,100
Es ist nur ein weiteres Stück Kirchenmüll. 

3
00:00:12,480 --> 00:00:17,520
In den Vereinigten Staaten gibt es Tausende von Zen-Meistern. 

4
00:00:17,820 --> 00:00:20,880
Jeder möchte ein Zen-Meister sein. 

5
00:00:21,220 --> 00:00:24,680
Und Zen ist der letzte Schrei, 

6
00:00:25,280 --> 00:00:28,820
und ich bin nicht interessiert. 

7
00:00:29,320 --> 00:00:31,960
Warum denkst du, ist es der letzte Schrei? 

8
00:00:32,260 --> 00:00:36,240
Nein, jetzt ist es aus der Mode! 

9
00:00:37,300 --> 00:00:40,060
In Argentinien kauft jeder einen kleinen Zen-Garten. 

10
00:00:40,340 --> 00:00:42,720
Wir haben ihn ins Büro gebracht, in sein Wohnzimmer. 

11
00:00:43,020 --> 00:00:45,740
Es ist wunderschön. Es macht Spaß. Es macht Spaß. 

12
00:00:46,100 --> 00:00:49,000
Aber Zen ist nutzlos! 

13
00:00:53,900 --> 00:00:57,360
Was halten Sie von Krishnamurtis Gesichtsausdruck: 

14
00:00:57,640 --> 00:01:01,060
"Lass niemanden zwischen dich und die Wahrheit kommen"? 

15
00:01:01,380 --> 00:01:07,140
Wenn es eine Diskrepanz zwischen Ihnen und der Wahrheit gibt, ist es nicht die Wahrheit. 

16
00:01:11,480 --> 00:01:16,780
Er meinte, dass er immer gegen die Flut war. 

17
00:01:17,040 --> 00:01:21,960
Er hatte sich von der gesamten theologischen Gesellschaft getrennt, die ihn als Referenz nehmen wollte. 

18
00:01:22,300 --> 00:01:24,260
Bewundern Sie das an ihm? 

19
00:01:24,540 --> 00:01:31,440
Ja. Meine Mutter, als sie mit mir schwanger war, war eine Schülerin von Krishnamurti. 

20
00:01:31,760 --> 00:01:37,500
Eines Tages fühlte sie auf einer Konferenz eine Ekstase, einen Satori. 

21
00:01:37,760 --> 00:01:42,620
Sie sagte sich: "Mein Sohn wird ein Heiliger sein." 

22
00:01:43,020 --> 00:01:50,060
Ich mag Krishnamurti wirklich, er war eine außergewöhnliche Person. 

23
00:01:51,640 --> 00:01:56,940
Sai Baba? Ich weiß es nicht, ich kenne ihn nicht. 

24
00:01:57,360 --> 00:02:02,020
Er ist der einzige Lehrer, von dem ich je gehört habe, dass er Gott ist. 

25
00:02:02,320 --> 00:02:04,680
Aber jeder ist Gott! 

26
00:02:05,740 --> 00:02:11,540
Ein echter Meister muss nicht sagen "Ich bin Gott". 

27
00:02:11,940 --> 00:02:16,200
Aber du musst anderen sagen, dass du Gott bist. 

28
00:02:16,500 --> 00:02:22,740
Also sagt der Mann: "Ich bin Gott. Gib mir dein Geld!" 

29
00:02:23,060 --> 00:02:27,960
Und die Tatsache, dass er Tricks macht, wie Uhren erscheinen zu lassen, weißt du was? 

30
00:02:28,240 --> 00:02:32,940
Ich weiß es nicht Ich weiß es nicht Ich weiß es nicht Ja, vielleicht hat er eine Stärke. Ja das ist möglich 

31
00:02:33,080 --> 00:02:34,400
Ist es möglich Ja das ist möglich 

32
00:02:34,680 --> 00:02:37,040
Es gibt Zauberer, die sagen, es sei Illusionismus. 

33
00:02:37,320 --> 00:02:40,820
Es ist auch möglich, dass es ein Trick war. 

34
00:02:40,980 --> 00:02:48,180
Ich denke, alle Religionen, alle Kirchen sind gegen den Menschen. 

35
00:02:48,280 --> 00:02:50,280
Jeder ohne Unterschied? 

36
00:02:51,740 --> 00:02:54,760
Auch der Buddhismus, alle zusammen. 

37
00:02:55,020 --> 00:02:57,820
Das sind Kirchen. 

38
00:02:58,400 --> 00:03:00,400
Ich mag es nicht. 

39
00:03:02,740 --> 00:03:08,100
Das wahre Zen von Deshimaru ist keine Kirche, es ist eine Schule. 

40
00:03:08,480 --> 00:03:10,060
Es ist etwas anderes. 

41
00:03:10,380 --> 00:03:12,380
Wie können Sie den Unterschied erkennen? 

42
00:03:12,900 --> 00:03:16,740
Die Kirche, es sind die Japaner! 

43
00:03:20,060 --> 00:03:26,000
In einer Kirche suchen Menschen nach einem spirituellen Weg. 

44
00:03:26,400 --> 00:03:33,240
Und nach 10 Jahren suchen sie einen Abschluss in einer Hierarchie, eine Macht über andere. 

45
00:03:33,560 --> 00:03:42,620
Es ist eine schreckliche Sache, die die Wahrheit tötet. 

46
00:03:43,160 --> 00:03:45,680
Ich übe Zazen. 

47
00:03:46,040 --> 00:03:49,820
Ich übe die Lehren der Buddhas, was sehr einfach ist. 

48
00:03:50,120 --> 00:03:54,520
Für mich ist es ein Schatz der Menschheit. 

49
00:03:56,500 --> 00:03:58,640
Zazen [sitzende Meditation]. 

50
00:03:58,960 --> 00:04:02,180
Kinhin. [Langsam gehen] 

51
00:04:02,700 --> 00:04:04,700
Sampaï. 

52
00:04:04,900 --> 00:04:09,020
Genmai [Morgenmahlzeit] und Samu [Konzentration bei der Arbeit]. 

53
00:04:09,360 --> 00:04:10,880
Es ist sehr einfach. 

54
00:04:11,180 --> 00:04:12,880
Aber es ändert sich immer. 

55
00:04:13,140 --> 00:04:16,000
Sie fügen immer neue Dinge hinzu. 

56
00:04:16,320 --> 00:04:18,340
Ja, ich lerne. 

57
00:04:18,480 --> 00:04:27,580
Weil ich denke, ich lerne gerne und habe mehr Weisheit, um mich weiterzuentwickeln. 

58
00:04:27,880 --> 00:04:29,480
Frage mich. 

59
00:04:29,800 --> 00:04:33,660
Ich bin mehr ein Schüler als ein Meister. 

60
00:04:34,100 --> 00:04:35,740
Ich mag sie beide. 

61
00:04:36,120 --> 00:04:40,540
Ich bin ein guter Meister, aber ich bin ein sehr schlechter Schüler. 

62
00:04:40,820 --> 00:04:42,780
Sehr undiszipliniert. 

63
00:04:43,200 --> 00:04:46,160
Warst du schon immer so? In Deshimaru enthalten? 

64
00:04:46,400 --> 00:04:47,140
Ja. 

65
00:04:47,420 --> 00:04:48,840
Und er hat dich korrigiert? 

66
00:04:49,080 --> 00:04:50,700
Ja, er hat mir sehr geholfen. 

67
00:04:50,980 --> 00:04:54,240
Er hat mich beauftragt, es hat mich ein bisschen beruhigt. 

68
00:04:58,060 --> 00:05:00,280
"Was mache ich mit Stéphane?" 

69
00:05:00,560 --> 00:05:04,180
"Hier bin ich für ihn verantwortlich!" 

70
00:05:08,920 --> 00:05:11,860
Erinnerst du dich an den Tag, an dem du Deshimaru getroffen hast? 

71
00:05:12,120 --> 00:05:12,620
Ja. 

72
00:05:12,920 --> 00:05:14,920
Wie war es 

73
00:05:15,280 --> 00:05:16,820
Ich kann es nicht sagen 

74
00:05:17,060 --> 00:05:19,240
Ich war in Zazen und er war mit dem Kyosaku hinter mir. 

75
00:05:22,920 --> 00:05:28,320
Später kaufte ich Blumen in der U-Bahn auf dem Weg zum Dojo. 

76
00:05:28,640 --> 00:05:33,060
Am Eingang des Dojos sage ich: "Ich habe Blumen für den Meister". » 

77
00:05:33,400 --> 00:05:36,080
Und nach Zazen rufen sie mich an. 

78
00:05:36,340 --> 00:05:38,540
"Der Meister fragt nach dir." 

79
00:05:38,780 --> 00:05:42,860
Ich gehe in sein Zimmer, wo er sitzt. 

80
00:05:43,060 --> 00:05:46,760
"Wer hat mir diese Blumen gebracht?" 

81
00:05:47,040 --> 00:05:47,800
I. 

82
00:05:48,660 --> 00:05:51,580
"Gut, gut, gut." 

83
00:05:54,280 --> 00:05:58,080
Erinnerst du dich an den letzten Tag, als du ihn gesehen hast? 

84
00:05:58,580 --> 00:05:59,920
Er war tot. 

85
00:06:05,280 --> 00:06:07,280
Sein Körper würde verbrannt werden. 

86
00:06:07,520 --> 00:06:09,160
Auf Wiedersehen! 

87
00:06:10,320 --> 00:06:12,320
Was ist mit dem letzten Tag, an dem du ihn lebend gesehen hast? 

88
00:06:13,860 --> 00:06:19,800
Dann ging er zum letzten Mal nach Japan und sagte "Auf Wiedersehen!". 

89
00:06:20,200 --> 00:06:23,680
An dem Tag, als Deshimaru starb, hast du geweint? 

90
00:06:24,360 --> 00:06:34,420
Als er starb, war ich im Dojo und saß in Zazen … 

91
00:06:37,260 --> 00:06:40,780
Jemand betritt das Dojo und sagt: 

92
00:06:41,060 --> 00:06:45,840
"Wir müssen ohne Deshimaru weitermachen." 

93
00:06:46,100 --> 00:06:48,900
Ich habe in Zazen gelebt. 

94
00:06:49,200 --> 00:06:50,560
Ich stand immer vor ihm. 

95
00:06:50,900 --> 00:06:53,500
Ich war immer eine Säule für ihn. 

96
00:06:53,940 --> 00:06:57,120
So habe ich seinen Tod erlebt. 

97
00:06:57,420 --> 00:07:02,200
Ich musste für die anderen stark bleiben. 

98
00:07:02,840 --> 00:07:11,040
Dann ging ich direkt zur Einäscherung nach Japan. 

99
00:07:13,100 --> 00:07:23,920
Es gab eine Zeremonie mit vielen Lehrern, mit der gesamten japanischen Kirche. Riesig. 

100
00:07:24,660 --> 00:07:31,060
Und es war in einer Schachtel mit vielen Blumen. 

101
00:07:31,500 --> 00:07:37,790
Sein sehr schönes Gesicht, sehr lächelnd. 

102
00:07:38,100 --> 00:07:50,620
Wenn er am Leben war, wenn er einen Hut oder eine falsche Nase aufsetzte, wurde er sehr komisch. 

103
00:07:50,940 --> 00:07:54,380
Dort war es genauso, mit den Blumen, die es umgaben. 

104
00:07:54,720 --> 00:07:59,520
Wir haben die Zeremonie gemacht, ich blieb sehr konzentriert und ungerührt. 

105
00:07:59,820 --> 00:08:05,400
Dann setzen wir die Box in ein Auto. 

106
00:08:05,880 --> 00:08:09,720
An vier Schüler. 

107
00:08:09,840 --> 00:08:20,280
Ich war der letzte und schiebe die Kiste auf einen Wagen. 

108
00:08:20,600 --> 00:08:27,160
Im letzten Moment drücke ich meinen Finger mit der Brust. 

109
00:08:29,180 --> 00:08:37,700
Ich fing an zu weinen und konnte zwei oder drei Stunden lang nicht aufhören. 

110
00:08:38,020 --> 00:08:42,220
Es kam heraus und ich weinte und weinte. 

111
00:08:42,520 --> 00:08:45,560
Alle schauten mich an, weil niemand so weinte. 

112
00:08:46,040 --> 00:08:51,440
Ich weinte ununterbrochen vor der Familie und den anderen Teilnehmern. 

113
00:08:52,280 --> 00:08:57,500
Ich konnte mich nicht aufhalten. 

114
00:09:00,560 --> 00:09:05,720
Bis ich den Rauch aus dem Schornstein kommen sah. 

115
00:09:19,379 --> 00:09:27,799
Tot - lebendig, alles lebt, alles ist tot. 

116
00:09:28,840 --> 00:09:33,500
Wir leben noch, aber wir sind tot. 

117
00:09:33,800 --> 00:09:42,220
Wenn du stirbst, siehst du dein Leben vorbeiziehen. 

118
00:09:42,540 --> 00:09:48,580
Wir haben jetzt unser ganzes Leben betrachtet. 

119
00:10:10,010 --> 00:10:13,620
Zazen ist der Normalzustand. 

