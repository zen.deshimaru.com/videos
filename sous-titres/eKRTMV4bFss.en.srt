1
00:00:08,480 --> 00:00:13,559
hello everyone I will explain how you 

2
00:00:13,559 --> 00:00:20,760
sit down to practice zen meditation called zazen the necessary material 

3
00:00:20,760 --> 00:00:27,090
it’s very simple a human being and a meditation chick if you have a 

4
00:00:27,090 --> 00:00:30,869
cushion rather hamburg and that’s fine 

5
00:00:30,869 --> 00:00:35,730
and if not you make one with a blanket where a sleeping bag by 

6
00:00:35,730 --> 00:00:41,060
example it must be firm enough 

7
00:00:57,490 --> 00:01:06,070
So according to you explains the Zen meditation posture Zen meditation is 

8
00:01:06,070 --> 00:01:13,620
one above all to know that it is an immobile and silent sitting posture 

9
00:01:13,620 --> 00:01:22,630
and the particularity of this posture and that we practice it facing the wall here 

10
00:01:22,630 --> 00:01:26,620
Obviously I face the camera to inform you 

11
00:01:26,620 --> 00:01:32,560
so what you have to do is sit together cushion the one I 

12
00:01:32,560 --> 00:01:37,479
explain it that needs to be sufficiently padded insufficiently hard you 

13
00:01:37,479 --> 00:01:42,520
sit well at the bottom of the chick don’t sit on the edge if you have 

14
00:01:42,520 --> 00:01:49,330
slipped and you should as far as possible cross your legs to 

15
00:01:49,330 --> 00:01:55,600
cross them like these like these like alone and it’s called the 2010 if 

16
00:01:55,600 --> 00:02:04,270
you put your other people here it’s called lotus the important thing is that 

17
00:02:04,270 --> 00:02:13,480
your knees go to the ground to press put knees pressure on the floor 

18
00:02:13,480 --> 00:02:19,780
so how to do that to do it without any organization of without any effort 

19
00:02:19,780 --> 00:02:27,810
physical good muscles at all zen meditation posture than let go 

20
00:02:27,810 --> 00:02:37,950
so that your knees go down you’ll tilt the pelvis slightly 

21
00:02:37,950 --> 00:02:46,180
forward the top of the pelvis goes forward like 6 to a little bit last I 

22
00:02:46,180 --> 00:02:49,440
shows it to you in profile 

23
00:02:55,200 --> 00:03:02,769
my pelvis is not cynical as if for example i am driving 

24
00:03:02,769 --> 00:03:07,629
a car see there but i show us last 

25
00:03:07,629 --> 00:03:11,860
because my center of gravity is behind my body I want to 

26
00:03:11,860 --> 00:03:19,420
looks high quality hi in front and I do this at dassin bridges naturally the 

27
00:03:19,420 --> 00:03:24,970
what we would like and pass in front with gravity my knees go to the ground 

28
00:03:24,970 --> 00:03:32,970
this is what you must start by establishing in the posture a seat 

29
00:03:32,970 --> 00:03:42,609
in which the knee goes on strike when you have performed 7 to 6 from the pelvis 

30
00:03:42,609 --> 00:03:44,920
who slightly tilted to make the request 

31
00:03:44,920 --> 00:03:51,940
you will straighten the spine as much as possible up to 

32
00:03:51,940 --> 00:03:57,000
top of the head consider our are considered that 

33
00:03:57,000 --> 00:04:05,889
cologne goes as far as if i would put a game is not a hook i reread 

34
00:04:05,889 --> 00:04:12,280
which means that I pulled the nape of the neck I fit my chin see I don’t have the 

35
00:04:12,280 --> 00:04:16,349
head spinning in front i don’t have a curved back 

36
00:04:16,349 --> 00:04:24,610
I am as straight as possible I show you two profiles I am not 

37
00:04:24,610 --> 00:04:33,910
like i’m not at auction before or whatever i’m the most 

38
00:04:33,910 --> 00:04:43,659
of cross also when you have managed to sit up straight you do not move 

39
00:04:43,659 --> 00:04:53,080
the more I continue the left hand the fingers are joined the right hand 

40
00:04:53,080 --> 00:04:58,690
the fingers are blessed and I’m going to superimpose the left fingers on the 

41
00:04:58,690 --> 00:05:08,030
tracks of the right hand but pushes meet overlap the finger of the 

42
00:05:08,030 --> 00:05:14,300
left hand a horizontal line forms the edge of my hands 

43
00:05:14,300 --> 00:05:17,840
so we could the county over all the wrist 

44
00:05:17,840 --> 00:05:23,480
until then the edge of my hands tomorrow you will apply it to the base 

45
00:05:23,480 --> 00:05:31,370
of the abdomen to do this used something that 

46
00:05:31,370 --> 00:05:37,460
can allow you to stall at the hands of terrasson that you can release 

47
00:05:37,460 --> 00:05:43,780
full attention from the shoulders the posture of a posture where you have to let go 

48
00:05:43,780 --> 00:05:47,990
release the tension you see my elbows are free i’m a long time 

49
00:05:47,990 --> 00:05:56,090
like this o like this quietly but I don’t make an effort to maintain 

50
00:05:56,090 --> 00:06:01,070
my hands the effort ideas to maintain the pushes at the restaurant 

51
00:06:01,070 --> 00:06:08,650
once i realize this is the point of posture i touch it 

52
00:06:08,650 --> 00:06:13,120
this is physical posture 

53
00:06:20,000 --> 00:06:25,460
after this first part on physical posture second part 

54
00:06:25,460 --> 00:06:30,860
the mental attitude once I’m in the posture of 

55
00:06:30,860 --> 00:06:39,220
zen meditation facing the wall what do i do what should i do 

56
00:06:39,220 --> 00:06:46,190
in the silence of this posture you will go you others 

57
00:06:46,190 --> 00:06:53,390
realize that you think you see that you keep thinking and that there are 

58
00:06:53,390 --> 00:06:58,490
Thousands of thought brushes that come that come said come to you 

59
00:06:58,490 --> 00:07:03,010
come on realize that this is a natural phenomenon 

60
00:07:03,010 --> 00:07:08,840
you cannot control what we will do during Zen meditation 

61
00:07:08,840 --> 00:07:16,790
we will let our thoughts pass, that is to say that in this immobility 

62
00:07:16,790 --> 00:07:23,150
we will be able to observe that with a ransom you will observe your thoughts and 

63
00:07:23,150 --> 00:07:29,419
thoughts instead of nurturing them instead of growing them you go 

64
00:07:29,419 --> 00:07:34,610
let them pass so it’s not about refusing what 

65
00:07:34,610 --> 00:07:39,580
let it be there again this is the machine you will let pass 

66
00:07:39,580 --> 00:07:46,190
how to let through attention focus 

67
00:07:46,190 --> 00:07:51,400
here now on your breath 

68
00:07:55,430 --> 00:07:59,960
breathing in zen is abdominal breathing 

69
00:07:59,960 --> 00:08:10,400
you breathe in through your nostril and you breathe in slowly slowly 

70
00:08:10,400 --> 00:08:14,270
noiseless recess silent meditation 

71
00:08:14,270 --> 00:08:20,090
the characteristic that breathing and you’re going to bring your attention 

72
00:08:20,090 --> 00:08:30,930
on the face of exaltation we inspire by arriving quietly in being 

73
00:08:30,930 --> 00:08:37,620
generous fill and face the nation when 

74
00:08:37,620 --> 00:08:44,070
you go out all the air you do it slowly slowly as for 

75
00:08:44,070 --> 00:08:47,720
get longer and longer 

76
00:08:56,540 --> 00:09:03,889
when you are at the end of your relationships wing you yearn and again 

77
00:09:03,889 --> 00:09:12,709
you exhale and you were no longer slowly deeper naked the longer 

78
00:09:12,709 --> 00:09:18,069
deep new like the waves on an ocean 

79
00:09:18,069 --> 00:09:31,639
because they are the beach and all these speak with the v6 you focus 

80
00:09:31,639 --> 00:09:36,440
here now on the points of the physical points posture 

81
00:09:36,440 --> 00:09:42,259
so watch out watch out here now your body if you 

82
00:09:42,259 --> 00:09:47,660
focused here now at the same time not to follow your 

83
00:09:47,660 --> 00:09:55,040
thoughts let’s go back to the points that posture if you focus here 

84
00:09:55,040 --> 00:10:03,139
now on establishing a breathing in valiant harmonious and little by little 

85
00:10:03,139 --> 00:10:11,110
get the togo the breath out get longer and longer 

86
00:10:11,110 --> 00:10:18,860
but the flippers no longer have time to think about what you are here and 

87
00:10:18,860 --> 00:10:27,860
now in zaza posture here is in posture nor the body matter in 

88
00:10:27,860 --> 00:10:35,569
posture I remind you you press ice a pressure knee towards the 

89
00:10:35,569 --> 00:10:38,680
soil you push the earth 

90
00:10:38,680 --> 00:10:44,920
and straighten your spine as much as possible to the top of the 

91
00:10:44,920 --> 00:10:49,260
skull as if to push the sky with the head 

92
00:10:49,260 --> 00:10:55,290
the left hand as on the right the horizontal thumbs the cutting edge of the hands 

93
00:10:55,290 --> 00:11:05,020
against the ball well I’m still in saumur facing the wall there’s nothing to do 

94
00:11:05,020 --> 00:11:11,459
specially what I’m going to do is turn my gaze inside 

95
00:11:11,459 --> 00:11:16,330
so the senses i will no longer use them during 

96
00:11:16,330 --> 00:11:22,060
this fact meditation i’m facing the wall so there is nothing to see so it’s 

97
00:11:22,060 --> 00:11:29,890
as if my vision I pose it in front of me on my busy therefore during 

98
00:11:29,890 --> 00:11:38,620
meditation there was the 45 ° view diagonally towards europe you can 

99
00:11:38,620 --> 00:11:43,839
meditate with your eyes closed but if you keep your eyes open or last one 

100
00:11:43,839 --> 00:11:50,100
contact with reality do not go especially in your thoughts 

101
00:11:50,100 --> 00:11:56,110
as are in silence by we also don’t use the water there so 

102
00:11:56,110 --> 00:12:00,880
hearing it doesn’t need a little music and we have already seen that we are 

103
00:12:00,880 --> 00:12:09,040
gémonies so little by little suddenly we cut with the solicitations of the world 

104
00:12:09,040 --> 00:12:15,880
outside we do the meditation shot you sit 

105
00:12:15,880 --> 00:12:21,970
physically it’s the body that is in posture you cut yourself 

106
00:12:21,970 --> 00:12:28,360
solicitations with the stalls you put everything in instant airplane mode don’t tell me 

107
00:12:28,360 --> 00:12:34,340
not meditate with your cellphone next door 

108
00:13:09,540 --> 00:13:14,399
so to complete a tip for beginners it is not easy to 

109
00:13:14,399 --> 00:13:23,519
finding like that confides is now neither di t as armor but it is 

110
00:13:23,519 --> 00:13:28,579
a really good a good help you really i advise you to 

111
00:13:28,579 --> 00:13:33,959
practice trying it out and you’ll see instead of entertaining thoughts 

112
00:13:33,959 --> 00:13:38,790
that they are maybe not ready bright maybe not ready cheerful instead 

113
00:13:38,790 --> 00:13:43,380
to be in a bad mood or the center of attention of stress all this is 

114
00:13:43,380 --> 00:13:52,620
normal here with this this work on the gold the attitude of the mind you let go 

115
00:13:52,620 --> 00:13:57,810
you distance yourself from everything that happens to us to go see that you 

116
00:13:57,810 --> 00:14:03,329
will you calm down physically emotionally breathing all this 

117
00:14:03,329 --> 00:14:08,310
it will help you let go it will deeply help you and your 

118
00:14:08,310 --> 00:14:13,079
entourage if you will was accompanied when did not doubt the 

119
00:14:13,079 --> 00:14:15,529
practice 

