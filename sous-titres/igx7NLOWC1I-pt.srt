1
00:00:00,000 --> 00:00:06,820
Eu mostro um pequeno detalhe da postura zazen,

2
00:00:07,200 --> 00:00:12,720
do que André Lemort, com quem comecei zazen em 1969 com Monsieur Lambert,

3
00:00:13,120 --> 00:00:17,440
explicou-o bem em seu vídeo e eu acho que
que é muito importante,

4
00:00:17,640 --> 00:00:21,540
é a abertura do quadril.

5
00:00:21,860 --> 00:00:26,880
Quando você trouxer a perna de volta - Eu farei o lótus à direita -

6
00:00:27,560 --> 00:00:34,840
então quando você faz a meia-lótus você desliza um pé contra o zafu,

7
00:00:35,180 --> 00:00:40,340
e você coloca o outro contra a virilha.

8
00:00:41,200 --> 00:00:48,600
O importante é soltar o quadril.

9
00:00:49,160 --> 00:00:52,920
Já o estamos fazendo mais ou menos, mas não o suficiente.

10
00:00:53,560 --> 00:00:58,640
Temos que fazer isso direito, a todo vapor!

11
00:00:59,200 --> 00:01:15,080
Basta destravá-lo [o quadril] e você pega o ísquio na mão debaixo das nádegas e se puxa para fora.

12
00:01:15,720 --> 00:01:25,440
para que ela abra o
pélvis e isso muda tudo, em lótus estamos realmente confortáveis, estamos bem encravados.

13
00:01:26,680 --> 00:01:32,840
Depois a outra perna.

14
00:01:35,000 --> 00:01:41,040
A perna direita que está no topo, eu faço a mesma coisa.

15
00:01:41,480 --> 00:01:51,160
Vou me pôr de pé e destravar isto. Eu apenas pego o ischium na minha mão e o puxo para fora,

16
00:01:51,640 --> 00:01:58,960
Eu atiro em uma nádega e na outra. Você puxa esse osso, você puxa a pele para fora,

17
00:01:59,600 --> 00:02:05,600
então você está livre e agora você está realmente se mantendo firme.

18
00:02:05,720 --> 00:02:09,640
Estamos em uma posição muito boa

19
00:02:09,720 --> 00:02:18,320
você se sente como se os pinos estivessem assim [bem posicionados] você está realmente confortável.

20
00:02:18,480 --> 00:02:21,160
Bom zazen para todos!
