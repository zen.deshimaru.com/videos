1
00:00:00,000 --> 00:00:02,200
Many people are interested in meditation … 

2
00:00:02,200 --> 00:00:04,220
… and search for information on Youtube. 

3
00:00:04,420 --> 00:00:07,860
The problem is that we don’t see much of it. Master Deshimaru’s Zen … 

4
00:00:07,980 --> 00:00:10,900
… nor the Kosen Sangha. 

5
00:00:11,100 --> 00:00:13,180
Many talented practitioners … 

6
00:00:13,360 --> 00:00:16,300
… made Kosen Sangha chains. 

7
00:00:16,520 --> 00:00:18,660
But the general public wonders: 

8
00:00:18,940 --> 00:00:21,080
Which is the right one? 

9
00:00:21,300 --> 00:00:23,600
Now, we’re going to chain it all together: 

10
00:00:23,820 --> 00:00:26,840
bit.ly/chosen 

11
00:00:28,420 --> 00:00:31,160
So, if you want to introduce zazen to as many people as possible: 

12
00:00:32,260 --> 00:00:33,980
Subscribe to the channel. 

13
00:00:33,980 --> 00:00:35,220
Watch the videos. 

14
00:00:35,220 --> 00:00:36,520
Please comment. 

15
00:00:36,700 --> 00:00:38,700
And don’t hesitate to like them! 

