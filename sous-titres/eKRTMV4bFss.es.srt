1
00:00:08,480 --> 00:00:13,559
hola a todos te explicare como 

2
00:00:13,559 --> 00:00:20,760
siéntate para practicar la meditación zen llamada zazen el material necesario 

3
00:00:20,760 --> 00:00:27,090
es muy simple un ser humano y una chica de meditación si tienes un 

4
00:00:27,090 --> 00:00:30,869
cojín en lugar de Hamburgo y eso está bien 

5
00:00:30,869 --> 00:00:35,730
y si no, haces uno con una manta donde pasa un saco de dormir 

6
00:00:35,730 --> 00:00:41,060
ejemplo, debe ser lo suficientemente firme 

7
00:00:57,490 --> 00:01:06,070
Entonces, según explica la postura de meditación Zen, la meditación Zen es 

8
00:01:06,070 --> 00:01:13,620
uno sobre todo para saber que es una postura sentada inmóvil y silenciosa 

9
00:01:13,620 --> 00:01:22,630
y la particularidad de esta postura y que la practicamos frente a la pared aquí 

10
00:01:22,630 --> 00:01:26,620
Obviamente me enfrento a la cámara para informarte 

11
00:01:26,620 --> 00:01:32,560
así que lo que tienes que hacer es sentarse juntos amortiguar el que yo 

12
00:01:32,560 --> 00:01:37,479
explíquele que necesita estar suficientemente acolchado lo suficientemente duro 

13
00:01:37,479 --> 00:01:42,520
siéntate bien en la parte inferior de la chica, no te sientes en el borde si tienes 

14
00:01:42,520 --> 00:01:49,330
resbaló y debe cruzar las piernas lo más posible 

15
00:01:49,330 --> 00:01:55,600
cruzarlos como estos como estos como solos y se llama el 2010 si 

16
00:01:55,600 --> 00:02:04,270
pones a tus otras personas aquí se llama lotus lo importante es que 

17
00:02:04,270 --> 00:02:13,480
sus rodillas van al suelo para presionar presionar las rodillas en el piso 

18
00:02:13,480 --> 00:02:19,780
Entonces, ¿cómo hacerlo para hacerlo sin ninguna organización o sin ningún esfuerzo 

19
00:02:19,780 --> 00:02:27,810
buenos músculos físicos en todas las posturas de meditación zen que soltar 

20
00:02:27,810 --> 00:02:37,950
para que tus rodillas bajen, inclinarás ligeramente la pelvis 

21
00:02:37,950 --> 00:02:46,180
adelante la parte superior de la pelvis avanza como 6 a un poco pasado 

22
00:02:46,180 --> 00:02:49,440
te lo muestra en el perfil 

23
00:02:55,200 --> 00:03:02,769
mi pelvis no es cínica como si por ejemplo estuviera conduciendo 

24
00:03:02,769 --> 00:03:07,629
un coche ve allí pero me muestro la última 

25
00:03:07,629 --> 00:03:11,860
porque mi centro de gravedad está detrás de mi cuerpo, quiero 

26
00:03:11,860 --> 00:03:19,420
se ve de alta calidad hola al frente y hago esto en los puentes de dassin, naturalmente, el 

27
00:03:19,420 --> 00:03:24,970
lo que nos gustaría y pasar por delante con gravedad mis rodillas caen al suelo 

28
00:03:24,970 --> 00:03:32,970
esto es lo que debes comenzar estableciendo en la postura un asiento 

29
00:03:32,970 --> 00:03:42,609
en el que la rodilla golpea cuando ha realizado 7 a 6 desde la pelvis 

30
00:03:42,609 --> 00:03:44,920
quien se inclinó ligeramente para hacer la solicitud 

31
00:03:44,920 --> 00:03:51,940
enderezará la columna tanto como sea posible hasta 

32
00:03:51,940 --> 00:03:57,000
la parte superior de la cabeza considera que se consideran nuestros 

33
00:03:57,000 --> 00:04:05,889
Colonia va tan lejos como si fuera a poner un juego no es un gancho que releí 

34
00:04:05,889 --> 00:04:12,280
lo que significa que saqué la nuca, me puse la barbilla, veo que no tengo el 

35
00:04:12,280 --> 00:04:16,349
cabeza girando al frente no tengo la espalda curva 

36
00:04:16,349 --> 00:04:24,610
Soy lo más recto posible te muestro dos perfiles no soy 

37
00:04:24,610 --> 00:04:33,910
como si no estuviera en una subasta antes o lo que sea que sea más 

38
00:04:33,910 --> 00:04:43,659
de cruz también cuando has logrado sentarte derecho no te mueves 

39
00:04:43,659 --> 00:04:53,080
cuanto más continúo con la mano izquierda, los dedos se unen con la mano derecha 

40
00:04:53,080 --> 00:04:58,690
los dedos están bendecidos y voy a superponer los dedos izquierdos en el 

41
00:04:58,690 --> 00:05:08,030
huellas de la mano derecha, pero los empujes se encuentran superpuestos con el dedo de la 

42
00:05:08,030 --> 00:05:14,300
mano izquierda una línea horizontal forma el borde de mis manos 

43
00:05:14,300 --> 00:05:17,840
para que pudiéramos el condado sobre toda la muñeca 

44
00:05:17,840 --> 00:05:23,480
hasta entonces el borde de mis manos mañana lo aplicarás a la base 

45
00:05:23,480 --> 00:05:31,370
del abdomen para hacer esto usó algo que 

46
00:05:31,370 --> 00:05:37,460
puede permitirte detenerte en las manos de terrasson que puedes liberar 

47
00:05:37,460 --> 00:05:43,780
toda la atención desde los hombros la postura de una postura donde tienes que dejar ir 

48
00:05:43,780 --> 00:05:47,990
libera la tensión ves mis codos están libres estoy mucho tiempo 

49
00:05:47,990 --> 00:05:56,090
así o tranquilamente, pero no me esfuerzo por mantener 

50
00:05:56,090 --> 00:06:01,070
mis manos las ideas de esfuerzo para mantener los empujones en el restaurante 

51
00:06:01,070 --> 00:06:08,650
una vez que me doy cuenta de que este es el punto de postura, lo toco 

52
00:06:08,650 --> 00:06:13,120
esta es la postura física 

53
00:06:20,000 --> 00:06:25,460
después de esta primera parte en postura física segunda parte 

54
00:06:25,460 --> 00:06:30,860
la actitud mental una vez que estoy en la postura de 

55
00:06:30,860 --> 00:06:39,220
meditación zen frente a la pared, ¿qué hago? ¿qué debo hacer? 

56
00:06:39,220 --> 00:06:46,190
en el silencio de esta postura irán ustedes otros 

57
00:06:46,190 --> 00:06:53,390
darse cuenta de que cree que ve que sigue pensando y que hay 

58
00:06:53,390 --> 00:06:58,490
Miles de pinceles de pensamiento que vienen que vienen y dicen que vienen a ti 

59
00:06:58,490 --> 00:07:03,010
vamos darse cuenta de que este es un fenómeno natural 

60
00:07:03,010 --> 00:07:08,840
no puedes controlar lo que haremos durante la meditación Zen 

61
00:07:08,840 --> 00:07:16,790
dejaremos pasar nuestros pensamientos, es decir que en esta inmovilidad 

62
00:07:16,790 --> 00:07:23,150
podremos observar que con un rescate observarás tus pensamientos y 

63
00:07:23,150 --> 00:07:29,419
pensamientos en lugar de nutrirlos en lugar de cultivarlos vas 

64
00:07:29,419 --> 00:07:34,610
dejarlos pasar, así que no se trata de negar lo 

65
00:07:34,610 --> 00:07:39,580
déjalo estar allí de nuevo, esta es la máquina que dejarás pasar 

66
00:07:39,580 --> 00:07:46,190
cómo dejar pasar el foco de atención 

67
00:07:46,190 --> 00:07:51,400
aquí ahora en tu aliento 

68
00:07:55,430 --> 00:07:59,960
respirar en zen es respirar abdominal 

69
00:07:59,960 --> 00:08:10,400
inhalas por la nariz y respiras lentamente lentamente 

70
00:08:10,400 --> 00:08:14,270
recreo silencioso meditación silenciosa 

71
00:08:14,270 --> 00:08:20,090
la característica que respira y vas a llamar tu atención 

72
00:08:20,090 --> 00:08:30,930
En la cara de la exaltación inspiramos al llegar tranquilamente en ser 

73
00:08:30,930 --> 00:08:37,620
relleno generoso y enfrentar a la nación cuando 

74
00:08:37,620 --> 00:08:44,070
sales todo el aire lo haces lentamente lentamente como para 

75
00:08:44,070 --> 00:08:47,720
cada vez más largo 

76
00:08:56,540 --> 00:09:03,889
cuando estás al final de tus relaciones, anhelas una y otra vez 

77
00:09:03,889 --> 00:09:12,709
exhalas y ya no estás despacio desnudo más profundo cuanto más tiempo 

78
00:09:12,709 --> 00:09:18,069
nuevo y profundo como las olas en un océano 

79
00:09:18,069 --> 00:09:31,639
porque son la playa y todo esto habla con el v6 en el que te enfocas 

80
00:09:31,639 --> 00:09:36,440
aquí ahora en los puntos de la postura de los puntos físicos 

81
00:09:36,440 --> 00:09:42,259
así que ten cuidado aquí ahora tu cuerpo si 

82
00:09:42,259 --> 00:09:47,660
enfocado aquí ahora al mismo tiempo para no seguir su 

83
00:09:47,660 --> 00:09:55,040
pensamientos volvamos a los puntos que postura si te enfocas aquí 

84
00:09:55,040 --> 00:10:03,139
ahora en establecer una respiración valiente armoniosa y poco a poco 

85
00:10:03,139 --> 00:10:11,110
sacar el togo, exhalar cada vez más 

86
00:10:11,110 --> 00:10:18,860
pero las aletas ya no tienen tiempo para pensar en lo que estás aquí y 

87
00:10:18,860 --> 00:10:27,860
ahora en postura zaza aquí está en postura ni el cuerpo importa en 

88
00:10:27,860 --> 00:10:35,569
postura te recuerdo que presionas hielo una rodilla de presión hacia el 

89
00:10:35,569 --> 00:10:38,680
suelo empujas la tierra 

90
00:10:38,680 --> 00:10:44,920
y endereza la columna lo más posible hacia la parte superior de la 

91
00:10:44,920 --> 00:10:49,260
cráneo como para empujar el cielo con la cabeza 

92
00:10:49,260 --> 00:10:55,290
la mano izquierda como a la derecha los pulgares horizontales el filo de las manos 

93
00:10:55,290 --> 00:11:05,020
contra la pelota bien, todavía estoy en Saumur mirando a la pared, no hay nada que hacer 

94
00:11:05,020 --> 00:11:11,459
especialmente lo que voy a hacer es mirar hacia adentro 

95
00:11:11,459 --> 00:11:16,330
así que los sentidos ya no los usaré durante 

96
00:11:16,330 --> 00:11:22,060
este hecho de meditación estoy frente a la pared, así que no hay nada que ver, así que es 

97
00:11:22,060 --> 00:11:29,890
como si mi visión la planteo frente a mí en mi ocupado por lo tanto durante 

98
00:11:29,890 --> 00:11:38,620
meditación había una vista de 45 ° en diagonal hacia Europa que puedas 

99
00:11:38,620 --> 00:11:43,839
medita con los ojos cerrados pero si mantienes los ojos abiertos o el último 

100
00:11:43,839 --> 00:11:50,100
contacto con la realidad no te vayas especialmente en tus pensamientos 

101
00:11:50,100 --> 00:11:56,110
como están en silencio por nosotros tampoco usamos el agua allí, así 

102
00:11:56,110 --> 00:12:00,880
escucharlo no necesita un poco de música y ya hemos visto que somos 

103
00:12:00,880 --> 00:12:09,040
gémonies tan poco a poco de repente cortamos con las solicitudes del mundo 

104
00:12:09,040 --> 00:12:15,880
afuera hacemos la toma de meditación te sientas 

105
00:12:15,880 --> 00:12:21,970
físicamente es el cuerpo que está en la postura que te cortas 

106
00:12:21,970 --> 00:12:28,360
las solicitudes con los puestos lo pones todo en modo avión instantáneo no me digas 

107
00:12:28,360 --> 00:12:34,340
no medites con tu celular al lado 

108
00:13:09,540 --> 00:13:14,399
así que para completar un consejo para principiantes no es fácil 

109
00:13:14,399 --> 00:13:23,519
encontrar así confía ahora no es ni t t como armadura pero es 

110
00:13:23,519 --> 00:13:28,579
una muy buena una buena ayuda realmente te aconsejo 

111
00:13:28,579 --> 00:13:33,959
practica probarlo y verás en lugar de pensamientos entretenidos 

112
00:13:33,959 --> 00:13:38,790
que tal vez no estén listos brillantes tal vez no estén listos alegres en su lugar 

113
00:13:38,790 --> 00:13:43,380
estar de mal humor o ser el centro de atención del estrés, todo esto es 

114
00:13:43,380 --> 00:13:52,620
normal aquí con esto, este trabajo sobre el oro, la actitud de la mente que dejas ir 

115
00:13:52,620 --> 00:13:57,810
te alejas de todo lo que nos pasa para ir a ver que 

116
00:13:57,810 --> 00:14:03,329
vas a calmarte físicamente emocionalmente respirando todo esto 

117
00:14:03,329 --> 00:14:08,310
te ayudará a dejarlo ir, te ayudará profundamente a ti y a tu 

118
00:14:08,310 --> 00:14:13,079
Entourage si fue acompañado cuando no dudaba de la 

119
00:14:13,079 --> 00:14:15,529
practicar 

