1
00:00:00,080 --> 00:00:00,920
Frage: 

2
00:00:01,140 --> 00:00:05,380
Ich fühle manchmal in der Zazen-Meditation ein sehr angenehmes Gefühl. 

3
00:00:05,680 --> 00:00:09,880
Ein kleines Kribbeln am ganzen Körper. 

4
00:00:10,340 --> 00:00:15,040
Wie kleine Stromschläge. 

5
00:00:15,440 --> 00:00:17,880
Antwort von Meister Kosen: 

6
00:00:18,400 --> 00:00:22,420
Alle Gefühle können gut oder schlecht sein. 

7
00:00:23,080 --> 00:00:28,760
Das gleiche Gefühl kann gut oder schlecht erscheinen. 

8
00:00:29,220 --> 00:00:33,520
Wichtig ist, dass es angenehm ist. 

9
00:00:34,180 --> 00:00:37,240
Möge es dich entspannen. 

10
00:00:37,500 --> 00:00:42,000
Es hängt normalerweise mit der Durchblutung zusammen. 

11
00:00:42,340 --> 00:00:50,640
In der Lotus-Pose gibt es wenig Durchblutung in den Beinen. 

12
00:00:51,020 --> 00:00:55,960
Es zirkuliert viel mehr im Bauchbereich. 

13
00:00:56,400 --> 00:01:00,840
Der Oberkörper, einschließlich des Gehirns, wird besser gespült. 

14
00:01:01,100 --> 00:01:05,240
Es kann ein angenehmes Gefühl verursachen. 

15
00:01:05,500 --> 00:01:09,660
Es ist gut, weil es ein positiver Gedanke ist. 

16
00:01:10,060 --> 00:01:13,580
Wenn du dich gut fühlst, ist es ein positives Bewusstsein. 

17
00:01:13,820 --> 00:01:16,240
Es beeinflusst auch unser Leben. 

18
00:01:16,480 --> 00:01:18,160
Meister Deshimaru sagte immer: 

19
00:01:18,380 --> 00:01:20,500
„Zazen ist keine Demütigung. "" 

20
00:01:20,840 --> 00:01:26,260
Aber wenn Sie sich Ihrem Körper stellen müssen, 

21
00:01:26,620 --> 00:01:31,480
Manchmal sind sie unangenehm oder schmerzhaft. 

22
00:01:31,860 --> 00:01:34,940
Wenn du dich gut fühlst, ist es großartig! 

