1
00:00:00,280 --> 00:00:02,560
Non so molto di sofrologia.

2
00:00:02,800 --> 00:00:12,140
So che si tratta di un’applicazione di alcune pratiche di benessere, di sviluppo personale,

3
00:00:12,540 --> 00:00:17,160
e c’è una sofrologia che si chiama "orientalista",

4
00:00:17,440 --> 00:00:24,080
che si ispira molto alle pratiche yogiche, buddiste o indiane.

5
00:00:24,540 --> 00:00:27,940
Lo Zen è molto semplice.

6
00:00:28,380 --> 00:00:38,400
Si basa su posture umane fondamentali.

7
00:00:39,820 --> 00:00:48,480
Vi mostrerò la prima postura fondamentale dello Zen.

8
00:01:01,500 --> 00:01:06,120
Gli esseri umani erano scimmie.

9
00:01:06,460 --> 00:01:08,580
E’ così che camminavano.

10
00:01:09,200 --> 00:01:20,360
A poco a poco, si sono seduti dritti, come maestri zen.

11
00:01:22,900 --> 00:01:24,900
Poi si sono raddrizzati.

12
00:01:25,180 --> 00:01:28,600
E’ così che camminavano.

13
00:01:29,680 --> 00:01:33,760
E la coscienza umana si è evoluta

14
00:01:35,180 --> 00:01:39,980
insieme alla postura.

15
00:01:40,340 --> 00:01:46,000
La postura del corpo è un pensiero fondamentale.

16
00:01:46,340 --> 00:01:52,200
Per esempio, Usain Bolt, il corridore più veloce del mondo,

17
00:01:52,540 --> 00:01:55,160
ha fatto quel gesto.

18
00:01:55,440 --> 00:02:02,300
Tutti capiscono cosa significa, eppure non ci sono parole.

19
00:02:02,640 --> 00:02:05,440
E’ solo una postura.

20
00:02:05,900 --> 00:02:11,120
Il modo in cui ci sediamo e camminiamo,

21
00:02:11,520 --> 00:02:13,400
cosa si fa con le mani,

22
00:02:13,700 --> 00:02:16,500
è tutto fondamentalmente importante.

23
00:02:18,160 --> 00:02:21,440
In zen, c’è la postura di Buddha.

24
00:02:22,980 --> 00:02:31,120
Esisteva prima del Buddha, è menzionato nella Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
Ti siedi su un cuscino d’erba e pieghi le gambe.

26
00:02:41,560 --> 00:02:47,140
In natura, l’uomo preistorico comunicava

27
00:02:47,440 --> 00:02:53,220
con le orecchie, con il naso, con gli occhi, per vedere lontano.

28
00:02:54,920 --> 00:02:59,000
Così hanno voluto raddrizzarsi.

29
00:02:59,400 --> 00:03:04,220
E mentre si raddrizzavano, la loro coscienza si affinava, si risvegliava.

30
00:03:06,040 --> 00:03:08,720
Questa è la prima postura.

31
00:03:08,980 --> 00:03:11,580
Di solito, dobbiamo fare un loto completo.

32
00:03:12,060 --> 00:03:14,260
È così che si incrociano le gambe.

33
00:03:14,700 --> 00:03:18,460
Si chiama "pretzel".

34
00:03:24,960 --> 00:03:30,980
È molto importante assumere questa postura.

35
00:03:31,380 --> 00:03:37,380
L’oratore precedente ha parlato di dinamismo e di relax.

36
00:03:37,780 --> 00:03:42,340
Abbiamo un sistema nervoso simpatico e parasimpatico.

37
00:03:42,600 --> 00:03:45,800
Abbiamo i muscoli esterni e i muscoli profondi.

38
00:03:46,320 --> 00:03:51,400
Abbiamo la respirazione superficiale e la respirazione profonda.

39
00:03:51,820 --> 00:03:54,620
Abbiamo una coscienza superficiale, corticale,

40
00:03:54,920 --> 00:03:59,760
e la coscienza profonda, il rettile o il mesencefalo.

41
00:04:00,340 --> 00:04:04,160
Per stare in piedi in questa posizione,

42
00:04:04,440 --> 00:04:06,800
devi spingere la terra con le ginocchia.

43
00:04:10,240 --> 00:04:15,640
Ci vuole uno sforzo per essere dinamici e avere una buona postura.

44
00:04:16,000 --> 00:04:22,180
Il mio maestro [Deshimaru] ha insistito sulla bellezza e la forza della postura.

45
00:04:22,460 --> 00:04:31,280
Ma nel loto, più ci si rilassa, più i piedi crescono sulle cosce,

46
00:04:31,520 --> 00:04:35,100
e più le ginocchia crescono sul terreno.

47
00:04:35,620 --> 00:04:40,280
Più ci si rilassa, più si è dinamici, più si è diritti e più forti.

48
00:04:40,560 --> 00:04:46,560
Questo non ha nulla a che vedere con il relax su una sedia o su un letto.

49
00:04:46,940 --> 00:04:57,280
È un modo per abbandonare il corpo a se stesso,

50
00:05:04,380 --> 00:05:09,700
pur essendo ampiamente sveglio.

51
00:05:10,120 --> 00:05:16,240
Questa è la prima postura, la posizione seduta del Buddha.

52
00:05:16,680 --> 00:05:20,280
Anche le mani sono molto importanti.

53
00:05:20,640 --> 00:05:23,600
Fai sempre qualcosa con le mani:

54
00:05:23,860 --> 00:05:26,560
lavorare, proteggersi...

55
00:05:26,960 --> 00:05:28,740
Le mani sono molto espressive

56
00:05:29,040 --> 00:05:31,040
e sono collegati al cervello.

57
00:05:31,500 --> 00:05:41,040
Prendere questo mudra con le mani cambia la coscienza.

58
00:05:41,680 --> 00:05:47,680
La concentrazione è nelle nostre mani, nel corpo, nel sistema nervoso e nel cervello.

59
00:05:48,100 --> 00:05:53,360
Un’altra cosa importante è il pensiero, la coscienza.

60
00:05:53,820 --> 00:05:57,680
Se sei come il Pensatore di Rodin,

61
00:05:58,000 --> 00:06:01,100
non avete affatto la stessa coscienza.

62
00:06:09,960 --> 00:06:12,240
che quando la tua postura è perfettamente dritta,

63
00:06:12,580 --> 00:06:14,640
specialmente con il collo allungato,

64
00:06:15,700 --> 00:06:18,400
con la testa rivolta verso l’alto, aperta verso il cielo,

65
00:06:18,760 --> 00:06:22,020
naso e ombelico allineati

66
00:06:25,060 --> 00:06:27,220
e respiri con tutto il tuo essere.

67
00:06:29,020 --> 00:06:31,860
Il pensiero non è più dissociato dal corpo.

68
00:06:32,240 --> 00:06:36,100
Non c’è più dissociazione tra la nostra coscienza

69
00:06:36,300 --> 00:06:39,080
e ogni cellula del nostro corpo.

70
00:06:39,500 --> 00:06:45,520
Nello Zen lo chiamiamo "mente-corpo", in una parola.

71
00:06:46,360 --> 00:06:50,540
Questa è la postura fondamentale: zazen.

72
00:06:56,380 --> 00:07:00,200
La seconda postura è quella del camminare.

73
00:07:00,680 --> 00:07:06,340
In zazen abbandoniamo tutto e non ci muoviamo affatto.

74
00:07:06,560 --> 00:07:08,280
Quindi, la coscienza è speciale.

75
00:07:08,720 --> 00:07:13,800
Ma c’è un’altra meditazione che si fa camminando.

76
00:07:16,180 --> 00:07:18,160
Dobbiamo muoverci.

77
00:07:18,540 --> 00:07:20,540
Facciamo qualcosa, andiamo avanti.

78
00:07:21,180 --> 00:07:24,180
Non si va avanti per prendere qualcosa.

79
00:07:24,580 --> 00:07:28,380
Il respiro si accelera con la camminata.

80
00:07:29,020 --> 00:07:30,740
Il coreografo Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
che era un discepolo del mio maestro, Deshimaru,

82
00:07:34,060 --> 00:07:38,040
ha fatto sì che i suoi ballerini lo praticassero ogni giorno.

83
00:07:39,060 --> 00:07:45,340
La seconda postura è la camminata "kin-hin".

84
00:07:45,880 --> 00:07:51,040
Notate cosa fate con le mani.

85
00:07:51,680 --> 00:07:57,840
Un pugile che si fa fotografare lo fa con le mani.

86
00:07:58,240 --> 00:08:05,220
Quando si mettono le mani così, automaticamente il cervello esprime una forte aggressività.

87
00:08:05,820 --> 00:08:10,360
Potete mettere le mani in tasca.

88
00:08:10,840 --> 00:08:13,140
"Dove ho messo il portafoglio? »

89
00:08:13,640 --> 00:08:16,360
"Qualcuno mi ha rubato il cellulare! »

90
00:08:16,700 --> 00:08:19,580
Possiamo ficcarci le dita nel naso.

91
00:08:21,660 --> 00:08:25,820
Tutto ciò che è stato costruito qui è stato costruito a mano.

92
00:08:26,260 --> 00:08:28,820
E’ straordinaria, la mano.

93
00:08:29,160 --> 00:08:31,860
Siamo gli unici animali con le mani,

94
00:08:32,260 --> 00:08:35,340
sono legate alla nostra evoluzione.

95
00:08:37,700 --> 00:08:40,660
Anche le mani meditano.

96
00:08:40,940 --> 00:08:43,580
Metti il pollice nel pugno.

97
00:08:43,820 --> 00:08:49,420
Mettete la radice del pollice sotto lo sterno, metteteci l’altra mano sopra.

98
00:08:49,760 --> 00:08:55,060
Questi dettagli posturali esistono fin dai tempi del Buddha.

99
00:08:55,380 --> 00:09:02,520
Testi antichi spiegano che sono stati trasmessi in Cina dall’India.

100
00:09:02,960 --> 00:09:08,920
E’ una trasmissione orale da persona a persona.

101
00:09:09,480 --> 00:09:12,500
Nel Buddismo ci sono molti aspetti.

102
00:09:12,780 --> 00:09:17,320
Ma l’aspetto meditativo e posturale è molto importante.

103
00:09:17,820 --> 00:09:28,540
C’erano due grandi discepoli del Buddha, Śāriputra e Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Il loro maestro spirituale non era sicuro:

105
00:09:37,220 --> 00:09:43,680
"Non sono sicuro di avere il trucco per uscire dalla vita e dalla morte. »

106
00:09:43,940 --> 00:09:52,860
"Se trovate un maestro che insegna questo, fatemelo sapere e io verrò con voi. »

107
00:09:53,620 --> 00:10:04,320
Un giorno vedono un monaco, come me, che cammina per la strada.

108
00:10:04,780 --> 00:10:10,000
Sono stupiti dalla bellezza di quest’uomo.

109
00:10:10,420 --> 00:10:16,200
Poi questo monaco si ferma in un boschetto per fare pipì.

110
00:10:24,440 --> 00:10:33,240
Gli dice Śāriputra, considerata una delle più grandi intelligenze dell’India dell’epoca:

111
00:10:34,180 --> 00:10:43,940
"Siamo stupiti dalla tua presenza, chi è il tuo padrone? »

112
00:10:46,560 --> 00:10:49,540
- Il mio maestro è Shakyamuni Buddha".

113
00:10:49,860 --> 00:10:53,520
- Spiegaci la tua dottrina! »

114
00:10:53,780 --> 00:10:58,220
- "Non posso spiegartelo, sono troppo nuovo in questo campo. »

115
00:10:59,100 --> 00:11:04,440
E hanno seguito questo ragazzo solo per il modo in cui cammina.

116
00:11:04,800 --> 00:11:06,980
Il modo in cui sta ottenendo qualcosa da lei.

117
00:11:07,440 --> 00:11:11,680
Quindi la seconda postura è camminare.

118
00:11:12,020 --> 00:11:15,280
Quando la gente fa zazen, è molto forte.

119
00:11:15,500 --> 00:11:21,580
Anche i praticanti di arti marziali sono impressionati dalla potenza che questo emana.

120
00:11:21,940 --> 00:11:25,860
Con il potere del respiro.

121
00:11:26,240 --> 00:11:35,520
Ma come trovare questa energia, questa pienezza, nella vita di tutti i giorni?

122
00:11:35,840 --> 00:11:39,220
Quella calma, quella concentrazione.

123
00:11:39,600 --> 00:11:42,960
È un punto difficile.

124
00:11:43,280 --> 00:11:47,140
Perché la postura di zazen è estremamente precisa.

125
00:11:47,600 --> 00:11:55,100
All’inizio ci concentriamo sull’aspetto tecnico della camminata kin-hin.

126
00:11:55,540 --> 00:11:58,800
Ma una volta che abbiamo tutti i dettagli,

127
00:11:59,300 --> 00:12:02,160
in qualsiasi momento della nostra vita quotidiana

128
00:12:02,440 --> 00:12:08,400
(anche se mi dimentico sempre di farlo), possiamo praticarlo.

129
00:12:10,600 --> 00:12:16,740
Potete rifocalizzarvi sul vostro essere spirituale.

130
00:12:17,740 --> 00:12:22,760
Sul tuo essere che non fa un cazzo.

131
00:12:23,320 --> 00:12:31,340
Ti ho parlato delle mani.

132
00:12:31,840 --> 00:12:39,980
La postura religiosa fondamentale è questa.

133
00:12:40,560 --> 00:12:44,240
Quando si hanno le mani in mano in quel modo,

134
00:12:45,080 --> 00:12:48,260
solo facendo questo,

135
00:12:48,580 --> 00:12:51,280
ha un effetto sul cervello,

136
00:12:51,620 --> 00:12:55,340
costruisce un ponte tra i due emisferi cerebrali.

137
00:12:55,740 --> 00:12:59,960
Questa postura è usata in tutte le religioni.

138
00:13:00,280 --> 00:13:02,180
È abbastanza semplice.

139
00:13:02,560 --> 00:13:05,640
Vedi l’importanza delle mani!

140
00:13:06,060 --> 00:13:13,280
La terza postura...

141
00:13:13,860 --> 00:13:16,520
(Pensavo fossero quattro...)

142
00:13:16,880 --> 00:13:20,580
Oh, sì, c’è la postura inclinata, ma non ne parleremo.

143
00:13:21,000 --> 00:13:25,520
La postura sdraiata, come dormire.

144
00:13:26,700 --> 00:13:31,980
Vi ho spiegato come gli uomini preistorici si aggiravano a quattro zampe,

145
00:13:32,260 --> 00:13:34,440
Poco a poco a poco, si sono raddrizzati,

146
00:13:34,700 --> 00:13:38,760
e che la nostra evoluzione deriva dalla nostra postura,

147
00:13:39,060 --> 00:13:42,580
dell’irrigazione della colonna vertebrale che arriva fino al cervello,

148
00:13:42,940 --> 00:13:46,400
e il cervello, che è uno strumento straordinario,

149
00:13:46,700 --> 00:13:51,440
di cui si conosce solo il 10-15% della sua utilità totale.

150
00:13:51,760 --> 00:13:57,740
Abbiamo un grosso problema tra le orecchie che stiamo sottoutilizzando.

151
00:14:08,640 --> 00:14:12,380
Ognuno ha il suo cervello!

152
00:14:19,020 --> 00:14:22,140
Ognuno ha il suo cervello!

153
00:14:22,400 --> 00:14:25,200
Come un indirizzo IP.

154
00:14:30,220 --> 00:14:33,220
Ognuno ha il suo cervello!

155
00:14:33,940 --> 00:14:35,900
E siamo soli!

156
00:14:36,700 --> 00:14:41,940
È molto importante per quello che sto per dirvi.

157
00:14:44,760 --> 00:14:50,040
I nostri piedi (torniamo ai nostri piedi...).

158
00:14:51,720 --> 00:14:56,420
Sai che viviamo su una piccola Terra.

159
00:14:57,040 --> 00:15:01,420
Hai visto il film Gravity?

160
00:15:02,120 --> 00:15:07,060
È uno dei film che risvegliano veramente la coscienza umana.

161
00:15:07,560 --> 00:15:13,380
A Hollywood, a volte fanno qualche lavoro di manipolazione.

162
00:15:13,680 --> 00:15:17,220
Ma anche, a volte, di informazione e di evoluzione spirituale.

163
00:15:17,760 --> 00:15:19,660
Questo film è uno di questi.

164
00:15:20,040 --> 00:15:26,920
E potete vedere voi stessi quanto sia fragile la nostra Terra.

165
00:15:29,200 --> 00:15:33,400
Di solito, quando guardiamo il cielo, andiamo su tutte le furie: "È infinitamente blu! ».

166
00:15:33,760 --> 00:15:38,940
No. Non per sempre. 30 chilometri di atmosfera.

167
00:15:39,360 --> 00:15:44,660
La fine dell’atmosfera è tra i 30 e i 1.000 chilometri.

168
00:15:52,260 --> 00:15:56,700
Anche mille miglia non sono niente.

169
00:15:58,800 --> 00:16:04,780
Se ti mettessi a 20 miglia di distanza, non sopravviveresti un minuto.

170
00:16:05,380 --> 00:16:15,940
Anche a parte le tasse, le nostre condizioni di vita sono precarie.

171
00:16:17,420 --> 00:16:20,440
La Terra è un piccolo, fragile, essere vivente.

172
00:16:21,120 --> 00:16:30,660
La nostra biosfera è minuscola e fragile, deve essere preservata.

173
00:16:30,940 --> 00:16:35,100
Dovremmo sempre preoccuparci di preservare questa cosa.

174
00:16:36,860 --> 00:16:39,940
Perché siamo nati dalla Terra.

175
00:16:40,220 --> 00:16:45,700
Questo miracolo della vita che ha avuto luogo sulla Terra è effimero.

176
00:16:46,240 --> 00:16:47,840
Siamo nati dalla Terra.

177
00:16:48,220 --> 00:16:53,320
E tutti noi abbiamo i piedi puntati verso lo stesso centro, il centro della Terra.

178
00:16:53,900 --> 00:16:57,760
Così i nostri piedi sono in comunità.

179
00:17:01,860 --> 00:17:08,680
Ma la nostra testa è unica, nessuno punta nella stessa direzione.

180
00:17:18,400 --> 00:17:23,500
Da qui la terza postura che vi mostrerò.

181
00:17:23,800 --> 00:17:28,140
È comune a molte religioni, si chiama "prostrazione".

182
00:17:28,380 --> 00:17:31,240
È così che facciamo.

183
00:17:40,640 --> 00:17:42,780
Ah, che bella sensazione!

184
00:17:43,060 --> 00:17:49,940
Quando facciamo questa pratica, è come quando facciamo l’amore:

185
00:17:50,260 --> 00:17:58,460
a volte non facciamo sesso per 10 anni, 20 anni, 6 mesi...

186
00:17:58,780 --> 00:18:07,460
e quando facciamo di nuovo l’amore, diciamo: "Ah, che bello, mi ero dimenticato di quanto fosse bello! ».

187
00:18:08,220 --> 00:18:12,200
Quando si fa quella prostrazione, è la stessa cosa.

188
00:18:13,460 --> 00:18:15,740
Non sto scherzando.

189
00:18:16,240 --> 00:18:20,260
Stiamo riformando i nostri cervelli.

190
00:18:21,060 --> 00:18:26,280
Quando si mette il cervello in linea con il centro della Terra.

191
00:18:26,620 --> 00:18:29,080
Non si tratta solo di umiltà,

192
00:18:29,360 --> 00:18:32,720
o inchinarsi davanti a statue o altro.

193
00:18:34,600 --> 00:18:38,880
E’ una cosa magica, fondamentale, sciamanica, forse...

194
00:18:39,320 --> 00:18:41,860
Dal momento in cui ti prostri...

195
00:18:42,180 --> 00:18:43,340
Il mio maestro diceva sempre:

196
00:18:43,660 --> 00:18:47,440
"Se i capi di stato si prostrassero una volta al giorno,"

197
00:18:47,800 --> 00:18:50,200
"cambierebbe il mondo". »

198
00:18:50,640 --> 00:18:54,560
Non si tratta di jibber-jabber o misticismo.

199
00:18:55,280 --> 00:18:58,780
Deve essere scientificamente misurabile.

200
00:19:00,780 --> 00:19:05,280
Riformatta il cervello, per atterrare.

201
00:19:05,740 --> 00:19:09,280
È costantemente nella sua linea individuale.

202
00:19:09,620 --> 00:19:17,300
E lì ti connetti alla tua radice, ai tuoi fratelli, al centro della Terra.

203
00:19:19,940 --> 00:19:25,380
A volte lo faccio e sento qualcosa di forte nella mia testa.

204
00:19:25,660 --> 00:19:27,540
Ci si sente così bene.

205
00:19:27,820 --> 00:19:39,420
Allo stesso tempo, relativizza il male che facciamo involontariamente intorno a noi.

206
00:19:40,560 --> 00:19:42,960
E’ una buona medicina.

207
00:19:43,180 --> 00:19:47,040
Tutte queste posture sono medicine fondamentali.

208
00:19:47,760 --> 00:19:53,260
Avevo preparato un testo un po’ intellettuale.

209
00:19:53,520 --> 00:19:58,140
Ma non riesco a passare da un soggetto all’altro.

210
00:19:58,440 --> 00:20:02,300
Cosa stavo dicendo?

211
00:20:06,860 --> 00:20:12,040
Ritorno al corpo e alla preistoria.

212
00:20:12,780 --> 00:20:28,200
È stato dimostrato che gli uomini di Neanderthal avevano una pratica religiosa.

213
00:20:30,680 --> 00:20:36,340
Abbiamo trovato delle tombe.

214
00:20:36,860 --> 00:20:40,440
Hanno portato offerte ai loro morti.

215
00:20:40,760 --> 00:20:43,680
Essi celebravano una cerimonia religiosa per i loro defunti.

216
00:20:48,420 --> 00:20:52,180
La cosa interessante non è solo questo!

217
00:20:52,720 --> 00:20:56,260
Quando facevano queste cerimonie religiose -

218
00:20:56,740 --> 00:21:01,700
- religiosi nel senso che comunicavano con l’invisibile -

219
00:21:02,540 --> 00:21:06,940
- con la vita eterna, con i morti -

220
00:21:08,800 --> 00:21:11,020
- si collegavano all’invisibile.

221
00:21:11,420 --> 00:21:18,140
All’epoca, però, il linguaggio articolato non esisteva ancora.

222
00:21:18,560 --> 00:21:24,380
Quindi, la religione esisteva prima del linguaggio articolato.

223
00:21:24,840 --> 00:21:30,140
(Sono disposto a passare al linguaggio articolato...)

224
00:21:31,480 --> 00:21:36,500
(È molto interessante, mi ci sono voluti quattro giorni per scriverlo...)

225
00:21:43,820 --> 00:21:45,660
Il mio maestro diceva sempre:

226
00:21:45,920 --> 00:21:48,880
"Zazen è la religione prima della religione. »

227
00:21:49,380 --> 00:21:54,380
Diremmo: "Si’, vuoi sempre essere il primo..."

228
00:21:55,860 --> 00:21:59,700
Ma questa è la religione prima della religione come la conosciamo noi.

229
00:21:59,960 --> 00:22:02,720
Dove si fa riferimento ai libri.

230
00:22:03,080 --> 00:22:06,280
I libri vengono dopo la religione, non prima.

231
00:22:06,720 --> 00:22:11,940
Prima, l’uomo non riusciva nemmeno a parlare.

232
00:22:12,360 --> 00:22:15,060
Quindi era già in quella posizione.

233
00:22:15,480 --> 00:22:19,620
Queste sono posture preistoriche.

234
00:22:21,460 --> 00:22:23,740
(Quanto tempo mi rimane?)

235
00:22:23,960 --> 00:22:28,320
(Mi restano solo 3 minuti? Va bene, allora!)

236
00:22:31,020 --> 00:22:34,460
Allora, a proposito di sofrologia...

237
00:22:36,500 --> 00:22:39,280
Volevo parlarvi del tradizionale zazen.

238
00:22:39,740 --> 00:22:43,660
Sono vestito come un monaco tradizionale.

239
00:22:47,440 --> 00:22:51,700
Lo zazen tradizionale non si pratica da solo.

240
00:22:52,100 --> 00:22:54,500
Non è una tecnica di benessere.

241
00:22:58,260 --> 00:23:05,060
Vi leggerò quello che ha detto il Buddha, penso che sia fantastico!

242
00:23:08,520 --> 00:23:12,780
Shakyamuni Buddha disse al suo pubblico:

243
00:23:13,120 --> 00:23:18,540
"Lo studio dei sutra e degli insegnamenti" - testi -

244
00:23:18,900 --> 00:23:28,260
L’osservanza dei precetti" - nelle religioni questo è importante, non uccidere, non rubare, eccetera - è una parte molto importante dell’"osservanza dei precetti". -

245
00:23:29,300 --> 00:23:34,960
"e anche la pratica dello zazen, la meditazione seduta."

246
00:23:35,900 --> 00:23:43,060
"sono incapaci di spegnere i desideri, le paure e le ansie umane. »

247
00:23:43,340 --> 00:23:45,080
Questo è ciò che ha detto Shakyamuni Buddha.

248
00:23:45,340 --> 00:23:51,660
Quindi io sono un fanatico, e lui è un rompipalle...

249
00:23:52,080 --> 00:23:56,800
Zazen, anche il Buddha dice che è inutile!

250
00:23:57,600 --> 00:24:01,680
Allora, cosa faremo al riguardo?

251
00:24:14,440 --> 00:24:17,480
Certo, c’è una suite...

252
00:24:23,360 --> 00:24:29,360
Il mio maestro [Deshimaru], quando incontrò il suo maestro [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
Quest’ultimo ha detto in una conferenza "Zazen è inutile, non porta alcun merito! ».

254
00:24:34,680 --> 00:24:38,800
Il mio maestro ha pensato: "Ma è davvero interessante se è inutile! »

255
00:24:39,140 --> 00:24:41,940
"Facciamo sempre cose che servono a qualcosa! »

256
00:24:42,280 --> 00:24:43,760
"Qualcosa di inutile? »

257
00:24:44,120 --> 00:24:47,400
"Voglio andare, voglio farlo! »
