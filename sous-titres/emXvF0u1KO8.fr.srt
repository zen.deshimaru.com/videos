1
00:00:13,360 --> 00:00:18,820
Un poème du Shodoka, le poème de ce matin, de Maître Yoka Daishi :

2
00:00:19,740 --> 00:00:29,120
"Si quelqu’un me demande à quelle religion j’appartiens, je réponds la puissance de Maka Hannya"

3
00:00:30,800 --> 00:00:35,200
Je lis un extrait d’un commentaire de Maître Deshimaru "Maka Hannya, ça signifie la plus haute sagesse,

4
00:00:35,640 --> 00:00:39,160
la plus profonde, l’universelle.

5
00:00:39,740 --> 00:00:42,000
Quel est son pouvoir, quelle sa puissance?

6
00:00:42,340 --> 00:00:45,900
Chacun possède un pouvoir mais dans

7
00:00:46,140 --> 00:00:51,300
L’Hannya Shingyo, dans le sutra de la grande sagesse, le Bodhisattva Avalokiteshvara

8
00:00:51,820 --> 00:00:55,540
possède le pouvoir suprême,
celui de Maka Hannya.

9
00:00:56,560 --> 00:01:02,600
Il a compris que son corps et toutes choses sont ku, sans noumène sans substance propre,

10
00:01:03,420 --> 00:01:07,980
il peut aider tous les êtres
humains et les libérer de leur anxiété."

11
00:01:08,300 --> 00:01:14,440
Le début du sutra de l’Hannya Shingyo est en lien avec ce poème. Je vais aussi lire

12
00:01:15,340 --> 00:01:19,260
un extrait d’un d’enseignement de Maître 
Kosen. Il enseigne" mon Maître disait

13
00:01:19,780 --> 00:01:25,460
toujours, quelle que soit la situation,
difficile ou facile, le mieux pour l’être humain

14
00:01:26,440 --> 00:01:30,920
est de continuer, de persévérer à
se concentrer sur la pratique de zazen,

15
00:01:31,380 --> 00:01:37,960
développer sa nature de Bouddha." Maître Kosen dit "c’est plus important que tout.

16
00:01:39,200 --> 00:01:43,900
Toujours s’il y a une crise, une
révolution, une guerre, c’est toujours

17
00:01:44,780 --> 00:01:49,020
possible de continuer la pratique, de
respirer, de se connecter avec le cosmos

18
00:01:49,620 --> 00:01:55,480
avec la force, avec la sagesse
fondamentale. On dit que le lotus qui est

19
00:01:56,100 --> 00:02:00,740
né du feu ne sera jamais détruit."
