1
00:00:11,160 --> 00:00:13,720
Bonjour, comment allez-vous ?

2
00:00:16,560 --> 00:00:21,280
La posture, enseignée par Maître Deshimaru…

3
00:00:25,000 --> 00:00:28,460
Il l’appelait la méthode zen…

4
00:00:31,920 --> 00:00:35,640
Commence par avoir une base stable.

5
00:00:39,600 --> 00:00:43,160
Ce qui veut dire que les deux genoux touchent le sol.

6
00:00:47,480 --> 00:00:50,540
Et vous placez le périnée sur le zafu [coussin].

7
00:00:53,860 --> 00:00:58,940
Si vous faites le lotus complet, vous placez d’abord une jambe.

8
00:01:04,220 --> 00:01:07,300
Puis l’autre.

9
00:01:07,900 --> 00:01:12,140
Si vous avez une bonne stabilité, vous pouvez vraiment lâcher le corps.

10
00:01:12,170 --> 00:01:15,220
Comme on dit : lâchez le corps-esprit.

11
00:01:22,460 --> 00:01:24,320
Maître Deshimaru disait :

12
00:01:25,020 --> 00:01:28,640
« Le zen est le nouvel humanisme pour le 21e siècle. »

13
00:01:29,560 --> 00:01:31,440
« Il doit toujours être frais. »

14
00:01:31,480 --> 00:01:34,000
« Si vous pratiquez le véritable zazen, »

15
00:01:34,060 --> 00:01:36,600
« menton rentré, »

16
00:01:37,060 --> 00:01:39,980
« La colonne vertébrale étirée, »

17
00:01:40,580 --> 00:01:42,580
« seulement zazen, »

18
00:01:43,620 --> 00:01:46,270
« le reste n’a plus tellement d’importance. »

19
00:01:46,320 --> 00:01:49,600
Au début du zazen, il y a trois coups de cloche.

20
00:01:52,040 --> 00:01:56,060
Et après, quand il y a kin-hin, la méditation en marche,

21
00:02:00,530 --> 00:02:03,580
Il y a deux coups de cloche.

22
00:02:15,980 --> 00:02:18,940
Étirez bien le genou de la jambe avant.

23
00:02:22,900 --> 00:02:28,340
Pressez le sol avec la racine du gros orteil du pied avant.

24
00:02:34,580 --> 00:02:36,580
Relâchez bien les épaules.

25
00:02:38,260 --> 00:02:41,480
À la fin de l’expiration,

26
00:02:44,100 --> 00:02:46,100
on inspire,

27
00:02:47,560 --> 00:02:51,900
et on fait un petit pas en avant.

28
00:02:56,820 --> 00:03:00,060
On recommence, on expire…

29
00:03:01,360 --> 00:03:05,180
On met tout le poids du corps sur la jambe avant.

30
00:03:10,920 --> 00:03:13,760
Le pouce de la main gauche

31
00:03:16,120 --> 00:03:19,960
on l’enserre dans le poing.

32
00:03:22,400 --> 00:03:25,300
Puis on prend la main droite

33
00:03:26,960 --> 00:03:29,480
et on couvre la main gauche.

34
00:03:36,140 --> 00:03:40,040
Faire zazen par zoom est très pratique…

35
00:03:40,260 --> 00:03:43,710
surtout en ces temps difficiles.

36
00:03:48,280 --> 00:03:53,440
Mais normalement, ça complète la pratique dans un vrai dojo.

37
00:03:58,860 --> 00:04:03,340
Évidemment, on dédie cette pratique qu’on fait ensemble

38
00:04:12,540 --> 00:04:19,560
À tous les gens qui se dévouent pour soigner et sauver d’autres personnes.

39
00:04:34,720 --> 00:04:36,720
Salut Toshi. Bonjour.

40
00:04:36,780 --> 00:04:38,720
Bonjour, maître.

41
00:04:40,680 --> 00:04:43,940
Ah, voilà, ça y est ! Bonjour !

42
00:04:46,100 --> 00:04:48,100
Bonjour, Ingrid.

43
00:04:51,840 --> 00:04:54,640
Là , c’est Grabriela, mais laquelle ?

44
00:04:57,460 --> 00:05:00,520
Ah, je ne t’avais pas reconnue avec tes lunettes !

45
00:05:06,020 --> 00:05:09,160
C’est moi qui traduis, tu ne me reconnais pas ?
