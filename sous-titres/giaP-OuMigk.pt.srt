1
00:00:00,080 --> 00:00:00,920
Pergunta: 

2
00:00:01,140 --> 00:00:05,380
Às vezes, sinto na meditação zazen uma sensação muito agradável. 

3
00:00:05,680 --> 00:00:09,880
Um pouco de formigamento por todo o corpo. 

4
00:00:10,340 --> 00:00:15,040
Como pequenos choques elétricos. 

5
00:00:15,440 --> 00:00:17,880
Resposta do Master Kosen: 

6
00:00:18,400 --> 00:00:22,420
Todos os sentimentos podem ser bons ou ruins. 

7
00:00:23,080 --> 00:00:28,760
O mesmo sentimento pode parecer bom ou ruim. 

8
00:00:29,220 --> 00:00:33,520
O importante é que seja agradável. 

9
00:00:34,180 --> 00:00:37,240
Que você relaxe. 

10
00:00:37,500 --> 00:00:42,000
Geralmente está relacionado à circulação do sangue. 

11
00:00:42,340 --> 00:00:50,640
Na posição de lótus, há pouca circulação sanguínea nas pernas. 

12
00:00:51,020 --> 00:00:55,960
Circula muito mais na parte abdominal. 

13
00:00:56,400 --> 00:01:00,840
A parte superior do corpo, incluindo o cérebro, é melhor irrigada. 

14
00:01:01,100 --> 00:01:05,240
Pode causar uma sensação agradável. 

15
00:01:05,500 --> 00:01:09,660
É bom, porque é um pensamento positivo. 

16
00:01:10,060 --> 00:01:13,580
Quando você se sente bem, é uma consciência positiva. 

17
00:01:13,820 --> 00:01:16,240
Também influencia nossas vidas. 

18
00:01:16,480 --> 00:01:18,160
Mestre Deshimaru sempre dizia: 

19
00:01:18,380 --> 00:01:20,500
“Zazen não é uma mortificação. " 

20
00:01:20,840 --> 00:01:26,260
Mas quando você tem que enfrentar seu corpo, 

21
00:01:26,620 --> 00:01:31,480
algumas vezes são desagradáveis ​​ou dolorosas. 

22
00:01:31,860 --> 00:01:34,940
Quando você se sente bem, é ótimo! 

