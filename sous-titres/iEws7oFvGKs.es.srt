1
00:00:06,180 --> 00:00:11,550
He estado practicando zazen durante más de la mitad de mi vida.

2
00:00:14,890 --> 00:00:20,170
Y es muy raro que esté en perfecto equilibrio.

3
00:00:25,650 --> 00:00:29,445
Así que hay muy pocas posibilidades

4
00:00:29,605 --> 00:00:33,910
que me doy cuenta totalmente del Buda?

5
00:00:38,110 --> 00:00:40,910
¡El Buda también tenía la espalda mal!

6
00:00:47,340 --> 00:00:54,790
Estamos tratando de armonizar con algo perfecto y vivo.

7
00:00:55,110 --> 00:00:59,740
Lo que es perfecto en nosotros es que estamos vivos.

8
00:01:01,860 --> 00:01:06,660
Hay gente que está pensando en hacer
de la modificación genética.

9
00:01:13,480 --> 00:01:22,120
Se dice en las fábulas que el Buda tenía
las treinta y dos marcas de perfección.

10
00:01:23,400 --> 00:01:27,440
Es decir, no tenía ningún defecto.

11
00:01:41,780 --> 00:01:48,180
Es lo suficientemente bueno para acercarse,
para imitar a su dios.

12
00:01:51,530 --> 00:01:55,780
Pero creo que el Buda era un hombre como nosotros.
y que tenía defectos.

13
00:02:02,690 --> 00:02:05,685
El primer defecto es que está muerto.

14
00:02:05,685 --> 00:02:08,710
Si no hubiera tenido un defecto, no habría muerto.

15
00:02:11,630 --> 00:02:15,510
Murió, hizo llorar a todos sus discípulos.

16
00:02:18,733 --> 00:02:21,406
Estamos haciendo zazen con un cuerpo imperfecto.

17
00:02:21,686 --> 00:02:24,100
Como un ser imperfecto.

18
00:02:26,130 --> 00:02:30,650
Pero lo que es suficientemente grande es darnos la dirección

19
00:02:32,670 --> 00:02:39,020
celular, muscular, mental.

20
00:02:39,670 --> 00:02:43,320
La dirección para acercarse o imitar.

21
00:02:48,450 --> 00:02:51,840
Los musulmanes, por ejemplo, no llevan Buda.

22
00:02:51,910 --> 00:02:55,900
Dicen: "No tenemos una estatua,
ninguna imagen de Dios".

23
00:02:57,440 --> 00:02:59,165
Creo que es bastante bueno.

24
00:03:02,695 --> 00:03:07,520
Pero sabemos la dirección para imitar a Dios.

25
00:03:11,630 --> 00:03:18,510
Hacer todo lo que podamos para hacer nuestro ser divino.

26
00:03:22,120 --> 00:03:24,730
Pero no somos perfectos
y vamos a morir.

27
00:03:27,000 --> 00:03:29,375
Y hay algunos que no son perfectos…

28
00:03:29,375 --> 00:03:31,932
y otros que NO son PERFECTOS.

29
00:03:34,552 --> 00:03:39,830
A menudo, la gente perfecta no tiene que…

30
00:03:40,390 --> 00:03:43,060
Yo, cuando hacía música…

31
00:03:46,120 --> 00:03:49,685
A menudo cuento esta historia.

32
00:03:50,445 --> 00:03:53,590
…tenía un productor.

33
00:03:54,070 --> 00:03:58,640
Era un multimillonario italiano…

34
00:04:00,860 --> 00:04:04,415
No importa lo que estaba haciendo.

35
00:04:05,265 --> 00:04:10,450
Era muy rico y quería abrir una editorial.

36
00:04:13,480 --> 00:04:17,123
Se ofreció a producir mi disco.

37
00:04:19,480 --> 00:04:23,820
Y no sé por qué, él vino
en el Templo de la Gendronnière.

38
00:04:24,780 --> 00:04:28,520
Durante los días de preparación para el campamento de verano.

39
00:04:28,750 --> 00:04:33,050
Dijo: "¡Quería ver lo que estás haciendo!"

40
00:04:33,520 --> 00:04:36,215
Le mostramos las reglas del dojo:

41
00:04:36,425 --> 00:04:39,417
"Entras así, y luego te sientas."

42
00:04:39,877 --> 00:04:42,420
Se sienta: todo el loto directo.

43
00:04:46,690 --> 00:04:48,450
Impecable loto.

44
00:04:48,570 --> 00:04:51,190
"¡Bueno, veo que lo tienes todo resuelto!"

45
00:04:51,190 --> 00:04:54,080
"Zazen" estará en una hora."

46
00:04:55,870 --> 00:04:58,580
"Puedes visitar el templo."

47
00:04:58,760 --> 00:05:02,870
Hizo su zazen durante una hora.

48
00:05:04,300 --> 00:05:06,060
- "¿No fue difícil?"

49
00:05:06,190 --> 00:05:08,720
- "No, no, fue muy bonito!"

50
00:05:11,590 --> 00:05:14,655
¡Juro que es verdad!

51
00:05:15,314 --> 00:05:18,080
Y nunca ha vuelto a hacer zazen en su vida.

52
00:05:19,420 --> 00:05:21,920
Él es el que me enseñó una lección.

53
00:05:27,150 --> 00:05:32,332
Hace dos o tres años, mi nieto, de esta altura…

54
00:05:32,632 --> 00:05:37,460
me dijo: "¡Abuelo, muéstrame el zazen!"

55
00:05:40,840 --> 00:05:45,540
Lo traje a la habitación
donde hago zazen online a través de Zoom.

56
00:05:47,140 --> 00:05:50,780
Le dije: "Siéntate aquí, pon tus piernas así."

57
00:05:51,000 --> 00:05:52,019
Está estresado.

58
00:05:52,115 --> 00:05:55,950
Se parece a Stephen Zeisler [discípulo de Deshimaru].

59
00:05:58,050 --> 00:06:00,410
- "Te pones así."

60
00:06:00,570 --> 00:06:04,130
Estoy corrigiendo su postura.

61
00:06:04,400 --> 00:06:06,485
Tiene una gran postura.

62
00:06:06,575 --> 00:06:08,750
Dije: "¡Eso es, eso es!"

63
00:06:08,910 --> 00:06:10,250
- "¡Gracias, abuelo!"

64
00:06:15,755 --> 00:06:17,270
Lo tiene.

65
00:06:17,350 --> 00:06:19,645
Sabe lo que es el zazen.

66
00:06:22,430 --> 00:06:25,660
No sé qué pregunta estaba respondiendo…

67
00:06:27,345 --> 00:06:30,995
¿Existe la plena realización de Buda?

68
00:06:33,520 --> 00:06:37,090
Todo es la realización total del Buda!

69
00:06:38,090 --> 00:06:42,240
Aunque seamos retorcidos, aunque no hagamos zazen.

70
00:06:43,130 --> 00:06:47,830
Por eso a veces es bueno decir que el zazen es inútil.

71
00:06:49,410 --> 00:06:55,340
Porque cuando haces zazen en pantalones cortos en casa

72
00:06:55,760 --> 00:07:01,180
sin kesa, sin ceremonia, sin amigos,
sin un maestro, sin discípulos,

73
00:07:01,980 --> 00:07:07,360
Te dices a ti mismo "Es el mismo zazen
que solía hacer cuando era un novato!"

74
00:07:07,970 --> 00:07:12,030
Al principio, cuando estaba en casa y pensaba:

75
00:07:12,030 --> 00:07:14,410
"¡Ah, tengo ganas de hacer zazen!"

76
00:07:16,521 --> 00:07:17,751
Estaba pensando…
