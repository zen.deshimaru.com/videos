1
00:00:00,000 --> 00:00:03,380
Es ist sinnlos, nach Japan zu gehen, um Zen zu üben. 

2
00:00:03,640 --> 00:00:07,360
Wenn Leute mich in Dijon um Informationen bitten, sage ich ihnen Folgendes: 

3
00:00:07,620 --> 00:00:09,240
"Wir werden nicht über Zen reden." 

4
00:00:09,500 --> 00:00:10,180
"Komm ins Dojo." 

5
00:00:10,440 --> 00:00:14,040
"Nur dann wirst du fühlen, ob es zu dir passt oder nicht." 

6
00:00:14,360 --> 00:00:18,100
Vom ersten Training an können Sie fühlen, was es bringt. 

7
00:00:18,680 --> 00:00:21,800
Wenn Sie Lust dazu haben, ist dies ein Zeichen dafür, dass Sie damit einverstanden sind. 

8
00:00:22,000 --> 00:00:26,480
Es sind keine theoretischen Erklärungen erforderlich, um zu wissen, warum wir praktizieren. 

9
00:00:26,780 --> 00:00:35,280
Wenn wir uns in Zazen niederlassen, haben Sie sofort das Gefühl, dass die Haltung etwas hat, das Ruhe gibt, das sich beruhigt. 

10
00:00:35,580 --> 00:00:40,660
Die körperliche Einstellung ist wirklich wichtig, um zu spüren, wie sie zirkuliert. 

11
00:00:40,940 --> 00:00:46,960
Ich denke, es hängt hauptsächlich mit diesem Gleichgewicht zusammen, das es uns ermöglicht, uns zu ergeben. 

12
00:00:47,240 --> 00:00:49,280
Zazen ist auch eine Erfahrung der Stille. 

13
00:00:49,620 --> 00:00:59,800
Wenn wir zusammen in der Stille sind, Wenn Sie diese Stille hören, wird etwas Tiefgründiges vermittelt. 

14
00:01:00,140 --> 00:01:03,140
Ich denke an eine Krankenschwester, die völlig gestresst hereinkommt: 

15
00:01:03,340 --> 00:01:05,840
"Ich kann den Geist nicht beruhigen!" 

16
00:01:06,120 --> 00:01:08,840
Sie kam völlig ruhig heraus. 

17
00:01:09,120 --> 00:01:14,620
Wenn wir allmählich tiefes Atmen erkennen können, 

18
00:01:15,140 --> 00:01:22,300
es löscht all diese nervigen Gedanken, alle Komplikationen aus. 

19
00:01:22,720 --> 00:01:29,720
Es ist sogar schwer zu sagen, dass ich Buddhist bin, weil es wie eine Kirche klingt. 

20
00:01:30,280 --> 00:01:33,720
Wenn Sie dies nicht brauchen, um Zen zu üben … 

21
00:01:34,020 --> 00:01:38,800
Es ist heutzutage schade, dass die Menschen weltliche Meditation wollen. 

22
00:01:39,220 --> 00:01:42,800
In Dijon gibt es viele Arten von Meditationen. 

23
00:01:43,140 --> 00:01:47,360
Ich kenne sie nicht, ich kann nicht über sie sprechen. 

24
00:01:47,560 --> 00:01:52,940
Aber ich persönlich übe gerne so, wie es gemacht wird. 

25
00:01:53,180 --> 00:01:54,340
Mit ein paar einfachen Regeln: 

26
00:01:54,700 --> 00:01:56,780
Sag Hallo, wenn wir nach Hause kommen. 

27
00:01:57,080 --> 00:01:58,880
Begrüßen Sie andere Leute, mit denen Sie üben. 

28
00:01:59,120 --> 00:02:00,620
Wir machen unsere Meditation. 

29
00:02:00,880 --> 00:02:06,040
Dann rezitieren wir praktisch für das gesamte Universum ein Sutra. 

30
00:02:06,300 --> 00:02:08,200
Es ist eine komplette Angelegenheit. 

31
00:02:08,520 --> 00:02:12,480
Es ist eine Möglichkeit, nicht nur für sich selbst zu trainieren. 

32
00:02:13,500 --> 00:02:17,180
Wir hören nicht immer auf unseren Komfort. 

33
00:02:17,460 --> 00:02:24,300
Während Menschen, die irgendeine Form von Meditation wollen, treffen sie bereits eine Wahl. 

34
00:02:24,560 --> 00:02:29,720
Ich mag diesen Begriff der "Ziellosigkeit" in dieser Zeit, in der alles dazu neigt, Gewinn zu machen. 

35
00:02:30,000 --> 00:02:32,180
Wir sind in Zazen und das war’s. 

36
00:02:32,420 --> 00:02:35,660
Ich habe die Nachricht von Meister Kosen erhalten. 

37
00:02:35,960 --> 00:02:38,180
Ich fühle mich geehrt und eingeschüchtert. 

38
00:02:38,520 --> 00:02:42,800
Die Leute stellen sich vor, dass es vollständig verwirklicht werden muss, ein Meister zu sein. 

39
00:02:43,080 --> 00:02:45,180
Ich fühle mich nicht so. 

40
00:02:45,440 --> 00:02:50,720
Ich empfinde diese Übertragung als den Wunsch, die Sangha weiter zu übertragen und ihr Wachstum zu fördern. 

41
00:02:51,040 --> 00:02:56,300
Unsere Sanghas sind alle Menschen, die mit Meister Kosen üben. 

42
00:02:56,980 --> 00:03:04,360
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

