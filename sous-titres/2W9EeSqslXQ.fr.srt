1
00:00:00,000 --> 00:00:04,799
Comment synchronisez-vous lentement la marche et la respiration kin-hin? 

2
00:00:04,799 --> 00:00:08,550
Vous avez dit que nous irions plus loin à chaque expiration. 

3
00:00:08,550 --> 00:00:15,690
Mais la plupart des gens respirent deux fois en une seule étape. 

4
00:00:15,690 --> 00:00:19,470
Bien sûr, je veux en faire deux. 

5
00:00:19,470 --> 00:00:24,180
Les gens doublent leur respiration car ils ne font pas de pause entre les respirations. 

6
00:00:24,180 --> 00:00:28,650
Lorsque vous marchez lentement, la petite pause est fondamentale. 

7
00:00:28,650 --> 00:00:33,059
Si vous vous reposez un peu sur l’expiration …. 

8
00:00:33,059 --> 00:00:38,210
Vous êtes dans le rythme. 

9
00:00:38,210 --> 00:00:44,309
Dogen et son maître Nyojo disent …. 

10
00:00:44,309 --> 00:00:50,219
… que le Bouddha lui-même a dit que vous ne doubliez pas votre souffle. 

11
00:00:50,219 --> 00:00:55,050
Pour ce faire, à la fin de l’expiration, il faut faire une petite pause … 

12
00:00:55,050 --> 00:01:03,239
…. dans lequel nous avancerons d’un demi-pas … 

13
00:01:03,239 --> 00:01:09,299
… après une courte pause avant d’inhaler et nous sommes en bon rythme. 

14
00:01:09,299 --> 00:01:14,610
Nous en avons assez … beaucoup moins. 

15
00:01:14,610 --> 00:01:20,580
Pendant la sesshin, j’ai agrandi les hins de la famille. 

16
00:01:20,580 --> 00:01:26,159
J’avais déjà vécu cela dans un autre exercice avec un autre maître … 

17
00:01:26,159 --> 00:01:32,670
Vous avez dû marcher longtemps les yeux bandés. 

18
00:01:32,670 --> 00:01:38,280
Il faut marcher sans rien voir. 

19
00:01:38,280 --> 00:01:42,270
C’est fatigant car il faut aussi mettre les mains en position … 

20
00:01:42,270 --> 00:01:45,540
… moins confortable qu’en gentil hin. 

21
00:01:45,540 --> 00:01:51,180
Si vous faites du hin en famille pendant longtemps, il se fatiguera …. 

22
00:01:51,180 --> 00:01:54,659
… un autre aspect de nous-mêmes. 

23
00:01:54,659 --> 00:01:59,360
C’est un aspect actif. J’ai appris en médecine chinoise …. 

24
00:01:59,360 --> 00:02:06,920
…. que pour soigner une activité excessive, vous avez besoin de repos. 

25
00:02:13,340 --> 00:02:18,170
Si vous faites du hin depuis longtemps, vous êtes épuisé. 

26
00:02:18,170 --> 00:02:23,230
Vous avez des maux de dos, des épaules, des bras … 

27
00:02:23,230 --> 00:02:29,330
Si vous marchez ainsi pendant une heure, les gens sont épuisés. 

28
00:02:29,330 --> 00:02:33,290
Ils ne voudront qu’une chose: être en zazen. 

29
00:02:33,290 --> 00:02:41,470
Quel soulagement quand on est en zazen: c’est complémentaire. 

30
00:02:41,470 --> 00:02:46,549
Vous vous dites: quelle détente, quel abandon! 

31
00:02:46,549 --> 00:02:51,650
Vous voulez vraiment être à Zazen. 

32
00:02:51,650 --> 00:02:57,920
Après une heure à Zazen, vous aurez le sentiment …. 

33
00:02:57,920 --> 00:03:02,720
….. picotements désagréables … 

34
00:03:02,720 --> 00:03:07,040
Vous voudrez bouger et souhaiteriez que cela se termine à cause de la douleur. 

35
00:03:07,040 --> 00:03:11,630
Le zazen et la marche lente sont complètement complémentaires. 

36
00:03:11,630 --> 00:03:14,870
Mais parce que nous pratiquons le hin enfant pendant une courte période, nous ne le réalisons pas. 

37
00:03:14,870 --> 00:03:19,430
Je vais maintenant faire l’enfant plus longtemps pendant les sesshinsµ. 

38
00:03:19,430 --> 00:03:25,130
Pour que les gens aiment s’asseoir et abandonner certaines choses en zazen. 

39
00:03:25,130 --> 00:03:29,350
Votre question est très bonne et n’est pas nouvelle. 

40
00:03:29,350 --> 00:03:38,920
Maître Dogen a posé le problème il y a 1400 ans. 

