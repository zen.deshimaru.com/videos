Zen en sofrologie: fundamentele houdingen en hun invloed op de hersenen

Master Kosen werd uitgenodigd voor het congres 2013 van de Federatie van Professionele Scholen voor Wijsbegeerte (www.sophro.fr).

Hij legt de vier fundamentele houdingen van Zen uit en hun invloed op de hersenen. Hij keert terug naar de bronnen van dynamische ontspanning. Hij benadrukt het belang van de drie pijlers van houding, ademhaling en gemoedstoestand in boeddhistische meditatie, met name de beoefening van Zen, zazen.
