1
00:00:07,490 --> 00:00:10,656
Les gens pensent que le zazen c’est une technique

2
00:00:10,656 --> 00:00:13,173
qui va les aider dans leur vie.

3
00:00:13,173 --> 00:00:16,800
Ils en font un petit truc ridicule

4
00:00:16,800 --> 00:00:21,820
et font zazen en espérant que ça va les aider dans leur vie.

5
00:00:21,820 --> 00:00:31,280
C’est vrai, parce que dans ce moment le plus conscient,

6
00:00:31,280 --> 00:00:35,300
le plus énergisé, le plus présent qui soit…

7
00:00:35,300 --> 00:00:39,820
… on influence complètement tous les phénomènes de notre vie, c’est sûr.

8
00:00:40,020 --> 00:00:46,240
Mais nous ne faisons pas cela pour améliorer notre ego…

9
00:00:46,250 --> 00:00:50,720
Pour nous rendre plus calmes…

10
00:00:50,720 --> 00:00:52,880
Parce que ça va nous faire du bien…

11
00:00:52,880 --> 00:00:58,500
Parce que ça va nous économiser quelques heures de sommeil…

12
00:00:58,500 --> 00:01:02,759
Nous donner de plus grandes performances sexuelles…

13
00:01:02,759 --> 00:01:06,100
Ou nous aider à assumer le stress.

14
00:01:06,100 --> 00:01:11,500
Ça, c’est rabaisser le zazen

15
00:01:11,500 --> 00:01:16,230
au niveau d’un outil pour notre propre vie illusoire.

16
00:01:16,230 --> 00:01:23,760
Mais c’est le contraire.

17
00:01:23,920 --> 00:01:28,980
On va concentrer sa vie pour générer le zazen le plus efficace possible.

18
00:01:30,890 --> 00:01:33,880
S’asseoir dans la position du Bouddha,

19
00:01:33,880 --> 00:01:39,380
c’est le moment le plus intense, le plus conscient.

20
00:01:39,380 --> 00:01:43,020
Tout tourne autour de ça.

21
00:01:43,020 --> 00:01:46,340
Tout se réunit : le passé, le présent, le futur.

22
00:01:46,340 --> 00:01:52,060
Tous les phénomènes de notre vie sont dans le zazen.

23
00:01:52,060 --> 00:02:00,040
C’est en zazen qu’on a le pouvoir, la conscience,

24
00:02:06,620 --> 00:02:12,020
de changer fondamentalement les tenants et les aboutissants de notre vie.

25
00:02:16,320 --> 00:02:21,240
Dans le zazen, on s’assoit dans une posture digne.

26
00:02:21,240 --> 00:02:25,380
Une posture de héros, de Bouddha, de Dieu .

27
00:02:25,500 --> 00:02:31,320
On est droit, fort, beau, calme, immobile.

28
00:02:31,320 --> 00:02:36,600
Il ne faut pas s’endormir, il faut tendre la colonne vertébrale.

29
00:02:36,600 --> 00:02:41,980
Il faut respirer, se détendre.

30
00:02:46,640 --> 00:02:50,740
Dans le zazen, il y a tout, tout ce qu’on est.

31
00:02:50,740 --> 00:02:55,800
Tout notre passé, notre présent et, indirectement, notre futur.

32
00:02:55,980 --> 00:03:05,460
Il y a notre corps, avec ses faiblesses, ses fatigues,

33
00:03:05,600 --> 00:03:11,200
ses impuretés, ses toxines,

34
00:03:11,209 --> 00:03:14,900
ses maladies, son karma, etc.

35
00:03:14,900 --> 00:03:21,380
Il y a notre esprit, notre conscience.

36
00:03:21,380 --> 00:03:25,055
Dans le zazen, tout est réuni.

37
00:03:25,055 --> 00:03:29,320
La posture de zazen,

38
00:03:29,320 --> 00:03:33,880
c’est la plus belle image de soi qu’on peut montrer.

39
00:03:40,620 --> 00:03:46,090
J’ai rencontré Maître Deshimaru, il m’a appris le zazen, je me suis assis en zazen.

40
00:03:46,090 --> 00:03:48,620
Et j’ai tout de suite dit : « Ça, c’est le top ! »

41
00:04:01,360 --> 00:04:05,680
De savoir qu’est-ce que toi tu crées, quelle est la réalité que tu crées…

42
00:04:05,680 --> 00:04:10,680
C’est toi qui crées la réalité. Tu es en train de vivre ton rêve.

43
00:04:10,680 --> 00:04:15,920
Chacun est en train de vivre son rêve, et il faut assumer son rêve, c’est ça le truc.

44
00:04:15,920 --> 00:04:19,480
Pour moi le zazen, ça a toujours été ça.
