1
00:00:08,039 --> 00:00:12,389
Où que vous soyez.

2
00:00:21,107 --> 00:00:23,587
Asseyez-vous en silence.

3
00:00:23,995 --> 00:00:26,755
Soyez totalement présent.

4
00:00:27,698 --> 00:00:32,578
Qui que vous soyez.

5
00:00:39,450 --> 00:00:41,700
Sortez de votre routine quotidienne.

6
00:00:41,790 --> 00:00:43,254
Pratiquez zazen.

7
00:00:43,401 --> 00:00:45,881
avec la Kosen Sangha.

8
00:00:51,238 --> 00:00:57,194
Les événements liés à cette pandémie

9
00:00:58,324 --> 00:01:02,861
certifient le zen de Dogen

10
00:01:03,081 --> 00:01:05,538
et le zen de Deshimaru.

11
00:01:05,862 --> 00:01:09,802
L’impermanence, nous la vivons

12
00:01:11,770 --> 00:01:16,620
L’interdépendance, nous la vivons aussi.

13
00:01:21,799 --> 00:01:24,759
L’unique moment qui compte,

14
00:01:26,209 --> 00:01:29,069
c’est bien maintenant même.

15
00:01:29,259 --> 00:01:30,919
Ici et maintenant.

16
00:01:31,008 --> 00:01:33,698
Quoi que vous fassiez.

17
00:01:36,710 --> 00:01:40,610
Tournez votre attention vers l’intérieur.

18
00:01:40,908 --> 00:01:43,698
Simplement assis.

19
00:01:44,461 --> 00:01:47,161
Droit et silencieux.

20
00:01:48,301 --> 00:01:49,821
Le Bouddha,

21
00:01:50,388 --> 00:01:53,538
notre maître intérieur.

22
00:01:53,942 --> 00:01:56,332
Le dharma, la voie, la pratique.

23
00:01:56,671 --> 00:01:59,091
La sangha, la sangha virtuelle.

24
00:02:03,066 --> 00:02:07,486
Quand vous le pouvez.

25
00:02:12,081 --> 00:02:16,081
Rejoignez-nous ici et maintenant.

26
00:02:16,630 --> 00:02:20,630
Sans ego.

27
00:02:20,769 --> 00:02:24,269
Sans but.

28
00:02:52,434 --> 00:02:54,384
Bonjour à tout le monde !

29
00:02:54,904 --> 00:02:57,614
Et merci d’avoir fait zazen ensemble !
