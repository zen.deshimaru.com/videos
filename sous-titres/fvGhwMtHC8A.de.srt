1
00:00:38,480 --> 00:00:40,340
Die Sitzung beginnt, 

2
00:00:42,060 --> 00:00:45,680
Richten Sie Ihre Kamera vor dem Hinsetzen gut aus. 

3
00:00:48,700 --> 00:00:54,020
Du darfst nicht sprechen, dies ist nicht die Zeit, wir machen Zazen in der Stille. 

4
00:01:00,860 --> 00:01:05,320
Installieren Sie sich. 

5
00:01:05,320 --> 00:01:10,240
Schieben Sie die Erde mit den Wurzeln 

6
00:01:10,240 --> 00:01:18,780
und gleichzeitig wird die Wirbelsäule in Richtung Himmel gestreckt. 

7
00:01:21,600 --> 00:01:24,320
Das Atmen ist nasal, 

8
00:01:24,320 --> 00:01:27,560
Sie atmen durch die Nasenlöcher ein 

9
00:01:27,560 --> 00:01:29,540
für diejenigen, die haben 

10
00:01:29,540 --> 00:01:34,820
und du atmest tief aus 

11
00:01:34,820 --> 00:01:38,460
und dort lässt du die Spannung los 

12
00:01:45,340 --> 00:01:50,960
Zana, du atmest zu schwer, dein Körper bewegt sich zu viel. 

13
00:01:50,960 --> 00:01:56,810
Ich dachte du musst rauf und runter gehen. 

14
00:01:56,810 --> 00:02:00,240
Sei vor allem ruhig, Zana. Stille! 

15
00:02:04,360 --> 00:02:06,840
Du hast gute Körperhaltungen. 

16
00:02:11,880 --> 00:02:19,940
Selbst wenn es sich um ein virtuelles Dojo handelt, müssen Sie Ihre Haare mindestens frisieren, bevor Sie kommen. 

17
00:02:19,940 --> 00:02:23,940
ohne jemanden zu nennen. El Broco zum Beispiel. 

18
00:02:24,740 --> 00:02:29,460
Hast du dir heute Morgen die Haare mit einem Kracher gekämmt? 

19
00:02:30,720 --> 00:02:33,220
Es ist auch notwendig, die Wurzeln zu stylen. 

20
00:02:36,000 --> 00:02:39,080
Es gibt sehr schöne Haltungen. 

21
00:02:42,740 --> 00:02:44,380
Keine Angst, 

22
00:02:45,100 --> 00:02:51,660
Lassen Sie sich in diesen schwierigen Zeiten nicht von außen absorbieren. 

23
00:02:52,160 --> 00:02:57,940
Konzentriere dich neu, sei dein eigener Meister, 

24
00:02:59,480 --> 00:03:04,940
und wie Ronaldo sagte: 

25
00:03:38,340 --> 00:03:41,960
Lass dich nicht von deinen Gefühlen täuschen, 

26
00:03:44,640 --> 00:03:48,620
Wie fühlst du dich, wenn du das siehst? 

27
00:03:55,480 --> 00:03:57,640
Und das zu sehen? 

28
00:04:07,160 --> 00:04:10,300
Suche keine Freude, 

29
00:04:12,040 --> 00:04:14,960
Lauf nicht vor Angst weg 

30
00:04:17,460 --> 00:04:20,620
Denn wie Maître Deshimaru sagte: 

31
00:04:22,380 --> 00:04:26,060
"Auch wenn du Blumen magst, verdorren sie. 

32
00:04:27,640 --> 00:04:32,780
Auch wenn Sie kein Unkraut mögen, wachsen sie. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Ja! Ja! 

35
00:04:55,600 --> 00:04:58,700
Haben wir noch nicht angefangen? Können wir uns bewegen? 

36
00:04:58,700 --> 00:05:02,640
Wir werden wieder mit Radis und Rose beginnen, die unten sind. 

37
00:05:03,780 --> 00:05:09,160
Ich verstehe nicht, soll ich die Kamera anhalten? Was muss getan werden? Beides gleichzeitig? 

38
00:05:10,820 --> 00:05:15,340
Wer hat gesprochen? Es ist Muriel, nein, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, pass auf deine Karotte auf, denn wenn du dich zu viel bewegst, willst du dich übergeben. 

