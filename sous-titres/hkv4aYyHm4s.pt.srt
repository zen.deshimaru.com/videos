1
00:00:13,360 --> 00:00:15,750
Um conto contado pelo Mestre Deshimaru

2
00:00:17,320 --> 00:00:19,320
que se chama "Caminhar na Montanha".

3
00:00:19,980 --> 00:00:21,980
Portanto, já só o título,

4
00:00:22,560 --> 00:00:24,080
é maravilhoso,

5
00:00:24,609 --> 00:00:30,360
para poder caminhar nas montanhas. Um mestre costumava ir dar um passeio nas montanhas e quando voltava

6
00:00:31,179 --> 00:00:33,179
um dos seus discípulos perguntou-lhe

7
00:00:33,700 --> 00:00:35,700
"Mestre, onde foi dar um passeio?"

8
00:00:35,980 --> 00:00:40,420
"Na montanha" respondeu o mestre e o discípulo insistiu "Mas que caminho

9
00:00:40,900 --> 00:00:43,380
tomou, o que viu"?

10
00:00:45,260 --> 00:00:52,940
O mestre respondeu: "Segui o cheiro das flores e vagueei ao longo dos jovens rebentos primaveris".

11
00:00:55,480 --> 00:00:57,480
Mestre Deshimaru continua

12
00:00:58,540 --> 00:01:02,180
"É preciso deixar-se guiar pelo Darma de Buda.

13
00:01:02,380 --> 00:01:03,820
confiança

14
00:01:04,140 --> 00:01:08,040
com ervas e flores a crescerem sem rumo,

15
00:01:08,600 --> 00:01:10,220
sem egoísmo,

16
00:01:10,440 --> 00:01:15,640
naturalmente, inconscientemente. Essa resposta veio da fonte de sabedoria

17
00:01:16,900 --> 00:01:20,560
a verdadeira sabedoria deve ser criada para além do conhecimento...

18
00:01:21,100 --> 00:01:26,240
e memória". É um ensino bastante poderoso.

19
00:01:26,700 --> 00:01:29,160
Num kusen,

20
00:01:29,340 --> 00:01:37,040
O ensino dado no dojo, Mestre Kosen ensina: "A filosofia de Shikantaza",

21
00:01:37,900 --> 00:01:40,580
a filosofia de ’apenas zazen’.

22
00:01:40,800 --> 00:01:47,380
deve guiar as nossas vidas é a verdadeira essência do budismo, o método mais perfeito

23
00:01:48,249 --> 00:01:50,699
O próprio zazen é o satori, o despertar.

24
00:01:51,520 --> 00:01:53,520
Estamos a cometer um erro.

25
00:01:54,159 --> 00:01:56,368
se pensa que há um zazen antes

26
00:01:57,100 --> 00:02:02,460
e que com a acumulação de zazen, zazen, quinze dias, algo acontece, o despertar.

27
00:02:05,300 --> 00:02:11,520
Qualquer pessoa que pratique zazen desencadeia de imediato o processo de despertar.

28
00:02:13,020 --> 00:02:15,800
Bem, este processo não se enquadra nas nossas categorias.

29
00:02:16,900 --> 00:02:18,900
às vezes as pessoas dizem

30
00:02:18,959 --> 00:02:20,959
"’zazen foi muito difícil,

31
00:02:21,420 --> 00:02:24,949
era "zazen mau" ou por vezes "zazen mau".

32
00:02:25,860 --> 00:02:27,860
É muito confortável.

33
00:02:28,409 --> 00:02:35,929
e o processo que desencadeia o zazen está para além das nossas categorias ou da nossa consciência pessoal".

34
00:02:36,660 --> 00:02:42,169
Portanto, o método de Dogen, o método dos outros Mestres, é óptimo, é único.

35
00:02:43,799 --> 00:02:48,769
São ensinamentos que parecem muito simples que se recebe,

36
00:02:50,190 --> 00:02:52,190
que recebemos durante o zazen

37
00:02:53,400 --> 00:02:55,400
e que vamos a

38
00:02:58,530 --> 00:03:02,750
integrar, o que significa que vamos torná-los nossos,

39
00:03:04,019 --> 00:03:09,319
suavemente, profundamente, inconscientemente, é uma lição valiosa.
