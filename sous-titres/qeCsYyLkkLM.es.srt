1
00:00:11,160 --> 00:00:13,720
Buen dia Como te va 

2
00:00:16,560 --> 00:00:21,280
La actitud, enseñada por el Maestro Deshimaru … 

3
00:00:25,000 --> 00:00:28,460
Lo llamó el método Zen … 

4
00:00:31,920 --> 00:00:35,640
Comienza con una base estable … 

5
00:00:39,600 --> 00:00:43,160
Es decir, tienes las dos rodillas que tocan el suelo … 

6
00:00:47,480 --> 00:00:50,540
Y pones el pyrhenuaem en la caja fuerte (cojín) 

7
00:00:53,860 --> 00:00:58,940
Al hacer el loto completo, primero coloca un pie … 

8
00:01:04,220 --> 00:01:07,300
Y luego el otro. 

9
00:01:07,900 --> 00:01:12,140
Si tienes una buena actitud, realmente puedes soltarte. 

10
00:01:12,140 --> 00:01:15,220
Como dicen; liberar cuerpo y mente. 

11
00:01:22,460 --> 00:01:24,320
El maestro Deshimaru dijo: 

12
00:01:25,020 --> 00:01:28,640
El zen es el nuevo humanismo para el siglo XXI. 

13
00:01:29,560 --> 00:01:31,440
Siempre debe estar fresco. 

14
00:01:31,440 --> 00:01:34,000
Tienes que practicar el zen real. 

15
00:01:34,000 --> 00:01:36,600
Jala la barbilla 

16
00:01:37,060 --> 00:01:39,980
La columna recta, estirada. 

17
00:01:40,580 --> 00:01:42,580
Solo Zazen. 

18
00:01:43,780 --> 00:01:45,400
El resto no es tan importante. 

19
00:01:45,820 --> 00:01:48,140
Al comienzo de Zazen oirás tres campanillas. 

20
00:01:52,040 --> 00:01:56,060
Y luego hacemos kinhin. La meditación caminando. 

21
00:02:01,580 --> 00:02:03,580
Habrá dos pequeñas burbujas. 

22
00:02:15,980 --> 00:02:18,940
Estire bien la rodilla de su pierna delantera. 

23
00:02:22,900 --> 00:02:28,340
Y empuja el suelo con la raíz del dedo gordo del antepié … 

24
00:02:34,580 --> 00:02:36,580
Relaja bien tus hombros. 

25
00:02:38,260 --> 00:02:41,480
Y luego al final de su fecha de vencimiento … 

26
00:02:44,100 --> 00:02:46,100
Tu inspiras … 

27
00:02:47,560 --> 00:02:51,500
Y das un pequeño paso adelante con tu otro pie. 

28
00:02:56,820 --> 00:02:58,820
Y luego comienzas de nuevo, exhalas … 

29
00:03:01,360 --> 00:03:05,180
Y pones todo tu peso en tu antepié. 

30
00:03:10,920 --> 00:03:13,760
El pulgar de tu mano izquierda … 

31
00:03:16,120 --> 00:03:19,960
Lo pones en tu puño. 

32
00:03:22,400 --> 00:03:24,400
Entonces tomas tu mano derecha … 

33
00:03:26,960 --> 00:03:29,480
Y te cubres la mano izquierda. 

34
00:03:36,140 --> 00:03:40,040
Practicar ’zazoom’es muy útil … 

35
00:03:40,260 --> 00:03:42,600
Especialmente en estos tiempos. 

36
00:03:48,280 --> 00:03:53,440
Pero normalmente es complementario a la práctica en un dojo real. 

37
00:03:58,860 --> 00:04:03,320
Está claro que estamos practicando juntos y dedicamos esta práctica a … 

38
00:04:12,540 --> 00:04:16,880
Para todas las personas que se cuidan y tratan de salvar a otro. 

39
00:04:34,720 --> 00:04:36,720
Hola toshi Hola 

40
00:04:36,720 --> 00:04:38,720
Hola maestro 

41
00:04:40,680 --> 00:04:43,940
Y a todos los demás. Buen dia 

42
00:04:46,100 --> 00:04:48,100
Buen dia Ingrid. > Hola 

43
00:04:48,300 --> 00:04:49,960
Miro a la gente porque … 

44
00:04:51,840 --> 00:04:54,640
Aquí dice Gabriela, pero esa Gabriela. 

45
00:04:54,640 --> 00:04:55,440
¿Dónde está ella? 

46
00:04:57,460 --> 00:05:00,520
Ah, no te reconocí con tus lentes. 

47
00:05:06,020 --> 00:05:09,160
Yo, quien hace las traducciones, ¿cómo es que no me reconoces? 

