1
00:00:00,000 --> 00:00:02,240
Je suis moine zen. 

2
00:00:04,540 --> 00:00:09,960
Je suis également calligraphe. 

3
00:00:10,980 --> 00:00:18,660
La calligraphie chinoise est appréciée des personnes qui pratiquent le zazen. 

4
00:00:19,280 --> 00:00:24,980
Il n’est pas facile de parler de Zen. 

5
00:00:25,400 --> 00:00:28,820
Pour l’exprimer, pour le manifester. 

6
00:00:29,240 --> 00:00:34,480
À travers la calligraphie, nous voyons l’esprit d’une personne qui fait du zazen. 

7
00:00:35,000 --> 00:00:39,100
Qui pratique l’attitude d’éveil. 

8
00:00:40,020 --> 00:00:46,260
Maître Deshimaru explique que la calligraphie n’est pas seulement une technique. 

9
00:00:46,980 --> 00:00:52,700
Nous ne faisons pas que de la calligraphie selon des techniques, des propriétés … 

10
00:00:53,360 --> 00:00:58,400
Nous calligraphions avec l’esprit, avec le tibia. 

11
00:00:58,880 --> 00:01:09,440
Avec l’esprit, avec tout le corps et l’esprit en un. 

12
00:01:10,720 --> 00:01:18,400
La calligraphie l’imprime sur papier. C’est une présence. 

13
00:01:18,960 --> 00:01:24,320
Cela se ressent dans la calligraphie de grands maîtres comme Deshimaru et Niwa Zenji. 

14
00:01:25,760 --> 00:01:30,000
La valeur de la calligraphie n’est pas seulement dans la forme. 

15
00:01:31,440 --> 00:01:36,560
Mais aussi dans l’élan, la présence, l’énergie. 

16
00:01:37,280 --> 00:01:46,240
Dans l’activité qui se pose …. 

17
00:01:47,040 --> 00:01:52,080
Et que nous ressentons et percevons. 

18
00:01:52,500 --> 00:01:55,800
Nous partons d’un élan qui vient de l’immobilité. 

19
00:01:56,950 --> 00:01:58,853
Zen, calme, calme, silence. 

20
00:02:00,160 --> 00:02:06,093
Pour ne rien faire. 

21
00:02:06,100 --> 00:02:07,910
Pour laisser passer les pensées. 

22
00:02:07,950 --> 00:02:10,750
Ne pas être monopolisé. 

23
00:02:10,840 --> 00:02:15,040
Être concentré dans une attitude. 

24
00:02:15,920 --> 00:02:21,329
Jambes croisées. 

25
00:02:21,329 --> 00:02:26,880
Le coussin permet d’incliner le bassin vers l’avant. 

26
00:02:26,880 --> 00:02:31,280
Pour nettoyer toute la cage thoracique. 

27
00:02:32,240 --> 00:02:37,040
Dans cette pose, nous ramenons nos mains. 

28
00:02:38,160 --> 00:02:43,680
Cela nous permet de nous unir et de nous centrer. 

29
00:02:44,400 --> 00:02:51,040
C’est l’attitude de la méditation zen, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
C’est à partir de cela, et de l’élan, de l’activité, que la calligraphie émergera. 

31
00:02:58,480 --> 00:03:05,200
Pas de complications, de vouloir faire le bien, de vouloir faire le bien, de vouloir faire du beau temps … 

32
00:03:06,240 --> 00:03:11,520
Seulement cet élan de vie. 

33
00:03:12,560 --> 00:03:22,160
La calligraphie émergera de cette impulsion zen, cette impulsion, cette impulsion. 

34
00:03:23,280 --> 00:03:33,200
C’est à cela que sert la calligraphie zen. 

35
00:03:34,720 --> 00:03:59,600
Compte tenu de la phase de civilisation atteinte par la Chine sous la dynastie Tang … 

36
00:04:01,120 --> 00:04:15,520
Aux 7e, 8e et 9e siècles, les pays environnants, le Japon, la Corée et le Vietnam, étaient attirés par cette culture et par la langue, les caractères chinois. 

37
00:04:16,400 --> 00:04:27,200
Le script chinois s’est donc étendu à ces pays. 

38
00:04:28,080 --> 00:04:34,480
Mais tout le monde l’a enregistré selon son propre ressenti, sa propre langue. 

39
00:04:35,280 --> 00:04:44,560
Chacun d’eux a développé sa propre méthode de calligraphie. 

40
00:04:45,520 --> 00:04:56,080
Ils ont également introduit des syllabi, des caractères proches d’un alphabet mélangés à des caractères chinois. 

41
00:04:58,320 --> 00:05:04,480
La calligraphie japonaise présente des différences mineures par rapport à la calligraphie chinoise. 

42
00:05:05,680 --> 00:05:13,440
Tous utilisent des pinceaux, dessinent des caractères chinois, mais utilisent également des systèmes syllabiques. 

43
00:05:15,040 --> 00:05:23,280
Les Vietnamiens, précédemment influencés par la Chine, étaient encore plus créatifs. 

44
00:05:24,800 --> 00:05:31,600
Ils ont inventé leur propre système idéographique pour transcrire leur langue au modèle du chinois. 

45
00:05:33,440 --> 00:05:38,400
Il est écrit: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
C’est le soutra de l’Esprit de la Grande Sagesse qui nous fait avancer. 

48
00:05:59,840 --> 00:06:06,160
C’est l’enseignement universel du Bouddha, spécifique à toutes les écoles bouddhistes. 

49
00:06:07,360 --> 00:06:16,800
Il est chanté en chinois ancien, en japonais ancien, en transcription phonétique du sanskrit. 

50
00:06:18,080 --> 00:06:24,000
Cela en fait un texte universel, car personne ne le comprend ni ne le lit dans sa propre langue. 

51
00:06:25,360 --> 00:06:39,120
Les Japonais, les Chinois, les Indiens …. Nous le prononçons nous-mêmes avec quelques ajustements mineurs …. 

52
00:06:40,980 --> 00:06:47,460
C’est quelque chose qui appartient à tout le monde et à personne en particulier. 

53
00:06:48,220 --> 00:06:53,440
C’est la phrase que j’ai écrite ici, dans un style d’écriture appelé cursif. 

54
00:06:53,680 --> 00:06:58,520
Ce n’est pas écrire le dictionnaire. 

55
00:06:58,560 --> 00:07:03,680
Ce n’est pas une écriture régulière. 

56
00:07:05,740 --> 00:07:09,100
Il est développé pour une écriture rapide. 

57
00:07:09,480 --> 00:07:15,960
Une sorte de sténographie. 

58
00:07:16,240 --> 00:07:28,560
Nous maintenons les lignes principales du personnage en simplifiant, par des courbures, par des points, où il y avait une ligne … 

59
00:07:29,600 --> 00:07:39,440
Le personnage a été simplifié à l’extrême, il prend une forme différente de sa forme d’origine. 

60
00:07:39,760 --> 00:07:47,680
Nous nous dirigeons vers une abstraction, en préservant l’essence de la forme originale. 

61
00:07:48,800 --> 00:07:55,920
Il vous permet d’écrire plus rapidement. 

62
00:07:58,080 --> 00:08:05,240
Dans l’intrigue, nous trouvons cette impulsion, cette impulsion, cette impulsion de zazen. 

63
00:08:05,640 --> 00:08:13,280
Parce que nous connectons tout le personnage ensemble. 

64
00:08:13,660 --> 00:08:18,479
À la fois. 

65
00:08:18,479 --> 00:08:23,470
Nous pouvons même relier les personnages entre eux. 

66
00:08:24,000 --> 00:08:31,840
Nous lions chacun des personnages, chacune des propriétés du personnage. 

67
00:08:33,120 --> 00:08:38,520
Si plusieurs caractères sont dessinés, ils peuvent être liés ensemble. 

68
00:08:38,940 --> 00:08:44,960
Nous le faisons sur le souffle, sur l’impulsion. 

69
00:08:45,400 --> 00:08:51,540
À l’élan, l’énergie, la dynamique, l’activité de tout le corps, tout l’esprit en un. 

70
00:08:51,540 --> 00:08:55,200
En un seul mouvement, du début à la fin. 

71
00:08:55,200 --> 00:08:59,200
Nous ne pouvons pas revenir en arrière, nous ne pouvons pas le mettre à jour. 

72
00:08:59,200 --> 00:09:02,900
Nous ne pouvons pas le corriger. 

73
00:09:03,000 --> 00:09:05,120
Nous le faisons en conscience. 

74
00:09:05,920 --> 00:09:11,600
Nous nous laissons aussi aller avec joie. 

75
00:09:12,060 --> 00:09:18,220
Mais nous ne pouvons rien corriger. 

76
00:09:18,560 --> 00:09:22,160
Quand il est tracé, il est tracé. 

77
00:09:22,200 --> 00:09:25,529
C’est ici et maintenant. 

78
00:09:25,529 --> 00:09:29,000
C’est quelque chose qui ne reviendra jamais. 

79
00:09:29,100 --> 00:09:33,800
Cela existe complètement, éternellement, dans le moment. 

80
00:09:33,900 --> 00:09:36,200
C’est zen aussi. 

81
00:09:36,250 --> 00:09:39,640
Chaque fois, chaque souffle zazen est unique. 

82
00:09:42,520 --> 00:09:44,640
Elle ne revient pas. 

83
00:09:45,340 --> 00:09:52,540
C’est un point, un trait d’union de l’attitude. 

84
00:09:52,600 --> 00:09:58,320
Dans l’humeur, la respiration et l’activité totale. 

85
00:09:58,420 --> 00:10:03,160
C’est un temps de vie, de conscience, absolu, universel. 

86
00:10:03,720 --> 00:10:06,760
Qui ne revient pas. 

87
00:10:07,120 --> 00:10:11,000
C’est très important en calligraphie. 

88
00:10:11,000 --> 00:10:17,500
Si vous calligraphiez comme un robot sans y mettre toutes vos pensées, c’est une technique. 

89
00:10:17,540 --> 00:10:18,920
Zazen est le même. 

90
00:10:19,000 --> 00:10:25,880
Si vous pratiquez le zazen comme technique de bien-être, ce n’est pas la dimension du zen. 

