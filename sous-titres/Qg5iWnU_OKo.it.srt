1
00:00:00,100 --> 00:00:01,460
Mi chiamo Christophe Desmur. 

2
00:00:01,780 --> 00:00:03,540
Il mio monaco Zen si chiama Ryurin. 

3
00:00:03,880 --> 00:00:05,580
Sono anche un calligrafo. 

4
00:00:05,900 --> 00:00:09,560
Insegno calligrafia cinese, che ho scoperto nello Zen. 

5
00:00:09,840 --> 00:00:13,860
L’ho imparato da un monaco Zen. 

6
00:00:14,320 --> 00:00:18,260
Mi piace condividere la calligrafia con tutti. 

7
00:00:18,860 --> 00:00:22,540
Lavoro molto nelle scuole. 

8
00:00:22,900 --> 00:00:27,360
Faccio calligrafia con i bambini nei seminari. 

9
00:00:27,700 --> 00:00:31,020
Anche per adulti e disabili. 

10
00:00:31,380 --> 00:00:33,300
A loro piace molto. 

11
00:00:33,780 --> 00:00:39,220
I miei studenti più piccoli hanno 6 anni e il mio studente più grande ha 90 anni. 

12
00:00:39,760 --> 00:00:42,800
Insegno anche cinese. 

13
00:00:43,180 --> 00:00:44,840
Questo è il mio core business. 

14
00:00:45,140 --> 00:00:48,020
Questa calligrafia è semplicemente "Zen" (禅). 

15
00:00:48,320 --> 00:00:54,060
È una calligrafia realizzata nell’attuale stile semi-corsivo. 

16
00:00:54,580 --> 00:01:06,760
Il pennello è corto, va da punti a linee, per formare un carattere cinese o giapponese. 

17
00:01:08,420 --> 00:01:11,320
Questo è "Reiki" (靈氣), l’energia spirituale. 

18
00:01:11,620 --> 00:01:13,880
È una tecnica di guarigione. 

19
00:01:14,220 --> 00:01:20,260
Continuiamo questa tradizione perché proviene da un maestro che ha praticato molta calligrafia. 

20
00:01:20,500 --> 00:01:23,920
Ha anche praticato il sumi-e, (墨 絵), era un pittore. 

21
00:01:24,280 --> 00:01:27,580
Apprezzo molto i dipinti del Maestro Deshimaru, era un artista. 

22
00:01:27,840 --> 00:01:31,020
Dipinse e rese molto bene la calligrafia. 

23
00:01:31,440 --> 00:01:37,860
La sua calligrafia è molto forte, densa ed energica. 

24
00:01:38,980 --> 00:01:43,280
"Maka hannya haramita shingyo" (般若 心 経) è il nome di un sutra cantato. 

25
00:01:43,700 --> 00:01:52,480
L’ho rintracciato in ricci: un po ’come piccoli spaghetti. 

26
00:01:52,780 --> 00:01:56,440
I personaggi sono molto collegati e semplificati. 

27
00:01:56,780 --> 00:02:00,800
Ci piace anche quello nella pratica Zen. 

28
00:02:01,240 --> 00:02:08,040
Ai praticanti Zen è piaciuta molto questa scrittura, che è abbastanza libera ed espressiva. 

29
00:02:09,260 --> 00:02:10,740
È un’arte energica. 

30
00:02:11,020 --> 00:02:14,840
È praticato con tutto il corpo, in connessione con la respirazione. 

31
00:02:15,240 --> 00:02:21,900
In connessione con la sua disponibilità della mente, la sua coscienza. 

32
00:02:22,360 --> 00:02:24,720
Quindi è un’arte dell’energia. 

33
00:02:25,000 --> 00:02:28,120
Calmeremo, riorienteremo qualcosa dentro di noi. 

34
00:02:28,360 --> 00:02:33,240
È una metamorfosi completa, un’alchimia dell’equilibrio. 

35
00:02:33,560 --> 00:02:40,040
Orizzontalità, verticalità, centratura, accoppiamento durante il disegno dei segni. 

36
00:02:40,540 --> 00:02:43,120
Disponibilità a mano. 

37
00:02:43,460 --> 00:02:48,080
Nella meditazione Zen, la presenza nelle mani è molto importante. 

38
00:02:48,420 --> 00:02:55,600
Che sia fermo o in funzione. 

39
00:02:55,940 --> 00:02:57,760
Il gesto è diverso nella calligrafia. 

40
00:02:58,180 --> 00:03:00,120
Ma c’è una presenza per mano. 

41
00:03:00,340 --> 00:03:02,320
È la relazione mano-cervello. 

42
00:03:02,600 --> 00:03:04,140
La calligrafia ha una vita. 

43
00:03:04,440 --> 00:03:05,440
Anche se non lo sai! 

44
00:03:05,680 --> 00:03:07,980
Qualche calligrafia ti toccherà, altri no. 

45
00:03:08,200 --> 00:03:11,280
"Yang Sheng" (養生) è cinese ed è più taoista. 

46
00:03:11,740 --> 00:03:15,820
Significa "nutrire il principio vitale". 

47
00:03:16,540 --> 00:03:23,440
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

