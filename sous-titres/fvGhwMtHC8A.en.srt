1
00:00:38,480 --> 00:00:40,340
The session will start, 

2
00:00:42,060 --> 00:00:45,680
before sitting down, orient your camera well. 

3
00:00:48,700 --> 00:00:54,020
You must not speak, this is not the time, we do zazen in silence. 

4
00:01:00,860 --> 00:01:05,320
Get comfortable. 

5
00:01:05,320 --> 00:01:10,240
Push the earth with the roots 

6
00:01:10,240 --> 00:01:18,780
and at the same time the spine is stretched towards the sky. 

7
00:01:21,600 --> 00:01:24,320
Breathing is nasal, 

8
00:01:24,320 --> 00:01:27,560
you breathe in through the nostrils 

9
00:01:27,560 --> 00:01:29,540
for those who have 

10
00:01:29,540 --> 00:01:34,820
and you breathe out deeply 

11
00:01:34,820 --> 00:01:38,460
and that’s where you let go of the tension 

12
00:01:45,340 --> 00:01:50,960
Zana, you’re breathing too hard, your body is moving too much. 

13
00:01:50,960 --> 00:01:56,810
I thought you had to go up and down. 

14
00:01:56,810 --> 00:02:00,240
Above all, be quiet Zana. Silence! 

15
00:02:04,360 --> 00:02:06,840
You have good postures. 

16
00:02:11,880 --> 00:02:19,940
Even if it’s a virtual dojo, you have to do your hair a minimum before coming, 

17
00:02:19,940 --> 00:02:23,940
without naming anyone. El Broco, for example. 

18
00:02:24,740 --> 00:02:29,460
Did you comb your hair with a firecracker this morning? 

19
00:02:30,720 --> 00:02:33,220
It is also necessary to style the roots. 

20
00:02:36,000 --> 00:02:39,080
There are very beautiful postures. 

21
00:02:42,740 --> 00:02:44,380
No fear, 

22
00:02:45,100 --> 00:02:51,660
in these difficult times, do not allow yourself to be absorbed from the outside. 

23
00:02:52,160 --> 00:02:57,940
Refocus, be your own master, 

24
00:02:59,480 --> 00:03:04,940
and as Ronaldo said: 

25
00:03:38,340 --> 00:03:41,960
Don’t be fooled by your emotions, 

26
00:03:44,640 --> 00:03:48,620
seeing this, how do you feel? 

27
00:03:55,480 --> 00:03:57,640
And seeing this? 

28
00:04:07,160 --> 00:04:10,300
Do not seek joy, 

29
00:04:12,040 --> 00:04:14,960
don’t run away from fear 

30
00:04:17,460 --> 00:04:20,620
Because as Maître Deshimaru said: 

31
00:04:22,380 --> 00:04:26,060
"Even if you like flowers they wither. 

32
00:04:27,640 --> 00:04:32,780
Even if you don’t like weeds, they grow. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Yes! Yes! 

35
00:04:55,600 --> 00:04:58,700
Haven’t we started yet? Can we move? 

36
00:04:58,700 --> 00:05:02,640
We will start again with Radis and Rose which is down. 

37
00:05:03,780 --> 00:05:09,160
I don’t understand, should I stop the camera? What must be done? Both of them? 

38
00:05:10,820 --> 00:05:15,340
Who spoke? It’s Muriel, uh no, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, watch your carrot because if you move too much, it makes you want to vomit. 

