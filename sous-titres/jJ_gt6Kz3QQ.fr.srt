1
00:00:03,420 --> 00:00:07,900
Qu’est-ce que l’esprit? 

2
00:00:08,220 --> 00:00:16,200
En chinois et en japonais, divers idéogrammes sont traduits par «fantôme». 

3
00:00:16,880 --> 00:00:19,340
Parfois, il est traduit par "coeur". 

4
00:00:21,960 --> 00:00:23,560
C’est chaque existence. 

5
00:00:23,820 --> 00:00:25,240
C’est la communauté vivante. 

6
00:00:25,500 --> 00:00:27,400
Tel est l’esprit commun de la communauté vivante. 

7
00:00:27,740 --> 00:00:28,900
Voilà l’esprit. 

8
00:00:29,120 --> 00:00:30,780
Il n’y a qu’un seul esprit. 

9
00:00:32,000 --> 00:00:34,960
Qu’est-ce qui crée l’esprit? 

10
00:00:35,740 --> 00:00:37,780
D’un point de vue bouddhiste, rien ne crée l’esprit. 

11
00:00:38,100 --> 00:00:40,540
Parce que l’esprit est sans naissance. 

12
00:00:40,880 --> 00:00:46,780
C’est sans cause ni disparition. 

13
00:00:47,060 --> 00:00:50,020
L’esprit est créé, mais pas créé. 

14
00:00:50,500 --> 00:00:54,220
L’esprit est-il la première cause? 

15
00:00:54,460 --> 00:00:57,820
Il ne peut pas y avoir de première cause. 

16
00:00:58,100 --> 00:00:59,940
Parce que l’origine est pure. 

17
00:01:02,280 --> 00:01:05,420
Quelle aurait été la première cause? 

18
00:01:05,840 --> 00:01:11,200
Nous comprenons, même intellectuellement, que si nous allons dans le passé, nous ne pouvons pas trouver la première cause. 

19
00:01:11,660 --> 00:01:13,300
La première cause n’existe donc pas. 

20
00:01:13,660 --> 00:01:15,640
La première cause est l’effet. 

21
00:01:16,060 --> 00:01:16,900
Maintenant. 

22
00:01:17,440 --> 00:01:20,940
Où réside le fantôme? 

23
00:01:21,280 --> 00:01:23,120
L’esprit existe toujours et réside partout. 

24
00:01:23,420 --> 00:01:25,200
Que nous soyons à Zazen ou ailleurs. 

25
00:01:25,400 --> 00:01:30,140
Il n’y a pas de méditation spéciale qui change la structure de l’esprit. 

26
00:01:30,460 --> 00:01:32,240
L’esprit est toujours présent. 

27
00:01:32,520 --> 00:01:35,780
Qu’est-ce que "l’esprit-corps"? 

28
00:01:36,520 --> 00:01:38,920
La matière et l’énergie ne sont pas séparées l’une de l’autre. 

29
00:01:39,280 --> 00:01:44,600
Ce sont deux expressions différentes de la même chose. 

30
00:01:47,080 --> 00:01:50,360
Le corps et l’esprit sont donc deux expressions différentes de la même chose. 

31
00:01:50,760 --> 00:01:52,280
Vous ne devez pas dénigrer le corps. 

32
00:01:52,640 --> 00:01:53,980
Le corps est l’esprit. 

33
00:01:54,340 --> 00:01:55,720
La matière est l’esprit. 

34
00:01:56,220 --> 00:02:00,520
Quel est l’effet de la pensée sur le corps spirituel? 

35
00:02:01,240 --> 00:02:06,120
La pensée se matérialise dans la réalité. 

36
00:02:07,020 --> 00:02:09,100
Dans le rêve de la matière. 

37
00:02:09,580 --> 00:02:11,040
Le rêve ne se produit pas seulement lorsque vous dormez. 

38
00:02:11,480 --> 00:02:15,580
Le rêve de la matière est ce que nous appelons la «réalité». 

39
00:02:15,900 --> 00:02:17,880
Réalité matérielle. 

40
00:02:18,400 --> 00:02:21,980
Que pouvons-nous en conclure? 

41
00:02:22,340 --> 00:02:27,080
L’esprit voit toujours dans l’esprit. 

