1
00:00:06,180 --> 00:00:11,550
Ça fait plus de la moitié de ma vie que je pratique zazen.

2
00:00:14,890 --> 00:00:20,170
Et c’est très rare que je sois en équilibre parfait.

3
00:00:25,650 --> 00:00:29,445
Il y a donc très peu de chances

4
00:00:29,605 --> 00:00:33,910
que je réalise totalement le Bouddha ?

5
00:00:38,110 --> 00:00:40,910
Le Bouddha aussi avait mal au dos !

6
00:00:47,340 --> 00:00:54,790
On essaie de s’harmoniser avec un truc parfait et vivant.

7
00:00:55,110 --> 00:00:59,740
Ce qui est parfait en nous, c’est qu’on est vivants.

8
00:01:01,860 --> 00:01:06,660
Il y a des gens qui pensent à faire
de la modification génétique.

9
00:01:13,480 --> 00:01:22,120
On dit dans les fables que le bouddha avait
les trente-deux marques de perfection.

10
00:01:23,400 --> 00:01:27,440
C’est-à-dire qu’il n’avait pas de défaut.

11
00:01:41,780 --> 00:01:48,180
C’est déjà bien pouvoir s’approcher,
d’ imiter son dieu.

12
00:01:51,530 --> 00:01:55,780
Mais je pense que le Bouddha était un homme comme nous
et qu’il avait des défauts.

13
00:02:02,690 --> 00:02:05,685
Le premier défaut, c’est qu’il est mort.

14
00:02:05,685 --> 00:02:08,710
S’il n’avait pas eu de défaut, il ne serait pas mort.

15
00:02:11,630 --> 00:02:15,510
Il est mort, il a fait pleurer tous ses disciples.

16
00:02:18,733 --> 00:02:21,406
On fait zazen avec un corps imparfait.

17
00:02:21,686 --> 00:02:24,100
Comme un être imparfait.

18
00:02:26,130 --> 00:02:30,650
Mais ce qui est déjà génial, c’est de nous donner la direction

19
00:02:32,670 --> 00:02:39,020
cellulaire, musculaire, mentale.

20
00:02:39,670 --> 00:02:43,320
La direction où s’approcher ou imiter.

21
00:02:48,450 --> 00:02:51,840
Les musulmans, par exemple, ne mettent pas de Bouddha.

22
00:02:51,910 --> 00:02:55,900
Ils disent : « Nous, on n’a pas de statue,
pas d’image de Dieu ».

23
00:02:57,440 --> 00:02:59,165
Je trouve ça pas mal.

24
00:03:02,695 --> 00:03:07,520
Mais nous, on connaît la direction pour imiter Dieu.

25
00:03:11,630 --> 00:03:18,510
Pour faire tout notre possible pour rendre notre être divin.

26
00:03:22,120 --> 00:03:24,730
Mais nous ne sommes pas parfaits
et nous allons mourir.

27
00:03:27,000 --> 00:03:29,375
Et il y en a qui ne sont pas parfaits

28
00:03:29,375 --> 00:03:31,932
et d’autres qui ne sont PAS PARFAITS.

29
00:03:34,552 --> 00:03:39,830
Souvent, les gens parfaits ne sont pas obligés de…

30
00:03:40,390 --> 00:03:43,060
Moi, quand je faisais de la musique…

31
00:03:46,120 --> 00:03:49,685
Je raconte souvent cette histoire.

32
00:03:50,445 --> 00:03:53,590
… j’avais un producteur.

33
00:03:54,070 --> 00:03:58,640
C’était un Italien multimillionnaire

34
00:04:00,860 --> 00:04:04,415
Peu importe ce qu’il faisait.

35
00:04:05,265 --> 00:04:10,450
Il était très riche et il avait envie de monter une maison d’édition.

36
00:04:13,480 --> 00:04:17,123
Il m’a proposé de produire mon disque.

37
00:04:19,480 --> 00:04:23,820
Et je ne sais pas pourquoi, il est passé
au temple de la Gendronnière.

38
00:04:24,780 --> 00:04:28,520
Pendant les jours de préparation d’un camp d’été.

39
00:04:28,750 --> 00:04:33,050
Il m’a dit : « J’avais envie de voir ce que vous faites ! »

40
00:04:33,520 --> 00:04:36,215
On lui a montré les règles du dojo :

41
00:04:36,425 --> 00:04:39,417
« Tu rentres comme ça, puis tu t’assois. »

42
00:04:39,877 --> 00:04:42,420
Il s’assoit : lotus complet direct.

43
00:04:46,690 --> 00:04:48,450
Lotus impeccable.

44
00:04:48,570 --> 00:04:51,190
« Bon, ben je vois que tu as tout compris ! »

45
00:04:51,190 --> 00:04:54,080
« Le zazen, ça va être dans une heure. »

46
00:04:55,870 --> 00:04:58,580
« Tu peux visiter le temple. »

47
00:04:58,760 --> 00:05:02,870
Il a fait son zazen pendant une heure.

48
00:05:04,300 --> 00:05:06,060
- « Ça n’a pas été dur ? »

49
00:05:06,190 --> 00:05:08,720
- « Non, non, c’était très agréable ! »

50
00:05:11,590 --> 00:05:14,655
Je te jure que c’est vrai !

51
00:05:15,314 --> 00:05:18,080
Et il n’a jamais refait zazen de sa vie.

52
00:05:19,420 --> 00:05:21,920
C’est lui qui m’a donné une leçon.

53
00:05:27,150 --> 00:05:32,332
Il y a deux ou trois ans, mon petit fils, haut comme ça

54
00:05:32,632 --> 00:05:37,460
me dit : « Papy, montre-moi le zazen ! »

55
00:05:40,840 --> 00:05:45,540
Je l’ai amené dans la pièce
où je fais le zazen en ligne par Zoom.

56
00:05:47,140 --> 00:05:50,780
Je lui dis : « Assieds-toi ici, mets les jambes comme ça. »

57
00:05:51,000 --> 00:05:52,019
Il stresse.

58
00:05:52,115 --> 00:05:55,950
Il ressemble à Étienne Zeisler [disciple de Deshimaru].

59
00:05:58,050 --> 00:06:00,410
- « Tu te mets comme ça. »

60
00:06:00,570 --> 00:06:04,130
Je corrige sa posture.

61
00:06:04,400 --> 00:06:06,485
Il a une posture super.

62
00:06:06,575 --> 00:06:08,750
Je lui dis : « Ça y est, c’est ça ! »

63
00:06:08,910 --> 00:06:10,250
- « Merci papy ! »

64
00:06:15,755 --> 00:06:17,270
Il l’a.

65
00:06:17,350 --> 00:06:19,645
Il sait ce qu’est zazen.

66
00:06:22,430 --> 00:06:25,660
Je ne sais plus à quelle question je répondais…

67
00:06:27,345 --> 00:06:30,995
Est-ce que la réalisation totale du Bouddha existe ?

68
00:06:33,520 --> 00:06:37,090
Tout est réalisation totale du Bouddha !

69
00:06:38,090 --> 00:06:42,240
Même si on est tordu, même si on ne fait pas zazen.

70
00:06:43,130 --> 00:06:47,830
C’est pour ça que parfois, il est bon de dire que zazen ne sert à rien.

71
00:06:49,410 --> 00:06:55,340
Parce quand tu fais zazen en short chez toi

72
00:06:55,760 --> 00:07:01,180
sans kesa, sans cérémonie, sans amis,
sans maître, sans disciples,

73
00:07:01,980 --> 00:07:07,360
Tu te dis « C’est les mêmes zazens
que je faisais quand j’étais débutant ! »

74
00:07:07,970 --> 00:07:12,030
Au tout début, quand j’étais chez moi et que je me disais :

75
00:07:12,030 --> 00:07:14,410
« Ah, j’ai envie de faire zazen ! »

76
00:07:16,521 --> 00:07:17,751
Je me disais…

77
00:07:17,808 --> 00:07:22,288
Voilà, enfin, voilà !
