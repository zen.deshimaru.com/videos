1
00:00:00,100 --> 00:00:02,020
Zen è zazen! 

2
00:00:02,220 --> 00:00:04,920
Zazen siede come un Buddha. 

3
00:00:05,160 --> 00:00:08,400
Prendi la posa originale del Buddha. 

4
00:00:08,620 --> 00:00:09,920
Prendi il suo cuore. 

5
00:00:10,140 --> 00:00:12,360
Siediti in questa posizione. 

6
00:00:12,660 --> 00:00:16,000
E gradualmente dimentica che siamo un corpo doloroso. 

7
00:00:16,320 --> 00:00:18,840
Tutto contorto, con una mente complicata. 

8
00:00:19,080 --> 00:00:23,120
E lasciare che avvenga la trasmutazione, l’alchimia dello zazen. 

9
00:00:23,760 --> 00:00:25,700
Solo le persone possono esercitare. 

10
00:00:26,060 --> 00:00:33,200
È la nostra evoluzione nel corso di centinaia di migliaia di anni che rende possibile praticare questo atteggiamento. 

11
00:00:33,520 --> 00:00:36,700
Equilibrio, stabilità, calma. 

12
00:00:37,000 --> 00:00:40,200
Allunga, apri, dimentica te stesso. 

13
00:00:40,480 --> 00:00:46,960
Unità con tutti gli esseri, con tutto, con l’intero universo. 

14
00:00:47,740 --> 00:00:51,760
Il maestro Kosen afferma che lo zazen è l’eredità mondiale dell’umanità. 

15
00:00:52,040 --> 00:00:54,600
Non forzeremo qualcuno a fare lo zazen. 

16
00:00:54,860 --> 00:00:56,220
E a volte non è il momento giusto. 

17
00:00:56,460 --> 00:00:57,960
Possiamo sederci ad un certo punto. 

18
00:00:58,220 --> 00:01:02,940
E perdere ciò che potrebbe significare per le nostre vite. 

19
00:01:03,220 --> 00:01:07,460
E poi in un altro momento, improvvisamente, è lì. 

20
00:01:07,680 --> 00:01:09,560
C’è una domanda e una risposta. 

21
00:01:09,760 --> 00:01:11,120
Questo è esattamente ciò che dovremmo praticare. 

22
00:01:11,360 --> 00:01:17,460
Fare lo zazen solo una volta, anche se non fa per te, se sei ricettivo, se ne hai voglia, ti dà informazioni. 

23
00:01:18,700 --> 00:01:21,140
Esistono molte pratiche diverse. 

24
00:01:21,380 --> 00:01:22,880
Il che è decisamente buono. 

25
00:01:23,160 --> 00:01:25,700
Non puoi voler mettere insieme le cose. 

26
00:01:26,000 --> 00:01:29,940
Zazen è una pratica piuttosto impegnativa. 

27
00:01:30,240 --> 00:01:32,700
Non prendi semplicemente posizione. 

28
00:01:33,040 --> 00:01:38,440
La postura e l’umore di accompagnamento sono essenziali. 

29
00:01:39,160 --> 00:01:43,900
Ma lo zazen ha la sua vita, il suo funzionamento. 

30
00:01:44,160 --> 00:01:46,220
Si chiama anche "meditazione". 

31
00:01:46,420 --> 00:01:47,980
Ma è solo una parola. 

32
00:01:48,320 --> 00:01:51,580
Lo chiamiamo "zazen", "l’atteggiamento del Buddha". 

33
00:01:51,800 --> 00:01:53,700
Sono solo parole. 

34
00:01:53,940 --> 00:01:56,180
Ma questo è l’atteggiamento che pratichiamo. 

35
00:01:56,840 --> 00:01:59,220
Non ci definiamo un insegnante o un maestro. 

36
00:01:59,500 --> 00:02:03,040
Anche se hai seguito 1, 2, 3 corsi. 

37
00:02:03,300 --> 00:02:06,140
C’è una relazione intima, segui un maestro. 

38
00:02:06,380 --> 00:02:09,460
Non lo seguiamo ovunque vada. 

39
00:02:09,780 --> 00:02:12,920
Seguiamo il suo insegnamento, la sua mente. 

40
00:02:13,200 --> 00:02:14,760
C’è lo zazen stesso. 

41
00:02:15,000 --> 00:02:22,300
Quindi, non seguire le tue cose, la tua idea. 

42
00:02:22,580 --> 00:02:25,120
Ma seguire qualcosa di più grande. 

43
00:02:25,560 --> 00:02:29,480
Qualcosa che abbraccia le generazioni. 

44
00:02:29,860 --> 00:02:34,060
Dal momento che il Buddha storico e prima di lui. 

45
00:02:34,380 --> 00:02:36,960
Ci sono altri Buddha che lo hanno preceduto. 

46
00:02:37,240 --> 00:02:40,020
Altri che erano seduti in posa. 

47
00:02:40,360 --> 00:02:42,720
Altri che hanno raggiunto il sentiero. 

48
00:02:43,500 --> 00:02:45,520
Ed è serio! 

49
00:02:45,840 --> 00:02:59,580
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

