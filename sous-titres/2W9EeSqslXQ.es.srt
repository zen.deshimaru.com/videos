1
00:00:00,000 --> 00:00:04,799
¿Cómo sincronizas el kin-hin caminando y respirando lentamente? 

2
00:00:04,799 --> 00:00:08,550
Dijiste que iríamos un paso más allá con cada exhalación. 

3
00:00:08,550 --> 00:00:15,690
Pero la mayoría de las personas respira dos veces en un solo paso. 

4
00:00:15,690 --> 00:00:19,470
Por supuesto que quiero hacer dos. 

5
00:00:19,470 --> 00:00:24,180
Las personas duplican su respiración porque no descansan entre respiraciones. 

6
00:00:24,180 --> 00:00:28,650
Al caminar lentamente, el pequeño descanso es fundamental. 

7
00:00:28,650 --> 00:00:33,059
Si descansas un poco encima de la exhalación … 

8
00:00:33,059 --> 00:00:38,210
Estás en el ritmo. 

9
00:00:38,210 --> 00:00:44,309
Dogen y su maestro Nyojo dicen …. 

10
00:00:44,309 --> 00:00:50,219
… que el mismo Buda dijo que no doblas el aliento. 

11
00:00:50,219 --> 00:00:55,050
Para hacer esto, al final de la exhalación, es necesario tomar un pequeño descanso … 

12
00:00:55,050 --> 00:01:03,239
… en el que iremos medio paso adelante … 

13
00:01:03,239 --> 00:01:09,299
… después de una breve pausa antes de inhalar y estamos en buen ritmo. 

14
00:01:09,299 --> 00:01:14,610
Nos hartamos … mucho menos. 

15
00:01:14,610 --> 00:01:20,580
Durante sesshin, amplié las ventajas familiares. 

16
00:01:20,580 --> 00:01:26,159
Ya había vivido esto en otro ejercicio con otro maestro … 

17
00:01:26,159 --> 00:01:32,670
Tenías que caminar mucho con los ojos vendados. 

18
00:01:32,670 --> 00:01:38,280
Debemos caminar sin ver nada. 

19
00:01:38,280 --> 00:01:42,270
Es agotador porque también tienes que poner tus manos en una posición … 

20
00:01:42,270 --> 00:01:45,540
… menos cómodo que en kind-hin. 

21
00:01:45,540 --> 00:01:51,180
Si haces hinchas familiares durante mucho tiempo, se cansará … 

22
00:01:51,180 --> 00:01:54,659
… otro aspecto de nosotros mismos. 

23
00:01:54,659 --> 00:01:59,360
Es un aspecto activo. Aprendí en medicina china … 

24
00:01:59,360 --> 00:02:06,920
…. que para curar la actividad excesiva, necesitas descansar. 

25
00:02:13,340 --> 00:02:18,170
Si hace hincapié en la familia durante mucho tiempo, está agotado. 

26
00:02:18,170 --> 00:02:23,230
Tiene dolor de espalda, dolor de hombro, dolor en los brazos … 

27
00:02:23,230 --> 00:02:29,330
Si caminas así durante una hora, la gente está exhausta. 

28
00:02:29,330 --> 00:02:33,290
Solo querrán una cosa: estar en zazen. 

29
00:02:33,290 --> 00:02:41,470
Qué alivio cuando estás en zazen: es complementario. 

30
00:02:41,470 --> 00:02:46,549
Te dices a ti mismo: ¡qué relajación, qué rendición! 

31
00:02:46,549 --> 00:02:51,650
Realmente quieres estar en Zazen. 

32
00:02:51,650 --> 00:02:57,920
Después de una hora en Zazen, tendrás la sensación … 

33
00:02:57,920 --> 00:03:02,720
….. hormigueo desagradable … 

34
00:03:02,720 --> 00:03:07,040
Querrá moverse y deseará que termine debido al dolor. 

35
00:03:07,040 --> 00:03:11,630
Zazen y caminar lento son completamente complementarios. 

36
00:03:11,630 --> 00:03:14,870
Pero debido a que practicamos el hin de niño por un corto tiempo, no nos damos cuenta. 

37
00:03:14,870 --> 00:03:19,430
Ahora haré que el niño rinda más durante sesshinsµ. 

38
00:03:19,430 --> 00:03:25,130
Para que a la gente le guste sentarse y soltar ciertas cosas en zazen. 

39
00:03:25,130 --> 00:03:29,350
Tu pregunta es muy buena y no nueva. 

40
00:03:29,350 --> 00:03:38,920
El Maestro Dogen planteó el problema hace 1.400 años. 

