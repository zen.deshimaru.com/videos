1
00:00:00,100 --> 00:00:02,020
Zen é zazen! 

2
00:00:02,220 --> 00:00:04,920
Zazen senta-se como um Buda. 

3
00:00:05,160 --> 00:00:08,400
Pegue a pose original de Buda. 

4
00:00:08,620 --> 00:00:09,920
Pegue o coração dele. 

5
00:00:10,140 --> 00:00:12,360
Sente-se nesta posição. 

6
00:00:12,660 --> 00:00:16,000
E gradualmente esqueça que somos um corpo doloroso. 

7
00:00:16,320 --> 00:00:18,840
Tudo torcido, com uma mente complicada. 

8
00:00:19,080 --> 00:00:23,120
E que aconteça a transmutação, a alquimia do zazen. 

9
00:00:23,760 --> 00:00:25,700
Somente pessoas podem se exercitar. 

10
00:00:26,060 --> 00:00:33,200
É nossa evolução ao longo de centenas de milhares de anos que possibilita a prática dessa atitude. 

11
00:00:33,520 --> 00:00:36,700
Equilíbrio, estabilidade, calma. 

12
00:00:37,000 --> 00:00:40,200
Esticar, abrir, esqueça-se. 

13
00:00:40,480 --> 00:00:46,960
Unidade com todos os seres, com tudo, com o universo inteiro. 

14
00:00:47,740 --> 00:00:51,760
Mestre Kosen diz que o zazen é a herança mundial da humanidade. 

15
00:00:52,040 --> 00:00:54,600
Não vamos forçar alguém a fazer zazen. 

16
00:00:54,860 --> 00:00:56,220
E às vezes não é a hora certa. 

17
00:00:56,460 --> 00:00:57,960
Podemos sentar em algum momento. 

18
00:00:58,220 --> 00:01:02,940
E perca o que isso pode significar para nossas vidas. 

19
00:01:03,220 --> 00:01:07,460
E então, em outro momento, de repente, está lá. 

20
00:01:07,680 --> 00:01:09,560
Há uma pergunta e uma resposta. 

21
00:01:09,760 --> 00:01:11,120
É exatamente isso que devemos praticar. 

22
00:01:11,360 --> 00:01:17,460
Fazer zazen apenas uma vez, mesmo que não seja o seu caso, se você é receptivo, se lhe apetecer, isso fornece informações. 

23
00:01:18,700 --> 00:01:21,140
Existem muitas práticas diferentes. 

24
00:01:21,380 --> 00:01:22,880
O que é definitivamente bom. 

25
00:01:23,160 --> 00:01:25,700
Você não pode querer juntar as coisas. 

26
00:01:26,000 --> 00:01:29,940
O zazen é uma prática bastante exigente. 

27
00:01:30,240 --> 00:01:32,700
Você não apenas se posiciona. 

28
00:01:33,040 --> 00:01:38,440
A postura e o humor acompanhante são essenciais. 

29
00:01:39,160 --> 00:01:43,900
Mas o zazen tem sua própria vida, seu próprio funcionamento. 

30
00:01:44,160 --> 00:01:46,220
É também chamado de "meditação". 

31
00:01:46,420 --> 00:01:47,980
Mas é apenas uma palavra. 

32
00:01:48,320 --> 00:01:51,580
Nós chamamos de "zazen", "atitude de Buda". 

33
00:01:51,800 --> 00:01:53,700
São apenas palavras. 

34
00:01:53,940 --> 00:01:56,180
Mas essa é a atitude que praticamos. 

35
00:01:56,840 --> 00:01:59,220
Não nos chamamos de professor ou mestre. 

36
00:01:59,500 --> 00:02:03,040
Mesmo se você tiver tomado 1, 2, 3 cursos. 

37
00:02:03,300 --> 00:02:06,140
Existe um relacionamento íntimo, você segue um mestre. 

38
00:02:06,380 --> 00:02:09,460
Não o seguimos aonde quer que ele vá. 

39
00:02:09,780 --> 00:02:12,920
Seguimos seus ensinamentos, sua mente. 

40
00:02:13,200 --> 00:02:14,760
Existe o próprio zazen. 

41
00:02:15,000 --> 00:02:22,300
Então, não siga suas próprias coisas, suas próprias idéias. 

42
00:02:22,580 --> 00:02:25,120
Mas seguir algo maior. 

43
00:02:25,560 --> 00:02:29,480
Algo que abrange gerações. 

44
00:02:29,860 --> 00:02:34,060
Desde o histórico Buda e antes dele. 

45
00:02:34,380 --> 00:02:36,960
Existem outros budas que o precederam. 

46
00:02:37,240 --> 00:02:40,020
Outros que se sentaram na pose. 

47
00:02:40,360 --> 00:02:42,720
Outros que alcançaram o caminho. 

48
00:02:43,500 --> 00:02:45,520
E isso é sério! 

49
00:02:45,840 --> 00:02:59,580
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

