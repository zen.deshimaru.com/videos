1
00:00:00,280 --> 00:00:03,640
Daily life 

2
00:00:03,840 --> 00:00:06,460
One day… 

3
00:00:07,200 --> 00:00:12,320
During the day we have to stop what we do regularly. 

4
00:00:13,040 --> 00:00:15,320
Stop his job. 

5
00:00:15,780 --> 00:00:18,780
And come on. 

6
00:00:19,020 --> 00:00:22,220
In his work, in everyday life, you find the mind. 

7
00:00:22,560 --> 00:00:24,720
It only takes two breaths. 

8
00:00:25,060 --> 00:00:26,340
You work at your desk … 

9
00:00:26,680 --> 00:00:28,820
Just straighten it out. 

10
00:00:29,340 --> 00:00:32,760
And breathe. 

11
00:00:41,120 --> 00:00:49,260
And to immediately return to his being. 

12
00:00:50,960 --> 00:00:54,140
I knew a community called Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
They work very hard. 

14
00:00:57,580 --> 00:01:00,400
They work the land. 

15
00:01:00,660 --> 00:01:02,660
They build their house. 

16
00:01:02,880 --> 00:01:04,540
They do a lot of manual labor. 

17
00:01:04,740 --> 00:01:06,040
They work all day. 

18
00:01:06,300 --> 00:01:07,500
They get up at 5 a.m. 

19
00:01:07,720 --> 00:01:08,820
They say their prayers. 

20
00:01:09,080 --> 00:01:10,840
They are having breakfast. 

21
00:01:11,160 --> 00:01:12,420
They go to work. 

22
00:01:12,780 --> 00:01:15,820
A bell rings every hour. 

23
00:01:16,180 --> 00:01:18,060
Everyone has to stop working. 

24
00:01:18,720 --> 00:01:25,520
And refocus for a minute. 

25
00:01:26,060 --> 00:01:28,960
Each in their own way. 

26
00:01:34,080 --> 00:01:36,040
And then they go back. 

27
00:01:36,560 --> 00:01:39,160
So there is not only zazen. 

28
00:01:39,540 --> 00:01:42,100
There are many ways to get back to yourself. 

29
00:01:42,640 --> 00:01:45,220
The daily life of the Zen monks … 

30
00:01:45,540 --> 00:01:48,820
We live our lives for zazen. 

31
00:01:49,080 --> 00:01:52,150
To collect this consciousness, this knowledge. 

32
00:01:52,440 --> 00:01:53,580
That energy. 

33
00:01:54,880 --> 00:01:58,780
At the time of the zazen experience. 

34
00:01:58,900 --> 00:02:03,120
It is not a question of giving up your life for Zazen. 

35
00:02:03,380 --> 00:02:05,400
In a sectarian spirit. 

36
00:02:05,740 --> 00:02:07,680
It is not an ideology. 

37
00:02:07,900 --> 00:02:10,380
It is not "-ism". 

38
00:02:10,840 --> 00:02:14,780
It is, "I live my life in such a way that I can have this experience." 

39
00:02:15,060 --> 00:02:16,560
"As deep as possible." 

40
00:02:16,900 --> 00:02:18,980
This is an experience of zazen meditation. 

41
00:02:19,220 --> 00:02:25,060
When you are in zazen, you have the opportunity to change your whole life. 

42
00:02:25,400 --> 00:02:27,900
That’s what zazen is all about! 

43
00:02:28,340 --> 00:02:31,980
It is no small way to feel good. 

44
00:02:32,500 --> 00:02:35,040
Zen in everyday life … 

45
00:02:35,640 --> 00:02:39,200
Even in profit and work … 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku", (without goal or spirit of profit) … 

47
00:02:41,760 --> 00:02:43,140
that is the secret. 

48
00:02:43,340 --> 00:02:46,320
In other words, stay focused on your essence. 

49
00:02:46,540 --> 00:02:48,880
Then we can get everything. 

50
00:02:49,080 --> 00:02:50,360
We have everything. 

51
00:02:50,880 --> 00:02:55,280
What is the difference between the life of a master and that of an ordinary being? 

52
00:02:55,660 --> 00:02:59,520
There is no such thing as ordinary. 

53
00:02:59,840 --> 00:03:05,780
There are only extraordinary creatures. 

54
00:03:06,660 --> 00:03:14,900
The bad thing is that people don’t know they are extraordinary. 

55
00:03:16,420 --> 00:03:22,260
I don’t like the phrase "just be"! 

