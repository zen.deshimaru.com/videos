1
00:00:08,980 --> 00:00:16,230
Olá ! Vou lhe explicar como sentar-se para praticar a meditação Zen,
2
00:00:17,200 --> 00:00:19,180
chamado zazen.

3
00:00:19,180 --> 00:00:22,829
O material necessário é muito simples, um ser humano.

4
00:00:23,320 --> 00:00:28,349
e uma almofada de meditação. Se você tiver uma almofada

5
00:00:29,230 --> 00:00:33,689
bastante almofadado, tudo bem, e se não, você faz um com um cobertor.

6
00:00:35,320 --> 00:00:39,060
ou um saco de dormir, por exemplo, ele deve ser suficientemente firme.

7
00:00:57,490 --> 00:01:04,120
Explico a postura da meditação Zen.

8
00:01:05,560 --> 00:01:08,940
Antes de mais nada, saber que é uma postura.

9
00:01:09,440 --> 00:01:10,880
base

10
00:01:11,240 --> 00:01:13,440
imóvel e silencioso.

11
00:01:14,120 --> 00:01:16,660
A particularidade desta postura

12
00:01:17,300 --> 00:01:19,300
é que nós o praticamos

13
00:01:19,400 --> 00:01:21,400
de frente para a parede.

14
00:01:21,800 --> 00:01:27,460
Aqui, é claro, eu estou diante da câmera para ensinar-lhes.

15
00:01:27,960 --> 00:01:34,640
O que você tem que fazer é sentar-se em uma almofada, como já expliquei, que tem que ser

16
00:01:35,000 --> 00:01:37,000
suficientemente almofadado, suficientemente duro.

17
00:01:37,300 --> 00:01:44,060
Você se senta bem na parte inferior da almofada, não se sente na borda que você pode deslizar,

18
00:01:44,440 --> 00:01:48,360
e você deve cruzar suas pernas o máximo que puder.

19
00:01:48,820 --> 00:01:56,220
Você pode cruzá-los assim, assim, assim, assim, é chamado de meio-lótus,

20
00:01:56,660 --> 00:01:59,410
se você colocar sua outra perna aqui, ela se chama lótus.

21
00:02:02,860 --> 00:02:07,620
O importante é que seus joelhos se ajoelhem no chão,

22
00:02:08,220 --> 00:02:09,660
para empurrar,

23
00:02:10,789 --> 00:02:13,420
para exercer pressão no joelho sobre o piso.

24
00:02:13,880 --> 00:02:20,079
Então, como fazemos isso? Faça-o sem nenhum esforço físico,

25
00:02:20,720 --> 00:02:25,240
nada de muscular. Postura de meditação Zen, zazen,

26
00:02:25,900 --> 00:02:27,780
é uma postura onde você tem que se soltar.

27
00:02:28,300 --> 00:02:33,000
Para que seus joelhos se ajoelhem no chão

28
00:02:33,920 --> 00:02:37,360
você vai inclinar ligeiramente a pélvis

29
00:02:38,440 --> 00:02:42,220
para frente, a parte superior da pélvis avança,

30
00:02:42,760 --> 00:02:46,300
o cóccix vai um pouco para trás.

31
00:02:46,760 --> 00:02:48,760
Mostrarei a você em perfil

32
00:02:55,700 --> 00:03:03,040
Minha pélvis não é colocada assim, como se eu estivesse dirigindo um carro, por exemplo,

33
00:03:03,260 --> 00:03:08,700
você vê aqui meus joelhos subirem para trás porque meu centro de gravidade

34
00:03:08,900 --> 00:03:15,700
foi para trás do meu corpo. Quero que meu centro de gravidade vá na frente e faço isso com minha pélvis.

35
00:03:15,940 --> 00:03:24,300
Naturalmente, o centro de gravidade passa na frente com a gravidade, meus joelhos vão para o chão.

36
00:03:24,300 --> 00:03:30,860
Ali, é isso que você tem que estabelecer primeiro na postura,

37
00:03:31,160 --> 00:03:37,040
um assento no qual seus joelhos vão para o chão.

38
00:03:37,480 --> 00:03:40,200
Quando você percebeu que a sessão

39
00:03:40,800 --> 00:03:45,500
da pélvis, que é ligeiramente inclinada para a frente.

40
00:03:45,900 --> 00:03:49,280
você vai endireitar a coluna vertebral o máximo possível

41
00:03:50,200 --> 00:03:52,860
até o topo da cabeça.

42
00:03:54,580 --> 00:04:02,780
Vamos assumir que a coluna vai até o topo. Como se eu colocasse um gancho

43
00:04:05,060 --> 00:04:10,500
e para cima eu vou. O que significa que eu estico meu pescoço, encolho o queixo,

44
00:04:11,150 --> 00:04:15,699
Veja, eu não tenho a cabeça nos ombros, não tenho um palpite...

45
00:04:16,849 --> 00:04:18,849
Sou o mais direto que posso ser.

46
00:04:19,520 --> 00:04:22,000
Eu lhe mostrarei em perfil

47
00:04:23,300 --> 00:04:28,040
Eu não sou assim, não sou assim...

48
00:04:28,560 --> 00:04:35,780
ou dobrado ou o que quer que seja, sou o mais reto que posso ser.

49
00:04:38,800 --> 00:04:43,820
Quando você consegue assentar direito, você não se move.

50
00:04:45,320 --> 00:04:47,160
Eu vou continuar.

51
00:04:47,380 --> 00:04:49,280
a mão esquerda

52
00:04:49,540 --> 00:04:51,540
os dedos são unidos

53
00:04:52,250 --> 00:04:57,220
os dedos da mão direita estão unidos e eu vou sobrepor meus dedos da mão esquerda

54
00:04:58,070 --> 00:05:00,070
nos dedos da mão direita

55
00:05:01,490 --> 00:05:03,680
Meus polegares se encontram

56
00:05:05,730 --> 00:05:08,329
Sobrepostos nos dedos da mão esquerda,

57
00:05:09,630 --> 00:05:11,630
Eu formo uma linha horizontal.

58
00:05:12,690 --> 00:05:20,059
A borda de minhas mãos desde meu pulso até aqui, a borda de minhas mãos

59
00:05:21,030 --> 00:05:23,929
você vai aplicá-lo na base do abdômen.

60
00:05:25,280 --> 00:05:27,820
para fazer isto,

61
00:05:29,580 --> 00:05:33,440
use algo que lhe permita unir as mãos.

62
00:05:34,160 --> 00:05:36,280
de tal forma que você possa

63
00:05:36,700 --> 00:05:38,620
deixar sair completamente

64
00:05:38,880 --> 00:05:43,300
a tensão nos ombros a postura é uma postura onde você tem que deixar ir

65
00:05:44,280 --> 00:05:50,089
Solte a tensão, você vê que meus cotovelos estão livres, eu tenho estado assim ou assim por muito tempo,

66
00:05:51,380 --> 00:05:53,940
silenciosamente. Mas eu não faço um esforço

67
00:05:54,540 --> 00:06:00,940
para segurar minhas mãos, o esforço é para manter meus polegares na horizontal.

68
00:06:01,220 --> 00:06:03,220
uma vez que percebi

69
00:06:03,750 --> 00:06:05,750
estes pontos de postura

70
00:06:06,600 --> 00:06:08,600
Eu não estou me movendo,

71
00:06:09,160 --> 00:06:11,160
Isso é postura física.

72
00:06:20,060 --> 00:06:27,420
Depois desta primeira parte sobre a postura física, segunda parte: a atitude mental.

73
00:06:28,380 --> 00:06:32,340
Uma vez que estou em uma postura de meditação Zen, zazen

74
00:06:33,620 --> 00:06:37,960
de frente para a parede, o que eu faço, o que eu faço?

75
00:06:39,720 --> 00:06:42,260
No silêncio desta postura,

76
00:06:43,770 --> 00:06:48,380
você vai perceber que você pensa,

77
00:06:49,350 --> 00:06:51,769
ou mesmo que você não consegue parar de pensar

78
00:06:52,290 --> 00:06:57,469
e há milhares de pensamentos vindo, vindo, vindo, vindo.

79
00:06:57,980 --> 00:07:02,420
Você está prestes a me dizer que é um fenômeno natural...

80
00:07:03,500 --> 00:07:05,500
Você não pode controlá-lo.

81
00:07:05,940 --> 00:07:11,900
O que vamos fazer, durante a meditação zen, vamos deixar passar nossos pensamentos.

82
00:07:12,740 --> 00:07:15,580
ou seja, nesta imobilidade..,

83
00:07:16,040 --> 00:07:20,440
seremos capazes de observar o que pensamos,

84
00:07:21,040 --> 00:07:25,960
você observará seus pensamentos e estes pensamentos em vez de entretê-los,

85
00:07:26,360 --> 00:07:32,860
em vez de cultivá-los, você vai deixá-los passar.

86
00:07:33,160 --> 00:07:39,220
Portanto, não se trata de recusar nada, mais uma vez, trata-se de deixar ir, de deixar ir.

87
00:07:40,080 --> 00:07:42,400
Como passar

88
00:07:43,260 --> 00:07:45,260
atenção, concentração,

89
00:07:46,080 --> 00:07:47,860
aqui mesmo, agora mesmo,

90
00:07:48,240 --> 00:07:50,240
em sua respiração.

91
00:07:55,930 --> 00:07:58,680
Respirar em Zen é respiração abdominal.

92
00:08:00,460 --> 00:08:08,320
Você respira através, através do nariz, e respira suavemente,

93
00:08:09,640 --> 00:08:13,780
lentamente, obviamente em silêncio, a meditação silenciosa.

94
00:08:15,180 --> 00:08:20,180
A característica desta respiração é que você vai focalizar sua atenção

95
00:08:20,540 --> 00:08:24,120
no lado da exalação.

96
00:08:24,420 --> 00:08:26,420
Nós inspiramos

97
00:08:26,620 --> 00:08:33,680
pelo nariz, silenciosamente, ser generoso, encher

98
00:08:34,300 --> 00:08:41,880
e o rosto da exalação quando se vai exalar todo o ar que se faz lentamente,

99
00:08:42,340 --> 00:08:46,940
lentamente, como se fosse ficar cada vez mais longo

100
00:08:56,700 --> 00:09:00,140
Quando você estiver no final de sua expiração

101
00:09:00,880 --> 00:09:02,380
você aspira

102
00:09:02,850 --> 00:09:06,340
e novamente você expira

103
00:09:07,520 --> 00:09:12,020
e você está levando mais tempo, mais devagar, mais fundo,

104
00:09:12,200 --> 00:09:15,760
uma exalação mais profunda, em forma de onda novamente.

105
00:09:16,280 --> 00:09:18,280
do outro lado do oceano

106
00:09:18,560 --> 00:09:20,560
vindo para a praia

107
00:09:22,580 --> 00:09:24,580
Tudo isso com o nariz.

108
00:09:30,060 --> 00:09:33,100
Se você se concentrar aqui e agora

109
00:09:33,400 --> 00:09:40,880
sobre os pontos da postura, os pontos físicos, portanto a atenção, uma atenção aqui e agora em seu corpo

110
00:09:41,420 --> 00:09:48,960
se você se concentrar aqui e agora, ao mesmo tempo, em não seguir seus pensamentos,

111
00:09:49,820 --> 00:09:52,640
voltando aos pontos que a postura,

112
00:09:52,980 --> 00:09:55,880
se você se concentrar aqui e agora

113
00:09:56,460 --> 00:10:02,320
no estabelecimento de uma respiração harmoniosa, indo e vindo, e pouco a pouco

114
00:10:02,900 --> 00:10:06,180
do que o tempo de expiração,

115
00:10:06,800 --> 00:10:10,620
você sai do ar está ficando cada vez mais longo.

116
00:10:11,600 --> 00:10:16,240
Você não terá tempo para pensar em... o que você quer!

117
00:10:16,800 --> 00:10:22,940
Você está aqui e agora em uma postura de zazen.

118
00:10:23,520 --> 00:10:30,360
Em postura, nem o corpo nem a matéria, em postura. Lembro-lhes de pressionar,

119
00:10:30,660 --> 00:10:34,420
você aplica pressão de seus joelhos no chão,

120
00:10:34,680 --> 00:10:38,840
você empurra a terra com seus joelhos

121
00:10:38,900 --> 00:10:40,980
e você se endireita

122
00:10:41,500 --> 00:10:43,500
na medida do possível, a coluna vertebral

123
00:10:44,089 --> 00:10:48,999
até a parte superior do crânio como se fosse empurrar o céu com a cabeça.

124
00:10:49,760 --> 00:10:55,270
Mão esquerda na mão direita com os polegares horizontais na borda das mãos

125
00:10:55,780 --> 00:10:58,440
contra a parte inferior do abdômen.

126
00:10:58,840 --> 00:11:00,820
Estou imóvel.

127
00:11:01,190 --> 00:11:03,160
de frente para a parede

128
00:11:03,170 --> 00:11:05,170
De frente para a parede não há nada para ver especialmente,

129
00:11:05,360 --> 00:11:11,169
o que vou fazer em vez disso é virar meu olhar para dentro,

130
00:11:11,959 --> 00:11:13,959
assim os sentidos,

131
00:11:14,410 --> 00:11:16,959
Não vou mais usá-los durante esta meditação.

132
00:11:17,900 --> 00:11:23,360
Estou de frente para a parede, então não há nada para ver, então é como se

133
00:11:24,080 --> 00:11:28,640
Eu pego minha visão, minha visão, eu a pego, eu a coloco na minha frente, eu não me importo mais.

134
00:11:29,020 --> 00:11:33,180
Durante a meditação, você meditará com a visão,

135
00:11:34,160 --> 00:11:37,240
45° diagonalmente para baixo

136
00:11:38,300 --> 00:11:46,920
Você pode meditar com os olhos fechados, mas se você mantiver os olhos semi-abertos, você mantém contato com a realidade.

137
00:11:47,340 --> 00:11:49,980
Não vá especialmente em seus pensamentos.

138
00:11:50,320 --> 00:11:54,380
Como estamos em silêncio, também não utilizamos nossa audiência,

139
00:11:54,600 --> 00:11:58,980
Portanto, a audiência, não há necessidade disso, não há música, não há nada.

140
00:11:59,260 --> 00:12:03,740
e já vimos que estamos imobilizados.

141
00:12:04,260 --> 00:12:09,960
Pouco a pouco, de repente, nos isolamos das exigências do mundo exterior.

142
00:12:10,400 --> 00:12:12,680
nos livramos de tudo isso.

143
00:12:13,140 --> 00:12:18,920
Meditação, você está sentado fisicamente, é o corpo que está em postura,

144
00:12:19,910 --> 00:12:26,350
você se desliga do mundo exterior, coloca tudo no modo avião.

145
00:12:26,920 --> 00:12:33,740
Não medite com seu telefone celular ao seu lado.

146
00:13:09,580 --> 00:13:12,000
Para terminar uma dica para iniciantes,

147
00:13:12,660 --> 00:13:17,740
não é fácil ser assim, confinado agora,

148
00:13:18,140 --> 00:13:21,720
meditando em frente a uma parede,

149
00:13:22,820 --> 00:13:25,940
mas, uh, é realmente uma boa ajuda.

150
00:13:26,220 --> 00:13:28,640
Aconselho-o realmente a praticá-lo,

151
00:13:28,900 --> 00:13:36,940
para experimentar e você verá que, em vez de ter pensamentos que podem não ser muito brilhantes,

152
00:13:37,260 --> 00:13:43,920
não muito feliz, em vez de estar de mau humor, em vez de sentir tensão, estresse,

153
00:13:44,560 --> 00:13:48,569
tudo isso é normal, com este trabalho sobre o corpo.

154
00:13:49,200 --> 00:13:55,500
a atitude da mente que você deixa ir, você se distancia de tudo o que está acontecendo com você.

155
00:13:55,940 --> 00:13:58,240
você vai ver que vai se acalmar

156
00:13:58,560 --> 00:13:59,900
fisicamente

157
00:14:00,100 --> 00:14:01,480
emocionalmente

158
00:14:01,660 --> 00:14:04,820
a respiração, tudo isso, vai ajudar a soltar...

159
00:14:05,280 --> 00:14:09,880
ajudará profundamente você e sua família se você viver com alguém.

160
00:14:10,340 --> 00:14:14,480
Portanto, não duvide em praticá-lo. Obrigado.