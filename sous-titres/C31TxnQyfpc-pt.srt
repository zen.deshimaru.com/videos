1
00:00:00,010 --> 00:00:11,610
Os pontos-chave são: a bacia,

2
00:00:11,740 --> 00:00:17,895
a quinta vértebra lombar,

3
00:00:18,635 --> 00:00:24,900
a primeira vértebra dorsal, a que se projeta.

4
00:00:26,940 --> 00:00:31,820
E então você vê a morfologia, todos são diferentes.

5
00:00:31,960 --> 00:00:38,620
Digamos que ela deveria esticar um pouco mais o pescoço.

6
00:00:41,210 --> 00:00:48,145
Você sempre tem que acertar dois pontos.

7
00:00:48,645 --> 00:00:52,262
Exceto quando é a parte de cima do crânio.

8
00:00:52,602 --> 00:00:55,460
Um yin e um yang.

9
00:00:55,640 --> 00:00:58,675
Eu me ponho à vontade, eu me ajoelho.

10
00:00:59,855 --> 00:01:03,020
Vou tocar nesta mesma vértebra.

11
00:01:08,280 --> 00:01:09,815
Vou tocar o queixo com dois dedos.

12
00:01:09,975 --> 00:01:11,790
Eu não estou fazendo isso com ele.

13
00:01:14,170 --> 00:01:16,825
Estamos dando instruções.

14
00:01:16,855 --> 00:01:19,540
Mas quando elas são realmente bem dadas,

15
00:01:19,620 --> 00:01:21,635
se deixarmos a pessoa manter seu foco,

16
00:01:21,865 --> 00:01:24,132
tem um efeito muito profundo.

17
00:01:25,602 --> 00:01:30,670
Portanto, vou empurrar os dois pontos opostos.

18
00:01:30,753 --> 00:01:35,976
Também podemos fazer isso assim.

19
00:01:36,156 --> 00:01:39,650
Eu sei que ela teve uma operação...

20
00:01:41,560 --> 00:01:43,680
Também podemos fazer isso assim.

21
00:01:43,785 --> 00:01:46,570
O Sensei [Deshimaru] costumava fazer muito isso.

22
00:01:48,030 --> 00:01:53,480
Você pode atingir um ponto específico em que a pessoa tem fraquezas.

23
00:01:53,740 --> 00:01:56,860
Por exemplo, para o estômago ou para o fígado.

24
00:02:00,470 --> 00:02:03,900
Sempre olhe para a curvatura.

25
00:02:06,820 --> 00:02:10,385
Muitas vezes, ela é super ou subestimada.

26
00:02:10,575 --> 00:02:15,140
Não esqueça que você tem um kyosaku na mão.

27
00:02:18,340 --> 00:02:23,950
Não há como começar a remendar.

28
00:02:25,680 --> 00:02:30,245
Você tem que manter seu kyosaku.

29
00:02:32,525 --> 00:02:36,740
Podemos fazer isso com o kyosaku, isso não incomoda.

30
00:02:36,875 --> 00:02:39,895
Nós também podemos fazer isso.

31
00:02:40,010 --> 00:02:45,140
Se a pessoa estiver muito pressionada.

32
00:02:48,195 --> 00:02:50,880
Nós também podemos fazer isso.

33
00:02:52,790 --> 00:02:56,313
Você tem que gerar uma correção que a pessoa tomará.

34
00:02:56,405 --> 00:02:59,930
É importante fazer isso também.

35
00:03:04,960 --> 00:03:08,450
É muito delicado e eu não perturbo a meditação.

36
00:03:11,440 --> 00:03:13,570
Também podemos utilizar o kyosaku.

37
00:03:16,235 --> 00:03:19,825
Vamos tirar o lado largo para baixo.

38
00:03:20,330 --> 00:03:23,325
Ajoelhar-se.

39
00:03:24,216 --> 00:03:27,264
E nós vamos colocá-lo no arco.

40
00:03:29,334 --> 00:03:31,587
Isso lhe dará uma indicação.

41
00:03:32,747 --> 00:03:37,940
Muito negativo, porque você se sente como se não estivesse reto de forma alguma.

42
00:03:38,150 --> 00:03:41,930
Porque o bastão é reto, e a coluna não é reta.

43
00:03:48,600 --> 00:03:50,800
A coluna nunca é reta.

44
00:03:51,130 --> 00:03:52,995
Tem uma ligeira curvatura.

45
00:03:53,175 --> 00:03:59,240
Quando você veste isto, você está fadado a se sentir um pouco retorcido.

46
00:03:59,400 --> 00:04:06,705
Se você o colocar de costas...

47
00:04:06,955 --> 00:04:09,490
É um pouco espartano.

48
00:04:13,940 --> 00:04:16,660
Você tem uma postura muito agradável, Cristina!

49
00:04:22,280 --> 00:04:25,430
Ele tem uma ótima postura.

50
00:04:28,960 --> 00:04:32,320
Há pequenas coisas que você não percebe.

51
00:04:32,480 --> 00:04:34,340
Por exemplo, o polegar ali.

52
00:04:36,040 --> 00:04:39,820
Deve ser reto.

53
00:04:41,850 --> 00:04:44,710
É algo que ele não percebe.

54
00:04:44,890 --> 00:04:50,520
Mas você vai continuar por um ano, dois anos, e seus polegares vão ficar bem.

55
00:04:50,855 --> 00:04:53,735
É a primeira coisa que eu vejo.

56
00:04:55,180 --> 00:04:58,900
Certifique-se de que seus polegares estejam bem encostados ao umbigo.

57
00:05:10,260 --> 00:05:12,600
Um pouco demais no fundo da minha mente.

58
00:05:14,440 --> 00:05:20,300
A cabeça está indo um pouco adiante.

59
00:05:28,920 --> 00:05:32,970
Eu estou totalmente interessado em osteopatia.

60
00:05:34,420 --> 00:05:36,260
Muito bem!

61
00:05:38,315 --> 00:05:42,875
Relaxe um pouco aí.

62
00:05:53,292 --> 00:05:54,862
Muito bom!

63
00:05:56,060 --> 00:06:00,290
Quando você faz gassho, faça-o corretamente desta maneira.

64
00:06:04,605 --> 00:06:06,335
A postura é linda.

65
00:06:08,920 --> 00:06:11,120
Um pouco acobertado demais.

66
00:06:11,225 --> 00:06:14,765
Saiam daqui.

67
00:06:24,537 --> 00:06:28,737
Nesse ponto, porque queremos aguentar,

68
00:06:35,023 --> 00:06:37,393
Não, não, não se mexa!

69
00:06:37,591 --> 00:06:38,591
Voilà!

70
00:06:38,780 --> 00:06:39,780
Perfeito!

71
00:06:45,450 --> 00:06:49,410
Não esqueça que é mão esquerda na mão direita.

72
00:06:51,200 --> 00:06:52,950
Relaxe suas mãos!

73
00:06:53,010 --> 00:06:55,540
Relaxe seus dedos.

74
00:06:56,150 --> 00:06:58,530
Colocam-se um sobre o outro.

75
00:07:03,395 --> 00:07:06,015
É um pouco tenso, mas ainda assim...

76
00:07:08,797 --> 00:07:09,947
Aqui.

77
00:07:16,500 --> 00:07:19,185
O que vou fazer então...

78
00:07:19,275 --> 00:07:21,870
Estou vendo muita tensão. É normal.

79
00:07:21,910 --> 00:07:24,435
Porque, no início, temos que fazê-lo em vigor.

80
00:07:24,585 --> 00:07:27,130
Você faz isso.

81
00:07:30,560 --> 00:07:33,810
As mãos estão muito boas agora.

82
00:07:34,930 --> 00:07:38,150
Relaxe, eles não têm que estar muito tensos.

83
00:07:39,690 --> 00:07:40,470
Flexível.

84
00:07:40,560 --> 00:07:42,110
Isso é bom, ótimo!

85
00:07:48,975 --> 00:07:54,395
Eu, eu colocaria um pouco de ênfase na quinta vértebra.

86
00:08:03,997 --> 00:08:07,027
Veja, estou me sentindo um pouco tenso.

87
00:08:10,253 --> 00:08:12,099
Muito bem!

88
00:08:12,829 --> 00:08:15,839
Solte seus cotovelos.

89
00:08:19,849 --> 00:08:27,759
Sensei, não em iniciantes, mas ele costumava fazer muito isso.

90
00:08:27,894 --> 00:08:31,684
Definitivamente, ele estava empurrando com força.

91
00:08:33,682 --> 00:08:37,282
Por enquanto, não há necessidade.

92
00:08:37,941 --> 00:08:39,471
Muito bem.

93
00:08:40,660 --> 00:08:42,680
Você pode começar a respirar.

94
00:08:42,790 --> 00:08:44,875
Respire fundo.

95
00:08:45,065 --> 00:08:46,577
Siga seu fôlego.

96
00:08:46,727 --> 00:08:48,010
Aproveite.

97
00:08:54,425 --> 00:08:55,625
Isso é muito bom.

98
00:08:55,717 --> 00:08:58,987
Este é seu primeiro sesshin?

99
00:09:00,103 --> 00:09:02,613
Bravo!

100
00:09:02,853 --> 00:09:04,530
Deixe-me perguntar-lhe uma coisa:

101
00:09:04,608 --> 00:09:07,268
Para respirar,

102
00:09:07,380 --> 00:09:12,640
às vezes eu o sugo e não sei como parar...

103
00:09:20,830 --> 00:09:22,820
Desde que você se sinta bem...

104
00:09:26,310 --> 00:09:29,775
Esse é o meu ponto, há outros métodos...

105
00:09:29,935 --> 00:09:32,860
Mas em zazen, para respirar.

106
00:09:33,060 --> 00:09:36,780
Você tem que se sentir bem, não force.

107
00:09:37,130 --> 00:09:40,530
Desde que você se sinta bem, você continua.

108
00:09:45,380 --> 00:09:48,570
Você recupera o fôlego no momento em que o sente.

109
00:09:48,731 --> 00:09:51,461
É o seu corpo que tem que ditar isso.

110
00:10:25,390 --> 00:10:27,310
Postura em geral

111
00:11:05,935 --> 00:11:08,115
Muito bem!

112
00:11:15,740 --> 00:11:18,100
Você tem que olhar para os pontos principais:

113
00:11:18,190 --> 00:11:21,200
quinta vértebra lombar, primeira dorsal,

114
00:11:21,266 --> 00:11:24,790
cabeça, braços não muito apertados.

115
00:11:26,070 --> 00:11:31,060
Podemos ajudar a pessoa por uma posição das mãos.

116
00:11:31,240 --> 00:11:34,280
Eu gostaria de ver uma postura de kin-hin.

117
00:11:41,400 --> 00:11:44,925
Não há nada além de uma boa postura.

118
00:11:46,485 --> 00:11:50,500
Ele tem uma postura muito agradável, muito natural.

119
00:11:51,230 --> 00:11:59,200
Exceto por um detalhe: o dedo mindinho deve estar apertado.

120
00:11:59,785 --> 00:12:01,855
As pessoas muitas vezes se esquecem de apertá-la.

121
00:12:01,967 --> 00:12:03,822
Sensei sempre disse:

122
00:12:03,982 --> 00:12:08,977
os dois dedinhos têm que estar muito apertados.

123
00:12:15,043 --> 00:12:17,623
Eu até corrigi da última vez...

124
00:12:19,766 --> 00:12:22,006
Muito bem, bravo!

125
00:12:24,018 --> 00:12:27,238
Muitas vezes há pessoas com posturas como essa.

126
00:12:27,424 --> 00:12:28,974
Eles devem ser corrigidos?

127
00:12:29,047 --> 00:12:30,387
Deve ser corrigido.

128
00:12:30,510 --> 00:12:33,615
Por exemplo, Pierre, tome a postura...

129
00:12:33,920 --> 00:12:38,170
Já notei algo, veremos se você faz...

130
00:12:46,127 --> 00:12:50,227
A mão é um pouco assim...

131
00:12:55,366 --> 00:12:57,176
Relaxe um pouco.

132
00:12:59,735 --> 00:13:04,130
É realmente necessário que seja estratificado.

133
00:13:13,740 --> 00:13:15,870
Às vezes você pode cometer um erro.

134
00:13:15,935 --> 00:13:18,195
Se o consertarmos...

135
00:13:18,317 --> 00:13:20,887
Aperte um pouco os dedos pequenos...

136
00:13:23,093 --> 00:13:27,443
O Sensei sempre mostrou energia em ambos os dedos.

137
00:13:27,821 --> 00:13:30,621
Fazemos assim, depois assim.

138
00:13:32,250 --> 00:13:35,010
E depois esta nós colocamos em cima.

139
00:13:36,000 --> 00:13:38,040
Bonito e perpendicular.

140
00:13:38,310 --> 00:13:42,130
Quando você abre suas mãos, elas estão paralelas ao chão.

141
00:13:44,370 --> 00:13:46,580
Você não vai querer acabar assim...

142
00:13:47,610 --> 00:13:48,830
Isso é bom. Isso é bom.

143
00:13:48,990 --> 00:13:50,410
É perfeito.
