1
00:00:03,300 --> 00:00:07,060
Que horas são? 

2
00:00:07,240 --> 00:00:11,780
A hora é só agora. 

3
00:00:12,080 --> 00:00:15,080
O Zen está "aqui e agora". 

4
00:00:15,340 --> 00:00:16,820
É importante. 

5
00:00:17,220 --> 00:00:25,160
Mas entender o passado é um truque extraordinário para entender o presente. 

6
00:00:25,580 --> 00:00:31,640
E entender o futuro também é muito importante. 

7
00:00:32,460 --> 00:00:34,780
Como você passa no tempo? 

8
00:00:35,360 --> 00:00:38,400
O tempo é uma questão de frequência. 

9
00:00:38,700 --> 00:00:43,640
Quanto maior a frequência, mais rápido o tempo, isso é tudo. 

10
00:00:44,100 --> 00:00:49,760
A consciência também evolui de acordo com as frequências. 

11
00:00:59,700 --> 00:01:03,440
Caro é apenas uma impressão material. 

12
00:01:03,820 --> 00:01:06,000
É uma delícia. 

13
00:01:06,400 --> 00:01:09,100
E o zazen? 

14
00:01:09,400 --> 00:01:16,700
Para um amante do zazen, o tempo zazen é muito precioso. 

15
00:01:17,120 --> 00:01:22,560
Chegamos ao dojo e só temos uma hora e meia, até duas horas. 

16
00:01:23,020 --> 00:01:27,720
Ser capaz de viver nesta dimensão. 

17
00:01:28,000 --> 00:01:31,520
E colocar as coisas em seu lugar. 

18
00:01:31,780 --> 00:01:35,440
Você precisa economizar seu tempo de zazen. 

19
00:01:35,540 --> 00:01:38,760
Demora um pouco para chegar ao fundo. 

20
00:01:39,000 --> 00:01:40,760
E estar em boa postura. 

21
00:01:41,060 --> 00:01:43,960
O tempo está relacionado ao karma? 

22
00:01:44,720 --> 00:01:46,920
O passado só existe agora. 

23
00:01:47,280 --> 00:01:57,240
Cada "agora" corresponde a uma frequência específica no passado com o seu "agora". 

24
00:01:57,620 --> 00:02:00,900
Seu passado está mudando constantemente. 

25
00:02:01,000 --> 00:02:10,700
Mas quando você vai para o seu passado, você esquece o seu presente. 

26
00:02:11,040 --> 00:02:13,560
E seu presente se torna o passado. 

27
00:02:13,900 --> 00:02:17,720
E você entra em algo que se torna cada vez mais complicado. 

28
00:02:17,940 --> 00:02:19,340
No karma. 

29
00:02:19,820 --> 00:02:24,160
"A culpa é do meu carma …" 

30
00:02:25,240 --> 00:02:27,960
"Meus pais, minhas coisas …" 

31
00:02:28,280 --> 00:02:30,440
"Minha vida é complicada …" 

32
00:02:30,700 --> 00:02:32,380
"Meus ancestrais eram alcoólatras …" 

33
00:02:32,660 --> 00:02:35,420
Etc. E você se perde lá. 

34
00:02:35,780 --> 00:02:40,060
Você se identifica com esse personagem. 

35
00:02:40,340 --> 00:02:41,720
Você está ficando mais fraco. 

36
00:02:42,060 --> 00:02:44,140
Você não é mais deus. 

37
00:02:44,420 --> 00:02:45,720
Você está confuso. 

38
00:02:46,080 --> 00:02:47,760
Você está perdido na materialização. 

39
00:02:48,060 --> 00:02:53,300
Com o nosso estado atual, sempre existem causas no passado. 

40
00:02:53,680 --> 00:02:58,540
Se mudarmos a situação atual, o passado também mudará. 

41
00:02:59,160 --> 00:03:01,940
E o futuro? 

42
00:03:02,340 --> 00:03:05,300
O futuro é uma história diferente … 

