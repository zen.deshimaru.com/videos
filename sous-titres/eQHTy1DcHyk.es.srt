1
00:00:00,090 --> 00:00:05,980
Durante una ceremonia Zen, hacemos sanpai:
tres postraciones.

2
00:00:06,320 --> 00:00:09,145
Un primer acelerando de campanas

3
00:00:09,385 --> 00:00:11,607
le dice a los monjes y monjas

4
00:00:11,727 --> 00:00:14,660
que necesitan para desplegar su zagu.

5
00:00:16,080 --> 00:00:19,905
Una postración comienza con un saludo en gassho.

6
00:00:20,015 --> 00:00:22,562
Luego pones las rodillas en el suelo.

7
00:00:22,612 --> 00:00:24,685
Ponga su cabeza en el suelo.

8
00:00:24,780 --> 00:00:26,620
Manos arriba.

9
00:00:26,750 --> 00:00:30,450
Te levantas sin las manos
apoyándose en los dedos de los pies.

10
00:00:30,660 --> 00:00:32,780
Por cada postración,

11
00:00:32,870 --> 00:00:36,235
suena una campana que nos dice que bajemos,

12
00:00:36,295 --> 00:00:39,432
otra, para volver a subir.

13
00:00:39,572 --> 00:00:41,676
Para la tercera postración,

14
00:00:41,846 --> 00:00:46,270
dos campanas para bajar
indican que este es la última.

15
00:00:47,150 --> 00:00:50,770
La última vez que te levantas,

16
00:00:50,930 --> 00:00:53,444
agarras tu zagu por la esquina derecha.

17
00:00:53,525 --> 00:00:56,470
Lo subimos a medida que subimos.

18
00:00:56,520 --> 00:00:59,375
Lo doblas una vez por la mitad, y luego otra vez por la mitad.

19
00:00:59,515 --> 00:01:03,710
Estas explicaciones están destinadas a
los que practican en el Zoom

20
00:01:03,820 --> 00:01:05,875
y que nunca han hecho sanpai

21
00:01:05,955 --> 00:01:08,702
para que puedan practicar la ceremonia.

22
00:01:08,872 --> 00:01:13,000
zazenathome.org
