1
00:00:00,010 --> 00:00:11,610
I punti chiave sono: il bacino,

2
00:00:11,740 --> 00:00:17,895
la quinta vertebra lombare,

3
00:00:18,635 --> 00:00:24,900
la prima vertebra dorsale, quella che sporge.

4
00:00:26,940 --> 00:00:31,820
E poi si vede la morfologia, ognuno è diverso.

5
00:00:31,960 --> 00:00:38,620
Diciamo che penso che dovrebbe allungare un po’ di più il collo.

6
00:00:41,210 --> 00:00:48,145
Bisogna sempre colpire due punti.

7
00:00:48,645 --> 00:00:52,262
Tranne quando è la parte superiore del cranio.

8
00:00:52,602 --> 00:00:55,460
Uno yin e uno yang.

9
00:00:55,640 --> 00:00:58,675
Mi metto a mio agio, mi metto in ginocchio.

10
00:00:59,855 --> 00:01:03,020
Toccherò proprio questa vertebra.

11
00:01:08,280 --> 00:01:09,815
Toccherò il mento con due dita.

12
00:01:09,975 --> 00:01:11,790
Non gli faccio questo.

13
00:01:14,170 --> 00:01:16,825
Stiamo dando indicazioni.

14
00:01:16,855 --> 00:01:19,540
Ma quando sono davvero ben dati,

15
00:01:19,620 --> 00:01:21,635
se lasciamo che la persona mantenga la sua concentrazione,

16
00:01:21,865 --> 00:01:24,132
ha un effetto molto profondo.

17
00:01:25,602 --> 00:01:30,670
Quindi spingero’ i due punti opposti.

18
00:01:30,753 --> 00:01:35,976
Possiamo anche fare così.

19
00:01:36,156 --> 00:01:39,650
So che ha subito un’operazione...

20
00:01:41,560 --> 00:01:43,680
Possiamo anche fare così.

21
00:01:43,785 --> 00:01:46,570
Sensei [Deshimaru] lo faceva spesso.

22
00:01:48,030 --> 00:01:53,480
Si può colpire un punto specifico in cui la persona ha dei punti deboli.

23
00:01:53,740 --> 00:01:56,860
Per esempio, allo stomaco o al fegato.

24
00:02:00,470 --> 00:02:03,900
Guardate sempre la curvatura.

25
00:02:06,820 --> 00:02:10,385
Spesso viene sottolineato troppo o troppo poco.

26
00:02:10,575 --> 00:02:15,140
Non dimenticare che hai un kyosaku in mano.

27
00:02:18,340 --> 00:02:23,950
Non c’è modo di iniziare ad armeggiare.

28
00:02:25,680 --> 00:02:30,245
Devi tenere il tuo kyosaku.

29
00:02:32,525 --> 00:02:36,740
Possiamo farlo con il kyosaku, non è un problema.

30
00:02:36,875 --> 00:02:39,895
Possiamo fare anche questo.

31
00:02:40,010 --> 00:02:45,140
Se la persona è troppo gobba.

32
00:02:48,195 --> 00:02:50,880
Possiamo fare anche questo.

33
00:02:52,790 --> 00:02:56,313
È necessario generare una correzione che la persona prenderà.

34
00:02:56,405 --> 00:02:59,930
È importante fare anche questo.

35
00:03:04,960 --> 00:03:08,450
È molto delicato e non disturbo la meditazione.

36
00:03:11,440 --> 00:03:13,570
Possiamo usare anche il kyosaku.

37
00:03:16,235 --> 00:03:19,825
Noi prendiamo il lato largo.

38
00:03:20,330 --> 00:03:23,325
Inginocchiati.

39
00:03:24,216 --> 00:03:27,264
E lo collocheremo all’arco.

40
00:03:29,334 --> 00:03:31,587
Questo vi darà un’indicazione.

41
00:03:32,747 --> 00:03:37,940
Molto negativo, perché ti sembra di non essere per niente etero.

42
00:03:38,150 --> 00:03:41,930
Perché il bastone è dritto, e la colonna non è dritta.

43
00:03:48,600 --> 00:03:50,800
La colonna non è mai dritta.

44
00:03:51,130 --> 00:03:52,995
Ha una leggera curvatura.

45
00:03:53,175 --> 00:03:59,240
Quando si indossa questo, è inevitabile che ci si senta un po’ perversi.

46
00:03:59,400 --> 00:04:06,705
Se lo metti sulla schiena...

47
00:04:06,955 --> 00:04:09,490
E’ un po’ spartano.

48
00:04:13,940 --> 00:04:16,660
Hai una postura molto bella, Cristina!

49
00:04:22,280 --> 00:04:25,430
Ha un’ottima postura.

50
00:04:28,960 --> 00:04:32,320
Ci sono piccole cose di cui non ci si rende conto.

51
00:04:32,480 --> 00:04:34,340
Per esempio, il pollice lì.

52
00:04:36,040 --> 00:04:39,820
Deve essere dritto.

53
00:04:41,850 --> 00:04:44,710
È qualcosa di cui non si rende conto.

54
00:04:44,890 --> 00:04:50,520
Ma andrai avanti per un anno, due anni, e i tuoi pollici andranno bene.

55
00:04:50,855 --> 00:04:53,735
È la prima cosa che vedo.

56
00:04:55,180 --> 00:04:58,900
Assicuratevi che i vostri pollici siano vicini all’ombelico.

57
00:05:10,260 --> 00:05:12,600
Un po’ troppo in fondo alla mia mente.

58
00:05:14,440 --> 00:05:20,300
La testa va un po’ in avanti.

59
00:05:28,920 --> 00:05:32,970
Sono totalmente preso dall’osteopatia.

60
00:05:34,420 --> 00:05:36,260
Bene!

61
00:05:38,315 --> 00:05:42,875
Allenta un po’ la tensione.

62
00:05:53,292 --> 00:05:54,862
Molto bello!

63
00:05:56,060 --> 00:06:00,290
Quando fai il gassho, fallo bene così.

64
00:06:04,605 --> 00:06:06,335
La postura è bellissima.

65
00:06:08,920 --> 00:06:11,120
Un po’ troppo curvo.

66
00:06:11,225 --> 00:06:14,765
Fuori di qui.

67
00:06:24,537 --> 00:06:28,737
A quel punto, perché vogliamo resistere,

68
00:06:35,023 --> 00:06:37,393
No, non muoverti!

69
00:06:37,591 --> 00:06:38,591
Voilà!

70
00:06:38,780 --> 00:06:39,780
Perfetto!

71
00:06:45,450 --> 00:06:49,410
Non dimenticate che è la mano sinistra nella mano destra.

72
00:06:51,200 --> 00:06:52,950
Rilassa le mani!

73
00:06:53,010 --> 00:06:55,540
Rilassa le dita.

74
00:06:56,150 --> 00:06:58,530
Li metti l’uno sull’altro.

75
00:07:03,395 --> 00:07:06,015
E’ un po’ tesa, ma comunque...

76
00:07:08,797 --> 00:07:09,947
Qui.

77
00:07:16,500 --> 00:07:19,185
Cosa farò allora...

78
00:07:19,275 --> 00:07:21,870
Vedo molta tensione, è normale.

79
00:07:21,910 --> 00:07:24,435
Perché all’inizio dobbiamo farlo in forza.

80
00:07:24,585 --> 00:07:27,130
Fallo tu.

81
00:07:30,560 --> 00:07:33,810
Le mani sono abbastanza buone ora.

82
00:07:34,930 --> 00:07:38,150
Rilassati, non devono essere troppo tese.

83
00:07:39,690 --> 00:07:40,470
Flessibile.

84
00:07:40,560 --> 00:07:42,110
Bene, ottimo!

85
00:07:48,975 --> 00:07:54,395
Io, io metterei un po’ di enfasi sulla quinta vertebra.

86
00:08:03,997 --> 00:08:07,027
Vedi, mi sento un po’ teso.

87
00:08:10,253 --> 00:08:12,099
Bene!

88
00:08:12,829 --> 00:08:15,839
Allenta i gomiti.

89
00:08:19,849 --> 00:08:27,759
Sensei, non sui principianti, ma lo faceva spesso.

90
00:08:27,894 --> 00:08:31,684
Sicuramente, stava spingendo molto.

91
00:08:33,682 --> 00:08:37,282
Per ora, non ce n’è bisogno.

92
00:08:37,941 --> 00:08:39,471
Va bene. Va bene.

93
00:08:40,660 --> 00:08:42,680
Puoi iniziare a respirare.

94
00:08:42,790 --> 00:08:44,875
Fai un bel respiro.

95
00:08:45,065 --> 00:08:46,577
Segui il tuo respiro.

96
00:08:46,727 --> 00:08:48,010
Buon divertimento.

97
00:08:54,425 --> 00:08:55,625
Molto bene.

98
00:08:55,717 --> 00:08:58,987
È il tuo primo sesshin?

99
00:09:00,103 --> 00:09:02,613
Bravo!

100
00:09:02,853 --> 00:09:04,530
Lasciate che vi chieda una cosa:

101
00:09:04,608 --> 00:09:07,268
Per respirare,

102
00:09:07,380 --> 00:09:12,640
a volte mi faccio prendere dal panico e non so come smettere...

103
00:09:20,830 --> 00:09:22,820
Finché ti senti bene...

104
00:09:26,310 --> 00:09:29,775
Questo è il mio punto, ci sono altri metodi...

105
00:09:29,935 --> 00:09:32,860
Ma in zazen, per respirare.

106
00:09:33,060 --> 00:09:36,780
Devi sentirti bene, non forzarla.

107
00:09:37,130 --> 00:09:40,530
Finché ti senti bene, continua ad andare avanti.

108
00:09:45,380 --> 00:09:48,570
Si riprende fiato nel momento in cui lo si sente.

109
00:09:48,731 --> 00:09:51,461
È il tuo corpo che deve dettarlo.

110
00:10:25,390 --> 00:10:27,310
La postura in generale

111
00:11:05,935 --> 00:11:08,115
Bene!

112
00:11:15,740 --> 00:11:18,100
Bisogna guardare ai punti principali:

113
00:11:18,190 --> 00:11:21,200
quinta vertebra lombare, prima dorsale,

114
00:11:21,266 --> 00:11:24,790
testa, braccia non troppo strette.

115
00:11:26,070 --> 00:11:31,060
Possiamo aiutare la persona con la posizione delle mani.

116
00:11:31,240 --> 00:11:34,280
Vorrei vedere una postura kin-hin.

117
00:11:41,400 --> 00:11:44,925
Non c’è altro che una buona postura.

118
00:11:46,485 --> 00:11:50,500
Ha una postura molto bella, molto naturale.

119
00:11:51,230 --> 00:11:59,200
Tranne che per un dettaglio: il mignolo deve essere stretto.

120
00:11:59,785 --> 00:12:01,855
La gente spesso dimentica di stringere.

121
00:12:01,967 --> 00:12:03,822
Sensei diceva sempre:

122
00:12:03,982 --> 00:12:08,977
le due piccole dita devono essere molto strette.

123
00:12:15,043 --> 00:12:17,623
Ti ho anche corretto l’ultima volta...

124
00:12:19,766 --> 00:12:22,006
Bene, bravo!

125
00:12:24,018 --> 00:12:27,238
Ci sono spesso persone con posture del genere.

126
00:12:27,424 --> 00:12:28,974
Devono essere corretti?

127
00:12:29,047 --> 00:12:30,387
Dovrebbe essere corretto.

128
00:12:30,510 --> 00:12:33,615
Per esempio, Pierre, prendi la postura...

129
00:12:33,920 --> 00:12:38,170
Ho notato qualcosa, vedremo se lo farai...

130
00:12:46,127 --> 00:12:50,227
La mano è un po’ così...

131
00:12:55,366 --> 00:12:57,176
Rilassati un po’.

132
00:12:59,735 --> 00:13:04,130
Ha davvero bisogno di essere stratificato.

133
00:13:13,740 --> 00:13:15,870
A volte si può commettere un errore.

134
00:13:15,935 --> 00:13:18,195
Se lo aggiustiamo...

135
00:13:18,317 --> 00:13:20,887
Stringi un po’ le tue piccole dita...

136
00:13:23,093 --> 00:13:27,443
Sensei ha sempre mostrato energia in entrambe le dita.

137
00:13:27,821 --> 00:13:30,621
Facciamo così, poi così.

138
00:13:32,250 --> 00:13:35,010
E poi questo lo mettiamo in cima.

139
00:13:36,000 --> 00:13:38,040
Bello e perpendicolare.

140
00:13:38,310 --> 00:13:42,130
Quando si aprono le mani, sono parallele al suolo.

141
00:13:44,370 --> 00:13:46,580
Non vuoi finire così...

142
00:13:47,610 --> 00:13:48,830
Va bene, va bene.

143
00:13:48,990 --> 00:13:50,410
E’ perfetto.
