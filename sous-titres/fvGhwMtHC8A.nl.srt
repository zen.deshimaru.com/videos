1
00:00:38,480 --> 00:00:40,340
De sessie begint, 

2
00:00:42,060 --> 00:00:45,680
richt uw camera goed voordat u gaat zitten. 

3
00:00:48,700 --> 00:00:54,020
Je mag niet spreken, dit is niet het moment, we doen zazen in stilte. 

4
00:01:00,860 --> 00:01:05,320
Installeer jezelf. 

5
00:01:05,320 --> 00:01:10,240
Duw de aarde met de wortels 

6
00:01:10,240 --> 00:01:18,780
en tegelijkertijd wordt de ruggengraat naar de hemel uitgestrekt. 

7
00:01:21,600 --> 00:01:24,320
Ademen is nasaal, 

8
00:01:24,320 --> 00:01:27,560
je ademt in door de neusgaten 

9
00:01:27,560 --> 00:01:29,540
voor degenen die hebben 

10
00:01:29,540 --> 00:01:34,820
en je ademt diep uit 

11
00:01:34,820 --> 00:01:38,460
en daar laat je de spanning los 

12
00:01:45,340 --> 00:01:50,960
Zana, je ademt te hard, je lichaam beweegt te veel. 

13
00:01:50,960 --> 00:01:56,810
Ik dacht dat je op en neer moest gaan. 

14
00:01:56,810 --> 00:02:00,240
Wees vooral stil Zana. Stilte! 

15
00:02:04,360 --> 00:02:06,840
Je hebt een goede houding. 

16
00:02:11,880 --> 00:02:19,940
Zelfs als het een virtuele dojo is, moet je je haar minimaal doen voordat je komt, 

17
00:02:19,940 --> 00:02:23,940
zonder iemand te noemen. El Broco bijvoorbeeld. 

18
00:02:24,740 --> 00:02:29,460
Heb je vanmorgen je haar gekamd met een voetzoeker? 

19
00:02:30,720 --> 00:02:33,220
Het is ook nodig om de wortels te stylen. 

20
00:02:36,000 --> 00:02:39,080
Er zijn hele mooie houdingen. 

21
00:02:42,740 --> 00:02:44,380
Geen angst, 

22
00:02:45,100 --> 00:02:51,660
laat u in deze moeilijke tijden niet van buitenaf opnemen. 

23
00:02:52,160 --> 00:02:57,940
Richt je opnieuw, wees je eigen meester, 

24
00:02:59,480 --> 00:03:04,940
en zoals Ronaldo zei: 

25
00:03:38,340 --> 00:03:41,960
Laat u niet misleiden door uw emoties, 

26
00:03:44,640 --> 00:03:48,620
dit te zien, hoe voel je je? 

27
00:03:55,480 --> 00:03:57,640
En dit zien? 

28
00:04:07,160 --> 00:04:10,300
Zoek geen vreugde, 

29
00:04:12,040 --> 00:04:14,960
ren niet weg voor angst 

30
00:04:17,460 --> 00:04:20,620
Omdat zoals Maître Deshimaru zei: 

31
00:04:22,380 --> 00:04:26,060
’Zelfs als je van bloemen houdt, verwelken ze. 

32
00:04:27,640 --> 00:04:32,780
Zelfs als je niet van onkruid houdt, groeien ze. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Ja! Ja! 

35
00:04:55,600 --> 00:04:58,700
Zijn we nog niet begonnen? Kunnen we verhuizen? 

36
00:04:58,700 --> 00:05:02,640
We zullen opnieuw beginnen met Radis en Rose die niet werkt. 

37
00:05:03,780 --> 00:05:09,160
Ik begrijp het niet, moet ik de camera stoppen? Wat moet er gebeuren? Beiden tegelijk? 

38
00:05:10,820 --> 00:05:15,340
Wie heeft er gesproken? Het is Muriel, nee, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, let op je wortel, want als je te veel beweegt, wil je overgeven. 

