1
00:00:13,650 --> 00:00:17,340
Also wurde ich in Montpellier geboren, also in Frankreich 

2
00:00:17,340 --> 00:00:22,510
Aber jetzt lebe ich in einem Klang, in dem meine Eltern zwei verschiedene Länder sind 

3
00:00:22,510 --> 00:00:26,950
plötzlich reise ich oft zwischen dem einzigen in Frankreich und auch anderen Ländern 

4
00:00:26,950 --> 00:00:32,169
weil sie dort sehen, sehe ich dort mit ihrer Arbeit, aber jeden Sommer kommen wir 

5
00:00:32,169 --> 00:00:36,370
hier der Tempel im Berg seit der Bearbeitung, als ich zwei hatte 

6
00:00:36,370 --> 00:00:44,960
Wochen Ich denke, wir haben Vollzeit hier aufgewachsen, ich nie immer 

7
00:00:44,960 --> 00:00:50,390
Komm her, was dann ich und meine Freunde die ganze Zeit wissen, ja, wir gehen 

8
00:00:50,390 --> 00:00:55,130
Komme immer wieder hierher, auch wenn du kein Erwachsener bist, aber du hast es dir gesagt 

9
00:00:55,130 --> 00:00:58,430
Niemals, dass wir reden müssten, wäre es nicht wirklich gewesen 

10
00:00:58,430 --> 00:01:03,080
Etwas, an das wir gedacht haben, wenn Leute hierher kommen, zu denen sie hierher kommen 

11
00:01:03,080 --> 00:01:08,960
Zen mich, als ich nicht existierte, um die Beine zum Essen zu sehen 

12
00:01:08,960 --> 00:01:15,560
und plötzlich entdeckte ich dort die Sangha vor Zen mich und meine Meinung, die Sie haben, ist auf 

13
00:01:15,560 --> 00:01:19,850
hatte auch einen anderen vivien Freund, der einen Wind mehr hatte als er und wir 

14
00:01:19,850 --> 00:01:25,729
Für uns ist es ein bisschen so, als würde man es in die doot rue godot stecken, die es uns gemacht hat 

15
00:01:25,729 --> 00:01:32,090
Glaube an Magie, er brachte uns dazu, alle möglichen Dinge zu glauben 

16
00:01:32,090 --> 00:01:37,700
Beispiel eine Frau, als sie draußen im Wald war, sagte er uns halb fertig 

17
00:01:37,700 --> 00:01:42,530
grabe im Boden und weil es immer Diamanten unter der Erde gibt 

18
00:01:42,530 --> 00:01:48,140
Egal zu Fuß, es gibt mindestens einen Diamanten pro Meter vom Konsulat entfernt 

19
00:01:48,140 --> 00:01:54,680
gegraben und gegraben und gegraben und das auf jeden Fall hatten wir beide beide Seiten 

20
00:01:54,680 --> 00:01:59,270
Vor ihm versteckt zog er einen Diamanten aus der Tasche und tat so, als würde er aus dem Haus steigen 

21
00:01:59,270 --> 00:02:04,640
Der Boden eines riesigen Diamanten, der von seiner Mutter entlehnt worden war, wurde geblendet 

22
00:02:04,640 --> 00:02:12,230
wir müssen so etwas wie 30 haben und während der Art viel danach gegeben 

23
00:02:12,230 --> 00:02:15,860
manchmal grub ich ein wenig frech oft, wenn ich eine finde 

24
00:02:15,860 --> 00:02:21,110
Diamant, wenn ich jemals merkte, dass ich zehn Jahre alt sein könnte 

25
00:02:21,110 --> 00:02:24,250
etwas, wenn ich dir in meiner Haut alles erzähle 

26
00:02:24,250 --> 00:02:28,130
James ist anders für alle Ich glaube, meine Mutter, wenn die Müdigkeit von 

27
00:02:28,130 --> 00:02:31,840
Bei ihren Fans fühlte sie sich sofort für den Rest ihres Lebens pharsalblau 

28
00:02:31,840 --> 00:02:38,440
aber ich glaube, dass ich nach jedem Zen anfange, mehr zu lieben 

29
00:02:38,690 --> 00:02:43,950
Bei mehr oder weniger gut dem zweiten Schuss sage ich mir, dass ich hätte 

30
00:02:43,950 --> 00:02:48,709
Entrückung außerdem ist es noch nicht vorbei 

31
00:02:49,280 --> 00:02:54,740
Ich kannte immer die Ankündigungen, die ich seit jeher immer hatte, als meine 

32
00:02:54,740 --> 00:02:59,840
Mutter war schwanger sie übte schon Zazen plötzlich ist es ein bisschen wie 

33
00:02:59,840 --> 00:03:06,890
Ich wurde plötzlich geboren und kannte die Sangha immer in verschiedenen Tempeln 

34
00:03:06,890 --> 00:03:13,819
dass ich viele Erinnerungen habe, habe ich Freunde oder sogar Hütten getroffen 

35
00:03:13,819 --> 00:03:17,750
was immer unterschiedliche anforderungen gestellt hat und als ich klein war ich 

36
00:03:17,750 --> 00:03:21,470
Ich habe mich nicht versteckt Ich habe nur meinen Freunden gesagt, ja, ich gehe in die Berge und 

37
00:03:21,470 --> 00:03:25,850
Alles, aber ich habe ihnen nie gesagt, dass ich es tun werde. Ich gehe zum Zen. Ich habe es ihnen gesagt 

38
00:03:25,850 --> 00:03:28,400
nicht das, weil ich es trotzdem einmal versucht habe 

39
00:03:28,400 --> 00:03:31,190
Sie alle sahen mich sehr seltsam an, sagte ich mir, hey 

40
00:03:31,190 --> 00:03:36,110
Sprich nie wieder und aber sonst kam es mir nie seltsam vor 

41
00:03:36,110 --> 00:03:41,590
Noch normaler zu gehen, um zu gehen, wusste ich im Voraus, ob ich kam 

42
00:03:41,590 --> 00:03:46,010
Ich musste jeden Tag ein Zen machen, aber ich war überhaupt nicht gegen diese Idee 

43
00:03:46,010 --> 00:03:50,750
Ehrlich gesagt, dass ich für mein Spiel genau herausfinden wollte, dass es meine Wahl war 

44
00:03:50,750 --> 00:03:54,709
Komm zuerst her, um die ganze Saga zu sehen, die ich in zwei nicht gesehen habe 

45
00:03:54,709 --> 00:04:01,820
Drei Jahre dann der Ort das Essen und all die Erinnerungen, die zurückkommen 

46
00:04:01,820 --> 00:04:06,380
getan, sobald ich das erste Mal komme, habe ich nicht bemerkt 

47
00:04:06,380 --> 00:04:09,850
Konto, das ich tat, gibt es tatsächlich ziemlich seltsam in dem Sinne, dass 

48
00:04:09,850 --> 00:04:15,320
Ich habe nicht zu viele Veränderungen gesehen, tatsächlich habe ich nicht gesehen, weil ich nicht gesehen habe 

49
00:04:15,320 --> 00:04:19,310
die Nützlichkeit von Zen, aber es scheint mir so normal, dass ich keine gesehen habe 

50
00:04:19,310 --> 00:04:23,660
Veränderung finde ich eigentlich seltsam, aber ich mag es, wenn ich Lust habe 

51
00:04:23,660 --> 00:04:28,490
Je mehr Glück Jean Prat hat, desto mehr mag und fluffe ich als sie mich 

52
00:04:28,490 --> 00:04:31,090
anders aussehen 

53
00:04:33,470 --> 00:04:41,410
die dame validiert in frankreich youtube in 

54
00:04:45,910 --> 00:04:57,220
Frankreich sowohl in einfach als auch effizient 

55
00:05:05,560 --> 00:05:13,809
[Musik] 

56
00:05:22,860 --> 00:05:25,929
[Musik] 

57
00:07:03,510 --> 00:07:06,840
1 [Musik] 

58
00:07:06,840 --> 00:07:08,840
e 

