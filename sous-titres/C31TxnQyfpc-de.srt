1
00:00:00,010 --> 00:00:11,610
Die wichtigsten Punkte sind: das Becken,

2
00:00:11,740 --> 00:00:17,895
der fünfte Lendenwirbel,

3
00:00:18,635 --> 00:00:24,900
der erste Rückenwirbel, derjenige, der hervorsteht.

4
00:00:26,940 --> 00:00:31,820
Und dann sieht man die Morphologie, jeder ist anders.

5
00:00:31,960 --> 00:00:38,620
Sagen wir, ich denke, sie sollte ihren Hals etwas mehr strecken.

6
00:00:41,210 --> 00:00:48,145
Man muss immer zwei Punkte treffen.

7
00:00:48,645 --> 00:00:52,262
Außer wenn es sich um die Schädeldecke handelt.

8
00:00:52,602 --> 00:00:55,460
Ein Yin und ein Yang.

9
00:00:55,640 --> 00:00:58,675
Ich mache es mir bequem, ich gehe auf die Knie.

10
00:00:59,855 --> 00:01:03,020
Ich werde jetzt genau diesen Wirbel berühren.

11
00:01:08,280 --> 00:01:09,815
Ich werde das Kinn mit zwei Fingern berühren.

12
00:01:09,975 --> 00:01:11,790
Ich tue ihm das nicht an.

13
00:01:14,170 --> 00:01:16,825
Wir geben Anweisungen.

14
00:01:16,855 --> 00:01:19,540
Aber wenn sie wirklich gut gegeben sind,

15
00:01:19,620 --> 00:01:21,635
wenn wir die Person ihren Fokus behalten lassen,

16
00:01:21,865 --> 00:01:24,132
es hat eine sehr tiefgreifende Wirkung.

17
00:01:25,602 --> 00:01:30,670
Ich werde also die beiden gegensätzlichen Punkte vorantreiben.

18
00:01:30,753 --> 00:01:35,976
Wir können es auch so machen.

19
00:01:36,156 --> 00:01:39,650
Ich weiß, dass sie operiert wurde...

20
00:01:41,560 --> 00:01:43,680
Wir können es auch so machen.

21
00:01:43,785 --> 00:01:46,570
Sensei [Deshimaru] hat das oft getan.

22
00:01:48,030 --> 00:01:53,480
Sie können einen bestimmten Punkt treffen, an dem die Person Schwächen hat.

23
00:01:53,740 --> 00:01:56,860
Zum Beispiel auf den Magen oder die Leber.

24
00:02:00,470 --> 00:02:03,900
Achten Sie immer auf die Krümmung.

25
00:02:06,820 --> 00:02:10,385
Häufig wird sie über- oder unterbetont.

26
00:02:10,575 --> 00:02:15,140
Vergessen Sie nicht, dass Sie einen Kyosaku in der Hand halten.

27
00:02:18,340 --> 00:02:23,950
Wir werden auf keinen Fall anfangen zu basteln.

28
00:02:25,680 --> 00:02:30,245
Sie müssen Ihren Kyosaku behalten.

29
00:02:32,525 --> 00:02:36,740
Das können wir mit dem Kyosaku machen, es stört nicht.

30
00:02:36,875 --> 00:02:39,895
Das können wir auch tun.

31
00:02:40,010 --> 00:02:45,140
Wenn die Person zu gebückt ist.

32
00:02:48,195 --> 00:02:50,880
Das können wir auch tun.

33
00:02:52,790 --> 00:02:56,313
Sie müssen eine Korrektur erzeugen, die die Person übernehmen wird.

34
00:02:56,405 --> 00:02:59,930
Es ist auch wichtig, das zu tun.

35
00:03:04,960 --> 00:03:08,450
Es ist sehr delikat und ich störe die Meditation nicht.

36
00:03:11,440 --> 00:03:13,570
Wir können auch Kyosaku verwenden.

37
00:03:16,235 --> 00:03:19,825
Wir nehmen die breite Seite nach unten.

38
00:03:20,330 --> 00:03:23,325
Knie dich hin.

39
00:03:24,216 --> 00:03:27,264
Und wir werden ihn am Bogen platzieren.

40
00:03:29,334 --> 00:03:31,587
Das wird Ihnen einen Hinweis geben.

41
00:03:32,747 --> 00:03:37,940
Sehr negativ, weil man das Gefühl hat, überhaupt nicht heterosexuell zu sein.

42
00:03:38,150 --> 00:03:41,930
Denn der Stock ist gerade, und die Säule ist nicht gerade.

43
00:03:48,600 --> 00:03:50,800
Die Spalte ist nie gerade.

44
00:03:51,130 --> 00:03:52,995
Es hat eine leichte Krümmung.

45
00:03:53,175 --> 00:03:59,240
Wenn Sie das anziehen, werden Sie sich zwangsläufig ein wenig verdreht fühlen.

46
00:03:59,400 --> 00:04:06,705
Wenn man es auf den Rücken legt...

47
00:04:06,955 --> 00:04:09,490
Es ist ein bisschen spartanisch.

48
00:04:13,940 --> 00:04:16,660
Du hast eine sehr schöne Körperhaltung, Cristina!

49
00:04:22,280 --> 00:04:25,430
Er hat eine tolle Körperhaltung.

50
00:04:28,960 --> 00:04:32,320
Es gibt kleine Dinge, die Sie nicht erkennen.

51
00:04:32,480 --> 00:04:34,340
Zum Beispiel der Daumen dort.

52
00:04:36,040 --> 00:04:39,820
Es muss gerade sein.

53
00:04:41,850 --> 00:04:44,710
Es ist etwas, das er nicht erkennt.

54
00:04:44,890 --> 00:04:50,520
Aber Sie werden ein Jahr, zwei Jahre weitermachen, und Ihre Daumen werden in Ordnung sein.

55
00:04:50,855 --> 00:04:53,735
Es ist das erste, was ich sehe.

56
00:04:55,180 --> 00:04:58,900
Achten Sie darauf, dass Ihre Daumen direkt an Ihrem Bauchnabel anliegen.

57
00:05:10,260 --> 00:05:12,600
Ich habe ein bisschen zu viel im Hinterkopf.

58
00:05:14,440 --> 00:05:20,300
Der Kopf geht ein wenig nach vorne.

59
00:05:28,920 --> 00:05:32,970
Ich stehe total auf Osteopathie.

60
00:05:34,420 --> 00:05:36,260
Also gut!

61
00:05:38,315 --> 00:05:42,875
Lockern Sie sich dort ein wenig auf.

62
00:05:53,292 --> 00:05:54,862
Sehr schön!

63
00:05:56,060 --> 00:06:00,290
Wenn Sie Gasho machen, machen Sie es so richtig.

64
00:06:04,605 --> 00:06:06,335
Die Körperhaltung ist schön.

65
00:06:08,920 --> 00:06:11,120
Etwas zu gewölbt.

66
00:06:11,225 --> 00:06:14,765
Verschwinden Sie von hier.

67
00:06:24,537 --> 00:06:28,737
An diesem Punkt, weil wir durchhalten wollen,

68
00:06:35,023 --> 00:06:37,393
Nein, nein, nicht bewegen!

69
00:06:37,591 --> 00:06:38,591
Voilà!

70
00:06:38,780 --> 00:06:39,780
Perfekt!

71
00:06:45,450 --> 00:06:49,410
Vergessen Sie nicht, dass die linke Hand in der rechten Hand liegt.

72
00:06:51,200 --> 00:06:52,950
Entspannen Sie Ihre Hände!

73
00:06:53,010 --> 00:06:55,540
Entspannen Sie Ihre Finger.

74
00:06:56,150 --> 00:06:58,530
Man legt sie aufeinander.

75
00:07:03,395 --> 00:07:06,015
Es ist ein wenig angespannt, aber trotzdem...

76
00:07:08,797 --> 00:07:09,947
Hier.

77
00:07:16,500 --> 00:07:19,185
Was ich dann tun werde...

78
00:07:19,275 --> 00:07:21,870
Ich sehe eine große Spannung. Das ist normal.

79
00:07:21,910 --> 00:07:24,435
Denn am Anfang müssen wir es in Kraft tun.

80
00:07:24,585 --> 00:07:27,130
Tun Sie das.

81
00:07:30,560 --> 00:07:33,810
Die Hände sind jetzt ziemlich gut.

82
00:07:34,930 --> 00:07:38,150
Entspannen Sie sich, sie müssen nicht zu angespannt sein.

83
00:07:39,690 --> 00:07:40,470
Flexibel.

84
00:07:40,560 --> 00:07:42,110
Das ist gut, großartig!

85
00:07:48,975 --> 00:07:54,395
Ich würde den fünften Wirbel ein wenig betonen.

86
00:08:03,997 --> 00:08:07,027
Sehen Sie, ich fühle mich ein wenig angespannt.

87
00:08:10,253 --> 00:08:12,099
Also gut!

88
00:08:12,829 --> 00:08:15,839
Lösen Sie Ihre Ellbogen.

89
00:08:19,849 --> 00:08:27,759
Sensei, nicht bei Anfängern, aber das hat er früher oft gemacht.

90
00:08:27,894 --> 00:08:31,684
Auf jeden Fall hat er sich sehr bemüht.

91
00:08:33,682 --> 00:08:37,282
Im Moment besteht kein Bedarf.

92
00:08:37,941 --> 00:08:39,471
Schon gut. Schon gut.

93
00:08:40,660 --> 00:08:42,680
Sie können mit der Atmung beginnen.

94
00:08:42,790 --> 00:08:44,875
Atmen Sie tief durch.

95
00:08:45,065 --> 00:08:46,577
Folgen Sie Ihrem Atem.

96
00:08:46,727 --> 00:08:48,010
Viel Spaß.

97
00:08:54,425 --> 00:08:55,625
Das ist sehr gut.

98
00:08:55,717 --> 00:08:58,987
Ist dies Ihr erstes Sesshin?

99
00:09:00,103 --> 00:09:02,613
Bravo!

100
00:09:02,853 --> 00:09:04,530
Ich möchte Sie etwas fragen:

101
00:09:04,608 --> 00:09:07,268
Für die Atmung,

102
00:09:07,380 --> 00:09:12,640
manchmal schlucke ich es runter und weiß nicht, wie ich aufhören soll...

103
00:09:20,830 --> 00:09:22,820
Solange Sie sich gut fühlen...

104
00:09:26,310 --> 00:09:29,775
Das ist mein Punkt, es gibt andere Methoden...

105
00:09:29,935 --> 00:09:32,860
Aber in Zazen, für die Atmung.

106
00:09:33,060 --> 00:09:36,780
Man muss sich gut fühlen, erzwingen Sie es nicht.

107
00:09:37,130 --> 00:09:40,530
Solange man sich gut fühlt, macht man weiter.

108
00:09:45,380 --> 00:09:48,570
Man atmet in dem Moment, in dem man es spürt.

109
00:09:48,731 --> 00:09:51,461
Es ist Ihr Körper, der es diktieren muss.

110
00:10:25,390 --> 00:10:27,310
Körperhaltung im Allgemeinen

111
00:11:05,935 --> 00:11:08,115
Also gut!

112
00:11:15,740 --> 00:11:18,100
Man muss sich die wichtigsten Punkte anschauen:

113
00:11:18,190 --> 00:11:21,200
fünfter Lendenwirbel, erster dorsaler Wirbel,

114
00:11:21,266 --> 00:11:24,790
Kopf, Arme nicht zu fest anziehen.

115
00:11:26,070 --> 00:11:31,060
Wir können der Person durch eine Position der Hände helfen.

116
00:11:31,240 --> 00:11:34,280
Ich würde gerne eine Verwandten-Haltung sehen.

117
00:11:41,400 --> 00:11:44,925
Es gibt nichts als gute Körperhaltung.

118
00:11:46,485 --> 00:11:50,500
Er hat eine sehr schöne Körperhaltung, sehr natürlich.

119
00:11:51,230 --> 00:11:59,200
Bis auf ein Detail: Der kleine Finger muss straff sein.

120
00:11:59,785 --> 00:12:01,855
Man vergisst oft, sie anzuziehen.

121
00:12:01,967 --> 00:12:03,822
Sensei sagte immer:

122
00:12:03,982 --> 00:12:08,977
die beiden kleinen Finger müssen sehr straff sein.

123
00:12:15,043 --> 00:12:17,623
Letztes Mal habe ich dich sogar korrigiert...

124
00:12:19,766 --> 00:12:22,006
In Ordnung, bravo!

125
00:12:24,018 --> 00:12:27,238
Es gibt oft Menschen mit solchen Haltungen.

126
00:12:27,424 --> 00:12:28,974
Sollten sie korrigiert werden?

127
00:12:29,047 --> 00:12:30,387
Sie sollte korrigiert werden.

128
00:12:30,510 --> 00:12:33,615
Zum Beispiel, Pierre, nehmen Sie die Haltung ein...

129
00:12:33,920 --> 00:12:38,170
Mir ist etwas aufgefallen, wir werden sehen, ob Sie...

130
00:12:46,127 --> 00:12:50,227
Die Hand ist ein bisschen so...

131
00:12:55,366 --> 00:12:57,176
Etwas lockerer werden.

132
00:12:59,735 --> 00:13:04,130
Es muss wirklich geschichtet werden.

133
00:13:13,740 --> 00:13:15,870
Manchmal kann man einen Fehler machen.

134
00:13:15,935 --> 00:13:18,195
Wenn wir es in Ordnung bringen...

135
00:13:18,317 --> 00:13:20,887
Drücken Sie Ihre kleinen Finger ein wenig...

136
00:13:23,093 --> 00:13:27,443
Sensei zeigte immer Energie in beiden Fingern.

137
00:13:27,821 --> 00:13:30,621
Wir machen es so, dann so.

138
00:13:32,250 --> 00:13:35,010
Und dann haben wir das hier oben drauf gesetzt.

139
00:13:36,000 --> 00:13:38,040
Schön senkrecht.

140
00:13:38,310 --> 00:13:42,130
Wenn Sie Ihre Hände öffnen, sind sie parallel zum Boden.

141
00:13:44,370 --> 00:13:46,580
Du willst doch nicht so enden...

142
00:13:47,610 --> 00:13:48,830
Das ist gut. Das ist gut.

143
00:13:48,990 --> 00:13:50,410
Es ist perfekt.