1
00:00:04,240 --> 00:00:07,040
¿Qué es el zen? 

2
00:00:07,320 --> 00:00:10,120
Zen se trata de comprenderte a ti mismo. 

3
00:00:10,400 --> 00:00:11,800
Tu propia mente 

4
00:00:12,080 --> 00:00:14,140
Es asunto de todos. 

5
00:00:14,420 --> 00:00:16,500
Eso es lo que Deshimaru quería transmitir. 

6
00:00:16,800 --> 00:00:19,460
Eso es lo que quiero transmitir y darme cuenta. 

7
00:00:19,720 --> 00:00:22,460
Había visto un discípulo de Kodo Sawaki. 

8
00:00:22,840 --> 00:00:24,720
Me dijo: 

9
00:00:25,120 --> 00:00:27,620
"¡Tienes que mirarte a ti mismo!" 

10
00:00:27,960 --> 00:00:29,960
¡Tenía razón! 

11
00:00:30,380 --> 00:00:33,300
¿Qué significa un camino espiritual? 

12
00:00:33,640 --> 00:00:41,880
El significado de un camino espiritual es "volver al soñador". 

13
00:00:46,020 --> 00:00:48,260
Dogen dice: 

14
00:00:48,520 --> 00:00:51,260
"Estudiar budismo es estudiarte a ti mismo". 

15
00:00:51,540 --> 00:00:53,980
Estudia al soñador. 

16
00:00:54,380 --> 00:00:57,300
¿Por qué sueña así? 

17
00:00:58,260 --> 00:01:00,240
¿Cómo puede cambiar su sueño? 

18
00:01:00,540 --> 00:01:02,020
¿Cómo puede controlar su sueño? 

19
00:01:02,840 --> 00:01:04,000
Cuando el Buda dice: 

20
00:01:04,440 --> 00:01:06,300
"Todo es un sueño". 

21
00:01:06,620 --> 00:01:09,820
Eso no quiere decir "nada importa". 

22
00:01:10,100 --> 00:01:13,580
Lo importante no es el sueño, es el soñador. 

23
00:01:13,900 --> 00:01:16,540
Deja de soñar, eso es lo que llamamos "despertar" … 

24
00:01:17,180 --> 00:01:21,040
El despertar se está convirtiendo en un soñador. 

25
00:01:21,340 --> 00:01:22,980
Controlando tu sueño. 

26
00:01:24,000 --> 00:01:26,120
Ese es el camino espiritual. 

27
00:01:26,420 --> 00:01:27,980
Es muy dificil. 

28
00:01:28,560 --> 00:01:31,760
¿Cómo puedo comenzar? 

29
00:01:32,660 --> 00:01:45,080
La motivación principal para practicar un camino espiritual es: poder controlar tu sueño. 

30
00:01:46,700 --> 00:01:49,340
¿Qué es importante en el zen? 

31
00:01:49,920 --> 00:01:55,880
De esta manera es tu mente lo que importa. 

32
00:01:56,280 --> 00:02:01,280
Zazen no es un objeto de adoración. 

33
00:02:01,540 --> 00:02:03,980
El segundo punto importante de su enseñanza: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku 

35
00:02:07,180 --> 00:02:10,780
Lo que significa "sin fines de lucro". 

36
00:02:11,340 --> 00:02:23,340
Es la puerta que nos lleva del espíritu de Dios al mundo material. 

37
00:02:23,640 --> 00:02:27,000
Es muy importante 

38
00:02:27,360 --> 00:02:37,420
Incluso en el mundo cotidiano, incluso en los fenómenos, no pierdes la concentración. 

39
00:02:37,800 --> 00:02:42,920
No pierdes tu verdadera naturaleza. 

40
00:02:43,180 --> 00:02:45,100
Ese es Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
¿Pero qué es fácil? 

42
00:02:55,780 --> 00:03:00,260
La gente siempre toma el camino fácil. 

43
00:03:00,620 --> 00:03:03,780
Caemos en las cosas fáciles. 

44
00:03:10,640 --> 00:03:13,600
El poder es fácil. 

45
00:03:14,060 --> 00:03:19,220
Es fácil estar con personas que siempre sienten lo mismo. 

46
00:03:21,160 --> 00:03:26,400
Hacer una iglesia con zen es fácil. 

47
00:03:26,800 --> 00:03:32,080
Adjuntar a una iglesia es fácil. 

48
00:03:32,360 --> 00:03:37,300
La gente entra en algo seguro. 

49
00:03:37,600 --> 00:03:41,060
Sin ningún esfuerzo 

50
00:03:41,420 --> 00:03:44,620
Donde nunca se enfrentan realmente con ellos mismos. 

51
00:03:44,920 --> 00:03:47,080
Donde nunca están solos. 

52
00:03:47,500 --> 00:03:52,040
¿Cómo ves a tus antiguos compañeros seguidores? 

53
00:03:52,360 --> 00:03:58,720
Admiro a muchos de los discípulos del Maestro Deshimaru. 

54
00:03:58,980 --> 00:04:07,980
Quienes han recibido su educación y continúan practicando solos. 

55
00:04:08,240 --> 00:04:14,320
Es mejor practicar juntos. 

56
00:04:14,700 --> 00:04:19,140
Pero al menos hacen sus propias cosas. 

57
00:04:19,660 --> 00:04:23,980
Estan solos 

