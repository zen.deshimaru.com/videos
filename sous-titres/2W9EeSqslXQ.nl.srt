1
00:00:00,000 --> 00:00:04,799
Hoe synchroniseer je kin-hin langzaam lopen en ademen?

2
00:00:04,799 --> 00:00:08,550
Je zei dat we bij elke uitademing een stap verder zouden gaan.

3
00:00:08,550 --> 00:00:15,690
Maar de meeste mensen halen twee keer adem in één stap.

4
00:00:15,690 --> 00:00:19,470
Ik wil er natuurlijk twee doen.

5
00:00:19,470 --> 00:00:24,180
Mensen verdubbelen hun ademhaling omdat ze tussen twee ademhalingen door geen pauze nemen.

6
00:00:24,180 --> 00:00:28,650
Bij langzaam kind-hin lopen is de kleine pauze van fundamenteel belang.

7
00:00:28,650 --> 00:00:33,059
Als je een beetje rust op de top van de uitademing….

8
00:00:33,059 --> 00:00:38,210
Je zit in het ritme.

9
00:00:38,210 --> 00:00:44,309
Dogen en zijn meester Nyojo zeggen….

10
00:00:44,309 --> 00:00:50,219
…dat de Boeddha zelf zei dat je je adem niet verdubbelt.

11
00:00:50,219 --> 00:00:55,050
Om dit te doen, aan het einde van de uitademing, is het noodzakelijk om een kleine pauze te houden…

12
00:00:55,050 --> 00:01:03,239
…. waarin we een halve stap vooruit zullen gaan…

13
00:01:03,239 --> 00:01:09,299
…na een korte pauze voor het inhaleren en we zijn goed in het ritme.

14
00:01:09,299 --> 00:01:14,610
We worden het zat….
veel minder.

15
00:01:14,610 --> 00:01:20,580
Tijdens sesshin, breidde ik de familie-hins uit.

16
00:01:20,580 --> 00:01:26,159
Ik had al geleefd
dit in een andere oefening met een andere meester…

17
00:01:26,159 --> 00:01:32,670
Je moest lang lopen met je ogen geblinddoekt.

18
00:01:32,670 --> 00:01:38,280
We moeten lopen zonder iets te zien.

19
00:01:38,280 --> 00:01:42,270
Het is vermoeiend, omdat je ook je handen in een positie moet plaatsen….

20
00:01:42,270 --> 00:01:45,540
…minder comfortabel dan in kind-hin.

21
00:01:45,540 --> 00:01:51,180
Als je een lange tijd familie-hin doet, wordt het moe….

22
00:01:51,180 --> 00:01:54,659
…een ander aspect van onszelf.

23
00:01:54,659 --> 00:01:59,360
Het is een actief aspect. Ik leerde in de Chinese geneeskunde….

24
00:01:59,360 --> 00:02:06,920
….dat om overmatige activiteit te genezen, je rust nodig hebt.

25
00:02:13,340 --> 00:02:18,170
Als je een lange tijd familie-hin doet, ben je uitgeput.

26
00:02:18,170 --> 00:02:23,230
Je hebt rugpijn, schouderpijn, pijn in je armen….

27
00:02:23,230 --> 00:02:29,330
Als je een uur lang zo loopt, zijn de mensen uitgeput.

28
00:02:29,330 --> 00:02:33,290
Ze zullen maar één ding willen: in zazen zitten.

29
00:02:33,290 --> 00:02:41,470
Wat een opluchting als je in zazen zit: het is complementair.

30
00:02:41,470 --> 00:02:46,549
Je zegt tegen jezelf: wat een ontspanning, wat een overgave!

31
00:02:46,549 --> 00:02:51,650
Je wilt echt in Zazen zitten.

32
00:02:51,650 --> 00:02:57,920
Na een uur in Zazen, zal je het gevoel hebben….

33
00:02:57,920 --> 00:03:02,720
….. onaangename tintelingen…

34
00:03:02,720 --> 00:03:07,040
U zult willen verhuizen en wensen dat het zou eindigen vanwege de pijn.

35
00:03:07,040 --> 00:03:11,630
Zazen en langzaam lopen zijn volledig complementair.

36
00:03:11,630 --> 00:03:14,870
Maar omdat we een korte tijd kind-hin beoefenen, realiseren we ons dat niet.

37
00:03:14,870 --> 00:03:19,430
Ik zal nu de kind-hins langer doen tijdens sesshinsµ.

38
00:03:19,430 --> 00:03:25,130
Zodat mensen graag weer gaan zitten en bepaalde dingen in zazen loslaten.

39
00:03:25,130 --> 00:03:29,350
Uw vraag is zeer goed en niet nieuw.

40
00:03:29,350 --> 00:03:38,920
Meester Dogen stelde het probleem 1.400 jaar geleden.

