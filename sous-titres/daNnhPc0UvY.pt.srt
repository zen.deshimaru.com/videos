1
00:00:05,860 --> 00:00:08,580
Deshimaru, eu sei que vou morrer em breve. 

2
00:00:08,820 --> 00:00:17,320
Chegou a hora de ordená-lo como monge a transmitir os ensinamentos de Bodhidarma em uma nova terra. 

3
00:00:31,020 --> 00:00:32,820
Eu quero ajudar a Deus. 

4
00:00:33,100 --> 00:00:34,860
Eu quero ajudar a Cristo. 

5
00:00:43,260 --> 00:00:48,000
Eu quero ajudar o verdadeiro deus. 

6
00:00:48,240 --> 00:00:52,760
Eu quero voltar para o verdadeiro deus. 

7
00:00:53,360 --> 00:01:00,360
40 anos desde a chegada do Mestre Deshimaru na Europa. 

8
00:01:00,800 --> 00:01:04,000
Deus caiu. 

9
00:01:04,320 --> 00:01:09,040
Deus está "fatigado", cansado. 

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. O Bodhidarma dos tempos modernos. 

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru chegou a Paris no verão de 1967 na estação ferroviária Gare du Nord. 

12
00:01:31,120 --> 00:01:33,480
Apenas um monge, sem dinheiro, que não fala francês. 

13
00:01:33,820 --> 00:01:36,600
Em sua bagagem, apenas a nova semente do zen. 

14
00:01:45,440 --> 00:01:49,600
A chegada 

15
00:02:59,140 --> 00:03:03,060
Se podemos voltar à história do Zen … 

16
00:03:04,380 --> 00:03:09,520
Tudo começou na Índia e depois: o que aconteceu? 

17
00:03:09,720 --> 00:03:13,340
Como foi da Índia para o Japão? 

18
00:03:13,640 --> 00:03:19,020
Índia, meditação muito profunda. 

19
00:03:19,480 --> 00:03:22,340
Mas tradicional demais. 

20
00:03:24,380 --> 00:03:29,980
Yoga não está terminado na Índia. 

21
00:03:30,720 --> 00:03:36,160
Algumas pessoas têm um alto nível espiritual, mas a maioria não. 

22
00:03:36,760 --> 00:03:43,440
Mas, yoga, terminou. não terminamos 

23
00:03:43,760 --> 00:03:48,240
Mas a terra é muito antiga. 

24
00:03:48,940 --> 00:03:53,660
Bodhidarma trouxe o zazen da Índia para a China. 

25
00:03:53,900 --> 00:03:58,280
Mas não há mais zen na Índia. 

26
00:03:58,860 --> 00:04:04,880
Então Dogen trouxe o zazen da China para o Japão. 

27
00:04:05,160 --> 00:04:11,360
Você quer dizer que o solo é muito antigo na Ásia, Japão, China ou Índia? 

28
00:04:11,740 --> 00:04:14,740
Sim, a Índia é muito antiga. 

29
00:04:14,960 --> 00:04:17,700
A China também é muito antiga. 

30
00:04:18,280 --> 00:04:20,280
O Zen está acabado na China. 

31
00:04:20,700 --> 00:04:23,940
O Japão está muito acostumado ao zen. 

32
00:04:24,680 --> 00:04:27,240
Eles só se importam com cerimônias … 

33
00:04:27,680 --> 00:04:30,380
Eles sentem falta da essência do zen. 

34
00:04:36,160 --> 00:04:48,320
Você acha que o espírito zen tende a declinar? 

35
00:04:48,640 --> 00:04:50,620
Sim Sim Sim! 

36
00:04:51,040 --> 00:04:58,720
Você acha que o espírito zen pode capturar na França, como a semente de uma flor? 

37
00:04:59,040 --> 00:05:01,120
Sim! 

38
00:05:03,020 --> 00:05:06,980
O ensino começa. 

39
00:06:24,680 --> 00:06:30,080
Por que você escolheu se estabelecer na França? 

40
00:06:30,400 --> 00:06:37,000
A França é melhor que outro país para entender o zen. 

41
00:06:37,520 --> 00:06:44,300
Muito fácil de entender o zazen. 

42
00:06:45,120 --> 00:06:46,700
Ou filosofia. 

43
00:06:46,980 --> 00:06:48,980
Para idéias. 

44
00:06:49,300 --> 00:06:52,060
Muito profundo. 

45
00:06:52,520 --> 00:06:57,980
Por exemplo, Montaigne: 

46
00:06:58,420 --> 00:07:03,900
"Eu não me importo tanto com o que sou com os outros como com o que sou comigo mesmo." 

47
00:07:05,880 --> 00:07:09,640
Descartes, Bergson … 

48
00:07:10,140 --> 00:07:14,080
Portadores de mentes, filósofos … 

49
00:07:14,800 --> 00:07:16,840
Eles podem entender o zen. 

50
00:07:18,200 --> 00:07:25,740
Eles não conhecem o zen, mas o pensamento deles é o mesmo … 

51
00:07:26,460 --> 00:07:29,760
Por que você não foi para a América? 

52
00:07:30,280 --> 00:07:33,840
Eu queria trazer o Zen pela primeira vez nos Estados Unidos. 

53
00:07:34,220 --> 00:07:41,180
Mas eles querem usar o zen para negócios, indústria, psicologia, psicanálise … 

54
00:07:41,540 --> 00:07:44,180
Eles querem usá-lo. 

55
00:07:44,340 --> 00:07:49,020
Não é verdade o zen. Não profundo. 

56
00:07:52,500 --> 00:07:56,160
Uma sangha aparece. 

57
00:09:08,270 --> 00:09:14,100
Antes, a educação é boa, mas nos tempos modernos, apenas média. 

58
00:09:14,100 --> 00:09:23,740
A essência está acabada. 

59
00:09:24,560 --> 00:09:33,700
Procuramos apenas a média … 

60
00:09:34,380 --> 00:09:41,600
Somente a essência é necessária para uma educação forte. 

61
00:09:43,380 --> 00:09:49,960
Sempre direita ou esquerda … 

62
00:09:51,040 --> 00:09:54,260
Mas devemos abraçar contradições. 

63
00:09:54,600 --> 00:09:59,720
Somente abandonando nosso ego podemos encontrar nossa verdadeira individualidade. 

64
00:10:03,920 --> 00:10:13,100
Nossa personalidade mais elevada quando seguimos a verdade cósmica, o cosmos. 

65
00:10:13,500 --> 00:10:19,480
Podemos encontrar nosso ego verdadeiro e forte. 

66
00:10:20,580 --> 00:10:25,680
Você quer tornar as pessoas fortes? 

67
00:10:26,540 --> 00:10:32,060
Sim, equilibrado e forte, pouco a pouco. 

68
00:10:32,800 --> 00:10:41,600
Precisamos encontrar a verdadeira austeridade. 

69
00:10:43,360 --> 00:10:47,600
Ensino 

70
00:12:46,000 --> 00:12:57,680
A essência do Zen é "Mushotoku", sem propósito. 

71
00:12:59,200 --> 00:13:05,520
Todas as religiões têm um objeto. 

72
00:13:06,400 --> 00:13:10,560
Você reza para ficar rico ou feliz. 

73
00:13:11,440 --> 00:13:17,040
Mas se temos um propósito, não é forte. 

74
00:13:18,400 --> 00:13:30,000
Não devemos ter nenhum propósito. 

75
00:13:31,680 --> 00:13:38,000
Você não pode se concentrar. 

76
00:13:39,360 --> 00:13:47,680
Você quer dizer que a realização adequada deve ser espontânea? 

77
00:13:48,480 --> 00:13:56,080
Devemos voltar às condições normais. 

78
00:13:57,040 --> 00:14:01,440
Satori verdadeiro, consciência transcendental … 

79
00:14:02,560 --> 00:14:07,520
é voltar às condições normais. 

80
00:14:09,440 --> 00:14:18,080
As pessoas são anormais nos tempos modernos. 

81
00:14:19,120 --> 00:14:27,120
Somente o deus verdadeiro, por exemplo, Cristo ou Buda, tinha condições normais para os seres humanos. 

82
00:14:27,920 --> 00:14:33,900
A primeira condição necessária é a própria determinação? 

83
00:14:34,340 --> 00:14:39,500
E a segunda, abandonar essa determinação? 

84
00:14:39,760 --> 00:14:45,940
Sim. A individualidade é necessária, e devemos abandoná-la. 

85
00:14:46,320 --> 00:14:50,180
Nós devemos abraçar contradições. 

86
00:14:50,920 --> 00:14:56,560
Parece que você fuma muito … 

87
00:14:57,540 --> 00:15:03,260
Nem tanto! 

88
00:15:04,220 --> 00:15:10,620
O zen fez você ser forte o suficiente para aceitar alguma coisa? 

89
00:15:11,140 --> 00:15:15,360
Tudo é possível! 

90
00:15:15,920 --> 00:15:20,240
Não é tão importante se preocupar com a vida de alguém. 

91
00:15:20,540 --> 00:15:25,960
Os europeus têm muito egoísmo. 

92
00:15:26,400 --> 00:15:31,840
Você deve aceitar tudo e ir além! 

93
00:15:32,180 --> 00:15:36,960
Mas zen não é ascetismo, nem mortificação. 

94
00:15:37,920 --> 00:15:41,460
Nós devemos pela vida cósmica. 

95
00:15:41,680 --> 00:15:45,900
Não devemos colocar limites. 

96
00:15:46,320 --> 00:15:48,780
Caso contrário, a vida se torna estreita. 

97
00:15:52,340 --> 00:15:55,880
Manter contato com o Japão 

98
00:18:11,940 --> 00:18:15,640
Sobre a educação das crianças … 

99
00:18:15,980 --> 00:18:27,960
Nos tempos modernos, nos concentramos apenas no lado intelectual da educação. 

100
00:18:28,360 --> 00:18:35,880
Também devemos ver o lado físico, a tensão muscular correta. 

101
00:18:38,600 --> 00:18:45,740
Além disso, devemos nos concentrar nos aspectos mundanos da vida cotidiana. 

102
00:18:46,100 --> 00:18:50,560
Concentração e postura física são muito importantes. 

103
00:18:50,960 --> 00:18:59,200
Não devemos ficar desleixados ao comer, por exemplo. 

104
00:18:59,740 --> 00:19:04,200
Concentrar-se na vida diária é fundamental. 

105
00:19:04,620 --> 00:19:10,920
Até a nossa postura enquanto dorme é fundamental! 

106
00:19:11,860 --> 00:19:19,460
Os professores não prestam atenção suficiente à postura física. 

107
00:19:19,800 --> 00:19:23,920
Eles só se preocupam com o lado intelectual. 

108
00:19:24,220 --> 00:19:27,679
Por que as crianças não são bem criadas? 

109
00:19:28,260 --> 00:19:30,680
A educação é fácil demais? 

110
00:19:31,200 --> 00:19:36,100
Voltamos aos animais nos tempos modernos! 

111
00:19:37,160 --> 00:19:46,380
Tudo é muito fácil … 

112
00:19:46,660 --> 00:19:50,040
Dirigimos carros e não queremos mais andar … 

113
00:19:50,420 --> 00:20:00,160
A educação familiar também é fácil. É um erro. 

114
00:20:01,360 --> 00:20:09,479
Comida quente. Muito "softy-softy"! 

115
00:20:10,600 --> 00:20:19,320
Nosso corpo fica fraco. 

116
00:20:21,540 --> 00:20:26,180
Procuramos a liberdade dos animais? 

117
00:20:26,540 --> 00:20:28,540
Animais selvagens são fortes. 

118
00:20:28,940 --> 00:20:40,340
Animais em cativeiro e bem alimentados nos jardins zoológicos são fracos. 

119
00:20:40,680 --> 00:20:42,680
Eles não têm atividade. 

120
00:20:45,200 --> 00:20:47,700
Animais selvagens são fortes! 

121
00:20:56,140 --> 00:20:59,100
A inteligência é importante. 

122
00:20:59,360 --> 00:21:01,100
Mas não é suficiente. 

123
00:21:01,420 --> 00:21:06,720
Também precisamos de um corpo forte e de uma sabedoria superior. 

124
00:21:09,080 --> 00:21:12,800
Criando o templo Gendronière 

125
00:23:45,260 --> 00:23:50,540
Você trouxe para a Europa o fruto do Zen, para que as pessoas possam prová-lo. 

126
00:23:50,820 --> 00:24:05,540
Índia, China, Japão têm a verdadeira essência da cultura espiritual. 

127
00:24:07,480 --> 00:24:18,000
Agora devemos fundir as civilizações asiática e européia. 

128
00:24:23,660 --> 00:24:31,000
Zen é a essência da cultura asiática. 

129
00:24:31,760 --> 00:24:39,580
Se você pratica o Zen, entenderá as civilizações asiáticas. 

130
00:24:40,660 --> 00:24:55,280
Haverá um renascimento espiritual através do encontro da tradição ocidental e do zen? 

131
00:24:55,620 --> 00:24:59,780
Acho que sim. Acredito que sim. 

132
00:25:00,380 --> 00:25:04,780
É importante que todos se tornem Deus. 

133
00:25:05,120 --> 00:25:07,120
Qual é o caminho para conseguir isso? 

134
00:25:07,400 --> 00:25:11,320
Se você pratica zazen, pode se tornar Deus. 

135
00:25:11,660 --> 00:25:21,300
O deus verdadeiro é o deus natural em nós? 

136
00:25:21,520 --> 00:25:29,520
Sim, todos devem criar o verdadeiro deus. 

137
00:25:31,200 --> 00:25:35,180
O último ensinamento 

138
00:25:36,600 --> 00:25:40,360
A filosofia Zen é muito ampla. 

139
00:25:40,800 --> 00:25:44,980
Você deve abraçar contradições! 

140
00:25:45,420 --> 00:25:47,440
Vida e morte, por exemplo? 

141
00:25:47,680 --> 00:25:50,400
Sim, vida e morte! 

142
00:28:10,080 --> 00:28:27,360
Dedicado a Mokudo Taisen Deshimaru (1914-1982) 

