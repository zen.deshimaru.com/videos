1
00:00:00,000 --> 00:00:03,380
Pour pratiquer le zen, il est inutile d’aller au Japon.

2
00:00:03,640 --> 00:00:07,360
Quand des gens me demandent des renseignements, à Dijon, je leur dis :

3
00:00:07,620 --> 00:00:09,240
« On ne va pas discuter du zen. »

4
00:00:09,500 --> 00:00:10,180
« Venez au dojo. »

5
00:00:10,440 --> 00:00:14,040
« Il n’y a que comme ça que vous sentirez si cela vous convient ou non. »

6
00:00:14,360 --> 00:00:18,100
Dès la première fois que l’on pratique,
on peut sentir ce que ça apporte.

7
00:00:18,680 --> 00:00:21,800
Quand vous avez envie, c’est le signe que ça vous convient.

8
00:00:22,000 --> 00:00:26,480
Il n’y a pas besoin d’explications théoriques pour savoir pourquoi on pratique.

9
00:00:26,780 --> 00:00:35,280
Quand on s’installe en zazen,tout de suite
on sent que la posture a quelque chose qui repose, qui apaise.

10
00:00:35,580 --> 00:00:40,660
La posture physique a vraiment de l’importance pour sentir que ça circule.

11
00:00:40,940 --> 00:00:46,960
Je crois que c’est surtout par rapport à
cet équilibre, qui permet de s’abandonner.

12
00:00:47,240 --> 00:00:49,280
Le zazen, c’est aussi une expérience de silence.

13
00:00:49,620 --> 00:00:59,800
Quand on est en silence à plusieurs,
si on écoute ce silence, il y a quelque chose de profond qui se transmet.

14
00:01:00,140 --> 00:01:03,140
Je repense à une infirmière qui arrive complètement stressée :

15
00:01:03,340 --> 00:01:05,840
« Je n’arrive pas à calmer le mental ! »

16
00:01:06,120 --> 00:01:08,840
Elle est sortie complètement apaisée.

17
00:01:09,120 --> 00:01:14,620
Si on arrive petit à petit à établir une respiration profonde,

18
00:01:15,140 --> 00:01:22,300
ça balaye toutes ces pensées parasites, toutes les complications.

19
00:01:22,720 --> 00:01:29,720
J’ai même du mal à dire que je suis bouddhiste, car ça fait tout de suite église.

20
00:01:30,280 --> 00:01:33,720
Alors qu’il n’y a pas besoin de ça pour pratiquer le zen…

21
00:01:34,020 --> 00:01:38,800
À notre époque, c’est dommage,
les gens veulent une méditation laïque.

22
00:01:39,220 --> 00:01:42,800
à Dijon, il y a plein de sortes de méditations proposées.

23
00:01:43,140 --> 00:01:47,360
Je ne les connais pas, je ne peux pas en parler.

24
00:01:47,560 --> 00:01:52,940
Mais personnellement, j’aime bien la pratique comme on la fait.

25
00:01:53,180 --> 00:01:54,340
Avec quelques règles des règles simples :

26
00:01:54,700 --> 00:01:56,780
Saluer quand on rentre.

27
00:01:57,080 --> 00:01:58,880
Saluer les autres personnes avec qui on pratique.

28
00:01:59,120 --> 00:02:00,620
On fait notre méditation.

29
00:02:00,880 --> 00:02:06,040
Après, on récite un sutra pour renvoyer la
pratique à tout l’univers.

30
00:02:06,300 --> 00:02:08,200
Ça fait quelque chose de complet.

31
00:02:08,520 --> 00:02:12,480
C’est une façon de ne pas pratiquer seulement pour soi.

32
00:02:13,500 --> 00:02:17,180
On n’écoute pas toujours notre confort.

33
00:02:17,460 --> 00:02:24,300
Alors que les personnes qui veulent une certaine forme de méditation, elles font déjà un choix.

34
00:02:24,560 --> 00:02:29,720
J’aime bien cette notion de « sans but », à notre époque où tout tend à un profit.

35
00:02:30,000 --> 00:02:32,180
On s’assoit en zazen, et c’est tout.

36
00:02:32,420 --> 00:02:35,660
J’ai reçu la transmission de Maître Kosen.

37
00:02:35,960 --> 00:02:38,180
Je me sens honorée, et également intimidée.

38
00:02:38,520 --> 00:02:42,800
Les gens imaginent qu’être maître, c’est être pleinement réalisé.

39
00:02:43,080 --> 00:02:45,180
Je ne me sens pas comme ça.

40
00:02:45,440 --> 00:02:50,720
Je vis cette transmission comme l’envie de continuer à transmettre et d’aider la sangha à grandir.

41
00:02:51,040 --> 00:02:56,300
Notre sangha, ce sont toutes les
personnes qui pratiquent avec Maître Kosen.

42
00:02:56,980 --> 00:03:04,360
Si vous avez aimé cette vidéo, n’hésitez pas à la liker et à vous abonner à la chaîne !
