1
00:00:11,160 --> 00:00:13,720
Ciao, come stai?

2
00:00:16,560 --> 00:00:21,280
La postura, insegnata dal maestro Deshimaru...

3
00:00:25,000 --> 00:00:28,460
Lo chiamava il metodo Zen...

4
00:00:31,920 --> 00:00:35,640
Iniziate con una base stabile.

5
00:00:39,600 --> 00:00:43,160
Il che significa che entrambe le ginocchia toccano terra.

6
00:00:47,480 --> 00:00:50,540
E si mette il perineo sullo zafu [cuscino].

7
00:00:53,860 --> 00:00:58,940
Se fai il loto completo, metti prima una gamba.

8
00:01:04,220 --> 00:01:07,300
Poi l’altro.

9
00:01:07,900 --> 00:01:12,140
Se si ha una buona stabilità, si può davvero lasciare andare il corpo.

10
00:01:12,170 --> 00:01:15,220
Come si dice, lascia andare la mente-corpo.

11
00:01:22,460 --> 00:01:24,320
Il maestro Deshimaru ha detto:

12
00:01:25,020 --> 00:01:28,640
"Lo Zen è il nuovo umanesimo per il XXI secolo".

13
00:01:29,560 --> 00:01:31,440
"Deve essere sempre fresco".

14
00:01:31,480 --> 00:01:34,000
"Se pratichi il vero zazen,"

15
00:01:34,060 --> 00:01:36,600
"mento rimboccato".

16
00:01:37,060 --> 00:01:39,980
La "spina dorsale allungata".

17
00:01:40,580 --> 00:01:42,580
"solo zazen".

18
00:01:43,620 --> 00:01:46,270
"il resto non conta più così tanto".

19
00:01:46,320 --> 00:01:49,600
All’inizio di zazen, ci sono tre campane.

20
00:01:52,040 --> 00:01:56,060
E poi, quando c’è kin-hin, la meditazione è accesa,

21
00:02:00,530 --> 00:02:03,580
Ci sono due campane.

22
00:02:15,980 --> 00:02:18,940
Allungare bene il ginocchio della gamba anteriore.

23
00:02:22,900 --> 00:02:28,340
Premere il terreno con la radice dell’alluce del piede anteriore.

24
00:02:34,580 --> 00:02:36,580
Rilassate le spalle.

25
00:02:38,260 --> 00:02:41,480
Alla fine della scadenza,

26
00:02:44,100 --> 00:02:46,100
Inspiriamo,

27
00:02:47,560 --> 00:02:51,900
e facciamo un piccolo passo avanti.

28
00:02:56,820 --> 00:03:00,060
Ricomincia da capo, espira...

29
00:03:01,360 --> 00:03:05,180
Mettete tutto il vostro peso corporeo sulla gamba anteriore.

30
00:03:10,920 --> 00:03:13,760
Il pollice della mano sinistra

31
00:03:16,120 --> 00:03:19,960
lo stiamo stringendo in un pugno.

32
00:03:22,400 --> 00:03:25,300
Poi prendiamo la mano destra

33
00:03:26,960 --> 00:03:29,480
e noi copriamo la mano sinistra.

34
00:03:36,140 --> 00:03:40,040
Fare zazen con lo zoom è molto pratico...

35
00:03:40,260 --> 00:03:43,710
soprattutto in questi tempi difficili.

36
00:03:48,280 --> 00:03:53,440
Ma di solito è complementare alla pratica in un vero e proprio dojo.

37
00:03:58,860 --> 00:04:03,340
Ovviamente, dedichiamo questa pratica che stiamo facendo insieme

38
00:04:12,540 --> 00:04:19,560
A tutte le persone che si dedicano alla guarigione e alla salvezza degli altri.

39
00:04:34,720 --> 00:04:36,720
Ciao, Toshi. Salve.

40
00:04:36,780 --> 00:04:38,720
Buongiorno, maestro.

41
00:04:40,680 --> 00:04:43,940
Ah, eccolo! Ciao!

42
00:04:46,100 --> 00:04:48,100
Ciao, Ingrid.

43
00:04:51,840 --> 00:04:54,640
Quella è Grabriela, ma quale?

44
00:04:57,460 --> 00:05:00,520
Ah, non ti avevo riconosciuto con gli occhiali!

45
00:05:06,020 --> 00:05:09,160
Sono il traduttore, non mi riconosci?
