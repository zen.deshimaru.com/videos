1
00:00:10,260 --> 00:00:23,160
Todos os dias, em todo o mundo, os membros da sangha Kosen praticam zazen juntos. 

2
00:00:24,360 --> 00:00:28,520
Já contei sobre a infância do mestre Deshimaru. 

3
00:00:28,820 --> 00:00:30,820
Por suas dúvidas, sua pesquisa … 

4
00:00:30,820 --> 00:00:35,760
Até conhecer Kodo Sawaki e encontrar sua pose de zazen. 

5
00:00:36,040 --> 00:00:39,100
Encontrando sua própria pose de zazen: 

6
00:00:40,240 --> 00:00:41,860
"Que prazer!" 

7
00:00:42,240 --> 00:00:46,100
Eu acredito que aqueles que conheceram sua atitude zazen … 

8
00:00:46,100 --> 00:00:49,120
Não desperdiçaram seu tempo neste planeta. 

9
00:00:49,500 --> 00:00:52,540
É um presente imensurável .. 

10
00:00:52,540 --> 00:00:54,239
Mesmo que nesta atitude … 

11
00:00:54,540 --> 00:00:59,920
Há também sofrimento e medo. 

12
00:01:00,320 --> 00:01:01,960
Tudo está presente nessa atitude. 

13
00:01:01,960 --> 00:01:06,160
Mas você olha de cima. 

14
00:01:06,500 --> 00:01:10,740
Antes de aprender as lições de Kodo Sawaki .. 

15
00:01:12,040 --> 00:01:15,980
Mestre Deshimaru havia estudado a Bíblia. 

16
00:01:17,360 --> 00:01:21,020
Ele estava um pouco apaixonado por uma garota holandesa .. 

17
00:01:23,020 --> 00:01:24,820
Filha de um ministro. 

18
00:01:24,960 --> 00:01:28,380
Ele aproveitou a oportunidade para estudar a Bíblia e o inglês. 

19
00:01:28,720 --> 00:01:32,140
Na verdade, ele estudou inglês a partir do estudo da Bíblia. 

20
00:01:32,140 --> 00:01:37,060
Ele visitou igrejas cristãs e cantou hinos. 

21
00:01:37,220 --> 00:01:41,360
Estou lhe dizendo isso porque é Páscoa. 

22
00:01:41,660 --> 00:01:51,880
E agora Jesus está entre o túmulo e a quarta ou quinta dimensão. 

23
00:01:52,340 --> 00:01:56,580
Sensei nos disse que quando ele veio para a França .. 

24
00:01:57,000 --> 00:02:02,360
Ele trouxe sua Bíblia e os hinos de sua juventude. 

25
00:02:02,680 --> 00:02:07,940
Durante uma sessão de Páscoa, ele nos deu um beijo sobre Cristo. 

26
00:02:08,280 --> 00:02:11,720
De acordo com o Evangelho de Lucas .. 

27
00:02:11,720 --> 00:02:15,620
Para que Cristo … 

28
00:02:16,140 --> 00:02:20,240
Poderia falar para que Pôncio Pilatus fosse julgado. 

29
00:02:20,600 --> 00:02:26,240
Pôncio Pilatos chamou os principais sacerdotes. 

30
00:02:26,240 --> 00:02:28,820
E questionou Jesus na presença deles. 

31
00:02:29,180 --> 00:02:30,340
Ele lhes diz: 

32
00:02:30,600 --> 00:02:33,020
"Você me apresentou esse homem como … 

33
00:02:33,020 --> 00:02:39,820
Alguém que está liderando as pessoas na direção errada ". 

34
00:02:40,280 --> 00:02:47,640
"Eu o questionei e não encontrei nada de ruim neste homem." 

35
00:02:48,000 --> 00:02:50,260
"Eu não encontrei nenhum dos motivos pelos quais você o está acusando." 

36
00:02:50,580 --> 00:02:56,940
"Herodes também não encontrou motivos para cobrar". 

37
00:02:57,260 --> 00:03:02,620
"Concluo que este homem não fez nada para merecer a pena de morte." 

38
00:03:03,240 --> 00:03:06,480
Naquele momento, o povo exclamou em 1 voz: 

39
00:03:06,480 --> 00:03:15,100
"Morto para Jesus, solte Barrabás!" 

40
00:03:16,240 --> 00:03:24,280
"Barrabás foi preso por conduta e assassinato desordeiros." 

41
00:03:24,620 --> 00:03:29,100
Pontius Pilatus ofereceu .. 

42
00:03:29,100 --> 00:03:32,200
Para libertar um dos prisioneiros. 

43
00:03:32,800 --> 00:03:38,480
Mas o povo disse: "Barrabás grátis!" 

44
00:03:38,940 --> 00:03:44,260
Pôncio Pilatos se dirige novamente às pessoas: 

45
00:03:44,260 --> 00:03:49,640
E ele os diz novamente e fala ao povo: 

46
00:03:49,960 --> 00:03:53,820
"Seja razoável, Jesus não fez nada de errado .. 

47
00:03:53,820 --> 00:03:55,800
Ele acabou de falar, só isso! " 

48
00:03:55,800 --> 00:03:59,620
Todo mundo gritou: "Crucifique-o!" 

49
00:03:59,860 --> 00:04:02,320
Pôncio Pilatos disse finalmente: 

50
00:04:02,620 --> 00:04:04,340
"Como você deseja .. 

51
00:04:04,340 --> 00:04:06,300
Não é problema meu .. " 

52
00:04:07,200 --> 00:04:11,640
E em resposta ele falou a famosa frase: 

53
00:04:11,640 --> 00:04:13,500
"Eu era minhas mãos na inocência!" 

54
00:04:13,560 --> 00:04:17,540
Esta expressão significa que .. 

55
00:04:17,540 --> 00:04:19,960
Você deve lavar as mãos o mais rápido possível. 

56
00:04:20,140 --> 00:04:22,320
Para evitar o coronavírus. 

57
00:04:22,400 --> 00:04:24,920
Você tem que dizer toda vez: "Eu lavo minhas mãos na inocência!" 

58
00:04:25,240 --> 00:04:29,880
Este é um ensinamento de Deshimaru, ele diz isso. 

59
00:04:30,360 --> 00:04:35,760
"Assim, Jesus foi condenado pela opinião pública." 

60
00:04:36,020 --> 00:04:39,140
"A psicologia em massa matou a Cristo". 

61
00:04:39,380 --> 00:04:44,200
"Mimetismo, recusa em se questionar … 

62
00:04:44,200 --> 00:04:46,160
Isso matou a Cristo ". 

63
00:04:46,300 --> 00:04:48,940
A multidão, a multidão .. 

64
00:04:48,940 --> 00:04:54,800
Contém em si uma energia poderosa .. 

65
00:04:54,800 --> 00:04:57,720
O que pode ser negativo e perigoso. 

66
00:04:57,800 --> 00:05:02,900
É diferente quando nos reunimos para fazer zazen. 

67
00:05:03,180 --> 00:05:05,640
Cada um de nós se olha .. 

68
00:05:05,640 --> 00:05:11,820
Enquanto em harmonia com o grupo. 

69
00:05:12,160 --> 00:05:16,260
Isso é especialmente importante por enquanto com o Zazoom! 

70
00:05:16,680 --> 00:05:20,520
Por exemplo, queimei incenso no meu quarto. 

71
00:05:20,920 --> 00:05:25,260
O Sensei costumava dizer: "No zazen, você está sozinho." 

72
00:05:25,640 --> 00:05:28,220
Mas se você pratica zazen em um dojo .. 

73
00:05:28,220 --> 00:05:31,460
Estamos em um grupo, mesmo quando estamos sozinhos. 

74
00:05:31,820 --> 00:05:35,260
Mas então não há confusão emocional. 

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki costumava dizer sobre isso: 

76
00:05:38,320 --> 00:05:41,640
"Não devemos viver em loucura de grupo". 

77
00:05:42,020 --> 00:05:45,680
"E não guarde essa aberração pela experiência real." 

78
00:05:45,940 --> 00:05:48,480
"Este é o destino das pessoas comuns .. 

79
00:05:48,480 --> 00:05:50,540
Quem não tem outra maneira de ver isso .. 

80
00:05:50,860 --> 00:05:54,180
Depois, com os olhos da imbecilidade coletiva ". 

81
00:05:54,700 --> 00:06:02,080
O Buda dava uma aula pública uma vez por semana. 

82
00:06:02,600 --> 00:06:05,620
Todos os discípulos, monges e leigos .. 

83
00:06:05,620 --> 00:06:08,360
Reunidos fora .. 

84
00:06:08,360 --> 00:06:11,000
Certamente em lugares privilegiados. 

85
00:06:11,880 --> 00:06:14,960
O Buda deu um beijo. 

86
00:06:15,320 --> 00:06:16,980
E todo mundo estava no zazen .. 

87
00:06:17,960 --> 00:06:19,880
E ouviu ele .. 

88
00:06:19,980 --> 00:06:23,260
E então todos seguiram seu caminho novamente. 

89
00:06:23,760 --> 00:06:25,560
Todo mundo praticou sozinho .. 

90
00:06:25,560 --> 00:06:27,960
Uma semana, um mês 

91
00:06:28,240 --> 00:06:30,100
E depois voltou regularmente .. 

92
00:06:30,280 --> 00:06:32,680
para ouvir as palavras do Buda. 

93
00:06:32,680 --> 00:06:37,840
Dojos só apareceram na China. 

94
00:06:38,180 --> 00:06:40,900
E começamos a praticar em grupo .. 

95
00:06:40,900 --> 00:06:42,560
Com uma ordem e regras. 

96
00:06:44,580 --> 00:06:48,280
E então o grupo é muito poderoso .. 

97
00:06:48,800 --> 00:06:50,960
Bom e ruim. 

98
00:06:51,200 --> 00:06:53,020
Sensei realmente disse: 

99
00:06:53,020 --> 00:06:57,240
"Jesus Cristo deu a seu corpo o gosto da morte." 

100
00:06:57,620 --> 00:07:01,700
"No lago profundo do nada e do desespero." 

101
00:07:02,180 --> 00:07:06,420
"Ele se tornou um símbolo da vida eterna." 

102
00:07:06,740 --> 00:07:09,180
"Sua vida é idêntica à de um koan zen." 

103
00:07:09,380 --> 00:07:11,380
Foi o que o Sensei disse. 

