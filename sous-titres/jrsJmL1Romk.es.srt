1
00:00:08,039 --> 00:00:12,389
Dondequiera que estés.

2
00:00:21,107 --> 00:00:23,587
Siéntese en silencio.

3
00:00:23,995 --> 00:00:26,755
Estar totalmente presente.

4
00:00:27,698 --> 00:00:32,578
Quienquiera que seas.

5
00:00:39,450 --> 00:00:41,700
Salga de su rutina diaria.

6
00:00:41,790 --> 00:00:43,254
Practica zazen.

7
00:00:43,401 --> 00:00:45,881
con la Kosen Sangha.

8
00:00:51,238 --> 00:00:57,194
Eventos relacionados con esta pandemia

9
00:00:58,324 --> 00:01:02,861
certificar el Zen de Dogen

10
00:01:03,081 --> 00:01:05,538
y el Zen de Deshimaru.

11
00:01:05,862 --> 00:01:09,802
La impermanencia, la vivimos.

12
00:01:11,770 --> 00:01:16,620
La interdependencia es algo que también experimentamos.

13
00:01:21,799 --> 00:01:24,759
El único momento que importa,

14
00:01:26,209 --> 00:01:29,069
es ahora mismo.

15
00:01:29,259 --> 00:01:30,919
Aquí y ahora.

16
00:01:31,008 --> 00:01:33,698
No importa lo que hagas.

17
00:01:36,710 --> 00:01:40,610
Vuelve tu atención hacia adentro.

18
00:01:40,908 --> 00:01:43,698
Sólo siéntate.

19
00:01:44,461 --> 00:01:47,161
Recto y silencioso.

20
00:01:48,301 --> 00:01:49,821
El Buda,

21
00:01:50,388 --> 00:01:53,538
nuestro maestro interior.

22
00:01:53,942 --> 00:01:56,332
El Dharma, el camino, la práctica.

23
00:01:56,671 --> 00:01:59,091
La sangha, la sangha virtual.

24
00:02:03,066 --> 00:02:07,486
Cuando puedas.

25
00:02:12,081 --> 00:02:16,081
Únete a nosotros aquí y ahora.

26
00:02:16,630 --> 00:02:20,630
Sin ego.

27
00:02:20,769 --> 00:02:24,269
Sin objetivo.

28
00:02:52,434 --> 00:02:54,384
¡Buenos días a todos!

29
00:02:54,904 --> 00:02:57,614
¡Y gracias por hacer zazen juntos!
