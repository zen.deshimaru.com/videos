1
00:00:38,480 --> 00:00:40,340
A sessão começará, 

2
00:00:42,060 --> 00:00:45,680
antes de se sentar, oriente bem sua câmera. 

3
00:00:48,700 --> 00:00:54,020
Você não deve falar, não é a hora, fazemos zazen em silêncio. 

4
00:01:00,860 --> 00:01:05,320
Instale-se. 

5
00:01:05,320 --> 00:01:10,240
Empurre a terra com as raízes 

6
00:01:10,240 --> 00:01:18,780
e ao mesmo tempo a coluna é esticada em direção ao céu. 

7
00:01:21,600 --> 00:01:24,320
Respirar é nasal, 

8
00:01:24,320 --> 00:01:27,560
você respira pelas narinas 

9
00:01:27,560 --> 00:01:29,540
para quem tem 

10
00:01:29,540 --> 00:01:34,820
e você expira profundamente 

11
00:01:34,820 --> 00:01:38,460
e é aí que você solta a tensão 

12
00:01:45,340 --> 00:01:50,960
Zana, você está respirando com dificuldade, seu corpo está se movendo demais. 

13
00:01:50,960 --> 00:01:56,810
Eu pensei que você tinha que subir e descer. 

14
00:01:56,810 --> 00:02:00,240
Acima de tudo, fique quieto, Zana. Silêncio! 

15
00:02:04,360 --> 00:02:06,840
Você tem boas posturas. 

16
00:02:11,880 --> 00:02:19,940
Mesmo se for um dojo virtual, você precisa fazer um mínimo de esforço antes de vir, 

17
00:02:19,940 --> 00:02:23,940
sem nomear ninguém, por exemplo, El Broco. 

18
00:02:24,740 --> 00:02:29,460
Você penteava seu cabelo com um foguete esta manhã? 

19
00:02:30,720 --> 00:02:33,220
Também é necessário estilizar as raízes. 

20
00:02:36,000 --> 00:02:39,080
Existem posturas muito bonitas. 

21
00:02:42,740 --> 00:02:44,380
Sem medo 

22
00:02:45,100 --> 00:02:51,660
nestes tempos difíceis, não se deixe absorver do exterior. 

23
00:02:52,160 --> 00:02:57,940
Refocalize, seja seu próprio mestre, 

24
00:02:59,480 --> 00:03:04,940
e como Ronaldo disse: 

25
00:03:38,340 --> 00:03:41,960
Não se deixe enganar por suas emoções, 

26
00:03:44,640 --> 00:03:48,620
vendo isso, como você se sente? 

27
00:03:55,480 --> 00:03:57,640
E vendo isso? 

28
00:04:07,160 --> 00:04:10,300
Não busque alegria, 

29
00:04:12,040 --> 00:04:14,960
não fuja do medo 

30
00:04:17,460 --> 00:04:20,620
Porque como Maître Deshimaru disse: 

31
00:04:22,380 --> 00:04:26,060
"Mesmo se você gosta de flores, elas murcham. 

32
00:04:27,640 --> 00:04:32,780
Mesmo se você não gosta de ervas daninhas, elas crescem. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Sim! Sim! 

35
00:04:55,600 --> 00:04:58,700
Ainda não começamos? Podemos nos mudar? 

36
00:04:58,700 --> 00:05:02,640
Começaremos novamente com Radis e Rose, que estão em baixo. 

37
00:05:03,780 --> 00:05:09,160
Eu não entendo, devo parar a câmera? O que deve ser feito? Ambos ao mesmo tempo? 

38
00:05:10,820 --> 00:05:15,340
Quem falou? É Muriel, não, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, observe sua cenoura, porque se você se mexer demais, isso fará com que você queira vomitar. 

