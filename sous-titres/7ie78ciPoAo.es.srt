1
00:00:00,100 --> 00:00:02,020
El zen es zazen! 

2
00:00:02,220 --> 00:00:04,920
Zazen se sienta como un Buda. 

3
00:00:05,160 --> 00:00:08,400
Toma la pose original de Buda. 

4
00:00:08,620 --> 00:00:09,920
Toma su corazón. 

5
00:00:10,140 --> 00:00:12,360
Siéntate en esta posición. 

6
00:00:12,660 --> 00:00:16,000
Y poco a poco olvidemos que somos un cuerpo doloroso. 

7
00:00:16,320 --> 00:00:18,840
Todo retorcido, con una mente complicada. 

8
00:00:19,080 --> 00:00:23,120
Y que ocurra la transmutación, la alquimia del zazen. 

9
00:00:23,760 --> 00:00:25,700
Solo las personas pueden hacer ejercicio. 

10
00:00:26,060 --> 00:00:33,200
Es nuestra evolución a lo largo de cientos de miles de años lo que hace posible practicar esta actitud. 

11
00:00:33,520 --> 00:00:36,700
Equilibrio, estabilidad, calma. 

12
00:00:37,000 --> 00:00:40,200
Estírate, ábrete, olvídate. 

13
00:00:40,480 --> 00:00:46,960
Unidad con todos los seres, con todo, con todo el universo. 

14
00:00:47,740 --> 00:00:51,760
El Maestro Kosen dice que el zazen es patrimonio mundial de la humanidad. 

15
00:00:52,040 --> 00:00:54,600
No vamos a obligar a alguien a hacer zazen. 

16
00:00:54,860 --> 00:00:56,220
Y a veces no es el momento adecuado. 

17
00:00:56,460 --> 00:00:57,960
Podemos sentarnos en algún momento. 

18
00:00:58,220 --> 00:01:02,940
Y extrañar lo que podría significar para nuestras vidas. 

19
00:01:03,220 --> 00:01:07,460
Y luego, en otro momento, de repente, está allí. 

20
00:01:07,680 --> 00:01:09,560
Hay una pregunta y una respuesta. 

21
00:01:09,760 --> 00:01:11,120
Eso es exactamente lo que debemos practicar. 

22
00:01:11,360 --> 00:01:17,460
Hacer zazen solo una vez, incluso si no es lo tuyo, si eres receptivo, si te apetece, te da información. 

23
00:01:18,700 --> 00:01:21,140
Hay muchas prácticas diferentes. 

24
00:01:21,380 --> 00:01:22,880
Lo cual es definitivamente bueno. 

25
00:01:23,160 --> 00:01:25,700
No puedes querer poner las cosas juntas. 

26
00:01:26,000 --> 00:01:29,940
Zazen es una práctica bastante exigente. 

27
00:01:30,240 --> 00:01:32,700
No solo te pones de pie. 

28
00:01:33,040 --> 00:01:38,440
La postura y el estado de ánimo acompañante son esenciales. 

29
00:01:39,160 --> 00:01:43,900
Pero el zazen tiene su propia vida, su propio funcionamiento. 

30
00:01:44,160 --> 00:01:46,220
También se le llama "meditación". 

31
00:01:46,420 --> 00:01:47,980
Pero es solo una palabra. 

32
00:01:48,320 --> 00:01:51,580
Lo llamamos "zazen", "actitud de Buda". 

33
00:01:51,800 --> 00:01:53,700
Son solo palabras. 

34
00:01:53,940 --> 00:01:56,180
Pero esa es la actitud que practicamos. 

35
00:01:56,840 --> 00:01:59,220
No nos llamamos maestros o maestros. 

36
00:01:59,500 --> 00:02:03,040
Incluso si ha tomado 1, 2, 3 cursos. 

37
00:02:03,300 --> 00:02:06,140
Hay una relación íntima, sigues a un maestro. 

38
00:02:06,380 --> 00:02:09,460
No lo seguimos donde quiera que vaya. 

39
00:02:09,780 --> 00:02:12,920
Seguimos su enseñanza, su mente. 

40
00:02:13,200 --> 00:02:14,760
Hay zazen en sí. 

41
00:02:15,000 --> 00:02:22,300
Entonces, no sigas tu propia cosa, tu propia idea. 

42
00:02:22,580 --> 00:02:25,120
Pero para seguir algo más grande. 

43
00:02:25,560 --> 00:02:29,480
Algo que se extiende por generaciones. 

44
00:02:29,860 --> 00:02:34,060
Desde el Buda histórico y antes que él. 

45
00:02:34,380 --> 00:02:36,960
Hay otros Budas que lo han precedido. 

46
00:02:37,240 --> 00:02:40,020
Otros que se sentaron en la pose. 

47
00:02:40,360 --> 00:02:42,720
Otros que han alcanzado el camino. 

48
00:02:43,500 --> 00:02:45,520
¡Y es serio! 

49
00:02:45,840 --> 00:02:59,580
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

