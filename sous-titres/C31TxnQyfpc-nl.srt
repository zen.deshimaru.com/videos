1
00:00:00,010 --> 00:00:11,610
De belangrijkste punten zijn: het bassin,

2
00:00:11,740 --> 00:00:17,895
de vijfde lendenwervel,

3
00:00:18,635 --> 00:00:24,900
de eerste rugwervels, degene die uitsteekt.

4
00:00:26,940 --> 00:00:31,820
En dan zie je de morfologie, iedereen is anders.

5
00:00:31,960 --> 00:00:38,620
Laten we zeggen dat ik denk dat ze haar nek wat meer moet strekken.

6
00:00:41,210 --> 00:00:48,145
Je moet altijd twee punten raken.

7
00:00:48,645 --> 00:00:52,262
Behalve als het de top van de schedel is.

8
00:00:52,602 --> 00:00:55,460
Een yin en een yang.

9
00:00:55,640 --> 00:00:58,675
Ik maak het me gemakkelijk, ik ga op mijn knieën.

10
00:00:59,855 --> 00:01:03,020
Ik ga juist deze wervel aanraken.

11
00:01:08,280 --> 00:01:09,815
Ik ga de kin met twee vingers aanraken.

12
00:01:09,975 --> 00:01:11,790
Ik doe hem dit niet aan.

13
00:01:14,170 --> 00:01:16,825
We geven aanwijzingen.

14
00:01:16,855 --> 00:01:19,540
Maar als ze het echt goed hebben,

15
00:01:19,620 --> 00:01:21,635
als we de persoon zijn focus laten houden,

16
00:01:21,865 --> 00:01:24,132
het heeft een zeer diepgaand effect.

17
00:01:25,602 --> 00:01:30,670
Dus ik ga de twee tegengestelde punten doordrukken.

18
00:01:30,753 --> 00:01:35,976
We kunnen het ook zo doen.

19
00:01:36,156 --> 00:01:39,650
Ik weet dat ze een operatie heeft gehad...

20
00:01:41,560 --> 00:01:43,680
We kunnen het ook zo doen.

21
00:01:43,785 --> 00:01:46,570
Sensei [Deshimaru] deed dat vaak.

22
00:01:48,030 --> 00:01:53,480
Je kunt een specifiek punt raken waar de persoon zwakheden heeft.

23
00:01:53,740 --> 00:01:56,860
Bijvoorbeeld naar de maag of de lever.

24
00:02:00,470 --> 00:02:03,900
Kijk altijd naar de kromming.

25
00:02:06,820 --> 00:02:10,385
Vaak is het over- of onderbelicht.

26
00:02:10,575 --> 00:02:15,140
Vergeet niet dat je een kyosaku in je hand hebt.

27
00:02:18,340 --> 00:02:23,950
Er is geen kans dat we gaan sleutelen.

28
00:02:25,680 --> 00:02:30,245
Je moet je kyosaku houden.

29
00:02:32,525 --> 00:02:36,740
We kunnen dat doen met de kyosaku, het maakt niet uit.

30
00:02:36,875 --> 00:02:39,895
Dat kunnen we ook.

31
00:02:40,010 --> 00:02:45,140
Als de persoon te voorovergebogen is.

32
00:02:48,195 --> 00:02:50,880
Dat kunnen we ook.

33
00:02:52,790 --> 00:02:56,313
Je moet een correctie genereren die de persoon zal nemen.

34
00:02:56,405 --> 00:02:59,930
Het is belangrijk om dat ook te doen.

35
00:03:04,960 --> 00:03:08,450
Het is erg delicaat en ik stoor de meditatie niet.

36
00:03:11,440 --> 00:03:13,570
We kunnen ook kyosaku gebruiken.

37
00:03:16,235 --> 00:03:19,825
We nemen de brede kant naar beneden.

38
00:03:20,330 --> 00:03:23,325
Ga op je knieën zitten.

39
00:03:24,216 --> 00:03:27,264
En we gaan het bij de boog plaatsen.

40
00:03:29,334 --> 00:03:31,587
Dat geeft je een indicatie.

41
00:03:32,747 --> 00:03:37,940
Heel negatief, omdat je het gevoel hebt dat je helemaal niet eerlijk bent.

42
00:03:38,150 --> 00:03:41,930
Omdat de stok recht is, en de kolom niet recht is.

43
00:03:48,600 --> 00:03:50,800
De kolom is nooit recht.

44
00:03:51,130 --> 00:03:52,995
Het heeft een lichte kromming.

45
00:03:53,175 --> 00:03:59,240
Als je dit opdoet, voel je je vast een beetje verwrongen.

46
00:03:59,400 --> 00:04:06,705
Als je het op je rug legt...

47
00:04:06,955 --> 00:04:09,490
Het is een beetje Spartaans.

48
00:04:13,940 --> 00:04:16,660
Je hebt een hele mooie houding, Cristina!

49
00:04:22,280 --> 00:04:25,430
Hij heeft een geweldige houding.

50
00:04:28,960 --> 00:04:32,320
Er zijn kleine dingen die je niet beseft.

51
00:04:32,480 --> 00:04:34,340
Bijvoorbeeld, de duim daar.

52
00:04:36,040 --> 00:04:39,820
Het moet rechtdoorzee zijn.

53
00:04:41,850 --> 00:04:44,710
Het is iets wat hij zich niet realiseert.

54
00:04:44,890 --> 00:04:50,520
Maar je gaat nog een jaar, twee jaar, en je duimen zijn in orde.

55
00:04:50,855 --> 00:04:53,735
Het is het eerste wat ik zie.

56
00:04:55,180 --> 00:04:58,900
Zorg ervoor dat je duimen tegen je navel aanliggen.

57
00:05:10,260 --> 00:05:12,600
Een beetje te veel in het achterhoofd.

58
00:05:14,440 --> 00:05:20,300
Het hoofd gaat een beetje naar voren.

59
00:05:28,920 --> 00:05:32,970
Ik ben helemaal gek op osteopathie.

60
00:05:34,420 --> 00:05:36,260
Oké!

61
00:05:38,315 --> 00:05:42,875
Maak je daar wat losser.

62
00:05:53,292 --> 00:05:54,862
Heel mooi!

63
00:05:56,060 --> 00:06:00,290
Als je gassho doet, doe het dan zo.

64
00:06:04,605 --> 00:06:06,335
De houding is prachtig.

65
00:06:08,920 --> 00:06:11,120
Een beetje te welbespraakt.

66
00:06:11,225 --> 00:06:14,765
Maak dat je wegkomt.

67
00:06:24,537 --> 00:06:28,737
Op dat moment, omdat we het willen volhouden,

68
00:06:35,023 --> 00:06:37,393
Nee, nee, stil!

69
00:06:37,591 --> 00:06:38,591
Voilà!

70
00:06:38,780 --> 00:06:39,780
Perfect!

71
00:06:45,450 --> 00:06:49,410
Vergeet niet dat het linkerhand in rechterhand is.

72
00:06:51,200 --> 00:06:52,950
Ontspan je handen!

73
00:06:53,010 --> 00:06:55,540
Ontspan je vingers.

74
00:06:56,150 --> 00:06:58,530
Je zet ze op elkaar.

75
00:07:03,395 --> 00:07:06,015
Het is een beetje gespannen, maar toch...

76
00:07:08,797 --> 00:07:09,947
Hier.

77
00:07:16,500 --> 00:07:19,185
Wat ik dan zal doen...

78
00:07:19,275 --> 00:07:21,870
Ik zie veel spanning. Het is normaal.

79
00:07:21,910 --> 00:07:24,435
Want in het begin moeten we het doen.

80
00:07:24,585 --> 00:07:27,130
Doe jij dat maar.

81
00:07:30,560 --> 00:07:33,810
Handen zijn nu vrij goed.

82
00:07:34,930 --> 00:07:38,150
Relax, ze hoeven niet te gespannen te zijn.

83
00:07:39,690 --> 00:07:40,470
Flexibel.

84
00:07:40,560 --> 00:07:42,110
Dat is goed, geweldig!

85
00:07:48,975 --> 00:07:54,395
Ik zou een beetje de nadruk leggen op de vijfde wervel.

86
00:08:03,997 --> 00:08:07,027
Kijk, ik voel me een beetje gespannen.

87
00:08:10,253 --> 00:08:12,099
Oké!

88
00:08:12,829 --> 00:08:15,839
Maak je ellebogen los.

89
00:08:19,849 --> 00:08:27,759
Sensei, niet op beginners, maar hij deed dat veel.

90
00:08:27,894 --> 00:08:31,684
Zeker, hij was hard aan het pushen.

91
00:08:33,682 --> 00:08:37,282
Voorlopig is dat niet nodig.

92
00:08:37,941 --> 00:08:39,471
Oké. Oké.

93
00:08:40,660 --> 00:08:42,680
Je kunt gaan ademen.

94
00:08:42,790 --> 00:08:44,875
Haal even adem.

95
00:08:45,065 --> 00:08:46,577
Volg je adem.

96
00:08:46,727 --> 00:08:48,010
Geniet ervan.

97
00:08:54,425 --> 00:08:55,625
Dat is heel goed.

98
00:08:55,717 --> 00:08:58,987
Is dit je eerste sesshin?

99
00:09:00,103 --> 00:09:02,613
Bravo!

100
00:09:02,853 --> 00:09:04,530
Laat me je iets vragen:

101
00:09:04,608 --> 00:09:07,268
Om te ademen,

102
00:09:07,380 --> 00:09:12,640
soms zuig ik het op en weet ik niet hoe ik moet stoppen...

103
00:09:20,830 --> 00:09:22,820
Zolang je je goed voelt...

104
00:09:26,310 --> 00:09:29,775
Dat is mijn punt, er zijn andere methoden...

105
00:09:29,935 --> 00:09:32,860
Maar in zazen, om te ademen.

106
00:09:33,060 --> 00:09:36,780
Je moet je goed voelen. Dwing het niet.

107
00:09:37,130 --> 00:09:40,530
Zolang je je goed voelt, ga je door.

108
00:09:45,380 --> 00:09:48,570
Je komt op adem op het moment dat je het voelt.

109
00:09:48,731 --> 00:09:51,461
Het is jouw lichaam dat het moet dicteren.

110
00:10:25,390 --> 00:10:27,310
Houding in het algemeen

111
00:11:05,935 --> 00:11:08,115
Oké!

112
00:11:15,740 --> 00:11:18,100
Je moet naar de hoofdpunten kijken:

113
00:11:18,190 --> 00:11:21,200
vijfde lendenwervel, eerste rugwervel,

114
00:11:21,266 --> 00:11:24,790
hoofd, armen niet te strak.

115
00:11:26,070 --> 00:11:31,060
We kunnen de persoon helpen door een positie van de handen.

116
00:11:31,240 --> 00:11:34,280
Ik wil graag een kind-hin houding zien.

117
00:11:41,400 --> 00:11:44,925
Er is niets anders dan een goede houding.

118
00:11:46,485 --> 00:11:50,500
Hij heeft een heel mooie houding, heel natuurlijk.

119
00:11:51,230 --> 00:11:59,200
Op één detail na: de pink moet strak zitten.

120
00:11:59,785 --> 00:12:01,855
Mensen vergeten vaak om het aan te scherpen.

121
00:12:01,967 --> 00:12:03,822
Sensei zei altijd:

122
00:12:03,982 --> 00:12:08,977
de twee kleine vingers moeten erg strak zitten.

123
00:12:15,043 --> 00:12:17,623
Ik heb je zelfs de laatste keer gecorrigeerd...

124
00:12:19,766 --> 00:12:22,006
Oké, bravo!

125
00:12:24,018 --> 00:12:27,238
Er zijn vaak mensen met zulke houdingen.

126
00:12:27,424 --> 00:12:28,974
Moeten ze gecorrigeerd worden?

127
00:12:29,047 --> 00:12:30,387
Het moet worden gecorrigeerd.

128
00:12:30,510 --> 00:12:33,615
Bijvoorbeeld, Pierre, neem de houding aan...

129
00:12:33,920 --> 00:12:38,170
Ik heb iets gemerkt, we zullen zien of je...

130
00:12:46,127 --> 00:12:50,227
De hand is een beetje zoals dit...

131
00:12:55,366 --> 00:12:57,176
Maak je wat losser.

132
00:12:59,735 --> 00:13:04,130
Het moet echt gelaagd zijn.

133
00:13:13,740 --> 00:13:15,870
Soms kun je een fout maken.

134
00:13:15,935 --> 00:13:18,195
Als we het repareren...

135
00:13:18,317 --> 00:13:20,887
Knijp een beetje in je kleine vingers...

136
00:13:23,093 --> 00:13:27,443
Sensei toonde altijd energie in beide vingers.

137
00:13:27,821 --> 00:13:30,621
We doen het op deze manier, dan op deze manier.

138
00:13:32,250 --> 00:13:35,010
En dan deze die we er bovenop zetten.

139
00:13:36,000 --> 00:13:38,040
Mooi en loodrecht.

140
00:13:38,310 --> 00:13:42,130
Als je je handen opendoet, staan ze parallel aan de grond.

141
00:13:44,370 --> 00:13:46,580
Je wilt niet zo eindigen...

142
00:13:47,610 --> 00:13:48,830
Dat is goed. Dat is goed.

143
00:13:48,990 --> 00:13:50,410
Het is perfect.
