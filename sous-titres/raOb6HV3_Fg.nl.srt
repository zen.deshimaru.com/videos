1
00:00:00,000 --> 00:00:05,742
Mijn meester leerde me vroeger, toen ik een beginner was (ik ben nog steeds…).

2
00:00:05,842 --> 00:00:07,832
"Zazen is satori. »

3
00:00:07,932 --> 00:00:18,497
In die tijd, in de jaren ’70, was het woord "satori" erg interessant, we wilden weten wat satori was, een soort van verlichting.

4
00:00:18,597 --> 00:00:22,731
Mensen wilden zazen doen om satori te krijgen.

5
00:00:22,831 --> 00:00:31,329
En Sensei leerde dat het de houding van Boeddha zelf was, dat er geen satori buiten zazen is.

6
00:00:31,429 --> 00:00:35,793
Dat is Meester Deshimaru’s leer. Hij zei altijd:

7
00:00:35,893 --> 00:00:39,950
"Satori is de terugkeer naar normale omstandigheden. »

8
00:00:40,050 --> 00:00:41,529
Wij jongeren waren teleurgesteld.

9
00:00:41,629 --> 00:00:46,520
Normale omstandigheden zijn saai.

10
00:00:46,620 --> 00:00:49,388
We rookten graag wiet, namen LSD…

11
00:00:49,490 --> 00:00:53,471
We waren niet geïnteresseerd in normale omstandigheden.

12
00:00:53,571 --> 00:00:58,853
Nu, in 2018, zijn de normale omstandigheden zeer gewild.

13
00:00:58,960 --> 00:01:02,400
Normale seizoenen, normaal klimaat, normale luchtkwaliteit.

14
00:01:02,400 --> 00:01:04,400
Een normale oceaan, normale fauna en flora.

15
00:01:08,380 --> 00:01:10,980
Normaliteit wordt kostbaar.

16
00:01:11,088 --> 00:01:13,155
Vergeet deze woorden nooit:

17
00:01:13,255 --> 00:01:15,705
"Zazen zelf is satori. »

18
00:01:15,805 --> 00:01:19,809
Satori gaat over het op het juiste moment op de juiste plaats zijn.

19
00:01:19,909 --> 00:01:21,516
Het is erg moeilijk.

20
00:01:21,616 --> 00:01:27,840
Als je de Boeddha-houding aanneemt, ben je op het juiste moment op de juiste plaats.

21
00:01:27,940 --> 00:01:32,939
Je bent normaal. Je bent aan het synchroniseren met de kosmische orde.

22
00:01:33,039 --> 00:01:34,264
Het is mechanisch.

23
00:01:34,364 --> 00:01:38,498
Met zijn skelet van een abnormaal mens, een vervuiler…

24
00:01:38,598 --> 00:01:42,831
Als je de Boeddha’s houding aanneemt, is het mechanisch.

25
00:01:42,931 --> 00:01:47,854
We gaan terug naar het universele kosmische ritme. Dat is satori.

26
00:01:47,954 --> 00:01:50,503
Tegelijkertijd is het heel eenvoudig.

27
00:01:50,603 --> 00:01:59,507
Maar als je zo ver weg bent van de normale omstandigheden zoals we nu zijn, heeft het een enorme waarde.

28
00:01:59,607 --> 00:02:01,521
En wat is Zen?

29
00:02:01,621 --> 00:02:04,017
Het neemt de exacte houding aan.

30
00:02:04,117 --> 00:02:07,945
Het is als een code om een geheime deur te openen.

31
00:02:08,045 --> 00:02:10,035
Je ziet het in de films.

32
00:02:10,135 --> 00:02:16,359
Je moet hier op drukken, je moet de lichtstraal op de juiste plaats krijgen.

33
00:02:16,459 --> 00:02:17,990
En de deur gaat open.

34
00:02:18,090 --> 00:02:21,229
Het is hetzelfde als je je positie uitlijnt.

35
00:02:25,280 --> 00:02:29,320
Je bent mooi op de pinnen. Op het kussen, in lotus.

36
00:02:29,720 --> 00:02:32,400
De positie van de Boeddha is in lotus.

37
00:02:32,860 --> 00:02:37,700
Als je de lotus niet kunt doen, doe dan een halve lotus.

38
00:02:37,980 --> 00:02:40,580
Maar de lotus heeft er niets mee te maken.

39
00:02:41,060 --> 00:02:44,120
Het keert de polariteiten om.

40
00:02:44,760 --> 00:02:52,720
Zodra je de lotus neemt, in plaats van te forceren, in plaats van te kwetsen, laat je los.

41
00:02:53,480 --> 00:02:56,380
De houding van de Boeddha is precies.

42
00:02:56,940 --> 00:03:03,020
Als je precies de juiste kleren aantrekt.

43
00:03:03,140 --> 00:03:08,240
Adem goed, genees je lichaam en geest.

44
00:03:08,240 --> 00:03:14,360
Het is prachtig en het is heel eenvoudig.

45
00:03:14,860 --> 00:03:17,840
Het is geen speciale voorwaarde.

46
00:03:18,520 --> 00:03:23,020
Je hoeft niet te denken: "Ik heb meer satori dan de anderen".

47
00:03:23,400 --> 00:03:26,080
Het is gewoon normaal.

48
00:03:26,600 --> 00:03:32,940
Als er iets abnormaals is in het zonnestelsel, laat alles scheten.

49
00:03:33,280 --> 00:03:38,720
Dus alles is precies op de juiste plaats op het juiste moment, en in één beweging.

50
00:03:38,920 --> 00:03:42,660
Het geheim is duidelijk: we kennen de methode.

51
00:03:42,940 --> 00:03:45,700
Je moet de moeite nemen.

52
00:03:46,100 --> 00:03:48,460
Het vergt oefening.

53
00:03:48,740 --> 00:03:52,100
Het is buitengewoon om de kosmische orde te kunnen falen.

54
00:03:52,300 --> 00:03:56,000
Sensei bleef herhalen: "Volg de kosmische orde".

55
00:03:56,160 --> 00:03:58,080
Hij schreeuwde tegen me:

56
00:03:58,320 --> 00:04:01,500
"STIEFAAN, WAAROM VOLG JE DE KOSMISCHE ORDE NIET? JE MOET DE KOSMISCHE ORDE VOLGEN! »

