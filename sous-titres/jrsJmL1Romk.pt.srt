1
00:00:08,039 --> 00:00:12,389
Onde quer que você esteja.

2
00:00:21,107 --> 00:00:23,587
Sente-se em silêncio.

3
00:00:23,995 --> 00:00:26,755
Esteja totalmente presente.

4
00:00:27,698 --> 00:00:32,578
Quem quer que você seja.

5
00:00:39,450 --> 00:00:41,700
Saia de sua rotina diária.

6
00:00:41,790 --> 00:00:43,254
Pratique zazen.

7
00:00:43,401 --> 00:00:45,881
com a Kosen Sangha.

8
00:00:51,238 --> 00:00:57,194
Eventos relacionados a esta pandemia

9
00:00:58,324 --> 00:01:02,861
certificam o Zen da Dogen

10
00:01:03,081 --> 00:01:05,538
e o Zen de Deshimaru.

11
00:01:05,862 --> 00:01:09,802
Impermanência, nós a vivemos.

12
00:01:11,770 --> 00:01:16,620
A interdependência é algo que nós também experimentamos.

13
00:01:21,799 --> 00:01:24,759
O único momento que importa,

14
00:01:26,209 --> 00:01:29,069
é agora mesmo.

15
00:01:29,259 --> 00:01:30,919
Aqui e agora.

16
00:01:31,008 --> 00:01:33,698
Não importa o que você faça.

17
00:01:36,710 --> 00:01:40,610
Volte sua atenção para dentro.

18
00:01:40,908 --> 00:01:43,698
Basta sentar-se.

19
00:01:44,461 --> 00:01:47,161
Reto e silencioso.

20
00:01:48,301 --> 00:01:49,821
O Buda,

21
00:01:50,388 --> 00:01:53,538
nosso mestre interior.

22
00:01:53,942 --> 00:01:56,332
Dharma, o caminho, a prática.

23
00:01:56,671 --> 00:01:59,091
La sangha, a sangha virtual.

24
00:02:03,066 --> 00:02:07,486
Quando você pode.

25
00:02:12,081 --> 00:02:16,081
Junte-se a nós aqui e agora.

26
00:02:16,630 --> 00:02:20,630
Sem ego.

27
00:02:20,769 --> 00:02:24,269
Sem objetivo.

28
00:02:52,434 --> 00:02:54,384
Bom dia a todos!

29
00:02:54,904 --> 00:02:57,614
E obrigado por fazerem zazen juntos!
