1
00:00:38,480 --> 00:00:40,340
La séance va commencer,

2
00:00:42,060 --> 00:00:45,680
avant de vous asseoir, orientez bien votre caméra.

3
00:00:48,700 --> 00:00:54,020
Tu dois pas parler c’est pas le moment, on fait zazen en silence.

4
00:01:00,860 --> 00:01:05,320
Installez vous.

5
00:01:05,320 --> 00:01:10,240
Poussez la terre avec les racines

6
00:01:10,240 --> 00:01:18,780
et en même temps la colonne vertébrale est tendue vers le ciel.

7
00:01:21,600 --> 00:01:24,320
La respiration est nasale,

8
00:01:24,320 --> 00:01:27,560
vous inspirez par les narines

9
00:01:27,560 --> 00:01:29,540
pour ceux qui en ont

10
00:01:29,540 --> 00:01:34,820
et vous expirez profondément

11
00:01:34,820 --> 00:01:38,460
et c’est là ou vous lâchez les tensions

12
00:01:45,340 --> 00:01:50,960
Zana, tu respires trop fort, ton corps y bouge trop.

13
00:01:50,960 --> 00:01:56,810
Je pensais qu’il fallait monter et descendre.

14
00:01:56,810 --> 00:02:00,240
Il faut surtout se taire Zana. Silence!

15
00:02:04,360 --> 00:02:06,840
Vous avez des bonnes postures.

16
00:02:11,880 --> 00:02:19,940
Même si c’est un dojo virtuel, il faut se coiffer un minimum avant de venir,

17
00:02:19,940 --> 00:02:23,940
sans nommer personne..  El Broco, par exemple.

18
00:02:24,740 --> 00:02:29,460
Vous vous êtes coiffé avec un pétard ce matin?

19
00:02:30,720 --> 00:02:33,220
Il faut aussi se coiffer les racines..

20
00:02:36,000 --> 00:02:39,080
Il y a de très belles postures.

21
00:02:42,740 --> 00:02:44,380
La non peur,

22
00:02:45,100 --> 00:02:51,660
en ces temps difficiles, ne vous laissez pas absorber par l’’extérieur.

23
00:02:52,160 --> 00:02:57,940
Recentrez vous, soyez votre propre maître,

24
00:02:59,480 --> 00:03:04,940
et comme disait Ronaldo:

25
00:03:38,340 --> 00:03:41,960
Ne vous laissez pas
abuser par vos émotions,

26
00:03:44,640 --> 00:03:48,620
en voyant ceci, que ressentez vous?

27
00:03:55,480 --> 00:03:57,640
Et en voyant cela?

28
00:04:07,160 --> 00:04:10,300
Ne recherchez pas la joie,

29
00:04:12,040 --> 00:04:14,960
ne fuyez pas la peur

30
00:04:17,460 --> 00:04:20,620
Car comme le disait Maître Deshimaru:

31
00:04:22,380 --> 00:04:26,060
"Même si tu aimes les fleurs elles fanent.

32
00:04:27,640 --> 00:04:32,780
Même si tu n’aimes pas les mauvaises herbes, elles
poussent.

33
00:04:35,840 --> 00:04:37,940
Kaijo!

34
00:04:49,200 --> 00:04:51,380
Véro! Véro!

35
00:04:55,600 --> 00:04:58,700
On a pas encore commencé? On peut bouger?

36
00:04:58,700 --> 00:05:02,640
On va recommencez par qu’il y a Radis et Rose qui est en panne.

37
00:05:03,780 --> 00:05:09,160
Je comprends pas, il faut arrêter la caméra? Qu’est-ce qui faut faire? Les deux à la fois?

38
00:05:10,820 --> 00:05:15,340
Qui a parlé? C’est Muriel, heu non, Rose.

39
00:05:15,840 --> 00:05:24,640
Gabi, attention à ta carotte parce que si
tu bouges trop, ça donne envie de vomir.

