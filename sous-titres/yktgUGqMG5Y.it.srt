1
00:00:23,160 --> 00:00:26,820
Arti, ci sei? 

2
00:00:26,820 --> 00:00:28,560
Sono proprio qui. 

3
00:00:28,560 --> 00:00:32,940
È bello vederti, come stai? 

4
00:00:32,940 --> 00:00:34,240
Sto bene… 

5
00:00:34,240 --> 00:00:38,620
Sai che sei divertente, che ti sta succedendo ultimamente? 

6
00:00:38,740 --> 00:00:41,620
Sono stanco, ho una festa con i vicini. 

7
00:00:41,620 --> 00:00:46,184
Cosa, ma è un blocco, stai scherzando? Non hai il diritto di uscire. 

8
00:00:46,184 --> 00:00:50,520
Non mi è permesso, ma non sono uscito, è sul balcone. 

9
00:00:50,520 --> 00:00:56,280
Mi fai sentire meglio. Attento, proprio dietro, backup. 

10
00:00:56,440 --> 00:00:58,360
Oh, amico, stai soffrendo. 

11
00:00:58,360 --> 00:01:01,980
Ogni sera è un aperitivo. 

12
00:01:01,980 --> 00:01:09,000
E ieri sono caduto nella casa del vicino, mi sono ritrovato nelle mutande, un piano più in basso, nel bidone dei gerani. 

13
00:01:09,000 --> 00:01:12,180
Dovresti comunque stare attento ad Arti. 

14
00:01:12,180 --> 00:01:14,140
Come te la passi? 

15
00:01:14,140 --> 00:01:21,220
Va bene, sai che sono una patata, non è per niente che mi chiamano patata. 

16
00:01:21,400 --> 00:01:24,840
Oh sì, giusto, cosa fai nella tua giornata? 

17
00:01:24,840 --> 00:01:29,957
Inizierò con un po ’di meditazione. 

18
00:01:29,960 --> 00:01:31,340
Dimmi… 

19
00:01:31,340 --> 00:01:40,920
Non c’è niente da dire, mi siedo così, guardo … Mi concentro sulla respirazione. 

20
00:01:40,980 --> 00:01:44,140
Forse dovrei farlo … 

21
00:01:44,140 --> 00:01:49,340
Poi mi sento meglio, con tutti gli altri qui … Siamo pieni. 

22
00:01:49,340 --> 00:01:53,420
Siamo pieni? Ma non ti è permesso, è … blocco! 

23
00:01:53,420 --> 00:01:55,980
No, ma non lo capisci, lo facciamo con lo zoom. 

24
00:01:55,980 --> 00:01:57,460
Con lo zoom? 

25
00:01:57,460 --> 00:02:03,200
Sì, ti connetti e poi sei con molte persone … 

26
00:02:03,200 --> 00:02:05,820
Sono interessato alle tue cose. 

27
00:02:05,820 --> 00:02:10,452
Domani sabato ci eserciteremo con il Maestro Kosen. 

28
00:02:10,460 --> 00:02:13,320
Sono interessato. Chi è lui? 

29
00:02:13,320 --> 00:02:17,340
Beh, guarda, non è male. Domani a mezzogiorno. 

30
00:02:17,500 --> 00:02:20,740
Va bene, ci sarò. Me lo spiegherai? 

31
00:02:20,740 --> 00:02:25,580
Sì, dai, ti spiego ora. 

32
00:02:25,740 --> 00:02:28,040
Bummer! Bummer? 

33
00:02:28,040 --> 00:02:29,760
Che succede Arti? 

34
00:02:29,760 --> 00:02:33,360
Sto diventando un po ’caldo nella padella. 

35
00:02:33,360 --> 00:02:39,260
È buono per quello che hai Arti, uccide il virus, lo garantisco. 

36
00:02:39,260 --> 00:02:43,560
Sì, beh, sto iniziando a fare caldo. 

37
00:02:43,560 --> 00:02:45,280
Non ti preoccupare … 

