1
00:00:08,980 --> 00:00:16,230
Hallo! Ich werde Ihnen erklären, wie man sich hinsetzt, um Zen-Meditation zu praktizieren,
2
00:00:17,200 --> 00:00:19,180
genannt Zazen.

3
00:00:19,180 --> 00:00:22,829
Das benötigte Material ist sehr einfach, ein Mensch...

4
00:00:23,320 --> 00:00:28,349
und ein Meditationskissen. Wenn Sie ein Kissen haben

5
00:00:29,230 --> 00:00:33,689
hübsch gepolstert, das ist in Ordnung, und wenn nicht, macht man eins mit einer Decke...

6
00:00:35,320 --> 00:00:39,060
oder einen Schlafsack zum Beispiel, muss er fest genug sein.

7
00:00:57,490 --> 00:01:04,120
Ich erkläre die Haltung der Zen-Meditation.

8
00:01:05,560 --> 00:01:08,940
Zuerst einmal zu wissen, dass es eine Haltung ist...

9
00:01:09,440 --> 00:01:10,880
Basis

10
00:01:11,240 --> 00:01:13,440
reglos und still.

11
00:01:14,120 --> 00:01:16,660
Die Besonderheit dieser Haltung

12
00:01:17,300 --> 00:01:19,300
ist, dass wir es praktizieren

13
00:01:19,400 --> 00:01:21,400
mit dem Gesicht zur Wand.

14
00:01:21,800 --> 00:01:27,460
Hier stehe ich natürlich vor der Kamera, um Sie zu unterrichten.

15
00:01:27,960 --> 00:01:34,640
Was Sie tun müssen, ist, sich auf ein Kissen zu setzen, wie ich erklärt habe, das

16
00:01:35,000 --> 00:01:37,000
ausreichend gepolstert, ausreichend hart.

17
00:01:37,300 --> 00:01:44,060
Man sitzt gut unten auf dem Kissen, sitzt nicht auf der Kante, die man verschieben kann,

18
00:01:44,440 --> 00:01:48,360
und Sie müssen Ihre Beine so weit wie möglich übereinander schlagen.

19
00:01:48,820 --> 00:01:56,220
Man kann sie so überqueren, so, so, so, das nennt man den Halblotus,

20
00:01:56,660 --> 00:01:59,410
Wenn Sie Ihr anderes Bein hierhin stellen, nennt man das den Lotus.

21
00:02:02,860 --> 00:02:07,620
Das Wichtigste ist, dass die Knie zu Boden gehen,

22
00:02:08,220 --> 00:02:09,660
zu drücken,

23
00:02:10,789 --> 00:02:13,420
Druck mit dem Knie auf den Boden auszuüben.

24
00:02:13,880 --> 00:02:20,079
Also, wie machen wir das? Tun Sie es ohne jede körperliche Anstrengung,

25
00:02:20,720 --> 00:02:25,240
überhaupt nichts Muskulöses. Zen-Meditationshaltung, Zazen,

26
00:02:25,900 --> 00:02:27,780
Es ist eine Haltung, in der man loslassen muss.

27
00:02:28,300 --> 00:02:33,000
Damit die Knie zu Boden gehen

28
00:02:33,920 --> 00:02:37,360
Sie werden das Becken leicht neigen

29
00:02:38,440 --> 00:02:42,220
nach vorne, die Oberseite des Beckens geht nach vorne,

30
00:02:42,760 --> 00:02:46,300
das Steißbein geht ein wenig zurück.

31
00:02:46,760 --> 00:02:48,760
Ich werde es Ihnen im Profil zeigen

32
00:02:55,700 --> 00:03:03,040
Mein Becken wird nicht so gestellt, als ob ich zum Beispiel Auto fahren würde,

33
00:03:03,260 --> 00:03:08,700
Sie sehen hier, dass meine Knie hinten hochgehen, weil mein Schwerpunkt

34
00:03:08,900 --> 00:03:15,700
ging hinter meinen Körper. Ich möchte, dass mein Schwerpunkt nach vorne geht, und das mache ich mit meinem Becken.

35
00:03:15,940 --> 00:03:24,300
Natürlich geht der Schwerpunkt mit der Schwerkraft nach vorne, meine Knie gehen zu Boden.

36
00:03:24,300 --> 00:03:30,860
Da, das muss man zuerst in der Haltung feststellen,

37
00:03:31,160 --> 00:03:37,040
einen Sitz, in dem die Knie zu Boden gehen.

38
00:03:37,480 --> 00:03:40,200
Als Ihnen klar wurde, dass die Sitzung

39
00:03:40,800 --> 00:03:45,500
aus dem Becken, das leicht nach vorne geneigt ist...

40
00:03:45,900 --> 00:03:49,280
Sie werden die Wirbelsäule so weit wie möglich aufrichten

41
00:03:50,200 --> 00:03:52,860
bis zum oberen Ende des Kopfes.

42
00:03:54,580 --> 00:04:02,780
Wir gehen davon aus, dass die Kolumne bis ganz nach oben reicht. Als hätte ich einen Haken gesetzt

43
00:04:05,060 --> 00:04:10,500
und nach oben gehe ich. Das heißt, ich strecke meinen Hals aus und stecke mein Kinn ein,

44
00:04:11,150 --> 00:04:15,699
Sehen Sie, ich habe keinen Kopf auf meinen Schultern, ich habe keinen Buckel...

45
00:04:16,849 --> 00:04:18,849
Ich bin so ehrlich, wie ich nur sein kann.

46
00:04:19,520 --> 00:04:22,000
Ich zeige Ihnen im Profil

47
00:04:23,300 --> 00:04:28,040
Ich bin nicht so, ich bin nicht so, ich bin nicht so...

48
00:04:28,560 --> 00:04:35,780
oder gebückt oder was auch immer, ich bin so gerade, wie ich nur sein kann.

49
00:04:38,800 --> 00:04:43,820
Wenn man es geschafft hat, sich gerade niederzulassen, bewegt man sich nicht mehr.

50
00:04:45,320 --> 00:04:47,160
Ich mache weiter.

51
00:04:47,380 --> 00:04:49,280
die linke Hand

52
00:04:49,540 --> 00:04:51,540
die Finger sind miteinander verbunden

53
00:04:52,250 --> 00:04:57,220
die Finger der rechten Hand werden miteinander verbunden und ich werde die Finger der linken Hand übereinander legen

54
00:04:58,070 --> 00:05:00,070
an den Fingern der rechten Hand

55
00:05:01,490 --> 00:05:03,680
Meine Daumen treffen sich

56
00:05:05,730 --> 00:05:08,329
Auf die Finger der linken Hand aufgelegt,

57
00:05:09,630 --> 00:05:11,630
Ich bilde eine horizontale Linie.

58
00:05:12,690 --> 00:05:20,059
Die Kante meiner Hände von meinem Handgelenk bis hierher, die Kante meiner Hände

59
00:05:21,030 --> 00:05:23,929
Sie werden es an der Basis des Abdomens anwenden...

60
00:05:25,280 --> 00:05:27,820
um dies zu tun,

61
00:05:29,580 --> 00:05:33,440
Verwenden Sie etwas, das es Ihnen erlaubt, Ihre Hände zusammenzukeilen.

62
00:05:34,160 --> 00:05:36,280
in einer Weise, die es Ihnen ermöglicht

63
00:05:36,700 --> 00:05:38,620
vollständig auslassen

64
00:05:38,880 --> 00:05:43,300
die Spannung in den Schultern die Haltung ist eine Haltung, bei der man loslassen muss

65
00:05:44,280 --> 00:05:50,089
Lassen Sie die Spannung nach, Sie sehen, dass meine Ellbogen frei sind, ich bin schon lange so oder so,

66
00:05:51,380 --> 00:05:53,940
leise. Aber ich gebe mir keine Mühe

67
00:05:54,540 --> 00:06:00,940
meine Hände zu halten, ist die Anstrengung, meine Daumen horizontal zu halten.

68
00:06:01,220 --> 00:06:03,220
sobald mir klar wurde

69
00:06:03,750 --> 00:06:05,750
diese Haltungspunkte

70
00:06:06,600 --> 00:06:08,600
Ich werde nicht umziehen,

71
00:06:09,160 --> 00:06:11,160
Das ist die körperliche Haltung.

72
00:06:20,060 --> 00:06:27,420
Nach diesem ersten Teil über die körperliche Haltung, zweiter Teil: die geistige Einstellung.

73
00:06:28,380 --> 00:06:32,340
Sobald ich mich in einer Zen-Meditationshaltung befinde, Zazen

74
00:06:33,620 --> 00:06:37,960
mit dem Gesicht zur Wand, was soll ich tun, was soll ich tun?

75
00:06:39,720 --> 00:06:42,260
In der Stille dieser Haltung,

76
00:06:43,770 --> 00:06:48,380
werden Sie erkennen, dass Sie denken,

77
00:06:49,350 --> 00:06:51,769
oder sogar, dass man nicht aufhören kann zu denken

78
00:06:52,290 --> 00:06:57,469
und es gibt Tausende von Gedanken, die kommen, kommen, kommen.

79
00:06:57,980 --> 00:07:02,420
Sie werden mir gleich sagen, dass es ein Naturphänomen ist...

80
00:07:03,500 --> 00:07:05,500
Sie können es nicht kontrollieren.

81
00:07:05,940 --> 00:07:11,900
Was wir tun werden, während der Zen-Meditation, lassen wir unsere Gedanken passieren.

82
00:07:12,740 --> 00:07:15,580
das heißt, dass in dieser Unbeweglichkeit..,

83
00:07:16,040 --> 00:07:20,440
werden wir in der Lage sein, zu beobachten, was wir denken,

84
00:07:21,040 --> 00:07:25,960
werden Sie Ihre Gedanken und diese Gedanken beobachten, anstatt sie zu unterhalten,

85
00:07:26,360 --> 00:07:32,860
Anstatt sie zu züchten, lassen Sie sie passieren.

86
00:07:33,160 --> 00:07:39,220
Es geht also nicht darum, etwas abzulehnen, es geht wieder darum, loszulassen, loszulassen.

87
00:07:40,080 --> 00:07:42,400
Wie man durchkommt

88
00:07:43,260 --> 00:07:45,260
Aufmerksamkeit, Konzentration,

89
00:07:46,080 --> 00:07:47,860
genau hier, genau jetzt,

90
00:07:48,240 --> 00:07:50,240
auf Ihre Atmung.

91
00:07:55,930 --> 00:07:58,680
Die Atmung im Zen ist die Bauchatmung.

92
00:08:00,460 --> 00:08:08,320
Sie atmen durch, durch Ihr Nasenloch, und Sie atmen sanft ein,

93
00:08:09,640 --> 00:08:13,780
langsame, offensichtlich ruhige, stille Meditation.

94
00:08:15,180 --> 00:08:20,180
Das Merkmal dieser Atmung ist, dass Sie Ihre Aufmerksamkeit

95
00:08:20,540 --> 00:08:24,120
auf der Ausatmungsseite.

96
00:08:24,420 --> 00:08:26,420
Wir inspirieren

97
00:08:26,620 --> 00:08:33,680
durch die Nase, leise, großzügig sein, auffüllen

98
00:08:34,300 --> 00:08:41,880
und das Ausatemgesicht, wenn Sie die ganze Luft ausatmen wollen, tun Sie das langsam,

99
00:08:42,340 --> 00:08:46,940
langsam, als ob sie immer länger und länger werden würde

100
00:08:56,700 --> 00:09:00,140
Wenn Sie am Ende Ihres Ablaufs sind

101
00:09:00,880 --> 00:09:02,380
Sie streben

102
00:09:02,850 --> 00:09:06,340
und wieder erlöschen Sie

103
00:09:07,520 --> 00:09:12,020
und Sie brauchen länger, langsamer, tiefer,

104
00:09:12,200 --> 00:09:15,760
wieder ein tieferes, wellenförmiges Ausatmen.

105
00:09:16,280 --> 00:09:18,280
über den Ozean

106
00:09:18,560 --> 00:09:20,560
an den Strand kommen

107
00:09:22,580 --> 00:09:24,580
All das mit der Nase.

108
00:09:30,060 --> 00:09:33,100
Wenn Sie sich hier und jetzt konzentrieren

109
00:09:33,400 --> 00:09:40,880
auf die Punkte der Körperhaltung, die physischen Punkte also die Aufmerksamkeit, eine Aufmerksamkeit hier und jetzt in Ihrem Körper

110
00:09:41,420 --> 00:09:48,960
wenn Sie sich hier und jetzt gleichzeitig darauf konzentrieren, Ihren Gedanken nicht zu folgen,

111
00:09:49,820 --> 00:09:52,640
zurück zu den Punkten, die sich in dieser Haltung befinden,

112
00:09:52,980 --> 00:09:55,880
wenn Sie sich hier und jetzt konzentrieren

113
00:09:56,460 --> 00:10:02,320
zur Herstellung einer harmonischen Atmung, zum Kommen und Gehen, und nach und nach

114
00:10:02,900 --> 00:10:06,180
als der Zeitpunkt des Ablaufs,

115
00:10:06,800 --> 00:10:10,620
Wenn man hinausgeht, wird die Luft immer länger.

116
00:10:11,600 --> 00:10:16,240
Sie werden keine Zeit haben, darüber nachzudenken... was Sie wollen!

117
00:10:16,800 --> 00:10:22,940
Sie befinden sich hier und jetzt in einer Zazen-Haltung.

118
00:10:23,520 --> 00:10:30,360
In der Haltung, weder der Körper noch die Materie, in der Haltung. Ich erinnere Sie daran, zu drücken,

119
00:10:30,660 --> 00:10:34,420
Sie üben Druck von Ihren Knien auf den Boden aus,

120
00:10:34,680 --> 00:10:38,840
Sie schieben die Erde mit Ihren Knien

121
00:10:38,900 --> 00:10:40,980
und Sie richten sich auf

122
00:10:41,500 --> 00:10:43,500
so viel wie möglich die Wirbelsäule

123
00:10:44,089 --> 00:10:48,999
bis zur Schädeldecke, als ob man mit dem Kopf den Himmel schieben wollte.

124
00:10:49,760 --> 00:10:55,270
Linke Hand in der rechten Hand mit waagerechten Daumen die Handkante

125
00:10:55,780 --> 00:10:58,440
gegen den Unterbauch.

126
00:10:58,840 --> 00:11:00,820
Ich bin reglos.

127
00:11:01,190 --> 00:11:03,160
mit dem Gesicht zur Wand

128
00:11:03,170 --> 00:11:05,170
Mit dem Gesicht zur Wand gibt es nichts Besonderes zu sehen,

129
00:11:05,360 --> 00:11:11,169
Was ich stattdessen tun werde, ist, meinen Blick nach innen zu wenden,

130
00:11:11,959 --> 00:11:13,959
also die Sinne,

131
00:11:14,410 --> 00:11:16,959
Ich werde sie während dieser Meditation nicht mehr verwenden.

132
00:11:17,900 --> 00:11:23,360
Ich stehe mit dem Gesicht zur Wand, es gibt also nichts zu sehen, es ist so, als ob

133
00:11:24,080 --> 00:11:28,640
Ich nehme meine Sicht, meine Vision, ich nehme sie, ich lege sie vor mich hin, es kümmert mich nicht mehr.

134
00:11:29,020 --> 00:11:33,180
Während der Meditation werden Sie mit Sicht meditieren,

135
00:11:34,160 --> 00:11:37,240
45° schräg nach unten

136
00:11:38,300 --> 00:11:46,920
Sie können mit geschlossenen Augen meditieren, aber wenn Sie die Augen halb offen halten, bleiben Sie mit der Realität in Kontakt.

137
00:11:47,340 --> 00:11:49,980
Gehen Sie nicht besonders in Gedanken.

138
00:11:50,320 --> 00:11:54,380
Da wir schweigend sind, nutzen wir auch unser Gehör nicht,

139
00:11:54,600 --> 00:11:58,980
also das Hören, es gibt keine Notwendigkeit dafür, es gibt keine Musik, es gibt nichts.

140
00:11:59,260 --> 00:12:03,740
und wir haben bereits gesehen, dass wir unbeweglich sind.

141
00:12:04,260 --> 00:12:09,960
Nach und nach schneiden wir uns plötzlich von den Forderungen der Außenwelt ab...

142
00:12:10,400 --> 00:12:12,680
werden wir all das los.

143
00:12:13,140 --> 00:12:18,920
Meditation, Sie sitzen körperlich, es ist der Körper, der in Haltung ist,

144
00:12:19,910 --> 00:12:26,350
Man schneidet sich von der Außenwelt ab, bringt alles in den Flugzeugmodus.

145
00:12:26,920 --> 00:12:33,740
Meditieren Sie nicht mit Ihrem Mobiltelefon neben sich.

146
00:13:09,580 --> 00:13:12,000
Zum Abschluss ein Tipp für Anfänger,

147
00:13:12,660 --> 00:13:17,740
Es ist nicht leicht, jetzt so eingesperrt zu sein,

148
00:13:18,140 --> 00:13:21,720
Meditation vor einer Wand,

149
00:13:22,820 --> 00:13:25,940
aber, äh, es ist eine wirklich gute Hilfe.

150
00:13:26,220 --> 00:13:28,640
Ich rate Ihnen wirklich, es zu üben,

151
00:13:28,900 --> 00:13:36,940
um es auszuprobieren, und Sie werden sehen, dass Sie statt Gedanken zu haben, die vielleicht nicht sehr hell sind,

152
00:13:37,260 --> 00:13:43,920
nicht sehr glücklich, anstatt schlecht gelaunt zu sein, anstatt Spannung, Stress zu empfinden,

153
00:13:44,560 --> 00:13:48,569
All dies ist normal, mit dieser Arbeit am Körper.

154
00:13:49,200 --> 00:13:55,500
die Haltung des Geistes, den man loslässt, man distanziert sich von allem, was einem passiert.

155
00:13:55,940 --> 00:13:58,240
Sie werden sehen, dass Sie sich beruhigen werden

156
00:13:58,560 --> 00:13:59,900
physisch

157
00:14:00,100 --> 00:14:01,480
emotional

158
00:14:01,660 --> 00:14:04,820
die Atmung, das alles, es wird dir helfen loszulassen...

159
00:14:05,280 --> 00:14:09,880
es wird Ihnen und Ihrer Familie zutiefst helfen, wenn Sie mit jemandem zusammenleben.

160
00:14:10,340 --> 00:14:14,480
Also zweifeln Sie nicht daran, es zu praktizieren. Danke.
