1
00:00:00,000 --> 00:00:02,200
Veel mensen zijn geïnteresseerd in meditatie…

2
00:00:02,200 --> 00:00:04,220
…en zoek naar informatie op Youtube.

3
00:00:04,420 --> 00:00:07,860
Het probleem is dat we er weinig van zien.
Meester Deshimaru’s Zen…

4
00:00:07,980 --> 00:00:10,900
…noch de Kosen Sangha.

5
00:00:11,100 --> 00:00:13,180
Veel getalenteerde beoefenaars…

6
00:00:13,360 --> 00:00:16,300
…hebben Kosen Sangha-kettingen gemaakt.

7
00:00:16,520 --> 00:00:18,660
Maar het grote publiek vraagt zich af:

8
00:00:18,940 --> 00:00:21,080
Welke is de goede?

9
00:00:21,300 --> 00:00:23,600
Nu, we gaan het allemaal samen in de keten zetten:

10
00:00:23,820 --> 00:00:26,840
bit.ly/gekozen

11
00:00:28,420 --> 00:00:31,160
Dus, als je zazen aan zoveel mogelijk mensen wilt voorstellen:

12
00:00:32,260 --> 00:00:33,980
Abonneer u op het kanaal.

13
00:00:33,980 --> 00:00:35,220
Kijk naar de video’s.

14
00:00:35,220 --> 00:00:36,520
Geef commentaar.

15
00:00:36,700 --> 00:00:38,700
En aarzel niet om ze leuk te vinden!

