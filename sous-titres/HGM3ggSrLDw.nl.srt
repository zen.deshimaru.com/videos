1
00:00:03,300 --> 00:00:07,060
Wat is de tijd?

2
00:00:07,240 --> 00:00:11,780
De tijd bestaat nu pas.

3
00:00:12,080 --> 00:00:15,080
Zen is "hier en nu."

4
00:00:15,340 --> 00:00:16,820
Het is belangrijk.

5
00:00:17,220 --> 00:00:25,160
Maar het begrijpen van het verleden is een buitengewone truc om het heden te begrijpen.

6
00:00:25,580 --> 00:00:31,640
En het begrijpen van de toekomst is ook erg belangrijk.

7
00:00:32,460 --> 00:00:34,780
Hoe ga je de tijd door?

8
00:00:35,360 --> 00:00:38,400
Tijd is een kwestie van frequentie.

9
00:00:38,700 --> 00:00:43,640
Hoe hoger de frequentie, hoe sneller de tijd, dat is alles.

10
00:00:44,100 --> 00:00:49,760
Het bewustzijn evolueert ook volgens de frequenties.

11
00:00:59,700 --> 00:01:03,440
Duur is slechts een materiële indruk.

12
00:01:03,820 --> 00:01:06,000
Het is een streling.

13
00:01:06,400 --> 00:01:09,100
Hoe zit het met zazen?

14
00:01:09,400 --> 00:01:16,700
Voor een zazenliefhebber is zazen tijd zeer kostbaar.

15
00:01:17,120 --> 00:01:22,560
We komen in de dojo, en we hebben maar anderhalf uur, maximaal twee uur.

16
00:01:23,020 --> 00:01:27,720
Om in deze dimensie te kunnen leven.

17
00:01:28,000 --> 00:01:31,520
En zet de dingen op hun plaats.

18
00:01:31,780 --> 00:01:35,440
Je moet je zazen tijd besparen.

19
00:01:35,540 --> 00:01:38,760
Het duurt een tijdje om het tot op de bodem uit te zoeken.

20
00:01:39,000 --> 00:01:40,760
En om in een goede houding te zijn.

21
00:01:41,060 --> 00:01:43,960
Is tijd gerelateerd aan karma?

22
00:01:44,720 --> 00:01:46,920
Het verleden bestaat nu pas.

23
00:01:47,280 --> 00:01:57,240
Elke "nu" komt overeen met een specifieke frequentie verleden tijd met je "nu".

24
00:01:57,620 --> 00:02:00,900
Je verleden verandert voortdurend.

25
00:02:01,000 --> 00:02:10,700
Maar als je naar je verleden gaat, vergeet je je heden.

26
00:02:11,040 --> 00:02:13,560
En je heden wordt het verleden.

27
00:02:13,900 --> 00:02:17,720
En je gaat in iets wat steeds ingewikkelder wordt.

28
00:02:17,940 --> 00:02:19,340
In karma.

29
00:02:19,820 --> 00:02:24,160
"Het is mijn karma’s schuld…"

30
00:02:25,240 --> 00:02:27,960
"Mijn ouders, mijn spullen…"

31
00:02:28,280 --> 00:02:30,440
"Mijn leven is ingewikkeld…"

32
00:02:30,700 --> 00:02:32,380
"Mijn voorouders waren alcoholisten…"

33
00:02:32,660 --> 00:02:35,420
Enz. En je verdwaalt daarbinnen.

34
00:02:35,780 --> 00:02:40,060
Je identificeert je met dit karakter.

35
00:02:40,340 --> 00:02:41,720
Je wordt steeds zwakker.

36
00:02:42,060 --> 00:02:44,140
Je bent geen god meer.

37
00:02:44,420 --> 00:02:45,720
Je bent in de war.

38
00:02:46,080 --> 00:02:47,760
Je bent verdwaald in de materialisatie.

39
00:02:48,060 --> 00:02:53,300
Met onze huidige staat zijn er altijd oorzaken in het verleden.

40
00:02:53,680 --> 00:02:58,540
Als we de huidige toestand veranderen, verandert ook het verleden.

41
00:02:59,160 --> 00:03:01,940
Hoe zit het met de toekomst?

42
00:03:02,340 --> 00:03:05,300
De toekomst is een ander verhaal…

