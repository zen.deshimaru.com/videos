1
00:00:00,080 --> 00:00:00,920
Vraag: 

2
00:00:01,140 --> 00:00:05,380
Ik voel soms in zazen meditatie een heel prettig gevoel. 

3
00:00:05,680 --> 00:00:09,880
Een beetje tintelingen over het hele lichaam. 

4
00:00:10,340 --> 00:00:15,040
Zoals kleine elektrische schokken. 

5
00:00:15,440 --> 00:00:17,880
Reactie van Master Kosen: 

6
00:00:18,400 --> 00:00:22,420
Alle gevoelens kunnen goed of slecht zijn. 

7
00:00:23,080 --> 00:00:28,760
Hetzelfde gevoel lijkt misschien goed of slecht. 

8
00:00:29,220 --> 00:00:33,520
Het belangrijkste is dat het aangenaam is. 

9
00:00:34,180 --> 00:00:37,240
Moge het je ontspannen. 

10
00:00:37,500 --> 00:00:42,000
Het is meestal gerelateerd aan de bloedcirculatie. 

11
00:00:42,340 --> 00:00:50,640
In de lotushouding is er weinig bloedcirculatie in de benen. 

12
00:00:51,020 --> 00:00:55,960
Het circuleert veel meer in het buikgedeelte. 

13
00:00:56,400 --> 00:01:00,840
Het bovenlichaam, inclusief de hersenen, wordt beter geïrrigeerd. 

14
00:01:01,100 --> 00:01:05,240
Het kan een prettig gevoel geven. 

15
00:01:05,500 --> 00:01:09,660
Het is goed, omdat het een positieve gedachte is. 

16
00:01:10,060 --> 00:01:13,580
Als je je goed voelt, is het een positief bewustzijn. 

17
00:01:13,820 --> 00:01:16,240
Het beïnvloedt ook ons ​​leven. 

18
00:01:16,480 --> 00:01:18,160
Meester Deshimaru zei altijd: 

19
00:01:18,380 --> 00:01:20,500
’Zazen is geen versterving. " 

20
00:01:20,840 --> 00:01:26,260
Maar als je naar je lichaam moet kijken, 

21
00:01:26,620 --> 00:01:31,480
soms zijn ze onaangenaam of pijnlijk. 

22
00:01:31,860 --> 00:01:34,940
Als je je goed voelt, is het geweldig! 

