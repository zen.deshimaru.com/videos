1
00:00:07,490 --> 00:00:10,656
Die Leute denken, dass Zazen eine Technik ist … 

2
00:00:10,656 --> 00:00:13,173
wer wird ihnen in ihrem Leben helfen. 

3
00:00:13,173 --> 00:00:16,800
Sie machen es zu einer lächerlich kleinen Sache. 

4
00:00:16,800 --> 00:00:21,820
und mache Zazen in der Hoffnung, dass es ihnen in ihrem Leben helfen wird. 

5
00:00:21,820 --> 00:00:31,280
Das ist wahr, denn in diesem sehr bewussten Moment …, 

6
00:00:31,280 --> 00:00:35,300
das energischste, das gegenwärtigste gibt es …. 

7
00:00:35,300 --> 00:00:39,820
…. wir haben vollen Einfluss auf alle Phänomene in unserem Leben, das ist sicher. 

8
00:00:40,020 --> 00:00:46,240
Aber wir tun dies nicht, um unser Ego zu verbessern … 

9
00:00:46,250 --> 00:00:50,720
Um uns zu beruhigen …. 

10
00:00:50,720 --> 00:00:52,880
Weil es uns gut tun wird …. 

11
00:00:52,880 --> 00:00:58,500
Weil es uns ein paar Stunden Schlaf erspart …. 

12
00:00:58,500 --> 00:01:02,759
Geben Sie uns mehr sexuelle Leistung …. 

13
00:01:02,759 --> 00:01:06,100
Oder helfen Sie uns, mit Stress umzugehen. 

14
00:01:06,100 --> 00:01:11,500
Dies senkt den Zazen …. 

15
00:01:11,500 --> 00:01:16,230
auf der Ebene eines Instruments für unser eigenes illusorisches Leben. 

16
00:01:16,230 --> 00:01:23,760
Aber es ist das Gegenteil. 

17
00:01:23,920 --> 00:01:28,980
Wir werden unser Leben darauf konzentrieren, das effektivste Zazen zu erzeugen. 

18
00:01:30,890 --> 00:01:33,880
Setz dich in die Position des Buddha, 

19
00:01:33,880 --> 00:01:39,380
Es ist der intensivste und bewussteste Moment. 

20
00:01:39,380 --> 00:01:43,020
Darum geht es. 

21
00:01:43,020 --> 00:01:46,340
Alles kommt zusammen: die Vergangenheit, die Gegenwart, die Zukunft. 

22
00:01:46,340 --> 00:01:52,060
Alle Phänomene unseres Lebens sind in Zazen. 

23
00:01:52,060 --> 00:02:00,040
In Zazen haben wir Kraft, Bewusstsein, 

24
00:02:06,620 --> 00:02:12,020
die Vor- und Nachteile unseres Lebens grundlegend zu verändern. 

25
00:02:16,320 --> 00:02:21,240
In Zazen sind wir in einer würdigen Haltung. 

26
00:02:21,240 --> 00:02:25,380
Eine Haltung von Helden, Buddha, Gott. 

27
00:02:25,500 --> 00:02:31,320
Wir sind gerade, stark, gutaussehend, ruhig, ruhig, ruhig, unbeweglich. 

28
00:02:31,320 --> 00:02:36,600
Sie sollten nicht einschlafen, Sie sollten die Wirbelsäule strecken. 

29
00:02:36,600 --> 00:02:41,980
Du musst atmen, dich entspannen. 

30
00:02:46,640 --> 00:02:50,740
In Zazen gibt es alles, alles was wir sind. 

31
00:02:50,740 --> 00:02:55,800
All unsere Vergangenheit, unsere Gegenwart und indirekt unsere Zukunft. 

32
00:02:55,980 --> 00:03:05,460
Da ist unser Körper mit seinen Schwächen, seiner Müdigkeit, 

33
00:03:05,600 --> 00:03:11,200
seine Verunreinigungen, seine Toxine, 

34
00:03:11,209 --> 00:03:14,900
seine Krankheiten, sein Karma usw. 

35
00:03:14,900 --> 00:03:21,380
Da ist unser Geist, unser Gewissen. 

36
00:03:21,380 --> 00:03:25,055
Alles ist in Zazen vereint. 

37
00:03:25,055 --> 00:03:29,320
Die Zazen Haltung, 

38
00:03:29,320 --> 00:03:33,880
Es ist das schönste Selbstbild, das Sie zeigen können. 

39
00:03:40,620 --> 00:03:46,090
Ich traf Meister Deshimaru, er brachte mir Zazen bei, ich saß in Zazen. 

40
00:03:46,090 --> 00:03:48,620
Und ich sagte sofort: "Das ist das Beste!" 

41
00:04:01,360 --> 00:04:05,680
Um zu wissen, was Sie erschaffen, was ist die Realität, die Sie erschaffen … 

42
00:04:05,680 --> 00:04:10,680
Du bist derjenige, der die Realität schafft. Du lebst deinen Traum. 

43
00:04:10,680 --> 00:04:15,920
Jeder lebt seinen Traum, und Sie müssen Ihren Traum nehmen, das ist die Sache. 

44
00:04:15,920 --> 00:04:19,480
Für mich war Zazen schon immer so. 

