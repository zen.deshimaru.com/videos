1
00:00:10,260 --> 00:00:23,160
Jeden Tag auf der ganzen Welt üben die Mitglieder der Kosen Sangha gemeinsam Zazen. 

2
00:00:24,360 --> 00:00:28,520
Ich habe dir von Meister Deshimarus Kindheit erzählt. 

3
00:00:28,820 --> 00:00:30,820
Aus seinen Zweifeln, seiner Forschung … 

4
00:00:30,820 --> 00:00:35,760
Bis ich Kodo Sawaki getroffen habe und seine Zazen-Pose gefunden habe. 

5
00:00:36,040 --> 00:00:39,100
Finden Sie Ihre eigene Zazen-Pose: 

6
00:00:40,240 --> 00:00:41,860
"Was für ein Vergnügen!" 

7
00:00:42,240 --> 00:00:46,100
Ich glaube denen, die ihre Zazen-Einstellung kennengelernt haben … 

8
00:00:46,100 --> 00:00:49,120
Habe ihre Zeit nicht auf diesem Planeten verschwendet. 

9
00:00:49,500 --> 00:00:52,540
Es ist ein unermessliches Geschenk. 

10
00:00:52,540 --> 00:00:54,239
Auch wenn in dieser Haltung … 

11
00:00:54,540 --> 00:00:59,920
Es gibt auch Leiden und Angst. 

12
00:01:00,320 --> 00:01:01,960
In dieser Haltung ist alles vorhanden. 

13
00:01:01,960 --> 00:01:06,160
Aber du siehst es von oben an. 

14
00:01:06,500 --> 00:01:10,740
Bevor er die Lektionen von Kodo Sawaki lernte .. 

15
00:01:12,040 --> 00:01:15,980
Meister Deshimaru hatte die Bibel studiert. 

16
00:01:17,360 --> 00:01:21,020
Er war ein bisschen verliebt in ein niederländisches Mädchen. 

17
00:01:23,020 --> 00:01:24,820
Die Tochter eines Ministers. 

18
00:01:24,960 --> 00:01:28,380
Er nutzte die Gelegenheit, um die Bibel und Englisch zu studieren. 

19
00:01:28,720 --> 00:01:32,140
Eigentlich lernte er Englisch aus dem Studium der Bibel. 

20
00:01:32,140 --> 00:01:37,060
Er besuchte christliche Kirchen und sang Hymnen. 

21
00:01:37,220 --> 00:01:41,360
Ich sage dir das, weil es Ostern ist. 

22
00:01:41,660 --> 00:01:51,880
Und jetzt ist Jesus zwischen seinem Grab und der vierten oder fünften Dimension. 

23
00:01:52,340 --> 00:01:56,580
Sensei erzählte uns das, als er nach Frankreich kam .. 

24
00:01:57,000 --> 00:02:02,360
Er hatte seine Bibel und die Hymnen seiner Jugend mitgebracht. 

25
00:02:02,680 --> 00:02:07,940
Während eines Ostersesshin gab er uns einen Kuss über Christus. 

26
00:02:08,280 --> 00:02:11,720
Nach dem Lukasevangelium .. 

27
00:02:11,720 --> 00:02:15,620
Damit dieser Christus … 

28
00:02:16,140 --> 00:02:20,240
Könnte dafür sprechen, dass Pontius Pilatus beurteilt wird. 

29
00:02:20,600 --> 00:02:26,240
Pontius Pilatus rief die Hohenpriester. 

30
00:02:26,240 --> 00:02:28,820
Und fragte Jesus in ihrer Gegenwart. 

31
00:02:29,180 --> 00:02:30,340
Er sagt zu ihnen: 

32
00:02:30,600 --> 00:02:33,020
"Du hast mir diesen Mann vorgestellt als … 

33
00:02:33,020 --> 00:02:39,820
Jemand, der die Leute in die falsche Richtung führt. " 

34
00:02:40,280 --> 00:02:47,640
"Ich habe ihn befragt und nichts Schlimmes an diesem Mann gefunden." 

35
00:02:48,000 --> 00:02:50,260
"Ich habe keines der Motive gefunden, die Sie ihm vorwerfen." 

36
00:02:50,580 --> 00:02:56,940
"Herodes hat auch keinen Grund gefunden, Anklage zu erheben." 

37
00:02:57,260 --> 00:03:02,620
"Ich komme zu dem Schluss, dass dieser Mann nichts getan hat, um die Todesstrafe zu verdienen." 

38
00:03:03,240 --> 00:03:06,480
Zu dieser Zeit riefen die Leute mit einer Stimme aus: 

39
00:03:06,480 --> 00:03:15,100
"Tot für Jesus, lass Barabbas frei!" 

40
00:03:16,240 --> 00:03:24,280
"Barabbas wurde wegen ungeordneten Verhaltens und Mordes inhaftiert." 

41
00:03:24,620 --> 00:03:29,100
Pontius Pilatus angeboten .. 

42
00:03:29,100 --> 00:03:32,200
Um einen der Gefangenen freizulassen. 

43
00:03:32,800 --> 00:03:38,480
Aber die Leute sagten: "Freier Barabbas!" 

44
00:03:38,940 --> 00:03:44,260
Pontius Pilatus spricht wieder Menschen an: 

45
00:03:44,260 --> 00:03:49,640
Und er sagt sie noch einmal und spricht zu den Menschen: 

46
00:03:49,960 --> 00:03:53,820
"Sei vernünftig, Jesus hat nichts falsch gemacht. 

47
00:03:53,820 --> 00:03:55,800
Er hat nur geredet, das ist alles! " 

48
00:03:55,800 --> 00:03:59,620
Alle schrien: "Kreuzige ihn!" 

49
00:03:59,860 --> 00:04:02,320
Pontius Pilatus sagte schließlich: 

50
00:04:02,620 --> 00:04:04,340
"Wie du willst .. 

51
00:04:04,340 --> 00:04:06,300
Es ist nicht mein Problem .. " 

52
00:04:07,200 --> 00:04:11,640
Und als Antwort sprach er den berühmten Satz: 

53
00:04:11,640 --> 00:04:13,500
"Ich war meine Hände in Unschuld!" 

54
00:04:13,560 --> 00:04:17,540
Dieser Ausdruck bedeutet, dass .. 

55
00:04:17,540 --> 00:04:19,960
Sie sollten Ihre Hände so oft wie möglich waschen. 

56
00:04:20,140 --> 00:04:22,320
Um das Coronavirus zu vermeiden. 

57
00:04:22,400 --> 00:04:24,920
Sie müssen jedes Mal sagen: "Ich wasche meine Hände in Unschuld!" 

58
00:04:25,240 --> 00:04:29,880
Dies ist eine Lehre von Deshimaru, er sagt dies. 

59
00:04:30,360 --> 00:04:35,760
"So wurde Jesus von der öffentlichen Meinung verurteilt." 

60
00:04:36,020 --> 00:04:39,140
"Massenpsychologie hat Christus getötet." 

61
00:04:39,380 --> 00:04:44,200
"Mimikry, Weigerung, sich selbst in Frage zu stellen … 

62
00:04:44,200 --> 00:04:46,160
Das hat Christus getötet. " 

63
00:04:46,300 --> 00:04:48,940
Die Menge, die Menge .. 

64
00:04:48,940 --> 00:04:54,800
Enthält an sich eine kraftvolle Energie. 

65
00:04:54,800 --> 00:04:57,720
Das kann sowohl negativ als auch gefährlich sein. 

66
00:04:57,800 --> 00:05:02,900
Es ist anders, wenn wir zusammen kommen, um Zazen zu machen. 

67
00:05:03,180 --> 00:05:05,640
Jeder von uns schaut sich an .. 

68
00:05:05,640 --> 00:05:11,820
In Harmonie mit der Gruppe. 

69
00:05:12,160 --> 00:05:16,260
Dies ist vor allem bei Zazoom besonders wichtig! 

70
00:05:16,680 --> 00:05:20,520
Zum Beispiel habe ich in meinem Zimmer Weihrauch verbrannt. 

71
00:05:20,920 --> 00:05:25,260
Sensei sagte oft: "In Zazen bist du allein." 

72
00:05:25,640 --> 00:05:28,220
Aber wenn Sie Zazen in einem Dojo üben .. 

73
00:05:28,220 --> 00:05:31,460
Wir sind in einer Gruppe, auch wenn wir alleine sind. 

74
00:05:31,820 --> 00:05:35,260
Aber dann gibt es keine emotionale Verwirrung. 

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki pflegte dazu zu sagen: 

76
00:05:38,320 --> 00:05:41,640
"Wir sollten nicht in Gruppendummheit leben." 

77
00:05:42,020 --> 00:05:45,680
"Und behalte diese Aberration nicht für die wirkliche Erfahrung." 

78
00:05:45,940 --> 00:05:48,480
"Dies ist das Schicksal der einfachen Leute .. 

79
00:05:48,480 --> 00:05:50,540
Wer hat keine andere Sichtweise? 

80
00:05:50,860 --> 00:05:54,180
Dann mit den Augen der kollektiven Dummheit. " 

81
00:05:54,700 --> 00:06:02,080
Der Buddha gab einmal pro Woche eine öffentliche Lektion. 

82
00:06:02,600 --> 00:06:05,620
Alle Jünger, Mönche und Laien. 

83
00:06:05,620 --> 00:06:08,360
Draußen versammelt .. 

84
00:06:08,360 --> 00:06:11,000
Sicher an privilegierten Orten. 

85
00:06:11,880 --> 00:06:14,960
Der Buddha gab einen Kuss. 

86
00:06:15,320 --> 00:06:16,980
Und jeder war in Zazen .. 

87
00:06:17,960 --> 00:06:19,880
Und hörte ihm zu .. 

88
00:06:19,980 --> 00:06:23,260
Und dann gingen alle wieder ihren Weg. 

89
00:06:23,760 --> 00:06:25,560
Jeder übte alleine .. 

90
00:06:25,560 --> 00:06:27,960
Eine Woche, ein Monat .. 

91
00:06:28,240 --> 00:06:30,100
Und dann regelmäßig zurückgekehrt .. 

92
00:06:30,280 --> 00:06:32,680
auf die Worte des Buddha hören. 

93
00:06:32,680 --> 00:06:37,840
Dojos erschienen nur in China. 

94
00:06:38,180 --> 00:06:40,900
Und wir haben als Gruppe angefangen zu üben. 

95
00:06:40,900 --> 00:06:42,560
Mit einer Bestellung und Regeln. 

96
00:06:44,580 --> 00:06:48,280
Und so ist die Gruppe sehr mächtig. 

97
00:06:48,800 --> 00:06:50,960
Sowohl gut als auch schlecht. 

98
00:06:51,200 --> 00:06:53,020
Sensei sagte tatsächlich: 

99
00:06:53,020 --> 00:06:57,240
"Jesus Christus gab seinem Körper den Geschmack des Todes." 

100
00:06:57,620 --> 00:07:01,700
"Am tiefen See des Nichts und der Verzweiflung." 

101
00:07:02,180 --> 00:07:06,420
"Er ist ein Symbol des ewigen Lebens geworden." 

102
00:07:06,740 --> 00:07:09,180
"Sein Leben ist identisch mit dem eines Zen-Koans." 

103
00:07:09,380 --> 00:07:11,380
Das hat Sensei gesagt. 
