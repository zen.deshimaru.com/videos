1
00:00:00,000 --> 00:00:03,380
Es inútil ir a Japón a practicar Zen. 

2
00:00:03,640 --> 00:00:07,360
Cuando la gente me pide información en Dijon, les digo que: 

3
00:00:07,620 --> 00:00:09,240
"No vamos a hablar sobre el zen". 

4
00:00:09,500 --> 00:00:10,180
"Ven al dojo". 

5
00:00:10,440 --> 00:00:14,040
"Solo entonces sentirás si te conviene o no". 

6
00:00:14,360 --> 00:00:18,100
Desde la primera vez que haces ejercicio, puedes sentir lo que trae. 

7
00:00:18,680 --> 00:00:21,800
Si te apetece, es una señal de que estás de acuerdo. 

8
00:00:22,000 --> 00:00:26,480
No hay necesidad de explicaciones teóricas para saber por qué practicamos. 

9
00:00:26,780 --> 00:00:35,280
Cuando nos instalamos en Zazen, inmediatamente … sientes que la postura tiene algo que da descanso, que se calma. 

10
00:00:35,580 --> 00:00:40,660
La actitud física es realmente importante para sentirla circulando. 

11
00:00:40,940 --> 00:00:46,960
Creo que está relacionado principalmente con este equilibrio, que nos permite rendirnos. 

12
00:00:47,240 --> 00:00:49,280
Zazen es también una experiencia de silencio. 

13
00:00:49,620 --> 00:00:59,800
Cuando estamos en silencio juntos, Cuando escuchas ese silencio, se transmite algo profundo. 

14
00:01:00,140 --> 00:01:03,140
Estoy pensando en una enfermera que viene completamente estresada: 

15
00:01:03,340 --> 00:01:05,840
"¡No puedo calmar la mente!" 

16
00:01:06,120 --> 00:01:08,840
Ella salió completamente tranquila. 

17
00:01:09,120 --> 00:01:14,620
Si podemos detectar gradualmente la respiración profunda, 

18
00:01:15,140 --> 00:01:22,300
borra todos esos pensamientos molestos, todas las complicaciones. 

19
00:01:22,720 --> 00:01:29,720
Incluso es difícil decir que soy budista porque suena como una iglesia. 

20
00:01:30,280 --> 00:01:33,720
Si no necesitas esto para practicar Zen … 

21
00:01:34,020 --> 00:01:38,800
Es una pena en estos días, la gente quiere meditación secular. 

22
00:01:39,220 --> 00:01:42,800
En Dijon hay muchos tipos de meditaciones. 

23
00:01:43,140 --> 00:01:47,360
No los conozco, no puedo hablar de ellos. 

24
00:01:47,560 --> 00:01:52,940
Pero personalmente me gusta practicar como se hace. 

25
00:01:53,180 --> 00:01:54,340
Con algunas reglas simples: 

26
00:01:54,700 --> 00:01:56,780
Saluda cuando lleguemos a casa. 

27
00:01:57,080 --> 00:01:58,880
Saluda a otras personas con las que practiques. 

28
00:01:59,120 --> 00:02:00,620
Hacemos nuestra meditación. 

29
00:02:00,880 --> 00:02:06,040
Luego recitamos un sutra prácticamente para todo el universo. 

30
00:02:06,300 --> 00:02:08,200
Es un asunto completo. 

31
00:02:08,520 --> 00:02:12,480
Es una forma de hacer ejercicio no solo para usted. 

32
00:02:13,500 --> 00:02:17,180
No siempre escuchamos nuestra comodidad. 

33
00:02:17,460 --> 00:02:24,300
Mientras que las personas que desean alguna forma de meditación, ya están haciendo una elección. 

34
00:02:24,560 --> 00:02:29,720
Me gusta esta noción de "falta de objetivos", en este momento cuando todo tiende a obtener ganancias. 

35
00:02:30,000 --> 00:02:32,180
Estamos en zazen, y eso es todo. 

36
00:02:32,420 --> 00:02:35,660
He recibido el mensaje del Maestro Kosen. 

37
00:02:35,960 --> 00:02:38,180
Me siento honrado e intimidado. 

38
00:02:38,520 --> 00:02:42,800
La gente imagina que ser un maestro debe realizarse plenamente. 

39
00:02:43,080 --> 00:02:45,180
No me siento así 

40
00:02:45,440 --> 00:02:50,720
Experimento esta transferencia como el deseo de continuar transfiriendo la Sangha y ayudarla a crecer. 

41
00:02:51,040 --> 00:02:56,300
Nuestras sanghas son todas las personas que practican con el Maestro Kosen. 

42
00:02:56,980 --> 00:03:04,360
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

