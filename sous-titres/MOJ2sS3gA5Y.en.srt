1
00:00:04,240 --> 00:00:07,040
What is Zen? 

2
00:00:07,320 --> 00:00:10,120
Zen is about understanding yourself. 

3
00:00:10,400 --> 00:00:11,800
Your own mind. 

4
00:00:12,080 --> 00:00:14,140
It is everyone’s business. 

5
00:00:14,420 --> 00:00:16,500
That’s what Deshimaru wanted to broadcast. 

6
00:00:16,800 --> 00:00:19,460
That is what I want to pass on and realize. 

7
00:00:19,720 --> 00:00:22,460
I had seen a disciple of Kodo Sawaki. 

8
00:00:22,840 --> 00:00:24,720
He said to me: 

9
00:00:25,120 --> 00:00:27,620
"You have to look at yourself!" 

10
00:00:27,960 --> 00:00:29,960
He was right! 

11
00:00:30,380 --> 00:00:33,300
What does a spiritual path mean? 

12
00:00:33,640 --> 00:00:41,880
The meaning of a spiritual path is "to come back to the dreamer." 

13
00:00:46,020 --> 00:00:48,260
Dogen says: 

14
00:00:48,520 --> 00:00:51,260
"Studying Buddhism is studying yourself." 

15
00:00:51,540 --> 00:00:53,980
Study the dreamer. 

16
00:00:54,380 --> 00:00:57,300
Why does he dream like this? 

17
00:00:58,260 --> 00:01:00,240
How can he change his dream? 

18
00:01:00,540 --> 00:01:02,020
How can he control his dream? 

19
00:01:02,840 --> 00:01:04,000
When the Buddha says: 

20
00:01:04,440 --> 00:01:06,300
"Everything is a dream." 

21
00:01:06,620 --> 00:01:09,820
That is not to say "Nothing matters". 

22
00:01:10,100 --> 00:01:13,580
It is not the dream that is important, it is the dreamer. 

23
00:01:13,900 --> 00:01:16,540
Stop dreaming, that’s what we call "awakening" … 

24
00:01:17,180 --> 00:01:21,040
Awakening is becoming a dreamer. 

25
00:01:21,340 --> 00:01:22,980
Controlling your dream. 

26
00:01:24,000 --> 00:01:26,120
That is the spiritual path. 

27
00:01:26,420 --> 00:01:27,980
It is very difficult. 

28
00:01:28,560 --> 00:01:31,760
How can I start? 

29
00:01:32,660 --> 00:01:45,080
The primary motivation for practicing a spiritual path is: being able to control your dream. 

30
00:01:46,700 --> 00:01:49,340
What is important in Zen? 

31
00:01:49,920 --> 00:01:55,880
In this way it is your mind that is important. 

32
00:01:56,280 --> 00:02:01,280
Zazen is not an object of worship. 

33
00:02:01,540 --> 00:02:03,980
The second important point of his teaching: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku. 

35
00:02:07,180 --> 00:02:10,780
Which means "non-profit". 

36
00:02:11,340 --> 00:02:23,340
It is the door that leads us from the spirit of God to the material world. 

37
00:02:23,640 --> 00:02:27,000
It is very important. 

38
00:02:27,360 --> 00:02:37,420
Even in the everyday world, even in phenomena, you don’t lose your concentration. 

39
00:02:37,800 --> 00:02:42,920
You do not lose your true nature. 

40
00:02:43,180 --> 00:02:45,100
That’s Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
But what is easy? 

42
00:02:55,780 --> 00:03:00,260
People always take the easy way. 

43
00:03:00,620 --> 00:03:03,780
We fall into the easy stuff. 

44
00:03:10,640 --> 00:03:13,600
Power is easy. 

45
00:03:14,060 --> 00:03:19,220
It’s easy to be with people who always feel the same. 

46
00:03:21,160 --> 00:03:26,400
Making a church out of zen is easy. 

47
00:03:26,800 --> 00:03:32,080
Attaching to a church is easy. 

48
00:03:32,360 --> 00:03:37,300
People go into something safe. 

49
00:03:37,600 --> 00:03:41,060
Without any effort. 

50
00:03:41,420 --> 00:03:44,620
Where they are never really confronted with themselves. 

51
00:03:44,920 --> 00:03:47,080
Where they are never alone. 

52
00:03:47,500 --> 00:03:52,040
How do you view your former fellow followers? 

53
00:03:52,360 --> 00:03:58,720
I admire many of Master Deshimaru’s disciples. 

54
00:03:58,980 --> 00:04:07,980
Who have received his education and continue to practice alone. 

55
00:04:08,240 --> 00:04:14,320
It is better to practice together. 

56
00:04:14,700 --> 00:04:19,140
But at least they do their own things. 

57
00:04:19,660 --> 00:04:23,980
They are alone. 

