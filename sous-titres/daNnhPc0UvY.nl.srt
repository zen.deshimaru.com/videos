1
00:00:05,860 --> 00:00:08,580
Deshimaru, ik weet dat ik gauw dood ga. 

2
00:00:08,820 --> 00:00:17,320
De tijd is gekomen om u tot monnik te wijden om Bodhidarma’s leer in een nieuw land door te geven. 

3
00:00:31,020 --> 00:00:32,820
Ik wil God helpen. 

4
00:00:33,100 --> 00:00:34,860
Ik wil Christus helpen. 

5
00:00:43,260 --> 00:00:48,000
Ik wil de ware god helpen. 

6
00:00:48,240 --> 00:00:52,760
Ik wil terugkomen bij de ware god. 

7
00:00:53,360 --> 00:01:00,360
40 jaar sinds de komst van Master Deshimaru in Europa. 

8
00:01:00,800 --> 00:01:04,000
God is down. 

9
00:01:04,320 --> 00:01:09,040
God is "vermoeid", moe. 

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. Het bodhidarma van de moderne tijd. 

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru arriveerde in de zomer van 1967 in Parijs op het treinstation Gare du Nord. 

12
00:01:31,120 --> 00:01:33,480
Alleen een monnik, zonder geld, die geen Frans spreekt. 

13
00:01:33,820 --> 00:01:36,600
In zijn bagage alleen het verse zaad van Zen. 

14
00:01:45,440 --> 00:01:49,600
De aankomst 

15
00:02:59,140 --> 00:03:03,060
Als we terug kunnen gaan naar de geschiedenis van Zen … 

16
00:03:04,380 --> 00:03:09,520
Het begon allemaal in India en toen: wat is er gebeurd? 

17
00:03:09,720 --> 00:03:13,340
Hoe ging het van India naar Japan? 

18
00:03:13,640 --> 00:03:19,020
India, heel diepe meditatie. 

19
00:03:19,480 --> 00:03:22,340
Maar te traditioneel. 

20
00:03:24,380 --> 00:03:29,980
Yoga is niet af in India. 

21
00:03:30,720 --> 00:03:36,160
Sommige mensen hebben een hoog spiritueel niveau, maar de meeste mensen niet. 

22
00:03:36,760 --> 00:03:43,440
Maar yoga, klaar. zijn we niet klaar? 

23
00:03:43,760 --> 00:03:48,240
Maar het land is te oud. 

24
00:03:48,940 --> 00:03:53,660
Bodhidarma bracht zazen uit India naar China. 

25
00:03:53,900 --> 00:03:58,280
Maar zen is er niet meer in India. 

26
00:03:58,860 --> 00:04:04,880
Vervolgens bracht Dogen zazen van China naar Japan. 

27
00:04:05,160 --> 00:04:11,360
Bedoel je dat de grond te oud is in Azië, Japan, China of India? 

28
00:04:11,740 --> 00:04:14,740
Ja, India is te oud. 

29
00:04:14,960 --> 00:04:17,700
China is ook te oud. 

30
00:04:18,280 --> 00:04:20,280
Zen is klaar in China. 

31
00:04:20,700 --> 00:04:23,940
Japan is te gewend aan zen. 

32
00:04:24,680 --> 00:04:27,240
Ze geven alleen om ceremonies … 

33
00:04:27,680 --> 00:04:30,380
Ze missen de essentie van zen. 

34
00:04:36,160 --> 00:04:48,320
Denk je dat de geest van Zen de neiging heeft af te nemen? 

35
00:04:48,640 --> 00:04:50,620
Ja ja ja! 

36
00:04:51,040 --> 00:04:58,720
Denk je dat de geest van Zen in Frankrijk kan vangen, zoals het zaad van een bloem? 

37
00:04:59,040 --> 00:05:01,120
Ja! 

38
00:05:03,020 --> 00:05:06,980
Het lesgeven begint. 

39
00:06:24,680 --> 00:06:30,080
Waarom heb je ervoor gekozen om je in Frankrijk te vestigen? 

40
00:06:30,400 --> 00:06:37,000
Frankrijk is beter dan een ander land om zen te begrijpen. 

41
00:06:37,520 --> 00:06:44,300
Heel gemakkelijk zazen te begrijpen. 

42
00:06:45,120 --> 00:06:46,700
Of filosofie. 

43
00:06:46,980 --> 00:06:48,980
Voor ideeën. 

44
00:06:49,300 --> 00:06:52,060
Heel diep. 

45
00:06:52,520 --> 00:06:57,980
Montaigne bijvoorbeeld: 

46
00:06:58,420 --> 00:07:03,900
’Het kan me niet zoveel schelen wat ik voor anderen ben, het maakt me uit wat ik voor mezelf ben.’

47
00:07:05,880 --> 00:07:09,640
Descartes, Bergson … 

48
00:07:10,140 --> 00:07:14,080
Geestdragers, filosofen … 

49
00:07:14,800 --> 00:07:16,840
Ze kunnen zen worden. 

50
00:07:18,200 --> 00:07:25,740
Ze kennen zen niet, maar hun denken is hetzelfde … 

51
00:07:26,460 --> 00:07:29,760
Waarom ben je niet naar Amerika gegaan? 

52
00:07:30,280 --> 00:07:33,840
Ik wilde eerst Zen in de Verenigde Staten brengen. 

53
00:07:34,220 --> 00:07:41,180
Maar ze willen zen gebruiken voor zaken, voor industrie, psychologie, psychoanalyse … 

54
00:07:41,540 --> 00:07:44,180
Ze willen het gebruiken. 

55
00:07:44,340 --> 00:07:49,020
Het is niet waar zen. Niet diep. 

56
00:07:52,500 --> 00:07:56,160
Er verschijnt een sangha. 

57
00:09:08,270 --> 00:09:14,100
Vroeger was het onderwijs goed, maar in de moderne tijd slechts gemiddeld. 

58
00:09:14,100 --> 00:09:23,740
De essentie is klaar. 

59
00:09:24,560 --> 00:09:33,700
We zoeken alleen naar het gemiddelde … 

60
00:09:34,380 --> 00:09:41,600
Alleen de essentie is nodig voor een sterke opleiding. 

61
00:09:43,380 --> 00:09:49,960
Altijd rechts of links … 

62
00:09:51,040 --> 00:09:54,260
Maar we moeten tegenstrijdigheden omarmen. 

63
00:09:54,600 --> 00:09:59,720
Alleen door ons ego te verlaten, kunnen we onze ware individualiteit vinden. 

64
00:10:03,920 --> 00:10:13,100
Onze hoogste persoonlijkheid als we de kosmische waarheid volgen, de kosmos. 

65
00:10:13,500 --> 00:10:19,480
We kunnen ons ware, sterke ego vinden. 

66
00:10:20,580 --> 00:10:25,680
Wil je mensen sterk maken? 

67
00:10:26,540 --> 00:10:32,060
Ja, evenwichtig en sterk, beetje bij beetje. 

68
00:10:32,800 --> 00:10:41,600
We moeten de echte soberheid vinden. 

69
00:10:43,360 --> 00:10:47,600
Onderwijs 

70
00:12:46,000 --> 00:12:57,680
De essentie van Zen is "Mushotoku", zonder doel. 

71
00:12:59,200 --> 00:13:05,520
Alle religies hebben een doel. 

72
00:13:06,400 --> 00:13:10,560
Je bidt om rijk of gelukkig te worden. 

73
00:13:11,440 --> 00:13:17,040
Maar als we een doel hebben, is het niet sterk. 

74
00:13:18,400 --> 00:13:30,000
We mogen geen enkel doel hebben. 

75
00:13:31,680 --> 00:13:38,000
Je kunt je niet concentreren. 

76
00:13:39,360 --> 00:13:47,680
Bedoelt u dat de juiste realisatie spontaan moet zijn? 

77
00:13:48,480 --> 00:13:56,080
We moeten terugkeren naar de normale omstandigheden. 

78
00:13:57,040 --> 00:14:01,440
Echte Satori, transcendentaal bewustzijn … 

79
00:14:02,560 --> 00:14:07,520
is om terug te keren naar normale omstandigheden. 

80
00:14:09,440 --> 00:14:18,080
Mensen zijn in de moderne tijd abnormaal. 

81
00:14:19,120 --> 00:14:27,120
Alleen de ware god, bijvoorbeeld Christus of Boeddha, had normale omstandigheden voor mensen. 

82
00:14:27,920 --> 00:14:33,900
Is de eerste noodzakelijke voorwaarde de eigen vastberadenheid? 

83
00:14:34,340 --> 00:14:39,500
En ten tweede om deze vastberadenheid op te geven? 

84
00:14:39,760 --> 00:14:45,940
Ja. Individualiteit is noodzakelijk en we moeten die opgeven. 

85
00:14:46,320 --> 00:14:50,180
We moeten tegenstellingen omarmen. 

86
00:14:50,920 --> 00:14:56,560
Het lijkt erop dat je veel rookt … 

87
00:14:57,540 --> 00:15:03,260
Niet zo veel! 

88
00:15:04,220 --> 00:15:10,620
Heeft zen je sterk genoeg gemaakt om iets te accepteren? 

89
00:15:11,140 --> 00:15:15,360
Alles is mogelijk! 

90
00:15:15,920 --> 00:15:20,240
Het is niet zo belangrijk om om je leven te geven. 

91
00:15:20,540 --> 00:15:25,960
De Europeanen hebben te veel egoïsme. 

92
00:15:26,400 --> 00:15:31,840
Je moet alles accepteren en verder gaan! 

93
00:15:32,180 --> 00:15:36,960
Maar zen is geen ascese of versterving. 

94
00:15:37,920 --> 00:15:41,460
We moeten voor kosmisch leven. 

95
00:15:41,680 --> 00:15:45,900
We mogen geen grenzen stellen. 

96
00:15:46,320 --> 00:15:48,780
Anders wordt het leven smal. 

97
00:15:52,340 --> 00:15:55,880
Contact houden met Japan 

98
00:18:11,940 --> 00:18:15,640
Over de opvoeding van kinderen … 

99
00:18:15,980 --> 00:18:27,960
In moderne tijden richten we ons alleen op de intellectuele kant van het onderwijs. 

100
00:18:28,360 --> 00:18:35,880
We moeten ook de fysieke kant zien, de juiste spierspanning. 

101
00:18:38,600 --> 00:18:45,740
We moeten ons ook concentreren op de alledaagse aspecten van het dagelijks leven. 

102
00:18:46,100 --> 00:18:50,560
Concentratie en fysieke houding zijn erg belangrijk. 

103
00:18:50,960 --> 00:18:59,200
We mogen bijvoorbeeld niet slordig zijn tijdens het eten. 

104
00:18:59,740 --> 00:19:04,200
Concentratie op het dagelijks leven is van cruciaal belang. 

105
00:19:04,620 --> 00:19:10,920
Zelfs onze houding tijdens het slapen is van cruciaal belang! 

106
00:19:11,860 --> 00:19:19,460
Leraren besteden onvoldoende aandacht aan fysieke houding. 

107
00:19:19,800 --> 00:19:23,920
Ze geven alleen om de intellectuele kant. 

108
00:19:24,220 --> 00:19:27,679
Waarom worden kinderen niet goed opgevoed? 

109
00:19:28,260 --> 00:19:30,680
Is onderwijs te gemakkelijk? 

110
00:19:31,200 --> 00:19:36,100
We komen terug naar dieren in de moderne tijd! 

111
00:19:37,160 --> 00:19:46,380
Alles is te gemakkelijk … 

112
00:19:46,660 --> 00:19:50,040
We rijden auto’s en willen niet meer lopen … 

113
00:19:50,420 --> 00:20:00,160
Gezinsonderwijs is ook te gemakkelijk. Het is een vergissing. 

114
00:20:01,360 --> 00:20:09,479
Warm, eten. Te "softy-softy"! 

115
00:20:10,600 --> 00:20:19,320
Ons lichaam wordt zwak. 

116
00:20:21,540 --> 00:20:26,180
Zoeken we naar de vrijheid van dieren? 

117
00:20:26,540 --> 00:20:28,540
Wilde dieren zijn sterk. 

118
00:20:28,940 --> 00:20:40,340
Gevangen, goed gevoede dieren in dierentuinen zijn zwak. 

119
00:20:40,680 --> 00:20:42,680
Ze missen activiteit. 

120
00:20:45,200 --> 00:20:47,700
Wilde dieren zijn sterk! 

121
00:20:56,140 --> 00:20:59,100
Intelligentie is belangrijk. 

122
00:20:59,360 --> 00:21:01,100
Maar dat is niet genoeg. 

123
00:21:01,420 --> 00:21:06,720
We hebben ook een sterk lichaam en hogere wijsheid nodig. 

124
00:21:09,080 --> 00:21:12,800
De Gendronière-tempel creëren 

125
00:23:45,260 --> 00:23:50,540
Je hebt de vrucht van Zen naar Europa gebracht, zodat mensen het kunnen proeven. 

126
00:23:50,820 --> 00:24:05,540
India, China en Japan hebben de ware essentie van spirituele cultuur. 

127
00:24:07,480 --> 00:24:18,000
We moeten nu de Aziatische en Europese beschavingen samenvoegen. 

128
00:24:23,660 --> 00:24:31,000
Zen is de essentie van de Aziatische cultuur. 

129
00:24:31,760 --> 00:24:39,580
Als je zen beoefent, zul je Aziatische beschavingen begrijpen. 

130
00:24:40,660 --> 00:24:55,280
Zal er een spirituele wedergeboorte zijn door de ontmoeting van de westerse traditie en zen? 

131
00:24:55,620 --> 00:24:59,780
Ik denk het wel. Ik hoop het. 

132
00:25:00,380 --> 00:25:04,780
Het is belangrijk dat iedereen God wordt. 

133
00:25:05,120 --> 00:25:07,120
Hoe kan dit worden bereikt? 

134
00:25:07,400 --> 00:25:11,320
Als je zazen beoefent, kun je God worden. 

135
00:25:11,660 --> 00:25:21,300
Is de ware god de natuurlijke god in ons? 

136
00:25:21,520 --> 00:25:29,520
Ja, iedereen moet de ware god creëren. 

137
00:25:31,200 --> 00:25:35,180
De laatste les 

138
00:25:36,600 --> 00:25:40,360
Zen-filosofie is erg breed. 

139
00:25:40,800 --> 00:25:44,980
Je moet tegenstrijdigheden omarmen! 

140
00:25:45,420 --> 00:25:47,440
Leven en dood bijvoorbeeld? 

141
00:25:47,680 --> 00:25:50,400
Ja, leven en dood! 

142
00:28:10,080 --> 00:28:27,360
Toegewijd aan Mokudo Taisen Deshimaru (1914-1982) 

