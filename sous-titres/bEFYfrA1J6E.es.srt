1
00:00:08,660 --> 00:00:11,260
¡Que tengas un buen día!

2
00:00:11,560 --> 00:00:19,400
¿Cómo sentarse en la meditación Zen, zazen?

3
00:00:19,800 --> 00:00:22,600
Déjeme explicarle.

4
00:00:22,700 --> 00:00:27,880
Lo único que necesitas,

5
00:00:28,340 --> 00:00:33,020
es ser un ser humano y tener un cojín de meditación.

6
00:00:33,020 --> 00:00:36,680
Si tienes un cojín de meditación, bien.

7
00:00:37,100 --> 00:00:40,560
Si no, puedes hacer una con una manta.

8
00:00:53,720 --> 00:00:57,580
La meditación Zen es una meditación sentada,

9
00:00:57,940 --> 00:01:00,820
inmóvil y silencioso.

10
00:01:04,425 --> 00:01:07,445
Nos sentamos frente a la pared.

11
00:01:08,400 --> 00:01:11,040
Ahora estoy sentado frente a la cámara,

12
00:01:11,380 --> 00:01:15,900
pero durante la meditación, te sientas frente a la pared.

13
00:01:16,520 --> 00:01:21,940
Vamos a ver tres aspectos diferentes.

14
00:01:27,300 --> 00:01:32,680
La meditación consiste en practicar estos tres aspectos al mismo tiempo.

15
00:01:33,420 --> 00:01:36,955
El primer aspecto es la postura física.

16
00:01:36,955 --> 00:01:38,435
Con el cuerpo.

17
00:01:39,175 --> 00:01:40,625
El segundo aspecto es

18
00:01:41,020 --> 00:01:42,900
...respirando.

19
00:01:46,300 --> 00:01:48,720
El tercer aspecto es

20
00:01:48,880 --> 00:01:50,320
actitud mental.

21
00:01:50,620 --> 00:01:56,180
¿Qué hacemos mientras estamos sentados sin movernos,

22
00:01:56,180 --> 00:01:59,080
y en silencio frente a la pared?

23
00:02:06,160 --> 00:02:09,600
La postura del cuerpo.

24
00:02:09,635 --> 00:02:12,585
La postura concreta de la meditación Zen.

25
00:02:12,585 --> 00:02:17,340
Estás sentado en un cojín.

26
00:02:17,760 --> 00:02:21,440
No en el borde del cojín, sino en la parte superior.

27
00:02:21,440 --> 00:02:24,640
Cruza las piernas.

28
00:02:24,640 --> 00:02:28,160
Para que tus rodillas

29
00:02:28,220 --> 00:02:32,540
caen o crecen en el suelo.

30
00:02:37,840 --> 00:02:41,120
Agarra tu pierna, lo mejor que puedas.

31
00:02:41,380 --> 00:02:44,020
Como esto, o

32
00:02:44,685 --> 00:02:47,435
si eres flexible, puedes colocarlo aquí.

33
00:02:47,435 --> 00:02:50,585
Medio loto.

34
00:02:50,585 --> 00:02:54,980
También puedes ponerte la parte superior de la pierna.

35
00:02:55,240 --> 00:02:59,040
Colóquelo de manera que se sienta lo más cómodo posible.

36
00:02:59,260 --> 00:03:02,240
Porque no nos estamos moviendo.

37
00:03:02,500 --> 00:03:07,120
Así que tenemos que encontrar una posición cómoda.

38
00:03:07,120 --> 00:03:10,600
Para que no te muevas,

39
00:03:10,600 --> 00:03:13,940
y para guardar silencio.

40
00:03:14,160 --> 00:03:17,460
Las rodillas tocan el suelo.

41
00:03:17,655 --> 00:03:19,655
¿Cómo lo haces?

42
00:03:19,660 --> 00:03:23,800
La pelvis se inclina un poco hacia adelante.

43
00:03:27,420 --> 00:03:31,000
Si la pelvis está posicionada al revés,

44
00:03:32,020 --> 00:03:36,180
que estoy bombardeando mi espalda,

45
00:03:36,180 --> 00:03:39,020
o como cuando conduzco un coche,

46
00:03:41,025 --> 00:03:43,860
mis rodillas están levantadas.

47
00:03:43,860 --> 00:03:47,060
Y quiero que mis rodillas bajen.

48
00:03:47,060 --> 00:03:51,700
En el Zen, decimos, "Empuja la tierra con tus rodillas".

49
00:03:51,900 --> 00:03:54,940
Y ese movimiento, con la gravedad,

50
00:03:55,040 --> 00:03:57,300
baja.

51
00:03:57,300 --> 00:04:03,120
Con esta ligera inclinación de la pelvis,

52
00:04:03,460 --> 00:04:06,995
no hay esfuerzo muscular.

53
00:04:12,355 --> 00:04:15,315
Si eres así,

54
00:04:15,780 --> 00:04:19,160
la pelvis está inclinada hacia adelante.

55
00:04:20,220 --> 00:04:21,700
Inclínalo un poco hacia arriba

56
00:04:22,720 --> 00:04:23,780
todo el camino hasta aquí.

57
00:04:24,960 --> 00:04:27,040
Perfectamente recto.

58
00:04:27,040 --> 00:04:30,080
Si te sientas así de forma natural,

59
00:04:30,080 --> 00:04:31,740
el centro de gravedad es

60
00:04:31,740 --> 00:04:34,875
más adelante.

61
00:04:34,875 --> 00:04:37,555
Y tus rodillas van a tocar el suelo.

62
00:04:39,555 --> 00:04:42,595
Desde la inclinación de la pelvis,

63
00:04:42,945 --> 00:04:45,725
...endereza su columna vertebral.

64
00:04:46,165 --> 00:04:48,885
Tanto como sea posible.

65
00:04:52,100 --> 00:04:55,460
Empuja el cielo

66
00:04:55,460 --> 00:04:58,700
con la parte superior de su cabeza.

67
00:05:04,620 --> 00:05:06,680
En el zen, la columna vertebral

68
00:05:07,560 --> 00:05:10,720
comienza aquí.

69
00:05:10,720 --> 00:05:14,020
Si enderezamos nuestra columna vertebral,

70
00:05:14,040 --> 00:05:17,100
tienes que tratar de estirarlo hasta aquí.

71
00:05:17,380 --> 00:05:18,740
Así que no vamos a

72
00:05:19,260 --> 00:05:21,400
piensa con tu cabeza

73
00:05:22,335 --> 00:05:25,525
hacia adelante, no hacia atrás.

74
00:05:26,015 --> 00:05:28,255
El mentón debe ser

75
00:05:29,080 --> 00:05:31,240
lo más recto posible.

76
00:05:31,960 --> 00:05:35,920
Mantenemos una línea vertical recta

77
00:05:36,280 --> 00:05:38,320
entre los ojos,

78
00:05:39,285 --> 00:05:40,345
la boca,

79
00:05:45,605 --> 00:05:46,695
y el ombligo.

80
00:05:54,060 --> 00:05:56,365
La postura de las manos.

81
00:05:56,365 --> 00:05:58,495
La mano izquierda,

82
00:05:59,765 --> 00:06:01,175
con los dedos apretados,

83
00:06:01,575 --> 00:06:04,725
se coloca en la mano derecha.

84
00:06:06,600 --> 00:06:09,740
Los dedos se superponen.

85
00:06:11,765 --> 00:06:13,975
Una línea horizontal con,

86
00:06:13,975 --> 00:06:17,115
pulgares, tocándose entre sí.

87
00:06:17,360 --> 00:06:21,280
Como sostener una pluma.

88
00:06:22,885 --> 00:06:24,925
Sin presión muscular.

89
00:06:25,915 --> 00:06:29,125
Las palmas de las manos.

90
00:06:33,675 --> 00:06:36,545
Más o menos en el medio del vientre.

91
00:06:36,760 --> 00:06:40,900
Así que, para mantener esta postura.

92
00:06:41,620 --> 00:06:47,740
Tienes que tomar cualquier pedazo de tela,

93
00:06:48,140 --> 00:06:51,800
que te ayudará

94
00:06:53,680 --> 00:06:56,675
para soltar tus manos,

95
00:06:56,680 --> 00:07:00,920
libremente, contra tu vientre.

96
00:07:01,180 --> 00:07:06,080
Sin sentirse obligado a usar los dedos o los hombros.

97
00:07:06,400 --> 00:07:09,600
Al contrario, puedes liberar todas las tensiones.

98
00:07:10,180 --> 00:07:12,780
Los codos están libres.

99
00:07:20,125 --> 00:07:22,925
Recto hacia adelante.

100
00:07:23,820 --> 00:07:27,460
Mano izquierda en la mano derecha.

101
00:07:27,740 --> 00:07:31,000
Los pulgares se están tocando ligeramente.

102
00:07:31,000 --> 00:07:34,480
Las manos en la base del abdomen.

103
00:07:35,140 --> 00:07:36,460
Codos rectos y sueltos.

104
00:07:40,120 --> 00:07:42,540
Es la postura.

105
00:07:47,640 --> 00:07:49,840
del cuerpo.

106
00:07:49,840 --> 00:07:53,240
Una vez que ponga mi cuerpo

107
00:07:54,345 --> 00:07:57,225
en la postura de meditación,

108
00:07:57,225 --> 00:08:00,455
en silencio frente a una pared,

109
00:08:01,280 --> 00:08:02,680
¿Qué está pasando?

110
00:08:04,760 --> 00:08:08,160
Frente a la pared, no está pasando nada interesante.

111
00:08:08,640 --> 00:08:12,380
Vamos a volver la mirada hacia adentro.

112
00:08:17,975 --> 00:08:20,795
Los ojos están medio abiertos.

113
00:08:21,085 --> 00:08:24,345
A 45 grados en la diagonal.

114
00:08:24,345 --> 00:08:25,820
Abajo.

115
00:08:26,000 --> 00:08:28,985
Es como quitarnos los ojos

116
00:08:28,985 --> 00:08:32,155
y los puso delante de ti.

117
00:08:32,245 --> 00:08:34,785
No hay nada interesante que ver.

118
00:08:35,160 --> 00:08:38,920
Hacemos lo mismo con los otros sentidos.

119
00:08:39,160 --> 00:08:43,180
Estamos hablando de la meditación silenciosa.

120
00:08:44,440 --> 00:08:48,780
No necesitas nada. No hay música, nada.

121
00:08:49,080 --> 00:08:51,580
Un silencio interior.

122
00:08:56,475 --> 00:08:58,315
Tampoco hay necesidad de hablar.

123
00:09:00,900 --> 00:09:02,880
Cuando estoy inmóvil.

124
00:09:02,880 --> 00:09:06,760
Mi visión es inútil.

125
00:09:06,760 --> 00:09:09,000
Tampoco mi oído.

126
00:09:09,280 --> 00:09:10,820
Ni mis palabras.

127
00:09:14,500 --> 00:09:17,920
Nuestros sentidos, con los que percibimos el mundo que nos rodea,

128
00:09:17,920 --> 00:09:21,300
los ponemos en "modo avión".

129
00:09:24,300 --> 00:09:27,400
Es el principio de la postura.

130
00:09:27,400 --> 00:09:30,140
Al establecer

131
00:09:30,740 --> 00:09:33,675
esta postura física y

132
00:09:33,675 --> 00:09:36,775
...para aislarnos del exterior.

133
00:09:36,775 --> 00:09:39,915
Para alejarnos de nuestros sentimientos.

134
00:09:40,060 --> 00:09:43,940
Entonces podemos empezar a meditar de verdad.

135
00:09:51,140 --> 00:09:55,440
Ahora que tenemos una postura más tranquila,

136
00:09:56,040 --> 00:09:59,380
Estoy parado frente a una pared, meditando.

137
00:09:59,685 --> 00:10:01,975
Y con ese silencio,

138
00:10:02,495 --> 00:10:04,675
Me doy cuenta de que

139
00:10:05,480 --> 00:10:09,440
mi cerebro es muy activo.

140
00:10:09,780 --> 00:10:12,880
Me doy cuenta

141
00:10:13,120 --> 00:10:17,080
que tengo muchos pensamientos.

142
00:10:17,080 --> 00:10:22,740
Que desaparecerá con nuestra mirada.

143
00:10:23,040 --> 00:10:26,240
Pensamientos, imágenes visuales.

144
00:10:26,245 --> 00:10:28,235
Sonidos.

145
00:10:28,865 --> 00:10:29,945
Muchas cosas.

146
00:10:33,720 --> 00:10:37,075
En nuestra meditación Zen,

147
00:10:37,080 --> 00:10:40,205
vamos a dejar que esos pensamientos pasen.

148
00:10:40,205 --> 00:10:43,180
Este es un punto fundamental

149
00:10:43,180 --> 00:10:45,500
de la postura de meditación Zen.

150
00:10:45,640 --> 00:10:49,200
Cuando estoy en zazen,

151
00:10:49,200 --> 00:10:53,380
No paso mi tiempo pensando en

152
00:10:53,380 --> 00:10:57,240
algo, interesante o no.

153
00:10:57,420 --> 00:10:59,340
No se trata de eso.

154
00:10:59,580 --> 00:11:03,700
Es más o menos como nuestros cuerpos

155
00:11:03,715 --> 00:11:06,685
que hemos puesto en una situación de calma.

156
00:11:06,825 --> 00:11:10,215
Voy a poner mi mente y mi espíritu

157
00:11:10,505 --> 00:11:13,415
que siempre está inquieto,

158
00:11:19,040 --> 00:11:20,500
en un estado de calma.

159
00:11:23,640 --> 00:11:26,620
Deja que mis pensamientos escapen.

160
00:11:27,760 --> 00:11:31,800
Nuestros pensamientos vienen de fuera.

161
00:11:32,680 --> 00:11:35,200
Si te lo estás preguntando:

162
00:11:35,200 --> 00:11:39,040
"¿Qué vamos a pensar en cinco minutos o media hora? ¿O mañana?"

163
00:11:39,305 --> 00:11:42,575
Nadie lo sabe.

164
00:11:43,440 --> 00:11:46,780
Incluso mientras dormimos,

165
00:11:46,960 --> 00:11:50,080
que soñamos.

166
00:11:50,540 --> 00:11:53,440
Nuestra actividad mental, consciente o inconsciente, nunca se detiene.

167
00:11:53,660 --> 00:11:57,000
Durante la meditación Zen, esta actividad mental,

168
00:11:57,165 --> 00:12:00,445
vamos a bajarla y a calmarla.

169
00:12:00,760 --> 00:12:04,760
Para dejar pasar nuestros pensamientos.

170
00:12:05,100 --> 00:12:07,520
Pero entonces dirás:

171
00:12:07,520 --> 00:12:08,900
"Vale, bien, ¿pero cómo lo hago?"

172
00:12:09,080 --> 00:12:12,920
Gracias a nuestra respiración.

173
00:12:16,440 --> 00:12:19,400
¿Cómo respirar durante el zazen?

174
00:12:29,880 --> 00:12:32,560
Es la respiración abdominal.

175
00:12:34,415 --> 00:12:36,265
Nosotros inspiramos,

176
00:12:36,705 --> 00:12:39,985
llenamos nuestros pulmones,

177
00:12:39,985 --> 00:12:42,185
con cierta generosidad.

178
00:12:42,925 --> 00:12:45,245
Y vamos a centrarnos

179
00:12:45,245 --> 00:12:46,740
especialmente

180
00:12:47,120 --> 00:12:49,900
...a nuestra expiración.

181
00:12:52,740 --> 00:12:55,875
Y exhalar lentamente.

182
00:12:55,875 --> 00:12:59,165
Estamos extendiendo la caducidad.

183
00:13:05,140 --> 00:13:06,720
A través de la nariz.

184
00:13:07,000 --> 00:13:10,360
Despacio, en silencio.

185
00:13:10,700 --> 00:13:13,080
Estamos en meditación silenciosa.

186
00:13:13,300 --> 00:13:16,240
Exhalamos por la nariz.

187
00:13:17,160 --> 00:13:21,260
Mucho tiempo.

188
00:13:21,620 --> 00:13:23,880
Al final de su expiración,

189
00:13:24,625 --> 00:13:27,785
Inspira y hazlo de nuevo.

190
00:13:43,440 --> 00:13:46,980
Si te centras en

191
00:13:47,240 --> 00:13:50,420
inhalación y exhalación, se establece

192
00:13:50,820 --> 00:13:53,500
una respiración muy tranquila.

193
00:13:57,500 --> 00:14:02,600
Necesitas amplificar tu exhalación.

194
00:14:03,160 --> 00:14:07,180
El diafragma está bajando.

195
00:14:08,980 --> 00:14:11,980
Está masajeando los intestinos.

196
00:14:11,980 --> 00:14:13,980
Lo cual es bueno para ellos.

197
00:14:14,160 --> 00:14:17,700
Y lentamente, puedes sentir,

198
00:14:18,160 --> 00:14:20,820
como si algo estuviera abultado

199
00:14:21,145 --> 00:14:23,720
en el área donde lo tenemos en nuestras manos.

200
00:14:24,020 --> 00:14:27,700
Aquí es donde todo sucede.

201
00:14:28,040 --> 00:14:30,520
Aquí no,

202
00:14:31,900 --> 00:14:34,925
pero aquí.

203
00:14:34,925 --> 00:14:39,360
Respire por la nariz y respire en silencio.

204
00:14:46,440 --> 00:14:49,900
Postura física: cruzar las piernas.

205
00:14:50,020 --> 00:14:52,860
Las rodillas tocan el suelo.

206
00:14:53,420 --> 00:14:56,140
La pelvis está ligeramente inclinada hacia adelante.

207
00:14:56,700 --> 00:15:00,760
Enderezamos nuestra columna vertebral.

208
00:15:01,160 --> 00:15:02,840
Hasta la parte superior de la cabeza.

209
00:15:02,840 --> 00:15:08,040
Empujamos el cielo con nuestras cabezas.

210
00:15:08,540 --> 00:15:11,800
Mano izquierda en la mano derecha.

211
00:15:11,925 --> 00:15:13,925
Pulgares horizontales.

212
00:15:13,925 --> 00:15:17,700
Que no forman ni valle ni montaña.

213
00:15:17,960 --> 00:15:20,100
Está bien.

214
00:15:20,100 --> 00:15:24,140
Cuando pones tus manos en contacto con tu vientre,

215
00:15:24,500 --> 00:15:26,640
pones algo debajo...

216
00:15:26,665 --> 00:15:28,675
para una posición más cómoda.

217
00:15:29,125 --> 00:15:31,805
Tienes los codos rectos.

218
00:15:31,805 --> 00:15:32,885
Es nuestra postura física.

219
00:15:33,645 --> 00:15:37,065
Segundo, nuestro estado mental.

220
00:15:37,280 --> 00:15:41,760
Nos aislamos del exterior.

221
00:15:42,160 --> 00:15:44,060
Suena.

222
00:15:44,060 --> 00:15:46,760
No vamos a meditar con una radiografía.

223
00:15:46,760 --> 00:15:48,755
O con tu celular.

224
00:15:48,755 --> 00:15:51,635
Entonces apáguelas.

225
00:15:52,040 --> 00:15:54,620
Estamos silenciando todos los sonidos.

226
00:15:54,620 --> 00:15:58,295
Miramos hacia adentro y volvemos a

227
00:15:58,300 --> 00:16:02,040
ser el observador de lo que está pasando dentro.

228
00:16:02,120 --> 00:16:05,000
¿Y qué está pasando dentro?

229
00:16:05,380 --> 00:16:07,180
Se nota que hay actividad mental.

230
00:16:07,660 --> 00:16:10,720
Los pensamientos surgen.

231
00:16:10,725 --> 00:16:12,665
No podemos controlarlos.

232
00:16:13,155 --> 00:16:16,415
Aparecen automáticamente.

233
00:16:16,440 --> 00:16:21,000
Así que no es cuestión de prevenir esta actividad cerebral.

234
00:16:24,740 --> 00:16:29,740
No se trata de seguir nuestros pensamientos.

235
00:16:29,960 --> 00:16:33,420
Los dejamos pasar.

236
00:16:33,500 --> 00:16:36,385
¿Cómo podemos dejarlos pasar?

237
00:16:36,385 --> 00:16:41,380
En primer lugar, con el enfoque en el aquí y ahora.

238
00:16:41,540 --> 00:16:43,260
En nuestra postura física.

239
00:16:43,795 --> 00:16:46,835
Y con esa concentración.

240
00:16:46,840 --> 00:16:51,640
Aquí y ahora, en mi aliento.

241
00:16:52,080 --> 00:16:54,960
Soy plenamente consciente de mi respiración.

242
00:16:55,265 --> 00:16:58,505
Por mi respiración abdominal.

243
00:16:58,540 --> 00:17:02,580
Así que respiras por la nariz.

244
00:17:03,080 --> 00:17:04,520
Nosotros inspiramos.

245
00:17:05,095 --> 00:17:08,385
Estamos extendiendo la caducidad.

246
00:17:08,900 --> 00:17:10,460
Siempre.

247
00:17:10,700 --> 00:17:14,040
Una exhalación más suave y larga.

248
00:17:14,740 --> 00:17:17,735
Y al final de la exhalación, empezamos de nuevo.

249
00:17:17,740 --> 00:17:19,800
Va y viene como

250
00:17:20,080 --> 00:17:23,920
wavesq en el océano...

251
00:17:24,360 --> 00:17:28,340
Largo, largo, largo.

252
00:17:29,180 --> 00:17:32,605
Te concentras en el aquí y ahora,

253
00:17:32,605 --> 00:17:36,620
en los puntos de postura.

254
00:17:36,920 --> 00:17:41,140
No dormimos durante la meditación.

255
00:17:41,680 --> 00:17:44,880
Y si te concentras al mismo tiempo

256
00:17:45,040 --> 00:17:48,500
en la respiración

257
00:17:48,680 --> 00:17:51,140
...armonioso y silencioso.

258
00:17:51,140 --> 00:17:53,780
Con una exhalación prolongada.

259
00:17:53,940 --> 00:17:57,680
No tendrás tiempo para pensar,

260
00:17:57,800 --> 00:18:00,060
por muy interesantes que sean sus pensamientos.

261
00:18:03,365 --> 00:18:05,665
Meditaremos durante 20 minutos, media hora, una hora.

262
00:18:06,820 --> 00:18:08,540
Depende de ti.

263
00:18:08,720 --> 00:18:12,380
Pero este momento de meditación, de concentración,

264
00:18:12,455 --> 00:18:15,635
le traerá beneficios inmediatos.

265
00:18:15,635 --> 00:18:17,255
Sentirás

266
00:18:18,405 --> 00:18:21,405
un poco más tranquilo,

267
00:18:21,405 --> 00:18:24,665
un poco más ligero.

268
00:18:27,355 --> 00:18:29,355
Vas a crear una distancia entre

269
00:18:29,355 --> 00:18:32,445
a ti mismo y a todas las emociones que surgen en tu vida.

270
00:18:34,785 --> 00:18:37,725
Es una herramienta muy buena.

271
00:18:44,060 --> 00:18:48,420
Ahora que la mitad de la población está confinada.

272
00:19:29,920 --> 00:19:34,960
Consejos para un principiante

273
00:19:35,300 --> 00:19:40,380
En lugar de enfadarse o hacer un escándalo,

274
00:19:48,420 --> 00:19:49,840
Siéntese.

275
00:19:49,995 --> 00:19:53,325
frente a una pared. Adopte esta meditación.

276
00:19:54,440 --> 00:19:58,320
Trabajen en ustedes mismos de esta manera.

277
00:19:58,760 --> 00:20:00,620
Y pronto te darás cuenta

278
00:20:00,625 --> 00:20:02,925
lo bueno que será para ti.
