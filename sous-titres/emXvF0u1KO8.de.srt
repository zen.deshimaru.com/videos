1
00:00:13,360 --> 00:00:18,820
Ein Shodoka-Gedicht, das Gedicht von Meister Yoka Daishi von heute Morgen:

2
00:00:19,740 --> 00:00:29,120
"Wenn mich jemand fragt, welcher Religion ich angehöre, sage ich die Macht von Maka Hannya".

3
00:00:30,800 --> 00:00:35,200
Ich lese einen Auszug aus einem Kommentar von Meister Deshimaru: "Maka Hannya, das bedeutet höchste Weisheit,

4
00:00:35,640 --> 00:00:39,160
das Tiefste, das Universelle.

5
00:00:39,740 --> 00:00:42,000
Was ist ihre Macht, wie mächtig ist sie?

6
00:00:42,340 --> 00:00:45,900
Jeder hat Macht, aber in

7
00:00:46,140 --> 00:00:51,300
Die Hannya Shingyo, im Sutra der Großen Weisheit, der Bodhisattva Avalokiteshvara

8
00:00:51,820 --> 00:00:55,540
...hat die höchste Macht,
Die von Maka Hannya.

9
00:00:56,560 --> 00:01:02,600
Er verstand, dass sein Körper und alle Dinge ku sind, ohne jede saubere Substanz,

10
00:01:03,420 --> 00:01:07,980
er kann allen Wesen helfen
und sie von ihren Ängsten befreien".

11
00:01:08,300 --> 00:01:14,440
Der Beginn des Hannya Shingyo-Sutra ist mit diesem Gedicht verwandt. Ich werde auch lesen

12
00:01:15,340 --> 00:01:19,260
ein Auszug aus einer von Meisters Lehren 
Kosen. Er lehrt", pflegte mein Meister zu sagen.

13
00:01:19,780 --> 00:01:25,460
immer, egal in welcher Situation,
schwierig oder leicht, je besser für den Menschen

14
00:01:26,440 --> 00:01:30,920
ist es, weiterzumachen, beharrlich
sich auf die Praxis von Zazen konzentrieren,

15
00:01:31,380 --> 00:01:37,960
seine Buddhanatur entwickeln". Meister Kosen sagt: "Das ist wichtiger als alles andere.

16
00:01:39,200 --> 00:01:43,900
Immer, wenn es eine Krise gibt, eine
Revolution, ein Krieg ist immer

17
00:01:44,780 --> 00:01:49,020
möglich, die Praxis fortzusetzen, um
zu atmen, sich mit dem Kosmos zu verbinden.

18
00:01:49,620 --> 00:01:55,480
mit Kraft, mit Weisheit
grundlegend. Man sagt, dass der Lotus, der

19
00:01:56,100 --> 00:02:00,740
die aus Feuer geboren wurden, werden niemals zerstört werden".
