1
00:00:07,600 --> 00:00:09,910
Farò la iniziazione.

2
00:00:10,080 --> 00:00:13,570
C'è almeno una persona registrata.

3
00:00:13,850 --> 00:00:17,860
Attraversiamo il municipio!

4
00:00:29,760 --> 00:00:32,305
Le misure di protezione sono:

5
00:00:33,225 --> 00:00:36,380
alcol e gel, più volte, dentro e fuori.

6
00:00:36,530 --> 00:00:39,390
La distanza sociale di un metro e mezzo.

7
00:00:39,550 --> 00:00:44,960
Durante la circolazione, negli spogliatoi e nel dojo.

8
00:00:45,140 --> 00:00:48,740
Indossiamo sempre la maschera.

9
00:00:48,910 --> 00:00:52,370
La portiamo fuori a fare zazen, la fermiamo nel kimono.

10
00:00:52,450 --> 00:00:55,175
E la mettiamo a fare la passeggiata dei parenti.

11
00:00:55,325 --> 00:00:58,940
Teniamo le finestre aperte, anche se è freddo o rumoroso.

12
00:01:00,330 --> 00:01:03,280
Buongiorno, sono le 6:15 del mattino.

13
00:01:03,360 --> 00:01:08,110
Non guidiamo fino al dojo per arrivare in tempo e preparare tutto.

14
00:01:08,330 --> 00:01:11,490
Godetevi il tour della città di Barcellona!

15
00:01:35,120 --> 00:01:37,935
Il nostro dojo in via Tordera è in ristrutturazione.

16
00:01:38,005 --> 00:01:44,940
Ci stiamo preparando per la mezza giornata di zazen...
guidata dal Maestro Pierre Soko alla fine del mese.

17
00:01:45,020 --> 00:01:50,790
È una sala di Barcellona dove si praticano attività legate al buddismo.

18
00:01:50,900 --> 00:01:56,587
L'abbiamo affittata per continuare a praticare lo zazen.

19
00:01:56,700 --> 00:02:00,730
Con il covid, abbiamo deprogrammato tutte le attività.

20
00:02:00,990 --> 00:02:08,090
Non possiamo fare sesshin, o di giorno, o conferenza.

21
00:02:08,470 --> 00:02:14,610
Non posso fare niente di speciale per diffondere la pratica: siamo bloccati.

22
00:02:14,880 --> 00:02:19,040
Manteniamo una pratica al meglio delle nostre capacità.

23
00:02:19,143 --> 00:02:25,460
Ma non possiamo promuovere o sviluppare il dojo Zen.

24
00:02:25,760 --> 00:02:30,375
L'unica pubblicità che possiamo fare è sul web e sui social network.

25
00:02:30,536 --> 00:02:34,905
Ma non affiggiamo manifesti per strada,
una cosa che facciamo da 20 anni.

26
00:02:35,521 --> 00:02:38,825
Ok. Ancora un giorno. Una pandemia.

27
00:02:38,955 --> 00:02:43,280
Oggi abbiamo avuto un bel po' di praticanti,
siamo felici.

28
00:02:50,000 --> 00:03:00,060
La covata ha sconvolto la mia vita familiare e professionale.

29
00:03:02,300 --> 00:03:03,950
Sono francese.

30
00:03:04,041 --> 00:03:12,920
Per il coronavirus, seguo le solite precauzioni:
maschera, lavaggio delle mani...

31
00:03:13,440 --> 00:03:15,390
Ciao, sono Helena.

32
00:03:15,500 --> 00:03:18,140
Mi esercito al Dojo Ryokan da 15 anni.

33
00:03:18,200 --> 00:03:25,850
È molto difficile per me non poter praticare nel dojo.

34
00:03:26,160 --> 00:03:30,660
Ma bisogna fare attenzione,
accettare le cose così come sono.

35
00:03:30,760 --> 00:03:34,080
Siamo nel bel mezzo di una seconda ondata.

36
00:03:34,350 --> 00:03:38,468
Faccio parte del gruppo a rischio e devo restare a casa.

37
00:03:39,100 --> 00:03:42,200
Ho iniziato a praticare lo Zen in "Zen e spiaggia".

38
00:03:42,440 --> 00:03:48,610
Sono un'attrice e il mondo della cultura
è soggetto a molte incertezze...

39
00:03:48,810 --> 00:03:53,970
Ciao, mi chiamo David e sono un praticante del dojo.

40
00:03:54,070 --> 00:03:59,110
Nonostante questa situazione, mi sento al sicuro qui.

41
00:03:59,230 --> 00:04:01,877
E voglio davvero praticare lo zazen.

42
00:04:02,000 --> 00:04:06,200
Torno all'introduzione perché mi esercitavo
al dojo in via Tordera.

43
00:04:06,520 --> 00:04:09,140
E devo fare una piccola introduzione.

44
00:04:09,260 --> 00:04:18,470
Ultimamente, ho provato molta paura.

45
00:04:19,660 --> 00:04:23,890
Voglio prendermi cura di me stesso e vivere.

46
00:04:44,720 --> 00:04:49,120
Per crescere spiritualmente.

47
00:04:51,800 --> 00:04:59,050
Il maestro Dogen ci invita a una pratica altruista e disinteressata.

48
00:05:16,070 --> 00:05:17,650
Buongiorno!

49
00:05:40,030 --> 00:05:42,400
Ci siamo. Fine dello zazen.

50
00:05:42,660 --> 00:05:46,460
Prendo la metropolitana per 40 minuti.

51
00:05:46,690 --> 00:05:51,340
E lo faremo di nuovo domani mattina
alle 7 per il primo zazen.

52
00:05:52,015 --> 00:05:56,015
Sono le 9:00 di sera e come potete vedere,
Barcellona è deserta.

53
00:05:57,239 --> 00:05:58,639
È così che stanno le cose.

54
00:05:58,729 --> 00:06:01,899
Buona notte, a presto!
