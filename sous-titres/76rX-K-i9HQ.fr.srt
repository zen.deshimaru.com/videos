1
00:00:00,280 --> 00:00:03,640
Vie quotidienne 

2
00:00:03,840 --> 00:00:06,460
Un jour… 

3
00:00:07,200 --> 00:00:12,320
Pendant la journée, nous devons arrêter ce que nous faisons régulièrement. 

4
00:00:13,040 --> 00:00:15,320
Arrêtez son travail. 

5
00:00:15,780 --> 00:00:18,780
Et viens. 

6
00:00:19,020 --> 00:00:22,220
Dans son travail, au quotidien, on retrouve l’esprit. 

7
00:00:22,560 --> 00:00:24,720
Cela ne prend que deux respirations. 

8
00:00:25,060 --> 00:00:26,340
Vous travaillez à votre bureau… 

9
00:00:26,680 --> 00:00:28,820
Redressez-le. 

10
00:00:29,340 --> 00:00:32,760
Et respire. 

11
00:00:41,120 --> 00:00:49,260
Et de retourner immédiatement à son être. 

12
00:00:50,960 --> 00:00:54,140
Je connaissais une communauté appelée Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
Ils travaillent très dur. 

14
00:00:57,580 --> 00:01:00,400
Ils travaillent la terre. 

15
00:01:00,660 --> 00:01:02,660
Ils construisent leur maison. 

16
00:01:02,880 --> 00:01:04,540
Ils font beaucoup de travail manuel. 

17
00:01:04,740 --> 00:01:06,040
Ils travaillent toute la journée. 

18
00:01:06,300 --> 00:01:07,500
Ils se lèvent à 5 heures du matin. 

19
00:01:07,720 --> 00:01:08,820
Ils disent leurs prières. 

20
00:01:09,080 --> 00:01:10,840
Ils prennent le petit déjeuner. 

21
00:01:11,160 --> 00:01:12,420
Ils vont travailler. 

22
00:01:12,780 --> 00:01:15,820
Une cloche sonne toutes les heures. 

23
00:01:16,180 --> 00:01:18,060
Tout le monde doit cesser de travailler. 

24
00:01:18,720 --> 00:01:25,520
Et recentrez-vous pendant une minute. 

25
00:01:26,060 --> 00:01:28,960
Chacun à sa manière. 

26
00:01:34,080 --> 00:01:36,040
Et puis ils reviennent. 

27
00:01:36,560 --> 00:01:39,160
Il n’y a donc pas que zazen. 

28
00:01:39,540 --> 00:01:42,100
Il existe de nombreuses façons de revenir à vous-même. 

29
00:01:42,640 --> 00:01:45,220
La vie quotidienne des moines zen… 

30
00:01:45,540 --> 00:01:48,820
Nous vivons notre vie pour zazen. 

31
00:01:49,080 --> 00:01:52,150
Pour collecter cette conscience, cette connaissance. 

32
00:01:52,440 --> 00:01:53,580
Cette énergie. 

33
00:01:54,880 --> 00:01:58,780
Au moment de l’expérience zazen. 

34
00:01:58,900 --> 00:02:03,120
Il ne s’agit pas de renoncer à votre vie pour Zazen. 

35
00:02:03,380 --> 00:02:05,400
Dans un esprit sectaire. 

36
00:02:05,740 --> 00:02:07,680
Ce n’est pas une idéologie. 

37
00:02:07,900 --> 00:02:10,380
Ce n’est pas du "-isme". 

38
00:02:10,840 --> 00:02:14,780
C’est: "Je vis ma vie de telle manière que je puisse vivre cette expérience." 

39
00:02:15,060 --> 00:02:16,560
"Aussi profond que possible." 

40
00:02:16,900 --> 00:02:18,980
Il s’agit d’une expérience de méditation zazen. 

41
00:02:19,220 --> 00:02:25,060
Lorsque vous êtes à zazen, vous avez la possibilité de changer toute votre vie. 

42
00:02:25,400 --> 00:02:27,900
C’est ça le zazen! 

43
00:02:28,340 --> 00:02:31,980
Ce n’est pas un petit moyen de se sentir bien. 

44
00:02:32,500 --> 00:02:35,040
Le zen au quotidien… 

45
00:02:35,640 --> 00:02:39,200
Même dans le profit et le travail… 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku", (sans but ni esprit de profit)… 

47
00:02:41,760 --> 00:02:43,140
c’est le secret. 

48
00:02:43,340 --> 00:02:46,320
En d’autres termes, restez concentré sur votre essence. 

49
00:02:46,540 --> 00:02:48,880
Ensuite, nous pouvons tout obtenir. 

50
00:02:49,080 --> 00:02:50,360
Nous avons tout. 

51
00:02:50,880 --> 00:02:55,280
Quelle est la différence entre la vie d’un maître et celle d’un être ordinaire? 

52
00:02:55,660 --> 00:02:59,520
Il n’y a pas d’ordinaire. 

53
00:02:59,840 --> 00:03:05,780
Il n’y a que des créatures extraordinaires. 

54
00:03:06,660 --> 00:03:14,900
La mauvaise chose, c’est que les gens ne savent pas qu’ils sont extraordinaires. 

55
00:03:16,420 --> 00:03:22,260
Je n’aime pas l’expression "juste être"! 

