1
00:00:00,800 --> 00:00:06,680
Kin-hin "Il cammino meditativo dello Zen".

2
00:00:07,600 --> 00:00:09,140
Buongiorno

3
00:00:09,400 --> 00:00:12,060
Oggi vorrei spiegare

4
00:00:12,060 --> 00:00:16,600
come si pratica la meditazione Zen camminando

5
00:00:16,820 --> 00:00:20,300
che noi chiamiamo "kin hin"

6
00:00:20,400 --> 00:00:23,340
Questa passeggiata è fatta

7
00:00:23,340 --> 00:00:26,515
tra due periodi di meditazione seduti.

8
00:00:26,520 --> 00:00:31,140
In generale, una sessione di meditazione Zen

9
00:00:31,140 --> 00:00:36,060
inizia con trenta o quaranta minuti di zazen.

10
00:00:36,360 --> 00:00:38,920
Nella postura ho spiegato 
(in un altro video):

11
00:00:38,920 --> 00:00:41,740
Immobile, silenzioso,

12
00:00:41,740 --> 00:00:43,500
di fronte al muro.

13
00:00:43,660 --> 00:00:46,240
Quindi, questa passeggiata

14
00:00:46,240 --> 00:00:49,860
viene fatto per 5 o 10 minuti.

15
00:00:50,140 --> 00:00:53,420
e alla fine di questa passeggiata,

16
00:00:53,480 --> 00:00:58,480
torniamo dove abbiamo il nostro cuscino per la meditazione

17
00:00:58,700 --> 00:01:04,160
per sedersi in zazen, in piedi davanti al muro.

18
00:01:04,620 --> 00:01:08,580
Come si cammina quando si fa "Kin hin"?

19
00:01:08,900 --> 00:01:11,480
Visto che stiamo camminando,

20
00:01:11,480 --> 00:01:13,780
Muoviamoci.

21
00:01:14,000 --> 00:01:16,820
Come ci muoviamo?

22
00:01:17,660 --> 00:01:20,740
Abbiamo messo i piedi a posto.

23
00:01:21,380 --> 00:01:23,500
Non è così.

24
00:01:23,800 --> 00:01:25,500
Non è così.

25
00:01:25,500 --> 00:01:28,480
Parallela e allineata.

26
00:01:28,480 --> 00:01:34,580
Separati l’uno dall’altro, la distanza di un pugno.

27
00:01:35,300 --> 00:01:40,420
Spostiamo l’equivalente di mezzo metro.

28
00:01:40,880 --> 00:01:47,180
Un piede misura circa 30 o 35 centimetri.

29
00:01:47,640 --> 00:01:50,820
Quindi, faremo dei passi di 6 o 8 pollici

30
00:01:51,000 --> 00:01:52,300
al massimo.

31
00:01:52,600 --> 00:01:55,555
Se guardate i miei piedi,

32
00:01:55,560 --> 00:01:59,100
vedrai i miei passi andare così.

33
00:01:59,240 --> 00:02:02,120
Diciamo mezzo metro.

34
00:02:08,120 --> 00:02:09,120
Beh...

35
00:02:09,520 --> 00:02:12,425
Come faremo a camminare?

36
00:02:12,425 --> 00:02:16,620
Camminiamo con il fiato sospeso.

37
00:02:17,540 --> 00:02:23,840
Il nostro respiro ci dà il ritmo.

38
00:02:25,100 --> 00:02:28,320
Ispirare.

39
00:02:28,320 --> 00:02:31,720
È lo stesso tipo di respirazione che facciamo durante lo zazen.

40
00:02:31,980 --> 00:02:34,760
Noi ispiriamo...

41
00:02:34,760 --> 00:02:38,720
Durante questo tempo in cui prendiamo aria,

42
00:02:38,720 --> 00:02:42,040
una gamba va avanti

43
00:02:44,955 --> 00:02:47,875
Durante l’espirazione,

44
00:02:47,875 --> 00:02:51,075
lento e profondo,

45
00:02:51,075 --> 00:02:53,725
spingendo verso il basso

46
00:02:54,100 --> 00:02:56,960
spingendo l’intestino verso il basso

47
00:02:57,265 --> 00:02:59,440
Durante l’espirazione

48
00:02:59,780 --> 00:03:02,120
per il naso,

49
00:03:02,660 --> 00:03:07,340
metteremo tutto il peso sul corpo

50
00:03:07,520 --> 00:03:10,800
nella gamba anteriore.

51
00:03:11,140 --> 00:03:15,160
Gamba anteriore allungata.

52
00:03:15,160 --> 00:03:18,700
tutta la pianta del piede appoggiata a terra,

53
00:03:18,700 --> 00:03:21,220
compreso il tallone.

54
00:03:21,620 --> 00:03:23,980
La zampa posteriore,

55
00:03:23,980 --> 00:03:26,660
che non ha alcun peso,

56
00:03:27,060 --> 00:03:31,880
viene tenuto libero.

57
00:03:31,880 --> 00:03:34,300
Ma anche,

58
00:03:34,300 --> 00:03:36,340
l’intera pianta del piede,

59
00:03:36,360 --> 00:03:42,660
compreso il tallone, rimane appoggiato a terra.

60
00:03:42,660 --> 00:03:46,540
non alziamo i tacchi.

61
00:03:47,465 --> 00:03:48,595
Quindi,

62
00:03:48,920 --> 00:03:53,260
Ispiro e faccio il passo.

63
00:03:53,460 --> 00:03:57,460
Una lunga e profonda espirazione.

64
00:03:57,660 --> 00:04:00,500
Durante l’espirazione

65
00:04:00,720 --> 00:04:06,100
trasferire il peso del corpo alla gamba anteriore,

66
00:04:06,100 --> 00:04:11,420
alla radice del grande dito davanti,

67
00:04:11,520 --> 00:04:15,140
come se volesse lasciare un segno sul terreno.

68
00:04:15,300 --> 00:04:21,380
Immaginate che il terreno sia argilloso o sabbioso.

69
00:04:22,380 --> 00:04:27,620
Poi l’impronta del piede verrebbe registrata

70
00:04:27,880 --> 00:04:30,900
Ispirazione: faccio il passo.

71
00:04:31,340 --> 00:04:37,740
Espirazione: trasferimento del peso dal corpo alla gamba anteriore

72
00:04:37,940 --> 00:04:40,420
Ho raggiunto la fine della mia espirazione.

73
00:04:40,660 --> 00:04:45,600
Non ho altra scelta che prendere di nuovo un po’ d’aria.

74
00:04:45,640 --> 00:04:52,060
Passerò la gamba che è più indietro, in avanti.

75
00:04:52,300 --> 00:04:55,600
Trasferisco il peso corporeo

76
00:04:55,600 --> 00:04:58,680
sulla gamba che ha fatto la mossa

77
00:04:58,680 --> 00:05:02,260
e di nuovo sull’altra gamba.

78
00:05:07,340 --> 00:05:10,620
Il ritmo del nostro respiro

79
00:05:10,640 --> 00:05:15,300
stabilire il ritmo della camminata

80
00:05:15,860 --> 00:05:17,800
Ovviamente,

81
00:05:17,800 --> 00:05:25,100
manteniamo lo stesso atteggiamento mentale che durante la meditazione da seduti

82
00:05:25,360 --> 00:05:27,920
Mi sono concentrato qui e ora,

83
00:05:27,920 --> 00:05:30,000
in quello che sto facendo.

84
00:05:30,000 --> 00:05:32,400
Non sto seguendo i miei pensieri.

85
00:05:32,400 --> 00:05:35,305
Stesso respiro.

86
00:05:35,305 --> 00:05:39,960
Solo l’attività fisica è cambiata.

87
00:05:39,960 --> 00:05:45,060
"La postura della mano"

88
00:05:46,360 --> 00:05:48,380
Mentre camminiamo,

89
00:05:48,540 --> 00:05:51,660
cosa facciamo con le mani?

90
00:05:52,355 --> 00:05:55,455
quando siamo di fronte al muro,

91
00:05:55,765 --> 00:05:58,545
abbiamo questo atteggiamento

92
00:06:00,020 --> 00:06:02,935
Lascia cadere la mano destra.

93
00:06:02,940 --> 00:06:05,800
Lascia la mano sinistra.

94
00:06:05,920 --> 00:06:08,760
Mi avvolgo il pollice con le dita.

95
00:06:08,940 --> 00:06:11,920
Tutte le dita insieme,

96
00:06:12,100 --> 00:06:17,020
lasciando risaltare la radice del pollice sinistro.

97
00:06:17,260 --> 00:06:19,400
Mano destra.

98
00:06:19,540 --> 00:06:22,580
Tutte le dita insieme

99
00:06:22,580 --> 00:06:28,800
Le dita della mano destra coinvolgono la mano sinistra.

100
00:06:29,560 --> 00:06:33,180
Mi stringerò un po’ i polsi.

101
00:06:33,760 --> 00:06:36,720
in modo che i due avambracci

102
00:06:36,720 --> 00:06:39,720
sono nella stessa linea.

103
00:06:39,720 --> 00:06:45,780
Non ho un avambraccio più alto dell’altro.

104
00:06:47,300 --> 00:06:51,860
La radice del pollice sinistro

105
00:06:52,640 --> 00:06:57,260
è in contatto con il plesso solare.

106
00:06:57,820 --> 00:06:59,740
La base dello sterno.

107
00:06:59,980 --> 00:07:03,860
Ogni essere umano ha un piccolo buco qui,

108
00:07:03,880 --> 00:07:05,760
alla base dello sterno.

109
00:07:05,780 --> 00:07:14,160
è lì che devi entrare in contatto con la radice del tuo pollice.

110
00:07:15,060 --> 00:07:18,640
Gli avambracci paralleli al pavimento.

111
00:07:18,700 --> 00:07:22,420
Anche le mie mani.

112
00:07:22,820 --> 00:07:25,020
Non così.

113
00:07:25,480 --> 00:07:30,080
Mantengo questa postura con le spalle rilassate.

114
00:07:30,200 --> 00:07:33,360
Non mantengo questa postura con le spalle così:

115
00:07:33,400 --> 00:07:35,360
con i gomiti che salgono.

116
00:07:35,500 --> 00:07:38,260
ma non in questo modo.

117
00:07:40,820 --> 00:07:42,880
Come en zazen:

118
00:07:42,940 --> 00:07:44,600
colonna dritta,

119
00:07:44,680 --> 00:07:46,020
nuca estesa,

120
00:07:46,360 --> 00:07:48,320
è arrivato il mento,

121
00:07:48,340 --> 00:07:51,920
Guarda la 45° diagonale.

122
00:07:54,520 --> 00:07:57,320
Ora, come farò a camminare?

123
00:07:57,900 --> 00:08:02,600
Durante l’ispirazione,

124
00:08:02,600 --> 00:08:04,120
stiamo facendo un passo avanti.

125
00:08:06,800 --> 00:08:09,660
In espirazione

126
00:08:10,840 --> 00:08:15,740
...si trasferisce il peso del corpo da una gamba all’altra.

127
00:08:15,820 --> 00:08:18,060
proprio come ho spiegato.

128
00:08:18,060 --> 00:08:20,240
Allo stesso tempo,

129
00:08:20,240 --> 00:08:25,740
ti stringerai un po’ la mano.

130
00:08:25,740 --> 00:08:30,500
Ecco perché la flessibilità dei polsi è importante.

131
00:08:31,980 --> 00:08:33,560
Allo stesso tempo,

132
00:08:33,680 --> 00:08:38,235
una leggera pressione dalla radice del pollice sinistro

133
00:08:38,235 --> 00:08:41,405
contro il plesso solare.

134
00:08:42,020 --> 00:08:45,120
Tutto questo durante l’espirazione.

135
00:08:46,120 --> 00:08:51,540
Resumen

136
00:08:53,040 --> 00:08:56,500
Mano sinistra, pollice, dita unite,

137
00:08:56,520 --> 00:08:58,760
mano destra,

138
00:08:58,760 --> 00:08:59,580
in cima.

139
00:08:59,780 --> 00:09:04,000
La radice del pollice sinistro sullo sterno.

140
00:09:04,160 --> 00:09:08,880
Io ispiro.

141
00:09:09,760 --> 00:09:13,680
La mia gamba va avanti

142
00:09:14,140 --> 00:09:16,120
Fase di espirazione:

143
00:09:16,300 --> 00:09:17,340
En quanto

144
00:09:17,480 --> 00:09:22,120
Sposto tutto il mio peso corporeo su questa gamba,

145
00:09:22,120 --> 00:09:24,480
ben tesa,

146
00:09:24,800 --> 00:09:27,920
pressa a mano

147
00:09:27,920 --> 00:09:30,340
e contro il plesso solare.

148
00:09:31,280 --> 00:09:32,620
Ispirazione.

149
00:09:32,860 --> 00:09:35,620
Un passo avanti.

150
00:09:35,620 --> 00:09:37,840
Espirazione lunga e profonda

151
00:09:38,020 --> 00:09:41,300
il peso sulla gamba anteriore.

152
00:09:41,320 --> 00:09:45,940
Pressione tra le mani e contro il plesso solare.

153
00:09:48,440 --> 00:09:51,180
Fine dell’espirazione

154
00:09:51,320 --> 00:09:55,640
Faccio un passo e ne approfitto per rilassarmi.

155
00:09:55,740 --> 00:09:57,500
Ci vado di nuovo...

156
00:09:57,540 --> 00:10:00,660
Premo il terreno

157
00:10:00,820 --> 00:10:03,815
con il peso del mio corpo.

158
00:10:03,820 --> 00:10:05,040
pressione tra le mani

159
00:10:05,240 --> 00:10:07,380
e contro il plesso.

160
00:10:07,420 --> 00:10:09,460
Le pressioni sono dolci.

161
00:10:09,460 --> 00:10:13,000
Non si tratta di farsi male.

162
00:10:14,140 --> 00:10:15,840
Tutto questo,

163
00:10:15,840 --> 00:10:20,340
guidati dal ritmo del nostro stesso respiro,

164
00:10:20,340 --> 00:10:22,460
per 5 o 10 minuti

165
00:10:22,660 --> 00:10:24,920
è che poi torniamo,

166
00:10:24,920 --> 00:10:28,980
dove si trova il nostro cuscino per la meditazione.

167
00:10:29,280 --> 00:10:30,280
Nella vita di tutti i giorni,

168
00:10:30,560 --> 00:10:34,740
questa passeggiata meditativa chiamata "Kin hin"

169
00:10:35,560 --> 00:10:37,940
è molto pratico, molto efficiente.

170
00:10:38,960 --> 00:10:43,040
Forse sei in un momento di attesa

171
00:10:43,520 --> 00:10:46,075
o in tempi di stress.

172
00:10:46,075 --> 00:10:47,805
E puoi isolarti

173
00:10:48,200 --> 00:10:50,860
per 2, 3 o 5 minuti.

174
00:10:50,980 --> 00:10:54,340
Dove vi collegherete con il qui e ora

175
00:10:54,815 --> 00:10:57,965
Nella coscienza del tuo respiro

176
00:10:57,965 --> 00:11:03,840
e tenendo conto dei punti che ho spiegato.

177
00:11:04,620 --> 00:11:07,755
Praticare questa passeggiata di meditazione

178
00:11:07,760 --> 00:11:15,480
vedrete quanto velocemente vi sentirete meno stressati

179
00:11:15,480 --> 00:11:21,880
più rilassato e in armonia.

180
00:11:22,880 --> 00:11:27,820
Spero che ora possiate fare uno zazen completo.

181
00:11:27,820 --> 00:11:31,060
Cioè, una prima parte seduta,

182
00:11:31,060 --> 00:11:32,055
allora,

183
00:11:32,055 --> 00:11:34,855
5 o 10 minuti di questa passeggiata

184
00:11:35,360 --> 00:11:38,960
e tornare ad una seconda parte della meditazione.

185
00:11:39,140 --> 00:11:40,300
Grazie mille.