1
00:00:00,280 --> 00:00:02,560
I don’t know much about sophrology.

2
00:00:02,600 --> 00:00:12,140
I know that it’s an application of certain wellness and personal development practices,

3
00:00:12,540 --> 00:00:17,160
and that there’s a type of sophrology that’s called “orientalist”,

4
00:00:17,440 --> 00:00:24,080
which is very much inspired by Yogic, Buddhist, or Indian practices.

5
00:00:24,540 --> 00:00:27,940
Zen is very simple.

6
00:00:28,380 --> 00:00:38,400
It’s based on fundamental human postures.

7
00:00:39,820 --> 00:00:48,480
I’m going to show you the first fundamental posture of Zen.

8
00:01:01,500 --> 00:01:06,120
Human beings used to be monkeys.

9
00:01:06,460 --> 00:01:08,580
They walked like this.

10
00:01:09,200 --> 00:01:20,360
Little by little, they sat up straight, like Zen masters.

11
00:01:22,900 --> 00:01:24,900
Then they straightened up.

12
00:01:25,180 --> 00:01:28,600
They walked like this.

13
00:01:29,680 --> 00:01:33,760
And human consciousness has evolved

14
00:01:35,180 --> 00:01:39,980
at the same time as the posture.

15
00:01:40,340 --> 00:01:46,000
The posture of the body is a fundamental thought.

16
00:01:46,340 --> 00:01:52,200
For example, Usain Bolt, the fastest runner in the world,

17
00:01:52,540 --> 00:01:55,160
makes this gesture.

18
00:01:55,440 --> 00:02:02,300
Everyone understands what it means, and yet there’re no words.

19
00:02:02,640 --> 00:02:05,440
It’s just a posture.

20
00:02:05,900 --> 00:02:11,120
The way we sit and walk

21
00:02:11,520 --> 00:02:13,400
what we do with our hands,

22
00:02:13,700 --> 00:02:16,500
all this is of primary importance.

23
00:02:18,160 --> 00:02:21,440
In Zen, there’s the posture of Buddha.

24
00:02:22,980 --> 00:02:31,120
It existed before the Buddha, they speak about it in the Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
They sit on a cushion of grass and fold their legs.

26
00:02:41,560 --> 00:02:47,140
In nature, you communicate with the ears, 

27
00:02:47,440 --> 00:02:53,220
with the nose, with the eyes, to see far.

28
00:02:54,920 --> 00:02:59,000
Therefore, they wanted to stand upright.

29
00:02:59,400 --> 00:03:04,220
And as they straightened up, their consciousness sharpened, awakened.

30
00:03:06,040 --> 00:03:08,720
Here’s the first posture. 

31
00:03:08,980 --> 00:03:11,580
Normally, we have to do a complete lotus.

32
00:03:12,060 --> 00:03:14,260
We cross our legs like this.

33
00:03:14,700 --> 00:03:18,460
We call it the “pretzel”.

34
00:03:24,960 --> 00:03:30,980
It’s very important to take this posture.

35
00:03:31,380 --> 00:03:37,380
The previous speaker talked about dynamism and relaxation.

36
00:03:37,780 --> 00:03:42,340
We have a sympathetic and parasympathetic nervous system.

37
00:03:42,600 --> 00:03:45,800
We have the outer muscles and deep muscles.

38
00:03:46,320 --> 00:03:51,400
We have shallow breathing and deep breathing.

39
00:03:51,820 --> 00:03:54,620
We have superficial, cortical consciousness,

40
00:03:54,920 --> 00:03:59,760
and deep, reptilian consciousness, in the middle brain.

41
00:04:00,340 --> 00:04:04,160
To stand upright in this posture,

42
00:04:04,440 --> 00:04:06,800
you have to push the earth with your knees.

43
00:04:10,240 --> 00:04:15,640
It requires an effort to be dynamic and to have a good posture.

44
00:04:16,000 --> 00:04:22,180
My master [Deshimaru] insisted a lot on the beauty and strength of the posture.

45
00:04:22,460 --> 00:04:31,280
But in lotus, the more you relax, the more the feet push on the thighs,

46
00:04:31,520 --> 00:04:35,100
and the more the knees push on the ground.

47
00:04:35,620 --> 00:04:40,280
The more you relax, the more dynamic you are, the straighter and stronger you are.

48
00:04:40,560 --> 00:04:46,560
It has nothing to do with relaxing in a chair or bed.

49
00:04:46,940 --> 00:04:57,280
It’s a way of abandoning the body to itself.

50
00:05:04,380 --> 00:05:09,700
While being perfectly awake.

51
00:05:10,120 --> 00:05:16,240
It’s the first posture, the sitting posture of the Buddha.

52
00:05:16,680 --> 00:05:20,280
Hands are also very important.

53
00:05:20,640 --> 00:05:23,600
We’re always doing something with our hands:

54
00:05:23,860 --> 00:05:26,560
working, protecting ourselves…

55
00:05:26,960 --> 00:05:28,740
The hands are very expressive

56
00:05:29,040 --> 00:05:31,040
and are connected to the brain.

57
00:05:31,500 --> 00:05:41,040
Taking this mudra with your hands changes consciousness.

58
00:05:41,680 --> 00:05:47,680
Concentration is in our hands, body, nervous system, and brain.

59
00:05:48,100 --> 00:05:53,360
Another important thing is thought and consciousness.

60
00:05:53,820 --> 00:05:57,680
If you’re like Rodin’s Thinker,

61
00:05:58,000 --> 00:06:01,100
you don’t have the same consciousness than

62
00:06:09,960 --> 00:06:12,240
when your posture is perfectly straight,

63
00:06:12,580 --> 00:06:14,640
especially with the neck stretched out,

64
00:06:15,700 --> 00:06:18,400
your head quite straight, open towards the sky,

65
00:06:18,760 --> 00:06:22,020
the nose and the navel aligned,

66
00:06:25,060 --> 00:06:27,220
and when you breathe with your whole being.

67
00:06:29,020 --> 00:06:31,860
Thought is then no longer dissociated from the body.

68
00:06:32,240 --> 00:06:36,100
There’s no more dissociation between our consciousness

69
00:06:36,300 --> 00:06:39,080
and the smallest cell of our body.

70
00:06:39,500 --> 00:06:45,520
In Zen, this is called “body-mind”, in a single word.

71
00:06:46,360 --> 00:06:50,540
This is the fundamental posture: zazen.

72
00:06:56,380 --> 00:07:00,200
The second posture is the posture in walking.

73
00:07:00,680 --> 00:07:06,340
In zazen, we abandon everything and don’t move at all.

74
00:07:06,560 --> 00:07:08,280
Therefore, consciousness is special.

75
00:07:08,720 --> 00:07:13,800
But another meditation is done while walking.

76
00:07:16,180 --> 00:07:18,160
We have to move.

77
00:07:18,540 --> 00:07:20,540
We do something, we move forward.

78
00:07:21,180 --> 00:07:24,180
You don’t move forward to catch something.

79
00:07:24,580 --> 00:07:28,380
We rhythm our breathing with walking.

80
00:07:29,020 --> 00:07:30,740
The choreographer Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
who was a disciple of my master Deshimaru, 

82
00:07:34,060 --> 00:07:38,040
made his dancers practice this every day.

83
00:07:39,060 --> 00:07:45,340
The second posture is the “kin-hin” walk.

84
00:07:45,880 --> 00:07:51,040
Notice what you do with your hands.

85
00:07:51,680 --> 00:07:57,840
A boxer who gets his picture taken does this with his hands.

86
00:07:58,240 --> 00:08:05,220
When you put your hands like this, the brain automatically expresses a strong aggressiveness.

87
00:08:05,820 --> 00:08:10,360
You can put your hands in your pockets.

88
00:08:10,840 --> 00:08:13,140
“Where did I put my wallet?” 

89
00:08:13,640 --> 00:08:16,360
“They stole my cell phone!”

90
00:08:16,700 --> 00:08:19,580
You can put your fingers in your nose. 

91
00:08:21,660 --> 00:08:25,820
Everything that has been built here has been built by hands.

92
00:08:26,260 --> 00:08:28,820
The hand is extraordinary.

93
00:08:29,160 --> 00:08:31,860
We’re the only animals to have hands,

94
00:08:32,260 --> 00:08:35,340
they’re connected to our evolution.

95
00:08:37,700 --> 00:08:40,660
The hands also meditate. 

96
00:08:40,940 --> 00:08:43,580
One puts the thumb in the fist.

97
00:08:43,820 --> 00:08:49,420
The root of the thumb is placed under the sternum and the other hand is placed on it.

98
00:08:49,760 --> 00:08:55,060
These postural details exist since the Buddha.

99
00:08:55,380 --> 00:09:02,520
Ancient texts explain that they were transmitted to China from India.

100
00:09:02,960 --> 00:09:08,920
It’s an oral transmission from person to person.

101
00:09:09,480 --> 00:09:12,500
In Buddhism, there’re many aspects.

102
00:09:12,780 --> 00:09:17,320
But the meditative and postural aspect is very important.

103
00:09:17,820 --> 00:09:28,540
There were two great disciples of the Buddha, Śāriputra, and Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Their spiritual master was not sure of himself:

105
00:09:37,220 --> 00:09:43,680
“I’m not sure I have the trick to getting out of life and death.”

106
00:09:43,940 --> 00:09:52,860
“If you find a master who teaches this, let me know, and I’ll go with you.”

107
00:09:53,620 --> 00:10:04,320
One day they see a monk, like me, see, walking on the road.

108
00:10:04,780 --> 00:10:10,000
They’re amazed by the beauty of this man.

109
00:10:10,420 --> 00:10:16,200
Then this monk stops in a grove to pee.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, who’s considered one of the most intelligent men of India at the time, tells him:

111
00:10:34,180 --> 00:10:43,940
“We’re amazed by your poise, who’s your master?”

112
00:10:46,560 --> 00:10:49,540
- “My master is Shakyamuni Buddha.”

113
00:10:49,860 --> 00:10:53,520
- “Explain your doctrine to us!”

114
00:10:53,780 --> 00:10:58,220
- “I couldn’t explain it to you, I’m too novice.”

115
00:10:59,100 --> 00:11:04,440
And they only followed this boy because of the way he walked.

116
00:11:04,800 --> 00:11:06,980
His way of expressing something through his attitude.

117
00:11:07,440 --> 00:11:11,680
The second posture is therefore walking.

118
00:11:12,020 --> 00:11:15,280
When people do zazen, it’s very strong.

119
00:11:15,500 --> 00:11:21,580
Even martial arts practitioners are impressed by the power that it exudes.

120
00:11:21,940 --> 00:11:25,860
By the power of the breath.

121
00:11:26,240 --> 00:11:35,520
But how to find this energy, this fullness, in everyday life?

122
00:11:35,840 --> 00:11:39,220
This calm, this concentration.

123
00:11:39,600 --> 00:11:42,960
This is a difficult point.

124
00:11:43,280 --> 00:11:47,140
For the posture of zazen is extremely precise.

125
00:11:47,600 --> 00:11:55,100
In the beginning, we focus on the technical aspects of kin-hin walking.

126
00:11:55,540 --> 00:11:58,800
But once you’ve integrated all the details,

127
00:11:59,300 --> 00:12:02,160
at any time of your daily life

128
00:12:02,440 --> 00:12:08,400
(even if I always forget to do it), you can practice it.

129
00:12:10,600 --> 00:12:16,740
You can refocus on your spiritual being.

130
00:12:17,740 --> 00:12:22,760
On your being that doesn’t screw up.

131
00:12:23,320 --> 00:12:31,340
I told you about hands.

132
00:12:31,840 --> 00:12:39,980
This is the fundamental religious posture.

133
00:12:40,560 --> 00:12:44,240
When you have your hands together like this,

134
00:12:45,080 --> 00:12:48,260
just doing that,

135
00:12:48,580 --> 00:12:51,280
it affects the brain, 

136
00:12:51,620 --> 00:12:55,340
and it builds a bridge between the two cerebral hemispheres.

137
00:12:55,740 --> 00:12:59,960
This posture is used in all religions.

138
00:13:00,280 --> 00:13:02,180
It’s very simple.

139
00:13:02,560 --> 00:13:05,640
See the importance of hands!

140
00:13:06,060 --> 00:13:13,280
The third posture…

141
00:13:13,860 --> 00:13:16,520
(I thought there were four…)

142
00:13:16,880 --> 00:13:20,580
Ah yes, there’s the reclining posture, but we’re not going to talk about that.

143
00:13:21,000 --> 00:13:25,520
The lying posture, how to sleep.

144
00:13:26,700 --> 00:13:31,980
I have explained to you how prehistoric men used to frolic on all fours,

145
00:13:32,260 --> 00:13:34,440
Little by little they straightened up.

146
00:13:34,700 --> 00:13:38,760
And our evolution comes from our posture.

147
00:13:39,060 --> 00:13:42,580
From the irrigation from the spine up to the brain.

148
00:13:42,940 --> 00:13:46,400
And the brain, which is an extraordinary instrument,

149
00:13:46,700 --> 00:13:51,440
of which only 10 to 15% of its total usefulness is known.

150
00:13:51,760 --> 00:13:57,740
We’ve got a big thing between our ears that we’re underutilizing.

151
00:14:08,640 --> 00:14:12,380
Everyone has their own brain!

152
00:14:19,020 --> 00:14:22,140
Everyone has their own brain!

153
00:14:22,400 --> 00:14:25,200
Like an IP address.

154
00:14:30,220 --> 00:14:33,220
Everyone has their own brain!

155
00:14:33,940 --> 00:14:35,900
And we’re alone!

156
00:14:36,700 --> 00:14:41,940
This is very important for what I’m about to tell you.

157
00:14:44,760 --> 00:14:50,040
Our feet (we’ll come back to the feet)…

158
00:14:51,720 --> 00:14:56,420
You know that we live on a small Earth.

159
00:14:57,040 --> 00:15:01,420
Have you seen the movie Gravity?

160
00:15:02,120 --> 00:15:07,060
It’s one of the movies that truly awaken human consciousness.

161
00:15:07,560 --> 00:15:13,380
In Hollywood, they do some work of manipulation,

162
00:15:13,680 --> 00:15:17,220
but also sometimes, of information and spiritual evolution.

163
00:15:17,760 --> 00:15:19,660
This movie is one of them.

164
00:15:20,040 --> 00:15:26,920
And one understands by seeing it, the fragility of our planet Earth.

165
00:15:29,200 --> 00:15:33,400
Usually, when we look at the sky, we marvel at it: “It’s infinitely blue!”.

166
00:15:33,760 --> 00:15:38,940
No. Not infinitely. 30 kilometers of atmosphere.

167
00:15:39,360 --> 00:15:44,660
The end of the atmosphere is between 30 and 1,000 kilometers.

168
00:15:52,260 --> 00:15:56,700
Even a thousand kilometers is nothing at all.

169
00:15:58,800 --> 00:16:04,780
If I put you 30 kilometers away, you don’t even survive a minute.

170
00:16:05,380 --> 00:16:15,940
Even apart from taxes, our living conditions are precarious.

171
00:16:17,420 --> 00:16:20,440
The Earth is a small, fragile, living being.

172
00:16:21,120 --> 00:16:30,660
Our biosphere is tiny and fragile.

173
00:16:30,940 --> 00:16:35,100
We should always be concerned about preserving this stuff.

174
00:16:36,860 --> 00:16:39,940
For we were born from the earth.

175
00:16:40,220 --> 00:16:45,700
This miracle of life that took place on earth is ephemeral.

176
00:16:46,240 --> 00:16:47,840
We were born from the earth.

177
00:16:48,220 --> 00:16:53,320
And we all have our feet pointing to the same center, the center of the Earth.

178
00:16:53,900 --> 00:16:57,760
So our feet are in community.

179
00:17:01,860 --> 00:17:08,680
But our head is unique, no one is pointing in the same direction.

180
00:17:18,400 --> 00:17:23,500
Hence the third posture that I will show you.

181
00:17:23,800 --> 00:17:28,140
It’s common to many religions, it’s called “prostration”.

182
00:17:28,380 --> 00:17:31,240
That’s how we do it.

183
00:17:40,640 --> 00:17:42,780
Ah, that feels good!

184
00:17:43,060 --> 00:17:49,940
When we do this practice, it’s like when we make love:

185
00:17:50,260 --> 00:17:58,460
Sometimes we don’t make love for 10 years, 20 years, 6 months…

186
00:17:58,780 --> 00:18:07,460
And when we make love again we say: “Ah, it feels good, I forgot how good it was!”.

187
00:18:08,220 --> 00:18:12,200
When you do this prostration, it’s the same thing.

188
00:18:13,460 --> 00:18:15,740
I’m not kidding.

189
00:18:16,240 --> 00:18:20,260
We reformat our brain.

190
00:18:21,060 --> 00:18:26,280
When you put your brain in line with the earth.

191
00:18:26,620 --> 00:18:29,080
It’s not just a question of humility,

192
00:18:29,360 --> 00:18:32,720
or bowing down before statues or whatever.

193
00:18:34,600 --> 00:18:38,880
It’s a magical, fundamental thing, a shaman’s thing, maybe?

194
00:18:39,320 --> 00:18:41,860
From the moment you make this prostration…

195
00:18:42,180 --> 00:18:43,340
My master used to say:

196
00:18:43,660 --> 00:18:47,440
“If the heads of state would do this prostration once a day,”

197
00:18:47,800 --> 00:18:50,200
“it would change the face of the world.”

198
00:18:50,640 --> 00:18:54,560
It’s neither blah blah blah blah nor mysticism.

199
00:18:55,280 --> 00:18:58,780
It must be scientifically measurable.

200
00:19:00,780 --> 00:19:05,280
It reformats the brain, to touch the ground with it.

201
00:19:05,740 --> 00:19:09,280
It’s constantly in its individual line.

202
00:19:09,620 --> 00:19:17,300
And there you connect to your root, to your brothers, to the center of the Earth.

203
00:19:19,940 --> 00:19:25,380
Sometimes I do it and feel something strong in my head.

204
00:19:25,660 --> 00:19:27,540
It feels great.

205
00:19:27,820 --> 00:19:39,420
At the same time, it relativizes the evil that we unintentionally do around us.

206
00:19:40,560 --> 00:19:42,960
It’s good medicine.

207
00:19:43,180 --> 00:19:47,040
All these postures are fundamental medicines.

208
00:19:47,760 --> 00:19:53,260
I had prepared a somewhat intellectual text.

209
00:19:53,520 --> 00:19:58,140
But I can’t get from one subject to another.

210
00:19:58,440 --> 00:20:02,300
What was I saying?

211
00:20:06,860 --> 00:20:12,040
I return to the body and prehistory.

212
00:20:12,780 --> 00:20:28,200
It has been proven that Neanderthal men had a religious practice.

213
00:20:30,680 --> 00:20:36,340
Tombs have been found.

214
00:20:36,860 --> 00:20:40,440
They brought offerings to their dead.

215
00:20:40,760 --> 00:20:43,680
They performed a religious ceremony for their dead.

216
00:20:48,420 --> 00:20:52,180
What’s interesting is not only that!

217
00:20:52,720 --> 00:20:56,260
At the time when they were doing these religious ceremonies -

218
00:20:56,740 --> 00:21:01,700
- religious in the sense that they communicated with the invisible -

219
00:21:02,540 --> 00:21:06,940
- with eternal life, with the dead -

220
00:21:08,800 --> 00:21:11,020
- they connected to the invisible.

221
00:21:11,420 --> 00:21:18,140
At that time, articulated language didn’t exist yet.

222
00:21:18,560 --> 00:21:24,380
So, religion existed before articulated language.

223
00:21:24,840 --> 00:21:30,140
(I’m willing to move on to articulated language.)

224
00:21:31,480 --> 00:21:36,500
(It’s very interesting, it took me four days to write it…)

225
00:21:43,820 --> 00:21:45,660
My master used to say:

226
00:21:45,920 --> 00:21:48,880
“Zazen is the religion before religion.”

227
00:21:49,380 --> 00:21:54,380
We’d say, “Yeah, you always want to be the first…”

228
00:21:55,860 --> 00:21:59,700
But this is the religion before religion as we know it.

229
00:21:59,960 --> 00:22:02,720
Where one refers to books.

230
00:22:03,080 --> 00:22:06,280
Books come after religion, not before.

231
00:22:06,720 --> 00:22:11,940
Before, humans couldn’t even speak.

232
00:22:12,360 --> 00:22:15,060
So they were already taking this posture.

233
00:22:15,480 --> 00:22:19,620
These are prehistoric postures.

234
00:22:21,460 --> 00:22:23,740
(I only have 3 minutes left?)

235
00:22:23,960 --> 00:22:28,320
(All right, then!)

236
00:22:31,020 --> 00:22:34,460
Then, about to sophrology…

237
00:22:36,500 --> 00:22:39,280
I wanted to talk to you about traditional zazen.

238
00:22:39,740 --> 00:22:43,660
I’m dressed as a traditional monk.

239
00:22:47,440 --> 00:22:51,700
Traditional zazen isn’t practiced alone.

240
00:22:52,100 --> 00:22:54,500
It isn’t a wellness technique.

241
00:22:58,260 --> 00:23:05,060
I’ll read you what the Buddha said, I think it’s so great!

242
00:23:08,520 --> 00:23:12,780
Shakyamuni Buddha once said to his audience:

243
00:23:13,120 --> 00:23:18,540
“The study of sutras and teachings” - texts -

244
00:23:18,900 --> 00:23:28,260
“observance of precepts” - in religions this is important, not to kill, not to steal, etc.

245
00:23:29,300 --> 00:23:34,960
“and even the practice of zazen” - sitting meditation

246
00:23:35,900 --> 00:23:43,060
“are incapable of extinguishing human desires, fears, and anxieties.”

247
00:23:43,340 --> 00:23:45,080
This is what Shakyamuni Buddha said.

248
00:23:45,340 --> 00:23:51,660
So, me, a fanatic, he ruins my plans…

249
00:23:52,080 --> 00:23:56,800
Zazen, even the Buddha says it’s useless!

250
00:23:57,600 --> 00:24:01,680
So, what are we going to do?

251
00:24:14,440 --> 00:24:17,480
Of course, there’s more to it…

252
00:24:23,360 --> 00:24:29,360
My master [Deshimaru], when he met his master [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
The latter said in the conference “Zazen is good for nothing, it brings no merit!”.

254
00:24:34,680 --> 00:24:38,800
My master said, “But it’s really interesting if it’s good for nothing!”

255
00:24:39,140 --> 00:24:41,940
We’re always doing things that are good for something!

256
00:24:42,280 --> 00:24:43,760
Something good for nothing?

257
00:24:44,120 --> 00:24:47,400
I want to practice it!
