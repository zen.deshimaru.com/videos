1
00:00:12,840 --> 00:00:20,310
Dans un poème de Yoka Daishi,
un maître chinois qui a écrit

2
00:00:20,310 --> 00:00:25,100
Le Chant de l’immédiat satori,

3
00:00:25,100 --> 00:00:27,100
le Shôdôka

4
00:00:27,800 --> 00:00:35,760
et qui était disciple de Maître Eno, 
dont il a reçu la transmission

5
00:00:35,760 --> 00:00:37,210
lors d’une rencontre qui a duré un jour et une nuit.

6
00:00:37,210 --> 00:00:43,100
Après il a continué l’enseignement.

7
00:00:44,040 --> 00:00:50,040
Son poème : "Abandonne les quatre éléments, ne cherche plus à thésauriser,

8
00:00:50,740 --> 00:00:57,880
dans la paix et l’achèvement absolu,

9
00:00:58,500 --> 00:01:01,940
bois et mange selon tes désirs.

10
00:01:02,360 --> 00:01:08,660
Tous les phénomènes sont impermanents, tout est KU, sans noumène, sans substance propre

11
00:01:09,080 --> 00:01:14,080
et c’est justement cela le grand
et complet satori du Bouddha."

12
00:01:14,560 --> 00:01:17,820
Maître Deshimaru dit qu’ici le poème traite

13
00:01:18,260 --> 00:01:23,620
des Trois Trésors du bouddhisme l’abandon des quatre éléments,

14
00:01:23,900 --> 00:01:30,300
les quatre éléments
c’est l’eau, la terre, le feu, l’air.

15
00:01:30,880 --> 00:01:35,440
Le deuxième Trésor c’est le calme et la
tranquillité du zazen, le nirvana,

16
00:01:36,060 --> 00:01:44,400
le troisième l’impermanence des phénomènes, la prise de conscience, l’absence de noumène des existences.

17
00:01:44,740 --> 00:01:50,120
Dans le premier Trésor, abandonne les quatre éléments

18
00:01:50,120 --> 00:01:56,720
et ne cherche plus à les attraper, à les retenir, à les saisir.

19
00:01:56,720 --> 00:02:04,300
Maître Deshimaru dit que nous ne devons pas rechercher le noumène ou l’ego.

20
00:02:04,300 --> 00:02:09,980
Tout le monde cherche l’ego de son corps
il veut posséder son corps

21
00:02:10,700 --> 00:02:15,580
et de là naissent des sentiments comme la jalousie, l’égoïsme, l’envie.

22
00:02:15,580 --> 00:02:21,940
donc dans le premier trésor,
on apprend à abandonner, lâcher,

23
00:02:21,940 --> 00:02:28,150
son corps, son ego, son esprit.
C’est intéressant parce qu’on peut voir aussi

24
00:02:28,150 --> 00:02:33,580
que ces quatre éléments, abandonner les
quatre éléments et ne plus chercher à

25
00:02:33,580 --> 00:02:39,560
les attraper, à les saisir
en lien avec notre monde.

26
00:02:39,560 --> 00:02:44,000
L’eau, la terre, le feu et l’air .

27
00:02:44,000 --> 00:02:48,840
Dans les enseignements
des Maîtres zen il y a toujours

28
00:02:48,850 --> 00:02:53,560
une intuition profonde et là c’est
intéressant parce que cette intuition

29
00:02:53,560 --> 00:03:00,370
elle rejoint la science moderne, les
sciences du vivant

30
00:03:00,370 --> 00:03:07,120
et de plus en plus de ces scientifiques des sciences du vivant nous disent également

31
00:03:07,120 --> 00:03:13,630
d’ abandonner les quatre éléments,
ne plus chercher à les attraper

32
00:03:13,630 --> 00:03:18,490
les océanologues, les hydrologues
nous expliquent que les océans

33
00:03:18,490 --> 00:03:24,250
c’est les véritables poumons de la terre et
qu’il faut arrêter de les acidifier,

34
00:03:24,250 --> 00:03:30,280
de les tuer, les remplir de plastique.
Les spécialistes de la terre,

35
00:03:30,280 --> 00:03:34,590
avec sa vie végétale, animale, avec ce qu’elle contient dans ses entrailles

36
00:03:34,590 --> 00:03:40,900
ses ressources, ses énergies fossiles,
ses terres rares, il faut les laisser,

37
00:03:40,900 --> 00:03:48,060
il faut les lâcher nous disent les agronomes, les géologues, les biochimistes

38
00:03:48,060 --> 00:03:53,290
les volcanologues. Il faut trouver un
autre fonctionnement.

39
00:03:53,290 --> 00:03:56,740
C’est pas les Maîtres zen là, c’est
vraiment les scientifiques

40
00:03:56,740 --> 00:04:03,760
eux, également maintenant ont compris profondément le principe d’interdépendance.

41
00:04:03,760 --> 00:04:10,810
Le feu que l’être humain utilise sans prudence, sans savoir bien le maîtriser,

42
00:04:10,810 --> 00:04:15,959
la fusion nucléaire atomique de l’atome,
le feu du soleil,

43
00:04:15,959 --> 00:04:21,669
il faut lâcher, on ne sait pas bien ,c’est
trop compliqué.

44
00:04:21,669 --> 00:04:27,340
Il faut trouver un autre fonctionnement plus simple et enfin l’air, que l’on rend irrespirable

45
00:04:27,340 --> 00:04:33,580
avec une atmosphère saturée de CO2, de
méthane, de gaz toxiques,

46
00:04:33,960 --> 00:04:41,160
les climatologues, les météorologistes
les glaciologues et bien d’autres,

47
00:04:41,160 --> 00:04:47,760
nous disent, stop, il faut arrêter, lâcher,
laisser de l’air libre

48
00:04:47,770 --> 00:04:52,330
donc il nous enseignent tous, également,
d’arrêter de vouloir saisir, d’attraper

49
00:04:52,330 --> 00:04:59,100
les quatre éléments. Dans le shôdôka
"abandonne" le caractère chinois c’est "放 fang"

50
00:04:59,100 --> 00:05:06,070
"放 fang" ça veut dire aussi libère ça veut
dire abandonner, libère, lâche.

51
00:05:06,070 --> 00:05:12,600
C’est comme quand on
amène les moutons à paître, on les laisse libres,

52
00:05:12,600 --> 00:05:20,139
brouter l’herbe, tranquille.
Laissez les ressources fossiles dans la terre,

53
00:05:20,139 --> 00:05:27,190
laissez la terre reposer. Maître Deshimaru dans son commentaire disait

54
00:05:27,190 --> 00:05:32,300
qu’on ne doit pas rechercher d’ego dans le
corps, tout le monde veut avoir et posséder ce corps

55
00:05:32,300 --> 00:05:36,450
mais c’est pareil entre nous et
la planète.

56
00:05:36,450 --> 00:05:43,479
On considère que c’est notre
corps mais simplement on n’en connaît pas

57
00:05:43,479 --> 00:05:51,970
la substance divine,
spirituelle, c’est un non-sens,

58
00:05:51,970 --> 00:05:59,420
on se comporte avec la planète
comme avec notre corps.

59
00:05:59,760 --> 00:06:07,680
Par zazen on peut très exactement le comprendre et si on lâche des quatre éléments

60
00:06:08,080 --> 00:06:14,340
c’est la Terre du Bouddha, la paix,le satori, qui se réalisent

61
00:06:14,780 --> 00:06:22,280
dans un fonctionnement simple, harmonieux, qui ne blesse pas, qui ne tue pas la vie

62
00:06:22,460 --> 00:06:27,920
la graine de Bouddha, la possibilité
de s’élever au-dessus.

63
00:06:28,260 --> 00:06:35,020
Ce qui m’intéresse là, c’est que maintenant les scientifiques eux mêmes

64
00:06:35,700 --> 00:06:44,060
commencent à découvrir ce que nous,
nous percevons et pratiquons naturellement.

65
00:06:47,560 --> 00:06:55,560
Les scientifiques expliquent également
qu’on est fait nous-mêmes de ces quatre éléments

66
00:06:55,900 --> 00:07:02,920
mais également que nous sommes un corps vivant dans lequel

67
00:07:03,400 --> 00:07:09,379
il y a plus de bactéries, de virus, 
de microbes

68
00:07:09,379 --> 00:07:18,439
on a plus, je crois, de virus que même
de cellules vivantes,

69
00:07:18,439 --> 00:07:23,649
ça fonctionne parce qu’il y a un équilibre, une protection.

70
00:07:23,649 --> 00:07:29,019
Bien évidemment, quand il n’y a plus d’équilibre, plus de protection...

71
00:07:29,019 --> 00:07:35,959
heureusement on a pu rencontrer,
connaître le zazen, le pratiquer,

72
00:07:35,959 --> 00:07:42,979
le faire connaître. C’est très précieux, pas seulement pour nous mais pour toutes les existences.
