1
00:00:04,240 --> 00:00:07,040
Qu’est-ce que le Zen? 

2
00:00:07,320 --> 00:00:10,120
Le zen, c’est se comprendre soi-même. 

3
00:00:10,400 --> 00:00:11,800
Votre propre esprit. 

4
00:00:12,080 --> 00:00:14,140
C’est l’affaire de tous. 

5
00:00:14,420 --> 00:00:16,500
C’est ce que Deshimaru voulait diffuser. 

6
00:00:16,800 --> 00:00:19,460
C’est ce que je veux transmettre et réaliser. 

7
00:00:19,720 --> 00:00:22,460
J’avais vu un disciple de Kodo Sawaki. 

8
00:00:22,840 --> 00:00:24,720
Il m’a dit: 

9
00:00:25,120 --> 00:00:27,620
"Vous devez vous regarder!" 

10
00:00:27,960 --> 00:00:29,960
Il avait raison! 

11
00:00:30,380 --> 00:00:33,300
Que signifie un chemin spirituel? 

12
00:00:33,640 --> 00:00:41,880
Le sens d’un chemin spirituel est «de revenir au rêveur». 

13
00:00:46,020 --> 00:00:48,260
Dogen dit: 

14
00:00:48,520 --> 00:00:51,260
"Étudier le bouddhisme, c’est vous étudier." 

15
00:00:51,540 --> 00:00:53,980
Étudiez le rêveur. 

16
00:00:54,380 --> 00:00:57,300
Pourquoi rêve-t-il ainsi? 

17
00:00:58,260 --> 00:01:00,240
Comment peut-il changer son rêve? 

18
00:01:00,540 --> 00:01:02,020
Comment peut-il contrôler son rêve? 

19
00:01:02,840 --> 00:01:04,000
Quand le Bouddha dit: 

20
00:01:04,440 --> 00:01:06,300
"Tout est un rêve." 

21
00:01:06,620 --> 00:01:09,820
Cela ne veut pas dire "Rien n’a d’importance". 

22
00:01:10,100 --> 00:01:13,580
Ce n’est pas le rêve qui est important, c’est le rêveur. 

23
00:01:13,900 --> 00:01:16,540
Arrêtez de rêver, c’est ce que nous appelons "l’éveil" … 

24
00:01:17,180 --> 00:01:21,040
L’éveil devient un rêveur. 

25
00:01:21,340 --> 00:01:22,980
Contrôler votre rêve. 

26
00:01:24,000 --> 00:01:26,120
Voilà le chemin spirituel. 

27
00:01:26,420 --> 00:01:27,980
C’est très difficile. 

28
00:01:28,560 --> 00:01:31,760
Comment puis-je commencer? 

29
00:01:32,660 --> 00:01:45,080
La motivation principale pour pratiquer un chemin spirituel est: être capable de contrôler son rêve. 

30
00:01:46,700 --> 00:01:49,340
Qu’est-ce qui est important dans le Zen? 

31
00:01:49,920 --> 00:01:55,880
De cette façon, c’est votre esprit qui est important. 

32
00:01:56,280 --> 00:02:01,280
Zazen n’est pas un objet d’adoration. 

33
00:02:01,540 --> 00:02:03,980
Le deuxième point important de son enseignement: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku. 

35
00:02:07,180 --> 00:02:10,780
Ce qui signifie "à but non lucratif". 

36
00:02:11,340 --> 00:02:23,340
C’est la porte qui nous conduit de l’esprit de Dieu au monde matériel. 

37
00:02:23,640 --> 00:02:27,000
C’est très important. 

38
00:02:27,360 --> 00:02:37,420
Même dans le monde quotidien, même dans les phénomènes, vous ne perdez pas votre concentration. 

39
00:02:37,800 --> 00:02:42,920
Vous ne perdez pas votre vraie nature. 

40
00:02:43,180 --> 00:02:45,100
C’est Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
Mais qu’est-ce qui est facile? 

42
00:02:55,780 --> 00:03:00,260
Les gens prennent toujours la voie facile. 

43
00:03:00,620 --> 00:03:03,780
Nous tombons dans les choses faciles. 

44
00:03:10,640 --> 00:03:13,600
Le pouvoir est facile. 

45
00:03:14,060 --> 00:03:19,220
C’est facile d’être avec des gens qui ressentent toujours la même chose. 

46
00:03:21,160 --> 00:03:26,400
Faire une église en zen est facile. 

47
00:03:26,800 --> 00:03:32,080
S’attacher à une église est facile. 

48
00:03:32,360 --> 00:03:37,300
Les gens vont dans quelque chose de sûr. 

49
00:03:37,600 --> 00:03:41,060
Sans aucun effort. 

50
00:03:41,420 --> 00:03:44,620
Où ils ne sont jamais vraiment confrontés à eux-mêmes. 

51
00:03:44,920 --> 00:03:47,080
Où ils ne sont jamais seuls. 

52
00:03:47,500 --> 00:03:52,040
Comment voyez-vous vos anciens camarades? 

53
00:03:52,360 --> 00:03:58,720
J’admire beaucoup de disciples de Maître Deshimaru. 

54
00:03:58,980 --> 00:04:07,980
Qui a reçu son éducation et continue de pratiquer seul. 

55
00:04:08,240 --> 00:04:14,320
Il vaut mieux pratiquer ensemble. 

56
00:04:14,700 --> 00:04:19,140
Mais au moins, ils font leurs propres affaires. 

57
00:04:19,660 --> 00:04:23,980
Ils sont seuls. 

