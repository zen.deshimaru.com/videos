1
00:00:00,560 --> 00:00:03,060

no no no

2
00:00:03,060 --> 00:00:03,070
no no no
 

3
00:00:03,070 --> 00:00:14,040
no no no
veremos

4
00:00:14,040 --> 00:00:14,050

 

5
00:00:14,050 --> 00:00:15,990

[Música]

6
00:00:15,990 --> 00:00:16,000
[Música]
 

7
00:00:16,000 --> 00:00:18,700
[Música]
no

8
00:00:18,700 --> 00:00:18,710

 

9
00:00:18,710 --> 00:00:25,250

[Música]

10
00:00:25,250 --> 00:00:25,260

 

11
00:00:25,260 --> 00:00:29,939

no agravarla

12
00:00:29,939 --> 00:00:29,949

 

13
00:00:29,949 --> 00:00:37,450

yo soy yo

14
00:00:37,450 --> 00:00:37,460

 

15
00:00:37,460 --> 00:00:38,310

yo

16
00:00:38,310 --> 00:00:38,320
yo
 

17
00:00:38,320 --> 00:00:41,410
yo
[Música]

18
00:00:41,410 --> 00:00:41,420
[Música]
 

19
00:00:41,420 --> 00:00:45,400
[Música]
d

20
00:00:45,400 --> 00:00:45,410

 

21
00:00:45,410 --> 00:00:48,980

yo sí

22
00:00:48,980 --> 00:00:48,990
yo sí
 

23
00:00:48,990 --> 00:00:51,100
yo sí
no

24
00:00:51,100 --> 00:00:51,110
no
 

25
00:00:51,110 --> 00:00:52,740
no
[Música]

26
00:00:52,740 --> 00:00:52,750
[Música]
 

27
00:00:52,750 --> 00:00:56,530
[Música]
de mí

28
00:00:56,530 --> 00:00:56,540
de mí
 

29
00:00:56,540 --> 00:01:04,210
de mí
[Música]

30
00:01:04,210 --> 00:01:04,220
[Música]
 

31
00:01:04,220 --> 00:01:05,290
[Música]
no

32
00:01:05,290 --> 00:01:05,300
no
 

33
00:01:05,300 --> 00:01:12,820
no
[Música]

34
00:01:12,820 --> 00:01:12,830

 

35
00:01:12,830 --> 00:01:19,480

yo era como un volcán en ebullición

36
00:01:19,480 --> 00:01:19,490

 

37
00:01:19,490 --> 00:01:24,130

con mucha fuerza pero muy desorbitada

38
00:01:24,130 --> 00:01:24,140
con mucha fuerza pero muy desorbitada
 

39
00:01:24,140 --> 00:01:27,100
con mucha fuerza pero muy desorbitada
como la fuerza muy salida de como

40
00:01:27,100 --> 00:01:27,110
como la fuerza muy salida de como
 

41
00:01:27,110 --> 00:01:30,130
como la fuerza muy salida de como
ponerme un pantalón estrecho y que se

42
00:01:30,130 --> 00:01:30,140
ponerme un pantalón estrecho y que se
 

43
00:01:30,140 --> 00:01:32,190
ponerme un pantalón estrecho y que se
revientan las costuras

44
00:01:32,190 --> 00:01:32,200
revientan las costuras
 

45
00:01:32,200 --> 00:01:34,889
revientan las costuras
todas las culturas reventándose por la

46
00:01:34,889 --> 00:01:34,899
todas las culturas reventándose por la
 

47
00:01:34,899 --> 00:01:37,740
todas las culturas reventándose por la
fuerza porque no cabe entonces será como

48
00:01:37,740 --> 00:01:37,750
fuerza porque no cabe entonces será como
 

49
00:01:37,750 --> 00:01:41,140
fuerza porque no cabe entonces será como
mi fuerza

50
00:01:41,140 --> 00:01:41,150

 

51
00:01:41,150 --> 00:01:45,469

desencajada desorientada sin guía en

52
00:01:45,469 --> 00:01:45,479
desencajada desorientada sin guía en
 

53
00:01:45,479 --> 00:01:48,980
desencajada desorientada sin guía en
búsqueda y la transformación es que

54
00:01:48,980 --> 00:01:48,990
búsqueda y la transformación es que
 

55
00:01:48,990 --> 00:01:51,230
búsqueda y la transformación es que
encontré el camino

56
00:01:51,230 --> 00:01:51,240
encontré el camino
 

57
00:01:51,240 --> 00:01:54,020
encontré el camino
y ha revolucionado toda mi todo mi mundo

58
00:01:54,020 --> 00:01:54,030
y ha revolucionado toda mi todo mi mundo
 

59
00:01:54,030 --> 00:01:55,840
y ha revolucionado toda mi todo mi mundo
interior

60
00:01:55,840 --> 00:01:55,850
interior
 

61
00:01:55,850 --> 00:01:59,590
interior
me ha hecho más serena y más intensa la

62
00:01:59,590 --> 00:01:59,600
me ha hecho más serena y más intensa la
 

63
00:01:59,600 --> 00:02:01,710
me ha hecho más serena y más intensa la
fuerza se ha transformado en intensidad

64
00:02:01,710 --> 00:02:01,720
fuerza se ha transformado en intensidad
 

65
00:02:01,720 --> 00:02:06,980
fuerza se ha transformado en intensidad
interior

66
00:02:06,980 --> 00:02:06,990

 

67
00:02:06,990 --> 00:02:10,520

son cambios también muy muy minúsculos

68
00:02:10,520 --> 00:02:10,530
son cambios también muy muy minúsculos
 

69
00:02:10,530 --> 00:02:13,280
son cambios también muy muy minúsculos
que uno siente al interior de uno mismo

70
00:02:13,280 --> 00:02:13,290
que uno siente al interior de uno mismo
 

71
00:02:13,290 --> 00:02:16,850
que uno siente al interior de uno mismo
y es muy difícil decir que se siente en

72
00:02:16,850 --> 00:02:16,860
y es muy difícil decir que se siente en
 

73
00:02:16,860 --> 00:02:18,700
y es muy difícil decir que se siente en
un orgasmo

74
00:02:18,700 --> 00:02:18,710
un orgasmo
 

75
00:02:18,710 --> 00:02:22,110
un orgasmo
para mí también una cosa muy importante

76
00:02:22,110 --> 00:02:22,120
para mí también una cosa muy importante
 

77
00:02:22,120 --> 00:02:26,200
para mí también una cosa muy importante
es en esta vida lo veo como algo que

78
00:02:26,200 --> 00:02:26,210
es en esta vida lo veo como algo que
 

79
00:02:26,210 --> 00:02:32,660
es en esta vida lo veo como algo que
debo cumplir tanto posible por lo menos

80
00:02:32,660 --> 00:02:32,670

 

81
00:02:32,670 --> 00:02:35,809

es descubrir a sí mismo estar más cerca

82
00:02:35,809 --> 00:02:35,819
es descubrir a sí mismo estar más cerca
 

83
00:02:35,819 --> 00:02:38,930
es descubrir a sí mismo estar más cerca
a sí mismo y estar a gusto con algo

84
00:02:38,930 --> 00:02:38,940
a sí mismo y estar a gusto con algo
 

85
00:02:38,940 --> 00:02:41,690
a sí mismo y estar a gusto con algo
tranquilo consigo mismo creo que eso es

86
00:02:41,690 --> 00:02:41,700
tranquilo consigo mismo creo que eso es
 

87
00:02:41,700 --> 00:02:46,240
tranquilo consigo mismo creo que eso es
como el sentido de la existencia porque

88
00:02:46,240 --> 00:02:46,250
como el sentido de la existencia porque
 

89
00:02:46,250 --> 00:02:52,590
como el sentido de la existencia porque
cuando una persona se conoce a sí mismo

90
00:02:52,590 --> 00:02:52,600

 

91
00:02:52,600 --> 00:02:56,610

está en todo y fuera de todo y muy

92
00:02:56,610 --> 00:02:56,620
está en todo y fuera de todo y muy
 

93
00:02:56,620 --> 00:03:00,060
está en todo y fuera de todo y muy
centrada muy centrada porque puede

94
00:03:00,060 --> 00:03:00,070
centrada muy centrada porque puede
 

95
00:03:00,070 --> 00:03:03,140
centrada muy centrada porque puede
controlar muy fácil emociones

96
00:03:03,140 --> 00:03:03,150
controlar muy fácil emociones
 

97
00:03:03,150 --> 00:03:06,830
controlar muy fácil emociones
tribulaciones abandonarse al placer

98
00:03:06,830 --> 00:03:06,840
tribulaciones abandonarse al placer
 

99
00:03:06,840 --> 00:03:09,460
tribulaciones abandonarse al placer
abandonarse as hacen

100
00:03:09,460 --> 00:03:09,470
abandonarse as hacen
 

101
00:03:09,470 --> 00:03:13,360
abandonarse as hacen
sí pero eso es un proceso que te puede

102
00:03:13,360 --> 00:03:13,370
sí pero eso es un proceso que te puede
 

103
00:03:13,370 --> 00:03:15,070
sí pero eso es un proceso que te puede
llevar la que te lleva la vida entera

104
00:03:15,070 --> 00:03:15,080
llevar la que te lleva la vida entera
 

105
00:03:15,080 --> 00:03:18,290
llevar la que te lleva la vida entera
espera

106
00:03:18,290 --> 00:03:18,300

 

107
00:03:18,300 --> 00:03:21,200

es amarse a sí mismo es tener mucho

108
00:03:21,200 --> 00:03:21,210
es amarse a sí mismo es tener mucho
 

109
00:03:21,210 --> 00:03:23,120
es amarse a sí mismo es tener mucho
gusto con uno

110
00:03:23,120 --> 00:03:23,130
gusto con uno
 

111
00:03:23,130 --> 00:03:25,550
gusto con uno
y si uno tiene ese conocimiento de uno

112
00:03:25,550 --> 00:03:25,560
y si uno tiene ese conocimiento de uno
 

113
00:03:25,560 --> 00:03:28,610
y si uno tiene ese conocimiento de uno
mismo profundiza en el conocimiento de

114
00:03:28,610 --> 00:03:28,620
mismo profundiza en el conocimiento de
 

115
00:03:28,620 --> 00:03:30,740
mismo profundiza en el conocimiento de
los demás también

116
00:03:30,740 --> 00:03:30,750
los demás también
 

117
00:03:30,750 --> 00:03:32,810
los demás también
porque no tiene que relacionarse así

118
00:03:32,810 --> 00:03:32,820
porque no tiene que relacionarse así
 

119
00:03:32,820 --> 00:03:35,060
porque no tiene que relacionarse así
como muy socialmente que digamos sino en

120
00:03:35,060 --> 00:03:35,070
como muy socialmente que digamos sino en
 

121
00:03:35,070 --> 00:03:38,760
como muy socialmente que digamos sino en
otro nivel un nivel más

122
00:03:38,760 --> 00:03:38,770
otro nivel un nivel más
 

123
00:03:38,770 --> 00:03:42,520
otro nivel un nivel más
más cósmico más universal mejor

124
00:03:42,520 --> 00:03:42,530
más cósmico más universal mejor
 

125
00:03:42,530 --> 00:03:45,910
más cósmico más universal mejor
como usted del platiqué que sus del

126
00:03:45,910 --> 00:03:45,920
como usted del platiqué que sus del
 

127
00:03:45,920 --> 00:03:49,199
como usted del platiqué que sus del
tráfago sanción corte john canavan

128
00:03:49,199 --> 00:03:49,209
tráfago sanción corte john canavan
 

129
00:03:49,209 --> 00:04:05,350
tráfago sanción corte john canavan
denuncia

130
00:04:05,350 --> 00:04:05,360

 

131
00:04:05,360 --> 00:04:08,470

y aquel que pensaba como para llamarla

132
00:04:08,470 --> 00:04:08,480
y aquel que pensaba como para llamarla
 

133
00:04:08,480 --> 00:04:12,910
y aquel que pensaba como para llamarla
de duelo igualado donde todo se

134
00:04:12,910 --> 00:04:12,920
de duelo igualado donde todo se
 

135
00:04:12,920 --> 00:04:15,610
de duelo igualado donde todo se
cuestiona de su vida no señor

136
00:04:15,610 --> 00:04:15,620
cuestiona de su vida no señor
 

137
00:04:15,620 --> 00:04:17,800
cuestiona de su vida no señor
la autopsia blemente al menos un civil

138
00:04:17,800 --> 00:04:17,810
la autopsia blemente al menos un civil
 

139
00:04:17,810 --> 00:04:28,170
la autopsia blemente al menos un civil
de los medios agua

140
00:04:28,170 --> 00:04:28,180

 

141
00:04:28,180 --> 00:04:32,730

y como son

142
00:04:32,730 --> 00:04:32,740
y como son
 

143
00:04:32,740 --> 00:04:35,290
y como son
al instituto buscada

144
00:04:35,290 --> 00:04:35,300
al instituto buscada
 

145
00:04:35,300 --> 00:04:38,159
al instituto buscada
lipton se dispara

146
00:04:38,159 --> 00:04:38,169
lipton se dispara
 

147
00:04:38,169 --> 00:04:42,709
lipton se dispara
te congelas al espejo es eliminación al

148
00:04:42,709 --> 00:04:42,719
te congelas al espejo es eliminación al
 

149
00:04:42,719 --> 00:04:45,749
te congelas al espejo es eliminación al
reclamo de la mente esa eliminación es

150
00:04:45,749 --> 00:04:45,759
reclamo de la mente esa eliminación es
 

151
00:04:45,759 --> 00:04:51,730
reclamo de la mente esa eliminación es
el equipo exterior

152
00:04:51,730 --> 00:04:51,740

 

153
00:04:51,740 --> 00:05:01,649

x

154
00:05:01,649 --> 00:05:01,659

 

155
00:05:01,659 --> 00:05:04,010

otros

156
00:05:04,010 --> 00:05:04,020
otros
 

157
00:05:04,020 --> 00:05:10,510
otros
con juan para que el tour

158
00:05:10,510 --> 00:05:10,520

 

159
00:05:10,520 --> 00:05:15,710

se pulsa lesiones con desilusión

160
00:05:15,710 --> 00:05:15,720

 

161
00:05:15,720 --> 00:05:17,680

las columnas y la buena explicación

162
00:05:17,680 --> 00:05:17,690
las columnas y la buena explicación
 

163
00:05:17,690 --> 00:05:20,510
las columnas y la buena explicación
nacional de lectura lección soul san

164
00:05:20,510 --> 00:05:20,520
nacional de lectura lección soul san
 

165
00:05:20,520 --> 00:05:24,230
nacional de lectura lección soul san
luis de todo

166
00:05:24,230 --> 00:05:24,240

 

167
00:05:24,240 --> 00:05:28,080

y además

168
00:05:28,080 --> 00:05:28,090
y además
 

169
00:05:28,090 --> 00:05:34,089
y además
los ojos una explicación

170
00:05:34,089 --> 00:05:34,099

 

171
00:05:34,099 --> 00:05:40,750

necesite

172
00:05:40,750 --> 00:05:40,760

 

173
00:05:40,760 --> 00:05:46,679

la explicación

174
00:05:46,679 --> 00:05:46,689
la explicación
 

175
00:05:46,689 --> 00:05:49,510
la explicación
al abastecerse

176
00:05:49,510 --> 00:05:49,520

 

177
00:05:49,520 --> 00:05:52,470

y si el domingo

178
00:05:52,470 --> 00:05:52,480
y si el domingo
 

179
00:05:52,480 --> 00:05:56,910
y si el domingo
escena de transmisión oral preescolar

180
00:05:56,910 --> 00:05:56,920
escena de transmisión oral preescolar
 

181
00:05:56,920 --> 00:06:02,130
escena de transmisión oral preescolar
sin transmisión del mal de la vida llama

182
00:06:02,130 --> 00:06:02,140
sin transmisión del mal de la vida llama
 

183
00:06:02,140 --> 00:06:07,629
sin transmisión del mal de la vida llama
a ama

184
00:06:07,629 --> 00:06:07,639

 

185
00:06:07,639 --> 00:06:15,350

ha sido del mundo en donde la

186
00:06:15,350 --> 00:06:15,360
ha sido del mundo en donde la
 

187
00:06:15,360 --> 00:06:20,060
ha sido del mundo en donde la
transmisión tradicional oficial de esta

188
00:06:20,060 --> 00:06:20,070
transmisión tradicional oficial de esta
 

189
00:06:20,070 --> 00:06:23,810
transmisión tradicional oficial de esta
edición a los juegos de mesa a comer

190
00:06:23,810 --> 00:06:23,820
edición a los juegos de mesa a comer
 

191
00:06:23,820 --> 00:06:29,860
edición a los juegos de mesa a comer
mejor es en sí el pacto sensible

192
00:06:29,860 --> 00:06:29,870

 

193
00:06:29,870 --> 00:06:33,210

a tener a autopsia

194
00:06:33,210 --> 00:06:33,220
a tener a autopsia
 

195
00:06:33,220 --> 00:06:37,680
a tener a autopsia
cerca de birmingham y se desplazan con

196
00:06:37,680 --> 00:06:37,690
cerca de birmingham y se desplazan con
 

197
00:06:37,690 --> 00:06:41,270
cerca de birmingham y se desplazan con
franz grupp donde la transmisión

198
00:06:41,270 --> 00:06:41,280
franz grupp donde la transmisión
 

199
00:06:41,280 --> 00:06:46,320
franz grupp donde la transmisión
actualísimo 2 y reparte 1 décima

200
00:06:46,320 --> 00:06:46,330
actualísimo 2 y reparte 1 décima
 

201
00:06:46,330 --> 00:06:51,240
actualísimo 2 y reparte 1 décima
celebré es certifique la visita de la

202
00:06:51,240 --> 00:06:51,250
celebré es certifique la visita de la
 

203
00:06:51,250 --> 00:06:54,970
celebré es certifique la visita de la
misión medicina

204
00:06:54,970 --> 00:06:54,980

 

205
00:06:54,980 --> 00:06:58,520

si fuera poco

206
00:06:58,520 --> 00:06:58,530
si fuera poco
 

207
00:06:58,530 --> 00:07:00,970
si fuera poco
sonia

208
00:07:00,970 --> 00:07:00,980
sonia
 

209
00:07:00,980 --> 00:07:17,020
sonia
como tal hay que como se va a la gana

210
00:07:17,020 --> 00:07:17,030

 

211
00:07:17,030 --> 00:07:23,970

digamos que está un poco en popa

212
00:07:23,970 --> 00:07:23,980
digamos que está un poco en popa
 

213
00:07:23,980 --> 00:07:26,540
digamos que está un poco en popa
puro

214
00:07:26,540 --> 00:07:26,550
puro
 

215
00:07:26,550 --> 00:07:37,260
puro
luján como el famoso mismo

216
00:07:37,260 --> 00:07:37,270

 

217
00:07:37,270 --> 00:07:43,959

o no

218
00:07:43,959 --> 00:07:43,969

 

219
00:07:43,969 --> 00:07:55,200

kingdom músculo zona

220
00:07:55,200 --> 00:07:55,210

 

221
00:07:55,210 --> 00:08:02,879

está tranquila

222
00:08:02,879 --> 00:08:02,889

 

223
00:08:02,889 --> 00:08:06,739

se posesiona vamos andando a la práctica

224
00:08:06,739 --> 00:08:06,749
se posesiona vamos andando a la práctica
 

225
00:08:06,749 --> 00:08:08,670
se posesiona vamos andando a la práctica
hablando

226
00:08:08,670 --> 00:08:08,680
hablando
 

227
00:08:08,680 --> 00:08:14,230
hablando
[Risas]

228
00:08:14,230 --> 00:08:14,240

 

229
00:08:14,240 --> 00:08:16,720

no

230
00:08:16,720 --> 00:08:16,730
no
 

231
00:08:16,730 --> 00:08:28,010
no
paren

232
00:08:28,010 --> 00:08:28,020

 

233
00:08:28,020 --> 00:08:51,730

de tiempo completo como ese

234
00:08:51,730 --> 00:08:51,740

 

235
00:08:51,740 --> 00:09:04,120

[Música]

236
00:09:04,120 --> 00:09:04,130

 

237
00:09:04,130 --> 00:09:12,510

colombi

238
00:09:12,510 --> 00:09:12,520

 

239
00:09:12,520 --> 00:09:17,990

no

240
00:09:17,990 --> 00:09:18,000

 

241
00:09:18,000 --> 00:09:22,220

[Música]

242
00:09:22,220 --> 00:09:22,230

 

243
00:09:22,230 --> 00:09:25,250

[Aplausos]

244
00:09:25,250 --> 00:09:25,260
[Aplausos]
 

245
00:09:25,260 --> 00:09:28,910
[Aplausos]
de veras

246
00:09:28,910 --> 00:09:28,920
de veras
 

247
00:09:28,920 --> 00:09:45,230
de veras
[Música]

248
00:09:45,230 --> 00:09:45,240

 

249
00:09:45,240 --> 00:10:36,060

[Música]

250
00:10:36,060 --> 00:10:36,070

 

251
00:10:36,070 --> 00:10:40,810

[Música]

252
00:10:40,810 --> 00:10:40,820
[Música]
 

253
00:10:40,820 --> 00:10:42,780
[Música]
[Aplausos]

254
00:10:42,780 --> 00:10:42,790
[Aplausos]
 

255
00:10:42,790 --> 00:11:00,450
[Aplausos]
[Música]

256
00:11:00,450 --> 00:11:00,460

 

257
00:11:00,460 --> 00:11:04,440

la gente piensan que una colisión es

258
00:11:04,440 --> 00:11:04,450
la gente piensan que una colisión es
 

259
00:11:04,450 --> 00:11:09,020
la gente piensan que una colisión es
algo al cual hay que

260
00:11:09,020 --> 00:11:09,030

 

261
00:11:09,030 --> 00:11:13,710

adherir hay que creer y que el factor

262
00:11:13,710 --> 00:11:13,720
adherir hay que creer y que el factor
 

263
00:11:13,720 --> 00:11:19,610
adherir hay que creer y que el factor
que hay que cavar es una cosa objetiva

264
00:11:19,610 --> 00:11:19,620

 

265
00:11:19,620 --> 00:11:24,960

nosotros no vemos las cosas así pensemos

266
00:11:24,960 --> 00:11:24,970
nosotros no vemos las cosas así pensemos
 

267
00:11:24,970 --> 00:11:28,200
nosotros no vemos las cosas así pensemos
que las colisiones son creación del

268
00:11:28,200 --> 00:11:28,210
que las colisiones son creación del
 

269
00:11:28,210 --> 00:11:31,410
que las colisiones son creación del
hombre tiraba del avión el nombre es

270
00:11:31,410 --> 00:11:31,420
hombre tiraba del avión el nombre es
 

271
00:11:31,420 --> 00:11:34,590
hombre tiraba del avión el nombre es
creativa debe estar activa es decir que

272
00:11:34,590 --> 00:11:34,600
creativa debe estar activa es decir que
 

273
00:11:34,600 --> 00:11:37,129
creativa debe estar activa es decir que
nosotros somos dios

274
00:11:37,129 --> 00:11:37,139
nosotros somos dios
 

275
00:11:37,139 --> 00:11:41,049
nosotros somos dios
y es verdad porque el hombre es cada

276
00:11:41,049 --> 00:11:41,059
y es verdad porque el hombre es cada
 

277
00:11:41,059 --> 00:11:52,930
y es verdad porque el hombre es cada
lección de su calidad

278
00:11:52,930 --> 00:11:52,940

 

279
00:11:52,940 --> 00:11:57,960

me enseñaron democracia casi libertad

280
00:11:57,960 --> 00:11:57,970
me enseñaron democracia casi libertad
 

281
00:11:57,970 --> 00:12:04,600
me enseñaron democracia casi libertad
sin embargo esa no era para mí y no en

282
00:12:04,600 --> 00:12:04,610
sin embargo esa no era para mí y no en
 

283
00:12:04,610 --> 00:12:14,879
sin embargo esa no era para mí y no en
esta clínica y me siento feliz feliz

284
00:12:14,879 --> 00:12:14,889

 

285
00:12:14,889 --> 00:12:18,820

electrochoques acá

286
00:12:18,820 --> 00:12:18,830

 

287
00:12:18,830 --> 00:12:21,800

datos que ya escribe

288
00:12:21,800 --> 00:12:21,810
datos que ya escribe
 

289
00:12:21,810 --> 00:12:29,800
datos que ya escribe
el dinero no es nada para mí

290
00:12:29,800 --> 00:12:29,810

 

291
00:12:29,810 --> 00:12:31,960

gracias a la música puede conocer a mi

292
00:12:31,960 --> 00:12:31,970
gracias a la música puede conocer a mi
 

293
00:12:31,970 --> 00:12:34,030
gracias a la música puede conocer a mi
maestro una vez y me tocó ir con mi

294
00:12:34,030 --> 00:12:34,040
maestro una vez y me tocó ir con mi
 

295
00:12:34,040 --> 00:12:37,930
maestro una vez y me tocó ir con mi
banda ayudar a antes a tocar a un

296
00:12:37,930 --> 00:12:37,940
banda ayudar a antes a tocar a un
 

297
00:12:37,940 --> 00:12:39,910
banda ayudar a antes a tocar a un
festival adelante que se llama de salud

298
00:12:39,910 --> 00:12:39,920
festival adelante que se llama de salud
 

299
00:12:39,920 --> 00:12:41,290
festival adelante que se llama de salud
men

300
00:12:41,290 --> 00:12:41,300
men
 

301
00:12:41,300 --> 00:12:44,259
men
y bueno hicimos dos shows con lo que nos

302
00:12:44,259 --> 00:12:44,269
y bueno hicimos dos shows con lo que nos
 

303
00:12:44,269 --> 00:12:47,410
y bueno hicimos dos shows con lo que nos
pagan nos quedó dinero como para dejar

304
00:12:47,410 --> 00:12:47,420
pagan nos quedó dinero como para dejar
 

305
00:12:47,420 --> 00:12:50,380
pagan nos quedó dinero como para dejar
tres semanas por europa y ahí bueno fui

306
00:12:50,380 --> 00:12:50,390
tres semanas por europa y ahí bueno fui
 

307
00:12:50,390 --> 00:12:52,870
tres semanas por europa y ahí bueno fui
a parís y en parís quise conocer al

308
00:12:52,870 --> 00:12:52,880
a parís y en parís quise conocer al
 

309
00:12:52,880 --> 00:12:55,750
a parís y en parís quise conocer al
maestro de llamaron y cuando fui al dow

310
00:12:55,750 --> 00:12:55,760
maestro de llamaron y cuando fui al dow
 

311
00:12:55,760 --> 00:12:57,610
maestro de llamaron y cuando fui al dow
le demuestro decimal un me enteré que ya

312
00:12:57,610 --> 00:12:57,620
le demuestro decimal un me enteré que ya
 

313
00:12:57,620 --> 00:13:00,280
le demuestro decimal un me enteré que ya
se había muerto pero había un argentino

314
00:13:00,280 --> 00:13:00,290
se había muerto pero había un argentino
 

315
00:13:00,290 --> 00:13:03,100
se había muerto pero había un argentino
que era fernando summers y que me dice

316
00:13:03,100 --> 00:13:03,110
que era fernando summers y que me dice
 

317
00:13:03,110 --> 00:13:05,160
que era fernando summers y que me dice
que el heredero está en en red

318
00:13:05,160 --> 00:13:05,170
que el heredero está en en red
 

319
00:13:05,170 --> 00:13:08,949
que el heredero está en en red
entonces me tomé el teje y fui a red y

320
00:13:08,949 --> 00:13:08,959
entonces me tomé el teje y fui a red y
 

321
00:13:08,959 --> 00:13:11,889
entonces me tomé el teje y fui a red y
estuve una semana practicando en el

322
00:13:11,889 --> 00:13:11,899
estuve una semana practicando en el
 

323
00:13:11,899 --> 00:13:15,040
estuve una semana practicando en el
dueño de la red conociendo a mi maestro

324
00:13:15,040 --> 00:13:15,050
dueño de la red conociendo a mi maestro
 

325
00:13:15,050 --> 00:13:17,620
dueño de la red conociendo a mi maestro
yo creo que el maestro jose en eso

326
00:13:17,620 --> 00:13:17,630
yo creo que el maestro jose en eso
 

327
00:13:17,630 --> 00:13:20,920
yo creo que el maestro jose en eso
patriarca que trae el zen de la europa a

328
00:13:20,920 --> 00:13:20,930
patriarca que trae el zen de la europa a
 

329
00:13:20,930 --> 00:13:24,579
patriarca que trae el zen de la europa a
la argentina de seguros y porque todos

330
00:13:24,579 --> 00:13:24,589
la argentina de seguros y porque todos
 

331
00:13:24,589 --> 00:13:25,870
la argentina de seguros y porque todos
los maestros que tienen templo son

332
00:13:25,870 --> 00:13:25,880
los maestros que tienen templo son
 

333
00:13:25,880 --> 00:13:28,810
los maestros que tienen templo son
lógicas y que son rossi y no sé por otro

334
00:13:28,810 --> 00:13:28,820
lógicas y que son rossi y no sé por otro
 

335
00:13:28,820 --> 00:13:33,120
lógicas y que son rossi y no sé por otro
lado en general la gente que practica

336
00:13:33,120 --> 00:13:33,130
lado en general la gente que practica
 

337
00:13:33,130 --> 00:13:36,699
lado en general la gente que practica
con mi maestro es gente que tiene que

338
00:13:36,699 --> 00:13:36,709
con mi maestro es gente que tiene que
 

339
00:13:36,709 --> 00:13:39,900
con mi maestro es gente que tiene que
ver con la creatividad en muchos actores

340
00:13:39,900 --> 00:13:39,910
ver con la creatividad en muchos actores
 

341
00:13:39,910 --> 00:13:45,009
ver con la creatividad en muchos actores
músicos escritores pintores yo no sé por

342
00:13:45,009 --> 00:13:45,019
músicos escritores pintores yo no sé por
 

343
00:13:45,019 --> 00:13:47,920
músicos escritores pintores yo no sé por
qué pero creo que hay no hay algo alguna

344
00:13:47,920 --> 00:13:47,930
qué pero creo que hay no hay algo alguna
 

345
00:13:47,930 --> 00:13:50,350
qué pero creo que hay no hay algo alguna
ligazón entre la creatividad y los

346
00:13:50,350 --> 00:13:50,360
ligazón entre la creatividad y los
 

347
00:13:50,360 --> 00:14:01,550
ligazón entre la creatividad y los
artistas

348
00:14:01,550 --> 00:14:01,560

 

349
00:14:01,560 --> 00:14:21,240

[Aplausos]

350
00:14:21,240 --> 00:14:21,250

 

351
00:14:21,250 --> 00:14:24,990

el sen es como universal abarca todo

352
00:14:24,990 --> 00:14:25,000
el sen es como universal abarca todo
 

353
00:14:25,000 --> 00:14:27,420
el sen es como universal abarca todo
todos los aspectos de la vida es como ir

354
00:14:27,420 --> 00:14:27,430
todos los aspectos de la vida es como ir
 

355
00:14:27,430 --> 00:14:31,560
todos los aspectos de la vida es como ir
entrando e ir abriendo a la vida misma a

356
00:14:31,560 --> 00:14:31,570
entrando e ir abriendo a la vida misma a
 

357
00:14:31,570 --> 00:14:34,260
entrando e ir abriendo a la vida misma a
mí realmente me abre mucho a todos

358
00:14:34,260 --> 00:14:34,270
mí realmente me abre mucho a todos
 

359
00:14:34,270 --> 00:14:36,600
mí realmente me abre mucho a todos
encuentro todo ahí

360
00:14:36,600 --> 00:14:36,610
encuentro todo ahí
 

361
00:14:36,610 --> 00:14:40,850
encuentro todo ahí
y uno va aprendiendo de no sé

362
00:14:40,850 --> 00:14:40,860

 

363
00:14:40,860 --> 00:14:43,760

se va conociendo mucho por un lado no

364
00:14:43,760 --> 00:14:43,770
se va conociendo mucho por un lado no
 

365
00:14:43,770 --> 00:14:45,380
se va conociendo mucho por un lado no
soy pero por el otro lado sí me

366
00:14:45,380 --> 00:14:45,390
soy pero por el otro lado sí me
 

367
00:14:45,390 --> 00:14:49,160
soy pero por el otro lado sí me
considero religiosa porque yo creo yo sí

368
00:14:49,160 --> 00:14:49,170
considero religiosa porque yo creo yo sí
 

369
00:14:49,170 --> 00:14:51,740
considero religiosa porque yo creo yo sí
creo en la vida

370
00:14:51,740 --> 00:14:51,750
creo en la vida
 

371
00:14:51,750 --> 00:14:56,920
creo en la vida
con esto claro lo que sucede

372
00:14:56,920 --> 00:14:56,930

 

373
00:14:56,930 --> 00:15:01,940

buen día y

374
00:15:01,940 --> 00:15:01,950

 

375
00:15:01,950 --> 00:15:05,070

[Música]

376
00:15:05,070 --> 00:15:05,080
[Música]
 

377
00:15:05,080 --> 00:15:09,570
[Música]
tengo la suerte de la suerte de conocer

378
00:15:09,570 --> 00:15:09,580
tengo la suerte de la suerte de conocer
 

379
00:15:09,580 --> 00:15:12,450
tengo la suerte de la suerte de conocer
a mi maestro porque para mí es el top no

380
00:15:12,450 --> 00:15:12,460
a mi maestro porque para mí es el top no
 

381
00:15:12,460 --> 00:15:23,620
a mi maestro porque para mí es el top no
hay no hay más

382
00:15:23,620 --> 00:15:23,630

 

383
00:15:23,630 --> 00:15:25,099

el sueño

384
00:15:25,099 --> 00:15:25,109
el sueño
 

385
00:15:25,109 --> 00:15:27,210
el sueño
i

386
00:15:27,210 --> 00:15:27,220
i
 

387
00:15:27,220 --> 00:15:33,300
i
[Música]

388
00:15:33,300 --> 00:15:33,310
[Música]
 

389
00:15:33,310 --> 00:15:35,360
[Música]
y

390
00:15:35,360 --> 00:15:35,370
y
 

391
00:15:35,370 --> 00:15:39,549
y
[Música]

392
00:15:39,549 --> 00:15:39,559
[Música]
 

393
00:15:39,559 --> 00:15:41,610
[Música]
s

394
00:15:41,610 --> 00:15:41,620
s
 

395
00:15:41,620 --> 00:15:44,319
s
[Música]

396
00:15:44,319 --> 00:15:44,329
[Música]
 

397
00:15:44,329 --> 00:15:47,610
[Música]
yo

398
00:15:47,610 --> 00:15:47,620

 

399
00:15:47,620 --> 00:15:49,490

[Música]

400
00:15:49,490 --> 00:15:49,500
[Música]
 

401
00:15:49,500 --> 00:15:49,590
[Música]
[Aplausos]

402
00:15:49,590 --> 00:15:49,600
[Aplausos]
 

403
00:15:49,600 --> 00:15:53,929
[Aplausos]
[Música]

404
00:15:53,929 --> 00:15:53,939
[Música]
 

405
00:15:53,939 --> 00:16:04,230
[Música]
es uno de corazón y no mentir

406
00:16:04,230 --> 00:16:04,240

 

407
00:16:04,240 --> 00:16:08,120

son solo

408
00:16:08,120 --> 00:16:08,130
son solo
 

409
00:16:08,130 --> 00:16:10,080
son solo
con corazón

410
00:16:10,080 --> 00:16:10,090
con corazón
 

411
00:16:10,090 --> 00:16:21,640
con corazón
[Música]

412
00:16:21,640 --> 00:16:21,650

 

413
00:16:21,650 --> 00:16:33,030

[Música]

414
00:16:33,030 --> 00:16:33,040

 

415
00:16:33,040 --> 00:16:42,260

[Música]

416
00:16:42,260 --> 00:16:42,270

 

417
00:16:42,270 --> 00:16:52,190

[Música]

