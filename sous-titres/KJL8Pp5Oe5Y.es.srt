1
00:01:31,170 --> 00:01:35,220
En Soto Zen, que practicamos, decimos: "No busques una gran iluminación". 

2
00:01:35,560 --> 00:01:38,407
La iluminación, en sí misma, no existe. 

3
00:01:38,407 --> 00:01:40,170
¿Por qué ella existiría? 

4
00:01:40,780 --> 00:01:45,720
Tienes que mirar la naturaleza y las cosas como son para ver la verdad. 

5
00:01:46,200 --> 00:01:49,663
La iluminación es absolutamente natural. 

6
00:01:49,663 --> 00:01:52,780
No es una condición especial. 

7
00:01:53,320 --> 00:01:59,400
En el zen, no hay una condición especial en la iluminación a menos que haya una enfermedad de la mente. 

8
00:01:59,700 --> 00:02:04,760
No hay una iluminación especial per se y uno no debería buscarla. 

9
00:02:05,540 --> 00:02:07,433
Muchos maestros insisten en ello. 

10
00:02:07,433 --> 00:02:08,681
Esto es extremadamente importante. 

11
00:02:08,681 --> 00:02:11,000
La iluminación está en cada momento, en cada zazen. 

12
00:02:11,320 --> 00:02:15,580
Estaba hablando con un especialista en drogas psicotrópicas … 

13
00:02:15,790 --> 00:02:20,700
diseñado para lograr estados especiales de conciencia. 

14
00:02:21,130 --> 00:02:24,686
Sostuvo que el zazen era un medio para lograr la iluminación. 

15
00:02:24,686 --> 00:02:25,889
Eso no es cierto en absoluto. 

16
00:02:26,350 --> 00:02:31,379
Zazen es la iluminación misma. 

17
00:02:32,410 --> 00:02:33,254
Dogen dijo: 

18
00:02:33,254 --> 00:02:36,269
"Fui a China y conocí a un verdadero maestro". 

19
00:02:37,390 --> 00:02:41,729
"Lo que aprendí de él es que los ojos son horizontales y la nariz es vertical". 

20
00:02:42,370 --> 00:02:45,810
"Y ese no debe ser engañado, ni por uno mismo ni por otros". 

21
00:03:44,750 --> 00:03:46,750
y cada músculo en el vientre 

22
00:03:47,269 --> 00:03:52,358
debe estar relajado para dar la impresión de que los intestinos están pesando. 

23
00:04:09,010 --> 00:04:15,420
si nos concentramos completamente en la postura en ese momento, podemos dejar que los pensamientos 

24
00:04:15,850 --> 00:04:17,850
sin parar ahí. 

25
00:04:25,810 --> 00:04:28,914
Lo más hermoso que puedes hacer con tu cuerpo es el zazen. 

26
00:04:28,914 --> 00:04:30,419
Este es el esplendor de la naturaleza. 

27
00:04:30,849 --> 00:04:33,728
Inspira calma, armonía, fuerza. 

28
00:04:33,728 --> 00:04:36,539
Todo está contenido en la postura de zazen. 

29
00:04:39,969 --> 00:04:43,447
En el campamento de verano trato de alentar la práctica. 

30
00:04:43,447 --> 00:04:46,111
El ambiente en el dojo, las posturas. 

31
00:04:46,111 --> 00:04:47,909
Una persona que perturba eso 

32
00:04:50,540 --> 00:04:53,940
con su historia personal, estoy dispuesto a matarla. 

33
00:04:57,120 --> 00:04:59,120
Es la única regla que conozco: compasión. 

34
00:05:00,600 --> 00:05:02,477
Sensei estaba mostrando su kyosaku: 

35
00:05:02,477 --> 00:05:04,160
"El personal de la compasión!" 

36
00:05:04,660 --> 00:05:11,080
A veces golpeaba 20 tiros seguidos. 

37
00:05:14,440 --> 00:05:19,640
Él decía: "¡Sensei, muy compasivo!" 

38
00:05:26,280 --> 00:05:32,940
Cuando un discípulo quiere ser ayudado en su meditación al ser golpeado en el hombro con un palo, lo pide. 

39
00:05:33,180 --> 00:05:36,800
Porque siente que su atención comienza a disminuir. 

40
00:05:37,140 --> 00:05:41,360
Cuando tienes sueño 

41
00:05:41,580 --> 00:05:46,300
O cuando sientes complicaciones nublando tu cerebro. 

42
00:05:46,540 --> 00:05:49,520
Ya no es tan tranquilo. 

43
00:05:49,940 --> 00:05:54,600
Si tienes sueño o si tu mente está inquieta … 

44
00:06:01,240 --> 00:06:03,240
Gasshô! 

45
00:06:21,580 --> 00:06:23,580
Zazen continúa durante 

46
00:06:23,840 --> 00:06:25,233
durante varias horas todos los días. 

47
00:06:25,233 --> 00:06:25,929
Y a veces, día y noche. 

48
00:06:26,180 --> 00:06:30,291
Pero es interrumpido por una meditación mientras camina. 

49
00:06:30,291 --> 00:06:33,489
Es decir, el estado interno permanece, 

50
00:06:34,280 --> 00:06:38,799
Pero la postura es diferente. 

51
00:06:39,940 --> 00:06:44,040
Toda la atención se centra en el pulgar. 

52
00:06:44,600 --> 00:06:47,500
Cada respiración corresponde a medio paso. 

53
00:06:58,880 --> 00:07:05,020
Los ojos están en el suelo a 3 metros de frente. 

54
00:07:13,340 --> 00:07:18,580
Es la misma respiración que en zazen, durante la meditación en postura sentada. 

55
00:07:20,740 --> 00:07:28,300
La respiración zen es lo opuesto a la respiración normal. 

56
00:07:28,560 --> 00:07:30,940
Cuando le pides a alguien que respire, 

57
00:07:31,240 --> 00:07:34,600
El reflejo es respirar profundamente. 

58
00:07:35,060 --> 00:07:38,860
En zen, nos vamos a enfocar 

59
00:07:39,080 --> 00:07:42,740
especialmente en la exhalación 

60
00:07:43,040 --> 00:07:46,780
La exhalación es larga, tranquila y profunda. 

61
00:07:54,360 --> 00:07:56,160
Está anclado en el suelo. 

62
00:07:56,160 --> 00:07:58,860
Puede durar hasta uno, dos minutos. 

63
00:07:59,080 --> 00:08:02,940
Dependiendo de su estado de concentración y calma. 

64
00:08:03,280 --> 00:08:09,600
Puede ser muy suave, imperceptible. 

65
00:08:09,860 --> 00:08:15,640
Primero estudiamos esta respiración insistiendo, como una vaca mugeando. 

66
00:08:38,020 --> 00:08:44,640
Al final de la exhalación, dejamos que la inspiración tenga lugar automáticamente. 

67
00:08:45,880 --> 00:08:49,640
Relajamos todo, abrimos los pulmones. 

68
00:08:50,300 --> 00:08:55,380
Dejamos que la inspiración venga automáticamente. 

69
00:08:55,820 --> 00:08:58,689
No ponemos mucha intención en la inspiración. 

70
00:08:59,600 --> 00:09:03,129
Al hacerlo, entenderás 

71
00:09:04,550 --> 00:09:06,550
esa respiración lo es todo. 

72
00:09:07,910 --> 00:09:09,910
Es un circulo. 

73
00:09:10,070 --> 00:09:12,070
A menudo los maestros dibujan un círculo. 

74
00:09:12,560 --> 00:09:14,560
Se trata de respirar. 

75
00:09:15,040 --> 00:09:18,880
Esa respiración tuvo mucha influencia, 

76
00:09:19,340 --> 00:09:23,200
especialmente en las artes marciales. 

77
00:09:23,480 --> 00:09:29,160
En las artes marciales japonesas, se usa la respiración zen. 

78
00:09:29,580 --> 00:09:34,840
Muchos grandes maestros de artes marciales como Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
practicó el zen y se convirtió en discípulos de grandes maestros. 

80
00:09:39,940 --> 00:09:45,480
Y han adaptado la enseñanza del zen y la respiración a su arte marcial. 

81
00:09:45,820 --> 00:09:53,860
Al final, la respiración está más allá de las técnicas y es totalmente libre. 

82
00:09:58,080 --> 00:10:03,340
La enseñanza del Buda a sus discípulos es muy simple. 

83
00:10:03,640 --> 00:10:09,440
Se basa en los actos fundamentales del ser humano. 

84
00:10:09,660 --> 00:10:11,260
Cómo mover. 

85
00:10:11,480 --> 00:10:14,120
Cómo comer, cómo respirar. 

86
00:10:14,440 --> 00:10:15,836
Cómo pensar. 

87
00:10:15,836 --> 00:10:20,860
Y finalmente, cómo sentarse para darse cuenta de su divinidad. 

88
00:11:24,940 --> 00:11:28,702
Genmai ha sido la comida de los monjes desde el Buda Shakyamuni. 

89
00:11:28,702 --> 00:11:31,800
Mezclamos las verduras que tenemos a mano con arroz. 

90
00:11:32,280 --> 00:11:39,440
Cocinamos durante mucho tiempo, con mucha atención y concentración. 

91
00:11:39,780 --> 00:11:42,730
Es una comida transmitida del Buda. 

92
00:11:42,730 --> 00:11:45,920
En China y Japón, comemos la misma comida. 

93
00:11:47,660 --> 00:11:49,980
Realmente es el cuerpo del Buda. 

94
00:11:50,280 --> 00:11:53,080
Cuando estamos a punto de comer, 

95
00:11:53,440 --> 00:11:55,560
tienes que hacerte la pregunta: 

96
00:11:55,900 --> 00:11:59,460
"¿Por qué voy a comer?" 

97
00:11:59,680 --> 00:12:01,580
Un animal o un patán 

98
00:12:01,960 --> 00:12:05,680
no va a estar preguntándose y saltando arriba y abajo en su plato. 

99
00:12:06,040 --> 00:12:09,820
Obviamente es para sobrevivir, pero ¿para qué sobrevivir? 

100
00:12:10,060 --> 00:12:13,520
Eso es lo que dice en los sutras que se cantan durante las comidas. 

101
00:12:13,780 --> 00:12:15,240
"¿Por qué estamos comiendo?" 

102
00:12:15,520 --> 00:12:17,591
"¿De dónde viene esta comida? 

103
00:12:17,800 --> 00:12:18,860
¿Quién lo preparó? 

104
00:12:19,180 --> 00:12:21,140
"¿Quién cultivó el arroz y las verduras?" 

105
00:12:21,400 --> 00:12:26,140
"¿Quién cocinaba?" 

106
00:12:26,500 --> 00:12:29,960
Dedicamos la comida y agradecemos a todas estas personas. 

107
00:12:30,240 --> 00:12:32,140
Y en plena conciencia, 

108
00:12:32,620 --> 00:12:37,340
Decido comer esta comida para 

109
00:12:37,780 --> 00:12:44,620
la evolución que llegué a lograr en esta tierra, en este mundo material. 

110
00:12:52,360 --> 00:12:56,900
EMS es a las 10:30. 6 personas para la cocina. 

111
00:13:01,080 --> 00:13:04,080
Dos personas para la gran inmersión. 

112
00:13:05,940 --> 00:13:10,340
Tres personas por el servicio. 

113
00:13:19,600 --> 00:13:23,580
Samu no está especialmente haciendo albañilería, barriendo … 

114
00:13:24,190 --> 00:13:27,989
o lavar los platos. 

115
00:13:28,540 --> 00:13:30,540
Samu es toda acción 

116
00:13:30,760 --> 00:13:32,939
(Escribir un libro es un samu) 

117
00:13:35,320 --> 00:13:37,320
llevado a cabo sin 

118
00:13:37,640 --> 00:13:38,860
intención 

119
00:13:39,180 --> 00:13:40,160
oculto 

120
00:13:44,950 --> 00:13:47,249
Sin motivos secretos. 

121
00:13:47,980 --> 00:13:50,048
Es como tu trabajo en la vida cotidiana. 

122
00:13:50,048 --> 00:13:52,289
Puedes tomarlo como enriquecimiento personal 

123
00:13:52,839 --> 00:13:57,749
o como una especie de regalo que te das a ti mismo y a todos los demás al mismo tiempo. 

124
00:13:59,320 --> 00:14:02,460
A veces considero 

125
00:14:03,400 --> 00:14:05,459
Algunas personas pueden tomarlo como una tarea. 

126
00:14:05,920 --> 00:14:09,390
Pero apuesto a que duelen en zazen. 

127
00:14:16,400 --> 00:14:19,550
Como una monja zen, 

128
00:14:23,040 --> 00:14:26,180
¿Qué actitud debes tomar en el trabajo? 

129
00:14:27,080 --> 00:14:30,100
¡No lo sé, nunca he trabajado! 

130
00:14:33,960 --> 00:14:37,060
¿Tenemos que hacer un samu con él? 

131
00:14:38,500 --> 00:14:39,520
Sí, ese es el punto. 

132
00:14:39,740 --> 00:14:41,100
Estaba hablando basura, eh. 

133
00:14:41,300 --> 00:14:47,500
Cuando era más joven tenía un trabajo como conserje. 

134
00:14:48,090 --> 00:14:54,920
Limpié las escaleras y saqué la basura todas las mañanas a las 5:00. 

135
00:14:55,140 --> 00:15:00,619
Las escaleras, era un hurra. Había mierda de perro. 

136
00:15:00,920 --> 00:15:05,360
Pensé: "Pero este no es el dojo o el zen". 

137
00:15:05,760 --> 00:15:09,760
"No importa. Para mí, es como un templo". 

138
00:15:10,320 --> 00:15:14,160
Lo hice como un samurai. 

139
00:15:14,480 --> 00:15:17,040
Y Sensei me preguntó: 

140
00:15:17,420 --> 00:15:20,080
"Stéphane, me gustaría que limpies el dojo". 

141
00:15:20,400 --> 00:15:23,660
"Sí, lo haré. El viernes estoy libre". 

142
00:15:25,440 --> 00:15:27,920
"Lo haré el próximo viernes". 

143
00:15:28,580 --> 00:15:29,660
Él me dijo: 

144
00:15:29,940 --> 00:15:32,500
"No, quiero que lo hagas todos los días". 

145
00:15:33,120 --> 00:15:34,272
Entonces el dijo: 

146
00:15:34,520 --> 00:15:37,440
"Un día sin trabajo, un día sin comida". 

147
00:15:37,860 --> 00:15:40,720
"¡Pero no tiene nada que ver con la comida!" 

148
00:15:41,000 --> 00:15:44,440
"¡Un día sin trabajar, un día sin comer mushotoku!" 

149
00:15:44,780 --> 00:15:47,380
Fue una expresión muy original: 

150
00:15:47,700 --> 00:15:50,380
"¡Debes comer mushotoku!" 

151
00:15:54,760 --> 00:15:57,540
¡No significaba nada! 

152
00:15:58,180 --> 00:16:02,500
Decir que eres un monje, practicar samu es una revolución. 

153
00:16:02,880 --> 00:16:05,880
Debemos hacer la revolución, ¡siempre lo he dicho! 

154
00:16:06,920 --> 00:16:10,657
¡Pero no sé cómo usar cócteles molotov! 

155
00:16:11,000 --> 00:16:14,100
Entonces, en su trabajo, tienes que practicar samu. 

156
00:16:14,400 --> 00:16:19,780
Para resolver todos los problemas del mundo, todo lo que tienes que hacer es encontrar esto. 

157
00:16:20,100 --> 00:16:22,060
La gente ya no tiene el espíritu del samu. 

158
00:16:22,520 --> 00:16:23,920
Ya no comen mushotoku. 

159
00:16:24,160 --> 00:16:25,580
Ellos comen hamburguesas y beben Coca-Cola. 

160
00:16:26,180 --> 00:16:28,880
Entonces, quien te diga: 

161
00:16:29,580 --> 00:16:31,580
Pero son las 6:30, ¡eres el único que queda! 

162
00:16:31,880 --> 00:16:33,140
Le dices: 

163
00:16:33,360 --> 00:16:36,160
"¡Yo, practico samu!" 

164
00:16:45,640 --> 00:16:47,640
"¡Porque quiero comer mushotoku!" 

165
00:16:49,780 --> 00:16:51,200
"¿Qué es mushotoku?" 

166
00:16:51,440 --> 00:16:52,780
"¡Yo también quiero un poco!" 

167
00:16:57,040 --> 00:17:01,200
Tenemos que enseñar a las personas sin tener miedo. 

168
00:17:03,180 --> 00:17:06,660
¡Enorgullécete de ser mis discípulos y de ser monjes! 

169
00:17:07,000 --> 00:17:09,000
Y para ser discípulos de Deshimaru. 

