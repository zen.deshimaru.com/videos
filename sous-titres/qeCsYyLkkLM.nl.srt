1
00:00:11,160 --> 00:00:13,720
Goedendag. Hoe gaat het met je?

2
00:00:16,560 --> 00:00:21,280
De houding, onderwezen door Meester Deshimaru…

3
00:00:25,000 --> 00:00:28,460
Hij noemde het de Zen-methode…

4
00:00:31,920 --> 00:00:35,640
Begint met het hebben van een stabiele basis…

5
00:00:39,600 --> 00:00:43,160
Dat wil zeggen, je hebt de twee knieën die de vloer raken…

6
00:00:47,480 --> 00:00:50,540
En je plaatst de pyrhenuaem op de kluis (kussen)

7
00:00:53,860 --> 00:00:58,940
Als je de volledige lotus doet, plaats je eerst een voet…

8
00:01:04,220 --> 00:01:07,300
En dan de andere.

9
00:01:07,900 --> 00:01:12,140
Als je een goede houding hebt, kun je echt loslaten.

10
00:01:12,140 --> 00:01:15,220
Zoals ze zeggen; laat lichaam en geest los.

11
00:01:22,460 --> 00:01:24,320
Meester Deshimaru zei:

12
00:01:25,020 --> 00:01:28,640
Zen is het nieuwe humanisme voor de 21ste eeuw.

13
00:01:29,560 --> 00:01:31,440
Het moet altijd vers zijn.

14
00:01:31,440 --> 00:01:34,000
Je moet de echte zen oefenen.

15
00:01:34,000 --> 00:01:36,600
Trek je kin naar binnen

16
00:01:37,060 --> 00:01:39,980
De wervelkolom recht, uitgerekt.

17
00:01:40,580 --> 00:01:42,580
Alleen Zazen.

18
00:01:43,780 --> 00:01:45,400
De rest is niet zo belangrijk.

19
00:01:45,820 --> 00:01:48,140
Aan het begin van Zazen hoor je drie kleine belletjes.

20
00:01:52,040 --> 00:01:56,060
En dan doen we kinhin. De wandelende meditatie.

21
00:02:01,580 --> 00:02:03,580
Er zullen twee kleine belletjes zijn.

22
00:02:15,980 --> 00:02:18,940
Rek de knie van je voorpoot goed uit.

23
00:02:22,900 --> 00:02:28,340
En duw de grond in met de wortel van je grote teen van je voorvoet…

24
00:02:34,580 --> 00:02:36,580
Ontspan goed je schouders.

25
00:02:38,260 --> 00:02:41,480
En dan aan het einde van je vervaldatum…

26
00:02:44,100 --> 00:02:46,100
Je inspireert…

27
00:02:47,560 --> 00:02:51,500
En je doet een kleine stap voorwaarts met je andere voet.

28
00:02:56,820 --> 00:02:58,820
En dan begin je opnieuw, je ademt uit…

29
00:03:01,360 --> 00:03:05,180
En je zet al je gewicht op je voorvoet.

30
00:03:10,920 --> 00:03:13,760
De duim van je linkerhand…

31
00:03:16,120 --> 00:03:19,960
Je sluit het in je vuist.

32
00:03:22,400 --> 00:03:24,400
Dan neem je je rechterhand…

33
00:03:26,960 --> 00:03:29,480
En je bedekt je linkerhand.

34
00:03:36,140 --> 00:03:40,040
Het oefenen van ’zazoom’is erg handig…

35
00:03:40,260 --> 00:03:42,600
Vooral in deze tijden.

36
00:03:48,280 --> 00:03:53,440
Maar normaal gesproken is het complementair aan de praktijk in een echte dojo.

37
00:03:58,860 --> 00:04:03,320
Het is duidelijk dat we samen aan het oefenen zijn en we wijden deze praktijk aan…

38
00:04:12,540 --> 00:04:16,880
Voor alle mensen die voor elkaar zorgen en een ander proberen te redden.

39
00:04:34,720 --> 00:04:36,720
Hoi Toshi. Hallo.

40
00:04:36,720 --> 00:04:38,720
Hallo, meester.

41
00:04:40,680 --> 00:04:43,940
En alle anderen. Goedendag.

42
00:04:46,100 --> 00:04:48,100
Goedendag Ingrid. >Hello

43
00:04:48,300 --> 00:04:49,960
Ik kijk naar de mensen, omdat…

44
00:04:51,840 --> 00:04:54,640
Er staat hier Gabriela, maar die Gabriela.

45
00:04:54,640 --> 00:04:55,440
Waar is ze?

46
00:04:57,460 --> 00:05:00,520
Ah, ik herkende je niet met je bril.

47
00:05:06,020 --> 00:05:09,160
Ik, die de vertalingen doet, hoe komt het dat je me niet herkent?

