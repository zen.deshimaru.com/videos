1
00:00:08,660 --> 00:00:11,260
Guten Tag alle zusammen 

2
00:00:11,560 --> 00:00:19,400
Um zu wissen, wie man in Zen-Meditation sitzt, zazen .. 

3
00:00:19,800 --> 00:00:22,600
Ich werde Ihnen sofort eine Erklärung geben. 

4
00:00:22,700 --> 00:00:27,880
Achten Sie vor all dem darauf, dass das einzige, was Sie brauchen .. 

5
00:00:28,340 --> 00:00:33,020
ist ein Mensch und ein Meditationskissen. 

6
00:00:33,020 --> 00:00:36,680
Wenn Sie ein Meditationskissen haben, sehr gut. 

7
00:00:37,100 --> 00:00:40,560
Und wenn nicht, können Sie eine mit ein paar Tüchern machen. 

8
00:00:43,060 --> 00:00:44,320
Auf geht’s. 

9
00:00:45,885 --> 00:00:48,855
Also, die Zen-Meditation .. 

10
00:00:49,620 --> 00:00:53,420
Vor allem müssen Sie klar verstehen, dass .. 

11
00:00:53,720 --> 00:00:57,580
Zen-Meditation ist eine sitzende Meditation. 

12
00:00:57,940 --> 00:01:00,820
Bewegungslos und leise. 

13
00:01:01,295 --> 00:01:04,425
Und die Besonderheit dieser Meditation ist .. 

14
00:01:04,425 --> 00:01:07,445
Dass wir vor der Wand sitzen. 

15
00:01:08,400 --> 00:01:11,040
Wie jetzt sitze ich vor der Kamera aber .. 

16
00:01:11,380 --> 00:01:15,900
Aber während der Meditation sitzt du vor der Wand. 

17
00:01:16,520 --> 00:01:21,940
Wir werden uns also mit drei verschiedenen Aspekten befassen. 

18
00:01:24,860 --> 00:01:27,060
Aber Sie werden feststellen, dass .. 

19
00:01:27,300 --> 00:01:32,680
Die Meditation übt diese drei Aspekte gleichzeitig. 

20
00:01:33,420 --> 00:01:36,955
Das erste Thema ist die Körperhaltung. 

21
00:01:36,955 --> 00:01:38,435
Mit dem Körper. 

22
00:01:39,175 --> 00:01:40,625
Das zweite Thema ist .. 

23
00:01:41,020 --> 00:01:42,900
Die Atmung. 

24
00:01:42,960 --> 00:01:45,580
Während der Zen-Meditation atmen wir. 

25
00:01:46,300 --> 00:01:48,720
Und der dritte Aspekt .. 

26
00:01:48,880 --> 00:01:50,320
Die mentale Einstellung. 

27
00:01:50,620 --> 00:01:56,180
Das heißt, was machen wir, während wir unbeweglich sitzen? 

28
00:01:56,180 --> 00:01:59,080
Und schweigend vor der Wand. 

29
00:02:00,040 --> 00:02:04,560
Die Körperhaltung 

30
00:02:06,160 --> 00:02:09,600
Die Körperhaltung, die Körperhaltung .. 

31
00:02:09,635 --> 00:02:12,585
Die konkrete Haltung der Zen-Meditation. 

32
00:02:12,585 --> 00:02:17,340
Sie sitzen also auf einem Kissen. 

33
00:02:17,760 --> 00:02:21,440
Nicht am Rand des Kissens, aber gut oben. 

34
00:02:21,440 --> 00:02:24,640
Und Sie sollten darauf achten, Ihre Beine zu kreuzen. 

35
00:02:24,640 --> 00:02:28,160
Auf eine Weise, dass Ihre Knie können .. 

36
00:02:28,220 --> 00:02:32,540
Auf den Boden fallen oder ihn schieben. 

37
00:02:32,540 --> 00:02:35,140
Oder dass sie untergehen. 

38
00:02:35,440 --> 00:02:37,220
Wie machst du das? 

39
00:02:37,840 --> 00:02:41,120
Du nimmst dein Bein so wie du kannst. 

40
00:02:41,380 --> 00:02:44,020
Sie können es so sagen oder .. 

41
00:02:44,685 --> 00:02:47,435
Weniger steif, könnte man es hier platzieren. 

42
00:02:47,435 --> 00:02:50,585
Die Position nennen wir mittleren Lotus. 

43
00:02:50,585 --> 00:02:54,980
Und natürlich können Sie auch das Oberschenkel wechseln. 

44
00:02:55,240 --> 00:02:59,040
Platzieren Sie es trotzdem so, dass Sie sich am wohlsten fühlen. 

45
00:02:59,260 --> 00:03:02,240
Denken Sie daran, wir werden uns nicht bewegen. 

46
00:03:02,500 --> 00:03:07,120
Wir sollten also eine Position finden, die nicht zu schwer ist. 

47
00:03:07,120 --> 00:03:10,600
Was uns davon abhalten wird .. 

48
00:03:10,600 --> 00:03:13,940
Und bleib ruhig. Damit.. 

49
00:03:14,160 --> 00:03:17,460
Die Knie gehen zu Boden. 

50
00:03:17,655 --> 00:03:19,655
Wie machen wir das? 

51
00:03:19,660 --> 00:03:23,800
Das Becken neigt sich etwas nach vorne. 

52
00:03:24,240 --> 00:03:27,415
Also, was ist los? 

53
00:03:27,420 --> 00:03:31,000
Wenn das Becken nach vorne so ist .. 

54
00:03:32,020 --> 00:03:36,180
Wegen der Schwerkraft hinter meinem Rücken .. 

55
00:03:36,180 --> 00:03:39,020
Oder wie wenn ich ein Auto dirigiere .. 

56
00:03:39,020 --> 00:03:41,025
Und wie Sie sehen können .. 

57
00:03:41,025 --> 00:03:43,860
Meine Knie sind in der Luft. 

58
00:03:43,860 --> 00:03:47,060
Und ich möchte, dass meine Knie nach unten gehen. 

59
00:03:47,060 --> 00:03:51,700
Im Zen sagen wir: "Drücke die Erde mit deinen Knien." 

60
00:03:51,900 --> 00:03:54,940
Und diese Bewegung mit der Schwerkraft .. 

61
00:03:55,040 --> 00:03:57,300
Abwärts gehen .. 

62
00:03:57,300 --> 00:04:03,120
Mit dieser leichten Neigung des Beckens sehen wir .. 

63
00:04:03,460 --> 00:04:06,995
Es gibt keine Intervention mit den Muskeln. 

64
00:04:06,995 --> 00:04:10,355
Suchen Sie es im Profil .. 

65
00:04:12,355 --> 00:04:15,315
Für den Fall, dass Sie so sind .. 

66
00:04:15,780 --> 00:04:19,160
Wo das Becken nach vorne gedreht ist .. 

67
00:04:20,220 --> 00:04:21,700
Dreh es ein wenig .. 

68
00:04:22,720 --> 00:04:23,780
Nur in Richtung hier. 

69
00:04:24,960 --> 00:04:27,040
Perfekt gerade. 

70
00:04:27,040 --> 00:04:30,080
Wenn Sie so natürlich sitzen .. 

71
00:04:30,080 --> 00:04:31,740
Der Schwerpunkt liegt .. 

72
00:04:31,740 --> 00:04:34,875
Mehr nach vorne kommen .. 

73
00:04:34,875 --> 00:04:37,555
Und deine Knie werden zu Boden fallen. 

74
00:04:39,555 --> 00:04:42,595
Von diesem geneigten Becken .. 

75
00:04:42,945 --> 00:04:45,725
Sie strecken Ihre Wirbelsäule. 

76
00:04:46,165 --> 00:04:48,885
So gut du kannst. 

77
00:04:49,600 --> 00:04:52,100
In dieser Hinsicht sprechen wir über .. 

78
00:04:52,100 --> 00:04:55,460
Den Himmel schieben .. 

79
00:04:55,460 --> 00:04:58,700
Mit der Spitze unseres Kopfes. 

80
00:05:04,620 --> 00:05:06,680
Im Zen die Wirbelsäule .. 

81
00:05:07,560 --> 00:05:10,720
Startet oder stoppt (wie Sie möchten) hier. 

82
00:05:10,720 --> 00:05:14,020
Wenn wir unsere Wirbelsäule strecken .. 

83
00:05:14,040 --> 00:05:17,100
Das heißt, versuchen Sie es bis dahin zu strecken. 

84
00:05:17,380 --> 00:05:18,740
Also werden wir nicht .. 

85
00:05:19,260 --> 00:05:21,400
Meditiere mit dem Kopf. 

86
00:05:22,335 --> 00:05:25,525
Herunterfallen, weder rückwärts fallen. 

87
00:05:26,015 --> 00:05:28,255
Das Kinn sollte sein .. 

88
00:05:29,080 --> 00:05:31,240
Das geradlinigste. 

89
00:05:31,960 --> 00:05:35,920
Wir sprechen davon, eine gerade horizontale Linie zu halten. 

90
00:05:36,280 --> 00:05:38,320
Die Augen.. 

91
00:05:39,285 --> 00:05:40,345
Der Mund.. 

92
00:05:41,015 --> 00:05:42,905
Eine vertikale Linie .. 

93
00:05:43,305 --> 00:05:45,125
Zwischen den Augen, dem Mund und .. 

94
00:05:45,605 --> 00:05:46,695
Der Bauchnabel. 

95
00:05:48,075 --> 00:05:49,075
Gut. 

96
00:05:49,880 --> 00:05:53,840
Sobald ich so gerade wie möglich sitze .. 

97
00:05:54,060 --> 00:05:56,365
Die Haltung der Hände .. 

98
00:05:56,365 --> 00:05:58,495
Die linke Hand 

99
00:05:59,765 --> 00:06:01,175
Finger zusammen .. 

100
00:06:01,575 --> 00:06:04,725
Sie legen Ihre rechte Hand. 

101
00:06:04,920 --> 00:06:06,380
Finger zusammen. 

102
00:06:06,600 --> 00:06:09,740
Weil die Finger übereinander liegen. 

103
00:06:11,765 --> 00:06:13,975
Eine horizontale Linie mit .. 

104
00:06:13,975 --> 00:06:17,115
Die Daumen berühren sich. 

105
00:06:17,360 --> 00:06:21,280
Sie berühren sich zum Beispiel, um eine Feder zu halten. 

106
00:06:21,280 --> 00:06:22,480
Also keine Notwendigkeit für .. 

107
00:06:22,885 --> 00:06:24,925
Muskeldruck. 

108
00:06:25,915 --> 00:06:29,125
Und das Innere unserer Hände. 

109
00:06:29,905 --> 00:06:33,295
Wir setzen hier an die Basis .. 

110
00:06:33,675 --> 00:06:36,545
Mehr oder weniger in der Mitte des Bauches. 

111
00:06:36,760 --> 00:06:40,900
Also, um diese Haltung beizubehalten .. 

112
00:06:41,620 --> 00:06:47,740
Sie müssen jedes Stück Stoff bekommen 

113
00:06:48,140 --> 00:06:51,800
Welches wird Ihnen helfen, .. 

114
00:06:53,680 --> 00:06:56,675
Lass deine Hände los 

115
00:06:56,680 --> 00:07:00,920
Auf freie Weise gegen deinen Bauch .. 

116
00:07:01,180 --> 00:07:06,080
Ohne sich verpflichtet zu fühlen, Ihre Finger und Schultern zu benutzen .. 

117
00:07:06,400 --> 00:07:09,600
Im Gegenteil, Sie können alle Spannungen loslassen. 

118
00:07:10,180 --> 00:07:12,780
Die Ellbogen sind frei .. 

119
00:07:12,995 --> 00:07:14,985
Ich werde mich ins Profil setzen .. 

120
00:07:15,355 --> 00:07:17,065
So können Sie gut sehen. 

121
00:07:17,485 --> 00:07:19,105
Wir erobern zurück 

122
00:07:20,125 --> 00:07:22,925
Gerade.. 

123
00:07:23,820 --> 00:07:27,460
Die Hände links oben rechts. 

124
00:07:27,740 --> 00:07:31,000
Die Daumen. Und die Innenseite der Hände .. 

125
00:07:31,000 --> 00:07:34,480
An der Basis des Bauches. 

126
00:07:35,140 --> 00:07:36,460
Ellbogen gerade. 

127
00:07:37,985 --> 00:07:38,985
Gut. 

128
00:07:40,120 --> 00:07:42,540
Dies ist die Haltung .. 

129
00:07:43,220 --> 00:07:47,000
Grob sagen wir mal .. 

130
00:07:47,640 --> 00:07:49,840
Vom Material der Körper. 

131
00:07:49,840 --> 00:07:53,240
Sobald ich meinen Körper gelegt habe .. 

132
00:07:54,345 --> 00:07:57,225
In der Meditationshaltung .. 

133
00:07:57,225 --> 00:08:00,455
Sie erinnern sich, schweigend vor einer Wand. 

134
00:08:01,280 --> 00:08:02,680
Was geschieht? 

135
00:08:02,960 --> 00:08:04,520
Was passieren wird ist, dass .. 

136
00:08:04,760 --> 00:08:08,160
Vor der Wand passiert nichts Interessantes. 

137
00:08:08,640 --> 00:08:12,380
Wir sprechen davon, Ihre Aufmerksamkeit nach innen zu lenken. 

138
00:08:14,360 --> 00:08:17,200
Also über deinen Blick während des Zazen: 

139
00:08:17,975 --> 00:08:20,795
Die Augen sind halboffen. 

140
00:08:21,085 --> 00:08:24,345
45 Grad in einer Diagonale .. 

141
00:08:24,345 --> 00:08:25,820
Abwärts. 

142
00:08:26,000 --> 00:08:28,985
Es ist wie eine Vision. 

143
00:08:28,985 --> 00:08:32,155
Und stell es vor dich. 

144
00:08:32,245 --> 00:08:34,785
Es gibt nichts Interessantes zu sehen. 

145
00:08:35,160 --> 00:08:38,920
Dann werden wir dasselbe mit den anderen Sinnen tun. 

146
00:08:39,160 --> 00:08:43,180
Wenn wir über eine stille Meditation sprechen … 

147
00:08:44,440 --> 00:08:48,780
Du brauchst nichts. Keine Musik, nichts. 

148
00:08:49,080 --> 00:08:51,580
Eine innere Stille. 

149
00:08:52,220 --> 00:08:55,680
Es ist eine stille Meditation, also .. 

150
00:08:56,475 --> 00:08:58,315
Es besteht auch keine Notwendigkeit zu sprechen. 

151
00:08:59,235 --> 00:09:00,235
Fazit.. 

152
00:09:00,900 --> 00:09:02,880
Wenn ich unbeweglich bin .. 

153
00:09:02,880 --> 00:09:06,760
Meine Vision nützt nichts. 

154
00:09:06,760 --> 00:09:09,000
Ich höre auch nicht 

155
00:09:09,280 --> 00:09:10,820
oder meine Worte. 

156
00:09:10,980 --> 00:09:14,280
Die Schlussfolgerung ist also, dass .. 

157
00:09:14,500 --> 00:09:17,920
Unsere Sinne, mit denen wir die Welt um uns herum spüren. 

158
00:09:17,920 --> 00:09:21,300
Wir versetzen sie in den Flugzeugmodus. 

159
00:09:21,300 --> 00:09:23,980
Wir könnten es so sagen. Im Standbye. 

160
00:09:24,300 --> 00:09:27,400
Und das ist das Prinzip der Haltung. 

161
00:09:27,400 --> 00:09:30,140
Durch die Gründung von .. 

162
00:09:30,740 --> 00:09:33,675
Diese Körperhaltung und .. 

163
00:09:33,675 --> 00:09:36,775
Um das Äußere zu schneiden .. 

164
00:09:36,775 --> 00:09:39,915
Um es mit unseren Gefühlen zu schneiden. 

165
00:09:40,060 --> 00:09:43,940
Dann können wir anfangen, wirklich zu meditieren. 

166
00:09:51,140 --> 00:09:55,440
Jetzt, wo wir eine ruhigere Haltung haben .. 

167
00:09:56,040 --> 00:09:59,380
Ich bin vor einer Wand und meditiere. 

168
00:09:59,685 --> 00:10:01,975
Und mit dieser Stille .. 

169
00:10:02,495 --> 00:10:04,675
Ich nehme zur Kenntnis, dass .. 

170
00:10:05,480 --> 00:10:09,440
Mein Kopf ist sehr aktiv. 

171
00:10:09,780 --> 00:10:12,880
Also berücksichtige ich das hier .. 

172
00:10:13,120 --> 00:10:17,080
Ich habe viele Gedanken .. 

173
00:10:17,080 --> 00:10:22,740
Was mit unserem Blick verschwinden wird, könnte man so sagen. 

174
00:10:23,040 --> 00:10:26,240
Gedanken, visuelle Bilder .. 

175
00:10:26,245 --> 00:10:28,235
Geräusche.. 

176
00:10:28,865 --> 00:10:29,945
Viele Dinge. 

177
00:10:31,420 --> 00:10:33,520
Also, was werden wir tun? 

178
00:10:33,720 --> 00:10:37,075
In unserer Zen-Meditation ist .. 

179
00:10:37,080 --> 00:10:40,205
Diese Gedanken loslassen. 

180
00:10:40,205 --> 00:10:43,180
Welches ist ein grundlegender Punkt .. 

181
00:10:43,180 --> 00:10:45,500
Von der Zen-Meditationshaltung. 

182
00:10:45,640 --> 00:10:49,200
Während ich so in zazen bin .. 

183
00:10:49,200 --> 00:10:53,380
Ich verbringe meine Zeit nicht damit, darüber nachzudenken. 

184
00:10:53,380 --> 00:10:57,240
Was auch immer, vielleicht interessant, vielleicht nicht .. 

185
00:10:57,420 --> 00:10:59,340
Darum geht es nicht. 

186
00:10:59,580 --> 00:11:03,700
Es geht darum, genau wie wir unseren Körper gelegt haben .. 

187
00:11:03,715 --> 00:11:06,685
In einer ruhigen Situation .. 

188
00:11:06,825 --> 00:11:10,215
Ich werde meine Gedanken und Gedanken setzen .. 

189
00:11:10,505 --> 00:11:13,415
Welches ist immer aufgeregt .. 

190
00:11:13,415 --> 00:11:16,035
Ich werde es sagen .. 

191
00:11:16,035 --> 00:11:19,035
Auf die gleiche Weise.. 

192
00:11:19,040 --> 00:11:20,500
In einem ruhigen Zustand. 

193
00:11:20,680 --> 00:11:23,640
Wie einfach gesagt .. 

194
00:11:23,640 --> 00:11:26,620
Lass meine Gedanken los. 

195
00:11:27,760 --> 00:11:31,800
Unsere Gedanken sprießen von außen. 

196
00:11:32,680 --> 00:11:35,200
Um dies zu demonstrieren; wenn wir uns fragen .. 

197
00:11:35,200 --> 00:11:39,040
Was werden wir in 5 Minuten oder in einer halben Stunde denken? 

198
00:11:39,305 --> 00:11:42,575
Oder morgen? Niemand kann wissen .. 

199
00:11:43,440 --> 00:11:46,780
Also, auch während wir schlafen .. 

200
00:11:46,960 --> 00:11:50,080
Wir haben Träume. Also machen wir immer weiter .. 

201
00:11:50,540 --> 00:11:53,440
Eine mentale Aktivität, bewusst oder nicht. 

202
00:11:53,660 --> 00:11:57,000
Während der Zen-Meditation diese mentale Aktivität .. 

203
00:11:57,165 --> 00:12:00,445
Wir werden es herunterbringen und beruhigen. 

204
00:12:00,760 --> 00:12:04,760
Wie wir sagen; Es geht darum, deine Gedanken vergehen zu lassen. 

205
00:12:05,100 --> 00:12:07,520
Aber dann wirst du sagen, ok sehr gut aber .. 

206
00:12:07,520 --> 00:12:08,900
Wie macht man das? 

207
00:12:09,080 --> 00:12:12,920
Dies können wir durch unsere Atmung tun. 

208
00:12:13,240 --> 00:12:16,400
Das bringt mich zum dritten Punkt; 

209
00:12:16,440 --> 00:12:19,400
Wie atme ich während Zazen? 

210
00:12:19,400 --> 00:12:23,920
Zen-Atmung 

211
00:12:26,740 --> 00:12:29,660
Atmen während der Zen-Meditation. 

212
00:12:29,880 --> 00:12:32,560
Es ist eine Bauchatmung. 

213
00:12:34,415 --> 00:12:36,265
Wir inspirieren.. 

214
00:12:36,705 --> 00:12:39,985
Wir füllen unsere Lungenkapazität wieder auf 

215
00:12:39,985 --> 00:12:42,185
Mit etwas Großzügigkeit. 

216
00:12:42,925 --> 00:12:45,245
Und wir werden uns konzentrieren .. 

217
00:12:45,245 --> 00:12:46,740
Insbesondere.. 

218
00:12:47,120 --> 00:12:49,900
Nach unserem Ablauf 

219
00:12:50,200 --> 00:12:52,560
Um die Luft zurückzugeben. 

220
00:12:52,740 --> 00:12:55,875
Diese Art des Ausatmens, Stück für Stück .. 

221
00:12:55,875 --> 00:12:59,165
Wir versuchen es lang zu machen. 

222
00:12:59,225 --> 00:13:02,275
Eine lange Zeit des Ausatmens. 

223
00:13:02,345 --> 00:13:05,135
Das heißt, die Luft ausatmen .. 

224
00:13:05,140 --> 00:13:06,720
Durch die Nase .. 

225
00:13:07,000 --> 00:13:10,360
Leise ohne Geräusche zu machen. 

226
00:13:10,700 --> 00:13:13,080
Wir sind in einer stillen Meditation. 

227
00:13:13,300 --> 00:13:16,240
Wir atmen die Luft durch die Nase aus. 

228
00:13:17,160 --> 00:13:21,260
Der Versuch, die Ausatmung zu verlängern. 

229
00:13:21,620 --> 00:13:23,880
Und am Ende Ihrer Ausatmung .. 

230
00:13:24,625 --> 00:13:27,785
Wir atmen ein und wieder .. 

231
00:13:36,465 --> 00:13:39,565
Wir lassen die Luft los und wieder … 

232
00:13:43,440 --> 00:13:46,980
Wenn Sie sich also auf … konzentrieren 

233
00:13:47,240 --> 00:13:50,420
das Kommen und Gehen etablierst du .. 

234
00:13:50,820 --> 00:13:53,500
Sie beruhigen Ihre Atmung .. 

235
00:13:54,540 --> 00:13:57,495
Sie sollten es ruhig lassen .. 

236
00:13:57,500 --> 00:14:02,600
Sie sollten die Ausatmung vergrößern .. 

237
00:14:03,160 --> 00:14:07,180
Und das führt dazu, dass Ihr Zwerchfell nach unten geht. 

238
00:14:07,480 --> 00:14:08,740
Es geht runter. 

239
00:14:08,980 --> 00:14:11,980
Es wird dem Darm eine innere Massage geben. 

240
00:14:11,980 --> 00:14:13,980
Welches wird ihnen gut tun. 

241
00:14:14,160 --> 00:14:17,700
Und langsam kann man fühlen .. 

242
00:14:18,160 --> 00:14:20,820
Als ob sich etwas aufbläst. 

243
00:14:21,145 --> 00:14:23,720
In der Zone, in der wir unsere Hände gelegt haben. 

244
00:14:24,020 --> 00:14:27,700
Hier passieren also die Dinge. 

245
00:14:28,040 --> 00:14:30,520
Also ist es nicht hier, aber es ist .. 

246
00:14:31,900 --> 00:14:34,925
Dort unten. 

247
00:14:34,925 --> 00:14:39,360
Atme durch die Nase und schweigend. 

248
00:14:46,440 --> 00:14:49,900
Die Körperhaltung: Überkreuzen Sie Ihre Beine. 

249
00:14:50,020 --> 00:14:52,860
Die Knie gehen zu Boden. 

250
00:14:53,420 --> 00:14:56,140
Das Becken leicht nach vorne gedreht. 

251
00:14:56,700 --> 00:15:00,760
Wir strecken unsere Wirbelsäule so gut wir können. 

252
00:15:01,160 --> 00:15:02,840
Bis zur Krone unseres Kopfes .. 

253
00:15:02,840 --> 00:15:08,040
Wir schieben den Himmel mit dem Kopf. 

254
00:15:08,540 --> 00:15:11,800
Die linke Hand in der rechten Hand. 

255
00:15:11,925 --> 00:15:13,925
Die Daumen in einer horizontalen Linie .. 

256
00:15:13,925 --> 00:15:17,700
Nicht als Tal, nicht als Berg, als Gipfel. 

257
00:15:17,960 --> 00:15:20,100
Einfach gerade. 

258
00:15:20,100 --> 00:15:24,140
Wenn Sie Ihre Hände in Kontakt mit dem Bauch bringen. 

259
00:15:24,500 --> 00:15:26,640
Sie verwenden etwas unter .. 

260
00:15:26,665 --> 00:15:28,675
Um eine bequemere Position zu haben. 

261
00:15:29,125 --> 00:15:31,805
Sie haben Ihre Ellbogen gerade. 

262
00:15:31,805 --> 00:15:32,885
Dies ist unsere Körperhaltung. 

263
00:15:33,645 --> 00:15:37,065
Zweitens unser Geisteszustand. 

264
00:15:37,280 --> 00:15:41,760
Wir schneiden uns von außen. 

265
00:15:42,160 --> 00:15:44,060
Aus Geräuschen .. 

266
00:15:44,060 --> 00:15:46,760
Wir werden nicht mit einem Radio meditieren. 

267
00:15:46,760 --> 00:15:48,755
Oder mit eingeschaltetem Handy. 

268
00:15:48,755 --> 00:15:51,635
Schalten Sie sie also aus, aber das ist logisch. 

269
00:15:52,040 --> 00:15:54,620
Wir schneiden alle Geräusche aus. 

270
00:15:54,620 --> 00:15:58,295
Wir bringen unseren Blick nach innen und kommen zurück zu .. 

271
00:15:58,300 --> 00:16:02,040
Beobachter dessen sein, was von innen heraus vor sich geht. 

272
00:16:02,120 --> 00:16:05,000
Und was passiert drinnen? 

273
00:16:05,380 --> 00:16:07,180
Wir bemerken, dass es eine mentale Aktivität gibt. 

274
00:16:07,660 --> 00:16:10,720
Gedanken brennen .. 

275
00:16:10,725 --> 00:16:12,665
Wir können sie nicht kontrollieren. 

276
00:16:13,155 --> 00:16:16,415
Die Gedanken entstehen automatisch. 

277
00:16:16,440 --> 00:16:21,000
Es geht also nicht darum, diese Erregung zu verhindern. 

278
00:16:21,380 --> 00:16:24,740
Besser gesagt, es geht um .. 

279
00:16:24,740 --> 00:16:29,740
Nicht in den Zug unserer Gedanken springen. 

280
00:16:29,960 --> 00:16:33,420
Und wir sprechen davon, sie passieren zu lassen. 

281
00:16:33,500 --> 00:16:36,385
Wie können wir sie passieren lassen? 

282
00:16:36,385 --> 00:16:41,380
Zuallererst mit der Konzentration des Hier und Jetzt. 

283
00:16:41,540 --> 00:16:43,260
Auf unserer Körperhaltung. 

284
00:16:43,795 --> 00:16:46,835
Und mit dieser Konzentration .. 

285
00:16:46,840 --> 00:16:51,640
Hier und jetzt beim Atmen. 

286
00:16:52,080 --> 00:16:54,960
Ich bin mir meiner Atmung voll bewusst. 

287
00:16:55,265 --> 00:16:58,505
Von meiner Bauchatmung. 

288
00:16:58,540 --> 00:17:02,580
Also atmen wir durch die Nase .. 

289
00:17:03,080 --> 00:17:04,520
Wir atmen ein .. 

290
00:17:05,095 --> 00:17:08,385
Wir versuchen die Ausatmung zu verlängern. 

291
00:17:08,900 --> 00:17:10,460
Jedes Mal.. 

292
00:17:10,700 --> 00:17:14,040
Weicher, länger. 

293
00:17:14,740 --> 00:17:17,735
Und am Ende der Ausatmung fangen wir wieder an. 

294
00:17:17,740 --> 00:17:19,800
Es kommt und geht wie .. 

295
00:17:20,080 --> 00:17:23,920
Eine Welle im Ozean. Die Wellen. 

296
00:17:24,360 --> 00:17:28,340
Aber … lang, lang, lang. 

297
00:17:29,180 --> 00:17:32,605
Wenn Sie sich auf das Hier und Jetzt konzentrieren .. 

298
00:17:32,605 --> 00:17:36,620
Auf den Punkten der Haltung 

299
00:17:36,920 --> 00:17:41,140
Das bedeutet, dass wir während der Meditation nicht schlafen. 

300
00:17:41,680 --> 00:17:44,880
Und wenn Sie sich gleichzeitig konzentrieren .. 

301
00:17:45,040 --> 00:17:48,500
Beim Einrichten Ihrer Atmung .. 

302
00:17:48,680 --> 00:17:51,140
Auf harmonisierte und ruhige Weise. 

303
00:17:51,140 --> 00:17:53,780
Und mit einer verlängerten Exspiration .. 

304
00:17:53,940 --> 00:17:57,680
Sie werden keine Zeit haben, in sich selbst zu denken oder .. 

305
00:17:57,800 --> 00:18:00,060
Oder in irgendetwas anderem, wie interessant es auch sein mag. 

306
00:18:00,295 --> 00:18:03,365
Die Schlussfolgerung lautet also .. 

307
00:18:03,365 --> 00:18:05,665
Wir werden 20 Minuten, eine halbe Stunde, eine Stunde meditieren. 

308
00:18:06,820 --> 00:18:08,540
Du entscheidest. 

309
00:18:08,720 --> 00:18:12,380
Aber dieser Moment der Meditation, der Konzentration. 

310
00:18:12,455 --> 00:18:15,635
Gibt Ihnen sofortige Vorteile. 

311
00:18:15,635 --> 00:18:17,255
Du wirst dich fühlen .. 

312
00:18:18,405 --> 00:18:21,405
Ein bisschen ruhiger. 

313
00:18:21,405 --> 00:18:24,665
Ein bisschen leichter. 

314
00:18:24,665 --> 00:18:27,355
Und noch besser .. 

315
00:18:27,355 --> 00:18:29,355
Sie erstellen einen Abstand zwischen .. 

316
00:18:29,355 --> 00:18:32,445
sich selbst und all die Emotionen, die in Ihrem Leben entstehen. 

317
00:18:32,885 --> 00:18:34,395
Und das.. 

318
00:18:34,785 --> 00:18:37,725
Es ist wirklich ein gutes Werkzeug 

319
00:18:38,135 --> 00:18:41,015
Meistens.. 

320
00:18:41,015 --> 00:18:44,055
Aber besonders jetzt .. 

321
00:18:44,060 --> 00:18:48,420
Diese Hälfte unserer Bevölkerung ist eingesperrt. 

322
00:19:29,920 --> 00:19:34,960
Rat für einen Anfänger 

323
00:19:35,300 --> 00:19:40,380
Anstatt wütend zu werden oder Aufhebens zu machen .. 

324
00:19:40,380 --> 00:19:45,380
In Pferden, die mehr oder weniger Gedanken verbreiten .. 

325
00:19:45,380 --> 00:19:48,160
Von geringerer Dimension .. 

326
00:19:48,420 --> 00:19:49,840
Setz dich. 

327
00:19:49,995 --> 00:19:53,325
Vor einer Wand. Nimm diese Meditation an. 

328
00:19:54,440 --> 00:19:58,320
Arbeiten Sie auf diese Weise an sich. 

329
00:19:58,760 --> 00:20:00,620
Und Sie werden bald bemerken .. 

330
00:20:00,625 --> 00:20:02,925
Wie gut wird es dir gehen. 
