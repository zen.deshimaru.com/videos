1
00:00:15,415 --> 00:00:17,385
When you want

2
00:00:17,385 --> 00:00:19,515
to explain about meditation

3
00:00:19,515 --> 00:00:21,520
In fact, what you do explain
is the posture

4
00:00:21,520 --> 00:00:22,745
in which you are sitting

5
00:00:23,405 --> 00:00:26,595
We even say you practice the posture

6
00:00:26,595 --> 00:00:27,765
The Buddha posture

7
00:00:29,495 --> 00:00:32,685
So during meditation

8
00:00:32,895 --> 00:00:35,805
Your body -the posture in which you are sitting-

9
00:00:35,805 --> 00:00:38,235
and your mind, they are one

10
00:00:38,255 --> 00:00:40,685
So your body influences your mind

11
00:00:40,745 --> 00:00:43,475
And it’s the reason why we call it: "body-mind".

12
00:00:43,475 --> 00:00:46,045
There is also a reason why this posture
is so important

13
00:00:48,355 --> 00:00:51,345
You’ll notice we are sitting
on a zafu

14
00:00:51,345 --> 00:00:52,675
on a cushion

15
00:00:54,405 --> 00:00:56,465
this one is filled with kapok

16
00:00:56,555 --> 00:00:59,585
And...it makes it...

17
00:00:59,585 --> 00:01:02,415
...when you are tall...

18
00:01:02,415 --> 00:01:05,455
you can fill it more

19
00:01:05,455 --> 00:01:08,475
You’ll need a zafu that is thick
like this

20
00:01:08,475 --> 00:01:10,140
And when you have a small posture

21
00:01:10,140 --> 00:01:11,535
you maybe have a thin

22
00:01:11,535 --> 00:01:13,345
a thin cushion

23
00:01:15,865 --> 00:01:18,555
The height of the zafu

24
00:01:18,555 --> 00:01:21,945
it should be

25
00:01:22,005 --> 00:01:25,245
on a way that your pelvis

26
00:01:25,245 --> 00:01:28,015
that your pelvis has a natural...

27
00:01:28,015 --> 00:01:29,845
it bends a little bit forward

28
00:01:31,295 --> 00:01:34,515
And it makes that you can relax
your stomach

29
00:01:35,565 --> 00:01:37,475
And you can stretch your spine

30
00:01:41,255 --> 00:01:42,255
So...

31
00:01:42,585 --> 00:01:45,555
in a meditation position
first thing you do is

32
00:01:45,555 --> 00:01:48,575
you bring one foot

33
00:01:48,575 --> 00:01:49,895
to your zafu

34
00:01:50,815 --> 00:01:53,645
And the other foot
to the opposite leg

35
00:01:53,645 --> 00:01:54,705
near the floor

36
00:01:58,275 --> 00:02:01,425
So now you have your both knees
on the floor

37
00:02:01,425 --> 00:02:03,135
And this is important:
both knees on the floor

38
00:02:03,595 --> 00:02:06,705
If this posture is too difficult for you

39
00:02:07,025 --> 00:02:09,255
you can put this foot

40
00:02:09,545 --> 00:02:10,675
on the floor

41
00:02:12,265 --> 00:02:15,305
And if at this moment you
still have your knee

42
00:02:15,305 --> 00:02:17,075
up in the air

43
00:02:19,305 --> 00:02:20,845
you can take a towel

44
00:02:23,255 --> 00:02:25,835
and place it under your knee

45
00:02:26,465 --> 00:02:29,265
Make your towel as thin as possible

46
00:02:30,045 --> 00:02:32,975
The result should be that both knees

47
00:02:32,975 --> 00:02:34,205
are pushing on the floor

48
00:02:42,845 --> 00:02:45,825
Your knees on the floor,
your pelvis on the cushion

49
00:02:45,825 --> 00:02:48,565
it makes like a triangle

50
00:02:48,565 --> 00:02:51,395
and this triangle is
like the socle

51
00:02:51,395 --> 00:02:52,395
of the posture

52
00:02:53,415 --> 00:02:55,545
And in both this socle
your spine

53
00:02:55,545 --> 00:02:56,995
is in balance

54
00:02:57,455 --> 00:03:00,645
Your spine is neither forward

55
00:03:00,645 --> 00:03:02,645
neither backward
Nor left, nor right

56
00:03:03,375 --> 00:03:05,625
It’s exactly in the middle,
in balance

57
00:03:06,775 --> 00:03:08,515
Your spine stretches

58
00:03:09,225 --> 00:03:11,775
It stretches until the top of your head

59
00:03:12,955 --> 00:03:15,845
So your knees are pushing on the floor

60
00:03:15,845 --> 00:03:18,435
The top of your head is stretching

61
00:03:18,435 --> 00:03:19,855
upwards the sky.

62
00:03:22,705 --> 00:03:25,095
Without creating any tension

63
00:03:27,145 --> 00:03:28,505
Pull your chin in

64
00:03:29,080 --> 00:03:32,220
Pull your chin in,
or we also say stretch your neck

65
00:03:33,040 --> 00:03:35,975
It’s a small detail
a small important

66
00:03:35,975 --> 00:03:38,215
detail of the posture

67
00:03:38,805 --> 00:03:39,805
Um...

68
00:03:40,600 --> 00:03:44,040
Without creating any tension in your neck

69
00:03:47,540 --> 00:03:50,060
Your eyes are

70
00:03:50,065 --> 00:03:52,915
half-opened, half-closed

71
00:03:52,915 --> 00:03:55,895
so your eyes are not close
if you close your eyes,

72
00:03:55,895 --> 00:03:59,055
you will have a tendency

73
00:03:59,055 --> 00:04:01,435
to create, to start dreaming

74
00:04:01,880 --> 00:04:04,180
so your eyes are half-opened

75
00:04:05,540 --> 00:04:08,960
And they are posed 1 meter
before you on the floor

76
00:04:12,320 --> 00:04:14,900
You are not really looking

77
00:04:14,905 --> 00:04:17,655
You are not actively looking

78
00:04:17,655 --> 00:04:18,975
But you can see everything 
around you

79
00:04:25,005 --> 00:04:28,115
Relax your mouth
and your lips

80
00:04:32,435 --> 00:04:34,065
Relax your shoulders

81
00:04:35,405 --> 00:04:36,995
Relax your pelvis

82
00:04:38,645 --> 00:04:39,645
Your stomach

83
00:04:43,165 --> 00:04:45,645
Your hands...one above each other

84
00:04:45,645 --> 00:04:48,555
The right hand is under
the left hand is up

85
00:04:49,435 --> 00:04:52,585
And your middle fingers
are just one above each other

86
00:04:53,855 --> 00:04:57,225
The thumbs touch each other
very lightly

87
00:04:58,025 --> 00:05:00,005
And they are above your
middle fingers

88
00:05:00,895 --> 00:05:03,475
Your hands...

89
00:05:03,475 --> 00:05:06,745
The side of your hands
is against your stomach

90
00:05:07,155 --> 00:05:09,435
To make this easier

91
00:05:10,475 --> 00:05:12,335
it’s good to have a towel

92
00:05:13,105 --> 00:05:14,935
on your legs
and to pose your hands

93
00:05:14,935 --> 00:05:18,225
on the towel
so they can rest on the towel

94
00:05:30,465 --> 00:05:31,905
Knees pressed on the floor

95
00:05:33,545 --> 00:05:34,935
Top of your head

96
00:05:36,125 --> 00:05:38,825
stretching
your spine is stretching

97
00:05:40,485 --> 00:05:41,595
Pull your chin in

98
00:05:44,175 --> 00:05:46,395
Eyes 45 degrees

99
00:05:49,885 --> 00:05:52,645
Your thumbs, they touch
very softly

100
00:05:55,945 --> 00:05:59,375
And you bring all your concentration
on your posture

101
00:06:02,895 --> 00:06:05,585
On the different points of the posture

102
00:06:05,585 --> 00:06:07,575
And also in the posture
in its total

103
00:06:13,015 --> 00:06:16,445
And when you do so
you start remarking several things

104
00:06:17,125 --> 00:06:19,535
The first thing you remark is

105
00:06:19,535 --> 00:06:20,815
your respiration

106
00:06:21,405 --> 00:06:23,435
you remark your breathing

107
00:06:26,945 --> 00:06:29,485
And the instruction 
for your breathing is

108
00:06:29,485 --> 00:06:32,115
It’s already good as it is

109
00:06:32,145 --> 00:06:35,065
So the breathing is now
as good as it is

110
00:06:35,065 --> 00:06:36,735
And you will remark

111
00:06:38,135 --> 00:06:40,455
That...with the time

112
00:06:40,645 --> 00:06:43,235
The posture, which is in 
balance

113
00:06:43,235 --> 00:06:45,875
The posture will influence
your breathing

114
00:06:45,875 --> 00:06:48,915
And it will make that
the breathing becomes more

115
00:06:48,915 --> 00:06:51,835
Slow.
and the exhalation

116
00:06:51,835 --> 00:06:54,315
will be more profound

117
00:06:54,315 --> 00:06:56,845
And the inhalation will be
more short

118
00:06:56,845 --> 00:06:59,515
This process you should let
develops by itself

119
00:06:59,520 --> 00:07:02,585
It will be the posture

120
00:07:02,585 --> 00:07:05,300
who will give you

121
00:07:05,305 --> 00:07:07,305
this breathing.
The breathing is ok

122
00:07:07,305 --> 00:07:10,780
as it is now.

123
00:07:14,880 --> 00:07:17,575
The second thing you remark is

124
00:07:17,575 --> 00:07:18,575
your thinking

125
00:07:19,095 --> 00:07:20,925
You remark your thoughts

126
00:07:21,735 --> 00:07:22,735
and...

127
00:07:23,195 --> 00:07:25,125
in general, we say

128
00:07:25,125 --> 00:07:26,835
let the thoughts pass by

129
00:07:27,695 --> 00:07:29,695
let the thoughts pass by,
we also say

130
00:07:29,700 --> 00:07:33,920
let the thoughts pass by
like clouds in the air

131
00:07:34,960 --> 00:07:37,580
Ok, in practice it means this:

132
00:07:38,155 --> 00:07:41,155
You are sitting in the posture,
concentrated

133
00:07:41,155 --> 00:07:44,145
on your posture, concentrated
on your breathing

134
00:07:44,145 --> 00:07:49,660
following your breathing

135
00:07:53,120 --> 00:07:54,580
And a thought comes up

136
00:07:56,460 --> 00:08:00,360
A thought comes up 
and now you remark the thought

137
00:08:00,540 --> 00:08:03,920
And at this very moment, you remark
the thought kindly

138
00:08:04,215 --> 00:08:05,355
You let it go

139
00:08:06,005 --> 00:08:08,005
And you bring your attention
on your posture

140
00:08:08,005 --> 00:08:12,100
and your breathing

141
00:08:16,760 --> 00:08:19,800
Maybe some moments later

142
00:08:19,805 --> 00:08:21,515
There is another thought
coming up

143
00:08:23,335 --> 00:08:25,945
And at this moment
you remark you are in this thought

144
00:08:28,425 --> 00:08:31,265
Ups... you let it go...
kindly...

145
00:08:31,835 --> 00:08:33,835
And retake the attention 
on your posture

146
00:08:33,840 --> 00:08:36,320
and the breathing

147
00:08:41,220 --> 00:08:44,315
It’s not important how many
thoughts you have

148
00:08:44,315 --> 00:08:47,495
so maybe your life was a little bit
nervous and

149
00:08:47,495 --> 00:08:50,515
today

150
00:08:50,515 --> 00:08:53,335
maybe you have a lot of thoughts

151
00:08:53,335 --> 00:08:54,335
but it’s not important

152
00:08:54,775 --> 00:08:57,115
We compare it to a glass of water

153
00:08:57,115 --> 00:09:00,405
and when you have some herbs
in this glass of water

154
00:09:01,345 --> 00:09:03,005
you move it

155
00:09:03,535 --> 00:09:05,845
the water will be troubled

156
00:09:06,360 --> 00:09:12,460
After a while, all the herbs
will go down to the floor

157
00:09:12,460 --> 00:09:15,055
and the water will become
clear again

158
00:09:15,055 --> 00:09:17,195
the same thing is happening 
with your mind

159
00:09:17,355 --> 00:09:18,805
During zazen,

160
00:09:19,245 --> 00:09:21,985
when you sit longer time

161
00:09:22,285 --> 00:09:24,915
you will maybe have fewer thoughts

162
00:09:24,915 --> 00:09:27,975
and the thoughts that you will have
they are less...

163
00:09:27,975 --> 00:09:30,085
wanting something from you

164
00:09:30,805 --> 00:09:33,985
so you can let them go easily

165
00:09:34,605 --> 00:09:37,305
and create more clearness
in your head

166
00:09:44,545 --> 00:09:46,700
So you see, the posture

167
00:09:46,775 --> 00:09:49,105
is a mixture,
a mixture of

168
00:09:49,395 --> 00:09:52,415
stretching and relaxing

169
00:09:52,455 --> 00:09:55,505
Stretching your spine

170
00:09:55,505 --> 00:09:57,145
Relaxing your stomach

171
00:09:57,825 --> 00:10:00,655
And you see

172
00:10:00,655 --> 00:10:03,785
the same two elements

173
00:10:03,785 --> 00:10:05,785
you can find them back
in your spirit

174
00:10:05,785 --> 00:10:08,440
in your mind

175
00:10:08,580 --> 00:10:09,880
You have...

176
00:10:10,245 --> 00:10:12,875
...you are concentrated

177
00:10:12,875 --> 00:10:15,815
you are concentrated on the posture

178
00:10:15,815 --> 00:10:16,815
and you observe

179
00:10:16,985 --> 00:10:20,365
and you alternate both

180
00:10:20,415 --> 00:10:21,740
you are concentrating,

181
00:10:21,740 --> 00:10:23,055
observing

182
00:10:23,695 --> 00:10:24,785
concentrating, observing

183
00:10:25,115 --> 00:10:27,965
until at a certain moment
they fall together

184
00:10:28,345 --> 00:10:30,045
you concentrate and observe

185
00:10:33,955 --> 00:10:36,855
You practice meditation

186
00:10:36,855 --> 00:10:38,645
without an aim

187
00:10:39,455 --> 00:10:42,195
without wanting something
from it

188
00:10:42,195 --> 00:10:44,895
So just leave all the aims

189
00:10:44,895 --> 00:10:47,125
you could create in your head

190
00:10:47,125 --> 00:10:48,860
like, for example
I would like

191
00:10:48,860 --> 00:10:52,980
to be very calm or
I would like to have

192
00:10:52,980 --> 00:10:54,780
a special experience

193
00:10:55,020 --> 00:10:56,540
Just leave it

194
00:10:56,620 --> 00:10:58,620
Practice without aim
That’s the more profound

195
00:10:58,620 --> 00:11:01,020
practice you can do

196
00:11:04,465 --> 00:11:07,445
How long should you meditate?
How many times?

197
00:11:07,445 --> 00:11:10,465
Well...the most important

198
00:11:10,465 --> 00:11:12,815
is that you visit when possible
a dojo

199
00:11:12,815 --> 00:11:15,645
A dojo in which

200
00:11:15,645 --> 00:11:20,900
older disciples can correct
your posture

201
00:11:21,260 --> 00:11:23,555
And also

202
00:11:23,555 --> 00:11:26,875
you are in contact with other
people who practice meditation

203
00:11:27,285 --> 00:11:29,025
And when you practice at home

204
00:11:29,565 --> 00:11:30,565
Hum...

205
00:11:31,565 --> 00:11:34,565
Beforehand, tell to yourself

206
00:11:34,565 --> 00:11:37,635
I want to practice
for example, 15 minutes

207
00:11:37,635 --> 00:11:40,385
or 20 minutes

208
00:11:40,385 --> 00:11:43,715
maybe you put your time on your phone

209
00:11:43,715 --> 00:11:47,035
the phone without a thick noise
or disturbing you

210
00:11:47,475 --> 00:11:49,075
You put it there

211
00:11:49,075 --> 00:11:52,095
and you sit 15 minutes
or 20 minutes,

212
00:11:52,095 --> 00:11:54,635
even if zazen is not so easy today

213
00:11:54,635 --> 00:11:57,215
you still continue

214
00:11:57,475 --> 00:11:59,415
until your timer says
now 20 minutes

215
00:11:59,415 --> 00:12:01,615
are gone

216
00:12:03,195 --> 00:12:04,195
Hum...

217
00:12:04,645 --> 00:12:07,665
The most difficult with practicing
is not starting

218
00:12:07,665 --> 00:12:10,695
to practice meditation

219
00:12:11,165 --> 00:12:14,135
is continuing
and therefore

220
00:12:14,760 --> 00:12:16,100
it’s important not to
start immediately

221
00:12:16,100 --> 00:12:17,125
with a level that is so high

222
00:12:17,125 --> 00:12:19,475
you cannot follow it

223
00:12:19,785 --> 00:12:22,505
It’s more important to make a rhythm

224
00:12:22,505 --> 00:12:24,935
that you can stay regularly

225
00:12:28,435 --> 00:12:29,435
Ok

226
00:12:30,295 --> 00:12:32,905
So the posture, practice

227
00:12:33,440 --> 00:12:36,480
by concentrating

228
00:12:38,360 --> 00:12:41,500
on the different points of the posture
and on your breathing
