1
00:00:03,420 --> 00:00:07,900
O que é espírito? 

2
00:00:08,220 --> 00:00:16,200
Em chinês e japonês, vários ideogramas são traduzidos como "fantasma". 

3
00:00:16,880 --> 00:00:19,340
Às vezes, é traduzido como "coração". 

4
00:00:21,960 --> 00:00:23,560
É toda existência. 

5
00:00:23,820 --> 00:00:25,240
É a comunidade viva. 

6
00:00:25,500 --> 00:00:27,400
Esse é o espírito comum da comunidade viva. 

7
00:00:27,740 --> 00:00:28,900
Esse é o espírito. 

8
00:00:29,120 --> 00:00:30,780
Existe apenas uma mente. 

9
00:00:32,000 --> 00:00:34,960
O que cria a mente? 

10
00:00:35,740 --> 00:00:37,780
Do ponto de vista budista, nada cria a mente. 

11
00:00:38,100 --> 00:00:40,540
Porque a mente está sem nascimento. 

12
00:00:40,880 --> 00:00:46,780
É sem causa ou desaparecimento. 

13
00:00:47,060 --> 00:00:50,020
A mente é criada, mas não criada. 

14
00:00:50,500 --> 00:00:54,220
A mente é a primeira causa? 

15
00:00:54,460 --> 00:00:57,820
Não pode haver uma primeira causa. 

16
00:00:58,100 --> 00:00:59,940
Porque a origem é pura. 

17
00:01:02,280 --> 00:01:05,420
Qual teria sido a primeira causa? 

18
00:01:05,840 --> 00:01:11,200
Entendemos, mesmo intelectualmente, que se formos no passado, não encontraremos a primeira causa. 

19
00:01:11,660 --> 00:01:13,300
Portanto, a primeira causa não existe. 

20
00:01:13,660 --> 00:01:15,640
A primeira causa é o efeito. 

21
00:01:16,060 --> 00:01:16,900
Agora 

22
00:01:17,440 --> 00:01:20,940
Onde reside o fantasma? 

23
00:01:21,280 --> 00:01:23,120
A mente sempre existe e reside em toda parte. 

24
00:01:23,420 --> 00:01:25,200
Quer estejamos no Zazen ou em outro lugar. 

25
00:01:25,400 --> 00:01:30,140
Não há meditação especial que mude a estrutura da mente. 

26
00:01:30,460 --> 00:01:32,240
A mente está sempre presente. 

27
00:01:32,520 --> 00:01:35,780
O que é "mente-corpo"? 

28
00:01:36,520 --> 00:01:38,920
Matéria e energia não são separadas uma da outra. 

29
00:01:39,280 --> 00:01:44,600
São duas expressões diferentes da mesma coisa. 

30
00:01:47,080 --> 00:01:50,360
Então corpo e mente são duas expressões diferentes da mesma coisa. 

31
00:01:50,760 --> 00:01:52,280
Você não deve denegrir o corpo. 

32
00:01:52,640 --> 00:01:53,980
O corpo é a mente. 

33
00:01:54,340 --> 00:01:55,720
A matéria é a mente. 

34
00:01:56,220 --> 00:02:00,520
Qual é o efeito do pensamento no corpo espiritual? 

35
00:02:01,240 --> 00:02:06,120
O pensamento se materializa na realidade. 

36
00:02:07,020 --> 00:02:09,100
No sonho da matéria. 

37
00:02:09,580 --> 00:02:11,040
Sonhar não acontece apenas quando você está dormindo. 

38
00:02:11,480 --> 00:02:15,580
O sonho da matéria é o que chamamos de "realidade". 

39
00:02:15,900 --> 00:02:17,880
Realidade material. 

40
00:02:18,400 --> 00:02:21,980
O que podemos concluir disso? 

41
00:02:22,340 --> 00:02:27,080
A mente sempre vê na mente. 

