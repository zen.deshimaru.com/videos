1
00:00:00,000 --> 00:00:02,200
Muchas personas están interesadas en la meditación … 

2
00:00:02,200 --> 00:00:04,220
… y busca información en Youtube. 

3
00:00:04,420 --> 00:00:07,860
El problema es que no vemos mucho de eso. Zen del maestro Deshimaru … 

4
00:00:07,980 --> 00:00:10,900
… ni el Kosen Sangha. 

5
00:00:11,100 --> 00:00:13,180
Muchos practicantes talentosos … 

6
00:00:13,360 --> 00:00:16,300
… hizo cadenas de Kosen Sangha. 

7
00:00:16,520 --> 00:00:18,660
Pero el público en general se pregunta: 

8
00:00:18,940 --> 00:00:21,080
¿Cuál es el correcto? 

9
00:00:21,300 --> 00:00:23,600
Ahora, vamos a encadenarlo todo: 

10
00:00:23,820 --> 00:00:26,840
bit.ly/chosen 

11
00:00:28,420 --> 00:00:31,160
Entonces, si quieres presentar zazen a tantas personas como sea posible: 

12
00:00:32,260 --> 00:00:33,980
Suscríbete al canal. 

13
00:00:33,980 --> 00:00:35,220
Mira los videos. 

14
00:00:35,220 --> 00:00:36,520
Por favor comente 

15
00:00:36,700 --> 00:00:38,700
¡Y no dudes en que te gusten! 

