1
00:00:00,000 --> 00:00:03,380
It is useless to go to Japan to practice Zen. 

2
00:00:03,640 --> 00:00:07,360
When people ask me for information in Dijon, I tell them that: 

3
00:00:07,620 --> 00:00:09,240
"We’re not going to talk about Zen." 

4
00:00:09,500 --> 00:00:10,180
"Come to the dojo." 

5
00:00:10,440 --> 00:00:14,040
"Only then will you feel whether it suits you or not." 

6
00:00:14,360 --> 00:00:18,100
From the first time you exercise, you can feel what it brings. 

7
00:00:18,680 --> 00:00:21,800
If you feel like it, it is a sign that you are okay with it. 

8
00:00:22,000 --> 00:00:26,480
There is no need for theoretical explanations to know why we practice. 

9
00:00:26,780 --> 00:00:35,280
When we settle in Zazen, immediately … you feel that the posture has something that gives rest, that calms down. 

10
00:00:35,580 --> 00:00:40,660
The physical attitude is really important to feel it circulating. 

11
00:00:40,940 --> 00:00:46,960
I think it is mainly related to this balance, which allows us to surrender. 

12
00:00:47,240 --> 00:00:49,280
Zazen is also an experience of silence. 

13
00:00:49,620 --> 00:00:59,800
When we’re in silence together, When you listen to that silence, something profound is conveyed. 

14
00:01:00,140 --> 00:01:03,140
I’m thinking of a nurse who comes in completely stressed: 

15
00:01:03,340 --> 00:01:05,840
"I can’t calm the mind!" 

16
00:01:06,120 --> 00:01:08,840
She came out completely calm. 

17
00:01:09,120 --> 00:01:14,620
If we can gradually detect deep breathing, 

18
00:01:15,140 --> 00:01:22,300
it wipes out all those annoying thoughts, all complications. 

19
00:01:22,720 --> 00:01:29,720
It is even difficult to say that I am a Buddhist because it sounds like a church. 

20
00:01:30,280 --> 00:01:33,720
If you don’t need this to practice Zen … 

21
00:01:34,020 --> 00:01:38,800
It is a pity these days, people want secular meditation. 

22
00:01:39,220 --> 00:01:42,800
in Dijon there are many types of meditations. 

23
00:01:43,140 --> 00:01:47,360
I don’t know them, I can’t talk about them. 

24
00:01:47,560 --> 00:01:52,940
But personally I like to practice as it is done. 

25
00:01:53,180 --> 00:01:54,340
With a few simple rules: 

26
00:01:54,700 --> 00:01:56,780
Say hello when we get home. 

27
00:01:57,080 --> 00:01:58,880
Greet other people you practice with. 

28
00:01:59,120 --> 00:02:00,620
We do our meditation. 

29
00:02:00,880 --> 00:02:06,040
Then we recite a sutra practically for the entire universe. 

30
00:02:06,300 --> 00:02:08,200
It is a complete matter. 

31
00:02:08,520 --> 00:02:12,480
It is a way to exercise not only for yourself. 

32
00:02:13,500 --> 00:02:17,180
We don’t always listen to our comfort. 

33
00:02:17,460 --> 00:02:24,300
While people who want some form of meditation, they are already making a choice. 

34
00:02:24,560 --> 00:02:29,720
I like this notion of "aimlessness", in this time when everything tends to make a profit. 

35
00:02:30,000 --> 00:02:32,180
We are in zazen, and that’s it. 

36
00:02:32,420 --> 00:02:35,660
I have received the message from Master Kosen. 

37
00:02:35,960 --> 00:02:38,180
I feel honored and intimidated. 

38
00:02:38,520 --> 00:02:42,800
People imagine that being a master must be fully realized. 

39
00:02:43,080 --> 00:02:45,180
I don’t feel that way. 

40
00:02:45,440 --> 00:02:50,720
I experience this transfer as the desire to continue to transfer the Sangha and to help it grow. 

41
00:02:51,040 --> 00:02:56,300
Our Sanghas are all people who practice with Master Kosen. 

42
00:02:56,980 --> 00:03:04,360
If you liked this video, feel free to like it and subscribe to the channel! 

