1
00:00:08,480 --> 00:00:13,559
ciao a tutti ti spiegherò come tu 

2
00:00:13,559 --> 00:00:20,760
siediti per praticare la meditazione zen chiamata zazen il materiale necessario 

3
00:00:20,760 --> 00:00:27,090
è molto semplice un essere umano e un pulcino di meditazione se ne hai uno 

4
00:00:27,090 --> 00:00:30,869
cuscino piuttosto Amburgo e va bene 

5
00:00:30,869 --> 00:00:35,730
e se no ne fai uno con una coperta dove passa un sacco a pelo 

6
00:00:35,730 --> 00:00:41,060
esempio deve essere abbastanza solido 

7
00:00:57,490 --> 00:01:06,070
Quindi secondo te spiega la posizione di meditazione Zen La meditazione Zen è 

8
00:01:06,070 --> 00:01:13,620
uno soprattutto per sapere che è una posizione seduta immobile e silenziosa 

9
00:01:13,620 --> 00:01:22,630
e la particolarità di questa posizione e che la pratichiamo di fronte al muro qui 

10
00:01:22,630 --> 00:01:26,620
Ovviamente devo affrontare la telecamera per informarti 

11
00:01:26,620 --> 00:01:32,560
quindi quello che devi fare è sederti insieme a cuscino colui che io 

12
00:01:32,560 --> 00:01:37,479
spiegalo che deve essere sufficientemente imbottito e non sufficientemente duro 

13
00:01:37,479 --> 00:01:42,520
siediti bene nella parte inferiore del pulcino, non sederti sul bordo se hai 

14
00:01:42,520 --> 00:01:49,330
scivolato e dovresti, per quanto possibile, incrociare le gambe 

15
00:01:49,330 --> 00:01:55,600
incrociali come questi come questi da soli e si chiama 2010 se 

16
00:01:55,600 --> 00:02:04,270
metti qui le altre persone che si chiama loto, l’importante è che 

17
00:02:04,270 --> 00:02:13,480
le ginocchia vanno a terra per premere e mettere la pressione delle ginocchia sul pavimento 

18
00:02:13,480 --> 00:02:19,780
quindi come farlo per farlo senza alcuna organizzazione o senza alcuno sforzo 

19
00:02:19,780 --> 00:02:27,810
buoni muscoli fisici in tutta la postura di meditazione zen che lasciarsi andare 

20
00:02:27,810 --> 00:02:37,950
in modo che le ginocchia scendano inclinerai leggermente il bacino 

21
00:02:37,950 --> 00:02:46,180
in avanti la parte superiore del bacino va avanti come 6 fino a un po ’per ultimo 

22
00:02:46,180 --> 00:02:49,440
te lo mostra di profilo 

23
00:02:55,200 --> 00:03:02,769
il mio bacino non è cinico come se, ad esempio, guidassi 

24
00:03:02,769 --> 00:03:07,629
un’auto ci vede ma ci mostro per ultimo 

25
00:03:07,629 --> 00:03:11,860
perché il mio centro di gravità è dietro il mio corpo che voglio 

26
00:03:11,860 --> 00:03:19,420
sembra di alta qualità ciao di fronte e lo faccio naturalmente sui ponti Dassin 

27
00:03:19,420 --> 00:03:24,970
quello che vorremmo e passare davanti con gravità le mie ginocchia vanno a terra 

28
00:03:24,970 --> 00:03:32,970
questo è ciò che devi iniziare stabilendo nella postura un posto 

29
00:03:32,970 --> 00:03:42,609
in cui il ginocchio va in sciopero quando hai eseguito da 7 a 6 dal bacino 

30
00:03:42,609 --> 00:03:44,920
che leggermente inclinato per fare la richiesta 

31
00:03:44,920 --> 00:03:51,940
raddrizzerai il dorso il più possibile fino a 

32
00:03:51,940 --> 00:03:57,000
la parte superiore della testa considera che i nostri sono considerati quello 

33
00:03:57,000 --> 00:04:05,889
Colonia arriva fino a dire che se volessi mettere un gioco non è un amo che rileggo 

34
00:04:05,889 --> 00:04:12,280
il che significa che ho tirato la nuca, mi sono adattato al mento, visto che non ce l’ho 

35
00:04:12,280 --> 00:04:16,349
girando la testa davanti non ho la schiena curva 

36
00:04:16,349 --> 00:04:24,610
Sono il più dritto possibile, ti mostro due profili che non sono 

37
00:04:24,610 --> 00:04:33,910
come se non fossi all’asta prima o qualunque cosa io sia più 

38
00:04:33,910 --> 00:04:43,659
di croce anche quando sei riuscito a sedere dritto non ti muovi 

39
00:04:43,659 --> 00:04:53,080
più continuo la mano sinistra le dita sono unite alla mano destra 

40
00:04:53,080 --> 00:04:58,690
le dita sono benedette e ho intenzione di sovrapporre le dita di sinistra sul 

41
00:04:58,690 --> 00:05:08,030
le tracce della mano destra ma le spinte si incontrano si sovrappongono al dito del 

42
00:05:08,030 --> 00:05:14,300
la mano sinistra una linea orizzontale forma il bordo delle mie mani 

43
00:05:14,300 --> 00:05:17,840
così potremmo la contea su tutto il polso 

44
00:05:17,840 --> 00:05:23,480
fino ad allora il bordo delle mie mani lo applicherai alla base 

45
00:05:23,480 --> 00:05:31,370
dell’addome per fare questo ha usato qualcosa che 

46
00:05:31,370 --> 00:05:37,460
può permetterti di fermarti per mano del terrasson che puoi rilasciare 

47
00:05:37,460 --> 00:05:43,780
piena attenzione da parte delle spalle la postura di una postura in cui devi lasciar andare 

48
00:05:43,780 --> 00:05:47,990
allenta la tensione che vedi i miei gomiti sono liberi sono da molto tempo 

49
00:05:47,990 --> 00:05:56,090
così o così piano ma non mi sforzo di mantenerlo 

50
00:05:56,090 --> 00:06:01,070
le mie mani le idee di sforzo per mantenere le spinte al ristorante 

51
00:06:01,070 --> 00:06:08,650
quando mi rendo conto che questo è il punto della postura, lo tocco 

52
00:06:08,650 --> 00:06:13,120
questa è la postura fisica 

53
00:06:20,000 --> 00:06:25,460
dopo questa prima parte sulla seconda parte della postura fisica 

54
00:06:25,460 --> 00:06:30,860
l’atteggiamento mentale una volta che sono nella posizione di 

55
00:06:30,860 --> 00:06:39,220
meditazione zen di fronte al muro cosa devo fare cosa dovrei fare 

56
00:06:39,220 --> 00:06:46,190
nel silenzio di questa posizione, andrete voi altri 

57
00:06:46,190 --> 00:06:53,390
renditi conto che pensi di vedere che continui a pensare e che ci sono 

58
00:06:53,390 --> 00:06:58,490
Migliaia di pennelli di pensiero che vengono che vengono, dicono che vengono da te 

59
00:06:58,490 --> 00:07:03,010
dai renditi conto che questo è un fenomeno naturale 

60
00:07:03,010 --> 00:07:08,840
non puoi controllare cosa faremo durante la meditazione Zen 

61
00:07:08,840 --> 00:07:16,790
lasceremo passare i nostri pensieri, vale a dire in questa immobilità 

62
00:07:16,790 --> 00:07:23,150
saremo in grado di osservare che con un riscatto osserverai i tuoi pensieri e 

63
00:07:23,150 --> 00:07:29,419
i pensieri invece di nutrirli invece di farli crescere vanno 

64
00:07:29,419 --> 00:07:34,610
lasciali passare, quindi non si tratta di rifiutare cosa 

65
00:07:34,610 --> 00:07:39,580
lascia che sia di nuovo lì, questa è la macchina che lascerai passare 

66
00:07:39,580 --> 00:07:46,190
come attirare l’attenzione 

67
00:07:46,190 --> 00:07:51,400
qui ora sul tuo respiro 

68
00:07:55,430 --> 00:07:59,960
respirare zen è respirazione addominale 

69
00:07:59,960 --> 00:08:10,400
inspiri attraverso la tua narice e inspiri lentamente lentamente 

70
00:08:10,400 --> 00:08:14,270
meditazione silenziosa della ricreazione silenziosa 

71
00:08:14,270 --> 00:08:20,090
la caratteristica che respira e che porterai la tua attenzione 

72
00:08:20,090 --> 00:08:30,930
sulla faccia dell’esaltazione ispiriamo arrivando tranquillamente nell’essere 

73
00:08:30,930 --> 00:08:37,620
generoso riempimento e affrontare la nazione quando 

74
00:08:37,620 --> 00:08:44,070
esci tutta l’aria che fai lentamente lentamente per quanto riguarda 

75
00:08:44,070 --> 00:08:47,720
sempre più a lungo 

76
00:08:56,540 --> 00:09:03,889
quando sei alla fine delle tue relazioni, brami ancora e ancora 

77
00:09:03,889 --> 00:09:12,709
espiri e non eri più lentamente più nudo nudo più a lungo 

78
00:09:12,709 --> 00:09:18,069
profondo come le onde di un oceano 

79
00:09:18,069 --> 00:09:31,639
perché sono la spiaggia e tutti questi parlano con il v6 che ti concentri 

80
00:09:31,639 --> 00:09:36,440
qui ora sui punti della postura dei punti fisici 

81
00:09:36,440 --> 00:09:42,259
quindi state attenti qui ora il vostro corpo se voi 

82
00:09:42,259 --> 00:09:47,660
focalizzato qui ora allo stesso tempo a non seguire il tuo 

83
00:09:47,660 --> 00:09:55,040
pensieri torniamo ai punti che posturano se ti concentri qui 

84
00:09:55,040 --> 00:10:03,139
ora stabilendo un respiro coraggioso e armonioso a poco a poco 

85
00:10:03,139 --> 00:10:11,110
Prendi il respiro, espira sempre più a lungo 

86
00:10:11,110 --> 00:10:18,860
ma le pinne non hanno più il tempo di pensare a cosa sei qui e 

87
00:10:18,860 --> 00:10:27,860
ora nella postura di Zaza qui non c’è la postura né la materia corporea 

88
00:10:27,860 --> 00:10:35,569
postura ti ricordo che premi il ginocchio con una pressione sul ginocchio 

89
00:10:35,569 --> 00:10:38,680
suolo spingi la terra 

90
00:10:38,680 --> 00:10:44,920
e raddrizza il più possibile la spina dorsale fino alla cima 

91
00:10:44,920 --> 00:10:49,260
teschio come per spingere il cielo con la testa 

92
00:10:49,260 --> 00:10:55,290
la mano sinistra come a destra l’orizzontale sfoglia il bordo tagliente delle mani 

93
00:10:55,290 --> 00:11:05,020
bene contro la palla sono ancora nel saumur di fronte al muro non c’è niente da fare 

94
00:11:05,020 --> 00:11:11,459
specialmente quello che farò è rivolgere lo sguardo dentro 

95
00:11:11,459 --> 00:11:16,330
così i sensi non li userò più durante 

96
00:11:16,330 --> 00:11:22,060
questo fatto di meditazione sono di fronte al muro, quindi non c’è niente da vedere, quindi lo è 

97
00:11:22,060 --> 00:11:29,890
come se la mia visione la posassi di fronte al mio occupato quindi durante 

98
00:11:29,890 --> 00:11:38,620
meditando c’era la vista a 45 ° in diagonale verso l’Europa che puoi 

99
00:11:38,620 --> 00:11:43,839
medita con gli occhi chiusi ma se tieni gli occhi aperti o l’ultimo 

100
00:11:43,839 --> 00:11:50,100
il contatto con la realtà non va specialmente nei tuoi pensieri 

101
00:11:50,100 --> 00:11:56,110
come in silenzio, anche noi non usiamo l’acqua così 

102
00:11:56,110 --> 00:12:00,880
sentirlo non ha bisogno di un po ’di musica e abbiamo già visto che lo siamo 

103
00:12:00,880 --> 00:12:09,040
gémonies a poco a poco all’improvviso abbiamo tagliato con le sollecitazioni del mondo 

104
00:12:09,040 --> 00:12:15,880
fuori facciamo il colpo di meditazione che ti siedi 

105
00:12:15,880 --> 00:12:21,970
fisicamente è il corpo in postura che ti tagli 

106
00:12:21,970 --> 00:12:28,360
le sollecitazioni con le bancarelle che mettono tutto in modalità aereo istantaneo non mi dicono 

107
00:12:28,360 --> 00:12:34,340
non meditare con il tuo cellulare accanto 

108
00:13:09,540 --> 00:13:14,399
quindi per completare un suggerimento per i principianti non è facile 

109
00:13:14,399 --> 00:13:23,519
scoprire che confida ora non è né un’armatura, ma lo è 

110
00:13:23,519 --> 00:13:28,579
davvero un buon aiuto ti consiglio davvero 

111
00:13:28,579 --> 00:13:33,959
prova a provarlo e vedrai invece di intrattenere pensieri 

112
00:13:33,959 --> 00:13:38,790
che forse non sono pronti brillanti, forse non sono pronti allegri 

113
00:13:38,790 --> 00:13:43,380
essere di cattivo umore o al centro dell’attenzione allo stress è tutto questo 

114
00:13:43,380 --> 00:13:52,620
normale qui con questo lavoro sull’oro l’atteggiamento della mente che lasci andare 

115
00:13:52,620 --> 00:13:57,810
ti allontani da tutto ciò che ci accade per andare a vederti 

116
00:13:57,810 --> 00:14:03,329
ti calmerai fisicamente emotivamente respirando tutto questo? 

117
00:14:03,329 --> 00:14:08,310
ti aiuterà a lasciarti andare, aiuterà profondamente te e il tuo 

118
00:14:08,310 --> 00:14:13,079
entourage se è stato accompagnato quando non si dubitava del 

119
00:14:13,079 --> 00:14:15,529
pratica 

