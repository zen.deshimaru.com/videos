1
00:00:08,660 --> 00:00:11,260
Goedendag iedereen 

2
00:00:11,560 --> 00:00:19,400
Om te weten hoe je in zen-meditatie moet zitten, zazen .. 

3
00:00:19,800 --> 00:00:22,600
Ik zal je meteen een uitleg geven. 

4
00:00:22,700 --> 00:00:27,880
Let er voor dit alles op dat het enige wat je nodig hebt .. 

5
00:00:28,340 --> 00:00:33,020
is een mens en een meditatiekussen. 

6
00:00:33,020 --> 00:00:36,680
Als je een meditatiekussen hebt, heel goed. 

7
00:00:37,100 --> 00:00:40,560
En zo niet, dan kun je er een maken met wat doeken. 

8
00:00:43,060 --> 00:00:44,320
Daar gaan we. 

9
00:00:45,885 --> 00:00:48,855
Dus de zen-meditatie .. 

10
00:00:49,620 --> 00:00:53,420
Voor alles moet je duidelijk begrijpen dat .. 

11
00:00:53,720 --> 00:00:57,580
Zen-meditatie is een zittende meditatie. 

12
00:00:57,940 --> 00:01:00,820
Roerloos en stil. 

13
00:01:01,295 --> 00:01:04,425
En de bijzonderheid van deze meditatie is .. 

14
00:01:04,425 --> 00:01:07,445
Dat we voor de muur zitten. 

15
00:01:08,400 --> 00:01:11,040
Zoals nu zit ik voor de camera maar .. 

16
00:01:11,380 --> 00:01:15,900
Maar tijdens de meditatie zit je voor de muur. 

17
00:01:16,520 --> 00:01:21,940
We gaan dus kijken naar drie verschillende aspecten. 

18
00:01:24,860 --> 00:01:27,060
Maar je zult merken dat .. 

19
00:01:27,300 --> 00:01:32,680
De meditatie beoefent deze drie aspecten tegelijkertijd. 

20
00:01:33,420 --> 00:01:36,955
Het eerste thema is de fysieke houding. 

21
00:01:36,955 --> 00:01:38,435
Met het lichaam. 

22
00:01:39,175 --> 00:01:40,625
Het tweede thema is .. 

23
00:01:41,020 --> 00:01:42,900
De ademhaling. 

24
00:01:42,960 --> 00:01:45,580
Ho we ademen tijdens zenmeditatie. 

25
00:01:46,300 --> 00:01:48,720
En het derde aspect .. 

26
00:01:48,880 --> 00:01:50,320
De mentale houding. 

27
00:01:50,620 --> 00:01:56,180
Dit betekent: wat doen we terwijl we onbeweeglijk zitten. 

28
00:01:56,180 --> 00:01:59,080
En in stilte voor de muur. 

29
00:02:00,040 --> 00:02:04,560
De lichaamshouding 

30
00:02:06,160 --> 00:02:09,600
De lichaamshouding, de fysieke houding .. 

31
00:02:09,635 --> 00:02:12,585
De concrete houding van zenmeditatie. 

32
00:02:12,585 --> 00:02:17,340
Je zit dus op een kussen. 

33
00:02:17,760 --> 00:02:21,440
Niet aan de rand van het kussen maar wel bovenop. 

34
00:02:21,440 --> 00:02:24,640
En je moet opletten om je benen te kruisen. 

35
00:02:24,640 --> 00:02:28,160
Op zo’n manier dat je knieën kunnen .. 

36
00:02:28,220 --> 00:02:32,540
Val op of duw de vloer. 

37
00:02:32,540 --> 00:02:35,140
Of dat ze naar beneden gaan. 

38
00:02:35,440 --> 00:02:37,220
Hoe doe je dat? 

39
00:02:37,840 --> 00:02:41,120
Je neemt je been, zoals je kunt. 

40
00:02:41,380 --> 00:02:44,020
Je kunt het zo zeggen of .. 

41
00:02:44,685 --> 00:02:47,435
Minder stijf, je zou het hier kunnen plaatsen. 

42
00:02:47,435 --> 00:02:50,585
De positie die we medium lotus noemen. 

43
00:02:50,585 --> 00:02:54,980
En natuurlijk kun je ook het bovenbeen veranderen. 

44
00:02:55,240 --> 00:02:59,040
Plaats het hoe dan ook zodat je je het meest comfortabel voelt. 

45
00:02:59,260 --> 00:03:02,240
Onthoud dat we niet verhuizen. 

46
00:03:02,500 --> 00:03:07,120
We moeten dus een positie vinden die niet te moeilijk is. 

47
00:03:07,120 --> 00:03:10,600
Dat voorkomt dat we .. 

48
00:03:10,600 --> 00:03:13,940
En houden is stil. Zo.. 

49
00:03:14,160 --> 00:03:17,460
De knieën gaan naar de grond. 

50
00:03:17,655 --> 00:03:19,655
Hoe doen we dit? 

51
00:03:19,660 --> 00:03:23,800
Het bekken kantelt een beetje naar voren. 

52
00:03:24,240 --> 00:03:27,415
Wat gebeurt er dan? 

53
00:03:27,420 --> 00:03:31,000
Als het bekken zo naar voren is .. 

54
00:03:32,020 --> 00:03:36,180
Vanwege de zwaartekracht achter mijn rug .. 

55
00:03:36,180 --> 00:03:39,020
Of zoals wanneer ik een auto dirigeer .. 

56
00:03:39,020 --> 00:03:41,025
En zoals je kunt zien .. 

57
00:03:41,025 --> 00:03:43,860
Mijn knieën hangen in de lucht. 

58
00:03:43,860 --> 00:03:47,060
En ik wil dat mijn knieën naar beneden gaan. 

59
00:03:47,060 --> 00:03:51,700
In zen zeggen we: ’druk met je knieën op de aarde’. 

60
00:03:51,900 --> 00:03:54,940
En deze beweging, met de zwaartekracht .. 

61
00:03:55,040 --> 00:03:57,300
Naar beneden gaan .. 

62
00:03:57,300 --> 00:04:03,120
Met deze lichte kanteling van het bekken zien we .. 

63
00:04:03,460 --> 00:04:06,995
Er wordt niet ingegrepen met de spieren. 

64
00:04:06,995 --> 00:04:10,355
Op zoek naar het in profiel .. 

65
00:04:12,355 --> 00:04:15,315
Als je dit leuk vindt .. 

66
00:04:15,780 --> 00:04:19,160
Waar het bekken naar voren is gedraaid .. 

67
00:04:20,220 --> 00:04:21,700
Draai het een beetje .. 

68
00:04:22,720 --> 00:04:23,780
Gewoon hierheen. 

69
00:04:24,960 --> 00:04:27,040
Volkomen recht. 

70
00:04:27,040 --> 00:04:30,080
Als je zo van nature zit .. 

71
00:04:30,080 --> 00:04:31,740
Het zwaartepunt is .. 

72
00:04:31,740 --> 00:04:34,875
Komt meer naar voren .. 

73
00:04:34,875 --> 00:04:37,555
En je knieën zullen op de grond vallen. 

74
00:04:39,555 --> 00:04:42,595
Vanuit dit gekantelde bekken .. 

75
00:04:42,945 --> 00:04:45,725
Je maakt je rug recht. 

76
00:04:46,165 --> 00:04:48,885
Zo goed als je kunt. 

77
00:04:49,600 --> 00:04:52,100
In dit opzicht spreken we over .. 

78
00:04:52,100 --> 00:04:55,460
De lucht duwen .. 

79
00:04:55,460 --> 00:04:58,700
Met de bovenkant van ons hoofd. 

80
00:05:04,620 --> 00:05:06,680
In zen de ruggengraat .. 

81
00:05:07,560 --> 00:05:10,720
Start of stopt (zoals je wilt) hier. 

82
00:05:10,720 --> 00:05:14,020
Als we onze ruggengraat rechttrekken .. 

83
00:05:14,040 --> 00:05:17,100
Dat betekent, probeer het tot daar uit te rekken. 

84
00:05:17,380 --> 00:05:18,740
Dus we gaan niet .. 

85
00:05:19,260 --> 00:05:21,400
Mediteer met het hoofd .. 

86
00:05:22,335 --> 00:05:25,525
Vallen, noch achterover vallen. 

87
00:05:26,015 --> 00:05:28,255
De kin zou moeten zijn .. 

88
00:05:29,080 --> 00:05:31,240
Het meest rechtlijnige mogelijk. 

89
00:05:31,960 --> 00:05:35,920
We spreken over het houden van een rechte horizontale lijn. 

90
00:05:36,280 --> 00:05:38,320
De ogen.. 

91
00:05:39,285 --> 00:05:40,345
De mond.. 

92
00:05:41,015 --> 00:05:42,905
Een verticale lijn .. 

93
00:05:43,305 --> 00:05:45,125
Tussen de ogen, de mond en .. 

94
00:05:45,605 --> 00:05:46,695
De navel. 

95
00:05:48,075 --> 00:05:49,075
Goed. 

96
00:05:49,880 --> 00:05:53,840
Zodra ik zo recht mogelijk zit .. 

97
00:05:54,060 --> 00:05:56,365
De houding van de handen .. 

98
00:05:56,365 --> 00:05:58,495
De linkerhand 

99
00:05:59,765 --> 00:06:01,175
Vingers bij elkaar .. 

100
00:06:01,575 --> 00:06:04,725
Je legt in je rechterhand. 

101
00:06:04,920 --> 00:06:06,380
Vingers bij elkaar. 

102
00:06:06,600 --> 00:06:09,740
Omdat de vingers op elkaar liggen. 

103
00:06:11,765 --> 00:06:13,975
Een horizontale lijn met .. 

104
00:06:13,975 --> 00:06:17,115
De duimen raken elkaar. 

105
00:06:17,360 --> 00:06:21,280
Ze raken bijvoorbeeld aan om een ​​veer vast te houden. 

106
00:06:21,280 --> 00:06:22,480
Dus geen behoefte aan .. 

107
00:06:22,885 --> 00:06:24,925
Spierdruk. 

108
00:06:25,915 --> 00:06:29,125
En de binnenkant van onze handen. 

109
00:06:29,905 --> 00:06:33,295
We zetten de basis, hier .. 

110
00:06:33,675 --> 00:06:36,545
Min of meer in het midden van de buik. 

111
00:06:36,760 --> 00:06:40,900
Dus om deze houding te behouden .. 

112
00:06:41,620 --> 00:06:47,740
Je moet een stuk stof pakken 

113
00:06:48,140 --> 00:06:51,800
Dat zal je helpen om .. 

114
00:06:53,680 --> 00:06:56,675
Laat je handen los 

115
00:06:56,680 --> 00:07:00,920
Op een vrije manier tegen je buik .. 

116
00:07:01,180 --> 00:07:06,080
Zonder je verplicht te voelen om je vingers en schouders te gebruiken .. 

117
00:07:06,400 --> 00:07:09,600
Integendeel, je kunt alle spanningen loslaten. 

118
00:07:10,180 --> 00:07:12,780
De ellebogen zijn vrij .. 

119
00:07:12,995 --> 00:07:14,985
Ik ga mezelf profileren. 

120
00:07:15,355 --> 00:07:17,065
Zodat u goed kunt zien. 

121
00:07:17,485 --> 00:07:19,105
We heroveren 

122
00:07:20,125 --> 00:07:22,925
Rechtdoor.. 

123
00:07:23,820 --> 00:07:27,460
De handen, links bovenaan rechts. 

124
00:07:27,740 --> 00:07:31,000
De duimen. En de binnenkant van de handen .. 

125
00:07:31,000 --> 00:07:34,480
Aan de basis van de buik. 

126
00:07:35,140 --> 00:07:36,460
Ellebogen recht. 

127
00:07:37,985 --> 00:07:38,985
Goed. 

128
00:07:40,120 --> 00:07:42,540
Dit is de houding .. 

129
00:07:43,220 --> 00:07:47,000
Laten we zeggen ongeveer … 

130
00:07:47,640 --> 00:07:49,840
Van het materiaal, het lichaam. 

131
00:07:49,840 --> 00:07:53,240
Als ik mijn lichaam eenmaal heb neergelegd .. 

132
00:07:54,345 --> 00:07:57,225
In de meditatiehouding .. 

133
00:07:57,225 --> 00:08:00,455
Weet je nog, in stilte voor een muur. 

134
00:08:01,280 --> 00:08:02,680
Wat gebeurt er? 

135
00:08:02,960 --> 00:08:04,520
Wat er zal gebeuren is dat .. 

136
00:08:04,760 --> 00:08:08,160
Voor de muur gebeurt er niets interessants. 

137
00:08:08,640 --> 00:08:12,380
We spreken over het naar binnen richten van uw aandacht. 

138
00:08:14,360 --> 00:08:17,200
Dus over je blik tijdens zazen: 

139
00:08:17,975 --> 00:08:20,795
Ogen zijn half open .. 

140
00:08:21,085 --> 00:08:24,345
45 graden diagonaal .. 

141
00:08:24,345 --> 00:08:25,820
Naar beneden. 

142
00:08:26,000 --> 00:08:28,985
Het is alsof je je visie neemt .. 

143
00:08:28,985 --> 00:08:32,155
En het voor je neerleggen. 

144
00:08:32,245 --> 00:08:34,785
Er is niets interessants te zien. 

145
00:08:35,160 --> 00:08:38,920
Dan doen we hetzelfde met de andere zintuigen. 

146
00:08:39,160 --> 00:08:43,180
Als we het hebben over een stille meditatie … 

147
00:08:44,440 --> 00:08:48,780
Je hebt niets nodig. Geen muziek, niets. 

148
00:08:49,080 --> 00:08:51,580
Een innerlijke stilte. 

149
00:08:52,220 --> 00:08:55,680
Het is een stille meditatie, dus .. 

150
00:08:56,475 --> 00:08:58,315
U hoeft ook niet te praten. 

151
00:08:59,235 --> 00:09:00,235
Conclusie.. 

152
00:09:00,900 --> 00:09:02,880
Als ik onbeweeglijk ben .. 

153
00:09:02,880 --> 00:09:06,760
Mijn visie heeft geen zin .. 

154
00:09:06,760 --> 00:09:09,000
Mijn gehoor ook niet 

155
00:09:09,280 --> 00:09:10,820
of mijn woorden. 

156
00:09:10,980 --> 00:09:14,280
Dus de conclusie is dat … 

157
00:09:14,500 --> 00:09:17,920
Onze zintuigen waarmee we de wereld om ons heen voelen … 

158
00:09:17,920 --> 00:09:21,300
We zetten ze in ’vliegtuigmodus’. 

159
00:09:21,300 --> 00:09:23,980
We zouden het zo kunnen zeggen. In stand-by. 

160
00:09:24,300 --> 00:09:27,400
En dit is het principe van de houding .. 

161
00:09:27,400 --> 00:09:30,140
Door vast te stellen .. 

162
00:09:30,740 --> 00:09:33,675
Deze fysieke houding en .. 

163
00:09:33,675 --> 00:09:36,775
Om de buitenkant te snijden .. 

164
00:09:36,775 --> 00:09:39,915
Om het te snijden met onze gevoelens. 

165
00:09:40,060 --> 00:09:43,940
Dan kunnen we echt gaan mediteren. 

166
00:09:51,140 --> 00:09:55,440
Nu we een rustiger houding hebben .. 

167
00:09:56,040 --> 00:09:59,380
Ik sta voor een muur en mediteer .. 

168
00:09:59,685 --> 00:10:01,975
En met deze stilte .. 

169
00:10:02,495 --> 00:10:04,675
Ik neem kennis van het feit dat .. 

170
00:10:05,480 --> 00:10:09,440
Mijn hoofd is erg actief. 

171
00:10:09,780 --> 00:10:12,880
Dus ik houd er rekening mee dat hier .. 

172
00:10:13,120 --> 00:10:17,080
Ik heb veel gedachten .. 

173
00:10:17,080 --> 00:10:22,740
Dat verdwijnt met onze blik, je zou het zo kunnen zeggen. 

174
00:10:23,040 --> 00:10:26,240
Gedachten, visuele beelden .. 

175
00:10:26,245 --> 00:10:28,235
Klinkt .. 

176
00:10:28,865 --> 00:10:29,945
Veel dingen. 

177
00:10:31,420 --> 00:10:33,520
Dus wat gaan we doen .. 

178
00:10:33,720 --> 00:10:37,075
In onze zen meditatie is .. 

179
00:10:37,080 --> 00:10:40,205
Om deze gedachten los te laten. 

180
00:10:40,205 --> 00:10:43,180
Wat een fundamenteel punt is .. 

181
00:10:43,180 --> 00:10:45,500
Van de zen-meditatiehouding. 

182
00:10:45,640 --> 00:10:49,200
Terwijl ik zo ben in zazen .. 

183
00:10:49,200 --> 00:10:53,380
Ik breng mijn tijd niet door, nadenkend over .. 

184
00:10:53,380 --> 00:10:57,240
Wat dan ook, misschien interessant, misschien niet .. 

185
00:10:57,420 --> 00:10:59,340
Daar gaat het niet om. 

186
00:10:59,580 --> 00:11:03,700
Het gaat ongeveer hetzelfde als we ons lichaam hebben gegeven .. 

187
00:11:03,715 --> 00:11:06,685
In een rustige situatie .. 

188
00:11:06,825 --> 00:11:10,215
Ik zet mijn geest en mentale .. 

189
00:11:10,505 --> 00:11:13,415
Wat altijd onrustig is .. 

190
00:11:13,415 --> 00:11:16,035
Ik zal het zeggen .. 

191
00:11:16,035 --> 00:11:19,035
Op dezelfde manier.. 

192
00:11:19,040 --> 00:11:20,500
In een rustige staat van zijn. 

193
00:11:20,680 --> 00:11:23,640
Zoals simpel gezegd .. 

194
00:11:23,640 --> 00:11:26,620
Mijn gedachten laten gaan. 

195
00:11:27,760 --> 00:11:31,800
Onze gedachten komen van buitenaf. 

196
00:11:32,680 --> 00:11:35,200
Om dit aan te tonen; als we ons afvragen .. 

197
00:11:35,200 --> 00:11:39,040
Wat gaan we denken in 5 minuten of in een half uur .. 

198
00:11:39,305 --> 00:11:42,575
Of morgen? Niemand kan het weten .. 

199
00:11:43,440 --> 00:11:46,780
Dus zelfs als we slapen .. 

200
00:11:46,960 --> 00:11:50,080
We hebben dromen. Dus we gaan altijd door .. 

201
00:11:50,540 --> 00:11:53,440
Een mentale activiteit, bewust of niet. 

202
00:11:53,660 --> 00:11:57,000
Tijdens zenmeditatie is deze mentale activiteit .. 

203
00:11:57,165 --> 00:12:00,445
We gaan het naar beneden halen en kalmeren. 

204
00:12:00,760 --> 00:12:04,760
Zoals we zeggen; het is om je gedachten te laten passeren. 

205
00:12:05,100 --> 00:12:07,520
Maar dan zeg je, ok heel goed maar .. 

206
00:12:07,520 --> 00:12:08,900
Hoe doe je dit? 

207
00:12:09,080 --> 00:12:12,920
Dit kunnen we doen door onze ademhaling. 

208
00:12:13,240 --> 00:12:16,400
Dat brengt mij bij het derde punt; 

209
00:12:16,440 --> 00:12:19,400
Hoe adem je tijdens zazen? 

210
00:12:19,400 --> 00:12:23,920
Zen ademen 

211
00:12:26,740 --> 00:12:29,660
Ademen tijdens zenmeditatie. 

212
00:12:29,880 --> 00:12:32,560
Het is een buikademhaling. 

213
00:12:34,415 --> 00:12:36,265
We inspireren.. 

214
00:12:36,705 --> 00:12:39,985
We vullen onze longcapaciteit opnieuw aan 

215
00:12:39,985 --> 00:12:42,185
Met enige vrijgevigheid. 

216
00:12:42,925 --> 00:12:45,245
En we zullen ons concentreren .. 

217
00:12:45,245 --> 00:12:46,740
Vooral.. 

218
00:12:47,120 --> 00:12:49,900
Op onze vervaldatum 

219
00:12:50,200 --> 00:12:52,560
Om de lucht terug te sturen. 

220
00:12:52,740 --> 00:12:55,875
Deze manier van uitademen, beetje bij beetje .. 

221
00:12:55,875 --> 00:12:59,165
We proberen het lang te maken. 

222
00:12:59,225 --> 00:13:02,275
Lange tijd uitademen. 

223
00:13:02,345 --> 00:13:05,135
Dat wil zeggen, om de lucht uit te ademen .. 

224
00:13:05,140 --> 00:13:06,720
Door de neus .. 

225
00:13:07,000 --> 00:13:10,360
Zachtjes zonder geluid te maken. 

226
00:13:10,700 --> 00:13:13,080
We zijn in een stille meditatie. 

227
00:13:13,300 --> 00:13:16,240
We ademen de lucht uit door de neus .. 

228
00:13:17,160 --> 00:13:21,260
Probeert de uitademing te verlengen. 

229
00:13:21,620 --> 00:13:23,880
En aan het einde van je uitademing … 

230
00:13:24,625 --> 00:13:27,785
We ademen in en weer .. 

231
00:13:36,465 --> 00:13:39,565
We laten de lucht los en opnieuw … 

232
00:13:43,440 --> 00:13:46,980
Dus als je je concentratie richt op .. 

233
00:13:47,240 --> 00:13:50,420
het komen en gaan, je vestigt .. 

234
00:13:50,820 --> 00:13:53,500
je kalmeert je ademhaling .. 

235
00:13:54,540 --> 00:13:57,495
Je moet het rustig laten .. 

236
00:13:57,500 --> 00:14:02,600
Je moet de uitademing vergroten. 

237
00:14:03,160 --> 00:14:07,180
En dat zorgt ervoor dat je middenrif naar beneden gaat. 

238
00:14:07,480 --> 00:14:08,740
Het gaat naar beneden. 

239
00:14:08,980 --> 00:14:11,980
Het geeft een innerlijke massage aan de darmen. 

240
00:14:11,980 --> 00:14:13,980
Wat ze goed zal doen. 

241
00:14:14,160 --> 00:14:17,700
En langzaamaan voel je .. 

242
00:14:18,160 --> 00:14:20,820
Alsof er iets opblaast. 

243
00:14:21,145 --> 00:14:23,720
In de zone waar we onze handen hebben neergelegd. 

244
00:14:24,020 --> 00:14:27,700
Dus hier gebeuren de dingen. 

245
00:14:28,040 --> 00:14:30,520
Dus het is hier niet, maar het is .. 

246
00:14:31,900 --> 00:14:34,925
Daar beneden. 

247
00:14:34,925 --> 00:14:39,360
Ademen door de neus en in stilte. 

248
00:14:46,440 --> 00:14:49,900
De fysieke houding: kruis je benen. 

249
00:14:50,020 --> 00:14:52,860
De knieën zakken naar de grond. 

250
00:14:53,420 --> 00:14:56,140
Het bekken is iets naar voren gedraaid. 

251
00:14:56,700 --> 00:15:00,760
We maken onze rug zo goed mogelijk recht. 

252
00:15:01,160 --> 00:15:02,840
Tot aan de kruin van ons hoofd .. 

253
00:15:02,840 --> 00:15:08,040
We duwen de lucht met ons hoofd. 

254
00:15:08,540 --> 00:15:11,800
De linkerhand in de rechterhand. 

255
00:15:11,925 --> 00:15:13,925
De duimen in een horizontale lijn .. 

256
00:15:13,925 --> 00:15:17,700
Niet als vallei, niet als berg, als top. 

257
00:15:17,960 --> 00:15:20,100
Gewoon rechtdoor. 

258
00:15:20,100 --> 00:15:24,140
Wanneer u uw handen in contact brengt met de buik. 

259
00:15:24,500 --> 00:15:26,640
Je gebruikt iets onder .. 

260
00:15:26,665 --> 00:15:28,675
Om een ​​comfortabelere positie te hebben. 

261
00:15:29,125 --> 00:15:31,805
Je hebt je ellebogen recht. 

262
00:15:31,805 --> 00:15:32,885
Dit is onze fysieke houding. 

263
00:15:33,645 --> 00:15:37,065
Ten tweede onze mentale toestand. 

264
00:15:37,280 --> 00:15:41,760
We hebben ons van de buitenkant gesneden. 

265
00:15:42,160 --> 00:15:44,060
Van geluiden .. 

266
00:15:44,060 --> 00:15:46,760
We gaan niet mediteren met een radio. 

267
00:15:46,760 --> 00:15:48,755
Of met je mobiel aan. 

268
00:15:48,755 --> 00:15:51,635
Dus zet ze uit, maar dat is logisch. 

269
00:15:52,040 --> 00:15:54,620
We hebben alle geluiden weggelaten. 

270
00:15:54,620 --> 00:15:58,295
We brengen onze blik naar binnen en komen terug op .. 

271
00:15:58,300 --> 00:16:02,040
De waarnemer zijn van wat er van binnen gebeurt. 

272
00:16:02,120 --> 00:16:05,000
En wat gebeurt er binnenin? 

273
00:16:05,380 --> 00:16:07,180
We merken dat er een mentale activiteit is. 

274
00:16:07,660 --> 00:16:10,720
Gedachten branden … 

275
00:16:10,725 --> 00:16:12,665
We kunnen ze niet controleren. 

276
00:16:13,155 --> 00:16:16,415
De gedachten komen automatisch op. 

277
00:16:16,440 --> 00:16:21,000
Het gaat er dus niet om deze opwinding te voorkomen. 

278
00:16:21,380 --> 00:16:24,740
Beter gezegd, het gaat over .. 

279
00:16:24,740 --> 00:16:29,740
Niet springen in de trein van onze gedachten. 

280
00:16:29,960 --> 00:16:33,420
En we spreken over het laten passeren. 

281
00:16:33,500 --> 00:16:36,385
Hoe kunnen we ze laten passeren? 

282
00:16:36,385 --> 00:16:41,380
Allereerst met de concentratie van het hier en het nu .. 

283
00:16:41,540 --> 00:16:43,260
Op onze fysieke houding. 

284
00:16:43,795 --> 00:16:46,835
En met deze concentratie … 

285
00:16:46,840 --> 00:16:51,640
Hier en nu, op mijn ademhaling. 

286
00:16:52,080 --> 00:16:54,960
Ik ben me volledig bewust van mijn ademhaling .. 

287
00:16:55,265 --> 00:16:58,505
Van mijn buikademhaling. 

288
00:16:58,540 --> 00:17:02,580
Dus we ademen door de neus .. 

289
00:17:03,080 --> 00:17:04,520
We ademen .. 

290
00:17:05,095 --> 00:17:08,385
We proberen de uitademing te verlengen. 

291
00:17:08,900 --> 00:17:10,460
Elke keer.. 

292
00:17:10,700 --> 00:17:14,040
Zachter, langer. 

293
00:17:14,740 --> 00:17:17,735
En aan het einde van de uitademing beginnen we opnieuw. 

294
00:17:17,740 --> 00:17:19,800
Het komt en gaat als … 

295
00:17:20,080 --> 00:17:23,920
Een golf in de oceaan. De golven. 

296
00:17:24,360 --> 00:17:28,340
Maar … lang, lang, lang. 

297
00:17:29,180 --> 00:17:32,605
Als je je concentreert op het hier en nu .. 

298
00:17:32,605 --> 00:17:36,620
Op de punten van de houding 

299
00:17:36,920 --> 00:17:41,140
Dit betekent dat we tijdens de meditatie niet slapen. 

300
00:17:41,680 --> 00:17:44,880
En als je je tegelijkertijd concentreert .. 

301
00:17:45,040 --> 00:17:48,500
Over het vaststellen van je ademhaling .. 

302
00:17:48,680 --> 00:17:51,140
Op een geharmoniseerde en rustige manier. 

303
00:17:51,140 --> 00:17:53,780
En met een lange adem … 

304
00:17:53,940 --> 00:17:57,680
Je hebt geen tijd om in jezelf te denken of .. 

305
00:17:57,800 --> 00:18:00,060
Of in iets anders, hoe interessant het ook is. 

306
00:18:00,295 --> 00:18:03,365
Dus de conclusie is .. 

307
00:18:03,365 --> 00:18:05,665
We mediteren 20 minuten, een half uur, een uur. 

308
00:18:06,820 --> 00:18:08,540
Jij beslist. 

309
00:18:08,720 --> 00:18:12,380
Maar dit moment van meditatie, van concentratie … 

310
00:18:12,455 --> 00:18:15,635
Geeft u onmiddellijk voordelen. 

311
00:18:15,635 --> 00:18:17,255
Je zult jezelf voelen .. 

312
00:18:18,405 --> 00:18:21,405
Iets rustiger. 

313
00:18:21,405 --> 00:18:24,665
Iets lichter. 

314
00:18:24,665 --> 00:18:27,355
En nog beter .. 

315
00:18:27,355 --> 00:18:29,355
Je creëert een afstand tussen .. 

316
00:18:29,355 --> 00:18:32,445
jezelf en alle emoties die in je leven opkomen. 

317
00:18:32,885 --> 00:18:34,395
En dit.. 

318
00:18:34,785 --> 00:18:37,725
Het is echt een goed hulpmiddel 

319
00:18:38,135 --> 00:18:41,015
Meestal.. 

320
00:18:41,015 --> 00:18:44,055
Maar vooral nu .. 

321
00:18:44,060 --> 00:18:48,420
Die helft van onze bevolking is opgesloten. 

322
00:19:29,920 --> 00:19:34,960
Advies voor een beginner 

323
00:19:35,300 --> 00:19:40,380
In plaats van boos te worden of ophef te maken .. 

324
00:19:40,380 --> 00:19:45,380
In steeds meer gedachten die gedachten min of meer verspreiden .. 

325
00:19:45,380 --> 00:19:48,160
Van een lagere dimensie .. 

326
00:19:48,420 --> 00:19:49,840
Ga zitten. 

327
00:19:49,995 --> 00:19:53,325
Voor een muur. Adopteer deze meditatie. 

328
00:19:54,440 --> 00:19:58,320
Werk op deze manier aan jezelf. 

329
00:19:58,760 --> 00:20:00,620
En je zult snel merken … 

330
00:20:00,625 --> 00:20:02,925
Hoe goed zal het je doen. 

