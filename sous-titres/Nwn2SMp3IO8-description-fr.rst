Zen et sophrologie : les postures fondamentales et leur influence sur le cerveau

Maître Kosen a été invité au congrès 2013 de la Fédération des Écoles Professionnelles en Sophrologie (www.sophro.fr).

Il explique les quatre postures fondamentales du zen et leur influence sur le cerveau. Il revient aux sources de la relaxation dynamique. Il insiste sur l’importance des trois piliers de la posture, de la respiration et de l’état d’esprit dans les méditations bouddhistes, surtout la pratique du zen, zazen.
