1
00:00:13,650 --> 00:00:17,340
so I was born in Montpellier so in France 

2
00:00:17,340 --> 00:00:22,510
but now I live in a sound my parents are two different countries 

3
00:00:22,510 --> 00:00:26,950
suddenly I often travel between the only one in France and also other countries 

4
00:00:26,950 --> 00:00:32,169
because they see there I see there with their work but every summer we come 

5
00:00:32,169 --> 00:00:36,370
here the temple in the mountain since the machined when I had two 

6
00:00:36,370 --> 00:00:44,960
weeks i think we have full time growing up here i never always 

7
00:00:44,960 --> 00:00:50,390
come here what then me and my friends we know all the time yeah we’re going 

8
00:00:50,390 --> 00:00:55,130
keep coming here for even when you’re not an adult but you said to yourself 

9
00:00:55,130 --> 00:00:58,430
never that we were going to have to talk had not been it was not really 

10
00:00:58,430 --> 00:01:03,080
something we thought of people who come here they come here to 

11
00:01:03,080 --> 00:01:08,960
Zen me when I didn’t exist to see the legs for food to play 

12
00:01:08,960 --> 00:01:15,560
and suddenly I discovered there the sangha before Zen me and my opinion you have is on 

13
00:01:15,560 --> 00:01:19,850
also had another vivien friend who had one more wind than he and we 

14
00:01:19,850 --> 00:01:25,729
for us it’s a bit like putting it in the doot rue godot it made us 

15
00:01:25,729 --> 00:01:32,090
believe in magic he made us believe all kinds of things by 

16
00:01:32,090 --> 00:01:37,700
example a woman when was out in the forest he told us half done 

17
00:01:37,700 --> 00:01:42,530
dig in the ground and because there are always diamonds underground 

18
00:01:42,530 --> 00:01:48,140
no matter foot there is at least one diamond every meter from the consulate 

19
00:01:48,140 --> 00:01:54,680
dug and dug and dug and that in any case the two of us had both sides 

20
00:01:54,680 --> 00:01:59,270
hidden from him he took a diamond out of his pocket and pretended to get out of the 

21
00:01:59,270 --> 00:02:04,640
ground of a huge diamond borrowed from his mother then were dazzled 

22
00:02:04,640 --> 00:02:12,230
we have to have something like 30 like that and during kind much given after 

23
00:02:12,230 --> 00:02:15,860
sometimes I would dig a little cheeky of often if I find a 

24
00:02:15,860 --> 00:02:21,110
diamond if I ever realized that I may be ten years old 

25
00:02:21,110 --> 00:02:24,250
something if in my skin I tell you everything 

26
00:02:24,250 --> 00:02:28,130
james is different for everyone i believe my mom when the fatigue of 

27
00:02:28,130 --> 00:02:31,840
their fans she immediately felt that she was pharsal blue for the rest of her life 

28
00:02:31,840 --> 00:02:38,440
but I believe that for me after each Zen I begin to love more 

29
00:02:38,690 --> 00:02:43,950
on more or less well the second shot I tell myself that I would have 

30
00:02:43,950 --> 00:02:48,709
rapture besides it’s not over 

31
00:02:49,280 --> 00:02:54,740
I always knew the announcements I have always since in fact when my 

32
00:02:54,740 --> 00:02:59,840
mother was pregnant she already practiced zazen suddenly it’s a bit like 

33
00:02:59,840 --> 00:03:06,890
I was born suddenly I always knew the sangha in different temples 

34
00:03:06,890 --> 00:03:13,819
that I have a lot of memories I met friends or even huts 

35
00:03:13,819 --> 00:03:17,750
thing what has always been of different demands and when I was little I 

36
00:03:17,750 --> 00:03:21,470
I was not hiding I was just telling my friends yes I am going to the mountains and 

37
00:03:21,470 --> 00:03:25,850
everything but i never told them i’m going to do i go to zen i told them 

38
00:03:25,850 --> 00:03:28,400
not that because i tried once all the same 

39
00:03:28,400 --> 00:03:31,190
they all looked at me very strangely I said to myself hey 

40
00:03:31,190 --> 00:03:36,110
never speak again and but otherwise it never seemed odd to me it seemed to me 

41
00:03:36,110 --> 00:03:41,590
even more normal to go to go in fact I knew in advance if I came 

42
00:03:41,590 --> 00:03:46,010
I had to do a Zen a day but I was not at all against this idea 

43
00:03:46,010 --> 00:03:50,750
frankly that to my game I wanted to discover precisely it was my choice of 

44
00:03:50,750 --> 00:03:54,709
come here first to see the whole saga that i haven’t seen in two 

45
00:03:54,709 --> 00:04:01,820
three years then the place the food and all the memories that come back in 

46
00:04:01,820 --> 00:04:06,380
done as soon as i come the first time i do hazen i didn’t realize 

47
00:04:06,380 --> 00:04:09,850
account I was doing it actually gives it was pretty weird in the sense that 

48
00:04:09,850 --> 00:04:15,320
I did not perceive too many changes in fact I did not because I did not perceive 

49
00:04:15,320 --> 00:04:19,310
the usefulness of zen but it seems so normal to me that I haven’t seen any 

50
00:04:19,310 --> 00:04:23,660
change actually i find it strange but i like i feel like 

51
00:04:23,660 --> 00:04:28,490
the more lucky Jean prat the more I like and fluff then they me 

52
00:04:28,490 --> 00:04:31,090
look different 

53
00:04:33,470 --> 00:04:41,410
the lady validated in france youtube in 

54
00:04:45,910 --> 00:04:57,220
france both in simple and efficient 

55
00:05:05,560 --> 00:05:13,809
[Music] 

56
00:05:22,860 --> 00:05:25,929
[Music] 

57
00:07:03,510 --> 00:07:06,840
1 [Music] 

58
00:07:06,840 --> 00:07:08,840
e 

