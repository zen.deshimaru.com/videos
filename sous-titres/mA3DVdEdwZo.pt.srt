1
00:00:02,555 --> 00:00:04,935
Nós conversamos no kusen...

2
00:00:05,365 --> 00:00:08,125
aqui na Europa...

3
00:00:14,475 --> 00:00:17,625
...volição.

4
00:00:18,965 --> 00:00:21,545
Não é a vontade.

5
00:00:22,980 --> 00:00:27,800
A volição é algo que é acionada

6
00:00:42,860 --> 00:00:44,860
inconscientemente.

7
00:00:56,040 --> 00:00:58,740
Em seguida, expliquei...

8
00:01:00,840 --> 00:01:03,660
...aqui no Oeste...

9
00:01:11,580 --> 00:01:16,280
Antes de mais nada, bom dia a todos!

10
00:01:21,100 --> 00:01:23,915
Vamos compartilhar este zazen agora mesmo.

11
00:01:27,795 --> 00:01:29,465
com muito prazer

12
00:01:41,220 --> 00:01:46,880
Conseguimos organizar e praticar um acampamento de verão.

13
00:01:49,540 --> 00:01:53,520
apesar de eventos complicados

14
00:01:56,080 --> 00:01:59,870
Somos limitados em número, porque você não veio

15
00:02:05,140 --> 00:02:08,080
por causa do espaço de segurança

16
00:02:13,600 --> 00:02:19,900
Somos forçados a ser mais espaçados no Dojo

17
00:02:20,140 --> 00:02:25,820
Fizemos tudo com seriedade, seguimos as regras...

18
00:02:48,715 --> 00:02:52,160
Ela é uma das...

19
00:02:52,160 --> 00:02:54,860
características especiais do zen

20
00:02:58,940 --> 00:03:01,860
do que seguir a regra

21
00:03:02,380 --> 00:03:07,780
"Sim, mas eu não fui educado dessa maneira... Eu não sou assim, eu!"

22
00:03:12,340 --> 00:03:16,840
Em zen... nós seguimos a regra...

23
00:03:20,685 --> 00:03:23,535
A regra onde estamos.

24
00:03:33,200 --> 00:03:36,580
Então expliquei que a volição

25
00:03:39,480 --> 00:03:42,380
é Yin ou Yang

26
00:03:47,760 --> 00:03:52,500
Quando digo relaxe suas pernas, é a vontade de Yin...

27
00:03:53,840 --> 00:03:56,220
Nós deixamos ir

28
00:04:06,125 --> 00:04:08,765
Quando você solta, você desencadeia uma ressonância.

29
00:04:19,460 --> 00:04:23,340
a volição Yang...

30
00:04:23,640 --> 00:04:26,820
está empurrando a terra com seus joelhos.

31
00:04:40,600 --> 00:04:43,640
Eu contei a história

32
00:04:46,480 --> 00:04:48,380
e pensamentos

33
00:04:50,300 --> 00:04:52,760
Sobre o método Yang de Baso

34
00:04:54,760 --> 00:04:57,500
E o método Yin de Sekito

35
00:04:59,440 --> 00:05:03,380
Compartilharei este kusen com você

36
00:05:22,140 --> 00:05:25,300
É uma história muito famosa

37
00:05:27,420 --> 00:05:31,780
mas visto de um ângulo diferente do habitual.

38
00:05:33,960 --> 00:05:39,300
Estamos falando de Baso que já tinha recebido a transmissão do Dharma.

39
00:05:40,620 --> 00:05:42,680
De seu mestre

40
00:05:49,820 --> 00:05:54,400
Ele continuou a seguir os ensinamentos de seu mestre e sesshins.

41
00:06:03,270 --> 00:06:08,320
Ele tinha uma maneira engraçada

42
00:06:11,580 --> 00:06:18,180
Ele ia fazer zazen sozinho no Dojo mesmo que não fosse a hora...

43
00:06:19,740 --> 00:06:23,730
Ele sempre quis fazer zazen

44
00:06:28,320 --> 00:06:34,360
Um dia, seu mestre veio vê-lo enquanto ele estava sentado.

45
00:06:36,360 --> 00:06:38,400
e ele lhe pergunta

46
00:06:40,480 --> 00:06:42,920
Grande monge,

47
00:06:43,425 --> 00:06:46,645
-Baso era muito grande...

48
00:06:48,400 --> 00:06:53,700
O que você está procurando quando se senta em zazen?

49
00:06:57,060 --> 00:06:58,640
Em outras palavras:

50
00:06:58,860 --> 00:07:03,920
o que você está procurando? Você tem um propósito?

51
00:07:04,300 --> 00:07:08,560
Você tem um propósito além...

52
00:07:10,580 --> 00:07:15,000
que é mais importante do que apenas sentar-se?

53
00:07:20,040 --> 00:07:22,540
Esta história é muito bem conhecida

54
00:07:24,325 --> 00:07:28,875
É o aspecto Yang da volição...

55
00:07:31,705 --> 00:07:34,625
Em seguida, inventei os diálogos

56
00:07:41,045 --> 00:07:44,175
"- Você tem um propósito?"

57
00:07:44,565 --> 00:07:47,525
"-Sim, eu tenho um propósito", diz Baso.

58
00:07:50,435 --> 00:07:54,045
Eu quero me tornar Buda. Eu quero me tornar Buda.

59
00:07:56,345 --> 00:07:59,445
Então... (Suspiros)

60
00:07:59,535 --> 00:08:02,035
Eu estou visando

61
00:08:02,225 --> 00:08:05,195
o centro do alvo

62
00:08:08,525 --> 00:08:11,655
e eu o alcanço sempre.

63
00:08:21,920 --> 00:08:25,000
Este é o aspecto Yang do zazen.

64
00:08:25,240 --> 00:08:26,720
que mencionei ontem

65
00:08:33,620 --> 00:08:35,080
Eu disse

66
00:08:36,800 --> 00:08:42,360
como o Mestre Deshimaru nos ensinou quando éramos jovens...

67
00:08:49,120 --> 00:08:53,785
Ele estava causando muita tensão no dojo com seu kyosaku.

68
00:08:55,045 --> 00:08:57,875
É o método Yang

69
00:08:59,385 --> 00:09:02,385
Assim que alguém se mudou

70
00:09:03,105 --> 00:09:05,395
ou adormeceu

71
00:09:06,385 --> 00:09:09,345
ele o atingiu

72
00:09:09,545 --> 00:09:12,765
Estávamos no acampamento de verão

73
00:09:12,875 --> 00:09:15,405
Estava muito quente

74
00:09:15,915 --> 00:09:17,455
estávamos encharcados.

75
00:09:19,765 --> 00:09:22,925
Havia uma poça no tatami depois do zazen.

76
00:09:32,260 --> 00:09:37,340
"Estou sentado para me tornar um Buda, e ponto final. Coloco toda minha energia nisso.

77
00:09:40,910 --> 00:09:43,550
É o método de Deshimaru.

78
00:09:45,080 --> 00:09:48,490
Praticar com todas as forças para o prazer

79
00:09:51,435 --> 00:09:54,475
Isso é o que Baso estava fazendo

80
00:10:00,980 --> 00:10:03,980
Portanto, o método Yin,

81
00:10:05,960 --> 00:10:10,240
O que eu queria dizer é que Baso não tinha dúvidas

82
00:10:13,420 --> 00:10:19,180
Ele não se importava se tinha um propósito ou não, se era Mushotoku ou não.

83
00:10:24,460 --> 00:10:28,020
Foi volição. Ele tinha o desejo, ele tinha a vontade.

84
00:10:37,535 --> 00:10:40,345
disse Sensei:

85
00:10:41,185 --> 00:10:44,415
é melhor fazer zazen menos tempo

86
00:10:48,985 --> 00:10:51,825
mas fazendo um zazen forte, como Baso...

87
00:10:58,875 --> 00:11:02,865
Em vez de fazer zazen por muito tempo, durante todo o dia,

88
00:11:06,525 --> 00:11:10,005
e para dormir ou pensar

89
00:11:18,425 --> 00:11:19,425
20 minutos.

90
00:11:19,565 --> 00:11:22,445
com todas as suas forças

91
00:11:24,055 --> 00:11:26,785
é muito eficaz

92
00:11:31,355 --> 00:11:33,725
Quando você ama zazen,

93
00:11:35,995 --> 00:11:38,475
por diversão...

94
00:11:42,375 --> 00:11:45,085
Não somos proselitistas.

95
00:11:56,515 --> 00:12:01,235
Estamos apenas compartilhando, simplesmente por diversão.

96
00:12:41,325 --> 00:12:44,085
Voltemos ao exemplo do Yin,

97
00:12:44,205 --> 00:12:45,865
soltem-se

98
00:12:47,820 --> 00:12:51,610
quando eu solto as pernas, liberta toda a pélvis.

99
00:12:54,180 --> 00:12:57,340
Sinto-me como se não tivesse peso.

100
00:13:10,580 --> 00:13:13,040
O exemplo dado

101
00:13:14,165 --> 00:13:18,815
é chegar ao momento tão esperado em que vamos tirar uma soneca

102
00:13:26,135 --> 00:13:29,145
Onde você larga tudo

103
00:13:30,775 --> 00:13:33,425
para dormir

104
00:13:35,935 --> 00:13:39,845
O exemplo da sesta é obviamente uma metáfora...

105
00:13:42,045 --> 00:13:45,005
que se refere ao zazen

106
00:13:59,545 --> 00:14:02,295
No entanto, 7 séculos a.C.

107
00:14:09,240 --> 00:14:12,240
um cientista chamado Pythagoras

108
00:14:14,595 --> 00:14:16,545
disse:

109
00:14:18,960 --> 00:14:21,300
prestar atenção à noite

110
00:14:27,500 --> 00:14:33,120
já que você não pode controlar a noite

111
00:14:34,600 --> 00:14:39,980
Tenha cuidado ao adormecer.

112
00:14:43,600 --> 00:14:48,240
Não adormeça sem verificar esse último momento antes de adormecer.

113
00:15:02,695 --> 00:15:05,735
Zazen é composto por estes dois aspectos

114
00:15:05,885 --> 00:15:08,785
yin e yang

115
00:15:11,440 --> 00:15:16,260
que foram expressos por sua vez pelos grandes mestres Baso e Sekito

116
00:15:21,370 --> 00:15:36,380
Um pode ser chamado de "Matem o Buda" e o outro de "Libertem o Buda".

117
00:15:41,680 --> 00:15:49,020
O grande mestre Sekito foi seu contemporâneo. Eles eram monges que viviam ao mesmo tempo.

118
00:15:52,300 --> 00:15:56,300
Baso e Sekito eram do mesmo período.

119
00:16:00,980 --> 00:16:04,560
Muitas vezes os discípulos de um iam para o outro.

120
00:16:10,570 --> 00:16:14,000
Naquela época, Sekito ainda não tinha discípulos.

121
00:16:17,455 --> 00:16:20,865
Ele vivia na floresta... nas montanhas...

122
00:16:22,445 --> 00:16:24,945
Ele se construiu a si mesmo

123
00:16:25,385 --> 00:16:28,405
uma pequena cabana

124
00:16:34,335 --> 00:16:36,915
com madeira e palha

125
00:16:44,315 --> 00:16:48,895
na mesma montanha onde o Mestre Nangaku havia ensinado...

126
00:16:54,435 --> 00:16:57,825
Sekito diz:

127
00:16:58,545 --> 00:17:01,475
"Eu me sinto protegido nesta cabana".

128
00:17:02,865 --> 00:17:07,355
Eu mesmo o construí. É a minha única riqueza.

129
00:17:10,965 --> 00:17:13,395
Mas quando eu terminar minha refeição,

130
00:17:16,045 --> 00:17:19,015
Eu preparo calmamente o momento da sesta.

131
00:17:23,655 --> 00:17:26,825
e eu digo que todos os dias

132
00:17:29,295 --> 00:17:34,525
e novamente todos os dias

133
00:17:38,875 --> 00:17:41,665
A experiência da refeição e da soneca,

134
00:17:44,135 --> 00:17:46,955
é um poder dos patriarcas

135
00:17:50,035 --> 00:17:53,055
aquele que ainda não terminou sua refeição,

136
00:17:56,345 --> 00:18:03,075
ainda não foi concluída.

137
00:18:08,215 --> 00:18:10,015
Ele ainda não está satisfeito.
