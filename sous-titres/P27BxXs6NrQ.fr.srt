1
00:00:00,000 --> 00:00:02,200
Beaucoup de gens s’intéressent à la méditation … 

2
00:00:02,200 --> 00:00:04,220
… et recherchez des informations sur Youtube. 

3
00:00:04,420 --> 00:00:07,860
Le problème est que nous n’en voyons pas beaucoup. Le zen de Maître Deshimaru … 

4
00:00:07,980 --> 00:00:10,900
… ni le Kosen Sangha. 

5
00:00:11,100 --> 00:00:13,180
Beaucoup de pratiquants talentueux … 

6
00:00:13,360 --> 00:00:16,300
… fait des chaînes Kosen Sangha. 

7
00:00:16,520 --> 00:00:18,660
Mais le grand public se demande: 

8
00:00:18,940 --> 00:00:21,080
Quelle est la bonne? 

9
00:00:21,300 --> 00:00:23,600
Maintenant, nous allons tout enchaîner: 

10
00:00:23,820 --> 00:00:26,840
bit.ly/chosen 

11
00:00:28,420 --> 00:00:31,160
Donc, si vous voulez présenter zazen au plus grand nombre de personnes possible: 

12
00:00:32,260 --> 00:00:33,980
Abonnez-vous à la chaîne. 

13
00:00:33,980 --> 00:00:35,220
Regardez les vidéos. 

14
00:00:35,220 --> 00:00:36,520
Veuillez commenter. 

15
00:00:36,700 --> 00:00:38,700
Et n’hésitez pas à les aimer! 

