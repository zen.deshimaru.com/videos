1
00:00:00,000 --> 00:00:05,742
Mein Meister hat mich als Anfänger unterrichtet (ich bin immer noch …). 

2
00:00:05,842 --> 00:00:07,832
"Zazen ist Satori." 

3
00:00:07,932 --> 00:00:18,497
Zu der Zeit, in den 1970er Jahren, war das Wort "Satori" sehr interessant, wir wollten wissen, was Satori ist, eine Art Erleuchtung. 

4
00:00:18,597 --> 00:00:22,731
Die Leute wollten Zazen machen, um Satori zu bekommen. 

5
00:00:22,831 --> 00:00:31,329
Und Sensei lehrte, dass es die Haltung Buddhas selbst war, dass es draußen keine Satori gibt. 

6
00:00:31,429 --> 00:00:35,793
Das ist die Lehre von Meister Deshimaru. Er sagte immer: 

7
00:00:35,893 --> 00:00:39,950
"Satori ist die Rückkehr zu normalen Umständen." 

8
00:00:40,050 --> 00:00:41,529
Wir jungen Leute waren enttäuscht. 

9
00:00:41,629 --> 00:00:46,520
Normale Bedingungen sind langweilig. 

10
00:00:46,620 --> 00:00:49,388
Wir rauchten gern Gras, nahmen LSD … 

11
00:00:49,490 --> 00:00:53,471
Wir waren nicht an normalen Umständen interessiert. 

12
00:00:53,571 --> 00:00:58,853
Jetzt, im Jahr 2018, sind normale Bedingungen sehr gefragt. 

13
00:00:58,960 --> 00:01:02,400
Normale Jahreszeiten, normales Klima, normale Luftqualität. 

14
00:01:02,400 --> 00:01:04,400
Ein normaler Ozean, normale Fauna und Flora. 

15
00:01:08,380 --> 00:01:10,980
Normalität wird teuer. 

16
00:01:11,088 --> 00:01:13,155
Vergiss niemals diese Worte: 

17
00:01:13,255 --> 00:01:15,705
"Zazen selbst ist Satori." 

18
00:01:15,805 --> 00:01:19,809
Bei Satori geht es darum, zur richtigen Zeit am richtigen Ort zu sein. 

19
00:01:19,909 --> 00:01:21,516
Es ist sehr schwierig. 

20
00:01:21,616 --> 00:01:27,840
Wenn Sie die Buddha-Pose einnehmen, sind Sie zur richtigen Zeit am richtigen Ort. 

21
00:01:27,940 --> 00:01:32,939
Du bist normal. Sie synchronisieren sich mit der kosmischen Ordnung. 

22
00:01:33,039 --> 00:01:34,264
Es ist mechanisch. 

23
00:01:34,364 --> 00:01:38,498
Mit seinem Skelett einer abnormalen Person, eines Verursachers … 

24
00:01:38,598 --> 00:01:42,831
Wenn Sie die Haltung des Buddha einnehmen, ist sie mechanisch. 

25
00:01:42,931 --> 00:01:47,854
Wir kehren zum universellen kosmischen Rhythmus zurück. Das ist Satori. 

26
00:01:47,954 --> 00:01:50,503
Gleichzeitig ist es sehr einfach. 

27
00:01:50,603 --> 00:01:59,507
Aber wenn Sie so weit von den normalen Bedingungen entfernt sind wie wir, hat dies einen enormen Wert. 

28
00:01:59,607 --> 00:02:01,521
Und was ist Zen? 

29
00:02:01,621 --> 00:02:04,017
Es nimmt die genaue Position ein. 

30
00:02:04,117 --> 00:02:07,945
Es ist wie ein Code, um eine Geheimtür zu öffnen. 

31
00:02:08,045 --> 00:02:10,035
Sie sehen es in den Filmen. 

32
00:02:10,135 --> 00:02:16,359
Sie müssen dies drücken, Sie müssen den Lichtstrahl an die richtige Stelle bringen. 

33
00:02:16,459 --> 00:02:17,990
Und die Tür öffnet sich. 

34
00:02:18,090 --> 00:02:21,229
Es ist dasselbe, wenn Sie Ihre Position ausrichten. 

35
00:02:25,280 --> 00:02:29,320
Du bist wunderschön auf den Pins. Auf dem Kissen im Lotus. 

36
00:02:29,720 --> 00:02:32,400
Die Position des Buddha ist im Lotus. 

37
00:02:32,860 --> 00:02:37,700
Wenn Sie den Lotus nicht machen können, machen Sie einen halben Lotus. 

38
00:02:37,980 --> 00:02:40,580
Aber der Lotus hat nichts damit zu tun. 

39
00:02:41,060 --> 00:02:44,120
Es kehrt die Polaritäten um. 

40
00:02:44,760 --> 00:02:52,720
Sobald Sie den Lotus nehmen, anstatt ihn zu erzwingen, anstatt ihn zu verletzen, lassen Sie los. 

41
00:02:53,480 --> 00:02:56,380
Die Haltung des Buddha ist präzise. 

42
00:02:56,940 --> 00:03:03,020
Wenn Sie genau die richtigen Klamotten anziehen. 

43
00:03:03,140 --> 00:03:08,240
Atme gut, heile deinen Körper und Geist. 

44
00:03:08,240 --> 00:03:14,360
Es ist wunderschön und sehr einfach. 

45
00:03:14,860 --> 00:03:17,840
Es ist keine besondere Bedingung. 

46
00:03:18,520 --> 00:03:23,020
Sie müssen nicht denken: "Ich habe mehr Satori als die anderen." 

47
00:03:23,400 --> 00:03:26,080
Es ist einfach normal. 

48
00:03:26,600 --> 00:03:32,940
Wenn es etwas Ungewöhnliches im Sonnensystem gibt, furz alles. 

49
00:03:33,280 --> 00:03:38,720
So ist alles genau zur richtigen Zeit am richtigen Ort und in einer Bewegung. 

50
00:03:38,920 --> 00:03:42,660
Das Geheimnis ist klar: Wir kennen die Methode. 

51
00:03:42,940 --> 00:03:45,700
Sie sollten sich die Mühe machen. 

52
00:03:46,100 --> 00:03:48,460
Es braucht Übung. 

53
00:03:48,740 --> 00:03:52,100
Es ist außergewöhnlich, die kosmische Ordnung verfehlen zu können. 

54
00:03:52,300 --> 00:03:56,000
Sensei wiederholte immer wieder: "Folge der kosmischen Ordnung." 

55
00:03:56,160 --> 00:03:58,080
Er schrie mich an: 

56
00:03:58,320 --> 00:04:01,500
"STIEFAN, WARUM BEFOLGEN SIE DIE KOSMISCHE BESTELLUNG NICHT? SIE MÜSSEN DER KOSMISCHEN BESTELLUNG BEFOLGEN!" 

