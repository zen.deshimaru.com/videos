1
00:00:09,675 --> 00:00:13,085
Hola. Hoy, me gustaría explicarles

2
00:00:13,280 --> 00:00:15,940
cómo practicar una caminata de meditación

3
00:00:16,760 --> 00:00:21,800
que practicamos en el Zen, este paseo se llama kin-hin.

4
00:00:22,180 --> 00:00:25,640
Es un paseo bastante corto,

5
00:00:25,920 --> 00:00:30,740
que uno practica entre dos momentos de meditación sentada...

6
00:00:30,920 --> 00:00:32,220
zazen

7
00:00:32,460 --> 00:00:35,380
como expliqué en un video anterior.

8
00:00:35,760 --> 00:00:39,220
En general, una sesión de meditación completa

9
00:00:39,440 --> 00:00:43,380
incluye una primera parte de meditación sentada, zazen,

10
00:00:43,660 --> 00:00:49,800
y luego este pequeño paseo de meditación que voy a explicarte...

11
00:00:50,060 --> 00:00:52,360
llamado kin-hin,

12
00:00:52,640 --> 00:00:54,820
y después de este kin-hin

13
00:00:54,820 --> 00:00:57,580
volvemos a donde está nuestro cojín de meditación.

14
00:00:57,580 --> 00:01:00,600
en segundo lugar

15
00:01:00,600 --> 00:01:04,460
de zazen, de cara a la pared, en silencio.

16
00:01:12,340 --> 00:01:15,640
La caminata de meditación, kin-hin,

17
00:01:16,445 --> 00:01:17,915
es un paseo,

18
00:01:18,480 --> 00:01:21,380
vas a poner tus pies

19
00:01:21,760 --> 00:01:26,320
si miras la posición de mis pies así,

20
00:01:26,600 --> 00:01:30,900
es decir, están espaciados, más o menos, el ancho de tu puño,

21
00:01:31,780 --> 00:01:36,100
sin distorsionar la pelvis.

22
00:01:37,580 --> 00:01:41,180
Y vas a tomar medidas

23
00:01:41,360 --> 00:01:44,480
medio pie de ancho

24
00:01:44,800 --> 00:01:47,540
un pie es más o menos 30-33 cm.

25
00:01:47,880 --> 00:01:51,160
así que serán pasos de seis pulgadas más o menos.

26
00:01:51,660 --> 00:01:54,480
Si estás mirando,

27
00:01:54,680 --> 00:01:57,660
Verás, estoy dando un paso de medio metro.

28
00:01:58,040 --> 00:02:03,580
Así que en algún momento tengo una pierna arriba y otra abajo...

29
00:02:03,840 --> 00:02:12,240
la pierna de atrás pasará por delante dando un paso de medio pie de ancho.

30
00:02:12,240 --> 00:02:15,880
Asegúrate de que tus pies estén

31
00:02:16,240 --> 00:02:18,560
recto y no así.

32
00:02:27,700 --> 00:02:30,875
Ahora la postura de la mano.

33
00:02:30,875 --> 00:02:33,685
Mientras caminamos, como acabo de explicar,

34
00:02:33,685 --> 00:02:36,820
también tenemos una actividad con nuestras manos.

35
00:02:37,520 --> 00:02:40,300
Recuerdas, la postura de meditación Zen,

36
00:02:40,620 --> 00:02:42,520
teníamos las manos así.

37
00:02:43,140 --> 00:02:45,660
Estoy quitando mi mano derecha,

38
00:02:45,980 --> 00:02:49,180
Doblo el pulgar de mi mano izquierda

39
00:02:49,500 --> 00:02:55,000
y lo agarro con todos mis dedos pegados,

40
00:02:55,320 --> 00:03:01,700
Dejé que la raíz de mi pulgar izquierdo sobresaliera.

41
00:03:02,900 --> 00:03:05,140
Mi mano derecha que envolvió

42
00:03:05,480 --> 00:03:08,765
envuelve la mano izquierda

43
00:03:08,765 --> 00:03:14,600
dedos juntos, pulgares juntos, todos pegados...

44
00:03:14,800 --> 00:03:18,640
Doblo mis muñecas ligeramente

45
00:03:19,380 --> 00:03:22,320
Verás, la raíz de mi pulgar izquierdo está saliendo...

46
00:03:22,940 --> 00:03:25,980
y esta raíz

47
00:03:26,340 --> 00:03:29,700
Voy a aplicarlo aquí, en la base del esternón.

48
00:03:30,320 --> 00:03:35,180
Todos los seres humanos, en la base del esternón tienen dos o tres pequeños agujeros.

49
00:03:35,720 --> 00:03:44,200
Aquí es donde se posará, haciendo contacto entre la raíz del pulgar izquierdo y el plexo.

50
00:03:44,780 --> 00:03:55,440
Como pueden ver, doblo mis muñecas ligeramente, para que mis antebrazos estén en la misma línea.

51
00:03:55,860 --> 00:03:59,160
No tengo un brazo sobre el otro.

52
00:03:59,600 --> 00:04:03,020
Las palmas de mis manos están paralelas al suelo,

53
00:04:03,540 --> 00:04:07,340
Quiero decir, yo no soy así.

54
00:04:07,680 --> 00:04:11,140
No tengo los brazos caídos.

55
00:04:11,520 --> 00:04:16,700
y no levanto los brazos con fuerza física cuando levanto los codos.

56
00:04:17,800 --> 00:04:21,040
¿Qué está pasando? ¿Qué está pasando?

57
00:04:21,560 --> 00:04:30,260
Recuerda que cuando exhalas, haces presión, todo el peso de tu cuerpo en la pierna delantera.

58
00:04:30,560 --> 00:04:33,640
Durante esta fase,

59
00:04:34,200 --> 00:04:40,720
de la misma manera, vamos a juntar las manos,

60
00:04:41,080 --> 00:04:49,220
aplicar una ligera presión y al mismo tiempo presionar la raíz del pulgar izquierdo contra el plexo.

61
00:04:49,580 --> 00:04:54,640
Obviamente, con delicadeza, no se trata de violencia.

62
00:04:54,840 --> 00:04:59,700
Verás que si estás demasiado bajo, te darás con un punto en el estómago.

63
00:05:00,080 --> 00:05:04,340
pronto te darás cuenta de que no es agradable

64
00:05:04,820 --> 00:05:09,720
así que naturalmente te levantarás para encontrar la postura correcta.

65
00:05:13,600 --> 00:05:16,880
Cuando inspiro, pongo mi pierna delante,

66
00:05:17,160 --> 00:05:18,460
todo está suelto.

67
00:05:18,860 --> 00:05:21,880
Todo está en posición, pero está suelto.

68
00:05:22,300 --> 00:05:26,300
Durante la fase de exhalación,

69
00:05:27,080 --> 00:05:34,440
Pongo el peso en la pierna delantera, presiono ligeramente contra el plexo, y ambas manos una contra la otra.

70
00:05:35,080 --> 00:05:40,920
Me pongo de perfil, como puedes ver, es como durante el zazen,

71
00:05:41,200 --> 00:05:49,180
Mantengo la columna vertebral lo más recta posible, el cuello estirado, la barbilla metida...

72
00:05:49,520 --> 00:05:53,840
mi mirada 45 grados en diagonal hacia abajo,

73
00:05:54,460 --> 00:05:59,560
Estoy totalmente concentrado, aquí y ahora, en lo que estoy haciendo.

74
00:06:00,200 --> 00:06:03,180
Y lo que hago es un paseo meditativo.

75
00:06:03,580 --> 00:06:08,420
No estás siguiendo tus pensamientos, no estás mirando hacia afuera,

76
00:06:08,640 --> 00:06:14,260
continúas con la concentración de zazen, la concentración de la postura sentada.

77
00:06:22,020 --> 00:06:27,600
El ritmo de la caminata va a ser el ritmo de tu respiración.

78
00:06:28,120 --> 00:06:31,380
Recuerdas, durante la meditación sentada,

79
00:06:31,760 --> 00:06:35,840
estamos enfocados aquí y ahora en los puntos físicos de la postura,

80
00:06:36,500 --> 00:06:41,380
el estado mental, la actitud mental, dejas que tus pensamientos se vayan,

81
00:06:41,640 --> 00:06:44,660
no les damos servicio

82
00:06:44,920 --> 00:06:48,000
y nos centramos aquí y ahora en nuestra respiración.

83
00:06:48,340 --> 00:06:51,560
estableciendo una armoniosa ida y vuelta

84
00:06:51,960 --> 00:06:55,780
y centrándose especialmente en la exhalación...

85
00:06:57,160 --> 00:07:01,260
cada vez más largo, más profundo y más profundo,

86
00:07:01,580 --> 00:07:04,540
abdominal, que va hasta la parte baja del abdomen.

87
00:07:05,860 --> 00:07:15,440
La respiración y la actitud mental son las mismas ya sea que estemos sentados, frente a la pared, en silencio, inmóviles, en zazen.

88
00:07:16,280 --> 00:07:24,660
o hacer esta meditación mientras se camina. Lo único que cambia es que nos vamos a mudar.

89
00:07:30,320 --> 00:07:33,880
Camina según el ritmo de tu respiración.

90
00:07:34,160 --> 00:07:41,660
Durante la inspiración pasas una pierna por delante

91
00:07:42,520 --> 00:07:45,000
medio pie

92
00:07:45,180 --> 00:07:50,220
y durante la fase de expiración,

93
00:07:52,440 --> 00:07:55,420
a través de la nariz, en silencio,

94
00:07:56,100 --> 00:07:59,820
vas a colocar gradualmente todo el peso del cuerpo

95
00:08:00,100 --> 00:08:03,240
en la pata delantera

96
00:08:03,520 --> 00:08:06,880
la pierna delantera está bien estirada

97
00:08:07,180 --> 00:08:11,480
las plantas de los pies completamente en contacto con el suelo

98
00:08:12,080 --> 00:08:15,680
la pierna que está detrás de ella, manténgase suelta,

99
00:08:16,180 --> 00:08:20,440
pero también toda la planta del pie en contacto con el suelo.

100
00:08:20,740 --> 00:08:24,320
Si ves el pie, no estoy con el talón en alto.

101
00:08:24,700 --> 00:08:29,860
Tengo los talones de ambos pies apoyados en el suelo.

102
00:08:30,480 --> 00:08:33,280
Así que, durante la fase de exhalación,

103
00:08:33,740 --> 00:08:40,180
Poco a poco estoy cambiando todo el peso de mi cuerpo en esta pierna delantera...

104
00:08:40,460 --> 00:08:43,520
hasta la raíz del dedo gordo del pie

105
00:08:43,920 --> 00:08:46,820
vagamente

106
00:08:47,380 --> 00:08:51,380
es sólo el peso que me hace golpear el suelo.

107
00:08:51,760 --> 00:08:57,960
Como si estuvieras en un suelo blando como la arena o la arcilla...

108
00:08:58,280 --> 00:09:05,800
e ibas a querer dejar tu marca en la arena o la arcilla.

109
00:09:06,700 --> 00:09:10,420
Te lo mostraré de perfil.

110
00:09:10,920 --> 00:09:14,580
Inspiro y pongo mi pierna encima,

111
00:09:15,100 --> 00:09:20,280
Exhalo, todo el peso de mi cuerpo en mi pierna delantera.

112
00:09:20,640 --> 00:09:23,420
Estoy al final de mi exhalación,

113
00:09:23,680 --> 00:09:28,940
Inspiro y pongo mi pierna detrás del frente...

114
00:09:29,960 --> 00:09:31,120
Expiro

115
00:09:32,620 --> 00:09:37,340
todo el peso de mi cuerpo en mi pierna delantera.

116
00:09:37,780 --> 00:09:44,240
Fin de la exhalación, inspiro, la pierna trasera va delante.

117
00:09:51,240 --> 00:09:55,380
Inténtalo y verás que el paseo de los kin-hin

118
00:09:55,640 --> 00:10:01,920
es un paseo que puedes usar en muchas circunstancias de tu vida.

119
00:10:02,360 --> 00:10:12,360
5 minutos de espera, estás estresado y bueno, practica esto, 3-4 minutos de esta caminata.

120
00:10:12,660 --> 00:10:18,980
y verás como te calmas, te sientes más en sintonía contigo mismo.

121
00:10:19,860 --> 00:10:21,240
Buena práctica
