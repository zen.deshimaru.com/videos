1
00:00:13,360 --> 00:00:15,750
Una storia raccontata dal Maestro Deshimaru

2
00:00:17,320 --> 00:00:19,320
che si chiama "Camminare in montagna".

3
00:00:19,980 --> 00:00:21,980
Quindi già solo il titolo,

4
00:00:22,560 --> 00:00:24,080
è meraviglioso,

5
00:00:24,609 --> 00:00:30,360
per poter camminare in montagna. Un maestro andava a fare una passeggiata in montagna e quando tornava

6
00:00:31,179 --> 00:00:33,179
gli chiese uno dei suoi discepoli

7
00:00:33,700 --> 00:00:35,700
"Maestro, dove sei andato a fare una passeggiata?

8
00:00:35,980 --> 00:00:40,420
"Nella montagna" rispose il maestro e il discepolo insistette "Ma che modo

9
00:00:40,900 --> 00:00:43,380
hai preso, cosa hai visto?"

10
00:00:45,260 --> 00:00:52,940
Il maestro rispose: "Ho seguito l’odore dei fiori e ho vagato lungo i giovani germogli di primavera".

11
00:00:55,480 --> 00:00:57,480
Il Maestro Deshimaru continua

12
00:00:58,540 --> 00:01:02,180
"Bisogna lasciarsi guidare dal Dharma del Buddha...

13
00:01:02,380 --> 00:01:03,820
fiducia

14
00:01:04,140 --> 00:01:08,040
con erbe e fiori che crescono senza meta,

15
00:01:08,600 --> 00:01:10,220
senza egoismo,

16
00:01:10,440 --> 00:01:15,640
naturalmente, inconsciamente. Questa risposta viene dalla fonte della saggezza

17
00:01:16,900 --> 00:01:20,560
la vera saggezza deve essere creata al di là della conoscenza...

18
00:01:21,100 --> 00:01:26,240
e la memoria". È un insegnamento piuttosto potente.

19
00:01:26,700 --> 00:01:29,160
In un kusen,

20
00:01:29,340 --> 00:01:37,040
L’insegnamento dato nel dojo, il Maestro Kosen insegna: "La filosofia di Shikantaza,

21
00:01:37,900 --> 00:01:40,580
la filosofia del "solo zazen".

22
00:01:40,800 --> 00:01:47,380
deve guidare la nostra vita è la vera essenza del Buddismo, il metodo più perfetto

23
00:01:48,249 --> 00:01:50,699
zazen stesso è il satori, il risveglio.

24
00:01:51,520 --> 00:01:53,520
Stiamo commettendo un errore.

25
00:01:54,159 --> 00:01:56,368
se pensi che ci sia uno zazen prima

26
00:01:57,100 --> 00:02:02,460
e che con l’accumulo di zazen, zazen, quindici giorni, succede qualcosa, il risveglio.

27
00:02:05,300 --> 00:02:11,520
Immediatamente chiunque pratichi lo zazen innesca il processo di risveglio.

28
00:02:13,020 --> 00:02:15,800
Beh, questo processo non rientra nelle nostre categorie.

29
00:02:16,900 --> 00:02:18,900
a volte la gente dice

30
00:02:18,959 --> 00:02:20,959
"zazen è stato molto, molto difficile,

31
00:02:21,420 --> 00:02:24,949
era un cattivo zazen’ o a volte

32
00:02:25,860 --> 00:02:27,860
E’ molto confortevole.

33
00:02:28,409 --> 00:02:35,929
e il processo che innesca zazen è al di là delle nostre categorie o della nostra coscienza personale".

34
00:02:36,660 --> 00:02:42,169
Quindi il metodo di Dogen, il metodo degli altri Maestri, è grande, è unico.

35
00:02:43,799 --> 00:02:48,769
Sono insegnamenti che sembrano molto semplici da ricevere,

36
00:02:50,190 --> 00:02:52,190
che riceviamo durante lo zazen

37
00:02:53,400 --> 00:02:55,400
e che andremo a

38
00:02:58,530 --> 00:03:02,750
integrare, il che significa che li faremo nostri,

39
00:03:04,019 --> 00:03:09,319
dolcemente, profondamente, inconsciamente, è una lezione preziosa.
