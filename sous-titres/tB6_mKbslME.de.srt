1
00:00:00,000 --> 00:00:04,660
Sie sagten, wenn die Staatsoberhäupter Prostationen durchführen, 

2
00:00:05,080 --> 00:00:08,560
Die Welt wäre friedlicher. 

3
00:00:08,880 --> 00:00:13,040
Ich habe mich gefragt … 

4
00:00:13,340 --> 00:00:18,340
im Vergleich zu anderen Religionen. 

5
00:00:18,620 --> 00:00:23,080
In der muslimischen Religion ist von Niederwerfung die Rede. 

6
00:00:23,460 --> 00:00:34,540
Dennoch reißen sich einige Länder des Nahen Ostens auseinander. 

7
00:00:35,180 --> 00:00:42,020
Ich wollte wissen warum. 

8
00:00:42,320 --> 00:00:46,000
Weil die Niederwerfung im Zen und im Islam gleich ist. 

9
00:00:46,340 --> 00:00:52,660
Ich denke, es gibt gute und schlechte in allen Religionen. 

10
00:00:53,080 --> 00:00:58,240
Aber Niederwerfung ist in der muslimischen Religion eine gute Sache. 

11
00:00:58,400 --> 00:01:04,620
Christen verbinden sich ebenso wie im Zen in Gasshô. 

12
00:01:05,100 --> 00:01:08,520
Das ist okay 

13
00:01:08,700 --> 00:01:14,040
Es gibt auch Fanatismus, es war schon immer da. 

14
00:01:14,300 --> 00:01:18,820
Es hat immer Religionskriege gegeben. 

15
00:01:21,320 --> 00:01:26,920
Im Zen wird die Niederwerfung nicht für einen bestimmten Zweck durchgeführt. 

16
00:01:27,280 --> 00:01:33,440
Weder um die Statue auf dem Altar anzubeten, noch um zu Buddha zu beten. 

17
00:01:33,960 --> 00:01:39,640
Es ist die Praxis der Haltung mit dem ganzen Körper. 

18
00:01:40,020 --> 00:01:46,140
In dem Moment, in dem Sie auf dem Boden aufschlagen, gibt es ein Bewusstsein. 

19
00:01:46,500 --> 00:01:50,860
Normalerweise berührt das Gehirn niemals den Boden. 

20
00:01:51,100 --> 00:01:55,820
Es ist immer am Himmel und zeigt immer auf die Sterne. 

21
00:01:56,100 --> 00:02:01,480
Es fühlt sich einfach richtig an. Zum erneuten Verbinden bücken, berühren. 

22
00:02:01,740 --> 00:02:06,300
Wenn Muslime dies tun, dann deshalb, weil es im Islam sehr gute Dinge gibt. 

23
00:02:06,660 --> 00:02:13,000
Familien ziehen ihre Kinder gut auf. 

24
00:02:15,500 --> 00:02:21,620
Echte Muslime sind voller Liebe. 

25
00:02:24,200 --> 00:02:30,020
Werfen Sie das Baby nicht mit dem Badewasser raus! 

26
00:02:30,550 --> 00:02:34,450
Es ist gut, dass sie sich niederwerfen! 

