1
00:00:00,000 --> 00:00:04,660
You said that if the heads of state carry out prostations, 

2
00:00:05,080 --> 00:00:08,560
the world would be more peaceful. 

3
00:00:08,880 --> 00:00:13,040
I wondered… 

4
00:00:13,340 --> 00:00:18,340
compared to other religions. 

5
00:00:18,620 --> 00:00:23,080
In the Muslim religion there is talk of prostration. 

6
00:00:23,460 --> 00:00:34,540
Still, some Middle Eastern countries are tearing each other apart. 

7
00:00:35,180 --> 00:00:42,020
I wanted to know why. 

8
00:00:42,320 --> 00:00:46,000
Because prostration is the same in Zen and in Islam. 

9
00:00:46,340 --> 00:00:52,660
I think there is good and bad in all religions. 

10
00:00:53,080 --> 00:00:58,240
But prostration is a good thing, in the Muslim religion. 

11
00:00:58,400 --> 00:01:04,620
Christians also join hands in the same way as in Zen, in Gasshô. 

12
00:01:05,100 --> 00:01:08,520
That’s okay. 

13
00:01:08,700 --> 00:01:14,040
There is also fanaticism, it has always been there. 

14
00:01:14,300 --> 00:01:18,820
There have always been religious wars. 

15
00:01:21,320 --> 00:01:26,920
In Zen, prostration is not done for a specific purpose. 

16
00:01:27,280 --> 00:01:33,440
Neither to worship the statue on the altar, nor to pray to Buddha. 

17
00:01:33,960 --> 00:01:39,640
It is the practice of posture with your whole body. 

18
00:01:40,020 --> 00:01:46,140
The moment you hit the ground, there is an awareness. 

19
00:01:46,500 --> 00:01:50,860
Usually the brain never touches the ground. 

20
00:01:51,100 --> 00:01:55,820
It is always in the sky, always pointing at the stars. 

21
00:01:56,100 --> 00:02:01,480
So it just feels right. To reconnect, bend over, touch. 

22
00:02:01,740 --> 00:02:06,300
When Muslims do this, it is because there are very good things in Islam. 

23
00:02:06,660 --> 00:02:13,000
Families raise their children well. 

24
00:02:15,500 --> 00:02:21,620
Real Muslims are full of love. 

25
00:02:24,200 --> 00:02:30,020
Do not throw the baby out with the bath water! 

26
00:02:30,550 --> 00:02:34,450
It is good that they prostrate! 

