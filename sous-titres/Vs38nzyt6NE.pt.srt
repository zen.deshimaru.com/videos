1
00:00:00,000 --> 00:00:03,380
É inútil ir ao Japão praticar Zen. 

2
00:00:03,640 --> 00:00:07,360
Quando as pessoas me pedem informações em Dijon, digo a elas que: 

3
00:00:07,620 --> 00:00:09,240
"Nós não vamos falar sobre o Zen." 

4
00:00:09,500 --> 00:00:10,180
"Venha para o dojo." 

5
00:00:10,440 --> 00:00:14,040
"Só então você sentirá se combina com você ou não." 

6
00:00:14,360 --> 00:00:18,100
Desde a primeira vez que você se exercita, você pode sentir o que ela traz. 

7
00:00:18,680 --> 00:00:21,800
Se você quiser, é um sinal de que você está bem com isso. 

8
00:00:22,000 --> 00:00:26,480
Não há necessidade de explicações teóricas para saber por que praticamos. 

9
00:00:26,780 --> 00:00:35,280
Quando nos instalamos no Zazen, imediatamente … você sente que a postura tem algo que dá descanso, que se acalma. 

10
00:00:35,580 --> 00:00:40,660
A atitude física é realmente importante para senti-la circulando. 

11
00:00:40,940 --> 00:00:46,960
Eu acho que está relacionado principalmente a esse equilíbrio, que nos permite render. 

12
00:00:47,240 --> 00:00:49,280
Zazen também é uma experiência de silêncio. 

13
00:00:49,620 --> 00:00:59,800
Quando estamos em silêncio juntos, quando você ouve esse silêncio, algo profundo é transmitido. 

14
00:01:00,140 --> 00:01:03,140
Estou pensando em uma enfermeira que entra completamente estressada: 

15
00:01:03,340 --> 00:01:05,840
"Eu não consigo acalmar a mente!" 

16
00:01:06,120 --> 00:01:08,840
Ela saiu completamente calma. 

17
00:01:09,120 --> 00:01:14,620
Se conseguirmos detectar gradualmente a respiração profunda, 

18
00:01:15,140 --> 00:01:22,300
apaga todos esses pensamentos irritantes, todas as complicações. 

19
00:01:22,720 --> 00:01:29,720
É até difícil dizer que sou budista porque soa como uma igreja. 

20
00:01:30,280 --> 00:01:33,720
Se você não precisa disso para praticar Zen … 

21
00:01:34,020 --> 00:01:38,800
É uma pena nos dias de hoje, as pessoas querem meditação secular. 

22
00:01:39,220 --> 00:01:42,800
em Dijon existem muitos tipos de meditações. 

23
00:01:43,140 --> 00:01:47,360
Eu não os conheço, não posso falar sobre eles. 

24
00:01:47,560 --> 00:01:52,940
Mas, pessoalmente, eu gosto de praticar como é feito. 

25
00:01:53,180 --> 00:01:54,340
Com algumas regras simples: 

26
00:01:54,700 --> 00:01:56,780
Diga olá quando chegarmos em casa. 

27
00:01:57,080 --> 00:01:58,880
Cumprimente outras pessoas com quem você pratica. 

28
00:01:59,120 --> 00:02:00,620
Nós fazemos nossa meditação. 

29
00:02:00,880 --> 00:02:06,040
Então recitamos um sutra praticamente para todo o universo. 

30
00:02:06,300 --> 00:02:08,200
É uma questão completa. 

31
00:02:08,520 --> 00:02:12,480
É uma maneira de se exercitar não apenas para si mesmo. 

32
00:02:13,500 --> 00:02:17,180
Nem sempre ouvimos nosso conforto. 

33
00:02:17,460 --> 00:02:24,300
Enquanto as pessoas que querem alguma forma de meditação, elas já estão fazendo uma escolha. 

34
00:02:24,560 --> 00:02:29,720
Gosto dessa noção de "falta de objetivo", nesta época em que tudo tende a gerar lucro. 

35
00:02:30,000 --> 00:02:32,180
Estamos no zazen, e é isso. 

36
00:02:32,420 --> 00:02:35,660
Eu recebi a mensagem do Mestre Kosen. 

37
00:02:35,960 --> 00:02:38,180
Sinto-me honrado e intimidado. 

38
00:02:38,520 --> 00:02:42,800
As pessoas imaginam que ser um mestre deve ser plenamente realizado. 

39
00:02:43,080 --> 00:02:45,180
Eu não me sinto assim. 

40
00:02:45,440 --> 00:02:50,720
Eu experimento essa transferência como o desejo de continuar transferindo a Sangha e ajudá-la a crescer. 

41
00:02:51,040 --> 00:02:56,300
Nossos Sanghas são todas as pessoas que praticam com o Mestre Kosen. 

42
00:02:56,980 --> 00:03:04,360
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

