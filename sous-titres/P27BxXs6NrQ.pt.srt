1
00:00:00,000 --> 00:00:02,200
Muitas pessoas estão interessadas em meditação … 

2
00:00:02,200 --> 00:00:04,220
… e procure informações no Youtube. 

3
00:00:04,420 --> 00:00:07,860
O problema é que não vemos muito disso. Mestre Zen Deshimaru … 

4
00:00:07,980 --> 00:00:10,900
… nem a Kosen Sangha. 

5
00:00:11,100 --> 00:00:13,180
Muitos profissionais talentosos … 

6
00:00:13,360 --> 00:00:16,300
… fez correntes Kosen Sangha. 

7
00:00:16,520 --> 00:00:18,660
Mas o público em geral se pergunta: 

8
00:00:18,940 --> 00:00:21,080
Qual é o caminho certo? 

9
00:00:21,300 --> 00:00:23,600
Agora, vamos encadear tudo: 

10
00:00:23,820 --> 00:00:26,840
bit.ly/chosen 

11
00:00:28,420 --> 00:00:31,160
Portanto, se você quiser apresentar o zazen ao maior número possível de pessoas: 

12
00:00:32,260 --> 00:00:33,980
Inscreva-se no canal. 

13
00:00:33,980 --> 00:00:35,220
Assista aos vídeos. 

14
00:00:35,220 --> 00:00:36,520
Por favor, comente. 

15
00:00:36,700 --> 00:00:38,700
E não hesite em gostar deles! 

