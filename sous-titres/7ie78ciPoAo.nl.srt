1
00:00:00,100 --> 00:00:02,020
Zen is zazen!

2
00:00:02,220 --> 00:00:04,920
Zazen zit als een Boeddha.

3
00:00:05,160 --> 00:00:08,400
Neem de originele Boeddha-houding aan.

4
00:00:08,620 --> 00:00:09,920
Pak zijn hart.

5
00:00:10,140 --> 00:00:12,360
Ga in deze houding zitten.

6
00:00:12,660 --> 00:00:16,000
En vergeet geleidelijk aan dat we een pijnlijk lichaam zijn.

7
00:00:16,320 --> 00:00:18,840
Allemaal verdraaid, met een gecompliceerde geest.

8
00:00:19,080 --> 00:00:23,120
En laat de transmutatie gebeuren, de alchemie van zazen.

9
00:00:23,760 --> 00:00:25,700
Alleen mensen kunnen oefenen.

10
00:00:26,060 --> 00:00:33,200
Het is onze evolutie over honderdduizenden jaren die het mogelijk maakt om deze houding te beoefenen.

11
00:00:33,520 --> 00:00:36,700
Balans, stabiliteit, kalmte.

12
00:00:37,000 --> 00:00:40,200
Rekken, zich openstellen, jezelf vergeten.

13
00:00:40,480 --> 00:00:46,960
Eenheid met alle wezens, met alles, met het hele universum.

14
00:00:47,740 --> 00:00:51,760
Meester Kosen zegt dat zazen het werelderfgoed van de mensheid is.

15
00:00:52,040 --> 00:00:54,600
We gaan niet iemand dwingen om zazen te doen.

16
00:00:54,860 --> 00:00:56,220
En soms is het niet het juiste moment.

17
00:00:56,460 --> 00:00:57,960
We kunnen op een gegeven moment gaan zitten.

18
00:00:58,220 --> 00:01:02,940
En missen wat het zou kunnen betekenen voor ons leven.

19
00:01:03,220 --> 00:01:07,460
En dan op een ander moment, ineens, is het er.

20
00:01:07,680 --> 00:01:09,560
Er is een vraag en een antwoord.

21
00:01:09,760 --> 00:01:11,120
Dat is precies wat we moeten oefenen.

22
00:01:11,360 --> 00:01:17,460
Zazen slechts één keer doen, zelfs als het niet jouw ding is, als je ontvankelijk bent, als je er zin in hebt, geeft het je informatie.

23
00:01:18,700 --> 00:01:21,140
Er zijn veel verschillende praktijken.

24
00:01:21,380 --> 00:01:22,880
Wat vast en zeker goed is.

25
00:01:23,160 --> 00:01:25,700
Je kunt geen dingen tegen elkaar willen opzetten.

26
00:01:26,000 --> 00:01:29,940
Zazen is een nogal veeleisende praktijk.

27
00:01:30,240 --> 00:01:32,700
Je neemt niet zomaar een standpunt in.

28
00:01:33,040 --> 00:01:38,440
Houding en de bijbehorende gemoedstoestand zijn essentieel.

29
00:01:39,160 --> 00:01:43,900
Maar zazen heeft zijn eigen leven, zijn eigen functioneren.

30
00:01:44,160 --> 00:01:46,220
Het wordt ook wel een "meditatie" genoemd.

31
00:01:46,420 --> 00:01:47,980
Maar het is maar een woord.

32
00:01:48,320 --> 00:01:51,580
We noemen het "zazen", "Boeddha’s houding".

33
00:01:51,800 --> 00:01:53,700
Het zijn maar woorden.

34
00:01:53,940 --> 00:01:56,180
Maar dat is de houding die we oefenen.

35
00:01:56,840 --> 00:01:59,220
We noemen onszelf geen leraar of meester.

36
00:01:59,500 --> 00:02:03,040
Zelfs als je 1, 2, 3 cursussen hebt gevolgd.

37
00:02:03,300 --> 00:02:06,140
Er is een intieme relatie, je volgt een meester.

38
00:02:06,380 --> 00:02:09,460
We volgen hem niet overal waar hij gaat.

39
00:02:09,780 --> 00:02:12,920
We volgen zijn leer, zijn geest.

40
00:02:13,200 --> 00:02:14,760
Er is zazen zelf.

41
00:02:15,000 --> 00:02:22,300
Dan, niet je eigen ding volgen, je eigen idee.

42
00:02:22,580 --> 00:02:25,120
Maar om iets groters te volgen.

43
00:02:25,560 --> 00:02:29,480
Iets dat generaties overspant.

44
00:02:29,860 --> 00:02:34,060
Sinds de historische Boeddha en voor hem.

45
00:02:34,380 --> 00:02:36,960
Er zijn andere boeddha’s die hem zijn voorgegaan.

46
00:02:37,240 --> 00:02:40,020
Anderen die in de houding zaten.

47
00:02:40,360 --> 00:02:42,720
Anderen die het pad hebben bereikt.

48
00:02:43,500 --> 00:02:45,520
En het is serieus!

49
00:02:45,840 --> 00:02:59,580
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

