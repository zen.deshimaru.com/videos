1
00:00:00,000 --> 00:00:05,180
Ik oefende met Meester Deshimaru in sesshin en zomerkampen.

2
00:00:05,500 --> 00:00:08,360
Er was mij gezegd: "Je zult zien, hij is een meester! »

3
00:00:08,700 --> 00:00:13,040
Ik durfde hem niet eens aan te kijken, ik was zo geïntimideerd.

4
00:00:13,540 --> 00:00:16,780
Toen ik erachter kwam dat hij ziek was, dacht ik bij mezelf:

5
00:00:17,120 --> 00:00:20,340
"Zal ik doorgaan met Zen als hij sterft?"

6
00:00:20,620 --> 00:00:26,180
Ik had de indruk dat hij me iets had bijgebracht dat me de moed gaf om door te gaan.

7
00:00:26,620 --> 00:00:30,400
Mijn naam is Françoise Julien.

8
00:00:32,400 --> 00:00:35,060
Ik had misschien eerder gedacht dat ik een katholieke non kon zijn…

9
00:00:35,400 --> 00:00:39,320
Maar hoe goed is het om een zen-nun te zijn in het werkende leven!

10
00:00:39,620 --> 00:00:43,200
Ik bleef de dojo van Lyon volgen met André Meissner.

11
00:00:43,440 --> 00:00:49,000
Het was door Meissner te volgen, die later Stéphane volgde, dat ik natuurlijk Meester Kosen volgde.

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen is een nauwe leerling van Meester Deshimaru.

13
00:00:53,420 --> 00:00:59,260
Ik werd in 1980 door Meester Deshimaru tot Bodhisattva gewijd.

14
00:00:59,680 --> 00:01:03,560
De Bodhisattva is iemand die alle wezens wil helpen.

15
00:01:03,780 --> 00:01:07,960
Ordinatie is de vervulling van deze gelofte.

16
00:01:08,200 --> 00:01:10,560
Ik heb nooit geprobeerd om Zen te oefenen.

17
00:01:10,860 --> 00:01:12,460
Ik wist niet wat het was.

18
00:01:12,740 --> 00:01:15,180
Er was een poster op een conferentie in Lyon.

19
00:01:15,400 --> 00:01:18,180
Het was een conferentie met Alain Cassan.

20
00:01:18,460 --> 00:01:25,840
Volgens hem zou je iets missen als je niet ging.

21
00:01:26,320 --> 00:01:33,860
Tijdens zijn lezing waren er mensen in zazen, maar ik was niet eens onder de indruk van de houding.

22
00:01:34,280 --> 00:01:37,780
Het was een beetje een intellectuele zet van mijn kant.

23
00:01:38,020 --> 00:01:42,220
Toen ik begon te oefenen, was het een manier om de tijd te stoppen.

24
00:01:42,500 --> 00:01:44,640
Het gaat allemaal zo snel!

25
00:01:44,940 --> 00:01:50,140
Ik vond ook dat het alle problemen op de basis kon oplossen.

26
00:01:50,460 --> 00:01:55,260
Het loste een probleem op, want ik voelde me uit de carrière van mijn wiskundeleraar geschopt.

27
00:01:55,540 --> 00:02:00,280
Ik vond de eerste zin van het gedicht Shin Jin Mei mooi:

28
00:02:00,620 --> 00:02:05,500
"De grote weg is niet moeilijk, vermijd gewoon een keuze. »

29
00:02:05,860 --> 00:02:11,100
Ik zei tegen mezelf: "Ja, ik ben heel erg beschikbaar voor wat er gebeurt! »

30
00:02:11,420 --> 00:02:16,560
Terwijl ik oefende, realiseerde ik me hoezeer ik nog steeds in een staat van keuze en afwijzing verkeerde.

31
00:02:16,940 --> 00:02:23,900
Het beoefenen van Zen helpt ons ook onze duisternis te zien.

32
00:02:25,100 --> 00:02:32,740
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

