1
00:00:00,000 --> 00:00:05,180
Practiqué con la hermana Deshimaru en sesshin y en campamentos de verano. 

2
00:00:05,500 --> 00:00:08,360
Me dijeron: "Ya verás, ¡él es un maestro!" 

3
00:00:08,700 --> 00:00:13,040
Ni siquiera me atreví a mirarlo, estaba tan intimidado. 

4
00:00:13,540 --> 00:00:16,780
Cuando descubrí que estaba enfermo, pensé: 

5
00:00:17,120 --> 00:00:20,340
"¿Debo continuar con Zen si él muere?" 

6
00:00:20,620 --> 00:00:26,180
Tenía la impresión de que me había enseñado algo que me dio el coraje para continuar. 

7
00:00:26,620 --> 00:00:30,400
Me llamo Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
Podría haber pensado antes que podría ser una monja católica … 

9
00:00:35,400 --> 00:00:39,320
¡Pero qué bueno es ser una monja zen en la vida laboral! 

10
00:00:39,620 --> 00:00:43,200
Seguí siguiendo el dojo de Lyon con André Meissner. 

11
00:00:43,440 --> 00:00:49,000
Fue siguiendo a Meissner, quien luego siguió a Stéphane, que naturalmente seguí al Maestro Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen es un alumno cercano del Maestro Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
Fui ordenado Bodhisattva en 1980 por el Maestro Deshimaru. 

14
00:00:59,680 --> 00:01:03,560
El Bodhisattva es aquel que quiere ayudar a todos los seres. 

15
00:01:03,780 --> 00:01:07,960
La ordenación es el cumplimiento de este voto. 

16
00:01:08,200 --> 00:01:10,560
Nunca intenté practicar el zen. 

17
00:01:10,860 --> 00:01:12,460
No sabía de qué se trataba. 

18
00:01:12,740 --> 00:01:15,180
Había un póster en una conferencia en Lyon. 

19
00:01:15,400 --> 00:01:18,180
Fue una conferencia con Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
Él dice que te perderías algo si no fueras. 

21
00:01:26,320 --> 00:01:33,860
Durante su conferencia había gente en zazen, pero ni siquiera me impresionó la actitud. 

22
00:01:34,280 --> 00:01:37,780
Fue un movimiento intelectual de mi parte. 

23
00:01:38,020 --> 00:01:42,220
Cuando comencé a hacer ejercicio, era una forma de detener el tiempo. 

24
00:01:42,500 --> 00:01:44,640
¡Todo va muy rápido! 

25
00:01:44,940 --> 00:01:50,140
También pensé que podría resolver todos los problemas en la base. 

26
00:01:50,460 --> 00:01:55,260
Resolvió un problema porque me sentí expulsado de la carrera de mi profesor de matemáticas. 

27
00:01:55,540 --> 00:02:00,280
Me gustó la primera oración del poema Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"El gran camino no es difícil, solo evita una elección". 

29
00:02:05,860 --> 00:02:11,100
Me dije a mí mismo: "Sí, estoy muy disponible para lo que está sucediendo". 

30
00:02:11,420 --> 00:02:16,560
Mientras practicaba, me di cuenta de cuánto estaba todavía en un estado de elección y rechazo. 

31
00:02:16,940 --> 00:02:23,900
Practicar Zen también nos ayuda a ver nuestra oscuridad. 

32
00:02:25,100 --> 00:02:32,740
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

