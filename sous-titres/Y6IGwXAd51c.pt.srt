1
00:00:00,000 --> 00:00:02,240
Eu sou um monge zen. 

2
00:00:04,540 --> 00:00:09,960
Eu também sou um calígrafo. 

3
00:00:10,980 --> 00:00:18,660
A caligrafia chinesa é apreciada por pessoas que praticam zazen. 

4
00:00:19,280 --> 00:00:24,980
Não é fácil falar sobre zen. 

5
00:00:25,400 --> 00:00:28,820
Expressar, manifestar. 

6
00:00:29,240 --> 00:00:34,480
Através da caligrafia, vemos o espírito de uma pessoa praticando zazen. 

7
00:00:35,000 --> 00:00:39,100
Quem pratica a atitude de despertar. 

8
00:00:40,020 --> 00:00:46,260
Mestre Deshimaru explica que caligrafia não é apenas uma técnica. 

9
00:00:46,980 --> 00:00:52,700
Não apenas caligrafia de acordo com técnicas, propriedades … 

10
00:00:53,360 --> 00:00:58,400
Caligrafia com a mente, com canela. 

11
00:00:58,880 --> 00:01:09,440
Com a mente, com todo o corpo e mente em um. 

12
00:01:10,720 --> 00:01:18,400
Caligrafia imprime isso no papel. É uma presença. 

13
00:01:18,960 --> 00:01:24,320
É sentida na caligrafia de grandes mestres como Deshimaru e Niwa Zenji. 

14
00:01:25,760 --> 00:01:30,000
O valor da caligrafia não está apenas na forma. 

15
00:01:31,440 --> 00:01:36,560
Mas também no momento, a presença, a energia. 

16
00:01:37,280 --> 00:01:46,240
Na atividade que surge …. 

17
00:01:47,040 --> 00:01:52,080
E que sentimos e percebemos. 

18
00:01:52,500 --> 00:01:55,800
Começamos a partir de um momento que vem da imobilidade. 

19
00:01:56,950 --> 00:01:58,853
Zen, calmo, quieto, silêncio. 

20
00:02:00,160 --> 00:02:06,093
Para manter nada acontecendo. 

21
00:02:06,100 --> 00:02:07,910
Deixar pensamentos passarem. 

22
00:02:07,950 --> 00:02:10,750
Não deve ser monopolizado. 

23
00:02:10,840 --> 00:02:15,040
Estar focado em uma atitude. 

24
00:02:15,920 --> 00:02:21,329
Pernas cruzadas. 

25
00:02:21,329 --> 00:02:26,880
A almofada permite inclinar a pelve para a frente. 

26
00:02:26,880 --> 00:02:31,280
Para limpar toda a caixa torácica. 

27
00:02:32,240 --> 00:02:37,040
Nesta pose, trazemos nossas mãos de volta. 

28
00:02:38,160 --> 00:02:43,680
Permite-nos unir e centralizar. 

29
00:02:44,400 --> 00:02:51,040
É a atitude da meditação zen, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
É a partir disso, e do momento, da atividade que a caligrafia emergirá. 

31
00:02:58,480 --> 00:03:05,200
Não por complicações, por querer fazer o bem, por querer fazer o bem, por querer fazer o bom tempo … 

32
00:03:06,240 --> 00:03:11,520
Somente esse momento da vida. 

33
00:03:12,560 --> 00:03:22,160
A caligrafia emergirá desse impulso zen, desse impulso, desse impulso. 

34
00:03:23,280 --> 00:03:33,200
É disso que trata a caligrafia zen. 

35
00:03:34,720 --> 00:03:59,600
Considerando a fase de civilização que a China alcançou durante a dinastia Tang … 

36
00:04:01,120 --> 00:04:15,520
Nos séculos 7, 8 e 9, os países vizinhos, Japão, Coréia e Vietnã, foram atraídos por essa cultura e pelo idioma, os caracteres chineses. 

37
00:04:16,400 --> 00:04:27,200
Portanto, o script chinês se espalhou para esses países. 

38
00:04:28,080 --> 00:04:34,480
Mas todo mundo gravou de acordo com seu próprio sentimento, sua própria língua. 

39
00:04:35,280 --> 00:04:44,560
Cada um deles desenvolveu seu próprio modo de caligrafia. 

40
00:04:45,520 --> 00:04:56,080
Eles também introduziram programas, caracteres próximos a um alfabeto misturado com caracteres chineses. 

41
00:04:58,320 --> 00:05:04,480
A caligrafia japonesa tem pequenas diferenças em relação à caligrafia chinesa. 

42
00:05:05,680 --> 00:05:13,440
Todos usam pincéis, desenham caracteres chineses, mas também usam sistemas silábicos. 

43
00:05:15,040 --> 00:05:23,280
Os vietnamitas, anteriormente influenciados pela China, eram ainda mais criativos. 

44
00:05:24,800 --> 00:05:31,600
Eles inventaram seu próprio sistema ideográfico para transcrever sua língua para o modelo chinês. 

45
00:05:33,440 --> 00:05:38,400
Está escrito: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
É o sutra do Espírito da Grande Sabedoria que nos move adiante. 

48
00:05:59,840 --> 00:06:06,160
Este é o ensino universal do Buda, específico para todas as escolas budistas. 

49
00:06:07,360 --> 00:06:16,800
É cantada em chinês antigo, japonês antigo, na transcrição fonética do sânscrito. 

50
00:06:18,080 --> 00:06:24,000
Isso o torna um texto universal, porque ninguém entende ou lê em seu próprio idioma. 

51
00:06:25,360 --> 00:06:39,120
Os japoneses, os chineses, os índios …. Nós nos pronunciamos com alguns pequenos ajustes …. 

52
00:06:40,980 --> 00:06:47,460
É algo que pertence a todos e a ninguém em particular. 

53
00:06:48,220 --> 00:06:53,440
Esta é a frase que escrevi aqui, em um estilo de escrita chamado cursivo. 

54
00:06:53,680 --> 00:06:58,520
Não está escrevendo o dicionário. 

55
00:06:58,560 --> 00:07:03,680
Não é uma escrita regular. 

56
00:07:05,740 --> 00:07:09,100
É desenvolvido para escrita rápida. 

57
00:07:09,480 --> 00:07:15,960
Uma espécie de taquigrafia. 

58
00:07:16,240 --> 00:07:28,560
Mantemos as linhas principais do personagem, simplificando, através de curvaturas, através de pontos, onde havia uma linha … 

59
00:07:29,600 --> 00:07:39,440
O personagem foi simplificado ao extremo, assume uma forma diferente da sua forma original. 

60
00:07:39,760 --> 00:07:47,680
Seguimos em direção a uma abstração, preservando a essência da forma original. 

61
00:07:48,800 --> 00:07:55,920
Ele permite que você escreva mais rápido. 

62
00:07:58,080 --> 00:08:05,240
Na trama, encontramos esse impulso, esse impulso, esse impulso do zazen. 

63
00:08:05,640 --> 00:08:13,280
Porque nós conectamos todo o personagem. 

64
00:08:13,660 --> 00:08:18,479
De uma só vez. 

65
00:08:18,479 --> 00:08:23,470
Podemos até conectar os personagens juntos. 

66
00:08:24,000 --> 00:08:31,840
Vinculamos cada um dos personagens, cada uma das propriedades do personagem. 

67
00:08:33,120 --> 00:08:38,520
Se vários caracteres forem desenhados, eles poderão ser vinculados. 

68
00:08:38,940 --> 00:08:44,960
Fazemos isso na respiração, no impulso. 

69
00:08:45,400 --> 00:08:51,540
No momento, a energia, a dinâmica, a atividade de todo o corpo, toda a mente em um. 

70
00:08:51,540 --> 00:08:55,200
Em um movimento, do início ao fim. 

71
00:08:55,200 --> 00:08:59,200
Não podemos voltar atrás, não podemos atualizá-lo. 

72
00:08:59,200 --> 00:09:02,900
Não podemos corrigi-lo. 

73
00:09:03,000 --> 00:09:05,120
Fazemos isso em consciência. 

74
00:09:05,920 --> 00:09:11,600
Também nos deixamos ir com alegria. 

75
00:09:12,060 --> 00:09:18,220
Mas não podemos corrigir nada. 

76
00:09:18,560 --> 00:09:22,160
Quando é rastreado, é rastreado. 

77
00:09:22,200 --> 00:09:25,529
É o aqui e agora. 

78
00:09:25,529 --> 00:09:29,000
É algo que nunca mais voltará. 

79
00:09:29,100 --> 00:09:33,800
Isso existe completamente, eternamente, no momento. 

80
00:09:33,900 --> 00:09:36,200
Isso é Zen também. 

81
00:09:36,250 --> 00:09:39,640
Toda vez, cada respiração zazen é única. 

82
00:09:42,520 --> 00:09:44,640
Ela não vai voltar. 

83
00:09:45,340 --> 00:09:52,540
É um ponto, um hífen da atitude. 

84
00:09:52,600 --> 00:09:58,320
De bom humor, respiração e atividade total. 

85
00:09:58,420 --> 00:10:03,160
É um tempo de vida, de consciência, absoluto, universal. 

86
00:10:03,720 --> 00:10:06,760
Quem não volta. 

87
00:10:07,120 --> 00:10:11,000
É muito importante na caligrafia. 

88
00:10:11,000 --> 00:10:17,500
Se você caligrafia como um robô sem colocar todos os seus pensamentos nele, é uma técnica. 

89
00:10:17,540 --> 00:10:18,920
Zazen é o mesmo. 

90
00:10:19,000 --> 00:10:25,880
Se você pratica o zazen como uma técnica de bem-estar, essa não é a dimensão do zen. 

