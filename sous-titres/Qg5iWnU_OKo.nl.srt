1
00:00:00,100 --> 00:00:01,460
Mijn naam is Christophe Desmur.

2
00:00:01,780 --> 00:00:03,540
Mijn Zen-monnik heet Ryurin.

3
00:00:03,880 --> 00:00:05,580
Ik ben ook een kalligraaf.

4
00:00:05,900 --> 00:00:09,560
Ik geef les in Chinese kalligrafie, die ik ontdekte in Zen.

5
00:00:09,840 --> 00:00:13,860
Ik heb het geleerd van een Zen monnik.

6
00:00:14,320 --> 00:00:18,260
Ik vind het leuk om kalligrafie met iedereen te delen.

7
00:00:18,860 --> 00:00:22,540
Ik doe veel werk op scholen.

8
00:00:22,900 --> 00:00:27,360
Ik kalligrafieer wel met kinderen in workshops.

9
00:00:27,700 --> 00:00:31,020
Ook bij volwassenen en gehandicapten.

10
00:00:31,380 --> 00:00:33,300
Ze vinden het erg leuk.

11
00:00:33,780 --> 00:00:39,220
Mijn jongste studenten zijn 6 jaar oud, en mijn oudste student is 90.

12
00:00:39,760 --> 00:00:42,800
Ik geef ook Chinese les.

13
00:00:43,180 --> 00:00:44,840
Dat is mijn core business.

14
00:00:45,140 --> 00:00:48,020
Deze kalligrafie is gewoon “Zen” (禅).

15
00:00:48,320 --> 00:00:54,060
Het is een kalligrafie gemaakt in de huidige, semi-cursieve stijl.

16
00:00:54,580 --> 00:01:06,760
Het penseel is kort, het gaat van stippen naar lijnen, om een Chinees of Japans karakter te vormen.

17
00:01:08,420 --> 00:01:11,320
Dit is “Reiki” (靈氣), de geestelijke energie.

18
00:01:11,620 --> 00:01:13,880
Het is een genezingstechniek.

19
00:01:14,220 --> 00:01:20,260
We zetten deze traditie voort, want het komt van een meester die veel kalligrafie heeft beoefend.

20
00:01:20,500 --> 00:01:23,920
Hij oefende ook sumi-e, (墨絵), hij was een schilder.

21
00:01:24,280 --> 00:01:27,580
Ik waardeer Master Deshimaru’s schilderijen zeer, hij was een kunstenaar.

22
00:01:27,840 --> 00:01:31,020
Hij schilderde en kalligrafeerde heel goed.

23
00:01:31,440 --> 00:01:37,860
Zijn kalligrafie is zeer sterk, dicht en energiek.

24
00:01:38,980 --> 00:01:43,280
“Maka hannya haramita shingyo” (般若心経) is de naam van een soetra die gezongen wordt.

25
00:01:43,700 --> 00:01:52,480
Ik heb het getraceerd in krullend: een beetje zoals kleine spaghetti.

26
00:01:52,780 --> 00:01:56,440
De karakters zijn zeer gelinkt en vereenvoudigd.

27
00:01:56,780 --> 00:02:00,800
Dat bevalt ons ook goed in de zenpraktijk.

28
00:02:01,240 --> 00:02:08,040
Zenbeoefenaars hielden echt van dit schrijven, dat vrij vrij en expressief is.

29
00:02:09,260 --> 00:02:10,740
Het is een energieke kunst.

30
00:02:11,020 --> 00:02:14,840
Het wordt geoefend met het hele lichaam, in verband met de ademhaling.

31
00:02:15,240 --> 00:02:21,900
In verband met zijn beschikbaarheid van de geest, zijn bewustzijn.

32
00:02:22,360 --> 00:02:24,720
Dus het is een energiekunst.

33
00:02:25,000 --> 00:02:28,120
We gaan kalmeren, iets in onszelf heroriënteren.

34
00:02:28,360 --> 00:02:33,240
Het is een hele metamorfose, een alchemie van evenwicht.

35
00:02:33,560 --> 00:02:40,040
Horizontaliteit, verticaliteit, centrering, koppeling bij het tekenen van de tekens.

36
00:02:40,540 --> 00:02:43,120
Beschikbaarheid door de hand.

37
00:02:43,460 --> 00:02:48,080
Bij zenmeditatie is de aanwezigheid in de handen erg belangrijk.

38
00:02:48,420 --> 00:02:55,600
Of het nu stilstaat of loopt.

39
00:02:55,940 --> 00:02:57,760
In de kalligrafie is het gebaar anders.

40
00:02:58,180 --> 00:03:00,120
Maar er is een aanwezigheid door de handen.

41
00:03:00,340 --> 00:03:02,320
Het is de hand-naar-hersenen relatie.

42
00:03:02,600 --> 00:03:04,140
Kalligrafie heeft een leven.

43
00:03:04,440 --> 00:03:05,440
Zelfs als je er niets van weet!

44
00:03:05,680 --> 00:03:07,980
Sommige kalligrafie zal je raken, andere niet.

45
00:03:08,200 --> 00:03:11,280
“Yang Sheng” (養生) is Chinees, en het is meer Taoïstisch.

46
00:03:11,740 --> 00:03:15,820
Het betekent: “het vitale principe voeden”.

47
00:03:16,540 --> 00:03:23,440
Als je deze video leuk vond, voel je vrij om hem leuk te vinden en je te abonneren op het kanaal!

