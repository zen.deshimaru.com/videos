1
00:00:00,000 --> 00:00:08,750
Je fais zazen avec un kesa de Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
En soie.

3
00:00:15,330 --> 00:00:19,215
Je suis très content.

4
00:00:20,235 --> 00:00:22,830
J’aime beaucoup Barbara.

5
00:00:24,580 --> 00:00:27,210
C’est une grande sainte.

6
00:00:33,770 --> 00:00:38,190
Heureusement, mes disciples en général sont mieux que moi.

7
00:00:42,910 --> 00:00:45,830
Surtout Barbara.

8
00:00:48,850 --> 00:00:51,270
Et Ariadna [qui traduit] aussi.

9
00:00:55,710 --> 00:00:58,515
Les trois trésors.

10
00:01:01,015 --> 00:01:03,817
Bouddha, dharma, sangha.

11
00:01:06,267 --> 00:01:10,440
sont inséparables.

12
00:01:24,820 --> 00:01:28,470
Je vous ai préparé comme kusen…

13
00:01:35,340 --> 00:01:41,560
… la continuité de ce que j’ai fait pendant le camp d’été.

14
00:01:45,170 --> 00:01:50,930
Sur les quatre fondements des pouvoirs magiques.

15
00:01:56,750 --> 00:02:01,770
J’ai fait ça pendant le camp d’été…

16
00:02:02,130 --> 00:02:10,530
On dit que les quatre fondements sont comme les quatre sabots d’un cheval.

17
00:02:35,940 --> 00:02:40,120
Le premier, c’est la volonté, la volition.

18
00:02:44,460 --> 00:02:50,300
La volition, c’est quelque chose de plus fort que vous.

19
00:02:52,290 --> 00:02:55,400
Et qui vous pousse à agir.

20
00:02:58,920 --> 00:03:02,400
C’est comme une question de survie.

21
00:03:06,430 --> 00:03:08,350
C’est très fort.

22
00:03:09,410 --> 00:03:12,160
J’en ai parlé pendant le camp d’été.

23
00:03:15,530 --> 00:03:18,550
Le second, c’est l’esprit.

24
00:03:20,745 --> 00:03:23,015
Le second sabot.

25
00:03:27,730 --> 00:03:33,390
Le troisième, c’est la progression vers l’avant, avancer.

26
00:03:38,440 --> 00:03:40,810
Le quatrième, c’est la pensée.

27
00:03:43,810 --> 00:03:46,855
Là, j’en suis à l’esprit.

28
00:03:48,135 --> 00:03:50,020
Le second.

29
00:03:54,970 --> 00:03:57,515
J’en avais parlé pendant le camp d’été,

30
00:03:57,625 --> 00:04:00,860
mais je n’avais pas fini.

31
00:04:03,270 --> 00:04:09,595
Je vais vous raconter trois histoires zen.

32
00:04:11,995 --> 00:04:16,060
Qui font comprendre mieux la nature de l’esprit.

33
00:04:19,990 --> 00:04:25,860
Le maître Eka, disciple de Bodhidharma…

34
00:04:32,370 --> 00:04:35,760
… debout dans la neige…

35
00:04:39,330 --> 00:04:41,145
… glaciale…

36
00:04:44,635 --> 00:04:47,564
… s’adressa à son maître !

37
00:04:53,470 --> 00:04:57,240
« Maître, mon esprit n’est pas paisible. »

38
00:05:08,460 --> 00:05:12,130
« Je vous en supplie donnez-lui la paix ! »

39
00:05:18,940 --> 00:05:25,380
Eka avait tué pas mal d’hommes vu qu’il avait été militaire.

40
00:05:33,130 --> 00:05:37,370
Il se sentait extrêmement coupable de tout ça.

41
00:05:40,780 --> 00:05:44,100
Il n’arrivait pas à se débarrasser de cette culpabilité.

42
00:05:49,410 --> 00:05:54,485
Il est dans la neige et demande à Bodhidharma : « Acceptez-moi ! »

43
00:06:00,245 --> 00:06:02,050
« Comme disciple ! »

44
00:06:06,000 --> 00:06:09,410
Bodhidharma ne répond pas.

45
00:06:17,770 --> 00:06:19,615
Il insiste.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma ne bouge pas.

47
00:06:30,960 --> 00:06:35,420
À la fin, il se tranche le bras avec son sabre.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma dit : « Bon… »

49
00:06:58,695 --> 00:07:02,350
Bodhidharma grogne : « Han… »

50
00:07:06,620 --> 00:07:09,980
Et il continue zazen.

51
00:07:14,280 --> 00:07:17,145
Eka insiste, supplie :

52
00:07:20,910 --> 00:07:24,630
« Je vous supplie de pacifier mon esprit ! »

53
00:07:29,190 --> 00:07:31,335
« C’est insupportable ! »

54
00:07:32,305 --> 00:07:34,400
« Je vais devenir fou ! »

55
00:07:44,290 --> 00:07:48,140
Enfin, Bodhidharma lui adresse un regard :

56
00:07:54,430 --> 00:07:58,595
« Amenez-moi votre esprit. »

57
00:08:10,745 --> 00:08:13,680
« Et je lui donnerai la paix. »

58
00:08:20,300 --> 00:08:22,590
Maître Eka lui répond :

59
00:08:26,210 --> 00:08:29,105
« Maître, je l’ai cherché, mon esprit ! »

60
00:08:33,665 --> 00:08:37,750
« Et il est resté inaccessible, insaisssable ! »

61
00:08:46,240 --> 00:08:49,020
Eka cherche la paix de l’esprit.

62
00:08:52,630 --> 00:08:55,050
Il est en souffrance.

63
00:08:56,520 --> 00:08:59,340
Il veut la paix, le calme.

64
00:09:04,085 --> 00:09:05,490
On a beau lui dire :

65
00:09:05,550 --> 00:09:07,845
« C’est ton mental ! »

66
00:09:13,950 --> 00:09:16,360
« Où est-il, mon mental ? »

67
00:09:17,805 --> 00:09:19,655
« Où est-il, mon esprit ? »

68
00:09:22,452 --> 00:09:25,182
« Où est la racine de cet esprit ? »

69
00:09:29,400 --> 00:09:32,480
« Quelle est l’essence ce cet esprit ? »

70
00:09:35,925 --> 00:09:38,528
« Est-elle dans le cerveau ? »

71
00:09:46,670 --> 00:09:48,510
« Est-elle dans le cœur ? »

72
00:09:56,655 --> 00:10:00,025
Même si tout le monde ne se tranche pas le bras,

73
00:10:03,270 --> 00:10:07,120
dans la neige, dans le froid…

74
00:10:08,810 --> 00:10:12,880
Tout le monde veut trouver quelque chose à propos de son esprit.

75
00:10:16,040 --> 00:10:18,570
Tout le monde veut devenir paisible.

76
00:10:21,970 --> 00:10:23,691
Pacifier son esprit.

77
00:10:27,930 --> 00:10:31,515
Eka a eu une réponse remarquable quand il a dit :

78
00:10:31,645 --> 00:10:35,920
« Je l’ai cherché, mais il est insaisissable ! »

79
00:10:43,990 --> 00:10:46,030
« Je n’ai pas pu le trouver. »

80
00:10:48,615 --> 00:10:53,235
En japonais, ça se dit : « Shin fukatoku ».

81
00:11:07,460 --> 00:11:09,885
« Fu », c’est la négation, « rien ».

82
00:11:13,960 --> 00:11:15,870
« Ta » : saisir.

83
00:11:19,270 --> 00:11:21,100
Saisir, trouver.

84
00:11:24,230 --> 00:11:28,260
« Toku » , c’est comme dans « Mushotoku ».

85
00:11:29,550 --> 00:11:33,470
« Toku » veut dire « obtenir ».

86
00:11:35,910 --> 00:11:37,930
« Atteindre son but ».

87
00:11:40,370 --> 00:11:43,320
« Shin fukatoku » ,

88
00:11:47,710 --> 00:11:51,310
C’est l’esprit complètement introuvable, insaisissable.

89
00:11:57,710 --> 00:11:58,955
Eka dit :

90
00:11:59,085 --> 00:12:01,650
« Je l’ai cherché, mais je ne l’ai pas trouvé. »

91
00:12:04,480 --> 00:12:06,670
C’est une excellente réponse.

92
00:12:09,470 --> 00:12:13,340
Mais celle de Bodhidharma est encore plus magnifique.

93
00:12:18,060 --> 00:12:22,330
« Amenez-moi votre esprit et je le pacifierai. »

94
00:12:29,700 --> 00:12:30,660
Eka a dit :

95
00:12:30,720 --> 00:12:32,670
« Je ne peux pas ! »

96
00:12:34,760 --> 00:12:37,320
« Non seulement je ne peux pas l’attraper, »

97
00:12:37,400 --> 00:12:40,320
« mais je ne peux même pas le trouver ! »

98
00:12:44,610 --> 00:12:46,270
Bodhidharma lui dit :

99
00:12:48,910 --> 00:12:52,340
« Dans ce cas, il est déjà pacifié ! »

100
00:13:07,610 --> 00:13:09,520
Deuxième histoire :

101
00:13:11,955 --> 00:13:14,575
J’aime bien les histoires.

102
00:13:16,890 --> 00:13:18,750
Maître Obaku.

103
00:13:20,240 --> 00:13:22,330
Le maître de Baso.

104
00:13:25,480 --> 00:13:30,240
Donne son enseignement, devant des moines.

105
00:13:33,405 --> 00:13:35,605
Quelqu’un l’interpelle :

106
00:13:41,550 --> 00:13:46,580
« Le maître doit avoir une grande faculté de concentration. »

107
00:13:52,200 --> 00:13:53,985
Maître Obaku répond :

108
00:13:58,735 --> 00:14:02,110
« La concentration, »

109
00:14:03,950 --> 00:14:07,594
« … ou au contraire la distraction, »

110
00:14:10,310 --> 00:14:14,610
« … vous dites que ces choses-là existent ? »

111
00:14:18,300 --> 00:14:20,600
« Moi, je vous dis : »

112
00:14:23,770 --> 00:14:27,100
« Cherchez-les, et vous ne les trouverez nulle part. »

113
00:14:32,840 --> 00:14:36,340
« Mais si vous me dites qu’elles n’existent pas, »

114
00:14:40,390 --> 00:14:42,490
« je vous dirai : »

115
00:14:45,510 --> 00:14:47,390
« quoi que vous pensiez, »

116
00:14:53,490 --> 00:14:58,050
« vous êtes constamment présent là où vous êtes ! »

117
00:15:08,040 --> 00:15:11,650
Même au moment où vous croyez être distrait,

118
00:15:16,330 --> 00:15:20,610
regardez le lieu l’origine de cette distraction.

119
00:15:24,850 --> 00:15:27,950
Vous verrez qu’elle n’a finalement pas d’origine.

120
00:15:33,360 --> 00:15:37,260
L’état mental de la distraction qui apparaît…

121
00:15:40,870 --> 00:15:43,490
… ne va nulle part.

122
00:15:44,920 --> 00:15:46,695
Dans les dix directions.

123
00:15:48,305 --> 00:15:50,690
Et il ne va nulle par ailleurs non plus.

124
00:16:10,270 --> 00:16:13,490
En zazen, il n’y a ni distraction ni concentration.

125
00:16:21,040 --> 00:16:23,470
Simplement assis sans bouger.

126
00:16:30,800 --> 00:16:33,090
Le reste, ce sont des techniques.

127
00:16:35,820 --> 00:16:40,168
Ça nous éloigne du zazen authentique.

128
00:16:45,129 --> 00:16:46,769
Troisième histoire.

129
00:16:47,119 --> 00:16:49,719
- Ouais, je vous ai gâtés ! -

130
00:17:00,600 --> 00:17:03,750
C’est à l’époque de Baso, en chine.

131
00:17:05,760 --> 00:17:18,250
Baso, c’est le disciple du maître dont je viens de parler, Obaku.

132
00:17:24,410 --> 00:17:28,330
À cette époque vivait un érudit

133
00:17:30,950 --> 00:17:34,280
… qui s’appelait Ryo.

134
00:17:36,560 --> 00:17:41,090
Il était réputé pour sa connaissance du bouddhisme.

135
00:17:45,320 --> 00:17:48,840
Il passait sa vie à faire des conférences.

136
00:17:53,410 --> 00:17:58,790
Il se trouve qu’un jour, il eut une entrevue avec Baso.

137
00:18:04,890 --> 00:18:07,160
Baso lui demanda :

138
00:18:09,190 --> 00:18:12,770
- « Sur quels sujets faites-vous vos conférences ? »

139
00:18:15,850 --> 00:18:20,040
- « Sur le Soutra de l’Esprit, l’Hannya Shingyo. »

140
00:18:25,360 --> 00:18:28,220
On l’appelle aussi le Soutra du Cœur.

141
00:18:32,420 --> 00:18:36,750
On ne sait pas trop si l’esprit dans le cerveau ou dans le cœur.

142
00:18:41,320 --> 00:18:47,240
Dans « Shingyo », « Shin » veut dire l’Esprit, ou le Cœur.

143
00:18:52,600 --> 00:18:55,460
« Gyo » veut dire Soutra.

144
00:18:58,090 --> 00:19:00,285
Alors Baso lui demande :

145
00:19:03,825 --> 00:19:07,260
- « Avec quoi faites-vous vos conférences ? »

146
00:19:09,950 --> 00:19:11,755
Ryo répondit :

147
00:19:14,525 --> 00:19:17,527
- « Avec l’Esprit. »

148
00:19:19,530 --> 00:19:21,355
Baso lui dit :

149
00:19:24,685 --> 00:19:30,210
- « L’esprit, il est comme un super bon acteur ! »

150
00:19:34,480 --> 00:19:39,390
« Le sens de sa pensée est comme les extravagances d’un bouffon. »

151
00:19:45,690 --> 00:19:51,580
« Quant aux six sens qui nous les font ressentir comme dans un film au cinéma… »

152
00:20:07,270 --> 00:20:10,420
« Ces six sens sont vides ! »

153
00:20:12,420 --> 00:20:16,970
« Comme une vague sans véritable début ni fin. »

154
00:20:21,260 --> 00:20:26,340
« Comment l’Esprit pourrait-il bien faire une conférence sur un soutra ? »

155
00:20:33,590 --> 00:20:38,200
Ryo répliqua, en se croyant intelligent :

156
00:20:42,150 --> 00:20:47,880
- « Si l’esprit ne peut pas le faire, peut-être que le non-esprit le peut ? »

157
00:20:51,630 --> 00:20:53,315
Baso lui répond :

158
00:20:55,055 --> 00:20:58,607
- « Oui, exactement ! »

159
00:21:00,167 --> 00:21:04,150
« Le non-esprit peut tout à fait faire une conférence. »

160
00:21:09,050 --> 00:21:13,230
Ryo, se croyant satisfait de sa réponse…

161
00:21:16,350 --> 00:21:19,730
… épousseta ses manches…

162
00:21:25,070 --> 00:21:28,100
… et s’apprêta à partir.

163
00:21:31,390 --> 00:21:33,820
Maître Baso l’appelle :

164
00:21:35,920 --> 00:21:38,605
- « Professeur ! »

165
00:21:39,305 --> 00:21:41,160
Ryo tourna la tête.

166
00:21:47,460 --> 00:21:49,115
Baso lui dit :

167
00:21:49,185 --> 00:21:51,560
- « Qu’est-ce que vous faites ? »

168
00:21:55,630 --> 00:21:58,225
Ryo eut une profonde révélation.

169
00:22:03,905 --> 00:22:07,490
Il a pris conscience de son non-esprit.

170
00:22:10,940 --> 00:22:12,440
Et Baso lui dit :

171
00:22:12,550 --> 00:22:16,000
« De la naissance à la mort, il en est ainsi. »

172
00:22:21,750 --> 00:22:24,976
On pense, on fait des trucs,

173
00:22:28,400 --> 00:22:30,670
spontanément.

174
00:22:33,320 --> 00:22:36,315
Ryo voulut se prosterner devant Baso,

175
00:22:42,215 --> 00:22:44,840
mais celui-ci lui dit :

176
00:22:46,660 --> 00:22:51,220
« Pas besoin de faire des simagrées imbéciles ! »

177
00:23:02,150 --> 00:23:07,280
Le corps de Ryo fut tout entier couvert de sueur.

178
00:23:16,080 --> 00:23:19,360
Il est retourné dans son temple.

179
00:23:21,260 --> 00:23:24,295
Et il dit à ses disciples :

180
00:23:27,575 --> 00:23:31,645
« Je pensais qu’on pourrait dire à ma mort : »

181
00:23:37,670 --> 00:23:41,440
« Que j’avais fait les meilleures conférences… »

182
00:23:44,650 --> 00:23:47,170
« … sur le zen. »

183
00:23:52,430 --> 00:23:55,830
« Aujourd’hui j’ai posé une question à Baso. »

184
00:24:00,250 --> 00:24:03,710
« Et il a dissipé le brouillard de tout ma vie. »

185
00:24:10,790 --> 00:24:14,400
Ryo abandonna les conférences.

186
00:24:18,180 --> 00:24:21,125
Et se retira au cœur des montagnes.

187
00:24:24,475 --> 00:24:28,290
Et on n’entendit plus jamais parler de lui.

188
00:25:15,750 --> 00:25:21,950
Pourquoi l’esprit est-il si difficile à attraper ?

189
00:25:24,620 --> 00:25:25,755
Pourquoi ?

190
00:25:35,090 --> 00:25:39,490
Parce que la pensée, la conscience…

191
00:25:44,230 --> 00:25:46,680
… et même la réalité…

192
00:26:00,830 --> 00:26:02,620
… tantôt apparaît…

193
00:26:06,380 --> 00:26:08,210
… tantôt disparaît.

194
00:26:10,690 --> 00:26:16,570
Tantôt apparaît, tantôt disparaît…

195
00:26:26,840 --> 00:26:30,275
Quand elle disparaît, on ne sait plus où elle est.

196
00:26:32,895 --> 00:26:35,532
Mais là où elle est…

197
00:26:37,512 --> 00:26:39,260
… elle y est.

198
00:26:42,620 --> 00:26:44,475
Elle est présente.

199
00:26:53,205 --> 00:26:56,900
Là où elle n’est pas, elle est présente aussi.

200
00:27:04,820 --> 00:27:09,140
Le temps et la conscience ne sont pas linéaires.

201
00:28:08,350 --> 00:28:10,574
Quant à la distraction…

202
00:28:19,220 --> 00:28:24,070
Quand j’étais petit, on me disait toujours
que je n’étais pas concentré.

203
00:28:37,820 --> 00:28:39,365
Distrait.

204
00:28:42,805 --> 00:28:45,670
Mais quand je jouais…

205
00:28:47,257 --> 00:28:50,930
Quand je jouais avec mes petites voitures…

206
00:28:55,110 --> 00:28:58,880
… j’étais complètement concentré.

207
00:29:06,020 --> 00:29:11,980
L’Hannya Shingyo et la cérémonie sont dédiés à jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
Et Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra de la grande sagesse qui permet d’aller au-delà

210
00:30:12,312 --> 00:30:17,006
Le Bodhisattva de la Vraie Liberté ,

211
00:30:17,196 --> 00:30:21,144
par la pratique profonde de la Grande Sagesse,

212
00:30:21,264 --> 00:30:25,022
comprend que le corps et les cinq skanda ne sont que vacuité

213
00:30:25,152 --> 00:30:30,151
et par cette compréhension, il aide tous ceux qui souffrent.

214
00:30:30,341 --> 00:30:34,870
Ô Sariputra, les phénomènes ne sont pas différents de ku.

215
00:30:34,970 --> 00:30:39,527
Ku n’est pas différent des phénomènes.

216
00:30:39,987 --> 00:30:42,996
Les phénomènes deviennent ku.

217
00:30:43,196 --> 00:30:51,305
Ku devient phénomène (la forme est le vide, le vide est la forme),

218
00:30:51,575 --> 00:30:56,155
les cinq skanda sont phénomènes également.

219
00:30:56,455 --> 00:31:03,001
Ô Sariputra, toute existence a le caractère de ku,

220
00:31:03,451 --> 00:31:08,429
il n’y a ni naissance ni commencement,

221
00:31:08,989 --> 00:31:14,707
ni pureté, ni souillure, ni croissance, ni décroissance.

222
00:31:14,977 --> 00:31:20,130
C’est pourquoi dans ku, il n’y a ni forme, ni skanda,

223
00:31:20,939 --> 00:31:26,080
ni œil, ni oreille, ni nez, ni langue, ni corps, ni conscience ;

224
00:31:26,491 --> 00:31:32,397
il n’y a ni couleur, ni son, ni odeur, ni goût, ni toucher, ni objet de pensée ;

225
00:31:32,527 --> 00:31:37,915
il n’y a ni savoir, ni ignorance, ni illusion, ni cessation de la souffrance,

226
00:31:38,225 --> 00:31:43,283
il n’y a pas de connaissance, ni profit, ni non-profit.

227
00:31:43,413 --> 00:31:48,649
Pour le Bodhisattva, grâce à cette Sagesse qui conduit au-delà,

228
00:31:48,969 --> 00:31:54,622
il n’existe ni peur, ni crainte.

229
00:31:54,882 --> 00:32:02,024
Toute illusion et attachement sont éloignés

230
00:32:02,504 --> 00:32:11,116
et il peut saisir la fin ultime de la vie, le Nirvana.

231
00:32:12,325 --> 00:32:16,407
Tous les Bouddhas du passé, du présent, du futur

232
00:32:16,497 --> 00:32:21,489
peuvent atteindre à la compréhension de cette Suprême Sagesse

233
00:32:21,569 --> 00:32:25,021
qui délivre de la souffrance, le Satori,

234
00:32:25,081 --> 00:32:30,329
par cette incantation, incomparable et sans pareille, authentique,

235
00:32:30,432 --> 00:32:36,163
qui supprime toute souffrance et permet de trouver la réalité, le vrai ku :

236
00:32:38,113 --> 00:32:52,170
« Aller, aller, aller ensemble au-delà du par-delà sur la rive du satori. »

237
00:33:01,240 --> 00:33:14,420
Si nombreux que soient les êtres, je fais le vœu de les sauver tous.

238
00:33:14,600 --> 00:33:23,095
Si nombreuses que soient les passions, je fais le vœu de les vaincre toutes.

239
00:33:25,635 --> 00:33:34,292
Si nombreux que soient les Dharma, je fais le vœu de les acquérir tous.

240
00:33:35,112 --> 00:33:43,700
Si parfait que soit un Bouddha, je fais le vœu de le devenir.

241
00:33:44,085 --> 00:33:48,247
Que les mérites de cette récitation

242
00:33:48,247 --> 00:33:52,768
pénètrent tous les êtres en tous lieux,

243
00:33:52,768 --> 00:33:57,538
afin que nous tous, les êtres sensibles,

244
00:33:57,538 --> 00:34:03,129
nous puissions réaliser ensemble la voie de Bouddha.

245
00:34:05,332 --> 00:34:16,597
Tous les Bouddhas passés, présents et futurs dans les dix directions.

246
00:34:17,517 --> 00:34:26,629
Tous les Bodhisattvas et les patriarches

247
00:34:27,869 --> 00:34:38,142
Le Sutra de la « Sagesse qui va au-delà »

248
00:34:53,546 --> 00:34:58,216
Sanpaï !

249
00:37:25,590 --> 00:37:28,060
Ouf !

250
00:37:46,750 --> 00:37:48,965
Bonjour tout le monde !

251
00:37:57,955 --> 00:38:03,310
J’espère que vous êtes contents de fin de la pandémie !

252
00:38:32,700 --> 00:38:34,600
Hola Paola !
