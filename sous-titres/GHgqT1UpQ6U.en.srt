1
00:00:10,260 --> 00:00:23,160
Every day, all over the world, the members of the Kosen sangha practice zazen together. 

2
00:00:24,360 --> 00:00:28,520
I have told you about Master Deshimaru’s childhood. 

3
00:00:28,820 --> 00:00:30,820
From his doubts, his research … 

4
00:00:30,820 --> 00:00:35,760
Until meeting Kodo Sawaki and finding his zazen pose. 

5
00:00:36,040 --> 00:00:39,100
Finding your own zazen pose: 

6
00:00:40,240 --> 00:00:41,860
"What a pleasure!" 

7
00:00:42,240 --> 00:00:46,100
I believe those who got to know their zazen attitude … 

8
00:00:46,100 --> 00:00:49,120
Have not wasted their time on this planet. 

9
00:00:49,500 --> 00:00:52,540
It is an immeasurable gift .. 

10
00:00:52,540 --> 00:00:54,239
Even if in this attitude … 

11
00:00:54,540 --> 00:00:59,920
There is also suffering and fear. 

12
00:01:00,320 --> 00:01:01,960
Everything is present in this attitude. 

13
00:01:01,960 --> 00:01:06,160
But you look at it from above. 

14
00:01:06,500 --> 00:01:10,740
Before he learned the lessons of Kodo Sawaki .. 

15
00:01:12,040 --> 00:01:15,980
Master Deshimaru had studied the Bible. 

16
00:01:17,360 --> 00:01:21,020
He was a bit in love with a Dutch girl .. 

17
00:01:23,020 --> 00:01:24,820
A minister’s daughter. 

18
00:01:24,960 --> 00:01:28,380
He took the opportunity to study the Bible and English. 

19
00:01:28,720 --> 00:01:32,140
Actually, he studied English from the study of the Bible. 

20
00:01:32,140 --> 00:01:37,060
He visited Christian churches and sang hymns. 

21
00:01:37,220 --> 00:01:41,360
I’m telling you this because it’s Easter. 

22
00:01:41,660 --> 00:01:51,880
And now Jesus is between his grave and the fourth or fifth dimension. 

23
00:01:52,340 --> 00:01:56,580
Sensei told us that when he came to France .. 

24
00:01:57,000 --> 00:02:02,360
He had brought his Bible and the hymns of his youth. 

25
00:02:02,680 --> 00:02:07,940
During an Easter sesshin, he gave us a kiss about Christ. 

26
00:02:08,280 --> 00:02:11,720
According to the Gospel of Luke .. 

27
00:02:11,720 --> 00:02:15,620
So that Christ … 

28
00:02:16,140 --> 00:02:20,240
Could speak out for Pontius Pilatus to be judged .. 

29
00:02:20,600 --> 00:02:26,240
Pontius Pilate called the chief priests .. 

30
00:02:26,240 --> 00:02:28,820
And questioned Jesus in their presence. 

31
00:02:29,180 --> 00:02:30,340
He says to them: 

32
00:02:30,600 --> 00:02:33,020
"You introduced this man to me as … 

33
00:02:33,020 --> 00:02:39,820
Someone who is leading the people in the wrong direction. " 

34
00:02:40,280 --> 00:02:47,640
"I questioned him and found nothing bad in this man." 

35
00:02:48,000 --> 00:02:50,260
"I haven’t found any of the motives you’re accusing him of." 

36
00:02:50,580 --> 00:02:56,940
"Herod has also found no reason to charge." 

37
00:02:57,260 --> 00:03:02,620
"I conclude that this man has done nothing to deserve the death penalty." 

38
00:03:03,240 --> 00:03:06,480
At that time, the people exclaimed in 1 voice: 

39
00:03:06,480 --> 00:03:15,100
"Dead to Jesus, release Barabbas!" 

40
00:03:16,240 --> 00:03:24,280
"Barabbas was jailed for disorderly conduct and murder." 

41
00:03:24,620 --> 00:03:29,100
Pontius Pilatus offered .. 

42
00:03:29,100 --> 00:03:32,200
To release one of the prisoners. 

43
00:03:32,800 --> 00:03:38,480
But the people said, "Free Barabbas!" 

44
00:03:38,940 --> 00:03:44,260
Pontius Pilate addresses people again: 

45
00:03:44,260 --> 00:03:49,640
And he says them again and speaks to the people: 

46
00:03:49,960 --> 00:03:53,820
"Be reasonable, Jesus did nothing wrong .. 

47
00:03:53,820 --> 00:03:55,800
He just talked, that’s all! " 

48
00:03:55,800 --> 00:03:59,620
Everyone yelled, "Crucify him!" 

49
00:03:59,860 --> 00:04:02,320
Pontius Pilate finally said: 

50
00:04:02,620 --> 00:04:04,340
"As you wish.. 

51
00:04:04,340 --> 00:04:06,300
It is not my problem .. " 

52
00:04:07,200 --> 00:04:11,640
And in response he spoke the famous phrase: 

53
00:04:11,640 --> 00:04:13,500
"I was my hands in innocence!" 

54
00:04:13,560 --> 00:04:17,540
This expression means that .. 

55
00:04:17,540 --> 00:04:19,960
You should wash your hands as often as possible .. 

56
00:04:20,140 --> 00:04:22,320
To avoid the coronavirus. 

57
00:04:22,400 --> 00:04:24,920
You have to say every time, "I wash my hands in innocence!" 

58
00:04:25,240 --> 00:04:29,880
This is a teaching from Deshimaru, he says this. 

59
00:04:30,360 --> 00:04:35,760
"Thus, Jesus was condemned by public opinion." 

60
00:04:36,020 --> 00:04:39,140
"Mass psychology killed Christ." 

61
00:04:39,380 --> 00:04:44,200
"Mimicry, refusal to question herself … 

62
00:04:44,200 --> 00:04:46,160
That killed Christ. " 

63
00:04:46,300 --> 00:04:48,940
The crowd, the crowd .. 

64
00:04:48,940 --> 00:04:54,800
Contains in itself a powerful energy .. 

65
00:04:54,800 --> 00:04:57,720
Which can be both negative and dangerous. 

66
00:04:57,800 --> 00:05:02,900
It is different when we come together to do zazen. 

67
00:05:03,180 --> 00:05:05,640
Each of us looks at ourselves .. 

68
00:05:05,640 --> 00:05:11,820
While in harmony with the group. 

69
00:05:12,160 --> 00:05:16,260
This is especially important for now with Zazoom! 

70
00:05:16,680 --> 00:05:20,520
For example, I burned incense in my room. 

71
00:05:20,920 --> 00:05:25,260
Sensei often said, "In zazen you are alone." 

72
00:05:25,640 --> 00:05:28,220
But if you practice zazen in a dojo .. 

73
00:05:28,220 --> 00:05:31,460
We are in a group, even when we are alone. 

74
00:05:31,820 --> 00:05:35,260
But then there is no emotional confusion. 

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki used to say about this: 

76
00:05:38,320 --> 00:05:41,640
"We shouldn’t be living in group foolishness." 

77
00:05:42,020 --> 00:05:45,680
"And don’t keep this aberration for the real experience." 

78
00:05:45,940 --> 00:05:48,480
"This is the fate of the common people .. 

79
00:05:48,480 --> 00:05:50,540
Who have no other way of looking at it .. 

80
00:05:50,860 --> 00:05:54,180
Then with the eyes of collective imbecility. " 

81
00:05:54,700 --> 00:06:02,080
The Buddha gave a public lesson once a week. 

82
00:06:02,600 --> 00:06:05,620
All disciples, monks and laymen .. 

83
00:06:05,620 --> 00:06:08,360
Gathered outside .. 

84
00:06:08,360 --> 00:06:11,000
Certainly in privileged places. 

85
00:06:11,880 --> 00:06:14,960
The Buddha gave a kiss. 

86
00:06:15,320 --> 00:06:16,980
And everyone was in zazen .. 

87
00:06:17,960 --> 00:06:19,880
And listened to him .. 

88
00:06:19,980 --> 00:06:23,260
And then everyone went their way again. 

89
00:06:23,760 --> 00:06:25,560
Everyone practiced alone .. 

90
00:06:25,560 --> 00:06:27,960
A week, a month .. 

91
00:06:28,240 --> 00:06:30,100
And then returned regularly .. 

92
00:06:30,280 --> 00:06:32,680
to listen to the words of the Buddha. 

93
00:06:32,680 --> 00:06:37,840
Dojos only appeared in China. 

94
00:06:38,180 --> 00:06:40,900
And we started practicing as a group .. 

95
00:06:40,900 --> 00:06:42,560
With an order and rules. 

96
00:06:44,580 --> 00:06:48,280
And so the group is very powerful .. 

97
00:06:48,800 --> 00:06:50,960
Both good and bad. 

98
00:06:51,200 --> 00:06:53,020
Sensei actually said: 

99
00:06:53,020 --> 00:06:57,240
"Jesus Christ gave his body the taste of death." 

100
00:06:57,620 --> 00:07:01,700
"By the deep lake of nothingness and despair." 

101
00:07:02,180 --> 00:07:06,420
"He has become a symbol of eternal life." 

102
00:07:06,740 --> 00:07:09,180
"His life is identical to that of a Zen koan." 

103
00:07:09,380 --> 00:07:11,380
This is what Sensei said. 

