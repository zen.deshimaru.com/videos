1
00:00:03,300 --> 00:00:07,060
Que hora es 

2
00:00:07,240 --> 00:00:11,780
El tiempo es solo ahora. 

3
00:00:12,080 --> 00:00:15,080
Zen es "aquí y ahora". 

4
00:00:15,340 --> 00:00:16,820
Es importante 

5
00:00:17,220 --> 00:00:25,160
Pero comprender el pasado es un truco extraordinario para comprender el presente. 

6
00:00:25,580 --> 00:00:31,640
Y comprender el futuro también es muy importante. 

7
00:00:32,460 --> 00:00:34,780
¿Cómo pasas el tiempo? 

8
00:00:35,360 --> 00:00:38,400
El tiempo es una cuestión de frecuencia. 

9
00:00:38,700 --> 00:00:43,640
Cuanto mayor es la frecuencia, más rápido es el tiempo, eso es todo. 

10
00:00:44,100 --> 00:00:49,760
La conciencia también evoluciona de acuerdo con las frecuencias. 

11
00:00:59,700 --> 00:01:03,440
Caro es solo una impresión material. 

12
00:01:03,820 --> 00:01:06,000
Es una delicia. 

13
00:01:06,400 --> 00:01:09,100
¿Qué hay de zazen? 

14
00:01:09,400 --> 00:01:16,700
Para un amante del zazen, el tiempo zazen es muy valioso. 

15
00:01:17,120 --> 00:01:22,560
Llegamos al dojo, y solo tenemos una hora y media, máximo dos horas. 

16
00:01:23,020 --> 00:01:27,720
Para poder vivir en esta dimensión. 

17
00:01:28,000 --> 00:01:31,520
Y poner las cosas en su lugar. 

18
00:01:31,780 --> 00:01:35,440
Tienes que ahorrar tu tiempo zazen. 

19
00:01:35,540 --> 00:01:38,760
Lleva un tiempo llegar al fondo. 

20
00:01:39,000 --> 00:01:40,760
Y estar en buena postura. 

21
00:01:41,060 --> 00:01:43,960
¿El tiempo está relacionado con el karma? 

22
00:01:44,720 --> 00:01:46,920
El pasado solo ahora existe. 

23
00:01:47,280 --> 00:01:57,240
Cada "ahora" corresponde a una frecuencia específica en tiempo pasado con su "ahora". 

24
00:01:57,620 --> 00:02:00,900
Tu pasado cambia constantemente. 

25
00:02:01,000 --> 00:02:10,700
Pero cuando vas a tu pasado, olvidas tu presente. 

26
00:02:11,040 --> 00:02:13,560
Y tu presente se convierte en el pasado. 

27
00:02:13,900 --> 00:02:17,720
Y entras en algo que se vuelve cada vez más complicado. 

28
00:02:17,940 --> 00:02:19,340
En karma 

29
00:02:19,820 --> 00:02:24,160
"Es culpa de mi karma …" 

30
00:02:25,240 --> 00:02:27,960
"Mis padres, mis cosas …" 

31
00:02:28,280 --> 00:02:30,440
"Mi vida es complicada …" 

32
00:02:30,700 --> 00:02:32,380
"Mis antepasados ​​eran alcohólicos …" 

33
00:02:32,660 --> 00:02:35,420
Etc. Y te pierdes allí. 

34
00:02:35,780 --> 00:02:40,060
Te identificas con este personaje. 

35
00:02:40,340 --> 00:02:41,720
Te estás volviendo más débil. 

36
00:02:42,060 --> 00:02:44,140
Ya no eres dios. 

37
00:02:44,420 --> 00:02:45,720
Estas confundido 

38
00:02:46,080 --> 00:02:47,760
Estás perdido en la materialización. 

39
00:02:48,060 --> 00:02:53,300
Con nuestro estado actual, siempre hay causas en el pasado. 

40
00:02:53,680 --> 00:02:58,540
Si cambiamos la situación actual, el pasado también cambiará. 

41
00:02:59,160 --> 00:03:01,940
¿Qué hay del futuro? 

42
00:03:02,340 --> 00:03:05,300
El futuro es una historia diferente … 

