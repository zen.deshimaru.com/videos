1
00:00:51,238 --> 00:00:57,194
Events related to this pandemic

2
00:00:58,324 --> 00:01:02,861
certify Dogen’s Zen

3
00:01:03,081 --> 00:01:05,538
and the Zen of Deshimaru.

4
00:01:05,862 --> 00:01:09,802
Impermanence, we live it

5
00:01:11,770 --> 00:01:16,620
Interdependence is something we also experience.

6
00:01:21,799 --> 00:01:24,759
The only moment that matters,

7
00:01:26,209 --> 00:01:29,069
is right now.

8
00:01:48,301 --> 00:01:49,821
The Buddha,

9
00:01:50,388 --> 00:01:53,538
our inner master.

10
00:01:53,942 --> 00:01:56,332
Dharma, the way, the practice.

11
00:01:56,671 --> 00:01:59,091
The sangha, the virtual sangha.

12
00:02:52,434 --> 00:02:54,384
Hello everyone!

13
00:02:54,904 --> 00:02:57,614
And thank you for doing zazen together!
