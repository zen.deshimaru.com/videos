1
00:01:31,170 --> 00:01:35,220
Dans Soto Zen, que nous pratiquons, nous disons: "Ne cherchez pas une grande illumination". 

2
00:01:35,560 --> 00:01:38,407
L’illumination, en soi, n’existe pas. 

3
00:01:38,407 --> 00:01:40,170
Pourquoi existerait-elle? 

4
00:01:40,780 --> 00:01:45,720
Vous devez regarder la nature et les choses telles qu’elles sont pour voir la vérité. 

5
00:01:46,200 --> 00:01:49,663
L’éclairage est absolument naturel. 

6
00:01:49,663 --> 00:01:52,780
Ce n’est pas une condition spéciale. 

7
00:01:53,320 --> 00:01:59,400
Dans le zen, il n’y a pas de condition spéciale dans l’illumination à moins qu’il n’y ait une maladie de l’esprit. 

8
00:01:59,700 --> 00:02:04,760
Il n’y a pas d’illumination spéciale en soi et il ne faut pas en chercher une. 

9
00:02:05,540 --> 00:02:07,433
De nombreux maîtres y insistent. 

10
00:02:07,433 --> 00:02:08,681
C’est extrêmement important. 

11
00:02:08,681 --> 00:02:11,000
L’illumination est à chaque instant, à chaque zazen. 

12
00:02:11,320 --> 00:02:15,580
Je parlais à un spécialiste des psychotropes … 

13
00:02:15,790 --> 00:02:20,700
conçu pour atteindre des états de conscience spéciaux. 

14
00:02:21,130 --> 00:02:24,686
Il a soutenu que le zazen était un moyen d’atteindre l’illumination. 

15
00:02:24,686 --> 00:02:25,889
Ce n’est pas vrai du tout. 

16
00:02:26,350 --> 00:02:31,379
Zazen est l’illumination elle-même. 

17
00:02:32,410 --> 00:02:33,254
Dogen a déclaré: 

18
00:02:33,254 --> 00:02:36,269
"Je suis allé en Chine et j’ai rencontré un vrai maître." 

19
00:02:37,390 --> 00:02:41,729
"Ce que j’en ai appris, c’est que les yeux sont horizontaux et le nez vertical." 

20
00:02:42,370 --> 00:02:45,810
"Et que l’on ne doit pas être dupe, ni par soi-même ni par les autres." 

21
00:03:44,750 --> 00:03:46,750
et tous les muscles du ventre 

22
00:03:47,269 --> 00:03:52,358
doit être détendu pour donner l’impression que les intestins pèsent. 

23
00:04:09,010 --> 00:04:15,420
si nous nous concentrons complètement dans la posture à ce moment-là, nous pouvons laisser les pensées 

24
00:04:15,850 --> 00:04:17,850
sans s’arrêter là. 

25
00:04:25,810 --> 00:04:28,914
La plus belle chose que vous puissiez faire avec votre corps est zazen. 

26
00:04:28,914 --> 00:04:30,419
C’est la splendeur de la nature. 

27
00:04:30,849 --> 00:04:33,728
Il inspire le calme, l’harmonie, la force. 

28
00:04:33,728 --> 00:04:36,539
Tout est contenu dans la posture de zazen 

29
00:04:39,969 --> 00:04:43,447
Au camp d’été, j’essaie d’encourager la pratique. 

30
00:04:43,447 --> 00:04:46,111
L’ambiance dans le dojo, les postures. 

31
00:04:46,111 --> 00:04:47,909
Une personne qui dérange cela 

32
00:04:50,540 --> 00:04:53,940
avec son histoire personnelle, je suis prêt à la tuer. 

33
00:04:57,120 --> 00:04:59,120
C’est la seule règle que je connaisse: la compassion. 

34
00:05:00,600 --> 00:05:02,477
Sensei exhibait son kyosaku: 

35
00:05:02,477 --> 00:05:04,160
"le personnel de compassion!" 

36
00:05:04,660 --> 00:05:11,080
Parfois, il avait réussi 20 tirs consécutifs. 

37
00:05:14,440 --> 00:05:19,640
Il disait: "Sensei, très compatissant!" 

38
00:05:26,280 --> 00:05:32,940
Lorsqu’un disciple veut être aidé dans sa méditation en étant frappé à l’épaule avec un bâton, il le demande. 

39
00:05:33,180 --> 00:05:36,800
Parce qu’il sent que son attention commence à décliner. 

40
00:05:37,140 --> 00:05:41,360
Lorsque vous vous sentez somnolent. 

41
00:05:41,580 --> 00:05:46,300
Ou lorsque vous sentez des complications assombrir votre cerveau. 

42
00:05:46,540 --> 00:05:49,520
Plus si calme. 

43
00:05:49,940 --> 00:05:54,600
Si vous avez sommeil ou si votre esprit est agité … 

44
00:06:01,240 --> 00:06:03,240
Gasshô! 

45
00:06:21,580 --> 00:06:23,580
Zazen continue pendant 

46
00:06:23,840 --> 00:06:25,233
pendant plusieurs heures chaque jour. 

47
00:06:25,233 --> 00:06:25,929
Et parfois, de jour comme de nuit. 

48
00:06:26,180 --> 00:06:30,291
Mais il est interrompu par une méditation en marchant. 

49
00:06:30,291 --> 00:06:33,489
Autrement dit, l’état intérieur reste, 

50
00:06:34,280 --> 00:06:38,799
mais la posture est différente. 

51
00:06:39,940 --> 00:06:44,040
Toute l’attention est concentrée sur le pouce. 

52
00:06:44,600 --> 00:06:47,500
Chaque respiration correspond à un demi-pas. 

53
00:06:58,880 --> 00:07:05,020
Les yeux sont au sol 3 mètres devant. 

54
00:07:13,340 --> 00:07:18,580
C’est la même respiration qu’en zazen, lors de la méditation en posture assise. 

55
00:07:20,740 --> 00:07:28,300
La respiration zen est l’opposé de la respiration normale. 

56
00:07:28,560 --> 00:07:30,940
Lorsque vous demandez à quelqu’un de respirer, 

57
00:07:31,240 --> 00:07:34,600
le réflexe est de prendre une profonde respiration. 

58
00:07:35,060 --> 00:07:38,860
En zen, on va se concentrer 

59
00:07:39,080 --> 00:07:42,740
surtout sur l’expiration. 

60
00:07:43,040 --> 00:07:46,780
L’expiration est longue, calme et profonde. 

61
00:07:54,360 --> 00:07:56,160
Il est ancré dans le sol. 

62
00:07:56,160 --> 00:07:58,860
Cela peut durer jusqu’à une, deux minutes. 

63
00:07:59,080 --> 00:08:02,940
Selon son état de concentration et de calme. 

64
00:08:03,280 --> 00:08:09,600
Il peut être très doux, imperceptible. 

65
00:08:09,860 --> 00:08:15,640
Nous étudions d’abord cette respiration en insistant, comme une vache qui moue. 

66
00:08:38,020 --> 00:08:44,640
À la fin de l’expiration, nous laissons l’inspiration se faire automatiquement. 

67
00:08:45,880 --> 00:08:49,640
On détend tout, on ouvre les poumons. 

68
00:08:50,300 --> 00:08:55,380
Nous laissons l’inspiration venir automatiquement. 

69
00:08:55,820 --> 00:08:58,689
Nous ne mettons pas beaucoup d’intention dans l’inspiration. 

70
00:08:59,600 --> 00:09:03,129
Ce faisant, vous comprendrez 

71
00:09:04,550 --> 00:09:06,550
cette respiration est tout. 

72
00:09:07,910 --> 00:09:09,910
C’est un cercle. 

73
00:09:10,070 --> 00:09:12,070
Souvent, les maîtres dessinent un cercle. 

74
00:09:12,560 --> 00:09:14,560
Il s’agit de respirer. 

75
00:09:15,040 --> 00:09:18,880
Cette respiration a eu beaucoup d’influence, 

76
00:09:19,340 --> 00:09:23,200
surtout dans les arts martiaux. 

77
00:09:23,480 --> 00:09:29,160
Dans les arts martiaux japonais, la respiration zen est utilisée. 

78
00:09:29,580 --> 00:09:34,840
De nombreux grands maîtres des arts martiaux comme Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
a pratiqué le zen et est devenu les disciples de grands maîtres. 

80
00:09:39,940 --> 00:09:45,480
Et ont adapté l’enseignement du Zen et de la respiration à leur art martial. 

81
00:09:45,820 --> 00:09:53,860
Au final, la respiration dépasse les techniques et est totalement libre. 

82
00:09:58,080 --> 00:10:03,340
L’enseignement du Bouddha à ses disciples est très simple. 

83
00:10:03,640 --> 00:10:09,440
Il est basé sur les actes fondamentaux de l’être humain. 

84
00:10:09,660 --> 00:10:11,260
Comment bouger. 

85
00:10:11,480 --> 00:10:14,120
Comment manger, respirer. 

86
00:10:14,440 --> 00:10:15,836
Comment penser. 

87
00:10:15,836 --> 00:10:20,860
Et enfin, comment s’asseoir pour réaliser sa divinité. 

88
00:11:24,940 --> 00:11:28,702
Genmai est la nourriture des moines depuis le Bouddha Shakyamuni. 

89
00:11:28,702 --> 00:11:31,800
Nous mélangeons les légumes que nous avons sous la main avec du riz. 

90
00:11:32,280 --> 00:11:39,440
Nous cuisinons depuis très longtemps, avec beaucoup d’attention et de concentration. 

91
00:11:39,780 --> 00:11:42,730
C’est une nourriture transmise par le Bouddha. 

92
00:11:42,730 --> 00:11:45,920
En Chine et au Japon, nous mangeons la même nourriture. 

93
00:11:47,660 --> 00:11:49,980
C’est vraiment le corps du Bouddha. 

94
00:11:50,280 --> 00:11:53,080
Quand nous allons prendre notre repas, 

95
00:11:53,440 --> 00:11:55,560
vous devez vous poser la question: 

96
00:11:55,900 --> 00:11:59,460
"Pourquoi vais-je manger?" 

97
00:11:59,680 --> 00:12:01,580
Un animal ou un lout 

98
00:12:01,960 --> 00:12:05,680
ne va pas se demander et sauter de haut en bas dans son assiette. 

99
00:12:06,040 --> 00:12:09,820
Évidemment, c’est pour la survie, mais la survie pour quoi? 

100
00:12:10,060 --> 00:12:13,520
C’est ce que cela dit dans les soutras qui sont chantés pendant les repas. 

101
00:12:13,780 --> 00:12:15,240
"Pourquoi mangeons-nous?" 

102
00:12:15,520 --> 00:12:17,591
"D’où vient cette nourriture? 

103
00:12:17,800 --> 00:12:18,860
Qui l’a préparé? " 

104
00:12:19,180 --> 00:12:21,140
"Qui a cultivé le riz et les légumes?" 

105
00:12:21,400 --> 00:12:26,140
"Qui a fait la cuisine?" 

106
00:12:26,500 --> 00:12:29,960
Nous dédions le repas et remercions tous ces gens. 

107
00:12:30,240 --> 00:12:32,140
Et en pleine conscience, 

108
00:12:32,620 --> 00:12:37,340
Je décide de manger cette nourriture pour 

109
00:12:37,780 --> 00:12:44,620
l’évolution que je suis venu accomplir sur cette terre, dans ce monde matériel. 

110
00:12:52,360 --> 00:12:56,900
L’EMS est à 10h30. 6 personnes pour la cuisine. 

111
00:13:01,080 --> 00:13:04,080
Deux personnes pour la grande plongée. 

112
00:13:05,940 --> 00:13:10,340
Trois personnes pour le service. 

113
00:13:19,600 --> 00:13:23,580
Samu ne fait pas spécialement de la maçonnerie, balaie … 

114
00:13:24,190 --> 00:13:27,989
ou faire la vaisselle. 

115
00:13:28,540 --> 00:13:30,540
Samu est toute l’action 

116
00:13:30,760 --> 00:13:32,939
(Écrire un livre est un samu) 

117
00:13:35,320 --> 00:13:37,320
réalisée sans 

118
00:13:37,640 --> 00:13:38,860
intention 

119
00:13:39,180 --> 00:13:40,160
caché 

120
00:13:44,950 --> 00:13:47,249
pas de motifs secrets. 

121
00:13:47,980 --> 00:13:50,048
C’est comme votre travail au quotidien. 

122
00:13:50,048 --> 00:13:52,289
Vous pouvez le prendre comme un enrichissement personnel 

123
00:13:52,839 --> 00:13:57,749
ou comme une sorte de cadeau que vous vous offrez à vous-même et à tous les autres en même temps. 

124
00:13:59,320 --> 00:14:02,460
Parfois je considère 

125
00:14:03,400 --> 00:14:05,459
Certaines personnes peuvent le considérer comme une corvée. 

126
00:14:05,920 --> 00:14:09,390
Mais je parie que ceux qui sont blessés en zazen. 

127
00:14:16,400 --> 00:14:19,550
En tant que religieuse zen, 

128
00:14:23,040 --> 00:14:26,180
quelle attitude devez-vous adopter au travail? 

129
00:14:27,080 --> 00:14:30,100
Je ne sais pas, je n’ai jamais travaillé! 

130
00:14:33,960 --> 00:14:37,060
Faut-il en faire un samu? 

131
00:14:38,500 --> 00:14:39,520
Oui, c’est ça le point. 

132
00:14:39,740 --> 00:14:41,100
Je parlais de merde, hein. 

133
00:14:41,300 --> 00:14:47,500
Quand j’étais plus jeune, j’avais un travail de concierge. 

134
00:14:48,090 --> 00:14:54,920
J’ai nettoyé les escaliers et sorti les poubelles tous les matins à 5h00. 

135
00:14:55,140 --> 00:15:00,619
L’escalier, c’était un hourra. Il y avait de la merde de chien. 

136
00:15:00,920 --> 00:15:05,360
J’ai pensé: "Mais ce n’est pas le dojo ou le zen." 

137
00:15:05,760 --> 00:15:09,760
"Tant pis. Pour moi, c’est comme un temple." 

138
00:15:10,320 --> 00:15:14,160
Je l’ai fait comme un samouraï. 

139
00:15:14,480 --> 00:15:17,040
Et Sensei m’a demandé: 

140
00:15:17,420 --> 00:15:20,080
"Stéphane, j’aimerais que tu nettoies le dojo." 

141
00:15:20,400 --> 00:15:23,660
"Oui, je le ferai. Vendredi, je suis libre." 

142
00:15:25,440 --> 00:15:27,920
"Je le ferai vendredi prochain." 

143
00:15:28,580 --> 00:15:29,660
Il m’a dit: 

144
00:15:29,940 --> 00:15:32,500
"Non, je veux que tu le fasses tous les jours." 

145
00:15:33,120 --> 00:15:34,272
Il a ensuite dit: 

146
00:15:34,520 --> 00:15:37,440
"Une journée sans travail, une journée sans nourriture." 

147
00:15:37,860 --> 00:15:40,720
"Mais ça n’a rien à voir avec la nourriture!" 

148
00:15:41,000 --> 00:15:44,440
"Une journée sans travailler, une journée sans manger de mushotoku!" 

149
00:15:44,780 --> 00:15:47,380
C’était une expression très originale: 

150
00:15:47,700 --> 00:15:50,380
"Vous devez manger du mushotoku!" 

151
00:15:54,760 --> 00:15:57,540
Ça ne voulait rien dire! 

152
00:15:58,180 --> 00:16:02,500
Dire que vous êtes un moine, pratiquer le samu est une révolution. 

153
00:16:02,880 --> 00:16:05,880
Il faut faire la révolution, je l’ai toujours dit! 

154
00:16:06,920 --> 00:16:10,657
Mais je ne sais pas comment utiliser les cocktails Molotov! 

155
00:16:11,000 --> 00:16:14,100
Donc, dans son travail, vous devez pratiquer le samu. 

156
00:16:14,400 --> 00:16:19,780
Pour résoudre tous les problèmes du monde, il vous suffit de trouver cela. 

157
00:16:20,100 --> 00:16:22,060
Les gens n’ont plus l’esprit du samu. 

158
00:16:22,520 --> 00:16:23,920
Ils ne mangent plus de mushotoku. 

159
00:16:24,160 --> 00:16:25,580
Ils mangent du hamburger et boivent du Coca. 

160
00:16:26,180 --> 00:16:28,880
Alors, celui qui vous dit: 

161
00:16:29,580 --> 00:16:31,580
Mais il est 6h30, tu es le seul qui reste! 

162
00:16:31,880 --> 00:16:33,140
Tu lui dis: 

163
00:16:33,360 --> 00:16:36,160
"Moi, je pratique samu!" 

164
00:16:45,640 --> 00:16:47,640
"Parce que je veux manger du mushotoku!" 

165
00:16:49,780 --> 00:16:51,200
"Qu’est-ce que le mushotoku?" 

166
00:16:51,440 --> 00:16:52,780
"J’en veux aussi!" 

167
00:16:57,040 --> 00:17:01,200
Nous devons enseigner aux gens sans avoir peur. 

168
00:17:03,180 --> 00:17:06,660
Soyez fiers d’être mes disciples et d’être moines! 

169
00:17:07,000 --> 00:17:09,000
Et d’être disciples de Deshimaru. 

