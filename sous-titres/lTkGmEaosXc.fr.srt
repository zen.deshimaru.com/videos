1
00:00:18,400 --> 00:00:23,380
Si vous voulez expliquer à quelqu’un comment médite, alors il vaut mieux lui mettre le

2
00:00:23,380 --> 00:00:29,619
la posture dans laquelle vous êtes assis. Encore plus fort. La méditation est en fait appelée

3
00:00:29,619 --> 00:00:37,360
la pratique de la posture. Vous pratiquez la position de Bouddha. Et ce, parce que

4
00:00:37,360 --> 00:00:43,239
lorsque vous méditez, votre corps et votre l’esprit en fait un.

5
00:00:43,239 --> 00:00:48,190
Donc cette posture, la posture dans lequel vous êtes assis, affecte votre esprit. Et

6
00:00:48,190 --> 00:00:51,070
et vice versa : votre état d’esprit affecte également votre

7
00:00:51,070 --> 00:00:53,369
posture.

8
00:00:53,470 --> 00:00:58,000
Alors maintenant, adoptons cette posture étude. Mais vous voyez, d’abord

9
00:00:58,000 --> 00:01:01,989
Je suis assis sur un oreiller. Cet oreiller fait concevable

10
00:01:01,989 --> 00:01:11,830
que mon bassin prenne cette position : mon le bassin est en fait légèrement incliné par rapport au précédent, est presque vertical, et que je peux détendre mon ventre.

11
00:01:11,830 --> 00:01:14,880
Vous êtes donc assis sur un oreiller.

12
00:01:16,180 --> 00:01:20,590
Un tel oreiller devient un zafu a appelé. La hauteur d’un oreiller est donc

13
00:01:20,590 --> 00:01:25,360
assez important : Le baiser devrait être comme ça

14
00:01:25,360 --> 00:01:29,650
que c’est précisément le pelvis lui-même qui s’incline vers l’avant,

15
00:01:29,650 --> 00:01:32,140
sans se soucier sans avoir besoin de se fatiguer les muscles

16
00:01:32,140 --> 00:01:36,010
l’utilisation. Donc, si vous avez un gros corps, c’est

17
00:01:36,010 --> 00:01:42,010
pratique pour avoir un gros oreiller, un oreiller bien rempli et si vous en avez

18
00:01:42,010 --> 00:01:46,570
plus petit, vous pouvez obtenir un oreiller plus fin

19
00:01:46,570 --> 00:01:49,540
Il existe sur le marché des oreillers qui ont juste un compagnon.

20
00:01:49,540 --> 00:01:52,840
C’est un oreiller qui contient du kapok et vous pouvez le compléter,

21
00:01:52,840 --> 00:01:57,570
pour que vous puissiez l’obtenir exactement au bon endroit ...l’élever pour vous.

22
00:01:59,350 --> 00:02:05,290
Dans cette explication, je suppose que le de la posture dans laquelle vous êtes assis

23
00:02:05,290 --> 00:02:09,039
sur un oreiller avec les genoux au sol, ce sera la position en demi-boucle

24
00:02:09,039 --> 00:02:13,000
a appelé. Certaines personnes ont difficulté à adopter cette attitude,

25
00:02:13,000 --> 00:02:17,739
ils préfèrent s’asseoir sur un banc. Ils le peuvent, même dans ce cas, vous pouvez

26
00:02:17,739 --> 00:02:20,470
votre dos dans la bonne position ont

27
00:02:20,470 --> 00:02:24,640
Vous pouvez aussi vous asseoir sur une chaise, qui va Je n’explique pas tout cela dans

28
00:02:24,640 --> 00:02:27,549
cette session Je suppose ici que l’attitude sur un

29
00:02:27,549 --> 00:02:30,870
Embrasser les deux genoux au sol

30
00:02:31,400 --> 00:02:36,090
Si vous adoptez cette attitude, La première chose que vous faites est : vous apportez un

31
00:02:36,090 --> 00:02:41,700
pied contre l’oreiller, genou sur le à la fois le sol et

32
00:02:41,700 --> 00:02:46,590
de l’autre jambe, mettez votre pied sur votre la cuisse et ce genou ici. Vous êtes donc assis

33
00:02:46,590 --> 00:02:51,600
sur jambes croisées. Ce pied contre la

34
00:02:51,600 --> 00:02:55,250
embrasser, celui-là en haut

35
00:02:55,470 --> 00:03:01,680
c’est la moitié de la pose du lotus. Si c’est trop difficile pour vous, si vous avez été un peu...

36
00:03:01,680 --> 00:03:05,070
sont raides dans les articulations de la hanche

37
00:03:05,070 --> 00:03:09,660
zazen, la méditation vous donnera l’aider à se détendre.

38
00:03:09,660 --> 00:03:14,970
Maintenant, si vous êtes encore un peu gêné, vous pouvez... vous éventuellement cette jambe plus en avant

39
00:03:14,970 --> 00:03:17,670
de l’entreprise, ce pied plus loin à soumettre et aussi

40
00:03:17,670 --> 00:03:22,800
vous aurez toujours vos deux genoux sur le sol. Toujours en cours

41
00:03:22,800 --> 00:03:27,780
votre genou un peu plus haut, alors dans ce cas, vous pouvez prendre une serviette

42
00:03:27,780 --> 00:03:31,740
et ensuite vous essayez de la rendre aussi fine que possible sous vous genou à terre,

43
00:03:31,740 --> 00:03:35,610
une serviette aussi fine que possible, juste ce dont vous avez besoin besoin de s’agenouiller

44
00:03:35,610 --> 00:03:40,970
soutien. Même dans ce cas, la deux genoux au sol.

45
00:03:44,080 --> 00:03:50,480
Donc ces genoux au sol, votre bassin sur l’oreiller,

46
00:03:50,480 --> 00:03:54,650
cela forme un triangle et ce triangle on pourrait appeler cela la

47
00:03:54,650 --> 00:03:59,360
piédestal de la posture, un piédestal solide et stable.

48
00:03:59,360 --> 00:04:03,590
Votre bassin s’étire, votre bassin s’incline légèrement vers l’avant, c’est presque

49
00:04:03,590 --> 00:04:12,920
verticalement, et votre colonne vertébrale est précisément équilibrée au dessus de votre bassin et de la colonne qui s’étire

50
00:04:12,920 --> 00:04:16,190
elle-même vers le haut et qui s’étend jusqu’à la

51
00:04:16,190 --> 00:04:19,030
couronne de votre tête.

52
00:04:19,520 --> 00:04:22,970
Pour que vous puissiez voir, vous pouvez sentir vos genoux sur le sol

53
00:04:22,970 --> 00:04:28,160
et d’autre part, la couronne de votre tête s’étend vers le haut

54
00:04:28,160 --> 00:04:30,580
à l’étage.

55
00:04:32,540 --> 00:04:37,500
Ensuite, et c’est un détail important

56
00:04:37,500 --> 00:04:41,300
de la posture, votre menton est rétracté, sans spasme...

57
00:04:41,300 --> 00:04:45,320
cause. Donc vous n’êtes pas assis comme ça, mais vous n’êtes pas assis comme ça.

58
00:04:45,320 --> 00:04:47,780
Le menton est rétracté et le cou est tendu,

59
00:04:47,780 --> 00:04:51,980
vous le sentez très bien, nous disons aussi : votre cou s’ouvre

60
00:04:51,980 --> 00:04:56,060
à l’envers, c’est comme si vous alliez droit dans le sens de votre

61
00:04:56,060 --> 00:05:02,770
colonne vertébrale et cela aussi rend la posture éveillée.

62
00:05:06,360 --> 00:05:11,669
Un autre aspect important de la posture est la position des yeux.

63
00:05:11,669 --> 00:05:14,280
Vos yeux ne sont pas fermés pendant la méditation,

64
00:05:14,280 --> 00:05:17,599
ils sont mi-ouverts mi-ouverts, vous baissez les yeux

65
00:05:17,599 --> 00:05:21,629
45 degrés vers le bas. Vous pouvez également dire

66
00:05:21,629 --> 00:05:26,060
vos yeux sont à environ un mètre devant vous sur le sol.

67
00:05:30,750 --> 00:05:35,280
Et d’ailleurs, vous ne regardez pas, vous ne regardez pas activement

68
00:05:35,280 --> 00:05:41,270
Vous pouvez percevoir tout ce qui vous entoure sans y prêter attention.

69
00:05:44,189 --> 00:05:48,509
La position de vos mains : les mains ensemble forment en fait une sorte de

70
00:05:48,509 --> 00:05:54,990
de mini-posture. Votre main droite est en dessous de votre main gauche est en dessus et ils forment un

71
00:05:54,990 --> 00:05:58,710
bol. Vos pouces se touchent horizontalement et

72
00:05:58,710 --> 00:06:01,830
sont juste au-dessus de vos doigts du milieu. Vous voyez, le côté des mains est

73
00:06:01,830 --> 00:06:08,400
contre mon ventre. Plus précisément, vous pouvez voir que les deux doigts du milieu sont exactement

74
00:06:08,400 --> 00:06:12,449
sont les uns sur les autres, afin de ne pas glisser les mains les unes sur les autres

75
00:06:12,449 --> 00:06:22,100
mais juste ici et ces pouces se touchent très doucement.

76
00:06:22,200 --> 00:06:25,860
Afin de permettre à cette position des mains de se reposer correctement...

77
00:06:25,860 --> 00:06:30,300
Regardez si quelqu’un fait le lotus complet, alors l’autre jambe aussi

78
00:06:30,300 --> 00:06:34,409
est pliée, vous pouvez alors poser vos mains sur vos talons.

79
00:06:34,409 --> 00:06:39,330
Si ce n’est pas le cas, il est utile d’utiliser une serviette qui couvre votre

80
00:06:39,330 --> 00:06:46,080
pose vos cuisses et cela forme une sorte de hamac pour vos mains.

81
00:06:46,080 --> 00:06:49,250
Vous pouvez vous reposer sur vos mains.

82
00:06:53,289 --> 00:07:00,460
Et maintenant, vous concentrez votre attention sur votre posture, sur les différents points de la posture :

83
00:07:00,770 --> 00:07:03,550
menton rétracté,

84
00:07:06,050 --> 00:07:15,219
la position de vos yeux, de vos pouces, de vos mains.

85
00:07:18,249 --> 00:07:22,709
Vous détendez votre bouche et vos mâchoires.

86
00:07:23,229 --> 00:07:27,279
Votre langue est contre votre palais et le bout de votre langue juste derrière vous...

87
00:07:27,279 --> 00:07:29,459
dents de devant.

88
00:07:33,699 --> 00:07:36,689
Vous détendez vos épaules.

89
00:07:37,359 --> 00:07:48,329
Vos coudes sont légèrement écartés de votre corps et vous détendez votre ventre.

90
00:07:57,740 --> 00:08:02,960
Si vous restez assis comme ça pendant un certain temps, vous allez remarquer certaines choses

91
00:08:02,960 --> 00:08:06,889
dont vous n’êtes normalement pas conscient.

92
00:08:06,889 --> 00:08:16,069
La première chose que vous remarquez, c’est votre respiration. L’indice de la

93
00:08:16,069 --> 00:08:21,770
La respiration est : il va bien comme ça.

94
00:08:21,770 --> 00:08:27,139
Nous ne recommandons donc pas d’influencer artificiellement la respiration. Le plus grand

95
00:08:27,139 --> 00:08:31,370
L’influence de la respiration est basée sur la posture : si votre posture est correcte

96
00:08:31,370 --> 00:08:36,080
est équilibré, si vous pouvez vous détendre, vous étirer et vous relaxer en même temps,

97
00:08:36,200 --> 00:08:39,409
alors que la respiration se fera lentement, par elle-même

98
00:08:39,409 --> 00:08:43,070
de plus en plus profond, de plus en plus

99
00:08:43,070 --> 00:08:47,360
utilisez votre ventre. Votre expiration sera probablement plus longue

100
00:08:47,360 --> 00:08:53,060
et votre inhalation est courte et spontanée. Mais vous n’avez pas à influencer cela, c’est une

101
00:08:53,060 --> 00:08:58,580
processus qui se déroule de lui-même. En d’autres termes, la respiration est maintenant

102
00:08:58,580 --> 00:09:02,350
aussi bien que possible.

103
00:09:02,350 --> 00:09:09,100
Concentrez-vous sur la respiration.

104
00:09:09,560 --> 00:09:15,620
Une deuxième chose que vous allez remarquer, c’est que vous observez vos pensées,

105
00:09:15,620 --> 00:09:19,210
vous percevez que vous pensez.

106
00:09:20,230 --> 00:09:25,300
L’indice des pensées est le suivant :

107
00:09:25,300 --> 00:09:32,000
En fait, vous concentrez votre attention sur la posture, sur le suivi de votre respiration

108
00:09:32,000 --> 00:09:33,590
En d’autres termes : vous concentrez votre attention

109
00:09:33,590 --> 00:09:38,420
pas sur votre esprit, les pensées sont les bienvenues,

110
00:09:38,420 --> 00:09:45,590
tu ne vas pas réfléchir à une pensée. Nous utilisons l’expression :

111
00:09:45,590 --> 00:09:50,000
Les pensées passent comme des nuages dans le ciel.

112
00:09:50,000 --> 00:09:53,200
En pratique, c’est ce qu’il semble :

113
00:09:55,940 --> 00:10:00,070
vous méditez et une pensée vous vient à l’esprit...

114
00:10:01,250 --> 00:10:05,870
... vous remarquez que vous êtes dans une pensée, alors vous le laissez gentiment...

115
00:10:05,870 --> 00:10:12,580
et vous ramenez votre attention sur la posture et la respiration.

116
00:10:14,240 --> 00:10:18,370
Un peu plus tard, une nouvelle pensée me vient à l’esprit...

117
00:10:19,730 --> 00:10:28,010
... maintenant vous le remarquez, vous le laissez partir, vous le laissez gentiment partir et vous vous retournez

118
00:10:28,010 --> 00:10:30,880
retour à la posture.

119
00:10:33,250 --> 00:10:37,220
Peu importe le nombre de pensées ou de

120
00:10:37,220 --> 00:10:43,279
le peu de pensées que vous avez. Vous avez peut-être une vie très occupée, vous avez

121
00:10:43,279 --> 00:10:47,420
beaucoup d’excitation autour de vous, vous avez beaucoup de pensées et sur un

122
00:10:47,420 --> 00:10:51,940
Une autre fois, vous n’aurez pas beaucoup de pensées, cela n’a pas d’importance.

123
00:10:52,010 --> 00:10:56,540
Nous comparons le processus avec les pensées d’une bouteille d’eau, d’un

124
00:10:56,540 --> 00:11:02,930
verre avec de l’eau, dans lequel il y a une poignée de sable, une poignée de terre, et si vous avez un tel verre

125
00:11:02,930 --> 00:11:06,100
tremble et vous le posez alors l’eau est trouble

126
00:11:06,100 --> 00:11:10,730
si vous attendez un peu, cette terre va lentement toucher le fond et devenir

127
00:11:10,730 --> 00:11:14,560
le verre transparent. C’est ce qui arrive à votre esprit.

128
00:11:14,560 --> 00:11:20,180
Donc, au début de la méditation, vous aurez probablement beaucoup de pensées ou...

129
00:11:20,180 --> 00:11:24,740
des pensées agitées et si vous restez assis un peu plus longtemps, si vous gardez la méditation plus longtemps

130
00:11:24,740 --> 00:11:28,610
persévère et vous reprenez à chaque fois la posture et la respiration,

131
00:11:28,610 --> 00:11:33,050
puis progressivement les pensées, vous aurez probablement un peu moins de pensées ou

132
00:11:33,050 --> 00:11:41,680 ils sont moins coercitifs et surviennent
pour que la clarté règne dans votre tête.

133
00:11:46,860 --> 00:11:53,220
Vous voyez donc que l’attitude est un jeu de sens, de présence, de signification

134
00:11:53,220 --> 00:11:59,850
dans votre colonne vertébrale, de présence et de détente.

135
00:11:59,850 --> 00:12:04,230
Il s’agit d’une combinaison de prétention et d’assouplissement

136
00:12:04,230 --> 00:12:09,540
présence et libération. Et la même combinaison que celle que l’on trouve dans le

137
00:12:09,600 --> 00:12:12,540
Dans le contrôle de l’esprit, c’est un

138
00:12:12,620 --> 00:12:15,700
combinaison d’attention

139
00:12:19,700 --> 00:12:23,760
et observer, lâcher prise.

140
00:12:28,960 --> 00:12:34,660
Lorsque vous méditez : essayez de ne pas penser en termes de bien ou de mal

141
00:12:34,660 --> 00:12:38,860
J’ai eu une bonne méditation ou cette méditation n’était pas bonne,

142
00:12:38,860 --> 00:12:42,730
En fait, l’essentiel est que vous laissiez passer cela :

143
00:12:42,730 --> 00:12:47,070
la méditation est bonne comme elle est.

144
00:12:49,300 --> 00:12:53,410
La pratique la plus approfondie que vous pouvez faire est en fait

145
00:12:53,410 --> 00:12:59,350
en zen, nous l’appelons mushotoku, ce qui signifie sans but

146
00:12:59,350 --> 00:13:05,140
sur une base non lucrative. Asseyez-vous, laissez-vous aller à votre objectif. Bien sûr, vous avez en

147
00:13:05,140 --> 00:13:10,140
commencer un but et vouloir quelque chose, etc. pendant la méditation elle-même :

148
00:13:10,140 --> 00:13:15,520
juste la méditation, lâchez votre objectif. N’essayez pas d’obtenir quoi que ce soit, le silence

149
00:13:15,520 --> 00:13:19,900
ou la concentration, ou je sais beaucoup de choses sur ce que vous aimeriez réaliser.

150
00:13:19,900 --> 00:13:22,260
Lâcher prise

151
00:13:23,380 --> 00:13:29,350
Quand vous commencez à méditer, combien de temps vous convient-il ? Quoi qu’il en soit, je vous recommande...

152
00:13:29,350 --> 00:13:34,660
dans le voisinage d’un dojo, allez régulièrement visiter un tel dojo,

153
00:13:34,660 --> 00:13:39,430
un endroit où d’autres praticiens sont assis, faites corriger votre posture par des praticiens plus âgés.

154
00:13:39,430 --> 00:13:45,430
praticiens. Et quand vous vous entraînez à la maison, fixez-vous un certain temps,

155
00:13:45,430 --> 00:13:50,560
15 ou 20 minutes, par exemple. Vous l’installez sur votre téléphone, vous mettez votre téléphone

156
00:13:50,560 --> 00:13:54,760
en mode avion et vous réglez votre minuterie sur 15 minutes, et ensuite, que vous...

157
00:13:54,820 --> 00:13:59,100
La méditation est maintenant facile, que vous vous concentriez facilement ou que vous trouviez cela difficile,

158
00:13:59,100 --> 00:14:04,580
vous restez assis pendant 15 ou 20 minutes, c’est un moment agréable quand vous êtes seul.

159
00:14:04,580 --> 00:14:09,060
Une ou deux fois par semaine, puis vous pouvez l’accumuler lentement. Vous pouvez le construire

160
00:14:09,070 --> 00:14:13,560
s’accumuler pour rester assis un peu plus longtemps ou peut-être pour rester assis pendant plus de jours. Faites-le comme ceci...

161
00:14:13,560 --> 00:14:16,690
Le plus dur dans la méditation, c’est de ne pas commencer,

162
00:14:16,690 --> 00:14:20,650
le plus dur dans la méditation, c’est de continuer, de continuer...

163
00:14:20,650 --> 00:14:24,270
Choisissez donc un rythme que vous pouvez suivre

164
00:14:25,330 --> 00:14:29,160
Bonne pratique !