1
00:00:00,000 --> 00:00:05,180
I practiced with Sister Deshimaru in sesshin and summer camps. 

2
00:00:05,500 --> 00:00:08,360
I was told, "You will see, he is a master!" 

3
00:00:08,700 --> 00:00:13,040
I didn’t even dare to look at him, I was so intimidated. 

4
00:00:13,540 --> 00:00:16,780
When I found out he was ill, I thought to myself: 

5
00:00:17,120 --> 00:00:20,340
"Shall I continue Zen if he dies?" 

6
00:00:20,620 --> 00:00:26,180
I had the impression that he had taught me something that gave me the courage to continue. 

7
00:00:26,620 --> 00:00:30,400
My name is Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
I might have thought before that I could be a Catholic nun … 

9
00:00:35,400 --> 00:00:39,320
But how good is it to be a zen nun in working life! 

10
00:00:39,620 --> 00:00:43,200
I continued to follow the Lyon dojo with André Meissner. 

11
00:00:43,440 --> 00:00:49,000
It was by following Meissner, who later followed Stéphane, that I naturally followed Master Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen is a close student of Master Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
I was ordained a Bodhisattva in 1980 by Master Deshimaru. 

14
00:00:59,680 --> 00:01:03,560
The Bodhisattva is one who wants to help all beings. 

15
00:01:03,780 --> 00:01:07,960
Ordination is the fulfillment of this vow. 

16
00:01:08,200 --> 00:01:10,560
I never tried to practice Zen. 

17
00:01:10,860 --> 00:01:12,460
I didn’t know what it was. 

18
00:01:12,740 --> 00:01:15,180
There was a poster at a conference in Lyon. 

19
00:01:15,400 --> 00:01:18,180
It was a conference with Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
He says you would miss something if you didn’t go. 

21
00:01:26,320 --> 00:01:33,860
During his lecture there were people in zazen, but I was not even impressed with the attitude. 

22
00:01:34,280 --> 00:01:37,780
It was a bit of an intellectual move on my part. 

23
00:01:38,020 --> 00:01:42,220
When I started exercising, it was a way to stop time. 

24
00:01:42,500 --> 00:01:44,640
It all goes so fast! 

25
00:01:44,940 --> 00:01:50,140
I also thought it could solve all the problems on the base. 

26
00:01:50,460 --> 00:01:55,260
It solved a problem because I felt kicked out of my math teacher’s career. 

27
00:01:55,540 --> 00:02:00,280
I liked the first sentence of the poem Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"The big road is not difficult, just avoid a choice." 

29
00:02:05,860 --> 00:02:11,100
I said to myself, "Yes, I am very available for what is happening!" 

30
00:02:11,420 --> 00:02:16,560
As I practiced, I realized how much I was still in a state of choice and rejection. 

31
00:02:16,940 --> 00:02:23,900
Practicing Zen also helps us see our darkness. 

32
00:02:25,100 --> 00:02:32,740
If you liked this video, feel free to like it and subscribe to the channel! 

