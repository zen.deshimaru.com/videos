1
00:00:00,200 --> 00:00:03,460
Manchmal kann man das nur im Zen finden, 

2
00:00:03,780 --> 00:00:06,680
wenn wir eine Weile zusammen sind, 

3
00:00:06,920 --> 00:00:08,960
dass den Menschen Mitgefühl fehlt. 

4
00:00:09,180 --> 00:00:11,600
Ich denke manchmal muss man die Art und Weise ändern, wie man Dinge betrachtet. 

5
00:00:11,980 --> 00:00:16,400
Anstatt zu denken: "Hey, das ist der andere, der mich ausgeraubt hat!" 

6
00:00:17,000 --> 00:00:20,680
Ich dachte: "Was zum Teufel soll ich damit machen?" 

7
00:00:21,180 --> 00:00:23,160
Und versuchen Sie es umzudrehen. 

8
00:00:24,360 --> 00:00:27,900
Das ist sehr aufrichtig. 

9
00:00:28,300 --> 00:00:30,340
Weil die Menschen sich selbst sind. 

10
00:00:30,600 --> 00:00:32,560
Sie versuchen, keine Rolle zu spielen: 

11
00:00:32,760 --> 00:00:36,900
"Mir geht es gut, mir geht es gut, mir geht es gut …" 

12
00:00:38,240 --> 00:00:41,760
Es gibt sicherlich Zeiten, in denen wir uns aufregen. 

13
00:00:42,040 --> 00:00:45,280
Und wo unsere Reaktionen auffallen. 

14
00:00:45,600 --> 00:00:50,780
Es ist mir wichtig, dass wir uns dort nicht niederlassen. 

15
00:00:51,040 --> 00:00:52,880
Sich nicht in der Idee suhlen: 

16
00:00:53,200 --> 00:00:55,700
"In einem Sesshin gibt es keine netten Leute." 

17
00:00:55,960 --> 00:00:59,320
Wenn ich nach Sesshin komme, 

18
00:00:59,880 --> 00:01:04,060
Meine Idee ist es, alles in ein positives zu verwandeln. 

19
00:01:04,600 --> 00:01:07,340
Ich weiß nicht, ob ich es die ganze Zeit schaffen kann! 

20
00:01:08,360 --> 00:01:10,960
Für mich all die Schwierigkeiten, die entstehen 

21
00:01:11,180 --> 00:01:16,960
seine Einladungen, in seinem Geisteszustand fortzufahren. 

22
00:01:17,360 --> 00:01:24,680
Ich würde sogar sagen, wenn wir zu Unrecht kritisiert werden, 

23
00:01:24,980 --> 00:01:27,600
wir können etwas Positives daraus machen. 

24
00:01:27,900 --> 00:01:33,580
Zum Beispiel zwingt es Sie, sich selbst weniger ernst zu nehmen. 

25
00:01:37,620 --> 00:01:40,340
Was ich an dieser Sangha mag, 

26
00:01:40,640 --> 00:01:42,860
ist, dass wir versuchen, einander ein bisschen mehr zuzuhören. 

27
00:01:43,140 --> 00:01:45,720
Weil es Zeit braucht, einander mehr zuzuhören, 

28
00:01:46,040 --> 00:01:50,460
einige andere Gesichtspunkte zu berücksichtigen. 

29
00:01:50,760 --> 00:01:53,580
Wir wissen, dass die Dinge falsch sind, 

30
00:01:53,980 --> 00:01:58,120
aber wir streben immer nach Verbesserungen. 

31
00:01:59,400 --> 00:02:07,120
Wenn dir dieses Video gefallen hat, kannst du es gerne mögen und den Kanal abonnieren! 

