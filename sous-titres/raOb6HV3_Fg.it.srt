1
00:00:00,000 --> 00:00:05,742
Il mio maestro mi ha insegnato quando ero un principiante (lo sono ancora …). 

2
00:00:05,842 --> 00:00:07,832
"Zazen è satori." 

3
00:00:07,932 --> 00:00:18,497
All’epoca, negli anni ’70, la parola "satori" era molto interessante, volevamo sapere cosa fosse il satori, una specie di illuminazione. 

4
00:00:18,597 --> 00:00:22,731
La gente voleva fare zazen per ottenere un satori. 

5
00:00:22,831 --> 00:00:31,329
E Sensei ha insegnato che era l’atteggiamento del Buddha stesso, che fuori non c’erano satori. 

6
00:00:31,429 --> 00:00:35,793
Questo è l’insegnamento del Maestro Deshimaru. Diceva sempre: 

7
00:00:35,893 --> 00:00:39,950
"Satori è il ritorno a circostanze normali." 

8
00:00:40,050 --> 00:00:41,529
Noi giovani siamo rimasti delusi. 

9
00:00:41,629 --> 00:00:46,520
Le condizioni normali sono noiose. 

10
00:00:46,620 --> 00:00:49,388
Ci piaceva fumare erba, prendevamo LSD … 

11
00:00:49,490 --> 00:00:53,471
Non eravamo interessati alle circostanze normali. 

12
00:00:53,571 --> 00:00:58,853
Ora, nel 2018, le condizioni normali sono molto richieste. 

13
00:00:58,960 --> 00:01:02,400
Stagioni normali, clima normale, normale qualità dell’aria. 

14
00:01:02,400 --> 00:01:04,400
Un oceano normale, una fauna e una flora normali. 

15
00:01:08,380 --> 00:01:10,980
La normalità diventa costosa. 

16
00:01:11,088 --> 00:01:13,155
Non dimenticare mai queste parole: 

17
00:01:13,255 --> 00:01:15,705
"Lo stesso Zazen è satori." 

18
00:01:15,805 --> 00:01:19,809
Satori significa essere nel posto giusto al momento giusto. 

19
00:01:19,909 --> 00:01:21,516
È molto difficile. 

20
00:01:21,616 --> 00:01:27,840
Se adotti la posa del Buddha, sei nel posto giusto al momento giusto. 

21
00:01:27,940 --> 00:01:32,939
Sei normale Stai sincronizzando con l’ordine cosmico. 

22
00:01:33,039 --> 00:01:34,264
È meccanico. 

23
00:01:34,364 --> 00:01:38,498
Con il suo scheletro di una persona anormale, un inquinatore … 

24
00:01:38,598 --> 00:01:42,831
Se prendi l’atteggiamento del Buddha, è meccanico. 

25
00:01:42,931 --> 00:01:47,854
Ritorniamo al ritmo cosmico universale. Questo è satori. 

26
00:01:47,954 --> 00:01:50,503
Allo stesso tempo, è molto semplice. 

27
00:01:50,603 --> 00:01:59,507
Ma se sei lontano dalle condizioni normali come lo siamo ora, ha un valore enorme. 

28
00:01:59,607 --> 00:02:01,521
E cos’è lo Zen? 

29
00:02:01,621 --> 00:02:04,017
Prende la posizione esatta. 

30
00:02:04,117 --> 00:02:07,945
È come un codice per aprire una porta segreta. 

31
00:02:08,045 --> 00:02:10,035
Lo vedi nei film. 

32
00:02:10,135 --> 00:02:16,359
Devi premere questo, devi ottenere il raggio di luce nel posto giusto. 

33
00:02:16,459 --> 00:02:17,990
E la porta si apre. 

34
00:02:18,090 --> 00:02:21,229
È lo stesso se si allinea la propria posizione. 

35
00:02:25,280 --> 00:02:29,320
Sei bellissima sui perni. Sul cuscino, in loto. 

36
00:02:29,720 --> 00:02:32,400
La posizione del Buddha è nel loto. 

37
00:02:32,860 --> 00:02:37,700
Se non riesci a fare il loto, fai mezzo loto. 

38
00:02:37,980 --> 00:02:40,580
Ma il loto non ha nulla a che fare con esso. 

39
00:02:41,060 --> 00:02:44,120
Inverte le polarità. 

40
00:02:44,760 --> 00:02:52,720
Non appena prendi il loto, invece di forzarlo, invece di ferirlo, lo lasci andare. 

41
00:02:53,480 --> 00:02:56,380
L’atteggiamento del Buddha è preciso. 

42
00:02:56,940 --> 00:03:03,020
Se indossi solo i vestiti giusti. 

43
00:03:03,140 --> 00:03:08,240
Respirare bene, curare il corpo e la mente. 

44
00:03:08,240 --> 00:03:14,360
È bello ed è molto semplice. 

45
00:03:14,860 --> 00:03:17,840
Non è una condizione speciale. 

46
00:03:18,520 --> 00:03:23,020
Non devi pensare "Ho più satori degli altri". 

47
00:03:23,400 --> 00:03:26,080
È semplicemente normale. 

48
00:03:26,600 --> 00:03:32,940
Se c’è qualcosa di anormale nel sistema solare, scoreggia tutto. 

49
00:03:33,280 --> 00:03:38,720
Quindi tutto è esattamente nel posto giusto al momento giusto e in un solo movimento. 

50
00:03:38,920 --> 00:03:42,660
Il segreto è chiaro: conosciamo il metodo. 

51
00:03:42,940 --> 00:03:45,700
Dovresti prenderti il ​​disturbo. 

52
00:03:46,100 --> 00:03:48,460
Ci vuole pratica. 

53
00:03:48,740 --> 00:03:52,100
È straordinario poter fallire l’ordine cosmico. 

54
00:03:52,300 --> 00:03:56,000
Sensei continuava a ripetere: "Segui l’ordine cosmico". 

55
00:03:56,160 --> 00:03:58,080
Mi ha urlato: 

56
00:03:58,320 --> 00:04:01,500
"STIEFAN, PERCHÉ NON SEGUI L’ORDINE COSMICO? DEVI SEGUIRE L’ORDINE COSMICO!" 

