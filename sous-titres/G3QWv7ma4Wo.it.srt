1
00:00:13,650 --> 00:00:17,340
quindi sono nato a Montpellier, quindi in Francia 

2
00:00:17,340 --> 00:00:22,510
ma ora vivo in un suono i miei genitori sono due paesi diversi 

3
00:00:22,510 --> 00:00:26,950
improvvisamente viaggio spesso tra l’unico in Francia e anche in altri paesi 

4
00:00:26,950 --> 00:00:32,169
perché loro vedono lì io vedo lì con il loro lavoro ma ogni estate arriviamo 

5
00:00:32,169 --> 00:00:36,370
qui il tempio nella montagna da quando ho lavorato quando ne avevo due 

6
00:00:36,370 --> 00:00:44,960
settimane penso che siamo cresciuti a tempo pieno qui, non sempre 

7
00:00:44,960 --> 00:00:50,390
vieni qui quello che poi io e i miei amici sappiamo tutto il tempo sì, andiamo 

8
00:00:50,390 --> 00:00:55,130
continua a venire qui anche quando non sei un adulto ma hai detto a te stesso 

9
00:00:55,130 --> 00:00:58,430
mai che avremmo dovuto parlare non era stato, non era proprio così 

10
00:00:58,430 --> 00:01:03,080
qualcosa che abbiamo pensato alle persone che vengono qui vengono qui 

11
00:01:03,080 --> 00:01:08,960
Zen me quando non esistevo per vedere le gambe per far giocare il cibo 

12
00:01:08,960 --> 00:01:15,560
e improvvisamente ho scoperto lì il sangha prima di Zen me e la mia opinione che hai è su 

13
00:01:15,560 --> 00:01:19,850
aveva anche un altro amico Vivien che aveva un vento in più di lui e noi 

14
00:01:19,850 --> 00:01:25,729
per noi è un po ’come metterlo nella fottuta rue Godot che ci ha fatto 

15
00:01:25,729 --> 00:01:32,090
credi nella magia ci ha fatto credere a tutti i tipi di cose 

16
00:01:32,090 --> 00:01:37,700
esempio una donna quando era fuori nella foresta, ci ha detto a metà 

17
00:01:37,700 --> 00:01:42,530
scavare nel terreno e perché ci sono sempre diamanti sottoterra 

18
00:01:42,530 --> 00:01:48,140
non importa piede c’è almeno un diamante ogni metro dal consolato 

19
00:01:48,140 --> 00:01:54,680
scavato, scavato e scavato e che in ogni caso noi due avevamo entrambi i lati 

20
00:01:54,680 --> 00:01:59,270
nascosto da lui, prese un diamante dalla tasca e fece finta di uscirne 

21
00:01:59,270 --> 00:02:04,640
il terreno di un enorme diamante preso in prestito da sua madre fu poi abbagliato 

22
00:02:04,640 --> 00:02:12,230
dobbiamo avere qualcosa del genere 30 e durante il genere molto dato dopo 

23
00:02:12,230 --> 00:02:15,860
a volte vorrei scavare un po ’sfacciato spesso se trovo a 

24
00:02:15,860 --> 00:02:21,110
diamante se mai mi rendessi conto che potrei avere dieci anni 

25
00:02:21,110 --> 00:02:24,250
qualcosa se nella mia pelle ti dico tutto 

26
00:02:24,250 --> 00:02:28,130
James è diverso per tutti, credo che mia mamma sia stanca 

27
00:02:28,130 --> 00:02:31,840
i loro fan sentì immediatamente di essere stata blu per il resto della sua vita 

28
00:02:31,840 --> 00:02:38,440
ma credo che per me dopo ogni Zen comincio ad amare di più 

29
00:02:38,690 --> 00:02:43,950
più o meno bene sul secondo scatto mi dico che avrei 

30
00:02:43,950 --> 00:02:48,709
il rapimento inoltre non è finito 

31
00:02:49,280 --> 00:02:54,740
Ho sempre saputo gli annunci che ho sempre fatto da quando i miei 

32
00:02:54,740 --> 00:02:59,840
la mamma era incinta, aveva già praticato lo zazen all’improvviso è un po ’come 

33
00:02:59,840 --> 00:03:06,890
Sono nato improvvisamente, ho sempre conosciuto il sangha in diversi templi 

34
00:03:06,890 --> 00:03:13,819
che ho molti ricordi che ho incontrato amici o persino capanne 

35
00:03:13,819 --> 00:03:17,750
cosa che è sempre stato di diverse esigenze e quando ero piccolo io 

36
00:03:17,750 --> 00:03:21,470
Non mi nascondevo, stavo solo dicendo ai miei amici di sì, vado in montagna e 

37
00:03:21,470 --> 00:03:25,850
tutto, ma non ho mai detto loro che ho intenzione di fare, vado a Zen, ho detto loro 

38
00:03:25,850 --> 00:03:28,400
non quello perché una volta ho provato lo stesso 

39
00:03:28,400 --> 00:03:31,190
mi guardarono tutti in modo molto strano, mi dissi, ehi 

40
00:03:31,190 --> 00:03:36,110
non parlare mai più, ma per il resto non mi è mai sembrato strano, mi è sembrato 

41
00:03:36,110 --> 00:03:41,590
ancora più normale per andare, infatti sapevo in anticipo se venissi 

42
00:03:41,590 --> 00:03:46,010
Ho dovuto fare uno Zen al giorno ma non ero affatto contrario a questa idea 

43
00:03:46,010 --> 00:03:50,750
francamente che per il mio gioco ho voluto scoprire precisamente che era una mia scelta 

44
00:03:50,750 --> 00:03:54,709
vieni prima qui a vedere l’intera saga che non ho visto in due 

45
00:03:54,709 --> 00:04:01,820
tre anni dopo il posto il cibo e tutti i ricordi che ritornano 

46
00:04:01,820 --> 00:04:06,380
fatto non appena vengo la prima volta che faccio una pazzia non me ne sono reso conto 

47
00:04:06,380 --> 00:04:09,850
conto che stavo facendo in realtà dà che era piuttosto strano nel senso che 

48
00:04:09,850 --> 00:04:15,320
Non ho percepito troppi cambiamenti, infatti non l’ho fatto perché non ho percepito 

49
00:04:15,320 --> 00:04:19,310
l’utilità di zen ma mi sembra così normale che non ne ho visto nessuno 

50
00:04:19,310 --> 00:04:23,660
cambio in realtà lo trovo strano ma mi piace mi sento come 

51
00:04:23,660 --> 00:04:28,490
più fortunato Jean Prat più mi piace e fluff poi me 

52
00:04:28,490 --> 00:04:31,090
aspetto diverso 

53
00:04:33,470 --> 00:04:41,410
la signora convalidata in Francia su YouTube 

54
00:04:45,910 --> 00:04:57,220
la Francia sia in modo semplice che efficiente 

55
00:05:05,560 --> 00:05:13,809
[Musica] 

56
00:05:22,860 --> 00:05:25,929
[Musica] 

57
00:07:03,510 --> 00:07:06,840
1 [musica] 

58
00:07:06,840 --> 00:07:08,840
e 

