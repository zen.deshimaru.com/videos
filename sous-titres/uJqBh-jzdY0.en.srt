1
00:00:06,180 --> 00:00:09,540
I don’t care about Zen. 

2
00:00:09,840 --> 00:00:12,100
It’s just another piece of church waste. 

3
00:00:12,480 --> 00:00:17,520
There are thousands of Zen masters in the United States. 

4
00:00:17,820 --> 00:00:20,880
Everyone wants to be a Zen master. 

5
00:00:21,220 --> 00:00:24,680
And Zen is all the rage, 

6
00:00:25,280 --> 00:00:28,820
and i am not interested. 

7
00:00:29,320 --> 00:00:31,960
Why do you think it’s all the rage? 

8
00:00:32,260 --> 00:00:36,240
No, now it is out of fashion! 

9
00:00:37,300 --> 00:00:40,060
In Argentina, everyone buys a small zen garden. 

10
00:00:40,340 --> 00:00:42,720
We put him in the office, in his living room. 

11
00:00:43,020 --> 00:00:45,740
It’s beautiful. It’s fun. It’s fun. 

12
00:00:46,100 --> 00:00:49,000
But Zen is useless! 

13
00:00:53,900 --> 00:00:57,360
What do you think of Krishnamurti’s expression: 

14
00:00:57,640 --> 00:01:01,060
"Don’t let anyone get between you and the truth"? 

15
00:01:01,380 --> 00:01:07,140
If there is a discrepancy between you and the truth, it is not the truth. 

16
00:01:11,480 --> 00:01:16,780
He meant that he was always against the tide. 

17
00:01:17,040 --> 00:01:21,960
He had separated himself from the whole theological society, which wanted to take him as a reference. 

18
00:01:22,300 --> 00:01:24,260
Do you admire that about him? 

19
00:01:24,540 --> 00:01:31,440
Yes. My mother, when she was pregnant with me, was a disciple of Krishnamurti. 

20
00:01:31,760 --> 00:01:37,500
One day, at a conference, she felt an ecstasy, a satori. 

21
00:01:37,760 --> 00:01:42,620
She said to herself, "my son is going to be a saint." 

22
00:01:43,020 --> 00:01:50,060
I really like Krishnamurti, he was an extraordinary person. 

23
00:01:51,640 --> 00:01:56,940
Sai Baba? I don’t know, I don’t know him. 

24
00:01:57,360 --> 00:02:02,020
He is the only teacher I have ever heard said that he was God. 

25
00:02:02,320 --> 00:02:04,680
But everyone is God! 

26
00:02:05,740 --> 00:02:11,540
A real master does not have to say "I am God". 

27
00:02:11,940 --> 00:02:16,200
But you have to tell others that you are God. 

28
00:02:16,500 --> 00:02:22,740
So the man says, "I am God. Give me your money!" 

29
00:02:23,060 --> 00:02:27,960
And the fact that he does tricks like making watches appear, guess what? 

30
00:02:28,240 --> 00:02:32,940
I do not know. I do not know. I do not know. Yes, maybe he has a strength. Yes, it is possible. 

31
00:02:33,080 --> 00:02:34,400
Is it possible? Yes, it is possible. 

32
00:02:34,680 --> 00:02:37,040
There are wizards who say it is illusionism. 

33
00:02:37,320 --> 00:02:40,820
It is also possible that it was a ruse. 

34
00:02:40,980 --> 00:02:48,180
I think all religions, all churches, are against man. 

35
00:02:48,280 --> 00:02:50,280
Everyone, without distinction? 

36
00:02:51,740 --> 00:02:54,760
Also Buddhism, everyone. 

37
00:02:55,020 --> 00:02:57,820
These are churches. 

38
00:02:58,400 --> 00:03:00,400
I do not like it. 

39
00:03:02,740 --> 00:03:08,100
The true Zen of Deshimaru is not a church, it is a school. 

40
00:03:08,480 --> 00:03:10,060
It is something else. 

41
00:03:10,380 --> 00:03:12,380
How can you tell the difference? 

42
00:03:12,900 --> 00:03:16,740
The church, it’s the Japanese! 

43
00:03:20,060 --> 00:03:26,000
In a church people look for a spiritual way. 

44
00:03:26,400 --> 00:03:33,240
And after 10 years they are looking for a degree, in a hierarchy, a power over others. 

45
00:03:33,560 --> 00:03:42,620
It is a terrible thing that kills the truth. 

46
00:03:43,160 --> 00:03:45,680
I practice zazen. 

47
00:03:46,040 --> 00:03:49,820
I practice the teachings of the Buddhas, which is very simple. 

48
00:03:50,120 --> 00:03:54,520
For me it is a treasure of humanity. 

49
00:03:56,500 --> 00:03:58,640
Zazen [seated meditation]. 

50
00:03:58,960 --> 00:04:02,180
Kinhin. [Walk slowly] 

51
00:04:02,700 --> 00:04:04,700
Sampaï. 

52
00:04:04,900 --> 00:04:09,020
Genmai [morning meal] and samu [concentration at work]. 

53
00:04:09,360 --> 00:04:10,880
It is very simple. 

54
00:04:11,180 --> 00:04:12,880
But it always changes. 

55
00:04:13,140 --> 00:04:16,000
You always add new things. 

56
00:04:16,320 --> 00:04:18,340
Yes, I am learning. 

57
00:04:18,480 --> 00:04:27,580
Because I think I like to learn and have more wisdom to evolve. 

58
00:04:27,880 --> 00:04:29,480
Question me. 

59
00:04:29,800 --> 00:04:33,660
I enjoy being a disciple more than a master. 

60
00:04:34,100 --> 00:04:35,740
I like them both. 

61
00:04:36,120 --> 00:04:40,540
I am a good master, but I am a very bad student. 

62
00:04:40,820 --> 00:04:42,780
Very undisciplined. 

63
00:04:43,200 --> 00:04:46,160
Have you always been like this? Included with Deshimaru? 

64
00:04:46,400 --> 00:04:47,140
Yes. 

65
00:04:47,420 --> 00:04:48,840
And he corrected you? 

66
00:04:49,080 --> 00:04:50,700
Yes, he helped me a lot. 

67
00:04:50,980 --> 00:04:54,240
He put me in charge, it calmed me down a bit. 

68
00:04:58,060 --> 00:05:00,280
"What am I going to do with Stéphane?" 

69
00:05:00,560 --> 00:05:04,180
"Here, I’m in charge of him!" 

70
00:05:08,920 --> 00:05:11,860
Do you remember the day you met Deshimaru? 

71
00:05:12,120 --> 00:05:12,620
Yes. 

72
00:05:12,920 --> 00:05:14,920
How was it? 

73
00:05:15,280 --> 00:05:16,820
I can not say it. 

74
00:05:17,060 --> 00:05:19,240
I was in Zazen and he was behind me with the kyosaku. 

75
00:05:22,920 --> 00:05:28,320
Later I bought flowers in the metro on my way to the dojo. 

76
00:05:28,640 --> 00:05:33,060
At the entrance of the dojo I say: "I have flowers for the master". » 

77
00:05:33,400 --> 00:05:36,080
And after Zazen, they call me. 

78
00:05:36,340 --> 00:05:38,540
"The master asks for you." 

79
00:05:38,780 --> 00:05:42,860
I go to his room, where he is sitting. 

80
00:05:43,060 --> 00:05:46,760
"Who brought me these flowers?" 

81
00:05:47,040 --> 00:05:47,800
I. 

82
00:05:48,660 --> 00:05:51,580
"Good, good, good." 

83
00:05:54,280 --> 00:05:58,080
Do you remember the last day you saw him? 

84
00:05:58,580 --> 00:05:59,920
He was dead. 

85
00:06:05,280 --> 00:06:07,280
His body was going to be burned. 

86
00:06:07,520 --> 00:06:09,160
Bye! 

87
00:06:10,320 --> 00:06:12,320
What about the last day you saw him alive? 

88
00:06:13,860 --> 00:06:19,800
Then he went to Japan for the last time and said "goodbye!". 

89
00:06:20,200 --> 00:06:23,680
The day Deshimaru died did you cry? 

90
00:06:24,360 --> 00:06:34,420
When he died, I was in the dojo, sitting in zazen … 

91
00:06:37,260 --> 00:06:40,780
Someone enters the dojo and says: 

92
00:06:41,060 --> 00:06:45,840
"We will have to continue without Deshimaru." 

93
00:06:46,100 --> 00:06:48,900
I lived in zazen. 

94
00:06:49,200 --> 00:06:50,560
I always stood before him. 

95
00:06:50,900 --> 00:06:53,500
I was always a pillar to him. 

96
00:06:53,940 --> 00:06:57,120
That’s how I experienced his death. 

97
00:06:57,420 --> 00:07:02,200
I had to stay strong for the others. 

98
00:07:02,840 --> 00:07:11,040
Then I went straight to Japan for the cremation ceremony. 

99
00:07:13,100 --> 00:07:23,920
There was a ceremony with many teachers, with the entire Japanese church. Huge. 

100
00:07:24,660 --> 00:07:31,060
And it was in a box, with many flowers. 

101
00:07:31,500 --> 00:07:37,790
His very beautiful face, very smiling. 

102
00:07:38,100 --> 00:07:50,620
When he was alive, whenever he put on a hat or a false nose, he became very comical. 

103
00:07:50,940 --> 00:07:54,380
It was the same there, with the flowers surrounding it. 

104
00:07:54,720 --> 00:07:59,520
We did the ceremony, I remained very focused and unmoved. 

105
00:07:59,820 --> 00:08:05,400
Then we put the box in a car. 

106
00:08:05,880 --> 00:08:09,720
To four disciples. 

107
00:08:09,840 --> 00:08:20,280
I was the last one and I push the box on a cart. 

108
00:08:20,600 --> 00:08:27,160
At the last moment I squeeze my finger with the chest. 

109
00:08:29,180 --> 00:08:37,700
I started crying and I couldn’t stop for two or three hours. 

110
00:08:38,020 --> 00:08:42,220
It came out and I cried and cried. 

111
00:08:42,520 --> 00:08:45,560
Everyone looked at me because no one was crying like that. 

112
00:08:46,040 --> 00:08:51,440
I cried incessantly in front of the family and the other participants. 

113
00:08:52,280 --> 00:08:57,500
I couldn’t stop myself. 

114
00:09:00,560 --> 00:09:05,720
Until I saw the smoke coming from the chimney. 

115
00:09:19,379 --> 00:09:27,799
Dead - alive, everything is alive, everything is dead. 

116
00:09:28,840 --> 00:09:33,500
We are still alive, but we are dead. 

117
00:09:33,800 --> 00:09:42,220
When you die, you see your life flash by. 

118
00:09:42,540 --> 00:09:48,580
We have been looking at our whole lives now. 

119
00:10:10,010 --> 00:10:13,620
Zazen is the normal state. 

