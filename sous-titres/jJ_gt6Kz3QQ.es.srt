1
00:00:03,420 --> 00:00:07,900
¿Qué es el espíritu? 

2
00:00:08,220 --> 00:00:16,200
En chino y japonés, varios ideogramas se traducen como "fantasmas". 

3
00:00:16,880 --> 00:00:19,340
A veces se traduce como "corazón". 

4
00:00:21,960 --> 00:00:23,560
Es toda existencia. 

5
00:00:23,820 --> 00:00:25,240
Es la comunidad viva. 

6
00:00:25,500 --> 00:00:27,400
Este es el espíritu común de la comunidad viva. 

7
00:00:27,740 --> 00:00:28,900
Ese es el espiritu. 

8
00:00:29,120 --> 00:00:30,780
Solo hay una mente. 

9
00:00:32,000 --> 00:00:34,960
¿Qué crea la mente? 

10
00:00:35,740 --> 00:00:37,780
Desde un punto de vista budista, nada crea la mente. 

11
00:00:38,100 --> 00:00:40,540
Porque la mente no tiene nacimiento. 

12
00:00:40,880 --> 00:00:46,780
Es sin causa o desaparición. 

13
00:00:47,060 --> 00:00:50,020
La mente es creada, pero no creada. 

14
00:00:50,500 --> 00:00:54,220
¿Es la mente la primera causa? 

15
00:00:54,460 --> 00:00:57,820
No puede haber una primera causa. 

16
00:00:58,100 --> 00:00:59,940
Porque el origen es puro. 

17
00:01:02,280 --> 00:01:05,420
¿Cuál habría sido la primera causa? 

18
00:01:05,840 --> 00:01:11,200
Entendemos, incluso intelectualmente, que si vamos en el pasado, no podemos encontrar la primera causa. 

19
00:01:11,660 --> 00:01:13,300
Entonces la primera causa no existe. 

20
00:01:13,660 --> 00:01:15,640
La primera causa es el efecto. 

21
00:01:16,060 --> 00:01:16,900
Ahora 

22
00:01:17,440 --> 00:01:20,940
¿Dónde reside el fantasma? 

23
00:01:21,280 --> 00:01:23,120
La mente siempre existe y reside en todas partes. 

24
00:01:23,420 --> 00:01:25,200
Ya sea que estemos en Zazen o en otro lugar. 

25
00:01:25,400 --> 00:01:30,140
No hay meditación especial que cambie la estructura de la mente. 

26
00:01:30,460 --> 00:01:32,240
La mente siempre está presente. 

27
00:01:32,520 --> 00:01:35,780
¿Qué es "mente-cuerpo"? 

28
00:01:36,520 --> 00:01:38,920
La materia y la energía no están separadas entre sí. 

29
00:01:39,280 --> 00:01:44,600
Son dos expresiones diferentes de la misma cosa. 

30
00:01:47,080 --> 00:01:50,360
Entonces, cuerpo y mente son dos expresiones diferentes de la misma cosa. 

31
00:01:50,760 --> 00:01:52,280
No debes denigrar el cuerpo. 

32
00:01:52,640 --> 00:01:53,980
El cuerpo es la mente. 

33
00:01:54,340 --> 00:01:55,720
La materia es la mente. 

34
00:01:56,220 --> 00:02:00,520
¿Cuál es el efecto del pensamiento en el cuerpo espiritual? 

35
00:02:01,240 --> 00:02:06,120
El pensamiento se materializa en la realidad. 

36
00:02:07,020 --> 00:02:09,100
En el sueño de la materia. 

37
00:02:09,580 --> 00:02:11,040
Soñar no solo sucede cuando estás durmiendo. 

38
00:02:11,480 --> 00:02:15,580
El sueño de la materia es lo que llamamos "realidad". 

39
00:02:15,900 --> 00:02:17,880
Realidad material. 

40
00:02:18,400 --> 00:02:21,980
¿Qué podemos concluir de esto? 

41
00:02:22,340 --> 00:02:27,080
La mente siempre ve en la mente. 

