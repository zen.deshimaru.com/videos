Zen und Sophrologie: Grundhaltungen und ihr Einfluss auf das Gehirn

Meister Kosen wurde zum Kongress 2013 der Federation of Professional Schools of Sophrology (www.sophro.fr) eingeladen.

Er erklärt die vier Grundhaltungen des Zen und ihren Einfluss auf das Gehirn. Er kehrt zu den Quellen der dynamischen Entspannung zurück. Er besteht auf der Bedeutung der drei Säulen Haltung, Atmung und Geisteszustand in der buddhistischen Meditation, insbesondere der Praxis des Zen, Zazen.
