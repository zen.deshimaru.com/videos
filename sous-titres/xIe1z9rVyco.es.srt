1
00:00:00,180 --> 00:00:04,420
Comencé a practicar Zen cuando tenía 16 años. 

2
00:00:04,740 --> 00:00:07,660
Estoy cumpliendo 49 años. 

3
00:00:07,860 --> 00:00:14,360
Traté de tomar la posición de zazen solo en mi cama. 

4
00:00:14,960 --> 00:00:19,460
Lo mantuve durante 2, 3, 4 minutos, fue realmente muy doloroso. 

5
00:00:19,700 --> 00:00:21,700
No pude hacerlo en absoluto. 

6
00:00:21,840 --> 00:00:30,800
Allí me mostraron la pose y sostuve el zazen normal durante aproximadamente dos horas y media. 

7
00:00:31,120 --> 00:00:33,420
Nos estamos preparando para partir, y aquí: 

8
00:00:33,640 --> 00:00:36,100
"¡Ah, no te olvides de pagar!" 

9
00:00:36,540 --> 00:00:38,500
Pensé que era gratis … 

10
00:00:38,880 --> 00:00:42,580
Me fui sin mi asignación … 

11
00:00:45,460 --> 00:00:48,680
Practico en el Zen Dojo de Lyon, en Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
Es un viejo dojo hecho por el Maestro Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
Mi nombre es Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
Mi nombre de monje es Ryurin. 

15
00:01:05,200 --> 00:01:09,700
He sido discípulo del Maestro Kosen durante 25 años. 

16
00:01:10,000 --> 00:01:13,000
Me llevó un tiempo conocer al Maestro Kosen. 

17
00:01:13,320 --> 00:01:17,480
Lo conocía, solía hacer sesshins con él … 

18
00:01:17,760 --> 00:01:21,000
Pero no estaba muy cerca. 

19
00:01:21,340 --> 00:01:26,980
Me he acercado debido a circunstancias más allá de mi control. 

20
00:01:27,260 --> 00:01:31,800
Y pude descubrir su enseñanza. 

21
00:01:32,160 --> 00:01:36,120
Así que practiqué zazen con él hasta hoy. 

22
00:01:36,280 --> 00:01:39,800
El Maestro Kosen me dio hace unos diez años. 

23
00:01:40,060 --> 00:01:42,920
como lo hizo con muchos de sus antiguos seguidores, 

24
00:01:43,260 --> 00:01:46,900
El shiho, la transmisión del Dharma. 

25
00:01:47,740 --> 00:01:50,740
Maestro zen, es un samu, no es un título. 

26
00:01:51,020 --> 00:01:57,420
Como decimos "maestro", sugiere la idea de que uno ha dominado algo, pero es un samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" es una palabra china, japonesa. 

28
00:02:01,740 --> 00:02:03,840
Estas son las tareas a realizar. 

29
00:02:04,100 --> 00:02:06,380
Él también enseña, él es un samu. 

30
00:02:06,560 --> 00:02:08,720
El maestro zen hace su samu … 

31
00:02:09,000 --> 00:02:11,020
Cuando termina con su samurai, se va a casa. 

32
00:02:11,300 --> 00:02:12,980
Irá a trabajar o no … 

33
00:02:13,280 --> 00:02:15,560
Tiene esposa y familia o no … 

34
00:02:15,980 --> 00:02:17,240
Depende de cada persona … 

35
00:02:18,060 --> 00:02:21,180
Y ahora está trabajando en otra cosa … 

36
00:02:21,500 --> 00:02:23,120
Esa es la vida! 

37
00:02:23,640 --> 00:02:26,660
Pero el maestro zen es un discípulo. 

38
00:02:27,060 --> 00:02:35,860
Si te gustó este video, ¡no dudes en darle me gusta y suscribirte al canal! 

