1
00:00:00,090 --> 00:00:05,980
Pendant une cérémonie zen, nous faisons sanpaï :
trois prosternations.

2
00:00:06,320 --> 00:00:09,145
Un premier accelerando de clochettes

3
00:00:09,385 --> 00:00:11,607
indique aux moines et nonnes

4
00:00:11,727 --> 00:00:14,660
qu’il leur faut déplier leur zagu.

5
00:00:16,080 --> 00:00:19,905
Une prosternation commence par un salut en gassho.

6
00:00:20,015 --> 00:00:22,562
Puis on pose les genoux par terre.

7
00:00:22,612 --> 00:00:24,685
On pose la tête contre le sol.

8
00:00:24,780 --> 00:00:26,620
On lève les mains.

9
00:00:26,750 --> 00:00:30,450
On se relève sans les mains
en prenant appui sur les orteils.

10
00:00:30,660 --> 00:00:32,780
Pour chaque prosternation,

11
00:00:32,870 --> 00:00:36,235
un coup de cloche nous dit de descendre,

12
00:00:36,295 --> 00:00:39,432
un autre, de remonter.

13
00:00:39,572 --> 00:00:41,676
Pour la troisième prosternation,

14
00:00:41,846 --> 00:00:46,270
deux coups de cloche pour descendre
indiquent que c’est la dernière.

15
00:00:47,150 --> 00:00:50,770
Lors de la dernière remontée,

16
00:00:50,930 --> 00:00:53,444
on attrape son zagu par le coin droit.

17
00:00:53,525 --> 00:00:56,470
On le remonte en même temps qu’on remonte.

18
00:00:56,520 --> 00:00:59,375
On le plie en deux, puis encore en deux.

19
00:00:59,515 --> 00:01:03,710
Ces explications sont destinées aux personnes
qui pratiquent sur Zoom

20
00:01:03,820 --> 00:01:05,875
et qui n’ont jamais fait sanpaï

21
00:01:05,955 --> 00:01:08,702
afin qu’elles puissent pratiquer la cérémonie.

22
00:01:08,872 --> 00:01:13,000
zazenathome.org
