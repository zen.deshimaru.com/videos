1
00:00:06,180 --> 00:00:09,540
Le zen, je m’en moque.

2
00:00:09,840 --> 00:00:12,100
C’est juste une saloperie d’église de plus.

3
00:00:12,480 --> 00:00:17,520
Il y a des milliers de maîtres zen, aux États-Unis.

4
00:00:17,820 --> 00:00:20,880
Tout le monde veut être un maître zen.

5
00:00:21,220 --> 00:00:24,680
Et le zen est une mode,

6
00:00:25,280 --> 00:00:28,820
et ça ne m’intéresse pas.

7
00:00:29,320 --> 00:00:31,960
Pourquoi penses-tu que c’est une mode ?

8
00:00:32,260 --> 00:00:36,240
Non, maintenant c’est passé de mode !

9
00:00:37,300 --> 00:00:40,060
En Argentine, tout le monde achète un petit jardin zen.

10
00:00:40,340 --> 00:00:42,720
On le met au bureau, dans son salon.

11
00:00:43,020 --> 00:00:45,740
C’est magnifique. C’est bien.

12
00:00:46,100 --> 00:00:49,000
Mais le zen, ça ne sert à rien !

13
00:00:53,900 --> 00:00:57,360
Que penses-tu de la phrase de Krishnamurti :

14
00:00:57,640 --> 00:01:01,060
« Ne laisse personne s’interposer entre toi et la vérité » ?

15
00:01:01,380 --> 00:01:07,140
S’il y a un écart entre vous et la vérité, ce n’est pas la vérité.

16
00:01:11,480 --> 00:01:16,780
Il voulait dire qu’il était toujours à contre-courant.

17
00:01:17,040 --> 00:01:21,960
Il s’était séparé de toute la société théologique qui voulait le prendre comme référence.

18
00:01:22,300 --> 00:01:24,260
Vous admirez cela chez lui ?

19
00:01:24,540 --> 00:01:31,440
Oui. Ma mère, quand elle était enceinte de moi, était disciple de Krishnamurti.

20
00:01:31,760 --> 00:01:37,500
Un jour, lors d’une conférence, elle a ressenti une extase, un satori.

21
00:01:37,760 --> 00:01:42,620
Elle s’est dit : « mon fils va devenir un saint ».

22
00:01:43,020 --> 00:01:50,060
J’aime beaucoup Krishnamurti, c’était quelqu’un d’extraordinaire.

23
00:01:51,640 --> 00:01:56,940
Sai Baba ? Je ne sais pas, je ne le connais pas.

24
00:01:57,360 --> 00:02:02,020
C’est le seul professeur que j’ai entendu dire qu’il était Dieu.

25
00:02:02,320 --> 00:02:04,680
Mais tout le monde est Dieu !

26
00:02:05,740 --> 00:02:11,540
Un vrai maître n’a pas besoin de dire « Je suis Dieu ».

27
00:02:11,940 --> 00:02:16,200
Mais vous devez dire aux autres que vous êtes Dieu.

28
00:02:16,500 --> 00:02:22,740
Alors le gars dit : « Je suis Dieu. Donne-moi ton argent ! »

29
00:02:23,060 --> 00:02:27,960
Et le fait qu’il fasse des tours comme faire apparaître des montres, qu’en penses-tu ?

30
00:02:28,240 --> 00:02:32,940
Je ne sais pas. Oui, peut-être qu’il a un pouvoir. Oui, c’est possible.

31
00:02:33,080 --> 00:02:34,400
C’est possible ?
Oui.

32
00:02:34,680 --> 00:02:37,040
Il y a des magiciens qui disent que c’est de l’illusionnisme.

33
00:02:37,320 --> 00:02:40,820
Il est également possible que ce soit une ruse.

34
00:02:40,980 --> 00:02:48,180
Je pense que toutes les religions, les églises, sont contre l’être humain.

35
00:02:48,280 --> 00:02:50,280
Tout le monde, sans distinction ?

36
00:02:51,740 --> 00:02:54,760
Le bouddhisme aussi, tous.

37
00:02:55,020 --> 00:02:57,820
Ce sont des églises.

38
00:02:58,400 --> 00:03:00,400
Ça ne me plaît pas.

39
00:03:02,740 --> 00:03:08,100
Le vrai zen de Deshimaru n’est pas une église, c’est une école.

40
00:03:08,480 --> 00:03:10,060
C’est autre chose.

41
00:03:10,380 --> 00:03:12,380
Comment faire la différence ?

42
00:03:12,900 --> 00:03:16,740
L’église, ce sont les Japonais !

43
00:03:20,060 --> 00:03:26,000
Dans une église, les gens recherchent une voie spirituelle.

44
00:03:26,400 --> 00:03:33,240
Et au bout de 10 ans, ils cherchent un diplôme, dans une hiérarchie, un pouvoir sur les autres.

45
00:03:33,560 --> 00:03:42,620
C’est une chose horrible qui tue la vérité.

46
00:03:43,160 --> 00:03:45,680
Je pratique zazen.

47
00:03:46,040 --> 00:03:49,820
Je pratique l’enseignement transmis par les Bouddhas, qui est très simple.

48
00:03:50,120 --> 00:03:54,520
Pour moi, c’est un trésor de l’humanité.

49
00:03:56,500 --> 00:03:58,640
Zazen [la méditation assise].

50
00:03:58,960 --> 00:04:02,180
Kinhin [la marche lente].

51
00:04:02,700 --> 00:04:04,700
Sampaï [la prosternation].

52
00:04:04,900 --> 00:04:09,020
Genmaï [le repas du matin] et samu [la concentration sur le travail].

53
00:04:09,360 --> 00:04:10,880
C’est très simple.

54
00:04:11,180 --> 00:04:12,880
Mais ça évolue sans cesse.

55
00:04:13,140 --> 00:04:16,000
Tu ajoutes tout le temps de nouvelles choses.

56
00:04:16,320 --> 00:04:18,340
Oui, j’apprends.

57
00:04:18,480 --> 00:04:27,580
Parce que je pense que j’aime apprendre et avoir plus de sagesse, évoluer.

58
00:04:27,880 --> 00:04:29,480
Me remettre en question.

59
00:04:29,800 --> 00:04:33,660
J’aime beaucoup plus être un disciple qu’un maître.

60
00:04:34,100 --> 00:04:35,740
J’aime bien les deux.

61
00:04:36,120 --> 00:04:40,540
Je suis un bon maître, mais je suis un très mauvais disciple.

62
00:04:40,820 --> 00:04:42,780
Très indiscipliné.

63
00:04:43,200 --> 00:04:46,160
Tu as toujours été comme ça ? Y compris avec Deshimaru ?

64
00:04:46,400 --> 00:04:47,140
Oui.

65
00:04:47,420 --> 00:04:48,840
Et il t’a corrigé ?

66
00:04:49,080 --> 00:04:50,700
Oui, il m’a beaucoup aidé.

67
00:04:50,980 --> 00:04:54,240
Il m’a désigné chef, ça m’a calmé un peu.

68
00:04:58,060 --> 00:05:00,280
« Qu’est-ce que je vais faire de Stéphane ? »

69
00:05:00,560 --> 00:05:04,180
« Tiens, je vais le désigner chef ! »

70
00:05:08,920 --> 00:05:11,860
Tu te souviens du jour où tu as rencontré Deshimaru ?

71
00:05:12,120 --> 00:05:12,620
Oui.

72
00:05:12,920 --> 00:05:14,920
C’était comment ?

73
00:05:15,280 --> 00:05:16,820
Je ne peux pas dire.

74
00:05:17,060 --> 00:05:19,240
J’étais en zazen et il était derrière moi, avec le kyosaku.

75
00:05:22,920 --> 00:05:28,320
Plus tard, j’ai acheté des fleurs dans le métro en venant au dojo.

76
00:05:28,640 --> 00:05:33,060
À l’entrée du dojo, je dis « J’ai des fleurs pour le Maître. »

77
00:05:33,400 --> 00:05:36,080
Et après zazen, ils m’appellent.

78
00:05:36,340 --> 00:05:38,540
« Le maître te demande ».

79
00:05:38,780 --> 00:05:42,860
J’entre dans sa chambre, où il est assis.

80
00:05:43,060 --> 00:05:46,760
« Qui m’a apporté ces fleurs ? »

81
00:05:47,040 --> 00:05:47,800
Moi.

82
00:05:48,660 --> 00:05:51,580
« Bien, bien, bien ».

83
00:05:54,280 --> 00:05:58,080
Te souviens-tu du dernier jour où tu l’as vu ?

84
00:05:58,580 --> 00:05:59,920
Il était mort.

85
00:06:05,280 --> 00:06:07,280
Son corps allait être brûlé.

86
00:06:07,520 --> 00:06:09,160
Au revoir !

87
00:06:10,320 --> 00:06:12,320
Et le dernier jour où tu l’as vu vivant ?

88
00:06:13,860 --> 00:06:19,800
C’est quand il est allé au Japon pour la dernière fois et qu’il nous a fait « au revoir ! ».

89
00:06:20,200 --> 00:06:23,680
Le jour où Deshimaru est mort, as-tu pleuré ?

90
00:06:24,360 --> 00:06:34,420
Quand il est mort, j’étais dans le dojo, assis en zazen

91
00:06:37,260 --> 00:06:40,780
Quelqu’un entre dans le dojo et dit :

92
00:06:41,060 --> 00:06:45,840
« Nous allons devoir continuer sans Deshimaru. »

93
00:06:46,100 --> 00:06:48,900
J’ai vécu ça en zazen.

94
00:06:49,200 --> 00:06:50,560
J’étais toujours placé en face de lui.

95
00:06:50,900 --> 00:06:53,500
J’étais toujours pilier, en face de lui.

96
00:06:53,940 --> 00:06:57,120
C’est comme ça que j’ai vécu sa mort.

97
00:06:57,420 --> 00:07:02,200
Il fallait que je reste fort pour les autres.

98
00:07:02,840 --> 00:07:11,040
Ensuite, je suis allé au Japon directement à la cérémonie de l’incinération.

99
00:07:13,100 --> 00:07:23,920
Il y a eu une cérémonie avec de nombreux professeurs, avec toute l’église japonaise. Énorme.

100
00:07:24,660 --> 00:07:31,060
Et il était dans un cercueil, avec beaucoup de fleurs.

101
00:07:31,500 --> 00:07:37,790
Son visage très gentil, très souriant.

102
00:07:38,100 --> 00:07:50,620
De son vivant, dès qu’il mettait un chapeau ou un faux-nez, il devenait très comique.

103
00:07:50,940 --> 00:07:54,380
Là, c’était pareil, avec les fleurs qui l’entouraient.

104
00:07:54,720 --> 00:07:59,520
Nous avons fait la cérémonie, je restais très concentré et impassible.

105
00:07:59,820 --> 00:08:05,400
Puis, on met le cercueil dans une voiture.

106
00:08:05,880 --> 00:08:09,720
À quatre disciples.

107
00:08:09,840 --> 00:08:20,280
J’étais le dernier et je pousse le cercueil sur un chariot.

108
00:08:20,600 --> 00:08:27,160
Au dernier moment, je me pince le doigt avec le cercueil.

109
00:08:29,180 --> 00:08:37,700
J’ai commencé à pleurer et je n’ai pas pu m’arrêter pendant deux, trois heures.

110
00:08:38,020 --> 00:08:42,220
C’est sorti et j’ai pleuré sans cesse.

111
00:08:42,520 --> 00:08:45,560
Tout le monde me regardait parce que personne ne pleurait comme ça.

112
00:08:46,040 --> 00:08:51,440
Devant la famille et les autres participants, je pleurais sans cesse.

113
00:08:52,280 --> 00:08:57,500
Je n’ai pas pu m’arrêter.

114
00:09:00,560 --> 00:09:05,720
Jusqu’à ce que je voie la fumée sortir de la cheminée.

115
00:09:19,379 --> 00:09:27,799
Mort - vivant, tout est vivant, tout est mort.

116
00:09:28,840 --> 00:09:33,500
Nous sommes vivants, mais nous sommes morts.

117
00:09:33,800 --> 00:09:42,220
Lorsque tu meurs, tu vois défiler toute ta vie.

118
00:09:42,540 --> 00:09:48,580
On regarde déjà toute notre vie, maintenant même.

119
00:10:10,010 --> 00:10:13,620
Zazen, c’est l’état normal.
