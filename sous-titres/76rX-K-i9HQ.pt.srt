1
00:00:00,280 --> 00:00:03,640
Vida cotidiana 

2
00:00:03,840 --> 00:00:06,460
Um dia … 

3
00:00:07,200 --> 00:00:12,320
Durante o dia, temos que parar o que fazemos regularmente. 

4
00:00:13,040 --> 00:00:15,320
Pare o trabalho dele. 

5
00:00:15,780 --> 00:00:18,780
E vamos lá. 

6
00:00:19,020 --> 00:00:22,220
No trabalho dele, na vida cotidiana, você encontra a mente. 

7
00:00:22,560 --> 00:00:24,720
Leva apenas duas respirações. 

8
00:00:25,060 --> 00:00:26,340
Você trabalha em sua mesa … 

9
00:00:26,680 --> 00:00:28,820
Apenas endireite-o. 

10
00:00:29,340 --> 00:00:32,760
E respire. 

11
00:00:41,120 --> 00:00:49,260
E retornar imediatamente ao seu ser. 

12
00:00:50,960 --> 00:00:54,140
Eu conhecia uma comunidade chamada Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
Eles trabalham muito. 

14
00:00:57,580 --> 00:01:00,400
Eles trabalham a terra. 

15
00:01:00,660 --> 00:01:02,660
Eles constroem sua casa. 

16
00:01:02,880 --> 00:01:04,540
Eles fazem muito trabalho manual. 

17
00:01:04,740 --> 00:01:06,040
Eles trabalham o dia todo. 

18
00:01:06,300 --> 00:01:07,500
Eles acordam às 5 da manhã. 

19
00:01:07,720 --> 00:01:08,820
Eles fazem suas orações. 

20
00:01:09,080 --> 00:01:10,840
Eles estão tomando café da manhã. 

21
00:01:11,160 --> 00:01:12,420
Eles vão trabalhar. 

22
00:01:12,780 --> 00:01:15,820
Um sino toca a cada hora. 

23
00:01:16,180 --> 00:01:18,060
Todo mundo tem que parar de trabalhar. 

24
00:01:18,720 --> 00:01:25,520
E volte a focar por um minuto. 

25
00:01:26,060 --> 00:01:28,960
Cada um à sua maneira. 

26
00:01:34,080 --> 00:01:36,040
E então eles voltam. 

27
00:01:36,560 --> 00:01:39,160
Portanto, não há apenas zazen. 

28
00:01:39,540 --> 00:01:42,100
Existem muitas maneiras de voltar para si mesmo. 

29
00:01:42,640 --> 00:01:45,220
A vida cotidiana dos monges zen … 

30
00:01:45,540 --> 00:01:48,820
Vivemos nossas vidas pelo zazen. 

31
00:01:49,080 --> 00:01:52,150
Para coletar essa consciência, esse conhecimento. 

32
00:01:52,440 --> 00:01:53,580
Essa energia. 

33
00:01:54,880 --> 00:01:58,780
No momento da experiência do zazen. 

34
00:01:58,900 --> 00:02:03,120
Não se trata de desistir de sua vida por Zazen. 

35
00:02:03,380 --> 00:02:05,400
Num espírito sectário. 

36
00:02:05,740 --> 00:02:07,680
Não é uma ideologia. 

37
00:02:07,900 --> 00:02:10,380
Não é "ismo". 

38
00:02:10,840 --> 00:02:14,780
É: "Vivo minha vida de tal maneira que posso ter essa experiência". 

39
00:02:15,060 --> 00:02:16,560
"O mais profundo possível." 

40
00:02:16,900 --> 00:02:18,980
Esta é uma experiência de meditação zazen. 

41
00:02:19,220 --> 00:02:25,060
Quando você está no zazen, você tem a oportunidade de mudar sua vida inteira. 

42
00:02:25,400 --> 00:02:27,900
É disso que se trata o zazen! 

43
00:02:28,340 --> 00:02:31,980
Não é uma maneira pequena de se sentir bem. 

44
00:02:32,500 --> 00:02:35,040
Zen na vida cotidiana … 

45
00:02:35,640 --> 00:02:39,200
Mesmo em lucro e trabalho … 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku", (sem objetivo ou espírito de lucro) … 

47
00:02:41,760 --> 00:02:43,140
esse é o segredo 

48
00:02:43,340 --> 00:02:46,320
Em outras palavras, mantenha o foco em sua essência. 

49
00:02:46,540 --> 00:02:48,880
Então podemos conseguir tudo. 

50
00:02:49,080 --> 00:02:50,360
Nós temos tudo. 

51
00:02:50,880 --> 00:02:55,280
Qual é a diferença entre a vida de um mestre e a de um ser comum? 

52
00:02:55,660 --> 00:02:59,520
Não existe algo comum. 

53
00:02:59,840 --> 00:03:05,780
Existem apenas criaturas extraordinárias. 

54
00:03:06,660 --> 00:03:14,900
O ruim é que as pessoas não sabem que são extraordinárias. 

55
00:03:16,420 --> 00:03:22,260
Eu não gosto da frase "apenas seja"! 

