1
00:00:03,420 --> 00:00:07,900
Che cos’è lo spirito? 

2
00:00:08,220 --> 00:00:16,200
In cinese e giapponese, vari ideogrammi sono tradotti come "fantasma". 

3
00:00:16,880 --> 00:00:19,340
A volte è tradotto come "cuore". 

4
00:00:21,960 --> 00:00:23,560
È ogni esistenza. 

5
00:00:23,820 --> 00:00:25,240
È la comunità vivente. 

6
00:00:25,500 --> 00:00:27,400
Questo è lo spirito comune della comunità vivente. 

7
00:00:27,740 --> 00:00:28,900
Questo è lo spirito. 

8
00:00:29,120 --> 00:00:30,780
C’è solo una mente. 

9
00:00:32,000 --> 00:00:34,960
Cosa crea la mente? 

10
00:00:35,740 --> 00:00:37,780
Da un punto di vista buddista, nulla crea la mente. 

11
00:00:38,100 --> 00:00:40,540
Perché la mente è senza nascita. 

12
00:00:40,880 --> 00:00:46,780
È senza causa o scomparsa. 

13
00:00:47,060 --> 00:00:50,020
La mente è creata, ma non creata. 

14
00:00:50,500 --> 00:00:54,220
La mente è la prima causa? 

15
00:00:54,460 --> 00:00:57,820
Non può esserci una prima causa. 

16
00:00:58,100 --> 00:00:59,940
Perché l’origine è pura. 

17
00:01:02,280 --> 00:01:05,420
Quale sarebbe stata la prima causa? 

18
00:01:05,840 --> 00:01:11,200
Comprendiamo, anche intellettualmente, che se andiamo nel passato, non possiamo trovare la prima causa. 

19
00:01:11,660 --> 00:01:13,300
Quindi la prima causa non esiste. 

20
00:01:13,660 --> 00:01:15,640
La prima causa è l’effetto. 

21
00:01:16,060 --> 00:01:16,900
Ora. 

22
00:01:17,440 --> 00:01:20,940
Dove risiede il fantasma? 

23
00:01:21,280 --> 00:01:23,120
La mente esiste sempre e risiede ovunque. 

24
00:01:23,420 --> 00:01:25,200
Sia che siamo a Zazen o altrove. 

25
00:01:25,400 --> 00:01:30,140
Non esiste una meditazione speciale che cambi la struttura della mente. 

26
00:01:30,460 --> 00:01:32,240
La mente è sempre presente. 

27
00:01:32,520 --> 00:01:35,780
Che cos’è il "corpo-mente"? 

28
00:01:36,520 --> 00:01:38,920
Materia ed energia non sono separate l’una dall’altra. 

29
00:01:39,280 --> 00:01:44,600
Sono due diverse espressioni della stessa cosa. 

30
00:01:47,080 --> 00:01:50,360
Quindi corpo e mente sono due diverse espressioni della stessa cosa. 

31
00:01:50,760 --> 00:01:52,280
Non dovresti denigrare il corpo. 

32
00:01:52,640 --> 00:01:53,980
Il corpo è la mente. 

33
00:01:54,340 --> 00:01:55,720
La materia è la mente. 

34
00:01:56,220 --> 00:02:00,520
Qual è l’effetto del pensiero sul corpo spirituale? 

35
00:02:01,240 --> 00:02:06,120
Il pensiero si materializza nella realtà. 

36
00:02:07,020 --> 00:02:09,100
Nel sogno della materia. 

37
00:02:09,580 --> 00:02:11,040
Sognare non succede solo quando dormi. 

38
00:02:11,480 --> 00:02:15,580
Il sogno della materia è ciò che chiamiamo "realtà". 

39
00:02:15,900 --> 00:02:17,880
Realtà materiale. 

40
00:02:18,400 --> 00:02:21,980
Cosa possiamo concludere da questo? 

41
00:02:22,340 --> 00:02:27,080
La mente vede sempre nella mente. 

