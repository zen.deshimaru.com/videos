1
00:00:00,200 --> 00:00:03,460
Às vezes, você só encontra isso no Zen, 

2
00:00:03,780 --> 00:00:06,680
quando estamos juntos por um tempo, 

3
00:00:06,920 --> 00:00:08,960
que as pessoas não têm compaixão. 

4
00:00:09,180 --> 00:00:11,600
Eu acho que às vezes você precisa mudar a maneira como vê as coisas. 

5
00:00:11,980 --> 00:00:16,400
Em vez de pensar: "Ei, esse foi o outro que me roubou!" 

6
00:00:17,000 --> 00:00:20,680
pensando: "O que diabos eu devo fazer com isso?" 

7
00:00:21,180 --> 00:00:23,160
E tente entregá-lo. 

8
00:00:24,360 --> 00:00:27,900
Isso é muito sincero. 

9
00:00:28,300 --> 00:00:30,340
Porque as pessoas são elas mesmas. 

10
00:00:30,600 --> 00:00:32,560
Eles tentam não desempenhar um papel: 

11
00:00:32,760 --> 00:00:36,900
"Estou bem, estou bem, estou bem …" 

12
00:00:38,240 --> 00:00:41,760
Certamente, há momentos em que ficamos chateados. 

13
00:00:42,040 --> 00:00:45,280
E onde nossas reações se destacam. 

14
00:00:45,600 --> 00:00:50,780
É importante para mim que não nos instalemos lá. 

15
00:00:51,040 --> 00:00:52,880
Para não mergulhar na idéia: 

16
00:00:53,200 --> 00:00:55,700
"Em uma sessão, não há pessoas legais." 

17
00:00:55,960 --> 00:00:59,320
Quando chego a Sesshin, 

18
00:00:59,880 --> 00:01:04,060
minha ideia é transformar tudo em positivo. 

19
00:01:04,600 --> 00:01:07,340
Não sei se consigo fazer isso o tempo todo! 

20
00:01:08,360 --> 00:01:10,960
Para mim, todas as dificuldades que surgem 

21
00:01:11,180 --> 00:01:16,960
seus convites para continuar em seu estado de espírito. 

22
00:01:17,360 --> 00:01:24,680
Eu diria até que, se somos injustamente criticados, 

23
00:01:24,980 --> 00:01:27,600
podemos fazer algo positivo disso. 

24
00:01:27,900 --> 00:01:33,580
Por exemplo, força você a se levar menos a sério. 

25
00:01:37,620 --> 00:01:40,340
O que eu gosto nessa sangha, 

26
00:01:40,640 --> 00:01:42,860
é que tentamos nos ouvir um pouco mais. 

27
00:01:43,140 --> 00:01:45,720
Porque leva tempo para ouvir um ao outro mais, 

28
00:01:46,040 --> 00:01:50,460
levar em consideração alguns outros pontos de vista. 

29
00:01:50,760 --> 00:01:53,580
Sabemos que as coisas estão erradas, 

30
00:01:53,980 --> 00:01:58,120
mas sempre nos esforçamos para melhorar. 

31
00:01:59,400 --> 00:02:07,120
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

