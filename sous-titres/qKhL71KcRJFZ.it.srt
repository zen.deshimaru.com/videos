1
00:00:08,980 --> 00:00:16,230
Ciao! Vi spiegherò come sedervi per praticare la meditazione Zen,
2
00:00:17,200 --> 00:00:19,180
chiamato zazen.

3
00:00:19,180 --> 00:00:22,829
Il materiale necessario è molto semplice, un essere umano...

4
00:00:23,320 --> 00:00:28,349
e un cuscino per la meditazione. Se avete un cuscino

5
00:00:29,230 --> 00:00:33,689
abbastanza imbottito, va bene, e se non lo fai, ne fai uno con una coperta...

6
00:00:35,320 --> 00:00:39,060
o un sacco a pelo, per esempio, deve essere abbastanza solido.

7
00:00:57,490 --> 00:01:04,120
Spiego la postura della meditazione Zen.

8
00:01:05,560 --> 00:01:08,940
Prima di tutto per sapere, che è una postura...

9
00:01:09,440 --> 00:01:10,880
base

10
00:01:11,240 --> 00:01:13,440
immobile e silenzioso.

11
00:01:14,120 --> 00:01:16,660
La particolarità di questa postura

12
00:01:17,300 --> 00:01:19,300
è che lo pratichiamo

13
00:01:19,400 --> 00:01:21,400
di fronte al muro.

14
00:01:21,800 --> 00:01:27,460
Qui, naturalmente, mi metto davanti alla telecamera per insegnarvi.

15
00:01:27,960 --> 00:01:34,640
Quello che devi fare è sederti su un cuscino, come ti ho spiegato, che deve essere

16
00:01:35,000 --> 00:01:37,000
sufficientemente imbottito, sufficientemente duro.

17
00:01:37,300 --> 00:01:44,060
Ci si siede bene in fondo al cuscino, non ci si siede sul bordo che si può far scivolare,

18
00:01:44,440 --> 00:01:48,360
e bisogna incrociare le gambe il più possibile.

19
00:01:48,820 --> 00:01:56,220
Si possono attraversare così, così, così, così, così, si chiama il mezzo loto,

20
00:01:56,660 --> 00:01:59,410
se metti l’altra gamba qui, si chiama loto.

21
00:02:02,860 --> 00:02:07,620
L’importante è che le ginocchia vadano a terra,

22
00:02:08,220 --> 00:02:09,660
per spingere,

23
00:02:10,789 --> 00:02:13,420
per esercitare una pressione al ginocchio sul pavimento.

24
00:02:13,880 --> 00:02:20,079
Allora, come facciamo? Fallo senza alcuno sforzo fisico,

25
00:02:20,720 --> 00:02:25,240
niente di muscoloso. Postura di meditazione Zen, zazen,

26
00:02:25,900 --> 00:02:27,780
è una postura in cui bisogna lasciarsi andare.

27
00:02:28,300 --> 00:02:33,000
In modo che le ginocchia vadano a terra

28
00:02:33,920 --> 00:02:37,360
inclinerai leggermente il bacino

29
00:02:38,440 --> 00:02:42,220
in avanti, la parte superiore del bacino va in avanti,

30
00:02:42,760 --> 00:02:46,300
il coccige va un po’ indietro.

31
00:02:46,760 --> 00:02:48,760
Te lo mostro di profilo

32
00:02:55,700 --> 00:03:03,040
Il mio bacino non è messo così, come se stessi guidando una macchina, per esempio,

33
00:03:03,260 --> 00:03:08,700
vedete qui le mie ginocchia salgono dietro perché il mio centro di gravità

34
00:03:08,900 --> 00:03:15,700
è andato dietro al mio corpo. Voglio che il mio centro di gravità vada davanti e lo faccio con il bacino.

35
00:03:15,940 --> 00:03:24,300
Naturalmente, il centro di gravità passa davanti con la gravità, le mie ginocchia vanno a terra.

36
00:03:24,300 --> 00:03:30,860
Ecco, questo è ciò che si deve stabilire prima nella postura,

37
00:03:31,160 --> 00:03:37,040
un sedile in cui le ginocchia vanno a terra.

38
00:03:37,480 --> 00:03:40,200
Quando ti sei reso conto che seduto

39
00:03:40,800 --> 00:03:45,500
dal bacino, che è leggermente inclinato in avanti...

40
00:03:45,900 --> 00:03:49,280
si sta andando a raddrizzare la colonna vertebrale il più possibile

41
00:03:50,200 --> 00:03:52,860
fino in cima alla testa.

42
00:03:54,580 --> 00:04:02,780
Supponiamo che la colonna arrivi fino in cima. Come se avessi messo un gancio

43
00:04:05,060 --> 00:04:10,500
e vado su. Il che significa che allungo il collo, rimbocco il mento,

44
00:04:11,150 --> 00:04:15,699
Vedete, non ho la testa sulle spalle, non ho la testa sulle spalle, non ho la schiena ricurva...

45
00:04:16,849 --> 00:04:18,849
Sono il più etero possibile.

46
00:04:19,520 --> 00:04:22,000
Ti mostro di profilo

47
00:04:23,300 --> 00:04:28,040
Non sono così, non sono così...

48
00:04:28,560 --> 00:04:35,780
o piegato o altro, sono il più dritto possibile.

49
00:04:38,800 --> 00:04:43,820
Quando sei riuscito a sistemarti dritto, non ti muovi.

50
00:04:45,320 --> 00:04:47,160
Vado avanti.

51
00:04:47,380 --> 00:04:49,280
la mano sinistra

52
00:04:49,540 --> 00:04:51,540
le dita sono unite

53
00:04:52,250 --> 00:04:57,220
le dita della mano destra sono unite tra loro e io sovrapporrò le mie dita della mano sinistra

54
00:04:58,070 --> 00:05:00,070
sulle dita della mano destra

55
00:05:01,490 --> 00:05:03,680
I miei pollici si incontrano

56
00:05:05,730 --> 00:05:08,329
Sovrapposto alle dita della mano sinistra,

57
00:05:09,630 --> 00:05:11,630
Formo una linea orizzontale.

58
00:05:12,690 --> 00:05:20,059
Il bordo delle mie mani dal polso fino a qui, il bordo delle mie mani

59
00:05:21,030 --> 00:05:23,929
lo applicherai alla base dell’addome...

60
00:05:25,280 --> 00:05:27,820
per fare questo,

61
00:05:29,580 --> 00:05:33,440
utilizzare qualcosa che vi permetta di incuneare le mani.

62
00:05:34,160 --> 00:05:36,280
in modo da poter

63
00:05:36,700 --> 00:05:38,620
lasciarsi andare completamente

64
00:05:38,880 --> 00:05:43,300
la tensione nelle spalle la postura è una postura in cui bisogna lasciarsi andare

65
00:05:44,280 --> 00:05:50,089
Allentare la tensione, vedi i miei gomiti sono liberi Sono stato così o così per molto tempo,

66
00:05:51,380 --> 00:05:53,940
in silenzio. Ma non mi sforzo

67
00:05:54,540 --> 00:06:00,940
per tenere le mani, lo sforzo è quello di tenere i pollici in orizzontale.

68
00:06:01,220 --> 00:06:03,220
una volta che ho capito

69
00:06:03,750 --> 00:06:05,750
questi punti di postura

70
00:06:06,600 --> 00:06:08,600
Non mi muovo,

71
00:06:09,160 --> 00:06:11,160
Questa è la postura fisica.

72
00:06:20,060 --> 00:06:27,420
Dopo questa prima parte sulla postura fisica, la seconda parte: l’atteggiamento mentale.

73
00:06:28,380 --> 00:06:32,340
Una volta in posizione di meditazione Zen, zazen

74
00:06:33,620 --> 00:06:37,960
rivolto verso il muro, cosa devo fare, cosa devo fare?

75
00:06:39,720 --> 00:06:42,260
Nel silenzio di questa postura,

76
00:06:43,770 --> 00:06:48,380
ti renderai conto che pensi,

77
00:06:49,350 --> 00:06:51,769
o anche che non riesci a smettere di pensare

78
00:06:52,290 --> 00:06:57,469
e ci sono migliaia di pensieri che arrivano, arrivano, arrivano.

79
00:06:57,980 --> 00:07:02,420
Stai per dirmi che è un fenomeno naturale...

80
00:07:03,500 --> 00:07:05,500
Non si può controllare.

81
00:07:05,940 --> 00:07:11,900
Quello che faremo, durante la meditazione Zen, lasceremo passare i nostri pensieri.

82
00:07:12,740 --> 00:07:15,580
cioè che, in questa immobilità..,

83
00:07:16,040 --> 00:07:20,440
potremo osservare ciò che pensiamo,

84
00:07:21,040 --> 00:07:25,960
osserverete i vostri pensieri e questi pensieri invece di intrattenerli,

85
00:07:26,360 --> 00:07:32,860
invece di coltivarli, li lascerai passare.

86
00:07:33,160 --> 00:07:39,220
Quindi non si tratta di rifiutare nulla, di nuovo si tratta di lasciar andare, di lasciar andare.

87
00:07:40,080 --> 00:07:42,400
Come passare

88
00:07:43,260 --> 00:07:45,260
attenzione, concentrazione,

89
00:07:46,080 --> 00:07:47,860
proprio qui, proprio ora,

90
00:07:48,240 --> 00:07:50,240
sul tuo respiro.

91
00:07:55,930 --> 00:07:58,680
Respirare nello Zen è la respirazione addominale.

92
00:08:00,460 --> 00:08:08,320
Si respira attraverso, attraverso la narice, e si respira dolcemente,

93
00:08:09,640 --> 00:08:13,780
lentamente, ovviamente in silenzio, meditazione silenziosa.

94
00:08:15,180 --> 00:08:20,180
La caratteristica di questa respirazione è quella di focalizzare l’attenzione

95
00:08:20,540 --> 00:08:24,120
sul lato dell’espirazione.

96
00:08:24,420 --> 00:08:26,420
Noi ispiriamo

97
00:08:26,620 --> 00:08:33,680
attraverso il naso, in silenzio, essere generoso, riempire

98
00:08:34,300 --> 00:08:41,880
e il volto dell’espirazione quando stai per espirare tutta l’aria lo fai lentamente,

99
00:08:42,340 --> 00:08:46,940
lentamente, come per diventare sempre più lunghi

100
00:08:56,700 --> 00:09:00,140
Quando siete alla fine della vostra scadenza

101
00:09:00,880 --> 00:09:02,380
si aspira

102
00:09:02,850 --> 00:09:06,340
e di nuovo scade

103
00:09:07,520 --> 00:09:12,020
e tu ci stai mettendo di più, più lentamente, più a fondo,

104
00:09:12,200 --> 00:09:15,760
un’espirazione più profonda, come un’onda, di nuovo.

105
00:09:16,280 --> 00:09:18,280
dall’altra parte dell’oceano

106
00:09:18,560 --> 00:09:20,560
in arrivo sulla spiaggia

107
00:09:22,580 --> 00:09:24,580
Tutto questo con il naso.

108
00:09:30,060 --> 00:09:33,100
Se ti concentri qui e ora

109
00:09:33,400 --> 00:09:40,880
sui punti della postura, i punti fisici quindi l’attenzione, un’attenzione qui e ora nel vostro corpo

110
00:09:41,420 --> 00:09:48,960
se vi concentrate qui e ora, allo stesso tempo, sul non seguire i vostri pensieri,

111
00:09:49,820 --> 00:09:52,640
Tornando ai punti che posano,

112
00:09:52,980 --> 00:09:55,880
se ti concentri qui e ora

113
00:09:56,460 --> 00:10:02,320
sull’instaurazione di un respiro armonioso, che va e viene, e a poco a poco

114
00:10:02,900 --> 00:10:06,180
che il momento della scadenza,

115
00:10:06,800 --> 00:10:10,620
si esce l’aria si allunga sempre di più.

116
00:10:11,600 --> 00:10:16,240
Non avrete il tempo di pensare a... quello che vuoi!

117
00:10:16,800 --> 00:10:22,940
Sei qui e ora sei in una posizione di zazen.

118
00:10:23,520 --> 00:10:30,360
In postura, né il corpo né la materia, in postura. Vi ricordo di premere,

119
00:10:30,660 --> 00:10:34,420
si applica una pressione dalle ginocchia al pavimento,

120
00:10:34,680 --> 00:10:38,840
spingi la terra con le ginocchia

121
00:10:38,900 --> 00:10:40,980
e si raddrizza

122
00:10:41,500 --> 00:10:43,500
il più possibile la colonna vertebrale

123
00:10:44,089 --> 00:10:48,999
fino alla sommità del cranio come per spingere il cielo con la testa.

124
00:10:49,760 --> 00:10:55,270
Mano sinistra nella mano destra con i pollici orizzontali il bordo delle mani

125
00:10:55,780 --> 00:10:58,440
contro il basso addome.

126
00:10:58,840 --> 00:11:00,820
Sono immobile.

127
00:11:01,190 --> 00:11:03,160
verso il muro

128
00:11:03,170 --> 00:11:05,170
Di fronte al muro non c’è niente da vedere in particolare,

129
00:11:05,360 --> 00:11:11,169
quello che farò invece è volgere lo sguardo verso l’interno,

130
00:11:11,959 --> 00:11:13,959
così i sensi,

131
00:11:14,410 --> 00:11:16,959
Non li userò più durante questa meditazione.

132
00:11:17,900 --> 00:11:23,360
Sono di fronte al muro, quindi non c’è niente da vedere, quindi è come se

133
00:11:24,080 --> 00:11:28,640
Prendo la mia vista, la mia visione, la prendo, la prendo, la metto davanti a me, non mi interessa più.

134
00:11:29,020 --> 00:11:33,180
Durante la meditazione, mediterete con la vista,

135
00:11:34,160 --> 00:11:37,240
45° in diagonale verso il basso

136
00:11:38,300 --> 00:11:46,920
Si può meditare ad occhi chiusi, ma se si tengono gli occhi socchiusi si mantiene il contatto con la realtà.

137
00:11:47,340 --> 00:11:49,980
Non andate particolarmente nei vostri pensieri.

138
00:11:50,320 --> 00:11:54,380
Dato che siamo in silenzio, non usiamo nemmeno l’udito,

139
00:11:54,600 --> 00:11:58,980
quindi l’udienza, non ce n’è bisogno, non c’è musica, non c’è niente.

140
00:11:59,260 --> 00:12:03,740
e abbiamo già visto che siamo immobili.

141
00:12:04,260 --> 00:12:09,960
A poco a poco, ci siamo improvvisamente isolati dalle richieste del mondo esterno...

142
00:12:10,400 --> 00:12:12,680
ci sbarazziamo di tutto questo.

143
00:12:13,140 --> 00:12:18,920
La meditazione, si sta seduti fisicamente, è il corpo che è in postura,

144
00:12:19,910 --> 00:12:26,350
ti tagli fuori dal mondo esterno, metti tutto in modalità aereo.

145
00:12:26,920 --> 00:12:33,740
Non meditare con il cellulare accanto a te.

146
00:13:09,580 --> 00:13:12,000
Per finire un consiglio per i principianti,

147
00:13:12,660 --> 00:13:17,740
non è facile essere così, confinati ora,

148
00:13:18,140 --> 00:13:21,720
meditando davanti a un muro,

149
00:13:22,820 --> 00:13:25,940
ma, uh, è davvero un buon aiuto.

150
00:13:26,220 --> 00:13:28,640
Vi consiglio vivamente di esercitarvi,

151
00:13:28,900 --> 00:13:36,940
per provarlo e vedrai che invece di avere pensieri che possono non essere molto brillanti,

152
00:13:37,260 --> 00:13:43,920
non molto felice, invece di essere di cattivo umore, invece di sentire tensione, stress,

153
00:13:44,560 --> 00:13:48,569
tutto questo è normale, con questo lavoro sul corpo.

154
00:13:49,200 --> 00:13:55,500
l’atteggiamento della mente che lasci andare, ti allontani da tutto ciò che ti succede.

155
00:13:55,940 --> 00:13:58,240
vedrai che ti calmerai

156
00:13:58,560 --> 00:13:59,900
fisicamente

157
00:14:00,100 --> 00:14:01,480
emotivamente

158
00:14:01,660 --> 00:14:04,820
il respiro, tutto quanto, ti aiutera’ a lasciarti andare...

159
00:14:05,280 --> 00:14:09,880
Se vivete con qualcuno, sarà di grande aiuto per voi e per la vostra famiglia.

160
00:14:10,340 --> 00:14:14,480
Quindi non dubitate di esercitarvi. Grazie.
