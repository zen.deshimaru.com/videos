1
00:00:00,280 --> 00:00:02,560
Niewiele wiem o sofrologii.

2
00:00:02,800 --> 00:00:12,140
Wiem, że jest to zastosowanie pewnych praktyk wellness, rozwoju osobistego,

3
00:00:12,540 --> 00:00:17,160
i jest tam sofologia, która nazywa się "orientalistą",

4
00:00:17,440 --> 00:00:24,080
która jest bardzo inspirowana praktykami jogicznymi, buddyjskimi lub indyjskimi.

5
00:00:24,540 --> 00:00:27,940
Zen jest bardzo proste.

6
00:00:28,380 --> 00:00:38,400
Opiera się na podstawowych ludzkich pozycjach.

7
00:00:39,820 --> 00:00:48,480
Zamierzam pokazać ci pierwszą podstawową postawę Zen.

8
00:01:01,500 --> 00:01:06,120
Ludzie byli kiedyś małpami.

9
00:01:06,460 --> 00:01:08,580
Tak chodzili.

10
00:01:09,200 --> 00:01:20,360
Krok po kroku, siedzieli prosto, jak mistrzowie Zen.

11
00:01:22,900 --> 00:01:24,900
Potem wyprostowali się.

12
00:01:25,180 --> 00:01:28,600
Tak chodzili.

13
00:01:29,680 --> 00:01:33,760
A ludzka świadomość ewoluowała

14
00:01:35,180 --> 00:01:39,980
wraz z postawą.

15
00:01:40,340 --> 00:01:46,000
Postawa ciała to podstawowa myśl.

16
00:01:46,340 --> 00:01:52,200
Na przykład Usain Bolt, najszybszy biegacz na świecie,

17
00:01:52,540 --> 00:01:55,160
wykonał ten gest.

18
00:01:55,440 --> 00:02:02,300
Wszyscy rozumieją, co to znaczy, a jednak nie ma słów.

19
00:02:02,640 --> 00:02:05,440
To tylko postawa.

20
00:02:05,900 --> 00:02:11,120
Sposób, w jaki siedzimy i chodzimy,

21
00:02:11,520 --> 00:02:13,400
co robisz ze swoimi rękami,

22
00:02:13,700 --> 00:02:16,500
to wszystko jest fundamentalnie ważne.

23
00:02:18,160 --> 00:02:21,440
W zen, jest postawa Buddy.

24
00:02:22,980 --> 00:02:31,120
Istniał przed Buddą, jest wymieniony w Bhagawad-Gicie.

25
00:02:32,400 --> 00:02:39,880
Siedzisz na poduszce z trawy i zginasz nogi.

26
00:02:41,560 --> 00:02:47,140
W naturze, prehistoryczny człowiek przekazał

27
00:02:47,440 --> 00:02:53,220
Uszami, nosem, oczami, by widzieć daleko.

28
00:02:54,920 --> 00:02:59,000
Więc chcieli się wyprostować.

29
00:02:59,400 --> 00:03:04,220
A gdy się wyprostowali, ich świadomość wyostrzyła się, obudziła.

30
00:03:06,040 --> 00:03:08,720
To jest pierwsza postawa.

31
00:03:08,980 --> 00:03:11,580
Normalnie, musimy zrobić pełny lotos.

32
00:03:12,060 --> 00:03:14,260
Tak się krzyżuje nogi.

33
00:03:14,700 --> 00:03:18,460
Nazywa się to "preclem".

34
00:03:24,960 --> 00:03:30,980
To bardzo ważne, aby przyjąć taką postawę.

35
00:03:31,380 --> 00:03:37,380
Poprzedni mówca mówił o dynamice i relaksie.

36
00:03:37,780 --> 00:03:42,340
Mamy współczulny i parasympatyczny układ nerwowy.

37
00:03:42,600 --> 00:03:45,800
Mamy mięśnie zewnętrzne i głębokie.

38
00:03:46,320 --> 00:03:51,400
Mamy płytki oddech i głęboki oddech.

39
00:03:51,820 --> 00:03:54,620
Mamy powierzchowną, korową świadomość,

40
00:03:54,920 --> 00:03:59,760
i głębokiej świadomości, reptilian lub midbrain.

41
00:04:00,340 --> 00:04:04,160
Stać prosto w tej postawie,

42
00:04:04,440 --> 00:04:06,800
musisz pchać ziemię kolanami.

43
00:04:10,240 --> 00:04:15,640
Potrzeba wysiłku, aby być dynamicznym i mieć dobrą postawę.

44
00:04:16,000 --> 00:04:22,180
Mój pan [Deshimaru] nalegał na piękno i siłę postawy.

45
00:04:22,460 --> 00:04:31,280
Ale w lotosie, im bardziej się relaksujesz, tym bardziej twoje stopy rosną na udach,

46
00:04:31,520 --> 00:04:35,100
i im bardziej kolana rosną na ziemi.

47
00:04:35,620 --> 00:04:40,280
Im bardziej się relaksujesz, tym bardziej jesteś dynamiczny, tym prostszy i silniejszy jesteś.

48
00:04:40,560 --> 00:04:46,560
Nie ma to nic wspólnego z odpoczynkiem na krześle lub w łóżku.

49
00:04:46,940 --> 00:04:57,280
To sposób na porzucenie ciała dla siebie,

50
00:05:04,380 --> 00:05:09,700
podczas przebudzenia.

51
00:05:10,120 --> 00:05:16,240
To jest pierwsza postawa, siedząca postawa Buddy.

52
00:05:16,680 --> 00:05:20,280
Ręce też są bardzo ważne.

53
00:05:20,640 --> 00:05:23,600
Zawsze robisz coś z rękami:

54
00:05:23,860 --> 00:05:26,560
pracując, chroniąc siebie…

55
00:05:26,960 --> 00:05:28,740
Ręce są bardzo wyraziste

56
00:05:29,040 --> 00:05:31,040
i są podłączone do mózgu.

57
00:05:31,500 --> 00:05:41,040
Zabranie tej mudry rękoma zmienia świadomość.

58
00:05:41,680 --> 00:05:47,680
Koncentracja jest w naszych rękach, ciele, układzie nerwowym i mózgu.


59
00:05:48,100 --> 00:05:53,360
Inną ważną rzeczą jest myśl, świadomość.

60
00:05:53,820 --> 00:05:57,680
Jeśli jesteś jak Rodin’s Thinker,

61
00:05:58,000 --> 00:06:01,100
nie masz w ogóle tego samego sumienia.

62
00:06:09,960 --> 00:06:12,240
że kiedy twoja postawa jest idealnie prosta,

63
00:06:12,580 --> 00:06:14,640
zwłaszcza z rozciągniętą szyją,

64
00:06:15,700 --> 00:06:18,400
z głową prosto w górę, otwartą na niebo,

65
00:06:18,760 --> 00:06:22,020
nos i pępek wyrównane

66
00:06:25,060 --> 00:06:27,220
a ty oddychasz całą swoją istotą.

67
00:06:29,020 --> 00:06:31,860
Myśl nie jest już wtedy oddzielona od ciała.

68
00:06:32,240 --> 00:06:36,100
Nie ma już dysocjacji pomiędzy naszą świadomością.

69
00:06:36,300 --> 00:06:39,080
i każdą komórkę w naszych ciałach.

70
00:06:39,500 --> 00:06:45,520
W Zen, nazywamy to "ciało umysłu", jednym słowem.

71
00:06:46,360 --> 00:06:50,540
To jest podstawowa postawa: zazen.

72
00:06:56,380 --> 00:07:00,200
Druga postawa to postawa chodząca.

73
00:07:00,680 --> 00:07:06,340
W zazen, porzucamy wszystko i w ogóle się nie ruszamy.

74
00:07:06,560 --> 00:07:08,280
Więc, świadomość jest wyjątkowa.

75
00:07:08,720 --> 00:07:13,800
Ale jest jeszcze jedna medytacja, która odbywa się podczas spaceru.

76
00:07:16,180 --> 00:07:18,160
Musimy ruszać.

77
00:07:18,540 --> 00:07:20,540
Robimy coś, ruszamy dalej.

78
00:07:21,180 --> 00:07:24,180
Nie idziesz do przodu, żeby coś złapać.

79
00:07:24,580 --> 00:07:28,380
Przyspieszasz oddech swoim chodzeniem.

80
00:07:29,020 --> 00:07:30,740
Choreograf Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
który był uczniem mojego mistrza, Deshimaru,

82
00:07:34,060 --> 00:07:38,040
kazał swoim tancerzom ćwiczyć to każdego dnia.

83
00:07:39,060 --> 00:07:45,340
Druga postawa to spacer "krewnych".

84
00:07:45,880 --> 00:07:51,040
Zauważ, co robisz ze swoimi rękami.

85
00:07:51,680 --> 00:07:57,840
Bokser, który robi sobie zdjęcie, robi to rękoma.

86
00:07:58,240 --> 00:08:05,220
Kiedy kładziesz takie ręce, automatycznie mózg wyraża silną agresywność.

87
00:08:05,820 --> 00:08:10,360
Możesz włożyć ręce do kieszeni.

88
00:08:10,840 --> 00:08:13,140
"Gdzie położyłem mój portfel? »

89
00:08:13,640 --> 00:08:16,360
"Ktoś ukradł moją komórkę! »

90
00:08:16,700 --> 00:08:19,580
Możemy wsadzić sobie palce do nosa.

91
00:08:21,660 --> 00:08:25,820
Wszystko, co zostało tu zbudowane, zostało zbudowane ręcznie.

92
00:08:26,260 --> 00:08:28,820
To niezwykłe, ręka.

93
00:08:29,160 --> 00:08:31,860
Jesteśmy jedynymi zwierzętami z rękami,

94
00:08:32,260 --> 00:08:35,340
są one związane z naszą ewolucją.

95
00:08:37,700 --> 00:08:40,660
Ręce też medytują.

96
00:08:40,940 --> 00:08:43,580
Włożyłeś kciuk w pięść.

97
00:08:43,820 --> 00:08:49,420
Połóż korzeń kciuka pod mostkiem, połóż na nim drugą rękę.

98
00:08:49,760 --> 00:08:55,060
Te szczegóły posturalne istnieją od czasów Buddy.

99
00:08:55,380 --> 00:09:02,520
Starożytne teksty wyjaśniają, że zostały one przekazane do Chin z Indii.

100
00:09:02,960 --> 00:09:08,920
To ustna transmisja z człowieka na człowieka.

101
00:09:09,480 --> 00:09:12,500
W buddyzmie istnieje wiele aspektów.

102
00:09:12,780 --> 00:09:17,320
Ale aspekt medytacyjny i posturalny jest bardzo ważny.

103
00:09:17,820 --> 00:09:28,540
Było tam dwóch wielkich uczniów Buddy, Śāriputra i Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Ich duchowy mistrz nie był pewien:

105
00:09:37,220 --> 00:09:43,680
"Nie jestem pewien, czy mam sztuczkę wydostania się z życia i śmierci. »

106
00:09:43,940 --> 00:09:52,860
"Jeśli znajdziesz mistrza, który tego nauczy, daj mi znać, a ja pójdę z tobą. »

107
00:09:53,620 --> 00:10:04,320
Pewnego dnia widzą mnicha, takiego jak ja, widzisz, idącego w dół drogi.

108
00:10:04,780 --> 00:10:10,000
Oni są zdumieni pięknem tego człowieka.

109
00:10:10,420 --> 00:10:16,200
Potem ten mnich zatrzymuje się w gaju, żeby się wysikać.

110
00:10:24,440 --> 00:10:33,240
Mówi mu Śāriputra, który jest uważany za jedną z największych inteligencji Indii w tym czasie:

111
00:10:34,180 --> 00:10:43,940
"Jesteśmy zdumieni twoją obecnością, kto jest twoim panem? »

112
00:10:46,560 --> 00:10:49,540
- "Moim mistrzem jest Budda Shakyamuni."

113
00:10:49,860 --> 00:10:53,520
- "Wyjaśnij nam swoją doktrynę! »

114
00:10:53,780 --> 00:10:58,220
- "Nie mogę ci tego wyjaśnić, jestem w tym zbyt nowy. »

115
00:10:59,100 --> 00:11:04,440
A oni podążali za tym chłopcem tylko z powodu sposobu w jaki chodzi.

116
00:11:04,800 --> 00:11:06,980
Sposób, w jaki on coś z niej wyciąga.



117
00:11:07,440 --> 00:11:11,680
Więc druga postawa to chodzenie.

118
00:11:12,020 --> 00:11:15,280
Kiedy ludzie robią zazen, to jest bardzo silne.

119
00:11:15,500 --> 00:11:21,580
Nawet praktykujący sztuki walki są pod wrażeniem siły, jaką to daje.

120
00:11:21,940 --> 00:11:25,860
Przez siłę oddechu.

121
00:11:26,240 --> 00:11:35,520
Ale jak znaleźć tę energię, tę pełnię, w codziennym życiu?

122
00:11:35,840 --> 00:11:39,220
Ten spokój, ta koncentracja.

123
00:11:39,600 --> 00:11:42,960
To trudna sprawa.

124
00:11:43,280 --> 00:11:47,140
Ponieważ postawa zazen jest niezwykle precyzyjna.

125
00:11:47,600 --> 00:11:55,100
Na początku skupiamy się na technicznym aspekcie spaceru krewnych.

126
00:11:55,540 --> 00:11:58,800
Ale kiedy już poznamy wszystkie szczegóły,

127
00:11:59,300 --> 00:12:02,160
w każdej chwili w naszym codziennym życiu

128
00:12:02,440 --> 00:12:08,400
(nawet jeśli zawsze o tym zapomnę), możemy to przećwiczyć.

129
00:12:10,600 --> 00:12:16,740
Możesz ponownie skupić się na swojej duchowej istocie.

130
00:12:17,740 --> 00:12:22,760
Na twojej istocie, która gówno robi.

131
00:12:23,320 --> 00:12:31,340
Mówiłem ci o rękach.

132
00:12:31,840 --> 00:12:39,980
Podstawową postawą religijną jest ta.

133
00:12:40,560 --> 00:12:44,240
Kiedy trzymasz ręce razem w ten sposób,

134
00:12:45,080 --> 00:12:48,260
po prostu to robię,

135
00:12:48,580 --> 00:12:51,280
to ma wpływ na mózg,

136
00:12:51,620 --> 00:12:55,340
buduje most pomiędzy dwoma półkulami mózgowymi.

137
00:12:55,740 --> 00:12:59,960
Ta postawa jest stosowana we wszystkich religiach.

138
00:13:00,280 --> 00:13:02,180
To całkiem proste.

139
00:13:02,560 --> 00:13:05,640
Zobacz, jak ważne są ręce!

140
00:13:06,060 --> 00:13:13,280
Trzecia postawa…

141
00:13:13,860 --> 00:13:16,520
(Myślałem, że są cztery…)

142
00:13:16,880 --> 00:13:20,580
Oh, tak, jest taka skłonna postawa, ale nie będziemy o tym rozmawiać.

143
00:13:21,000 --> 00:13:25,520
Kłamliwa postawa, jak spać.

144
00:13:26,700 --> 00:13:31,980
Wyjaśniłem ci, jak prehistoryczni ludzie czołgali się na wszystkich czworakach,

145
00:13:32,260 --> 00:13:34,440
krok po kroku, wyprostowali się,

146
00:13:34,700 --> 00:13:38,760
i że nasza ewolucja pochodzi z naszej postawy,

147
00:13:39,060 --> 00:13:42,580
z nawadniania kręgosłupa, który idzie do mózgu,

148
00:13:42,940 --> 00:13:46,400
i mózg, który jest niezwykłym instrumentem,

149
00:13:46,700 --> 00:13:51,440
z czego tylko 10-15% jego całkowitej użyteczności jest znane.

150
00:13:51,760 --> 00:13:57,740
Mamy wielką rzecz pomiędzy naszymi uszami, którą nie wykorzystujemy.

151
00:14:08,640 --> 00:14:12,380
Każdy ma swój własny mózg!

152
00:14:19,020 --> 00:14:22,140
Każdy ma swój własny mózg!

153
00:14:22,400 --> 00:14:25,200
Jak adres IP.

154
00:14:30,220 --> 00:14:33,220
Każdy ma swój własny mózg!

155
00:14:33,940 --> 00:14:35,900
A my jesteśmy sami!

156
00:14:36,700 --> 00:14:41,940
To bardzo ważne dla tego, co mam zamiar ci powiedzieć.

157
00:14:44,760 --> 00:14:50,040
Nasze stopy (wracamy do naszych stóp…).

158
00:14:51,720 --> 00:14:56,420
Wiesz, że żyjemy na małej Ziemi.

159
00:14:57,040 --> 00:15:01,420
Widziałeś film "Grawitacja"?

160
00:15:02,120 --> 00:15:07,060
Jest to jeden z filmów, które naprawdę budzą ludzką świadomość.

161
00:15:07,560 --> 00:15:13,380
W Hollywood czasami wykonują jakieś manipulacyjne prace.

162
00:15:13,680 --> 00:15:17,220
Ale także czasami informacji i duchowej ewolucji.

163
00:15:17,760 --> 00:15:19,660
Ten film jest jednym z nich.

164
00:15:20,040 --> 00:15:26,920
I sami widzicie, jak krucha jest nasza Ziemia.

165
00:15:29,200 --> 00:15:33,400
Zazwyczaj, gdy patrzymy na niebo, rabujemy: "Jest nieskończenie niebieskie!". ».

166
00:15:33,760 --> 00:15:38,940
Nie. Nie na zawsze. 30 kilometrów atmosfery.

167
00:15:39,360 --> 00:15:44,660
Koniec atmosfery wynosi od 30 do 1000 kilometrów.

168
00:15:52,260 --> 00:15:56,700
Nawet tysiąc mil to nic.

169
00:15:58,800 --> 00:16:04,780
Gdybym umieścił cię 20 mil stąd, nie przetrwałbyś ani minuty.

170
00:16:05,380 --> 00:16:15,940
Nawet poza podatkami, nasze warunki życia są niepewne.

171
00:16:17,420 --> 00:16:20,440
Ziemia jest małą, kruchą, żyjącą istotą.

172
00:16:21,120 --> 00:16:30,660
Nasza biosfera jest malutka i krucha, musi być zachowana.

173
00:16:30,940 --> 00:16:35,100
Powinniśmy zawsze troszczyć się o zachowanie tej rzeczy.

174
00:16:36,860 --> 00:16:39,940
Bo urodziliśmy się z Ziemi.

175
00:16:40,220 --> 00:16:45,700
Ten cud życia, który dokonał się na Ziemi, jest efemeryczny.

176
00:16:46,240 --> 00:16:47,840
Urodziliśmy się z Ziemi.


177
00:16:48,220 --> 00:16:53,320
I wszyscy mamy stopy skierowane do tego samego centrum, do centrum Ziemi.

178
00:16:53,900 --> 00:16:57,760
Więc nasze stopy są we wspólnocie.

179
00:17:01,860 --> 00:17:08,680
Ale nasza głowa jest wyjątkowa, nikt nie wskazuje w tym samym kierunku.

180
00:17:18,400 --> 00:17:23,500
Stąd ta trzecia postawa, którą ci pokażę.

181
00:17:23,800 --> 00:17:28,140
Jest to wspólne dla wielu religii, nazywa się to "prostracją".

182
00:17:28,380 --> 00:17:31,240
Tak to robimy.

183
00:17:40,640 --> 00:17:42,780
Ach, to miłe uczucie!

184
00:17:43,060 --> 00:17:49,940
Kiedy robimy tę praktykę, to jest jak wtedy, gdy się kochamy:

185
00:17:50,260 --> 00:17:58,460
czasami nie uprawiamy seksu przez 10 lat, 20 lat, 6 miesięcy…

186
00:17:58,780 --> 00:18:07,460
a kiedy znów się kochamy, mówimy: "Ach, to dobre uczucie, zapomniałem, jakie to było dobre! ».

187
00:18:08,220 --> 00:18:12,200
Kiedy robisz tę prostatę, to jest to samo.

188
00:18:13,460 --> 00:18:15,740
Nie żartuję.

189
00:18:16,240 --> 00:18:20,260
Reformujemy nasze mózgi.

190
00:18:21,060 --> 00:18:26,280
Kiedy ustawisz swój mózg w linii ze środkiem Ziemi.

191
00:18:26,620 --> 00:18:29,080
Nie chodzi tylko o pokorę,

192
00:18:29,360 --> 00:18:32,720
albo kłaniać się przed posągami czy czymkolwiek innym.

193
00:18:34,600 --> 00:18:38,880
To magiczna, podstawowa, szamańska rzecz, może…

194
00:18:39,320 --> 00:18:41,860
Od chwili, gdy się kłaniasz…

195
00:18:42,180 --> 00:18:43,340
Mój pan zwykł mawiać:

196
00:18:43,660 --> 00:18:47,440
"Gdyby głowy państw pokłonywały się raz dziennie,"

197
00:18:47,800 --> 00:18:50,200
"to zmieniłoby świat". »

198
00:18:50,640 --> 00:18:54,560
To nie żartobliwy żart ani mistycyzm.

199
00:18:55,280 --> 00:18:58,780
Musi być to naukowo mierzalne.

200
00:19:00,780 --> 00:19:05,280
Przeformatowuje mózg, żeby się wylądować.

201
00:19:05,740 --> 00:19:09,280
Ciągle jest w swojej indywidualnej linii.

202
00:19:09,620 --> 00:19:17,300
I tam łączysz się ze swoim korzeniem, ze swoimi braćmi, ze środkiem Ziemi.

203
00:19:19,940 --> 00:19:25,380
Czasami robię to i czuję coś mocnego w mojej głowie.

204
00:19:25,660 --> 00:19:27,540
To takie dobre uczucie.

205
00:19:27,820 --> 00:19:39,420
Jednocześnie relatywizuje ono zło, które nieumyślnie czynimy wokół siebie.

206
00:19:40,560 --> 00:19:42,960
To dobre lekarstwo.

207
00:19:43,180 --> 00:19:47,040
Wszystkie te postawy są podstawowymi lekami.

208
00:19:47,760 --> 00:19:53,260
Przygotowałem nieco intelektualny tekst.

209
00:19:53,520 --> 00:19:58,140
Ale nie mogę dostać się od jednego podmiotu do drugiego.

210
00:19:58,440 --> 00:20:02,300
Co ja mówiłem?

211
00:20:06,860 --> 00:20:12,040
Wracam do ciała i prehistorii.

212
00:20:12,780 --> 00:20:28,200
Udowodniono, że neandertalczycy mieli praktykę religijną.

213
00:20:30,680 --> 00:20:36,340
Znaleźliśmy kilka grobów.

214
00:20:36,860 --> 00:20:40,440
Przynosili ofiary dla swoich zmarłych.

215
00:20:40,760 --> 00:20:43,680
Kiedyś przeprowadzali religijną ceremonię za swoich zmarłych.

216
00:20:48,420 --> 00:20:52,180
Co ciekawe, to nie tylko to!

217
00:20:52,720 --> 00:20:56,260
Kiedy robili te religijne ceremonie -

218
00:20:56,740 --> 00:21:01,700
- religijne w tym sensie, że komunikowały się z niewidzialnym -

219
00:21:02,540 --> 00:21:06,940
- z życiem wiecznym, ze zmarłymi -

220
00:21:08,800 --> 00:21:11,020
- łączyli się z niewidzialnym.

221
00:21:11,420 --> 00:21:18,140
W tym czasie nie istniał jeszcze jednak język wyartykułowany.

222
00:21:18,560 --> 00:21:24,380
Więc, religia istniała przed językiem wyartykułowanym.

223
00:21:24,840 --> 00:21:30,140
(Jestem gotów przejść do języka wyartykułowanego…)

224
00:21:31,480 --> 00:21:36,500
(To bardzo ciekawe, napisanie tego zajęło mi cztery dni…)

225
00:21:43,820 --> 00:21:45,660
Mój pan zwykł mawiać:

226
00:21:45,920 --> 00:21:48,880
"Zazen jest religią przed religią. »

227
00:21:49,380 --> 00:21:54,380
Powiedzielibyśmy, "Tak, zawsze chcesz być pierwszy…"

228
00:21:55,860 --> 00:21:59,700
Ale to jest religia przed religią, jaką znamy.

229
00:21:59,960 --> 00:22:02,720
Gdzie jest odniesienie do książek.

230
00:22:03,080 --> 00:22:06,280
Książki przychodzą po religii, nie wcześniej.

231
00:22:06,720 --> 00:22:11,940
Wcześniej nawet nie mógł mówić.

232
00:22:12,360 --> 00:22:15,060
Więc był już w tej postawie.

233
00:22:15,480 --> 00:22:19,620
To są prehistoryczne postawy.

234
00:22:21,460 --> 00:22:23,740
(Ile czasu mi zostało?)

235
00:22:23,960 --> 00:22:28,320
(Zostały mi tylko 3 minuty? Dobra, więc!)

236
00:22:31,020 --> 00:22:34,460
Więc, jeśli chodzi o sofrologię…

237
00:22:36,500 --> 00:22:39,280
Chciałem porozmawiać z tobą o tradycyjnym zazen.

238
00:22:39,740 --> 00:22:43,660
Jestem ubrany jak tradycyjny mnich.

239
00:22:47,440 --> 00:22:51,700
Tradycyjny zazen nie jest praktykowany sam.

240
00:22:52,100 --> 00:22:54,500
To nie jest technika wellness.

241
00:22:58,260 --> 00:23:05,060
Przeczytam ci, co powiedział Budda, to takie wspaniałe!

242
00:23:08,520 --> 00:23:12,780
Budda Shakyamuni powiedział do swojej publiczności:

243
00:23:13,120 --> 00:23:18,540
"Studium Sutr i Nauk" - teksty -

244
00:23:18,900 --> 00:23:28,260
Przestrzeganie nakazów" - w religiach jest to ważne, aby nie zabijać, nie kraść, itp. -

245
00:23:29,300 --> 00:23:34,960
"a nawet praktykę zazen, siedzącą medytację."

246
00:23:35,900 --> 00:23:43,060
"są niezdolni do gaszenia ludzkich pragnień, lęków i niepokojów. »

247
00:23:43,340 --> 00:23:45,080
Tak powiedział Budda Shakyamuni.

248
00:23:45,340 --> 00:23:51,660
Więc jestem fanatykiem, a on jest wrzodem na dupie…

249
00:23:52,080 --> 00:23:56,800
Zazen, nawet Budda mówi, że to jest bezużyteczne!

250
00:23:57,600 --> 00:24:01,680
Więc, co z tym zrobimy?

251
00:24:14,440 --> 00:24:17,480
Oczywiście, jest tam apartament…

252
00:24:23,360 --> 00:24:29,360
Mój pan [Deshimaru], kiedy poznał swojego pana [Kodo Sawaki],

253
00:24:29,820 --> 00:24:34,280
Ten ostatni powiedział na konferencji: "Zazen jest bezużyteczny, nie przynosi żadnych korzyści! ».

254
00:24:34,680 --> 00:24:38,800
Mój mistrz pomyślał: "Ale to jest naprawdę interesujące, jeśli jest bezużyteczne! »

255
00:24:39,140 --> 00:24:41,940
"Zawsze robimy rzeczy, które są do czegoś dobre! »

256
00:24:42,280 --> 00:24:43,760
"Coś, co jest bezużyteczne? »

257
00:24:44,120 --> 00:24:47,400
"Chcę iść, chcę to zrobić! »

