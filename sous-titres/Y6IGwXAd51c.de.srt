1
00:00:00,000 --> 00:00:02,240
Ich bin ein Zen-Mönch. 

2
00:00:04,540 --> 00:00:09,960
Ich bin auch ein Kalligraph. 

3
00:00:10,980 --> 00:00:18,660
Die chinesische Kalligraphie wird von Menschen geschätzt, die Zazen praktizieren. 

4
00:00:19,280 --> 00:00:24,980
Es ist nicht einfach, über Zen zu sprechen. 

5
00:00:25,400 --> 00:00:28,820
Um es auszudrücken, um es zu manifestieren. 

6
00:00:29,240 --> 00:00:34,480
Durch die Kalligraphie sehen wir den Geist einer Person, die Zazen macht. 

7
00:00:35,000 --> 00:00:39,100
Wer übt die erwachende Haltung. 

8
00:00:40,020 --> 00:00:46,260
Meister Deshimaru erklärt, dass Kalligraphie nicht nur eine Technik ist. 

9
00:00:46,980 --> 00:00:52,700
Wir kalligraphieren nicht nur nach Techniken, Eigenschaften … 

10
00:00:53,360 --> 00:00:58,400
Wir Kalligraphie mit dem Verstand, mit dem Schienbein. 

11
00:00:58,880 --> 00:01:09,440
Mit dem Geist, mit dem ganzen Körper und Geist in einem. 

12
00:01:10,720 --> 00:01:18,400
Kalligraphie druckt das auf Papier. Es ist eine Präsenz. 

13
00:01:18,960 --> 00:01:24,320
Es ist in der Kalligraphie großer Meister wie Deshimaru und Niwa Zenji zu spüren. 

14
00:01:25,760 --> 00:01:30,000
Der Wert der Kalligraphie liegt nicht nur in der Form. 

15
00:01:31,440 --> 00:01:36,560
Aber auch im Schwung, in der Präsenz, in der Energie. 

16
00:01:37,280 --> 00:01:46,240
In der Aktivität, die entsteht …. 

17
00:01:47,040 --> 00:01:52,080
Und das fühlen und wahrnehmen. 

18
00:01:52,500 --> 00:01:55,800
Wir gehen von einem Moment aus, der von Unbeweglichkeit herrührt. 

19
00:01:56,950 --> 00:01:58,853
Zen, ruhig, still, Stille. 

20
00:02:00,160 --> 00:02:06,093
Um nichts am Laufen zu halten. 

21
00:02:06,100 --> 00:02:07,910
Gedanken vergehen lassen. 

22
00:02:07,950 --> 00:02:10,750
Nicht monopolisiert werden. 

23
00:02:10,840 --> 00:02:15,040
In einer Haltung fokussiert sein. 

24
00:02:15,920 --> 00:02:21,329
Gekreuzte Beine. 

25
00:02:21,329 --> 00:02:26,880
Das Kissen ermöglicht es, das Becken nach vorne zu kippen. 

26
00:02:26,880 --> 00:02:31,280
Den gesamten Brustkorb reinigen. 

27
00:02:32,240 --> 00:02:37,040
In dieser Pose bringen wir unsere Hände zurück. 

28
00:02:38,160 --> 00:02:43,680
Es ermöglicht uns, uns zu vereinen und zu zentrieren. 

29
00:02:44,400 --> 00:02:51,040
Es ist die Haltung der Zen-Meditation, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
Daraus und aus dem Impuls der Aktivität wird die Kalligraphie hervorgehen. 

31
00:02:58,480 --> 00:03:05,200
Nicht aus Komplikationen, aus dem Wunsch, Gutes zu tun, aus dem Wunsch, Gutes zu tun, aus dem Wunsch, gutes Wetter zu tun … 

32
00:03:06,240 --> 00:03:11,520
Nur dieser Schwung des Lebens. 

33
00:03:12,560 --> 00:03:22,160
Kalligraphie wird aus diesem Zen-Impuls, diesem Impuls, diesem Impuls hervorgehen. 

34
00:03:23,280 --> 00:03:33,200
Darum geht es in der Zen-Kalligraphie. 

35
00:03:34,720 --> 00:03:59,600
In Anbetracht der Zivilisationsphase, die China während der Tang-Dynastie erreicht hatte … 

36
00:04:01,120 --> 00:04:15,520
Im 7., 8. und 9. Jahrhundert waren die umliegenden Länder Japan, Korea und Vietnam von dieser Kultur und der Sprache, den chinesischen Schriftzeichen, angezogen. 

37
00:04:16,400 --> 00:04:27,200
Die chinesische Schrift hat sich also in diesen Ländern verbreitet. 

38
00:04:28,080 --> 00:04:34,480
Aber jeder hat es nach seinem eigenen Gefühl, seiner eigenen Sprache aufgenommen. 

39
00:04:35,280 --> 00:04:44,560
Jeder von ihnen hat seine eigene Art der Kalligraphie entwickelt. 

40
00:04:45,520 --> 00:04:56,080
Sie führten auch Lehrpläne ein, Zeichen in der Nähe eines Alphabets, gemischt mit chinesischen Zeichen. 

41
00:04:58,320 --> 00:05:04,480
Die japanische Kalligraphie unterscheidet sich geringfügig von der chinesischen Kalligraphie. 

42
00:05:05,680 --> 00:05:13,440
Alle verwenden Pinsel, zeichnen chinesische Schriftzeichen, verwenden aber auch Silbensysteme. 

43
00:05:15,040 --> 00:05:23,280
Die Vietnamesen, die zuvor von China beeinflusst wurden, waren noch kreativer. 

44
00:05:24,800 --> 00:05:31,600
Sie erfanden ihr eigenes ideographisches System, um ihre Sprache auf das chinesische Modell zu übertragen. 

45
00:05:33,440 --> 00:05:38,400
Es steht geschrieben: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
Es ist das Sutra des Geistes der großen Weisheit, das uns vorwärts bringt. 

48
00:05:59,840 --> 00:06:06,160
Dies ist die universelle Lehre des Buddha, die für alle buddhistischen Schulen spezifisch ist. 

49
00:06:07,360 --> 00:06:16,800
Es wird in altem Chinesisch, altem Japanisch und in phonetischer Transkription aus dem Sanskrit gesungen. 

50
00:06:18,080 --> 00:06:24,000
Das macht es zu einem universellen Text, weil niemand ihn in seiner eigenen Sprache versteht oder liest. 

51
00:06:25,360 --> 00:06:39,120
Die Japaner, die Chinesen, die Indianer … Wir sprechen es selbst mit einigen geringfügigen Anpassungen aus … 

52
00:06:40,980 --> 00:06:47,460
Es ist etwas, das jedem und niemandem gehört. 

53
00:06:48,220 --> 00:06:53,440
Dies ist der Satz, den ich hier in einem Schreibstil namens Kursiv geschrieben habe. 

54
00:06:53,680 --> 00:06:58,520
Das Wörterbuch wird nicht geschrieben. 

55
00:06:58,560 --> 00:07:03,680
Es ist kein reguläres Schreiben. 

56
00:07:05,740 --> 00:07:09,100
Es wurde für schnelles Schreiben entwickelt. 

57
00:07:09,480 --> 00:07:15,960
Eine Art Kurzschrift. 

58
00:07:16,240 --> 00:07:28,560
Wir behalten die Hauptlinien des Charakters bei, indem wir durch Krümmungen, durch Punkte, an denen es eine Linie gab, vereinfachen … 

59
00:07:29,600 --> 00:07:39,440
Der Charakter wurde bis zum Äußersten vereinfacht, er nimmt eine andere Form an als seine ursprüngliche Form. 

60
00:07:39,760 --> 00:07:47,680
Wir bewegen uns in Richtung einer Abstraktion und bewahren die Essenz der ursprünglichen Form. 

61
00:07:48,800 --> 00:07:55,920
Sie können damit schneller schreiben. 

62
00:07:58,080 --> 00:08:05,240
In der Handlung finden wir diesen Impuls, diesen Impuls, diesen Impuls von Zazen. 

63
00:08:05,640 --> 00:08:13,280
Weil wir den ganzen Charakter miteinander verbinden. 

64
00:08:13,660 --> 00:08:18,479
Sofort. 

65
00:08:18,479 --> 00:08:23,470
Wir können die Charaktere sogar miteinander verbinden. 

66
00:08:24,000 --> 00:08:31,840
Wir verknüpfen jedes der Zeichen, jede der Eigenschaften im Zeichen. 

67
00:08:33,120 --> 00:08:38,520
Wenn mehrere Zeichen gezeichnet werden, können sie miteinander verknüpft werden. 

68
00:08:38,940 --> 00:08:44,960
Wir tun es auf den Atem, auf den Impuls. 

69
00:08:45,400 --> 00:08:51,540
Im Moment die Energie, die Dynamik, die Aktivität des ganzen Körpers, der ganze Geist in einem. 

70
00:08:51,540 --> 00:08:55,200
In einer Bewegung von Anfang bis Ende. 

71
00:08:55,200 --> 00:08:59,200
Wir können nicht zurückgehen, wir können es nicht aktualisieren. 

72
00:08:59,200 --> 00:09:02,900
Wir können es nicht korrigieren. 

73
00:09:03,000 --> 00:09:05,120
Wir machen es mit Gewissen. 

74
00:09:05,920 --> 00:09:11,600
Wir lassen uns auch mit Freude gehen. 

75
00:09:12,060 --> 00:09:18,220
Aber wir können nichts korrigieren. 

76
00:09:18,560 --> 00:09:22,160
Wenn es verfolgt wird, wird es verfolgt. 

77
00:09:22,200 --> 00:09:25,529
Es ist das Hier und Jetzt. 

78
00:09:25,529 --> 00:09:29,000
Es ist etwas, das niemals zurückkommen wird. 

79
00:09:29,100 --> 00:09:33,800
Das existiert im Moment für immer und ewig. 

80
00:09:33,900 --> 00:09:36,200
Das ist auch Zen. 

81
00:09:36,250 --> 00:09:39,640
Jedes Mal ist jeder Zazen-Atemzug einzigartig. 

82
00:09:42,520 --> 00:09:44,640
Sie kommt nicht zurück. 

83
00:09:45,340 --> 00:09:52,540
Es ist ein Punkt, ein Bindestrich der Haltung. 

84
00:09:52,600 --> 00:09:58,320
In der Stimmung, Atmung und totale Aktivität. 

85
00:09:58,420 --> 00:10:03,160
Es ist eine Zeit des Lebens, des Bewusstseins, absolut, universell. 

86
00:10:03,720 --> 00:10:06,760
Wer kommt nicht zurück? 

87
00:10:07,120 --> 00:10:11,000
Es ist sehr wichtig in der Kalligraphie. 

88
00:10:11,000 --> 00:10:17,500
Wenn Sie Kalligraphie wie ein Roboter betreiben, ohne all Ihre Gedanken darauf zu richten, ist dies eine Technik. 

89
00:10:17,540 --> 00:10:18,920
Zazen ist das gleiche. 

90
00:10:19,000 --> 00:10:25,880
Wenn Sie Zazen als eine Technik des Wohlbefindens praktizieren, ist es nicht die Dimension des Zen. 

