1
00:00:38,480 --> 00:00:40,340
La sesión comenzará 

2
00:00:42,060 --> 00:00:45,680
antes de sentarse, oriente bien su cámara. 

3
00:00:48,700 --> 00:00:54,020
No debes hablar, este no es el momento, hacemos zazen en silencio. 

4
00:01:00,860 --> 00:01:05,320
Sientese. 

5
00:01:05,320 --> 00:01:10,240
Empuja la tierra con las raíces 

6
00:01:10,240 --> 00:01:18,780
y al mismo tiempo la columna se estira hacia el cielo. 

7
00:01:21,600 --> 00:01:24,320
La respiración es nasal. 

8
00:01:24,320 --> 00:01:27,560
respiras por las fosas nasales 

9
00:01:27,560 --> 00:01:29,540
para aquellos que tienen 

10
00:01:29,540 --> 00:01:34,820
y exhalas profundamente 

11
00:01:34,820 --> 00:01:38,460
y ahí es donde sueltas la tensión 

12
00:01:45,340 --> 00:01:50,960
Zana, estás respirando demasiado fuerte, tu cuerpo se mueve demasiado. 

13
00:01:50,960 --> 00:01:56,810
Pensé que tenías que subir y bajar. 

14
00:01:56,810 --> 00:02:00,240
Sobre todo, cállate Zana. Silencio! 

15
00:02:04,360 --> 00:02:06,840
Tienes buenas posturas. 

16
00:02:11,880 --> 00:02:19,940
Incluso si es un dojo virtual, debes peinarte un mínimo antes de venir, 

17
00:02:19,940 --> 00:02:23,940
sin nombrar a nadie. El Broco, por ejemplo. 

18
00:02:24,740 --> 00:02:29,460
¿Te peinaste con un petardo esta mañana? 

19
00:02:30,720 --> 00:02:33,220
También es necesario estilizar las raíces. 

20
00:02:36,000 --> 00:02:39,080
Hay posturas muy hermosas. 

21
00:02:42,740 --> 00:02:44,380
Sin miedo 

22
00:02:45,100 --> 00:02:51,660
En estos tiempos difíciles, no te dejes absorber desde el exterior. 

23
00:02:52,160 --> 00:02:57,940
Reenfoque, sea su propio maestro, 

24
00:02:59,480 --> 00:03:04,940
y como dijo Ronaldo: 

25
00:03:38,340 --> 00:03:41,960
No te dejes engañar por tus emociones, 

26
00:03:44,640 --> 00:03:48,620
viendo esto, ¿cómo te sientes? 

27
00:03:55,480 --> 00:03:57,640
¿Y viendo esto? 

28
00:04:07,160 --> 00:04:10,300
No busques alegría 

29
00:04:12,040 --> 00:04:14,960
no huyas del miedo 

30
00:04:17,460 --> 00:04:20,620
Porque como dijo Maître Deshimaru: 

31
00:04:22,380 --> 00:04:26,060
"Incluso si te gustan las flores, se marchitan. 

32
00:04:27,640 --> 00:04:32,780
Incluso si no te gustan las malezas, crecen. 

33
00:04:35,840 --> 00:04:37,940
Kaijo! 

34
00:04:49,200 --> 00:04:51,380
Si! Si! 

35
00:04:55,600 --> 00:04:58,700
¿Aún no hemos comenzado? Podemos movernos? 

36
00:04:58,700 --> 00:05:02,640
Comenzaremos de nuevo con Radis y Rose, que está abajo. 

37
00:05:03,780 --> 00:05:09,160
No entiendo, ¿debo detener la cámara? ¿Qué hay que hacer? ¿Ambos al mismo tiempo? 

38
00:05:10,820 --> 00:05:15,340
Quien hablo Es Muriel, uh no, Rose. 

39
00:05:15,840 --> 00:05:24,640
Gabi, cuida tu zanahoria porque si te mueves demasiado, te dan ganas de vomitar. 

