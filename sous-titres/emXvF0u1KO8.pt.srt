1
00:00:13,360 --> 00:00:18,820
Um poema de Shodoka, o poema desta manhã do Mestre Yoka Daishi:

2
00:00:19,740 --> 00:00:29,120
"Se alguém me pergunta a que religião eu pertenço, eu digo o poder de Maka Hannya".

3
00:00:30,800 --> 00:00:35,200
Estou lendo um trecho de um comentário do Mestre Deshimaru "Maka Hannya, isso significa a mais alta sabedoria,

4
00:00:35,640 --> 00:00:39,160
o mais profundo, o universal.

5
00:00:39,740 --> 00:00:42,000
Qual é o seu poder, qual é o seu poder?

6
00:00:42,340 --> 00:00:45,900
Todos têm poder, mas em

7
00:00:46,140 --> 00:00:51,300
O Hannya Shingyo, no Sutra da Grande Sabedoria, o Bodhisattva Avalokiteshvara

8
00:00:51,820 --> 00:00:55,540
...tem o poder supremo,
Maka Hannya’s.

9
00:00:56,560 --> 00:01:02,600
Ele entendeu que seu corpo e todas as coisas são ku, sem nenhuma substância limpa,

10
00:01:03,420 --> 00:01:07,980
ele pode ajudar todos os seres
e libertá-los de sua ansiedade".

11
00:01:08,300 --> 00:01:14,440
O início do sutra Hannya Shingyo está relacionado a este poema. Eu também vou ler

12
00:01:15,340 --> 00:01:19,260
um trecho de um dos ensinamentos do Mestre 
Kosen. Ele ensinava" meu Mestre costumava dizer

13
00:01:19,780 --> 00:01:25,460
sempre, não importa qual seja a situação,
difícil ou fácil, o melhor para o ser humano

14
00:01:26,440 --> 00:01:30,920
é continuar, perseverar para
focar na prática do zazen,

15
00:01:31,380 --> 00:01:37,960
desenvolver sua natureza de Buda". O Mestre Kosen diz que "é mais importante do que tudo".

16
00:01:39,200 --> 00:01:43,900
Sempre que há uma crise, um
revolução, uma guerra é sempre

17
00:01:44,780 --> 00:01:49,020
possível continuar a prática, para
para respirar, para se conectar com o cosmos.

18
00:01:49,620 --> 00:01:55,480
com força, com sabedoria
fundamental. Diz-se que o lótus que é

19
00:01:56,100 --> 00:02:00,740
nascido do fogo nunca serão destruído".
