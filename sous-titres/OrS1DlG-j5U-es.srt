1
00:00:00,000 --> 00:00:08,750
Hago zazen con un kesa de Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
Seda.

3
00:00:15,330 --> 00:00:19,215
Estoy muy contento.

4
00:00:20,235 --> 00:00:22,830
Me gusta mucho Barbara.

5
00:00:24,580 --> 00:00:27,210
Es una gran santa.

6
00:00:33,770 --> 00:00:38,190
Afortunadamente, mis discípulos en general son mejores que yo.

7
00:00:42,910 --> 00:00:45,830
Especialmente Bárbara.

8
00:00:48,850 --> 00:00:51,270
Y Ariadna también.

9
00:00:55,710 --> 00:00:58,515
Los tres tesoros.

10
00:01:01,015 --> 00:01:03,817
Buda, dharma, sangha.

11
00:01:06,267 --> 00:01:10,440
son inseparables.

12
00:01:24,820 --> 00:01:28,470
Te he preparado como un kusen…

13
00:01:35,340 --> 00:01:41,560
…la continuación de lo que hice en el campamento de verano.

14
00:01:45,170 --> 00:01:50,930
Sobre los cuatro fundamentos de los poderes mágicos.

15
00:01:56,750 --> 00:02:01,770
Hice esto en el campamento de verano…

16
00:02:02,130 --> 00:02:10,530
Dicen que los cuatro cimientos son como los cuatro cascos de un caballo.

17
00:02:35,940 --> 00:02:40,120
La primera es la fuerza de voluntad, la volición.

18
00:02:44,460 --> 00:02:50,300
La voluntad es algo más fuerte que tú.

19
00:02:52,290 --> 00:02:55,400
Y eso te empuja a actuar.

20
00:02:58,920 --> 00:03:02,400
Es como una cuestión de supervivencia.

21
00:03:06,430 --> 00:03:08,350
Es muy fuerte.

22
00:03:09,410 --> 00:03:12,160
Hablé de ello en el campamento de verano.

23
00:03:15,530 --> 00:03:18,550
El segundo es el espíritu.

24
00:03:20,745 --> 00:03:23,015
La segunda pezuña.

25
00:03:27,730 --> 00:03:33,390
El tercero es el progreso hacia adelante, el avance.

26
00:03:38,440 --> 00:03:40,810
El cuarto es el pensamiento.

27
00:03:43,810 --> 00:03:46,855
Eso es lo que estoy pensando.

28
00:03:48,135 --> 00:03:50,020
El segundo.

29
00:03:54,970 --> 00:03:57,515
Hablé de ello en el campamento de verano,

30
00:03:57,625 --> 00:04:00,860
pero no había terminado.

31
00:04:03,270 --> 00:04:09,595
Te voy a contar tres historias Zen…

32
00:04:11,995 --> 00:04:16,060
…que hacen que se entienda mejor la naturaleza de la mente.

33
00:04:19,990 --> 00:04:25,860
El Maestro Eka, un discípulo de Bodhidharma…

34
00:04:32,370 --> 00:04:35,760
…parado en la nieve…

35
00:04:39,330 --> 00:04:41,145
…helado…

36
00:04:44,635 --> 00:04:47,564
…se dirigió a su maestro!

37
00:04:53,470 --> 00:04:57,240
"Maestro, mi mente no está en paz. »

38
00:05:08,460 --> 00:05:12,130
"¡Te lo ruego, dale la paz! »

39
00:05:18,940 --> 00:05:25,380
Eka había matado a muchos hombres porque había estado en el ejército.

40
00:05:33,130 --> 00:05:37,370
Se sentía muy culpable por todo esto.

41
00:05:40,780 --> 00:05:44,100
No pudo deshacerse de la culpa.

42
00:05:49,410 --> 00:05:54,485
Está en la nieve y le pide a Bodhidharma: "¡Acéptame! »

43
00:06:00,245 --> 00:06:02,050
"¡Como un discípulo! »

44
00:06:06,000 --> 00:06:09,410
Bodhidharma no responde.

45
00:06:17,770 --> 00:06:19,615
Insiste.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma no se mueve.

47
00:06:30,960 --> 00:06:35,420
Al final, se corta el brazo con la espada.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma dice, "Bueno…"

49
00:06:58,695 --> 00:07:02,350
Bodhidharma gruñe: "Han…"

50
00:07:06,620 --> 00:07:09,980
Y continúa con el zazen.

51
00:07:14,280 --> 00:07:17,145
Eka insiste, ruega:

52
00:07:20,910 --> 00:07:24,630
"¡Te ruego que me tranquilices! »

53
00:07:29,190 --> 00:07:31,335
"¡Es insoportable! »

54
00:07:32,305 --> 00:07:34,400
"¡Me estoy volviendo loco! »

55
00:07:44,290 --> 00:07:48,140
Finalmente, Bodhidharma le da una mirada:

56
00:07:54,430 --> 00:07:58,595
"Tráeme tu espíritu. »

57
00:08:10,745 --> 00:08:13,680
"Y le daré la paz. »

58
00:08:20,300 --> 00:08:22,590
El Maestro Eka le respondió:

59
00:08:26,210 --> 00:08:29,105
"¡Maestro, lo busqué, mi espíritu! »

60
00:08:33,665 --> 00:08:37,750
"Y permaneció inaccesible, escurridizo! »

61
00:08:46,240 --> 00:08:49,020
Eka está buscando tranquilidad.

62
00:08:52,630 --> 00:08:55,050
Está sufriendo.

63
00:08:56,520 --> 00:08:59,340
Quiere paz, tranquilidad.

64
00:09:04,085 --> 00:09:05,490
No importa cuántas veces se lo digamos:

65
00:09:05,550 --> 00:09:07,845
"¡Es tu mente! »

66
00:09:13,950 --> 00:09:16,360
"¿Dónde está, mi mente? »

67
00:09:17,805 --> 00:09:19,655
"¿Dónde está, mi espíritu? »

68
00:09:22,452 --> 00:09:25,182
"¿Dónde está la raíz de este espíritu? »

69
00:09:29,400 --> 00:09:32,480
"¿Cuál es la esencia de este espíritu? »

70
00:09:35,925 --> 00:09:38,528
"¿Está en el cerebro? »

71
00:09:46,670 --> 00:09:48,510
"¿Está en el corazón? »

72
00:09:56,655 --> 00:10:00,025
Aunque no todos se corten el brazo,

73
00:10:03,270 --> 00:10:07,120
en la nieve, en el frío…

74
00:10:08,810 --> 00:10:12,880
Todo el mundo quiere encontrar algo en su mente.

75
00:10:16,040 --> 00:10:18,570
Todo el mundo quiere ser pacífico.

76
00:10:21,970 --> 00:10:23,691
Pacifica su mente.

77
00:10:27,930 --> 00:10:31,515
Eka tuvo una respuesta notable cuando dijo:

78
00:10:31,645 --> 00:10:35,920
"He estado buscándolo, pero es escurridizo! »

79
00:10:43,990 --> 00:10:46,030
"No pude encontrarlo. »

80
00:10:48,615 --> 00:10:53,235
En japonés, se llama "Shin fukatoku".

81
00:11:07,460 --> 00:11:09,885
"Fu" es la negación, "nada".

82
00:11:13,960 --> 00:11:15,870
"Ta": Aprovechar.

83
00:11:19,270 --> 00:11:21,100
Aprovechar, encontrar.

84
00:11:24,230 --> 00:11:28,260
"Toku" es como "Mushotoku".

85
00:11:29,550 --> 00:11:33,470
"Toku" significa "conseguir".

86
00:11:35,910 --> 00:11:37,930
"Alcanzando su meta".

87
00:11:40,370 --> 00:11:43,320
"Shin fukatoku",

88
00:11:47,710 --> 00:11:51,310
Es la mente completamente irrastreable y escurridiza.

89
00:11:57,710 --> 00:11:58,955
Eka dice:

90
00:11:59,085 --> 00:12:01,650
"Lo busqué, pero no lo encontré. »

91
00:12:04,480 --> 00:12:06,670
Es una respuesta excelente.

92
00:12:09,470 --> 00:12:13,340
Pero la de Bodhidharma es aún más hermosa.

93
00:12:18,060 --> 00:12:22,330
"Tráeme tu espíritu y lo apaciguaré. »

94
00:12:29,700 --> 00:12:30,660
Eka dijo:

95
00:12:30,720 --> 00:12:32,670
"No puedo". »

96
00:12:34,760 --> 00:12:37,320
"No sólo no puedo atraparlo,"

97
00:12:37,400 --> 00:12:40,320
"¡pero no puedo ni encontrarlo! »

98
00:12:44,610 --> 00:12:46,270
Bodhidharma le dice:

99
00:12:48,910 --> 00:12:52,340
"En ese caso, ¡ya está pacificado! »

100
00:13:07,610 --> 00:13:09,520
Historia dos:

101
00:13:11,955 --> 00:13:14,575
Me gustan las historias.

102
00:13:16,890 --> 00:13:18,750
Maestro Obaku.

103
00:13:20,240 --> 00:13:22,330
El maestro de Baso.

104
00:13:25,480 --> 00:13:30,240
Dar su enseñanza, delante de los monjes.

105
00:13:33,405 --> 00:13:35,605
Alguien lo llama:

106
00:13:41,550 --> 00:13:46,580
"El maestro debe tener una gran facultad de concentración. »

107
00:13:52,200 --> 00:13:53,985
El maestro Obaku responde:

108
00:13:58,735 --> 00:14:02,110
"Concentración".

109
00:14:03,950 --> 00:14:07,594
"…o por el contrario, distracción,"

110
00:14:10,310 --> 00:14:14,610
"…¿estás diciendo que estas cosas existen? »

111
00:14:18,300 --> 00:14:20,600
"Te digo:"

112
00:14:23,770 --> 00:14:27,100
"Búscalos, y no los encontrarás en ninguna parte. »

113
00:14:32,840 --> 00:14:36,340
"Pero si me dices que no existen,"

114
00:14:40,390 --> 00:14:42,490
"Te diré:"

115
00:14:45,510 --> 00:14:47,390
"lo que sea que pienses,"

116
00:14:53,490 --> 00:14:58,050
"¡Estás constantemente presente donde estás! »

117
00:15:08,040 --> 00:15:11,650
Incluso cuando crees que estás distraído,

118
00:15:16,330 --> 00:15:20,610
mira el lugar donde se originó la distracción.

119
00:15:24,850 --> 00:15:27,950
Verás que finalmente no tiene ningún origen.

120
00:15:33,360 --> 00:15:37,260
El estado mental de la distracción que aparece…

121
00:15:40,870 --> 00:15:43,490
…no va a ninguna parte.

122
00:15:44,920 --> 00:15:46,695
En las diez direcciones.

123
00:15:48,305 --> 00:15:50,690
Y tampoco va a ir a ningún otro sitio.

124
00:16:10,270 --> 00:16:13,490
En zazen, no hay distracción ni concentración.

125
00:16:21,040 --> 00:16:23,470
Sólo quédate quieto.

126
00:16:30,800 --> 00:16:33,090
El resto son técnicas.

127
00:16:35,820 --> 00:16:40,168
Nos aleja del auténtico zazen.

128
00:16:45,129 --> 00:16:46,769
Cuento tres.

129
00:16:47,119 --> 00:16:49,719
- ¡Sí, te he malcriado! -

130
00:17:00,600 --> 00:17:03,750
Es de la época de Baso en China.

131
00:17:05,760 --> 00:17:18,250
Baso, este es el discípulo del maestro del que hablaba, Obaku.

132
00:17:24,410 --> 00:17:28,330
En esa época vivía un erudito

133
00:17:30,950 --> 00:17:34,280
…cuyo nombre era Ryo.

134
00:17:36,560 --> 00:17:41,090
Era famoso por su conocimiento del budismo.

135
00:17:45,320 --> 00:17:48,840
Se pasó la vida dando conferencias.

136
00:17:53,410 --> 00:17:58,790
Resulta que un día tuvo una entrevista con Baso.

137
00:18:04,890 --> 00:18:07,160
Baso le preguntó:

138
00:18:09,190 --> 00:18:12,770
- ¿Sobre qué temas das conferencias? »

139
00:18:15,850 --> 00:18:20,040
- "En el Sutra de los Espíritus, el Hannya Shingyo. »

140
00:18:25,360 --> 00:18:28,220
También se llama el Sutra del Corazón.

141
00:18:32,420 --> 00:18:36,750
No está claro si la mente en el cerebro o el corazón.

142
00:18:41,320 --> 00:18:47,240
En "Shingyo", "Shin" significa mente o corazón.

143
00:18:52,600 --> 00:18:55,460
"Gyô" significa Sutra.

144
00:18:58,090 --> 00:19:00,285
Así que Baso le pregunta:

145
00:19:03,825 --> 00:19:07,260
- "¿Qué usas para tus conferencias? »

146
00:19:09,950 --> 00:19:11,755
Ryo respondió:

147
00:19:14,525 --> 00:19:17,527
- "Con el espíritu. »

148
00:19:19,530 --> 00:19:21,355
Baso le dice:

149
00:19:24,685 --> 00:19:30,210
- "El espíritu, es como un muy buen actor! »

150
00:19:34,480 --> 00:19:39,390
"El significado de su pensamiento es como la extravagancia de un bufón. »

151
00:19:45,690 --> 00:19:51,580
"En cuanto a los seis sentidos que nos hacen sentirlos como en una película en el cine…"

152
00:20:07,270 --> 00:20:10,420
"¡Estos seis sentidos están vacíos! »

153
00:20:12,420 --> 00:20:16,970
"Como una ola sin principio ni fin real. »

154
00:20:21,260 --> 00:20:26,340
"¿Cómo podría la mente dar una conferencia sobre un sutra?"

155
00:20:33,590 --> 00:20:38,200
Ryo respondió, pensando que era inteligente:

156
00:20:42,150 --> 00:20:47,880
- "Si el espíritu no puede hacerlo, tal vez la no mente pueda… »

157
00:20:51,630 --> 00:20:53,315
Baso le dice:

158
00:20:55,055 --> 00:20:58,607
- "¡Sí, exactamente! »

159
00:21:00,167 --> 00:21:04,150
"El no-espíritu puede dar una conferencia. »

160
00:21:09,050 --> 00:21:13,230
Ryo, creyendo estar satisfecho con su respuesta…

161
00:21:16,350 --> 00:21:19,730
…le quitó el polvo de las mangas…

162
00:21:25,070 --> 00:21:28,100
…y se preparó para salir.

163
00:21:31,390 --> 00:21:33,820
El maestro Baso lo llama:

164
00:21:35,920 --> 00:21:38,605
- "¡Profesor! »

165
00:21:39,305 --> 00:21:41,160
Ryo giró la cabeza.

166
00:21:47,460 --> 00:21:49,115
Baso le dice:

167
00:21:49,185 --> 00:21:51,560
- "¿Qué estás haciendo? »

168
00:21:55,630 --> 00:21:58,225
Ryo tuvo una profunda revelación.

169
00:22:03,905 --> 00:22:07,490
Se ha dado cuenta de su falta de espíritu.

170
00:22:10,940 --> 00:22:12,440
Y Baso le dice:

171
00:22:12,550 --> 00:22:16,000
"Desde el nacimiento hasta la muerte, es así. »

172
00:22:21,750 --> 00:22:24,976
Pensamos, hacemos cosas,

173
00:22:28,400 --> 00:22:30,670
espontáneamente.

174
00:22:33,320 --> 00:22:36,315
Ryo quería inclinarse ante Baso,

175
00:22:42,215 --> 00:22:44,840
pero éste le dice:

176
00:22:46,660 --> 00:22:51,220
"¡No hay necesidad de hacer tonterías! »

177
00:23:02,150 --> 00:23:07,280
Todo el cuerpo de Ryo estaba cubierto de sudor.

178
00:23:16,080 --> 00:23:19,360
Volvió a su templo.

179
00:23:21,260 --> 00:23:24,295
Y dijo a sus discípulos:

180
00:23:27,575 --> 00:23:31,645
"Estaba pensando que cuando muera, podríamos decir,"

181
00:23:37,670 --> 00:23:41,440
"Que había dado las mejores conferencias…"

182
00:23:44,650 --> 00:23:47,170
"…en el Zen. »

183
00:23:52,430 --> 00:23:55,830
"Hoy, le hice una pregunta a Baso. »

184
00:24:00,250 --> 00:24:03,710
"Y despejó la niebla de toda mi vida. »

185
00:24:10,790 --> 00:24:14,400
Ryo dejó de dar conferencias.

186
00:24:18,180 --> 00:24:21,125
Y se retiró al corazón de las montañas.

187
00:24:24,475 --> 00:24:28,290
Y no volvimos a saber nada de él.

188
00:25:15,750 --> 00:25:21,950
¿Por qué la mente es tan difícil de atrapar?

189
00:25:24,620 --> 00:25:25,755
¿Por qué?

190
00:25:35,090 --> 00:25:39,490
Porque el pensamiento, la conciencia…

191
00:25:44,230 --> 00:25:46,680
…e incluso la realidad…

192
00:26:00,830 --> 00:26:02,620
…a veces aparece…

193
00:26:06,380 --> 00:26:08,210
…a veces desaparece.

194
00:26:10,690 --> 00:26:16,570
A veces aparece, a veces desaparece…

195
00:26:26,840 --> 00:26:30,275
Cuando desaparece, no sabemos dónde está.

196
00:26:32,895 --> 00:26:35,532
Pero donde ella está…

197
00:26:37,512 --> 00:26:39,260
…ella está ahí.

198
00:26:42,620 --> 00:26:44,475
Ella está ahí.

199
00:26:53,205 --> 00:26:56,900
Donde no está, también está ahí.

200
00:27:04,820 --> 00:27:09,140
El tiempo y la conciencia no son lineales.

201
00:28:08,350 --> 00:28:10,574
En cuanto a la distracción…

202
00:28:19,220 --> 00:28:24,070
Cuando era niño, siempre me dijeron
que no estaba concentrado.

203
00:28:37,820 --> 00:28:39,365
Distraído.

204
00:28:42,805 --> 00:28:45,670
Pero cuando jugué…

205
00:28:47,257 --> 00:28:50,930
Cuando jugaba con mis pequeños coches…

206
00:28:55,110 --> 00:28:58,880
…estaba completamente concentrado.

207
00:29:06,020 --> 00:29:11,980
El Hannya Shingyo y la ceremonia están dedicados a Jonathan Duval,

208
00:29:20,210 --> 00:29:25,490
Y Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra de la gran sabiduría que permite ir más allá

210
00:30:12,312 --> 00:30:17,006
El Bodhisattva de la verdadera libertad,

211
00:30:17,196 --> 00:30:21,144
por la profunda práctica de la Gran Sabiduría,

212
00:30:21,264 --> 00:30:25,022
entiende que el cuerpo y los cinco skandah no son más que vacíos.

213
00:30:25,152 --> 00:30:30,151
y a través de ese entendimiento, ayuda a todos los que sufren.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, los fenómenos no son diferentes de ku.

215
00:30:34,970 --> 00:30:39,527
Ku no es diferente de los anormales.

216
00:30:39,987 --> 00:30:42,996
Los fenómenos se convierten en ku.

217
00:30:43,196 --> 00:30:51,305
Ku se convierte en un fenómeno (la forma es el vacío, el vacío es la forma),

218
00:30:51,575 --> 00:30:56,155
los cinco skanda también son un fenómeno.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, toda la existencia tiene el carácter de ku,

220
00:31:03,451 --> 00:31:08,429
no hay nacimiento ni comienzo,

221
00:31:08,989 --> 00:31:14,707
no hay pureza, no hay mancha, no hay crecimiento, no hay decadencia.

222
00:31:14,977 --> 00:31:20,130
Es por eso que en ku, no hay forma, no hay skanda,

223
00:31:20,939 --> 00:31:26,080
sin ojos, sin oídos, sin nariz, sin lengua, sin cuerpo, sin conciencia;

224
00:31:26,491 --> 00:31:32,397
no hay color, sonido, olor, sabor, tacto o pensamiento;

225
00:31:32,527 --> 00:31:37,915
no hay conocimiento, ni ignorancia, ni ilusión, ni cese del sufrimiento,

226
00:31:38,225 --> 00:31:43,283
no hay conocimiento, no hay beneficio, no hay ganancia.

227
00:31:43,413 --> 00:31:48,649
Para el Bodhisattva, gracias a esta Sabiduría que lleva más allá,

228
00:31:48,969 --> 00:31:54,622
no hay tal cosa como el miedo o el temor.

229
00:31:54,882 --> 00:32:02,024
Toda la ilusión y el apego se eliminan

230
00:32:02,504 --> 00:32:11,116
y puede aprovechar el fin último de la vida, el Nirvana.

231
00:32:12,325 --> 00:32:16,407
Todos los Budas del pasado, presente y futuro...

232
00:32:16,497 --> 00:32:21,489
...puede alcanzar la comprensión de esta Sabiduría Suprema...

233
00:32:21,569 --> 00:32:25,021
que entrega el sufrimiento, el Satori,

234
00:32:25,081 --> 00:32:30,329
por este encantamiento, incomparable e incomparable, auténtico,

235
00:32:30,432 --> 00:32:36,163
que elimina todo el sufrimiento y te permite encontrar la realidad, el verdadero ku:

236
00:32:38,113 --> 00:32:52,170
"Vayan, vayan, vayan juntos más allá del más allá en la orilla del satori. »

237
00:33:01,240 --> 00:33:14,420
No importa cuántos seres haya, juro que los salvaré a todos.

238
00:33:14,600 --> 00:33:23,095
No importa cuántas pasiones haya, juro que las derrotaré todas.

239
00:33:25,635 --> 00:33:34,292
No importa cuántos Dharmas haya, prometo adquirirlos todos.

240
00:33:35,112 --> 00:33:43,700
No importa cuán perfecto sea un Buda, juro que me convertiré en uno.

241
00:33:44,085 --> 00:33:48,247
Que los méritos de esta recitación

242
00:33:48,247 --> 00:33:52,768
penetren en todos los seres en todas partes,

243
00:33:52,768 --> 00:33:57,538
para que todos nosotros, seres sensibles,

244
00:33:57,538 --> 00:34:03,129
podamos realizar el Camino de Buda juntos.

245
00:34:05,332 --> 00:34:16,597
Todos los Budas pasados, presentes y futuros en las diez direcciones.

246
00:34:17,517 --> 00:34:26,629
Todos los Bodhisattvas y patriarcas

247
00:34:27,869 --> 00:34:38,142
El Sutra de la "Sabiduría que va más allá"

248
00:34:53,546 --> 00:34:58,216
¡Sanpaï!

249
00:37:25,590 --> 00:37:28,060
¡Uf!

250
00:37:46,750 --> 00:37:48,965
¡Hola a todos!

251
00:37:57,955 --> 00:38:03,310
¡Espero que esté feliz de que la pandemia haya terminado!

252
00:38:32,700 --> 00:38:34,600
¡Hola Paola!
