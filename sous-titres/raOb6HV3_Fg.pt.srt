1
00:00:00,000 --> 00:00:05,742
Meu mestre me ensinou quando eu era iniciante (eu ainda sou …). 

2
00:00:05,842 --> 00:00:07,832
"Zazen é satori." 

3
00:00:07,932 --> 00:00:18,497
Na época, na década de 1970, a palavra "satori" era muito interessante, queríamos saber o que era satori, uma espécie de iluminação. 

4
00:00:18,597 --> 00:00:22,731
As pessoas queriam fazer zazen para obter satori. 

5
00:00:22,831 --> 00:00:31,329
E o Sensei ensinou que era a atitude do próprio Buda, que não havia satori lá fora. 

6
00:00:31,429 --> 00:00:35,793
Esse é o ensinamento do mestre Deshimaru. Ele sempre dizia: 

7
00:00:35,893 --> 00:00:39,950
"Satori é o retorno às circunstâncias normais." 

8
00:00:40,050 --> 00:00:41,529
Nós, jovens, ficamos desapontados. 

9
00:00:41,629 --> 00:00:46,520
As condições normais são chatas. 

10
00:00:46,620 --> 00:00:49,388
Gostávamos de fumar maconha, tomamos LSD … 

11
00:00:49,490 --> 00:00:53,471
Não estávamos interessados ​​em circunstâncias normais. 

12
00:00:53,571 --> 00:00:58,853
Agora, em 2018, as condições normais estão em alta demanda. 

13
00:00:58,960 --> 00:01:02,400
Estações normais, clima normal, qualidade do ar normal. 

14
00:01:02,400 --> 00:01:04,400
Um oceano normal, fauna e flora normais. 

15
00:01:08,380 --> 00:01:10,980
A normalidade se torna cara. 

16
00:01:11,088 --> 00:01:13,155
Nunca esqueça estas palavras: 

17
00:01:13,255 --> 00:01:15,705
"O próprio Zazen é satori." 

18
00:01:15,805 --> 00:01:19,809
Satori é sobre estar no lugar certo, na hora certa. 

19
00:01:19,909 --> 00:01:21,516
Isso é muito difícil. 

20
00:01:21,616 --> 00:01:27,840
Se você adota a postura de Buda, está no lugar certo, na hora certa. 

21
00:01:27,940 --> 00:01:32,939
Você é normal. Você está sincronizando com a ordem cósmica. 

22
00:01:33,039 --> 00:01:34,264
É mecânico. 

23
00:01:34,364 --> 00:01:38,498
Com o esqueleto de uma pessoa anormal, um poluidor … 

24
00:01:38,598 --> 00:01:42,831
Se você adota a atitude de Buda, é mecânica. 

25
00:01:42,931 --> 00:01:47,854
Voltamos ao ritmo cósmico universal. Isso é satori. 

26
00:01:47,954 --> 00:01:50,503
Ao mesmo tempo, é muito simples. 

27
00:01:50,603 --> 00:01:59,507
Mas se você está tão longe das condições normais como agora, isso tem um valor tremendo. 

28
00:01:59,607 --> 00:02:01,521
E o que é Zen? 

29
00:02:01,621 --> 00:02:04,017
É preciso a posição exata. 

30
00:02:04,117 --> 00:02:07,945
É como um código para abrir uma porta secreta. 

31
00:02:08,045 --> 00:02:10,035
Você vê isso nos filmes. 

32
00:02:10,135 --> 00:02:16,359
Você tem que pressionar isso, você tem que colocar o feixe de luz no lugar certo. 

33
00:02:16,459 --> 00:02:17,990
E a porta se abre. 

34
00:02:18,090 --> 00:02:21,229
É o mesmo se você alinhar sua posição. 

35
00:02:25,280 --> 00:02:29,320
Você é linda nos pinos. No travesseiro, em lótus. 

36
00:02:29,720 --> 00:02:32,400
A posição do Buda é em lótus. 

37
00:02:32,860 --> 00:02:37,700
Se você não pode fazer o lótus, faça meio lótus. 

38
00:02:37,980 --> 00:02:40,580
Mas o lótus não tem nada a ver com isso. 

39
00:02:41,060 --> 00:02:44,120
Inverte as polaridades. 

40
00:02:44,760 --> 00:02:52,720
Assim que você pega o lótus, em vez de forçá-lo, em vez de machucá-lo, você o solta. 

41
00:02:53,480 --> 00:02:56,380
A atitude do Buda é precisa. 

42
00:02:56,940 --> 00:03:03,020
Se você vestir apenas as roupas certas. 

43
00:03:03,140 --> 00:03:08,240
Respire bem, cure seu corpo e mente. 

44
00:03:08,240 --> 00:03:14,360
É lindo e é muito simples. 

45
00:03:14,860 --> 00:03:17,840
Não é uma condição especial. 

46
00:03:18,520 --> 00:03:23,020
Você não precisa pensar: "Eu tenho mais satori do que os outros". 

47
00:03:23,400 --> 00:03:26,080
Isso é normal. 

48
00:03:26,600 --> 00:03:32,940
Se houver algo anormal no sistema solar, peide tudo. 

49
00:03:33,280 --> 00:03:38,720
Então, tudo está exatamente no lugar certo, na hora certa e em um movimento. 

50
00:03:38,920 --> 00:03:42,660
O segredo é claro: conhecemos o método. 

51
00:03:42,940 --> 00:03:45,700
Você deveria se dar bem. 

52
00:03:46,100 --> 00:03:48,460
É preciso prática. 

53
00:03:48,740 --> 00:03:52,100
É extraordinário ser capaz de falhar na ordem cósmica. 

54
00:03:52,300 --> 00:03:56,000
O Sensei repetia: "Siga a ordem cósmica". 

55
00:03:56,160 --> 00:03:58,080
Ele gritou comigo: 

56
00:03:58,320 --> 00:04:01,500
"StiFi, por que você não segue a ordem cósmica? Você deve seguir a ordem cósmica!" 

