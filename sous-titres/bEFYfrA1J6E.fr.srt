1
00:00:08,660 --> 00:00:11,260
Bonne journée à tous !

2
00:00:11,560 --> 00:00:19,400
Comment s'asseoir en méditation zen, zazen ?

3
00:00:19,800 --> 00:00:22,600
Je vais vous expliquer.

4
00:00:22,700 --> 00:00:27,880
La seule chose dont vous avez besoin,

5
00:00:28,340 --> 00:00:33,020
c'est d'être un être humain et d'avoir un coussin de méditation.

6
00:00:33,020 --> 00:00:36,680
Si vous avez un coussin de méditation, très bien.

7
00:00:37,100 --> 00:00:40,560
Sinon, vous pouvez en faire un avec une couverture.

8
00:00:43,060 --> 00:00:44,320


9
00:00:45,885 --> 00:00:48,855


10
00:00:49,620 --> 00:00:53,420


11
00:00:53,720 --> 00:00:57,580
La méditation zen est une méditation assise,

12
00:00:57,940 --> 00:01:00,820
immobile et en silence.

13
00:01:01,295 --> 00:01:04,425


14
00:01:04,425 --> 00:01:07,445
Nous nous asseyons devant le mur.

15
00:01:08,400 --> 00:01:11,040
Maintenant, je suis assis devant la caméra,

16
00:01:11,380 --> 00:01:15,900
mais pendant la méditation, vous vous asseyez devant le mur.

17
00:01:16,520 --> 00:01:21,940
Nous allons examiner trois aspects différents.

18
00:01:24,860 --> 00:01:27,060


19
00:01:27,300 --> 00:01:32,680
La méditation consiste à pratiquer ces trois aspects en même temps.

20
00:01:33,420 --> 00:01:36,955
Le premier aspect, c'est la posture physique.

21
00:01:36,955 --> 00:01:38,435
Avec le corps.

22
00:01:39,175 --> 00:01:40,625
Le deuxième aspect, c'est

23
00:01:41,020 --> 00:01:42,900
la respiration.

24
00:01:42,960 --> 00:01:45,580


25
00:01:46,300 --> 00:01:48,720
Le troisième aspect, c'est

26
00:01:48,880 --> 00:01:50,320
l'attitude mentale.

27
00:01:50,620 --> 00:01:56,180
Que faisons-nous pendant que nous sommes assis immobiles,

28
00:01:56,180 --> 00:01:59,080
et en silence devant le mur ?

29
00:02:00,040 --> 00:02:04,560


30
00:02:06,160 --> 00:02:09,600
La posture du corps.

31
00:02:09,635 --> 00:02:12,585
La posture concrète de la méditation zen.

32
00:02:12,585 --> 00:02:17,340
Vous êtes assis sur un coussin.

33
00:02:17,760 --> 00:02:21,440
Pas sur le bord du coussin, mais bien sur le dessus.

34
00:02:21,440 --> 00:02:24,640
Croisez les jambes.

35
00:02:24,640 --> 00:02:28,160
De manière à ce que vos genoux

36
00:02:28,220 --> 00:02:32,540
tombent ou poussent sur le sol.

37
00:02:32,540 --> 00:02:35,140


38
00:02:35,440 --> 00:02:37,220


39
00:02:37,840 --> 00:02:41,120
Prenez votre jambe, comme vous pouvez.

40
00:02:41,380 --> 00:02:44,020
Comme ça, ou

41
00:02:44,685 --> 00:02:47,435
si vous êtes souple, vous pouvez la placer ici.

42
00:02:47,435 --> 00:02:50,585
En demi lotus.

43
00:02:50,585 --> 00:02:54,980
Vous pouvez aussi mettre la partie supérieure de la jambe.

44
00:02:55,240 --> 00:02:59,040
Placez-la de manière à vous sentir le plus à l'aise possible.

45
00:02:59,260 --> 00:03:02,240
Car nous ne bougerons pas.

46
00:03:02,500 --> 00:03:07,120
Nous devons donc trouver une position confortable.

47
00:03:07,120 --> 00:03:10,600
Afin de ne pas bouger,

48
00:03:10,600 --> 00:03:13,940
et de garder le silence.

49
00:03:14,160 --> 00:03:17,460
Les genoux touchent le sol.

50
00:03:17,655 --> 00:03:19,655
Comment faire ?

51
00:03:19,660 --> 00:03:23,800
Le bassin s'incline un peu vers l'avant.

52
00:03:24,240 --> 00:03:27,415


53
00:03:27,420 --> 00:03:31,000
Si le bassin est positonné vers l'arrière,

54
00:03:32,020 --> 00:03:36,180
que je bombe le dos,

55
00:03:36,180 --> 00:03:39,020
ou comme quand je conduis une voiture,

56
00:03:39,020 --> 00:03:41,025


57
00:03:41,025 --> 00:03:43,860
mes genoux sont en l'air.

58
00:03:43,860 --> 00:03:47,060
Et je veux que mes genoux descendent.

59
00:03:47,060 --> 00:03:51,700
Dans le zen, on dit : "Poussez la terre avec les genoux."

60
00:03:51,900 --> 00:03:54,940
Et ce mouvement, avec la gravité,

61
00:03:55,040 --> 00:03:57,300
descend.

62
00:03:57,300 --> 00:04:03,120
Avec cette légère inclinaison du bassin,

63
00:04:03,460 --> 00:04:06,995
il n'y a pas d'effort musculaire.

64
00:04:06,995 --> 00:04:10,355


65
00:04:12,355 --> 00:04:15,315
Si vous êtes comme ça,

66
00:04:15,780 --> 00:04:19,160
le bassin est basculé vers l'avant.

67
00:04:20,220 --> 00:04:21,700
Basculez-le un peu

68
00:04:22,720 --> 00:04:23,780
jusque vers ici.

69
00:04:24,960 --> 00:04:27,040
Parfaitement droit.

70
00:04:27,040 --> 00:04:30,080
Si vous vous asseyez comme ça naturellement,

71
00:04:30,080 --> 00:04:31,740
le centre de gravité est

72
00:04:31,740 --> 00:04:34,875
plus vers l'avant.

73
00:04:34,875 --> 00:04:37,555
Et vos genoux vont toucher le sol.

74
00:04:39,555 --> 00:04:42,595
À partir de l'inclinaison du bassin,

75
00:04:42,945 --> 00:04:45,725
vous redressez votre colonne vertébrale.

76
00:04:46,165 --> 00:04:48,885
Autant que possible.

77
00:04:49,600 --> 00:04:52,100


78
00:04:52,100 --> 00:04:55,460
Poussez le ciel

79
00:04:55,460 --> 00:04:58,700
avec le sommet de la tête.

80
00:05:04,620 --> 00:05:06,680
Dans le zen, la colonne vertébrale

81
00:05:07,560 --> 00:05:10,720
commence ici.

82
00:05:10,720 --> 00:05:14,020
Si nous redressons notre colonne vertébrale,

83
00:05:14,040 --> 00:05:17,100
il faut essayer de l'étirer jusque-là.

84
00:05:17,380 --> 00:05:18,740
Nous n'allons donc pas

85
00:05:19,260 --> 00:05:21,400
méditer avec la tête

86
00:05:22,335 --> 00:05:25,525
vers l'avant, ni vers l'arrière.

87
00:05:26,015 --> 00:05:28,255
Le menton doit être

88
00:05:29,080 --> 00:05:31,240
le plus droit possible.

89
00:05:31,960 --> 00:05:35,920
On garde une ligne droite verticale

90
00:05:36,280 --> 00:05:38,320
entre les yeux,

91
00:05:39,285 --> 00:05:40,345
la bouche,

92
00:05:41,015 --> 00:05:42,905


93
00:05:43,305 --> 00:05:45,125


94
00:05:45,605 --> 00:05:46,695
et le nombril.

95
00:05:48,075 --> 00:05:49,075


96
00:05:49,880 --> 00:05:53,840


97
00:05:54,060 --> 00:05:56,365
La posture des mains.

98
00:05:56,365 --> 00:05:58,495
La main gauche,

99
00:05:59,765 --> 00:06:01,175
les doigts serrés,

100
00:06:01,575 --> 00:06:04,725
se place dans la main droite.

101
00:06:04,920 --> 00:06:06,380


102
00:06:06,600 --> 00:06:09,740
Les doigts sont superposés.

103
00:06:11,765 --> 00:06:13,975
Une ligne horizontale avec,

104
00:06:13,975 --> 00:06:17,115
les pouces, qui se touchent.

105
00:06:17,360 --> 00:06:21,280
Comme pour tenir une plume.

106
00:06:21,280 --> 00:06:22,480


107
00:06:22,885 --> 00:06:24,925
Sans pression musculaire.

108
00:06:25,915 --> 00:06:29,125
La paume des mains.

109
00:06:29,905 --> 00:06:33,295


110
00:06:33,675 --> 00:06:36,545
Plus ou moins au milieu du ventre.

111
00:06:36,760 --> 00:06:40,900
Donc, pour maintenir cette posture.

112
00:06:41,620 --> 00:06:47,740
Vous devez prendre n'importe quel morceau de tissu,

113
00:06:48,140 --> 00:06:51,800
ce qui vous aidera

114
00:06:53,680 --> 00:06:56,675
à lâcher vos mains,

115
00:06:56,680 --> 00:07:00,920
de manière libre, contre votre ventre.

116
00:07:01,180 --> 00:07:06,080
Sans vous sentir obligé d'utiliser vos doigts ni vos épaules.

117
00:07:06,400 --> 00:07:09,600
Au contraire, vous pouvez lâcher toutes les tensions.

118
00:07:10,180 --> 00:07:12,780
Les coudes sont libres.

119
00:07:12,995 --> 00:07:14,985


120
00:07:15,355 --> 00:07:17,065


121
00:07:17,485 --> 00:07:19,105


122
00:07:20,125 --> 00:07:22,925
Tout droit.

123
00:07:23,820 --> 00:07:27,460
La main gauche dans la main droite.

124
00:07:27,740 --> 00:07:31,000
Les pouces se touchent légèrement.

125
00:07:31,000 --> 00:07:34,480
Le mains à la base de l'abdomen.

126
00:07:35,140 --> 00:07:36,460
Les coudes droits et relâchés.

127
00:07:37,985 --> 00:07:38,985


128
00:07:40,120 --> 00:07:42,540
C'est la posture.

129
00:07:43,220 --> 00:07:47,000


130
00:07:47,640 --> 00:07:49,840
du corps.

131
00:07:49,840 --> 00:07:53,240
Une fois que j'ai mis mon corps

132
00:07:54,345 --> 00:07:57,225
dans la posture de méditation,

133
00:07:57,225 --> 00:08:00,455
en silence devant un mur,

134
00:08:01,280 --> 00:08:02,680
que se passe-t-il ?

135
00:08:02,960 --> 00:08:04,520


136
00:08:04,760 --> 00:08:08,160
Devant le mur, il ne se passe rien d'intéressant.

137
00:08:08,640 --> 00:08:12,380
Nous allons tourner notre regard vers l'intérieur.

138
00:08:14,360 --> 00:08:17,200


139
00:08:17,975 --> 00:08:20,795
Les yeux sont à demi ouverts.

140
00:08:21,085 --> 00:08:24,345
À 45 degrés en diagonale.

141
00:08:24,345 --> 00:08:25,820
Vers le bas.

142
00:08:26,000 --> 00:08:28,985
C'est comme prendre nos yeux

143
00:08:28,985 --> 00:08:32,155
et les poser devant vous.

144
00:08:32,245 --> 00:08:34,785
Il n'y a rien d'intéressant à voir.

145
00:08:35,160 --> 00:08:38,920
Nous faisons de même avec les autres sens.

146
00:08:39,160 --> 00:08:43,180
Nous parlons d'une méditation silencieuse.

147
00:08:44,440 --> 00:08:48,780
Vous n'avez besoin de rien. Pas de musique, rien.

148
00:08:49,080 --> 00:08:51,580
Un silence intérieur.

149
00:08:52,220 --> 00:08:55,680


150
00:08:56,475 --> 00:08:58,315
Il n'est pas non plus nécessaire de parler.

151
00:08:59,235 --> 00:09:00,235


152
00:09:00,900 --> 00:09:02,880
Quand je suis immobile.

153
00:09:02,880 --> 00:09:06,760
Ma vision ne sert à rien.

154
00:09:06,760 --> 00:09:09,000
Mon ouïe non plus.

155
00:09:09,280 --> 00:09:10,820
Ni mes paroles.

156
00:09:10,980 --> 00:09:14,280


157
00:09:14,500 --> 00:09:17,920
Nos sens, avec lesquels nous percevons le monde qui nous entoure,

158
00:09:17,920 --> 00:09:21,300
nous les mettons en "mode avion".

159
00:09:21,300 --> 00:09:23,980


160
00:09:24,300 --> 00:09:27,400
C'est le principe de la posture.

161
00:09:27,400 --> 00:09:30,140
En établissant

162
00:09:30,740 --> 00:09:33,675
cette posture physique et

163
00:09:33,675 --> 00:09:36,775
pour nous couper l'extérieur.

164
00:09:36,775 --> 00:09:39,915
Pour nous couper de nos sentiments.

165
00:09:40,060 --> 00:09:43,940
Nous pourrons alors commencer à vraiment méditer.

166
00:09:51,140 --> 00:09:55,440
Maintenant que nous avons une posture plus calme,

167
00:09:56,040 --> 00:09:59,380
je suis devant un mur, en train de méditer.

168
00:09:59,685 --> 00:10:01,975
Et avec ce silence,

169
00:10:02,495 --> 00:10:04,675
je prends conscience du fait que

170
00:10:05,480 --> 00:10:09,440
mon cerveau est très actif.

171
00:10:09,780 --> 00:10:12,880
Je me rends compte

172
00:10:13,120 --> 00:10:17,080
que j'ai beaucoup de pensées.

173
00:10:17,080 --> 00:10:22,740
Qui disparaîtront avec notre regard.

174
00:10:23,040 --> 00:10:26,240
Des pensées, des images visuelles.

175
00:10:26,245 --> 00:10:28,235
Des Sons.

176
00:10:28,865 --> 00:10:29,945
Beaucoup de choses.

177
00:10:31,420 --> 00:10:33,520


178
00:10:33,720 --> 00:10:37,075
Dans notre méditation zen,

179
00:10:37,080 --> 00:10:40,205
nou allons laisser passer ces pensées.

180
00:10:40,205 --> 00:10:43,180
Ceci est un point fondamental

181
00:10:43,180 --> 00:10:45,500
de la posture de méditation zen.

182
00:10:45,640 --> 00:10:49,200
Quand je suis en zazen,

183
00:10:49,200 --> 00:10:53,380
je ne passe pas mon temps à réfléchir sur

184
00:10:53,380 --> 00:10:57,240
quelque chose, d'intéressant ou non.

185
00:10:57,420 --> 00:10:59,340
Il ne s'agit pas de cela.

186
00:10:59,580 --> 00:11:03,700
C'est à peu près comme notre corps

187
00:11:03,715 --> 00:11:06,685
que nous avons placé dans une situation calme.

188
00:11:06,825 --> 00:11:10,215
Je vais mettre mon esprit et mon mental,

189
00:11:10,505 --> 00:11:13,415
qui est toujours agité,

190
00:11:13,415 --> 00:11:16,035


191
00:11:16,035 --> 00:11:19,035


192
00:11:19,040 --> 00:11:20,500
dans un état de calme.

193
00:11:20,680 --> 00:11:23,640


194
00:11:23,640 --> 00:11:26,620
Laisser mes pensées s'échapper.

195
00:11:27,760 --> 00:11:31,800
Nos pensées jaillissent de l'extérieur.

196
00:11:32,680 --> 00:11:35,200
Si on se demande :

197
00:11:35,200 --> 00:11:39,040
"Que va-t-on penser dans 5 minutes ou dans une demi-heure ? Ou demain ?"

198
00:11:39,305 --> 00:11:42,575
Personne ne sait.

199
00:11:43,440 --> 00:11:46,780
Même dans notre sommeil,

200
00:11:46,960 --> 00:11:50,080
nous rêvons.

201
00:11:50,540 --> 00:11:53,440
Notre activité mentale, consciente ou inconsciente, ne s'arrête jamais.

202
00:11:53,660 --> 00:11:57,000
Pendant la méditation zen, cette activité mentale,

203
00:11:57,165 --> 00:12:00,445
nous allons la faire tomber et la calmer.

204
00:12:00,760 --> 00:12:04,760
Pour laisser passer nos pensées.

205
00:12:05,100 --> 00:12:07,520
Mais alors vous direz :

206
00:12:07,520 --> 00:12:08,900
"OK, très bien, mais Comment faire ?"

207
00:12:09,080 --> 00:12:12,920
Grâce à notre respiration.

208
00:12:13,240 --> 00:12:16,400


209
00:12:16,440 --> 00:12:19,400
Comment respirer pendant zazen ?

210
00:12:19,400 --> 00:12:23,920


211
00:12:26,740 --> 00:12:29,660


212
00:12:29,880 --> 00:12:32,560
C'est une respiration abdominale.

213
00:12:34,415 --> 00:12:36,265
Nous inspirons,

214
00:12:36,705 --> 00:12:39,985
nous remplissons nos poumons,

215
00:12:39,985 --> 00:12:42,185
avec une certaine générosité.

216
00:12:42,925 --> 00:12:45,245
Et nous allons nous concentrer

217
00:12:45,245 --> 00:12:46,740
surtout

218
00:12:47,120 --> 00:12:49,900
sur notre expiration.

219
00:12:50,200 --> 00:12:52,560


220
00:12:52,740 --> 00:12:55,875
Et expirer petit à petit.

221
00:12:55,875 --> 00:12:59,165
Nous prolongeons l'expiration.

222
00:12:59,225 --> 00:13:02,275


223
00:13:02,345 --> 00:13:05,135


224
00:13:05,140 --> 00:13:06,720
Par le nez.

225
00:13:07,000 --> 00:13:10,360
Doucement, sans faire de bruit.

226
00:13:10,700 --> 00:13:13,080
Nous sommes dans une méditation silencieuse.

227
00:13:13,300 --> 00:13:16,240
Nous expirons par le nez.

228
00:13:17,160 --> 00:13:21,260
Longuement.

229
00:13:21,620 --> 00:13:23,880
À la fin de votre expiration,

230
00:13:24,625 --> 00:13:27,785
inspirez et recommencez.

231
00:13:36,465 --> 00:13:39,565


232
00:13:43,440 --> 00:13:46,980
Si vous vous concentrez sur

233
00:13:47,240 --> 00:13:50,420
l'inspiration et l'expiration, vous établissez

234
00:13:50,820 --> 00:13:53,500
une respiration très calme.

235
00:13:54,540 --> 00:13:57,495


236
00:13:57,500 --> 00:14:02,600
Vous devez amplifier votre expiration.

237
00:14:03,160 --> 00:14:07,180
Le diaphragme s'abaisse.

238
00:14:07,480 --> 00:14:08,740


239
00:14:08,980 --> 00:14:11,980
Il masse les intestins.

240
00:14:11,980 --> 00:14:13,980
Ce qui leur fait du bien.

241
00:14:14,160 --> 00:14:17,700
Et lentement, vous pouvez sentir,

242
00:14:18,160 --> 00:14:20,820
comme si quelque chose se gonflait

243
00:14:21,145 --> 00:14:23,720
dans la zone où nous avons mis la main.

244
00:14:24,020 --> 00:14:27,700
C'est ici que les choses se passent.

245
00:14:28,040 --> 00:14:30,520
Pas ici,

246
00:14:31,900 --> 00:14:34,925
mais là.

247
00:14:34,925 --> 00:14:39,360
Respirez par le nez et en silence.

248
00:14:46,440 --> 00:14:49,900
La posture physique : croisez les jambes.

249
00:14:50,020 --> 00:14:52,860
Les genoux touchent le sol.

250
00:14:53,420 --> 00:14:56,140
Le bassin est légèrement basculé vers l'avant.

251
00:14:56,700 --> 00:15:00,760
Nous redressons notre colonne vertébrale.

252
00:15:01,160 --> 00:15:02,840
Jusqu'au sommet de la tête.

253
00:15:02,840 --> 00:15:08,040
Nous poussons le ciel avec la tête.

254
00:15:08,540 --> 00:15:11,800
La main gauche dans la main droite.

255
00:15:11,925 --> 00:15:13,925
Les pouces horizontaux.

256
00:15:13,925 --> 00:15:17,700
Qui ne forment ni vallée, ni montagne.

257
00:15:17,960 --> 00:15:20,100
Tout droits.

258
00:15:20,100 --> 00:15:24,140
Lorsque vous mettez vos mains en contact avec le ventre,

259
00:15:24,500 --> 00:15:26,640
vous placez quelque chose dessous

260
00:15:26,665 --> 00:15:28,675
pour avoir une position plus confortable.

261
00:15:29,125 --> 00:15:31,805
Vous avez les coudes bien droits.

262
00:15:31,805 --> 00:15:32,885
C'est notre posture physique.

263
00:15:33,645 --> 00:15:37,065
Deuxièmement, notre état d'esprit.

264
00:15:37,280 --> 00:15:41,760
Nous nous coupons de l'extérieur.

265
00:15:42,160 --> 00:15:44,060
Des sons.

266
00:15:44,060 --> 00:15:46,760
Nous n'allons pas méditer avec une radio.

267
00:15:46,760 --> 00:15:48,755
Ou avec votre téléphone portable.

268
00:15:48,755 --> 00:15:51,635
Alors, éteignez-les.

269
00:15:52,040 --> 00:15:54,620
On coupe tous les sons.

270
00:15:54,620 --> 00:15:58,295
Nous portons notre regard vers l'intérieur et nous revenons à

271
00:15:58,300 --> 00:16:02,040
être l'observateur de ce qui se passe à l'intérieur.

272
00:16:02,120 --> 00:16:05,000
Et que se passe-t-il à l'intérieur ?

273
00:16:05,380 --> 00:16:07,180
On remarque qu'il y a une activité mentale.

274
00:16:07,660 --> 00:16:10,720
Les pensées surgissent.

275
00:16:10,725 --> 00:16:12,665
Nous ne pouvons pas les contrôler.

276
00:16:13,155 --> 00:16:16,415
Elles surgissent automatiquement.

277
00:16:16,440 --> 00:16:21,000
Il ne s'agit donc pas d'empêcher cette activité cérébrale.

278
00:16:21,380 --> 00:16:24,740


279
00:16:24,740 --> 00:16:29,740
Il ne s'agit pas de suivre nos pensées.

280
00:16:29,960 --> 00:16:33,420
Nous les laissons passer.

281
00:16:33,500 --> 00:16:36,385
Comment pouvons-nous les laisser passer ?

282
00:16:36,385 --> 00:16:41,380
Tout d'abord, avec la concentration sur l'ici et maintenant.

283
00:16:41,540 --> 00:16:43,260
Sur notre posture physique.

284
00:16:43,795 --> 00:16:46,835
Et avec cette concentration.

285
00:16:46,840 --> 00:16:51,640
Ici et maintenant, sur ma respiration.

286
00:16:52,080 --> 00:16:54,960
Je suis pleinement conscient de ma respiration.

287
00:16:55,265 --> 00:16:58,505
De ma respiration abdominale.

288
00:16:58,540 --> 00:17:02,580
Alors on respire par le nez.

289
00:17:03,080 --> 00:17:04,520
Nous inspirons.

290
00:17:05,095 --> 00:17:08,385
Nous prolongeons l'expiration.

291
00:17:08,900 --> 00:17:10,460
À chaque fois.

292
00:17:10,700 --> 00:17:14,040
Une expiration plus dousse, plus longue.

293
00:17:14,740 --> 00:17:17,735
Et à la fin de l'expiration, on recommence.

294
00:17:17,740 --> 00:17:19,800
Ça va et ça vient comme

295
00:17:20,080 --> 00:17:23,920
des vagueq dans l'océan..

296
00:17:24,360 --> 00:17:28,340
Long, long, long.

297
00:17:29,180 --> 00:17:32,605
Vous vous concentrez sur l'ici et maintenant,

298
00:17:32,605 --> 00:17:36,620
sur les points de la posture.

299
00:17:36,920 --> 00:17:41,140
Nous ne dormons pas, pendant la méditation.

300
00:17:41,680 --> 00:17:44,880
Et si vous vous concentrez en même temps

301
00:17:45,040 --> 00:17:48,500
sur la respiration

302
00:17:48,680 --> 00:17:51,140
harmonieuse et silencieuse.

303
00:17:51,140 --> 00:17:53,780
Avec une exspiration prolongée.

304
00:17:53,940 --> 00:17:57,680
Vous n'aurez pas le temps de penser,

305
00:17:57,800 --> 00:18:00,060
aussi intéressantes que vos pensées puissent être.

306
00:18:00,295 --> 00:18:03,365


307
00:18:03,365 --> 00:18:05,665
Nous allons méditer 20 minutes, une demi-heure, une heure.

308
00:18:06,820 --> 00:18:08,540
C'est vous qui décidez.

309
00:18:08,720 --> 00:18:12,380
Mais ce moment de méditation, de concentration,

310
00:18:12,455 --> 00:18:15,635
vous apportera des bénéfices immédiats.

311
00:18:15,635 --> 00:18:17,255
Vous vous sentirez

312
00:18:18,405 --> 00:18:21,405
un peu plus calme,

313
00:18:21,405 --> 00:18:24,665
un peu plus léger.

314
00:18:24,665 --> 00:18:27,355


315
00:18:27,355 --> 00:18:29,355
Vous allez créer une distance entre

316
00:18:29,355 --> 00:18:32,445
vous-même et toutes les émotions qui surgissent dans votre vie.

317
00:18:32,885 --> 00:18:34,395


318
00:18:34,785 --> 00:18:37,725
C'est vraiment un bon outil.

319
00:18:38,135 --> 00:18:41,015


320
00:18:41,015 --> 00:18:44,055


321
00:18:44,060 --> 00:18:48,420
Maintenant que la moitié de la population est confinée.

322
00:19:29,920 --> 00:19:34,960
Conseils pour un débutant

323
00:19:35,300 --> 00:19:40,380
Au lieu de se mettre en colère ou de faire des histoires,

324
00:19:40,380 --> 00:19:45,380


325
00:19:45,380 --> 00:19:48,160


326
00:19:48,420 --> 00:19:49,840
asseyez-vous

327
00:19:49,995 --> 00:19:53,325
devant un mur. Adoptez cette méditation.

328
00:19:54,440 --> 00:19:58,320
Travaillez sur vous-mêmes de cette manière.

329
00:19:58,760 --> 00:20:00,620
Et vous remarquerez bientôt

330
00:20:00,625 --> 00:20:02,925
à quel point cela vous conviendra.
