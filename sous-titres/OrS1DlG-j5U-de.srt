1
00:00:00,000 --> 00:00:08,750
Ich mache Zazen mit einem Kesa von Barbara [Kosen].

2
00:00:11,540 --> 00:00:12,540
Seide.

3
00:00:15,330 --> 00:00:19,215
Ich bin sehr glücklich.

4
00:00:20,235 --> 00:00:22,830
Ich mag Barbara sehr gerne.

5
00:00:24,580 --> 00:00:27,210
Sie ist eine große Heilige.

6
00:00:33,770 --> 00:00:38,190
Glücklicherweise sind meine Jünger im Allgemeinen besser als ich.

7
00:00:42,910 --> 00:00:45,830
Besonders Barbara.

8
00:00:48,850 --> 00:00:51,270
Und Ariadna auch.

9
00:00:55,710 --> 00:00:58,515
Die drei Schätze.

10
00:01:01,015 --> 00:01:03,817
Buddha, Dharma, Sangha.

11
00:01:06,267 --> 00:01:10,440
sind untrennbar miteinander verbunden.

12
00:01:24,820 --> 00:01:28,470
Ich habe dich als Kusen vorbereitet...

13
00:01:35,340 --> 00:01:41,560
...die Fortsetzung dessen, was ich im Sommercamp gemacht habe.

14
00:01:45,170 --> 00:01:50,930
Über die vier Grundlagen der magischen Kräfte.

15
00:01:56,750 --> 00:02:01,770
Ich habe das im Ferienlager gemacht...

16
00:02:02,130 --> 00:02:10,530
Man sagt, die vier Fundamente seien wie die vier Hufe eines Pferdes.

17
00:02:35,940 --> 00:02:40,120
Das erste ist Willenskraft, Wille.

18
00:02:44,460 --> 00:02:50,300
Der Wille ist etwas Stärkeres als Sie.

19
00:02:52,290 --> 00:02:55,400
Und das drängt Sie zum Handeln.

20
00:02:58,920 --> 00:03:02,400
Es ist wie eine Frage des Überlebens.

21
00:03:06,430 --> 00:03:08,350
Es ist sehr stark.

22
00:03:09,410 --> 00:03:12,160
Ich habe im Sommercamp darüber gesprochen.

23
00:03:15,530 --> 00:03:18,550
Der zweite ist der Geist.

24
00:03:20,745 --> 00:03:23,015
Der zweite Huf.

25
00:03:27,730 --> 00:03:33,390
Das dritte ist der Fortschritt, das Vorankommen.

26
00:03:38,440 --> 00:03:40,810
Der vierte ist gedacht.

27
00:03:43,810 --> 00:03:46,855
Das ist es, was ich denke.

28
00:03:48,135 --> 00:03:50,020
Die zweite.

29
00:03:54,970 --> 00:03:57,515
Ich habe im Sommercamp darüber gesprochen,

30
00:03:57,625 --> 00:04:00,860
aber ich war noch nicht fertig.

31
00:04:03,270 --> 00:04:09,595
Ich werde Ihnen drei Zen-Geschichten erzählen...

32
00:04:11,995 --> 00:04:16,060
...die die Natur des Geistes besser verstehen lassen.

33
00:04:19,990 --> 00:04:25,860
Meister Eka, ein Schüler von Bodhidharma...

34
00:04:32,370 --> 00:04:35,760
...im Schnee stehen...

35
00:04:39,330 --> 00:04:41,145
...eisig...

36
00:04:44,635 --> 00:04:47,564
... wandte sich an seinen Herrn!

37
00:04:53,470 --> 00:04:57,240
"Meister, mein Geist ist nicht friedlich. »

38
00:05:08,460 --> 00:05:12,130
"Ich bitte Sie, geben Sie ihm Frieden! »

39
00:05:18,940 --> 00:05:25,380
Eka hatte eine Menge Männer getötet, weil er beim Militär gewesen war.

40
00:05:33,130 --> 00:05:37,370
Er fühlte sich wegen all dieser Dinge extrem schuldig.

41
00:05:40,780 --> 00:05:44,100
Er konnte die Schuld nicht loswerden.

42
00:05:49,410 --> 00:05:54,485
Er ist im Schnee und fragt Bodhidharma: "Akzeptiere mich! »

43
00:06:00,245 --> 00:06:02,050
"Als Jünger! »

44
00:06:06,000 --> 00:06:09,410
Bodhidharma antwortet nicht.

45
00:06:17,770 --> 00:06:19,615
Er besteht darauf.

46
00:06:23,185 --> 00:06:25,790
Bodhidharma bewegt sich nicht.

47
00:06:30,960 --> 00:06:35,420
Am Ende schneidet er sich mit seinem Schwert den Arm ab.

48
00:06:45,860 --> 00:06:48,415
Bodhidharma sagt: "Nun..."

49
00:06:58,695 --> 00:07:02,350
Bodhidharma grunzt: "Han..."

50
00:07:06,620 --> 00:07:09,980
Und er setzt Zazen fort.

51
00:07:14,280 --> 00:07:17,145
Eka besteht darauf, bettelt:

52
00:07:20,910 --> 00:07:24,630
"Ich bitte Sie, meinen Geist zu besänftigen! »

53
00:07:29,190 --> 00:07:31,335
"Es ist unerträglich! »

54
00:07:32,305 --> 00:07:34,400
"Ich werde verrückt! »

55
00:07:44,290 --> 00:07:48,140
Schließlich gibt ihm Bodhidharma einen Blick:

56
00:07:54,430 --> 00:07:58,595
"Bring mir deinen Geist. »

57
00:08:10,745 --> 00:08:13,680
"Und ich werde ihm Frieden geben. »

58
00:08:20,300 --> 00:08:22,590
Meister Eka antwortete ihm:

59
00:08:26,210 --> 00:08:29,105
"Meister, ich suchte ihn, meinen Geist! »

60
00:08:33,665 --> 00:08:37,750
"Und es blieb unzugänglich, schwer fassbar! »

61
00:08:46,240 --> 00:08:49,020
Eka sucht nach Seelenfrieden.

62
00:08:52,630 --> 00:08:55,050
Er hat Schmerzen.

63
00:08:56,520 --> 00:08:59,340
Er will Ruhe und Frieden.

64
00:09:04,085 --> 00:09:05,490
Egal, wie oft wir es ihm sagen:

65
00:09:05,550 --> 00:09:07,845
"Es ist Ihr Verstand! »

66
00:09:13,950 --> 00:09:16,360
"Wo ist es, mein Verstand? »

67
00:09:17,805 --> 00:09:19,655
"Wo ist er, mein Geist? »

68
00:09:22,452 --> 00:09:25,182
"Wo ist die Wurzel dieses Geistes? »

69
00:09:29,400 --> 00:09:32,480
"Was ist das Wesen dieses Geistes? »

70
00:09:35,925 --> 00:09:38,528
"Ist es im Gehirn? »

71
00:09:46,670 --> 00:09:48,510
"Ist es im Herzen? »

72
00:09:56,655 --> 00:10:00,025
Auch wenn sich nicht jeder den Arm abschneidet,

73
00:10:03,270 --> 00:10:07,120
im Schnee, in der Kälte...

74
00:10:08,810 --> 00:10:12,880
Jeder will etwas über seinen Verstand herausfinden.

75
00:10:16,040 --> 00:10:18,570
Alle wollen friedlich sein.

76
00:10:21,970 --> 00:10:23,691
Befrieden Sie seinen Geist.

77
00:10:27,930 --> 00:10:31,515
Eka hatte eine bemerkenswerte Reaktion, als er sagte:

78
00:10:31,645 --> 00:10:35,920
"Ich habe ihn gesucht, aber er ist nicht zu finden! »

79
00:10:43,990 --> 00:10:46,030
"Ich konnte es nicht finden. »

80
00:10:48,615 --> 00:10:53,235
Auf Japanisch heißt es "Shin fukatoku".

81
00:11:07,460 --> 00:11:09,885
"Fu" ist die Verneinung, "nichts".

82
00:11:13,960 --> 00:11:15,870
"Ta": Ergreifen.

83
00:11:19,270 --> 00:11:21,100
Ergreifen, finden.

84
00:11:24,230 --> 00:11:28,260
"Toku" ist wie "Mushotoku".

85
00:11:29,550 --> 00:11:33,470
"Toku" bedeutet "holen".

86
00:11:35,910 --> 00:11:37,930
"Sein Ziel erreichen."

87
00:11:40,370 --> 00:11:43,320
"Shin fukatoku",

88
00:11:47,710 --> 00:11:51,310
Es ist der völlig unauffindbare, schwer fassbare Geist.

89
00:11:57,710 --> 00:11:58,955
sagt Eka:

90
00:11:59,085 --> 00:12:01,650
"Ich habe danach gesucht, aber ich konnte es nicht finden. »

91
00:12:04,480 --> 00:12:06,670
Das ist eine ausgezeichnete Antwort.

92
00:12:09,470 --> 00:12:13,340
Aber das von Bodhidharma ist noch schöner.

93
00:12:18,060 --> 00:12:22,330
"Bring mir deinen Geist, und ich werde ihn besänftigen. »

94
00:12:29,700 --> 00:12:30,660
sagte Eka:

95
00:12:30,720 --> 00:12:32,670
"Ich kann nicht! »

96
00:12:34,760 --> 00:12:37,320
"Nicht nur, dass ich ihn nicht fangen kann,"

97
00:12:37,400 --> 00:12:40,320
"aber ich kann es nicht einmal finden! »

98
00:12:44,610 --> 00:12:46,270
sagt Bodhidharma zu ihm:

99
00:12:48,910 --> 00:12:52,340
"In diesem Fall ist er bereits befriedet! »

100
00:13:07,610 --> 00:13:09,520
Zweite Geschichte:

101
00:13:11,955 --> 00:13:14,575
Ich mag Geschichten.

102
00:13:16,890 --> 00:13:18,750
Meister Obaku.

103
00:13:20,240 --> 00:13:22,330
Basos Meister.

104
00:13:25,480 --> 00:13:30,240
Seine Lehre vor den Mönchen halten.

105
00:13:33,405 --> 00:13:35,605
Jemand ruft nach ihm:

106
00:13:41,550 --> 00:13:46,580
"Der Meister muss eine große Konzentrationsfähigkeit besitzen. »

107
00:13:52,200 --> 00:13:53,985
Meister Obaku antwortet:

108
00:13:58,735 --> 00:14:02,110
"Konzentration".

109
00:14:03,950 --> 00:14:07,594
"...oder im Gegenteil, Ablenkung,"

110
00:14:10,310 --> 00:14:14,610
"...sagen Sie, dass diese Dinge existieren? »

111
00:14:18,300 --> 00:14:20,600
"Ich sage euch:"

112
00:14:23,770 --> 00:14:27,100
"Sucht sie, und ihr werdet sie nirgendwo finden. »

113
00:14:32,840 --> 00:14:36,340
"Aber wenn Sie mir sagen, dass es sie nicht gibt,"

114
00:14:40,390 --> 00:14:42,490
"Ich sage Ihnen:"

115
00:14:45,510 --> 00:14:47,390
"was immer Sie denken,"

116
00:14:53,490 --> 00:14:58,050
"Sie sind ständig dort präsent, wo Sie sind! »

117
00:15:08,040 --> 00:15:11,650
Selbst wenn Sie denken, dass Sie abgelenkt sind,

118
00:15:16,330 --> 00:15:20,610
schauen Sie sich den Ort an, an dem die Ablenkung entstanden ist.

119
00:15:24,850 --> 00:15:27,950
Sie werden sehen, dass es letztendlich keinen Ursprung hat.

120
00:15:33,360 --> 00:15:37,260
Der psychische Zustand der Ablenkung, die erscheint...

121
00:15:40,870 --> 00:15:43,490
...geht nirgendwo hin.

122
00:15:44,920 --> 00:15:46,695
In alle zehn Richtungen.

123
00:15:48,305 --> 00:15:50,690
Und er wird auch nirgendwo anders hingehen.

124
00:16:10,270 --> 00:16:13,490
In Zazen gibt es keine Ablenkung oder Konzentration.

125
00:16:21,040 --> 00:16:23,470
Bleiben Sie einfach still sitzen.

126
00:16:30,800 --> 00:16:33,090
Der Rest sind Techniken.

127
00:16:35,820 --> 00:16:40,168
Es bringt uns weg vom authentischen Zazen.

128
00:16:45,129 --> 00:16:46,769
Dritte Geschichte.

129
00:16:47,119 --> 00:16:49,719
- Ja, ich habe dich verwöhnt! -

130
00:17:00,600 --> 00:17:03,750
Es stammt aus der Zeit Basos in China.

131
00:17:05,760 --> 00:17:18,250
Baso, das ist der Schüler des Meisters, von dem ich gerade gesprochen habe, Obaku.

132
00:17:24,410 --> 00:17:28,330
Zu dieser Zeit lebte ein Gelehrter

133
00:17:30,950 --> 00:17:34,280
...dessen Name Ryo war.

134
00:17:36,560 --> 00:17:41,090
Er war berühmt für seine Kenntnis des Buddhismus.

135
00:17:45,320 --> 00:17:48,840
Er verbrachte sein Leben mit Vorträgen.

136
00:17:53,410 --> 00:17:58,790
Eines Tages stellte sich heraus, dass er ein Interview mit Baso hatte.

137
00:18:04,890 --> 00:18:07,160
fragte Baso ihn:

138
00:18:09,190 --> 00:18:12,770
- Über welche Themen referieren Sie? »

139
00:18:15,850 --> 00:18:20,040
- "Über das Geist-Sutra, das Hannya Shingyo. »

140
00:18:25,360 --> 00:18:28,220
Es wird auch das Herz-Sutra genannt.

141
00:18:32,420 --> 00:18:36,750
Es ist nicht klar, ob der Verstand im Gehirn oder im Herzen ist.

142
00:18:41,320 --> 00:18:47,240
In "Shingyo" bedeutet "Shin" Geist oder Herz.

143
00:18:52,600 --> 00:18:55,460
"Gyô" bedeutet Sutra.

144
00:18:58,090 --> 00:19:00,285
Also fragt Baso ihn:

145
00:19:03,825 --> 00:19:07,260
- "Was verwenden Sie für Ihre Vorlesungen? »

146
00:19:09,950 --> 00:19:11,755
Ryo antwortete:

147
00:19:14,525 --> 00:19:17,527
- "Mit dem Geist. »

148
00:19:19,530 --> 00:19:21,355
sagt Baso zu ihm:

149
00:19:24,685 --> 00:19:30,210
- "Der Geist, er ist wie ein wirklich guter Schauspieler! »

150
00:19:34,480 --> 00:19:39,390
"Die Bedeutung seines Denkens ist wie die Extravaganz eines Possenreißers. »

151
00:19:45,690 --> 00:19:51,580
"Was die sechs Sinne betrifft, die uns wie in einem Film im Kino fühlen lassen..."

152
00:20:07,270 --> 00:20:10,420
"Diese sechs Sinne sind leer! »

153
00:20:12,420 --> 00:20:16,970
"Wie eine Welle ohne wirklichen Anfang und Ende. »

154
00:20:21,260 --> 00:20:26,340
"Wie könnte der Verstand einen Vortrag über ein Sutra halten?"

155
00:20:33,590 --> 00:20:38,200
Ryo antwortete, er halte sich für klug:

156
00:20:42,150 --> 00:20:47,880
- "Wenn der Geist es nicht tun kann, kann es vielleicht der Nicht-Geist tun? »

157
00:20:51,630 --> 00:20:53,315
sagt Baso zu ihm:

158
00:20:55,055 --> 00:20:58,607
- "Ja, genau! »

159
00:21:00,167 --> 00:21:04,150
"Der Nichtgeist kann völlig belehren. »

160
00:21:09,050 --> 00:21:13,230
Ryo, der sich mit seiner Antwort zufrieden glaubte...

161
00:21:16,350 --> 00:21:19,730
...staubte seine Ärmel ab...

162
00:21:25,070 --> 00:21:28,100
...und machte sich zur Abfahrt bereit.

163
00:21:31,390 --> 00:21:33,820
Meister Baso ruft ihn:

164
00:21:35,920 --> 00:21:38,605
- "Professor! »

165
00:21:39,305 --> 00:21:41,160
Ryo drehte seinen Kopf.

166
00:21:47,460 --> 00:21:49,115
sagt Baso zu ihm:

167
00:21:49,185 --> 00:21:51,560
- "Was machen Sie da? »

168
00:21:55,630 --> 00:21:58,225
Ryo hatte eine tiefgreifende Offenbarung.

169
00:22:03,905 --> 00:22:07,490
Er ist sich seines Mangels an Geist bewusst geworden.

170
00:22:10,940 --> 00:22:12,440
Und Baso sagt zu ihm:

171
00:22:12,550 --> 00:22:16,000
"Von der Geburt bis zum Tod ist es so. »

172
00:22:21,750 --> 00:22:24,976
Wir denken und tun Dinge,

173
00:22:28,400 --> 00:22:30,670
spontan.

174
00:22:33,320 --> 00:22:36,315
Ryo wollte sich vor Baso verbeugen,

175
00:22:42,215 --> 00:22:44,840
aber dieser sagt zu ihm:

176
00:22:46,660 --> 00:22:51,220
"Kein Grund, sich töricht zu verstellen! »

177
00:23:02,150 --> 00:23:07,280
Ryo war am ganzen Körper mit Schweiß bedeckt.

178
00:23:16,080 --> 00:23:19,360
Er ging zurück zu seinem Tempel.

179
00:23:21,260 --> 00:23:24,295
Und er sagte zu seinen Jüngern:

180
00:23:27,575 --> 00:23:31,645
"Ich dachte, wenn ich sterbe, könnten wir sagen: "

181
00:23:37,670 --> 00:23:41,440
"Dass ich die besten Vorträge gehalten habe..."

182
00:23:44,650 --> 00:23:47,170
"...auf Zen. »

183
00:23:52,430 --> 00:23:55,830
"Heute habe ich Baso eine Frage gestellt. »

184
00:24:00,250 --> 00:24:03,710
"Und er lichtete den Nebel meines ganzen Lebens. »

185
00:24:10,790 --> 00:24:14,400
Ryo gab das Vortragen auf.

186
00:24:18,180 --> 00:24:21,125
Und zog sich in das Herz der Berge zurück.

187
00:24:24,475 --> 00:24:28,290
Und wir haben nie wieder von ihm gehört.

188
00:25:15,750 --> 00:25:21,950
Warum ist der Geist so schwer zu fangen?

189
00:25:24,620 --> 00:25:25,755
Warum?

190
00:25:35,090 --> 00:25:39,490
Weil das Denken, das Bewusstsein...

191
00:25:44,230 --> 00:25:46,680
...und sogar die Realität...

192
00:26:00,830 --> 00:26:02,620
...erscheint manchmal...

193
00:26:06,380 --> 00:26:08,210
...verschwindet manchmal.

194
00:26:10,690 --> 00:26:16,570
Manchmal erscheint, manchmal verschwindet...

195
00:26:26,840 --> 00:26:30,275
Wenn sie verschwindet, wissen wir nicht, wo sie ist.

196
00:26:32,895 --> 00:26:35,532
Aber wo sie ist...

197
00:26:37,512 --> 00:26:39,260
...sie ist da.

198
00:26:42,620 --> 00:26:44,475
Sie ist dort.

199
00:26:53,205 --> 00:26:56,900
Wo sie nicht ist, ist sie auch da.

200
00:27:04,820 --> 00:27:09,140
Zeit und Bewusstsein sind nicht linear.

201
00:28:08,350 --> 00:28:10,574
Was die Ablenkung angeht...

202
00:28:19,220 --> 00:28:24,070
Als ich ein Kind war, sagte man mir immer
dass ich nicht konzentriert war.

203
00:28:37,820 --> 00:28:39,365
abgelenkt.

204
00:28:42,805 --> 00:28:45,670
Aber als ich spielte...

205
00:28:47,257 --> 00:28:50,930
Als ich noch mit meinen kleinen Autos spielte...

206
00:28:55,110 --> 00:28:58,880
...war ich völlig konzentriert.

207
00:29:06,020 --> 00:29:11,980
Die Hannya Shingyo und die Zeremonie sind Jonathan Duval gewidmet,

208
00:29:20,210 --> 00:29:25,490
Und Alfonso Jill.

209
00:29:58,720 --> 00:30:09,005
Sutra der großen Weisheit, die es erlaubt, über

210
00:30:12,312 --> 00:30:17,006
Der Bodhisattva der wahren Freiheit,

211
00:30:17,196 --> 00:30:21,144
durch die tiefe Praxis der Großen Weisheit,

212
00:30:21,264 --> 00:30:25,022
versteht, dass der Körper und die fünf Skandah nichts anderes als Vakuum sind.

213
00:30:25,152 --> 00:30:30,151
und durch dieses Verständnis hilft er all denen, die leiden.

214
00:30:30,341 --> 00:30:34,870
O Sariputra, die Phänomene sind nicht anders als ku.

215
00:30:34,970 --> 00:30:39,527
Ku unterscheidet sich nicht von Anormalen.

216
00:30:39,987 --> 00:30:42,996
Aus Phänomenen werden ku.

217
00:30:43,196 --> 00:30:51,305
Ku wird zum Phänomen (Form ist Leere, Leere ist Form),

218
00:30:51,575 --> 00:30:56,155
auch die fünf Skandas sind Phänomene.

219
00:30:56,455 --> 00:31:03,001
O Sariputra, alle Existenz hat den Charakter von ku,

220
00:31:03,451 --> 00:31:08,429
Es gibt keine Geburt und keinen Anfang,

221
00:31:08,989 --> 00:31:14,707
keine Reinheit, kein Makel, kein Wachstum, kein Verfall.

222
00:31:14,977 --> 00:31:20,130
Deshalb gibt es in Ku keine Form, keine Skanda,

223
00:31:20,939 --> 00:31:26,080
keine Augen, keine Ohren, keine Nase, keine Zunge, kein Körper, kein Gewissen;

224
00:31:26,491 --> 00:31:32,397
Es gibt keine Farbe, keinen Ton, keinen Geruch, keinen Geschmack, keine Berührung oder Gedanken;

225
00:31:32,527 --> 00:31:37,915
Es gibt kein Wissen, keine Ignoranz, keine Illusion, kein Aufhören des Leidens,

226
00:31:38,225 --> 00:31:43,283
Es gibt kein Wissen, keinen Profit, keine Gemeinnützigkeit.

227
00:31:43,413 --> 00:31:48,649
Für den Bodhisattva, dank dieser Weisheit, die über ihn hinausführt,

228
00:31:48,969 --> 00:31:54,622
Es gibt keine Angst oder Furcht.

229
00:31:54,882 --> 00:32:02,024
Alle Illusionen und Anhaftungen werden entfernt

230
00:32:02,504 --> 00:32:11,116
und er kann das endgültige Ende des Lebens, das Nirwana, ergreifen.

231
00:32:12,325 --> 00:32:16,407
Alle Buddhas der Vergangenheit, Gegenwart und Zukunft...

232
00:32:16,497 --> 00:32:21,489
zum Verständnis dieser Höchsten Weisheit gelangen kann...

233
00:32:21,569 --> 00:32:25,021
der Leiden erlöst, der Satori,

234
00:32:25,081 --> 00:32:30,329
durch diese Beschwörungsformel, unvergleichlich und beispiellos, authentisch,

235
00:32:30,432 --> 00:32:36,163
die alles Leid beseitigt und es Ihnen ermöglicht, die Wirklichkeit, das wahre Ku zu finden:

236
00:32:38,113 --> 00:32:52,170
"Geht, geht, geht zusammen über das Jenseits hinaus am Ufer des Satori. »

237
00:33:01,240 --> 00:33:14,420
Ganz gleich, wie viele Wesen es gibt, ich gelobe, sie alle zu retten.

238
00:33:14,600 --> 00:33:23,095
Ganz gleich, wie viele Leidenschaften es gibt, ich gelobe, sie alle zu besiegen.

239
00:33:25,635 --> 00:33:34,292
Ganz gleich, wie viele Dharmas es gibt, ich gelobe, sie alle zu erwerben.

240
00:33:35,112 --> 00:33:43,700
Ganz gleich, wie perfekt ein Buddha ist, ich gelobe, einer zu werden.

241
00:33:44,085 --> 00:33:48,247
Mögen die Verdienste dieser Rezitation

242
00:33:48,247 --> 00:33:52,768
alle Wesen überall durchdringen,

243
00:33:52,768 --> 00:33:57,538
damit wir alle fühlenden Wesen

244
00:33:57,538 --> 00:34:03,129
gemeinsam den Buddha-Weg verwirklichen können.

245
00:34:05,332 --> 00:34:16,597
Alle Buddhas der Vergangenheit, Gegenwart und Zukunft in den zehn Richtungen.

246
00:34:17,517 --> 00:34:26,629
Alle Bodhisattvas und Patriarchen

247
00:34:27,869 --> 00:34:38,142
Das Sutra der "Weisheit, die darüber hinausgeht"

248
00:34:53,546 --> 00:34:58,216
Sanpaï!

249
00:37:25,590 --> 00:37:28,060
Puh!

250
00:37:46,750 --> 00:37:48,965
Hallo, alle zusammen!

251
00:37:57,955 --> 00:38:03,310
Ich hoffe, Sie sind glücklich, dass die Pandemie vorbei ist!

252
00:38:32,700 --> 00:38:34,600
Hola Paola!
