1
00:00:03,420 --> 00:00:07,900
Was ist Geist? 

2
00:00:08,220 --> 00:00:16,200
Auf Chinesisch und Japanisch werden verschiedene Ideogramme als "Geist" übersetzt. 

3
00:00:16,880 --> 00:00:19,340
Manchmal wird es als "Herz" übersetzt. 

4
00:00:21,960 --> 00:00:23,560
Es ist jede Existenz. 

5
00:00:23,820 --> 00:00:25,240
Es ist die lebendige Gemeinschaft. 

6
00:00:25,500 --> 00:00:27,400
Dies ist der gemeinsame Geist der lebendigen Gemeinschaft. 

7
00:00:27,740 --> 00:00:28,900
Das ist der Geist. 

8
00:00:29,120 --> 00:00:30,780
Es gibt nur einen Geist. 

9
00:00:32,000 --> 00:00:34,960
Was schafft den Geist? 

10
00:00:35,740 --> 00:00:37,780
Aus buddhistischer Sicht schafft nichts den Geist. 

11
00:00:38,100 --> 00:00:40,540
Weil der Geist ohne Geburt ist. 

12
00:00:40,880 --> 00:00:46,780
Es ist ohne Grund oder Verschwinden. 

13
00:00:47,060 --> 00:00:50,020
Der Geist wird erschaffen, aber nicht erschaffen. 

14
00:00:50,500 --> 00:00:54,220
Ist der Geist die erste Ursache? 

15
00:00:54,460 --> 00:00:57,820
Es kann keine erste Ursache geben. 

16
00:00:58,100 --> 00:00:59,940
Weil der Ursprung rein ist. 

17
00:01:02,280 --> 00:01:05,420
Was wäre die erste Ursache gewesen? 

18
00:01:05,840 --> 00:01:11,200
Wir verstehen sogar intellektuell, dass wir, wenn wir in die Vergangenheit gehen, die erste Ursache nicht finden können. 

19
00:01:11,660 --> 00:01:13,300
Die erste Ursache existiert also nicht. 

20
00:01:13,660 --> 00:01:15,640
Die erste Ursache ist die Wirkung. 

21
00:01:16,060 --> 00:01:16,900
Jetzt. 

22
00:01:17,440 --> 00:01:20,940
Wo wohnt der Geist? 

23
00:01:21,280 --> 00:01:23,120
Der Geist existiert immer und wohnt überall. 

24
00:01:23,420 --> 00:01:25,200
Ob wir in Zazen oder anderswo sind. 

25
00:01:25,400 --> 00:01:30,140
Es gibt keine spezielle Meditation, die die Struktur des Geistes verändert. 

26
00:01:30,460 --> 00:01:32,240
Der Geist ist immer präsent. 

27
00:01:32,520 --> 00:01:35,780
Was ist "Geist-Körper"? 

28
00:01:36,520 --> 00:01:38,920
Materie und Energie sind nicht voneinander getrennt. 

29
00:01:39,280 --> 00:01:44,600
Sie sind zwei verschiedene Ausdrücke derselben Sache. 

30
00:01:47,080 --> 00:01:50,360
Körper und Geist sind also zwei verschiedene Ausdrücke derselben Sache. 

31
00:01:50,760 --> 00:01:52,280
Sie sollten den Körper nicht verunglimpfen. 

32
00:01:52,640 --> 00:01:53,980
Der Körper ist der Geist. 

33
00:01:54,340 --> 00:01:55,720
Materie ist der Geist. 

34
00:01:56,220 --> 00:02:00,520
Wie wirkt sich der Gedanke auf den geistigen Körper aus? 

35
00:02:01,240 --> 00:02:06,120
Gedanken materialisieren sich in der Realität. 

36
00:02:07,020 --> 00:02:09,100
Im Traum von Materie. 

37
00:02:09,580 --> 00:02:11,040
Träumen passiert nicht nur im Schlaf. 

38
00:02:11,480 --> 00:02:15,580
Der Traum von Materie ist das, was wir "Realität" nennen. 

39
00:02:15,900 --> 00:02:17,880
Materielle Realität. 

40
00:02:18,400 --> 00:02:21,980
Was können wir daraus schließen? 

41
00:02:22,340 --> 00:02:27,080
Der Geist sieht immer im Geist. 

