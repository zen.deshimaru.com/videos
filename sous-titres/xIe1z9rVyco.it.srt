1
00:00:00,180 --> 00:00:04,420
Ho iniziato a praticare lo Zen a 16 anni. 

2
00:00:04,740 --> 00:00:07,660
Sto compiendo 49 anni. 

3
00:00:07,860 --> 00:00:14,360
Avevo provato a prendere la posizione di zazen da solo sul mio letto. 

4
00:00:14,960 --> 00:00:19,460
L’ho tenuto per 2, 3, 4 minuti, è stato davvero doloroso. 

5
00:00:19,700 --> 00:00:21,700
Non potrei farlo affatto. 

6
00:00:21,840 --> 00:00:30,800
Lì mi hanno mostrato la posa e ho tenuto il normale zazen per circa mezz’ora. 

7
00:00:31,120 --> 00:00:33,420
Ci stiamo preparando per partire, e qui: 

8
00:00:33,640 --> 00:00:36,100
"Ah, non dimenticare di pagare!" 

9
00:00:36,540 --> 00:00:38,500
Pensavo fosse libero … 

10
00:00:38,880 --> 00:00:42,580
Sono partito senza il mio assegno … 

11
00:00:45,460 --> 00:00:48,680
Mi esercito nel Dojo Zen di Lione, a Croix-Rousse. 

12
00:00:49,280 --> 00:00:56,940
È un vecchio dojo realizzato dal Maestro Deshimaru. 

13
00:00:59,380 --> 00:01:02,340
Mi chiamo Christophe Desmurs. 

14
00:01:02,700 --> 00:01:04,800
Il mio nome monaco è Ryurin. 

15
00:01:05,200 --> 00:01:09,700
Sono stato discepolo del Maestro Kosen per 25 anni. 

16
00:01:10,000 --> 00:01:13,000
Mi ci è voluto un po ’per incontrare il Maestro Kosen. 

17
00:01:13,320 --> 00:01:17,480
Lo conoscevo, facevo sesshins con lui … 

18
00:01:17,760 --> 00:01:21,000
Ma non ero molto vicino. 

19
00:01:21,340 --> 00:01:26,980
Mi sono avvicinato a causa di circostanze al di fuori del mio controllo. 

20
00:01:27,260 --> 00:01:31,800
E sono stato in grado di scoprire il suo insegnamento. 

21
00:01:32,160 --> 00:01:36,120
Quindi ho praticato lo zazen con lui fino ad oggi. 

22
00:01:36,280 --> 00:01:39,800
Il Maestro Kosen mi ha dato circa dieci anni fa, 

23
00:01:40,060 --> 00:01:42,920
come ha fatto con molti dei suoi ex seguaci, 

24
00:01:43,260 --> 00:01:46,900
lo shiho, la trasmissione del Dharma. 

25
00:01:47,740 --> 00:01:50,740
Maestro Zen, è un samu, non è un titolo. 

26
00:01:51,020 --> 00:01:57,420
Come diciamo "maestro", suggerisce l’idea che uno ha imparato qualcosa, ma è un samu. 

27
00:01:58,240 --> 00:02:01,480
"Samu" è una parola cinese, giapponese. 

28
00:02:01,740 --> 00:02:03,840
Questi sono i compiti da svolgere. 

29
00:02:04,100 --> 00:02:06,380
Insegna anche, è un samu. 

30
00:02:06,560 --> 00:02:08,720
Il maestro Zen fa il suo samu … 

31
00:02:09,000 --> 00:02:11,020
Quando ha finito con il suo samurai, torna a casa. 

32
00:02:11,300 --> 00:02:12,980
Andrà a lavorare o no … 

33
00:02:13,280 --> 00:02:15,560
Ha una moglie e una famiglia o no … 

34
00:02:15,980 --> 00:02:17,240
Dipende da ogni persona … 

35
00:02:18,060 --> 00:02:21,180
E ora sta lavorando a qualcos’altro … 

36
00:02:21,500 --> 00:02:23,120
Questa è la vita! 

37
00:02:23,640 --> 00:02:26,660
Ma il maestro Zen, è un discepolo. 

38
00:02:27,060 --> 00:02:35,860
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

