1
00:00:00,280 --> 00:00:02,560
Ik weet niet veel over de sofrologie.

2
00:00:02,800 --> 00:00:12,140
Ik weet dat het een toepassing is van bepaalde praktijken van wellness, van persoonlijke ontwikkeling,

3
00:00:12,540 --> 00:00:17,160
en er is een sophrologie die "oosterse" wordt genoemd,

4
00:00:17,440 --> 00:00:24,080
die zeer geïnspireerd is door Yogische, boeddhistische of indiaanse praktijken.

5
00:00:24,540 --> 00:00:27,940
Zen is heel eenvoudig.

6
00:00:28,380 --> 00:00:38,400
Het is gebaseerd op fundamentele menselijke houdingen.

7
00:00:39,820 --> 00:00:48,480
Ik ga je de eerste fundamentele houding van Zen laten zien.

8
00:01:01,500 --> 00:01:06,120
Mensen waren vroeger apen.

9
00:01:06,460 --> 00:01:08,580
Zo hebben ze gelopen.

10
00:01:09,200 --> 00:01:20,360
Beetje bij beetje gingen ze rechtop zitten, zoals Zen-meesters.

11
00:01:22,900 --> 00:01:24,900
Toen zijn ze rechtgezet.

12
00:01:25,180 --> 00:01:28,600
Zo hebben ze gelopen.

13
00:01:29,680 --> 00:01:33,760
En het menselijk bewustzijn is geëvolueerd

14
00:01:35,180 --> 00:01:39,980
samen met de houding.

15
00:01:40,340 --> 00:01:46,000
De houding van het lichaam is een fundamentele gedachte.

16
00:01:46,340 --> 00:01:52,200
Bijvoorbeeld Usain Bolt, de snelste loper ter wereld,

17
00:01:52,540 --> 00:01:55,160
maakte dat gebaar.

18
00:01:55,440 --> 00:02:02,300
Iedereen begrijpt wat het betekent, en toch zijn er geen woorden.

19
00:02:02,640 --> 00:02:05,440
Het is gewoon een houding.

20
00:02:05,900 --> 00:02:11,120
De manier waarop we zitten en lopen,

21
00:02:11,520 --> 00:02:13,400
wat je met je handen doet,

22
00:02:13,700 --> 00:02:16,500
het is allemaal fundamenteel belangrijk.

23
00:02:18,160 --> 00:02:21,440
In zen is er de Boeddha-houding.

24
00:02:22,980 --> 00:02:31,120
Het bestond al voor de Boeddha, het wordt genoemd in de Bhagavad-Gita.

25
00:02:32,400 --> 00:02:39,880
Je zit op een kussen van gras en buigt je benen.

26
00:02:41,560 --> 00:02:47,140
In het wild, communiceerde de prehistorische mens

27
00:02:47,440 --> 00:02:53,220
met de oren, met de neus, met de ogen, om ver te zien.

28
00:02:54,920 --> 00:02:59,000
Dus ze wilden het recht zetten.

29
00:02:59,400 --> 00:03:04,220
En toen ze rechtop kwamen te staan, werd hun bewustzijn verscherpt, ontwaakt.

30
00:03:06,040 --> 00:03:08,720
Dit is de eerste houding.

31
00:03:08,980 --> 00:03:11,580
Normaal gesproken moeten we een volledige lotus doen.

32
00:03:12,060 --> 00:03:14,260
Dat is hoe je je benen kruist.

33
00:03:14,700 --> 00:03:18,460
Het heet de "pretzel."

34
00:03:24,960 --> 00:03:30,980
Het is erg belangrijk om deze houding aan te nemen.

35
00:03:31,380 --> 00:03:37,380
De vorige spreker sprak over dynamiek en ontspanning.

36
00:03:37,780 --> 00:03:42,340
We hebben een sympathiek en parasympatisch zenuwstelsel.

37
00:03:42,600 --> 00:03:45,800
We hebben de buitenste spieren en de diepe spieren.

38
00:03:46,320 --> 00:03:51,400
We hebben een oppervlakkige en diepe ademhaling.

39
00:03:51,820 --> 00:03:54,620
We hebben een oppervlakkig, corticaal bewustzijn,

40
00:03:54,920 --> 00:03:59,760
en diep bewustzijn, de reptielachtige of middenhersenen.

41
00:04:00,340 --> 00:04:04,160
Om rechtop te staan in deze houding,

42
00:04:04,440 --> 00:04:06,800
je moet de aarde met je knieën duwen.

43
00:04:10,240 --> 00:04:15,640
Het vergt een inspanning om dynamisch te zijn en een goede houding aan te nemen.

44
00:04:16,000 --> 00:04:22,180
Mijn meester [Deshimaru] drong aan op de schoonheid en de kracht van de houding.

45
00:04:22,460 --> 00:04:31,280
Maar in lotus, hoe meer je ontspant, hoe meer je voeten op je dijen groeien,

46
00:04:31,520 --> 00:04:35,100
en hoe meer de knieën op de grond groeien.

47
00:04:35,620 --> 00:04:40,280
Hoe meer je je ontspant, hoe dynamischer je bent, hoe rechter en sterker je bent.

48
00:04:40,560 --> 00:04:46,560
Dit heeft niets te maken met ontspannen in een stoel of bed.

49
00:04:46,940 --> 00:04:57,280
Het is een manier om het lichaam aan zichzelf over te laten,

50
00:05:04,380 --> 00:05:09,700
terwijl je klaarwakker bent.

51
00:05:10,120 --> 00:05:16,240
Dit is de eerste houding, de zithouding van de Boeddha.

52
00:05:16,680 --> 00:05:20,280
Handen zijn ook erg belangrijk.

53
00:05:20,640 --> 00:05:23,600
Je doet altijd iets met je handen:

54
00:05:23,860 --> 00:05:26,560
werken, jezelf beschermen...

55
00:05:26,960 --> 00:05:28,740
Handen zijn zeer expressief

56
00:05:29,040 --> 00:05:31,040
en zijn verbonden met de hersenen.

57
00:05:31,500 --> 00:05:41,040
Het nemen van deze mudra met je handen verandert het bewustzijn.

58
00:05:41,680 --> 00:05:47,680
De concentratie ligt in onze handen, lichaam, zenuwstelsel en hersenen.

59
00:05:48,100 --> 00:05:53,360
Een ander belangrijk ding is de gedachte, het bewustzijn.

60
00:05:53,820 --> 00:05:57,680
Als je net als Rodin’s Denker bent,

61
00:05:58,000 --> 00:06:01,100
je hebt helemaal niet hetzelfde geweten.

62
00:06:09,960 --> 00:06:12,240
dat wanneer je houding perfect recht is,

63
00:06:12,580 --> 00:06:14,640
vooral met je nek uitgestrekt,

64
00:06:15,700 --> 00:06:18,400
met zijn hoofd recht omhoog, open naar de hemel,

65
00:06:18,760 --> 00:06:22,020
neus en navel uitgelijnd

66
00:06:25,060 --> 00:06:27,220
en je ademt met je hele wezen.

67
00:06:29,020 --> 00:06:31,860
De gedachte is dan niet meer losgekoppeld van het lichaam.

68
00:06:32,240 --> 00:06:36,100
Er is geen scheiding meer tussen ons bewustzijn...

69
00:06:36,300 --> 00:06:39,080
en elke cel in ons lichaam.

70
00:06:39,500 --> 00:06:45,520
In Zen noemen we dit "geest-lichaam", in één woord.

71
00:06:46,360 --> 00:06:50,540
Dit is de fundamentele houding: zazen.

72
00:06:56,380 --> 00:07:00,200
De tweede houding is de loophouding.

73
00:07:00,680 --> 00:07:06,340
In zazen laten we alles in de steek en bewegen we helemaal niet.

74
00:07:06,560 --> 00:07:08,280
Dus, het bewustzijn is speciaal.

75
00:07:08,720 --> 00:07:13,800
Maar er is nog een andere meditatie die wordt gedaan tijdens het lopen.

76
00:07:16,180 --> 00:07:18,160
We moeten verhuizen.

77
00:07:18,540 --> 00:07:20,540
We doen iets, we gaan verder.

78
00:07:21,180 --> 00:07:24,180
Je gaat niet vooruit om iets te vangen.

79
00:07:24,580 --> 00:07:28,380
Je gaat je ademhaling op gang brengen met het lopen.

80
00:07:29,020 --> 00:07:30,740
De choreograaf Maurice Béjart,

81
00:07:31,040 --> 00:07:33,560
die een discipel was van mijn meester, Deshimaru,

82
00:07:34,060 --> 00:07:38,040
liet zijn dansers dat elke dag oefenen.

83
00:07:39,060 --> 00:07:45,340
De tweede houding is de "kin-hin" wandeling.

84
00:07:45,880 --> 00:07:51,040
Let op wat je met je handen doet.

85
00:07:51,680 --> 00:07:57,840
Een bokser die op de foto gaat, doet dat met zijn handen.

86
00:07:58,240 --> 00:08:05,220
Als je je handen zo legt, geven de hersenen automatisch blijk van een sterke agressiviteit.

87
00:08:05,820 --> 00:08:10,360
Je kunt je handen in je zakken steken.

88
00:08:10,840 --> 00:08:13,140
"Waar heb ik mijn portemonnee gelegd? »

89
00:08:13,640 --> 00:08:16,360
"Iemand heeft mijn mobiele telefoon gestolen! »

90
00:08:16,700 --> 00:08:19,580
We kunnen onze vingers in onze neus steken.

91
00:08:21,660 --> 00:08:25,820
Alles wat hier gebouwd is, is met de hand gebouwd.

92
00:08:26,260 --> 00:08:28,820
Het is buitengewoon, de hand.

93
00:08:29,160 --> 00:08:31,860
We zijn de enige dieren met handen,

94
00:08:32,260 --> 00:08:35,340
ze zijn gerelateerd aan onze evolutie.

95
00:08:37,700 --> 00:08:40,660
De handen mediteren ook.

96
00:08:40,940 --> 00:08:43,580
Je doet je duim in je vuist.

97
00:08:43,820 --> 00:08:49,420
Plaats de duimwortel onder het borstbeen, leg de andere hand erop.

98
00:08:49,760 --> 00:08:55,060
Deze houdingsdetails bestaan al sinds de Boeddha.

99
00:08:55,380 --> 00:09:02,520
In oude teksten wordt uitgelegd dat ze vanuit India naar China zijn gestuurd.

100
00:09:02,960 --> 00:09:08,920
Het is een mondelinge overdracht van persoon tot persoon.

101
00:09:09,480 --> 00:09:12,500
In het boeddhisme zijn er veel aspecten.

102
00:09:12,780 --> 00:09:17,320
Maar het meditatieve en posturale aspect is zeer belangrijk.

103
00:09:17,820 --> 00:09:28,540
Er waren twee grote discipelen van de Boeddha, Śāriputra en Mokuren (Maudgalyayana).

104
00:09:28,940 --> 00:09:36,840
Hun spirituele meester was niet zeker:

105
00:09:37,220 --> 00:09:43,680
"Ik weet niet zeker of ik de truc heb om uit leven en dood te komen. »

106
00:09:43,940 --> 00:09:52,860
"Als je een meester vindt die dit leert, laat het me weten, en ik ga met je mee. »

107
00:09:53,620 --> 00:10:04,320
Op een dag zien ze een monnik, zoals ik, zie je, de weg aflopen.

108
00:10:04,780 --> 00:10:10,000
Ze staan versteld van de schoonheid van deze man.

109
00:10:10,420 --> 00:10:16,200
Dan stopt deze monnik in een bosje om te plassen.

110
00:10:24,440 --> 00:10:33,240
Śāriputra, dat beschouwd wordt als een van de grootste intelligenties van India in die tijd, vertelt hem:

111
00:10:34,180 --> 00:10:43,940
"We staan versteld van uw aanwezigheid. Wie is uw meester? »

112
00:10:46,560 --> 00:10:49,540
- "Mijn meester is Shakyamuni Boeddha."

113
00:10:49,860 --> 00:10:53,520
- "Leg ons je doctrine uit! »

114
00:10:53,780 --> 00:10:58,220
- "Ik kan het je niet uitleggen, ik ben hier te nieuw in. »

115
00:10:59,100 --> 00:11:04,440
En ze zijn deze jongen alleen maar gevolgd vanwege de manier waarop hij loopt.

116
00:11:04,800 --> 00:11:06,980
De manier waarop hij iets uit haar krijgt.

117
00:11:07,440 --> 00:11:11,680
Dus de tweede houding is lopen.

118
00:11:12,020 --> 00:11:15,280
Als mensen zazen doen, is het erg sterk.

119
00:11:15,500 --> 00:11:21,580
Zelfs vechtkunstbeoefenaars zijn onder de indruk van de kracht die dit uitstraalt.

120
00:11:21,940 --> 00:11:25,860
Door de kracht van de adem.

121
00:11:26,240 --> 00:11:35,520
Maar hoe vind je deze energie, deze volheid, in het dagelijks leven?

122
00:11:35,840 --> 00:11:39,220
Die kalmte, die concentratie.

123
00:11:39,600 --> 00:11:42,960
Het is een moeilijk punt.

124
00:11:43,280 --> 00:11:47,140
Omdat de houding van zazen uiterst nauwkeurig is.

125
00:11:47,600 --> 00:11:55,100
In het begin richten we ons op het technische aspect van de kin-hin-wandeling.

126
00:11:55,540 --> 00:11:58,800
Maar zodra we alle details binnen hebben,

127
00:11:59,300 --> 00:12:02,160
op elk moment in ons dagelijks leven

128
00:12:02,440 --> 00:12:08,400
(zelfs als ik het altijd vergeet te doen), kunnen we het oefenen.

129
00:12:10,600 --> 00:12:16,740
Je kunt je heroriënteren op je spirituele wezen.

130
00:12:17,740 --> 00:12:22,760
Op je wezen doet dat geen reet.

131
00:12:23,320 --> 00:12:31,340
Ik heb je over de handen verteld.

132
00:12:31,840 --> 00:12:39,980
De fundamentele religieuze houding is deze.

133
00:12:40,560 --> 00:12:44,240
Als je je handen zo bij elkaar hebt,

134
00:12:45,080 --> 00:12:48,260
Ik doe dit gewoon,

135
00:12:48,580 --> 00:12:51,280
het heeft een effect op de hersenen,

136
00:12:51,620 --> 00:12:55,340
het bouwt een brug tussen de twee hersenhelften.

137
00:12:55,740 --> 00:12:59,960
Deze houding wordt in alle religies gebruikt.

138
00:13:00,280 --> 00:13:02,180
Het is heel eenvoudig.

139
00:13:02,560 --> 00:13:05,640
Zie het belang van handen!

140
00:13:06,060 --> 00:13:13,280
De derde houding...

141
00:13:13,860 --> 00:13:16,520
(Ik dacht dat er vier waren...)

142
00:13:16,880 --> 00:13:20,580
Oh, ja, er is de buikligging, maar daar gaan we het niet over hebben.

143
00:13:21,000 --> 00:13:25,520
De liggende houding, hoe te slapen.

144
00:13:26,700 --> 00:13:31,980
Ik heb je uitgelegd hoe prehistorische mannen vroeger op handen en voeten rondkruipen,

145
00:13:32,260 --> 00:13:34,440
beetje bij beetje, ze rechtgetrokken,

146
00:13:34,700 --> 00:13:38,760
en dat onze evolutie voortkomt uit onze houding,

147
00:13:39,060 --> 00:13:42,580
van de irrigatie van de ruggengraat die naar de hersenen gaat,

148
00:13:42,940 --> 00:13:46,400
en het brein, dat een buitengewoon instrument is,

149
00:13:46,700 --> 00:13:51,440
waarvan slechts 10-15% van het totale nut bekend is.

150
00:13:51,760 --> 00:13:57,740
We hebben een groot ding tussen onze oren dat we onderbenut zijn.

151
00:14:08,640 --> 00:14:12,380
Iedereen heeft zijn eigen brein!

152
00:14:19,020 --> 00:14:22,140
Iedereen heeft zijn eigen brein!

153
00:14:22,400 --> 00:14:25,200
Zoals een IP-adres.

154
00:14:30,220 --> 00:14:33,220
Iedereen heeft zijn eigen brein!

155
00:14:33,940 --> 00:14:35,900
En we zijn alleen!

156
00:14:36,700 --> 00:14:41,940
Het is erg belangrijk voor wat ik je ga vertellen.

157
00:14:44,760 --> 00:14:50,040
Onze voeten. We gaan terug naar onze voeten...

158
00:14:51,720 --> 00:14:56,420
Je weet dat we op een kleine Aarde leven.

159
00:14:57,040 --> 00:15:01,420
Heb je de film Gravity gezien?

160
00:15:02,120 --> 00:15:07,060
Het is een van de films die het menselijk bewustzijn echt doen ontwaken.

161
00:15:07,560 --> 00:15:13,380
In Hollywood doen ze soms wat manipulatief werk.

162
00:15:13,680 --> 00:15:17,220
Maar soms ook van informatie en spirituele evolutie.

163
00:15:17,760 --> 00:15:19,660
Deze film is er één van.

164
00:15:20,040 --> 00:15:26,920
En je kunt zelf zien hoe fragiel onze aarde is.

165
00:15:29,200 --> 00:15:33,400
Meestal, als we naar de hemel kijken, razen we: "Het is oneindig blauw! ».

166
00:15:33,760 --> 00:15:38,940
Nee. Niet voor altijd. 30 kilometer atmosfeer.

167
00:15:39,360 --> 00:15:44,660
Het einde van de atmosfeer ligt tussen 30 en 1.000 kilometer.

168
00:15:52,260 --> 00:15:56,700
Zelfs duizend mijl is niets.

169
00:15:58,800 --> 00:16:04,780
Als ik je 20 mijl verderop zou zetten, zou je het geen minuut overleven.

170
00:16:05,380 --> 00:16:15,940
Zelfs afgezien van de belastingen zijn onze levensomstandigheden precair.

171
00:16:17,420 --> 00:16:20,440
De aarde is een klein, kwetsbaar, levend wezen.

172
00:16:21,120 --> 00:16:30,660
Onze biosfeer is klein en kwetsbaar, het moet bewaard blijven.

173
00:16:30,940 --> 00:16:35,100
We moeten ons altijd zorgen maken over het behoud van dit ding.

174
00:16:36,860 --> 00:16:39,940
Omdat we uit de aarde zijn geboren.

175
00:16:40,220 --> 00:16:45,700
Dit wonder van leven dat zich op aarde heeft voltrokken is efemeer.

176
00:16:46,240 --> 00:16:47,840
We zijn uit de aarde geboren.

177
00:16:48,220 --> 00:16:53,320
En we hebben allemaal onze voeten die naar hetzelfde centrum wijzen, het centrum van de Aarde.

178
00:16:53,900 --> 00:16:57,760
Dus onze voeten staan in de gemeenschap.

179
00:17:01,860 --> 00:17:08,680
Maar ons hoofd is uniek, niemand wijst in dezelfde richting.

180
00:17:18,400 --> 00:17:23,500
Vandaar de derde houding die ik je ga laten zien.

181
00:17:23,800 --> 00:17:28,140
Het is gemeenschappelijk voor veel religies, het heet "prostratie".

182
00:17:28,380 --> 00:17:31,240
Zo doen we dat.

183
00:17:40,640 --> 00:17:42,780
Ah, dat voelt goed!

184
00:17:43,060 --> 00:17:49,940
Als we dit doen, is het net als wanneer we de liefde bedrijven:

185
00:17:50,260 --> 00:17:58,460
soms hebben we 10 jaar, 20 jaar, 6 maanden geen seks...

186
00:17:58,780 --> 00:18:07,460
en als we weer vrijen, zeggen we: "Ah, dat voelt goed, ik ben vergeten hoe goed het was! ».

187
00:18:08,220 --> 00:18:12,200
Als je die prostratie doet, is het hetzelfde.

188
00:18:13,460 --> 00:18:15,740
Ik maak geen grapje.

189
00:18:16,240 --> 00:18:20,260
We zijn onze hersenen aan het hervormen.

190
00:18:21,060 --> 00:18:26,280
Als je je hersenen in lijn brengt met het centrum van de Aarde.

191
00:18:26,620 --> 00:18:29,080
Het gaat niet alleen om nederigheid,

192
00:18:29,360 --> 00:18:32,720
of buigen voor standbeelden of wat dan ook.

193
00:18:34,600 --> 00:18:38,880
Het is een magisch, fundamenteel, sjamanistisch ding, misschien...

194
00:18:39,320 --> 00:18:41,860
Vanaf het moment dat je je prostaat...

195
00:18:42,180 --> 00:18:43,340
Mijn meester zei altijd:

196
00:18:43,660 --> 00:18:47,440
"Als de staatshoofden zich één keer per dag zouden prostitueren,"

197
00:18:47,800 --> 00:18:50,200
"het zou de wereld veranderen. »

198
00:18:50,640 --> 00:18:54,560
Het is geen jibber-jabber of mystiek.

199
00:18:55,280 --> 00:18:58,780
Het moet wetenschappelijk meetbaar zijn.

200
00:19:00,780 --> 00:19:05,280
Het hervormt de hersenen, om ze aan te raken.

201
00:19:05,740 --> 00:19:09,280
Hij zit constant in zijn eigen lijn.

202
00:19:09,620 --> 00:19:17,300
En daar maak je verbinding met je wortel, met je broers, met het centrum van de Aarde.

203
00:19:19,940 --> 00:19:25,380
Soms doe ik het en voel ik iets sterks in mijn hoofd.

204
00:19:25,660 --> 00:19:27,540
Het voelt zo goed.

205
00:19:27,820 --> 00:19:39,420
Tegelijkertijd relativeert het het kwaad dat we onbedoeld om ons heen doen.

206
00:19:40,560 --> 00:19:42,960
Het is een goed medicijn.

207
00:19:43,180 --> 00:19:47,040
Al deze houdingen zijn fundamentele geneesmiddelen.

208
00:19:47,760 --> 00:19:53,260
Ik had een wat intellectuele tekst voorbereid.

209
00:19:53,520 --> 00:19:58,140
Maar ik kan niet van het ene naar het andere onderwerp gaan.

210
00:19:58,440 --> 00:20:02,300
Wat was ik aan het zeggen?

211
00:20:06,860 --> 00:20:12,040
Ik ga terug naar het lichaam en de prehistorie.

212
00:20:12,780 --> 00:20:28,200
Het is bewezen dat Neanderthalers een religieuze praktijk hadden.

213
00:20:30,680 --> 00:20:36,340
We hebben wat graven gevonden.

214
00:20:36,860 --> 00:20:40,440
Ze brachten offers aan hun doden.

215
00:20:40,760 --> 00:20:43,680
Ze voerden vroeger een religieuze ceremonie uit voor hun doden.

216
00:20:48,420 --> 00:20:52,180
Wat interessant is, is niet alleen dat!

217
00:20:52,720 --> 00:20:56,260
Toen ze vroeger deze religieuze ceremonies deden...

218
00:20:56,740 --> 00:21:01,700
- religieus in de zin dat ze communiceerden met het onzichtbare...

219
00:21:02,540 --> 00:21:06,940
- met het eeuwige leven, met de doden...

220
00:21:08,800 --> 00:21:11,020
- ze waren verbonden met het onzichtbare.

221
00:21:11,420 --> 00:21:18,140
Op dat moment bestond er echter nog geen gearticuleerde taal.

222
00:21:18,560 --> 00:21:24,380
Dus, religie bestond al voor gearticuleerde taal.

223
00:21:24,840 --> 00:21:30,140
(Ik ben bereid om over te gaan op gearticuleerde taal...)

224
00:21:31,480 --> 00:21:36,500
(Het is erg interessant, het kostte me vier dagen om het te schrijven...)

225
00:21:43,820 --> 00:21:45,660
Mijn meester zei altijd:

226
00:21:45,920 --> 00:21:48,880
"Zazen is de religie voor de religie. »

227
00:21:49,380 --> 00:21:54,380
We zouden zeggen: "Ja, je wilt altijd de eerste zijn..."

228
00:21:55,860 --> 00:21:59,700
Maar dat is de religie voor religie zoals we die kennen.

229
00:21:59,960 --> 00:22:02,720
Waar wordt verwezen naar boeken.

230
00:22:03,080 --> 00:22:06,280
Boeken komen na de religie, niet eerder.

231
00:22:06,720 --> 00:22:11,940
Vroeger kon de man niet eens spreken.

232
00:22:12,360 --> 00:22:15,060
Dus hij was al in die houding.

233
00:22:15,480 --> 00:22:19,620
Dit zijn prehistorische houdingen.

234
00:22:21,460 --> 00:22:23,740
(Hoeveel tijd heb ik nog?)

235
00:22:23,960 --> 00:22:28,320
(Ik heb nog maar 3 minuten? Oké, dan!)

236
00:22:31,020 --> 00:22:34,460
Dus, over de sofrologie...

237
00:22:36,500 --> 00:22:39,280
Ik wilde met je praten over de traditionele zazen.

238
00:22:39,740 --> 00:22:43,660
Ik ben gekleed als een traditionele monnik.

239
00:22:47,440 --> 00:22:51,700
De traditionele zazen wordt niet alleen beoefend.

240
00:22:52,100 --> 00:22:54,500
Het is geen wellness techniek.

241
00:22:58,260 --> 00:23:05,060
Ik zal je voorlezen wat de Boeddha zei, ik vind het zo geweldig!

242
00:23:08,520 --> 00:23:12,780
Shakyamuni Boeddha zei tegen zijn publiek:

243
00:23:13,120 --> 00:23:18,540
"De studie van de soetra’s en de leer" - teksten -

244
00:23:18,900 --> 00:23:28,260
De "naleving van de leefregels" - in godsdiensten is dit belangrijk, niet om te doden, niet om te stelen, etc. - is een zeer belangrijk onderdeel van de "naleving van de leefregels". -

245
00:23:29,300 --> 00:23:34,960
"en zelfs het beoefenen van Zazen, zittende meditatie."

246
00:23:35,900 --> 00:23:43,060
"zijn niet in staat om menselijke verlangens, angsten en angsten uit te roeien. »

247
00:23:43,340 --> 00:23:45,080
Dat is wat Shakyamuni Boeddha zei.

248
00:23:45,340 --> 00:23:51,660
Dus ik ben een fanaat, en hij is een lastpak...

249
00:23:52,080 --> 00:23:56,800
Zazen, zelfs de Boeddha zegt dat het nutteloos is!

250
00:23:57,600 --> 00:24:01,680
Dus, wat gaan we er aan doen?

251
00:24:14,440 --> 00:24:17,480
Natuurlijk, er is een suite...

252
00:24:23,360 --> 00:24:29,360
Mijn meester [Deshimaru], toen hij zijn meester [Kodo Sawaki] ontmoette,

253
00:24:29,820 --> 00:24:34,280
Deze laatste zei in een conferentie "Zazen is nutteloos, het brengt geen verdienste met zich mee! ».

254
00:24:34,680 --> 00:24:38,800
Mijn meester dacht: "Maar het is echt interessant als het nutteloos is! »

255
00:24:39,140 --> 00:24:41,940
"We doen altijd dingen die goed zijn voor iets! »

256
00:24:42,280 --> 00:24:43,760
"Iets dat nutteloos is? »

257
00:24:44,120 --> 00:24:47,400
"Ik wil gaan, ik wil het doen! »

