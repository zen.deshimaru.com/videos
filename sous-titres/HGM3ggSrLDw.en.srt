1
00:00:03,300 --> 00:00:07,060
What is the time? 

2
00:00:07,240 --> 00:00:11,780
The time is only now. 

3
00:00:12,080 --> 00:00:15,080
Zen is "here and now." 

4
00:00:15,340 --> 00:00:16,820
It is important. 

5
00:00:17,220 --> 00:00:25,160
But understanding the past is an extraordinary trick to understanding the present. 

6
00:00:25,580 --> 00:00:31,640
And understanding the future is also very important. 

7
00:00:32,460 --> 00:00:34,780
How do you go through time? 

8
00:00:35,360 --> 00:00:38,400
Time is a matter of frequency. 

9
00:00:38,700 --> 00:00:43,640
The higher the frequency, the faster the time, that’s all. 

10
00:00:44,100 --> 00:00:49,760
Consciousness also evolves according to the frequencies. 

11
00:00:59,700 --> 00:01:03,440
Expensive is only a material impression. 

12
00:01:03,820 --> 00:01:06,000
It is a delight. 

13
00:01:06,400 --> 00:01:09,100
What about zazen? 

14
00:01:09,400 --> 00:01:16,700
For a lover of zazen, zazen time is very precious. 

15
00:01:17,120 --> 00:01:22,560
We get to the dojo, and we only have an hour and a half, up to two hours. 

16
00:01:23,020 --> 00:01:27,720
To be able to live in this dimension. 

17
00:01:28,000 --> 00:01:31,520
And put things in their place. 

18
00:01:31,780 --> 00:01:35,440
You have to save your zazen time. 

19
00:01:35,540 --> 00:01:38,760
It takes a while to get to the bottom of it. 

20
00:01:39,000 --> 00:01:40,760
And to be in good posture. 

21
00:01:41,060 --> 00:01:43,960
Is time related to karma? 

22
00:01:44,720 --> 00:01:46,920
The past only now exists. 

23
00:01:47,280 --> 00:01:57,240
Each "now" corresponds to a specific frequency past tense with your "now". 

24
00:01:57,620 --> 00:02:00,900
Your past is constantly changing. 

25
00:02:01,000 --> 00:02:10,700
But when you go to your past, you forget your present. 

26
00:02:11,040 --> 00:02:13,560
And your present becomes the past. 

27
00:02:13,900 --> 00:02:17,720
And you go into something that gets more and more complicated. 

28
00:02:17,940 --> 00:02:19,340
In karma. 

29
00:02:19,820 --> 00:02:24,160
"It’s my karma’s fault …" 

30
00:02:25,240 --> 00:02:27,960
"My parents, my stuff …" 

31
00:02:28,280 --> 00:02:30,440
"My life is complicated …" 

32
00:02:30,700 --> 00:02:32,380
"My ancestors were alcoholics …" 

33
00:02:32,660 --> 00:02:35,420
Etc. And you get lost in there. 

34
00:02:35,780 --> 00:02:40,060
You identify with this character. 

35
00:02:40,340 --> 00:02:41,720
You are getting weaker. 

36
00:02:42,060 --> 00:02:44,140
You are no longer god. 

37
00:02:44,420 --> 00:02:45,720
You are confused. 

38
00:02:46,080 --> 00:02:47,760
You are lost in the materialization. 

39
00:02:48,060 --> 00:02:53,300
With our current state, there are always causes in the past. 

40
00:02:53,680 --> 00:02:58,540
If we change the current situation, the past will also change. 

41
00:02:59,160 --> 00:03:01,940
What about the future? 

42
00:03:02,340 --> 00:03:05,300
The future is a different story … 

