1
00:00:05,860 --> 00:00:08,580
Deshimaru, so che morirò presto.

2
00:00:08,820 --> 00:00:17,320
È giunto il momento di ordinarti come monaco di trasmettere l’insegnamento di Bodhidarma in una nuova terra.

3
00:00:31,020 --> 00:00:32,820
Voglio aiutare Dio.

4
00:00:33,100 --> 00:00:34,860
Voglio aiutare Cristo.

5
00:00:43,260 --> 00:00:48,000
Voglio aiutare il vero dio.

6
00:00:48,240 --> 00:00:52,760
Voglio tornare dal vero dio.

7
00:00:53,360 --> 00:01:00,360
40 anni dall’arrivo del Maestro Deshimaru in Europa.

8
00:01:00,800 --> 00:01:04,000
Dio è a terra.

9
00:01:04,320 --> 00:01:09,040
Dio è "affaticato", stanco.

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. Il Bodhidarma dei tempi moderni.

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru è arrivato a Parigi nell’estate del 1967 alla stazione ferroviaria di Gare du Nord.

12
00:01:31,120 --> 00:01:33,480
Solo un monaco, senza soldi, che non parla francese.

13
00:01:33,820 --> 00:01:36,600
Nel suo bagaglio, solo il seme fresco dello Zen.

14
00:01:45,440 --> 00:01:49,600
L’arrivo

15
00:02:59,140 --> 00:03:03,060
Se possiamo tornare alla storia dello Zen...

16
00:03:04,380 --> 00:03:09,520
Tutto è cominciato in India e poi: cosa è successo?

17
00:03:09,720 --> 00:03:13,340
Come è andata
dall’India al Giappone?

18
00:03:13,640 --> 00:03:19,020
India, meditazione molto profonda.

19
00:03:19,480 --> 00:03:22,340
Ma troppo tradizionale.

20
00:03:24,380 --> 00:03:29,980
Lo yoga non è finito in India.

21
00:03:30,720 --> 00:03:36,160
Alcune persone hanno un alto livello spirituale, ma la maggior parte non lo ha.

22
00:03:36,760 --> 00:03:43,440
Ma, yoga, finito. non abbiamo finito

23
00:03:43,760 --> 00:03:48,240
Ma la terra è troppo vecchia.

24
00:03:48,940 --> 00:03:53,660
Bodhidarma ha portato zazen dall’India in Cina.

25
00:03:53,900 --> 00:03:58,280
Ma in India non c’è più lo zen.

26
00:03:58,860 --> 00:04:04,880
Poi Dogen ha portato zazen dalla Cina in Giappone.

27
00:04:05,160 --> 00:04:11,360
Vuole dire che il suolo è troppo vecchio in Asia, Giappone, Cina o India?

28
00:04:11,740 --> 00:04:14,740
Sì, l’India è troppo vecchia.

29
00:04:14,960 --> 00:04:17,700
Anche la Cina è troppo vecchia.

30
00:04:18,280 --> 00:04:20,280
Lo Zen è finito in Cina.

31
00:04:20,700 --> 00:04:23,940
Il Giappone è troppo abituato allo zen.

32
00:04:24,680 --> 00:04:27,240
Si preoccupano solo delle cerimonie...

33
00:04:27,680 --> 00:04:30,380
A loro manca l’essenza dello zen.

34
00:04:36,160 --> 00:04:48,320
Pensi che lo spirito dello Zen tenda a declinare?

35
00:04:48,640 --> 00:04:50,620
Sì, sì, sì!

36
00:04:51,040 --> 00:04:58,720
Pensi che lo spirito dello Zen possa catturare in Francia, come il seme di un fiore?

37
00:04:59,040 --> 00:05:01,120
Sì!

38
00:05:03,020 --> 00:05:06,980
Inizia l’insegnamento.

39
00:06:24,680 --> 00:06:30,080
Perché ha scelto di stabilirsi in Francia?

40
00:06:30,400 --> 00:06:37,000
La Francia è meglio di un altro paese per capire lo zen.

41
00:06:37,520 --> 00:06:44,300
Molto facile da capire zazen.

42
00:06:45,120 --> 00:06:46,700
O filosofia.

43
00:06:46,980 --> 00:06:48,980
Per le idee.

44
00:06:49,300 --> 00:06:52,060
Molto profondo.

45
00:06:52,520 --> 00:06:57,980
Per esempio, Montaigne :

46
00:06:58,420 --> 00:07:03,900
"Non mi interessa tanto quello che sono per gli altri, quanto quello che sono per me stesso".

47
00:07:05,880 --> 00:07:09,640
Cartesio, Bergson...

48
00:07:10,140 --> 00:07:14,080
Portatori di mente, filosofi...

49
00:07:14,800 --> 00:07:16,840
Possono capire lo zen.

50
00:07:18,200 --> 00:07:25,740
Non conoscono lo zen, ma il loro pensiero è lo stesso...

51
00:07:26,460 --> 00:07:29,760
Perché non sei andato in America?

52
00:07:30,280 --> 00:07:33,840
All’inizio volevo portare lo Zen negli Stati Uniti.

53
00:07:34,220 --> 00:07:41,180
Ma vogliono usare lo zen per gli affari,
per l’industria, la psicologia, la psicoanalisi...

54
00:07:41,540 --> 00:07:44,180
Vogliono usarlo.

55
00:07:44,340 --> 00:07:49,020
Non è vero zen. Non è profondo.

56
00:07:52,500 --> 00:07:56,160
Appare un sangha.

57
00:09:08,270 --> 00:09:14,100
Prima, l’istruzione era buona, ma in tempi moderni, solo nella media.

58
00:09:14,100 --> 00:09:23,740
L’essenza è finita.

59
00:09:24,560 --> 00:09:33,700
Cerchiamo solo la media...

60
00:09:34,380 --> 00:09:41,600
Solo l’essenza è necessaria per una forte educazione.

61
00:09:43,380 --> 00:09:49,960
Sempre a destra o a sinistra...

62
00:09:51,040 --> 00:09:54,260
Ma dobbiamo abbracciare le contraddizioni.

63
00:09:54,600 --> 00:09:59,720
Solo abbandonando il nostro ego possiamo trovare la nostra vera individualità.

64
00:10:03,920 --> 00:10:13,100
La nostra più alta personalità in quando seguiamo
la verità cosmica, il cosmo.

65
00:10:13,500 --> 00:10:19,480
Possiamo trovare il nostro vero, forte ego.

66
00:10:20,580 --> 00:10:25,680
Volete rendere le persone forti?

67
00:10:26,540 --> 00:10:32,060
Sì, equilibrato e forte, a poco a poco.

68
00:10:32,800 --> 00:10:41,600
Dobbiamo trovare la vera austerità.

69
00:10:43,360 --> 00:10:47,600
Insegnamento

70
00:12:46,000 --> 00:12:57,680
L’essenza dello Zen è "Mushotoku", senza scopo. 

71
00:12:59,200 --> 00:13:05,520
Tutte le religioni hanno un oggetto. 

72
00:13:06,400 --> 00:13:10,560
Preghi di diventare ricco o felice. 

73
00:13:11,440 --> 00:13:17,040
Ma se abbiamo uno scopo, non è forte. 

74
00:13:18,400 --> 00:13:30,000
Non dobbiamo avere alcuno scopo. 

75
00:13:31,680 --> 00:13:38,000
Non puoi concentrarti. 

76
00:13:39,360 --> 00:13:47,680
Vuoi dire che una corretta realizzazione dovrebbe essere spontanea? 

77
00:13:48,480 --> 00:13:56,080
Dobbiamo tornare alle condizioni normali. 

78
00:13:57,040 --> 00:14:01,440
Vero Satori, coscienza trascendentale … 

79
00:14:02,560 --> 00:14:07,520
è di tornare alle condizioni normali. 

80
00:14:09,440 --> 00:14:18,080
Le persone sono anormali nei tempi moderni. 

81
00:14:19,120 --> 00:14:27,120
Solo il vero dio, ad esempio Cristo o Buddha, aveva condizioni normali per l’uomo. 

82
00:14:27,920 --> 00:14:33,900
La prima condizione necessaria è la propria determinazione? 

83
00:14:34,340 --> 00:14:39,500
E il secondo, abbandonare questa determinazione? 

84
00:14:39,760 --> 00:14:45,940
Sì. L’individualità è necessaria e dobbiamo abbandonarla. 

85
00:14:46,320 --> 00:14:50,180
Dobbiamo abbracciare le contraddizioni. 

86
00:14:50,920 --> 00:14:56,560
Sembra che fumi molto … 

87
00:14:57,540 --> 00:15:03,260
Non molto! 

88
00:15:04,220 --> 00:15:10,620
Lo zen ti ha reso abbastanza forte da accettare qualcosa? 

89
00:15:11,140 --> 00:15:15,360
Tutto è possibile! 

90
00:15:15,920 --> 00:15:20,240
Non è così importante preoccuparsi della propria vita. 

91
00:15:20,540 --> 00:15:25,960
Gli europei hanno troppo egotismo. 

92
00:15:26,400 --> 00:15:31,840
Devi accettare tutto e andare oltre! 

93
00:15:32,180 --> 00:15:36,960
Ma lo zen non è ascetismo, né mortificazione. 

94
00:15:37,920 --> 00:15:41,460
Dobbiamo per la vita cosmica. 

95
00:15:41,680 --> 00:15:45,900
Non dobbiamo porre limiti. 

96
00:15:46,320 --> 00:15:48,780
Altrimenti, la vita diventa stretta. 

97
00:15:52,340 --> 00:15:55,880
Rimanere in contatto con il Giappone 

98
00:18:11,940 --> 00:18:15,640
Sull’educazione dei bambini … 

99
00:18:15,980 --> 00:18:27,960
Nei tempi moderni, ci concentriamo solo sul lato intellettuale dell’educazione. 

100
00:18:28,360 --> 00:18:35,880
Dobbiamo anche vedere il lato fisico, la giusta tensione muscolare. 

101
00:18:38,600 --> 00:18:45,740
Inoltre, dobbiamo concentrarci sugli aspetti banali della vita quotidiana. 

102
00:18:46,100 --> 00:18:50,560
La concentrazione e la postura fisica sono molto importanti. 

103
00:18:50,960 --> 00:18:59,200
Non dobbiamo essere sciatti mentre mangiamo, per esempio. 

104
00:18:59,740 --> 00:19:04,200
Concentrarsi sulla vita quotidiana è fondamentale. 

105
00:19:04,620 --> 00:19:10,920
Anche la nostra postura durante il sonno è fondamentale! 

106
00:19:11,860 --> 00:19:19,460
Gli insegnanti delle scuole non prestano sufficiente attenzione alla postura fisica. 

107
00:19:19,800 --> 00:19:23,920
Si preoccupano solo del lato intellettuale. 

108
00:19:24,220 --> 00:19:27,679
Perché i bambini non sono ben educati? 

109
00:19:28,260 --> 00:19:30,680
L’istruzione è troppo facile? 

110
00:19:31,200 --> 00:19:36,100
Torniamo agli animali nei tempi moderni! 

111
00:19:37,160 --> 00:19:46,380
È tutto troppo facile … 

112
00:19:46,660 --> 00:19:50,040
Guidiamo auto e non vogliamo più camminare … 

113
00:19:50,420 --> 00:20:00,160
Anche l’educazione familiare è troppo facile. È un errore. 

114
00:20:01,360 --> 00:20:09,479
Caldo, cibo. Troppo "softy-softy"! 

115
00:20:10,600 --> 00:20:19,320
Il nostro corpo diventa debole. 

116
00:20:21,540 --> 00:20:26,180
Cerchiamo la libertà degli animali? 

117
00:20:26,540 --> 00:20:28,540
Gli animali selvatici sono forti. 

118
00:20:28,940 --> 00:20:40,340
Gli animali prigionieri e ben nutriti negli zoo sono deboli. 

119
00:20:40,680 --> 00:20:42,680
Mancano attività. 

120
00:20:45,200 --> 00:20:47,700
Gli animali selvatici sono forti! 

121
00:20:56,140 --> 00:20:59,100
L’intelligenza è importante. 

122
00:20:59,360 --> 00:21:01,100
Ma non è abbastanza. 

123
00:21:01,420 --> 00:21:06,720
Abbiamo anche bisogno di un corpo forte e di una saggezza superiore. 

124
00:21:09,080 --> 00:21:12,800
Creazione del tempio Gendronière 

125
00:23:45,260 --> 00:23:50,540
Hai portato in Europa il frutto dello Zen in modo che le persone possano assaggiarlo. 

126
00:23:50,820 --> 00:24:05,540
India, Cina, Giappone hanno la vera essenza della cultura spirituale. 

127
00:24:07,480 --> 00:24:18,000
Ora dobbiamo unire le civiltà asiatica ed europea. 

128
00:24:23,660 --> 00:24:31,000
Lo Zen è l’essenza della cultura asiatica. 

129
00:24:31,760 --> 00:24:39,580
Se pratichi lo Zen, capirai le civiltà asiatiche. 

130
00:24:40,660 --> 00:24:55,280
Ci sarà un rinascimento spirituale attraverso l’incontro tra tradizione occidentale e Zen? 

131
00:24:55,620 --> 00:24:59,780
Credo di si. Lo spero. 

132
00:25:00,380 --> 00:25:04,780
È importante che tutti diventino Dio. 

133
00:25:05,120 --> 00:25:07,120
Qual è il modo per raggiungere questo obiettivo? 

134
00:25:07,400 --> 00:25:11,320
Se pratichi lo zazen, puoi diventare Dio. 

135
00:25:11,660 --> 00:25:21,300
Il vero dio è il dio naturale in noi? 

136
00:25:21,520 --> 00:25:29,520
Sì, tutti devono creare il vero dio. 

137
00:25:31,200 --> 00:25:35,180
L’ultimo insegnamento 

138
00:25:36,600 --> 00:25:40,360
La filosofia Zen è molto ampia. 

139
00:25:40,800 --> 00:25:44,980
Devi abbracciare le contraddizioni! 

140
00:25:45,420 --> 00:25:47,440
Vita e morte, per esempio? 

141
00:25:47,680 --> 00:25:50,400
Sì, vita e morte! 

142
00:28:10,080 --> 00:28:27,360
Dedicato a Mokudo Taisen Deshimaru (1914-1982) 

