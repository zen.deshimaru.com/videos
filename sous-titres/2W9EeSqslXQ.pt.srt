1
00:00:00,000 --> 00:00:04,799
Como você sincroniza o kin-hin andando e respirando lentamente? 

2
00:00:04,799 --> 00:00:08,550
Você disse que daríamos um passo adiante a cada expiração. 

3
00:00:08,550 --> 00:00:15,690
Mas a maioria das pessoas respira duas vezes em uma única etapa. 

4
00:00:15,690 --> 00:00:19,470
Claro que quero fazer duas. 

5
00:00:19,470 --> 00:00:24,180
As pessoas dobram a respiração porque não fazem uma pausa entre as respirações. 

6
00:00:24,180 --> 00:00:28,650
Ao andar criança-hin lentamente, a pequena pausa é fundamental. 

7
00:00:28,650 --> 00:00:33,059
Se você descansar um pouco em cima da expiração … 

8
00:00:33,059 --> 00:00:38,210
Você está no ritmo. 

9
00:00:38,210 --> 00:00:44,309
Dogen e seu mestre Nyojo dizem …. 

10
00:00:44,309 --> 00:00:50,219
… que o próprio Buda disse que você não dobra o fôlego. 

11
00:00:50,219 --> 00:00:55,050
Para fazer isso, no final da expiração, é necessário fazer uma pequena pausa … 

12
00:00:55,050 --> 00:01:03,239
…. em que vamos dar meio passo à frente … 

13
00:01:03,239 --> 00:01:09,299
… após uma breve pausa antes de inalar e estamos em bom ritmo. 

14
00:01:09,299 --> 00:01:14,610
Nós estamos cansados ​​…. muito menos. 

15
00:01:14,610 --> 00:01:20,580
Durante a sessão, eu ampliei a família. 

16
00:01:20,580 --> 00:01:26,159
Eu já tinha vivido isso em outro exercício com outro mestre … 

17
00:01:26,159 --> 00:01:32,670
Você teve que andar muito com os olhos vendados. 

18
00:01:32,670 --> 00:01:38,280
Nós devemos andar sem ver nada. 

19
00:01:38,280 --> 00:01:42,270
É cansativo porque você também precisa colocar as mãos em uma posição …. 

20
00:01:42,270 --> 00:01:45,540
… menos confortável do que no tipo hin. 

21
00:01:45,540 --> 00:01:51,180
Se você faz hin familiar por um longo tempo, ele se cansará …. 

22
00:01:51,180 --> 00:01:54,659
… outro aspecto de nós mesmos. 

23
00:01:54,659 --> 00:01:59,360
É um aspecto ativo. Eu aprendi na medicina chinesa …. 

24
00:01:59,360 --> 00:02:06,920
…. para curar atividades excessivas, você precisa descansar. 

25
00:02:13,340 --> 00:02:18,170
Se você faz família por um longo tempo, está exausto. 

26
00:02:18,170 --> 00:02:23,230
Você tem dor nas costas, dor no ombro, dor nos braços … 

27
00:02:23,230 --> 00:02:29,330
Se você andar assim por uma hora, as pessoas estão esgotadas. 

28
00:02:29,330 --> 00:02:33,290
Eles só querem uma coisa: estar no zazen. 

29
00:02:33,290 --> 00:02:41,470
Que alívio quando você está no zazen: é complementar. 

30
00:02:41,470 --> 00:02:46,549
Você diz a si mesmo: que relaxamento, que rendição! 

31
00:02:46,549 --> 00:02:51,650
Você realmente quer estar no Zazen. 

32
00:02:51,650 --> 00:02:57,920
Depois de uma hora em Zazen, você terá a sensação …. 

33
00:02:57,920 --> 00:03:02,720
….. formigamento desagradável … 

34
00:03:02,720 --> 00:03:07,040
Você vai querer se mexer e gostaria que acabasse por causa da dor. 

35
00:03:07,040 --> 00:03:11,630
O zazen e a caminhada lenta são completamente complementares. 

36
00:03:11,630 --> 00:03:14,870
Mas porque praticamos a criança hin por um curto período de tempo, não percebemos isso. 

37
00:03:14,870 --> 00:03:19,430
Agora vou fazer a criança hins por mais tempo durante as sessões. 

38
00:03:19,430 --> 00:03:25,130
Para que as pessoas gostem de se sentar e deixar ir certas coisas no zazen. 

39
00:03:25,130 --> 00:03:29,350
Sua pergunta é muito boa e não é nova. 

40
00:03:29,350 --> 00:03:38,920
Mestre Dogen colocou o problema há 1.400 anos. 

