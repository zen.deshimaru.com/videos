1
00:00:00,000 --> 00:00:06,820
Ich zeige ein kleines Detail der Zazen-Haltung,

2
00:00:07,200 --> 00:00:12,720
als André Lemort, mit dem ich 1969 mit Monsieur Lambert Zazen begann,

3
00:00:13,120 --> 00:00:17,440
hat es in seinem Video gut erklärt, und ich denke
dass es sehr wichtig ist,

4
00:00:17,640 --> 00:00:21,540
Es ist die Hüftöffnung.

5
00:00:21,860 --> 00:00:26,880
Wenn Sie das Bein zurückbringen - ich mache den Lotus auf der rechten Seite -

6
00:00:27,560 --> 00:00:34,840
Wenn man also den Halblotus macht, rutscht man mit einem Fuß gegen das Zafu,

7
00:00:35,180 --> 00:00:40,340
und den anderen an die Leiste legen.

8
00:00:41,200 --> 00:00:48,600
Das Wichtigste ist, die Hüfte zu lockern.

9
00:00:49,160 --> 00:00:52,920
Wir tun es mehr oder weniger schon, aber nicht genug.

10
00:00:53,560 --> 00:00:58,640
Wir müssen es geradeaus machen, mit Vollgas!

11
00:00:59,200 --> 00:01:15,080
Man sperrt sie [die Hüfte] einfach auf, nimmt das Sitzbein in die Hand unter dem Gesäß und zieht sich selbst heraus.

12
00:01:15,720 --> 00:01:25,440
Damit eröffnet sie die
Becken und das ändert alles, im Lotus fühlen wir uns wirklich wohl, wir sind gut eingekeilt.

13
00:01:26,680 --> 00:01:32,840
Dann das andere Bein.

14
00:01:35,000 --> 00:01:41,040
Mit dem rechten Bein, das oben liegt, mache ich dasselbe.

15
00:01:41,480 --> 00:01:51,160
Ich stelle mich auf die Beine und schließe die Tür auf. Ich nehme einfach das Sitzbein in meiner Hand und ziehe es heraus,

16
00:01:51,640 --> 00:01:58,960
Ich schieße auf die eine und die andere Pobacke. Man zieht den Knochen, man zieht die Haut heraus,

17
00:01:59,600 --> 00:02:05,600
Sie sind also klar und jetzt halten Sie wirklich stand.

18
00:02:05,720 --> 00:02:09,640
Wir sind in einer wirklich guten Position

19
00:02:09,720 --> 00:02:18,320
Wenn Sie das Gefühl haben, dass die Pins so [gut positioniert] sind, fühlen Sie sich wirklich wohl.

20
00:02:18,480 --> 00:02:21,160
Gutes Zazen für alle!
