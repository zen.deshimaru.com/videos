1
00:00:07,490 --> 00:00:10,656
People think that zazen is a technique … 

2
00:00:10,656 --> 00:00:13,173
who will help them in their lives. 

3
00:00:13,173 --> 00:00:16,800
They make it a ridiculously small thing. 

4
00:00:16,800 --> 00:00:21,820
and do zazen in the hope that it will help them in their lives. 

5
00:00:21,820 --> 00:00:31,280
That is true, because at this very conscious moment …, 

6
00:00:31,280 --> 00:00:35,300
the most energetic, the most present there is …. 

7
00:00:35,300 --> 00:00:39,820
…. we have full influence on all phenomena in our life, that’s for sure. 

8
00:00:40,020 --> 00:00:46,240
But we are not doing this to improve our ego …. 

9
00:00:46,250 --> 00:00:50,720
To calm us down …. 

10
00:00:50,720 --> 00:00:52,880
Because it will do us good …. 

11
00:00:52,880 --> 00:00:58,500
Because it will save us a few hours of sleep …. 

12
00:00:58,500 --> 00:01:02,759
Giving us greater sexual performance …. 

13
00:01:02,759 --> 00:01:06,100
Or help us deal with stress. 

14
00:01:06,100 --> 00:01:11,500
This lowers the zazen …. 

15
00:01:11,500 --> 00:01:16,230
at the level of an instrument for our own illusory life. 

16
00:01:16,230 --> 00:01:23,760
But it is the opposite. 

17
00:01:23,920 --> 00:01:28,980
We will focus our lives to generate the most effective zazen. 

18
00:01:30,890 --> 00:01:33,880
Sit in the position of the Buddha, 

19
00:01:33,880 --> 00:01:39,380
it is the most intense, most conscious moment. 

20
00:01:39,380 --> 00:01:43,020
That’s what it’s all about. 

21
00:01:43,020 --> 00:01:46,340
Everything comes together: the past, the present, the future. 

22
00:01:46,340 --> 00:01:52,060
All phenomena of our life are in zazen. 

23
00:01:52,060 --> 00:02:00,040
It is in zazen that we have power, consciousness, 

24
00:02:06,620 --> 00:02:12,020
to fundamentally change the ins and outs of our lives. 

25
00:02:16,320 --> 00:02:21,240
In zazen we are in a dignified attitude. 

26
00:02:21,240 --> 00:02:25,380
An attitude of heroes, Buddha, God. 

27
00:02:25,500 --> 00:02:31,320
We are straight, strong, handsome, calm, calm, calm, immobile. 

28
00:02:31,320 --> 00:02:36,600
You should not fall asleep, you should stretch the spine. 

29
00:02:36,600 --> 00:02:41,980
You have to breathe, relax. 

30
00:02:46,640 --> 00:02:50,740
In zazen there is everything, everything we are. 

31
00:02:50,740 --> 00:02:55,800
All our past, our present and indirectly our future. 

32
00:02:55,980 --> 00:03:05,460
There is our body, with its weaknesses, its fatigue, 

33
00:03:05,600 --> 00:03:11,200
its impurities, its toxins, 

34
00:03:11,209 --> 00:03:14,900
his illnesses, his karma, etc. 

35
00:03:14,900 --> 00:03:21,380
There is our mind, our conscience. 

36
00:03:21,380 --> 00:03:25,055
Everything is united in zazen. 

37
00:03:25,055 --> 00:03:29,320
The zazen attitude, 

38
00:03:29,320 --> 00:03:33,880
it is the most beautiful self-image you can show. 

39
00:03:40,620 --> 00:03:46,090
I met Master Deshimaru, he taught me to zazen, I sat in zazen. 

40
00:03:46,090 --> 00:03:48,620
And I immediately said, "This is the best!" 

41
00:04:01,360 --> 00:04:05,680
To know what you create, what is the reality you create … 

42
00:04:05,680 --> 00:04:10,680
You are the one who creates reality. You live your dream. 

43
00:04:10,680 --> 00:04:15,920
Everyone is living their dream, and you have to take your dream, that’s the thing. 

44
00:04:15,920 --> 00:04:19,480
For me, Zazen has always been like this. 

