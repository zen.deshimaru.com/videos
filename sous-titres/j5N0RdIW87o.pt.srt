1
00:00:00,000 --> 00:00:05,180
Eu pratiquei com a irmã Deshimaru em sesshin e nos acampamentos de verão. 

2
00:00:05,500 --> 00:00:08,360
Foi-me dito: "Você verá, ele é um mestre!" 

3
00:00:08,700 --> 00:00:13,040
Eu nem ousei olhar para ele, fiquei tão intimidada. 

4
00:00:13,540 --> 00:00:16,780
Quando descobri que ele estava doente, pensei: 

5
00:00:17,120 --> 00:00:20,340
"Devo continuar Zen se ele morrer?" 

6
00:00:20,620 --> 00:00:26,180
Tive a impressão de que ele havia me ensinado algo que me deu coragem para continuar. 

7
00:00:26,620 --> 00:00:30,400
Meu nome é Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
Eu poderia ter pensado antes que eu poderia ser uma freira católica … 

9
00:00:35,400 --> 00:00:39,320
Mas como é bom ser uma freira zen na vida profissional! 

10
00:00:39,620 --> 00:00:43,200
Continuei a seguir o dojo de Lyon com André Meissner. 

11
00:00:43,440 --> 00:00:49,000
Foi seguindo Meissner, que mais tarde seguiu Stéphane, que naturalmente segui o mestre Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen é um aluno próximo do Mestre Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
Fui ordenado um Bodhisattva em 1980 pelo Mestre Deshimaru. 

14
00:00:59,680 --> 00:01:03,560
O Bodhisattva é alguém que quer ajudar todos os seres. 

15
00:01:03,780 --> 00:01:07,960
Ordenação é o cumprimento deste voto. 

16
00:01:08,200 --> 00:01:10,560
Eu nunca tentei praticar Zen. 

17
00:01:10,860 --> 00:01:12,460
Eu não sabia o que era. 

18
00:01:12,740 --> 00:01:15,180
Havia um cartaz em uma conferência em Lyon. 

19
00:01:15,400 --> 00:01:18,180
Foi uma conferência com Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
Ele diz que você sentiria falta de algo se não fosse. 

21
00:01:26,320 --> 00:01:33,860
Durante sua palestra, havia pessoas no zazen, mas nem fiquei impressionado com a atitude. 

22
00:01:34,280 --> 00:01:37,780
Foi um pouco de um movimento intelectual da minha parte. 

23
00:01:38,020 --> 00:01:42,220
Quando comecei a me exercitar, era uma maneira de parar o tempo. 

24
00:01:42,500 --> 00:01:44,640
Tudo vai tão rápido! 

25
00:01:44,940 --> 00:01:50,140
Eu também pensei que poderia resolver todos os problemas na base. 

26
00:01:50,460 --> 00:01:55,260
Isso resolveu um problema porque me senti expulso da carreira de meu professor de matemática. 

27
00:01:55,540 --> 00:02:00,280
Gostei da primeira frase do poema Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"A grande estrada não é difícil, apenas evite uma escolha." 

29
00:02:05,860 --> 00:02:11,100
Eu disse a mim mesmo: "Sim, estou muito disponível para o que está acontecendo!" 

30
00:02:11,420 --> 00:02:16,560
Enquanto praticava, percebi o quanto ainda estava em um estado de escolha e rejeição. 

31
00:02:16,940 --> 00:02:23,900
Praticar o Zen também nos ajuda a ver nossa escuridão. 

32
00:02:25,100 --> 00:02:32,740
Se você gostou deste vídeo, sinta-se à vontade para curtir e se inscrever no canal! 

