1
00:00:00,000 --> 00:00:06,820
Je montre un petit détail de la posture de zazen,

2
00:00:07,200 --> 00:00:12,720
qu’ André Lemort avec qui j’ai débuté zazen en 1969 avec Monsieur Lambert,

3
00:00:13,120 --> 00:00:17,440
a bien expliqué dans sa vidéo et je pense
que c’est très important,

4
00:00:17,640 --> 00:00:21,540
c’est l’ouverture de la hanche.

5
00:00:21,860 --> 00:00:26,880
Quand vous ramenez la jambe - je vais faire le lotus à droite -

6
00:00:27,560 --> 00:00:34,840
donc, quand vous faites le demi-lotus vous faites glisser un pied contre le zafu,

7
00:00:35,180 --> 00:00:40,340
et vous mettez l’autre contre l’aine.

8
00:00:41,200 --> 00:00:48,600
Ce qui est important c’est de décoincer la hanche.

9
00:00:49,160 --> 00:00:52,920
On le fait plus ou moins déjà, mais pas assez.

10
00:00:53,560 --> 00:00:58,640
Il faut le faire carrément, à fond la caisse!

11
00:00:59,200 --> 00:01:15,080
Carrément on débloque ça [la hanche] et vous prenez l’ischion dans la main sous les fesses et vous vous tirez vers l’extérieur.

12
00:01:15,720 --> 00:01:25,440
De manière à ce que ça ouvre le
bassin et ça change tout, en lotus on est vraiment confortable, on est bien calé.

13
00:01:26,680 --> 00:01:32,840
Ensuite l’autre jambe.

14
00:01:35,000 --> 00:01:41,040
La jambe droite qui est au-dessus, je fais la même chose.

15
00:01:41,480 --> 00:01:51,160
Je me mets sur le pied et je débloque tout ça. Je prends carrément l’ischion dans la main et je tire,

16
00:01:51,640 --> 00:01:58,960
je tire sur une fesse et sur l’autre. Vous tirez cet os, vous tirez la peau vers l’extérieur,

17
00:01:59,600 --> 00:02:05,600
comme ça vous est bien dégagé et là vous tenez vraiment bien stable.

18
00:02:05,720 --> 00:02:09,640
On est vraiment bien positionné

19
00:02:09,720 --> 00:02:18,320
on a l’impression que les ischions sont comme ça [bien positionnés] on est vraiment confortable.

20
00:02:18,480 --> 00:02:21,160
Bon zazen à tous!
