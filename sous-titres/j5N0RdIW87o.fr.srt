1
00:00:00,000 --> 00:00:05,180
J’ai pratiqué avec sœur Deshimaru en sesshin et dans des camps d’été. 

2
00:00:05,500 --> 00:00:08,360
On m’a dit: "Vous verrez, c’est un maître!" 

3
00:00:08,700 --> 00:00:13,040
Je n’ai même pas osé le regarder, j’étais tellement intimidée. 

4
00:00:13,540 --> 00:00:16,780
Quand j’ai découvert qu’il était malade, je me suis dit: 

5
00:00:17,120 --> 00:00:20,340
"Dois-je continuer Zen s’il meurt?" 

6
00:00:20,620 --> 00:00:26,180
J’avais l’impression qu’il m’avait appris quelque chose qui m’a donné le courage de continuer. 

7
00:00:26,620 --> 00:00:30,400
Je m’appelle Françoise Julien. 

8
00:00:32,400 --> 00:00:35,060
J’aurais peut-être pensé auparavant que je pouvais être religieuse catholique … 

9
00:00:35,400 --> 00:00:39,320
Mais comme c’est bon d’être une religieuse zen dans la vie professionnelle! 

10
00:00:39,620 --> 00:00:43,200
J’ai continué à suivre le dojo lyonnais avec André Meissner. 

11
00:00:43,440 --> 00:00:49,000
C’est en suivant Meissner, qui a ensuite suivi Stéphane, que j’ai naturellement suivi Maître Kosen. 

12
00:00:49,320 --> 00:00:52,920
Stéphane Kosen est un proche élève de Maître Deshimaru. 

13
00:00:53,420 --> 00:00:59,260
J’ai été ordonné Bodhisattva en 1980 par Maître Deshimaru. 

14
00:00:59,680 --> 00:01:03,560
Le Bodhisattva est celui qui veut aider tous les êtres. 

15
00:01:03,780 --> 00:01:07,960
L’ordination est l’accomplissement de ce vœu. 

16
00:01:08,200 --> 00:01:10,560
Je n’ai jamais essayé de pratiquer le Zen. 

17
00:01:10,860 --> 00:01:12,460
Je ne savais pas ce que c’était. 

18
00:01:12,740 --> 00:01:15,180
Il y avait une affiche lors d’une conférence à Lyon. 

19
00:01:15,400 --> 00:01:18,180
C’était une conférence avec Alain Cassan. 

20
00:01:18,460 --> 00:01:25,840
Il dit que tu manquerais quelque chose si tu n’y allais pas. 

21
00:01:26,320 --> 00:01:33,860
Pendant sa conférence, il y avait des gens à zazen, mais je n’ai même pas été impressionné par l’attitude. 

22
00:01:34,280 --> 00:01:37,780
C’était un peu une démarche intellectuelle de ma part. 

23
00:01:38,020 --> 00:01:42,220
Quand j’ai commencé à faire de l’exercice, c’était un moyen d’arrêter le temps. 

24
00:01:42,500 --> 00:01:44,640
Tout va si vite! 

25
00:01:44,940 --> 00:01:50,140
J’ai également pensé que cela pourrait résoudre tous les problèmes sur la base. 

26
00:01:50,460 --> 00:01:55,260
Cela a résolu un problème parce que je me sentais exclu de la carrière de mon professeur de mathématiques. 

27
00:01:55,540 --> 00:02:00,280
J’ai aimé la première phrase du poème Shin Jin Mei: 

28
00:02:00,620 --> 00:02:05,500
"La grande route n’est pas difficile, il suffit d’éviter un choix." 

29
00:02:05,860 --> 00:02:11,100
Je me suis dit: "Oui, je suis très disponible pour ce qui se passe!" 

30
00:02:11,420 --> 00:02:16,560
En pratiquant, j’ai réalisé à quel point j’étais encore dans un état de choix et de rejet. 

31
00:02:16,940 --> 00:02:23,900
La pratique du Zen nous aide également à voir notre obscurité. 

32
00:02:25,100 --> 00:02:32,740
Si vous avez aimé cette vidéo, n’hésitez pas à l’aimer et à vous abonner à la chaîne! 

