1
00:00:07,600 --> 00:00:09,910
Ich werde die Einweihung vornehmen.

2
00:00:10,080 --> 00:00:13,570
Es ist mindestens eine Person registriert.

3
00:00:13,850 --> 00:00:17,860
Dann gehen wir durch die Nachbarschaft!

4
00:00:29,760 --> 00:00:32,305
Die Schutzmassnahmen sind:

5
00:00:33,225 --> 00:00:36,380
Alkohol und Gel, mehrmals ein und aus.

6
00:00:36,530 --> 00:00:39,390
Die soziale Distanz von eineinhalb Metern.

7
00:00:39,550 --> 00:00:44,960
In der Lobby und im Umkleideraum, im Dojo.

8
00:00:45,140 --> 00:00:48,740
Wir tragen die Maske die ganze Zeit.

9
00:00:48,910 --> 00:00:52,370
Wir ziehen ihn aus, um Zazen zu machen, wir behalten ihn im Kimono.

10
00:00:52,450 --> 00:00:55,175
Und während des Kinnhinlaufs wieder anziehen.

11
00:00:55,325 --> 00:00:58,940
Wir halten die Fenster offen, trotz des Lärms oder der Kälte.

12
00:01:00,330 --> 00:01:03,280
Guten Morgen, es ist 6.15 Uhr morgens.

13
00:01:03,360 --> 00:01:08,110
Wir gehen ins Dojo, um rechtzeitig anzukommen und alles vorzubereiten.

14
00:01:08,330 --> 00:01:11,490
Viel Spaß bei der Stadtrundfahrt durch Barcelona!

15
00:01:35,120 --> 00:01:37,935
Unser Dojo in der Tordera-Straße befindet sich im Bau.

16
00:01:38,005 --> 00:01:44,940
Wir bereiten uns auf den Halbtag von Zazen vor...
unter der Leitung von Maître Pierre Soko am Ende des Monats.

17
00:01:45,020 --> 00:01:50,790
Es ist ein Raum in Barcelona, in dem Aktivitäten im Zusammenhang mit dem Buddhismus praktiziert werden.

18
00:01:50,900 --> 00:01:56,587
Wir haben es gemietet, um weiterhin Zazen zu praktizieren.

19
00:01:56,700 --> 00:02:00,730
Mit dem Covid haben wir alle Aktivitäten deprogrammiert.

20
00:02:00,990 --> 00:02:08,090
Wir können weder Sesshin, noch tagsüber oder am Unterricht teilnehmen.

21
00:02:08,470 --> 00:02:14,610
Ich kann nichts Besonderes tun, um die Praxis zu verbreiten: Wir stecken fest.

22
00:02:14,880 --> 00:02:19,040
Wir pflegen eine Praxis nach bestem Wissen und Gewissen.

23
00:02:19,143 --> 00:02:25,460
Aber wir können nicht für das Zen-Dojo werben oder es weiterentwickeln.

24
00:02:25,760 --> 00:02:30,375
Die einzige Werbung, die wir machen können, ist im Internet und in sozialen Netzwerken.

25
00:02:30,536 --> 00:02:34,905
Aber wir bringen keine Plakate auf die Straße,
etwas, was wir seit 20 Jahren tun.

26
00:02:35,521 --> 00:02:38,825
Also gut. Noch ein Tag. Pandemie.

27
00:02:38,955 --> 00:02:43,280
Wir hatten heute viele Praktiker, wir sind glücklich.

28
00:02:50,000 --> 00:03:00,060
Der Covid hat mein Familien- und Berufsleben gestört.

29
00:03:02,300 --> 00:03:03,950
Ich bin Franzose.

30
00:03:04,041 --> 00:03:12,920
Für das Coronavirus befolge ich die üblichen Vorsichtsmaßnahmen:
die Maske, das Händewaschen...

31
00:03:13,440 --> 00:03:15,390
Hallo, mein Name ist Helena.

32
00:03:15,500 --> 00:03:18,140
Ich praktiziere seit 15 Jahren im Ryokan Dojo.

33
00:03:18,200 --> 00:03:25,850
Es ist sehr schwierig für mich, nicht im Dojo trainieren zu können.

34
00:03:26,160 --> 00:03:30,660
Aber man muss vorsichtig sein, die Dinge so akzeptieren, wie sie sind.

35
00:03:30,760 --> 00:03:34,080
Wir befinden uns mitten in einer zweiten Welle.

36
00:03:34,350 --> 00:03:38,468
Ich gehöre zur Risikogruppe und muss zu Hause bleiben.

37
00:03:39,100 --> 00:03:42,200
Ich begann, Zen bei "Zen y playa" zu praktizieren.

38
00:03:42,440 --> 00:03:48,610
Ich bin Schauspielerin und die Welt der Kultur ist sehr unsicher...

39
00:03:48,810 --> 00:03:53,970
Hallo, mein Name ist David und ich bin Praktiker im Dojo.

40
00:03:54,070 --> 00:03:59,110
Trotz des Covids fühle ich mich hier sicher.

41
00:03:59,230 --> 00:04:01,877
Ich möchte wirklich Zazen praktizieren.

42
00:04:02,000 --> 00:04:06,200
Ich komme auf die Initiation zurück, weil ich früher im Dojo in der Tordera-Straße trainiert habe.

43
00:04:06,520 --> 00:04:09,140
Ich musste eine Einweihung wiederholen.

44
00:04:09,260 --> 00:04:16,910
In letzter Zeit verspüre ich sehr viel Angst.

45
00:04:20,100 --> 00:04:23,890
Ich möchte sowohl für mich selbst sorgen als auch weiterleben.

46
00:04:44,720 --> 00:04:49,120
Spirituell zu wachsen.

47
00:04:51,800 --> 00:04:59,050
Meister Dogen lädt uns zu einer altruistischen, selbstlosen Praxis ein.

48
00:05:16,070 --> 00:05:17,650
Hallo.

49
00:05:40,030 --> 00:05:42,400
Da haben wir's. Ende von Zazen.

50
00:05:42,660 --> 00:05:46,460
Ich nehme die U-Bahn für 40 Minuten.

51
00:05:46,690 --> 00:05:51,340
Und wir werden es morgen früh um 7 Uhr für das erste Zazen wieder tun.

52
00:05:52,015 --> 00:05:56,015
Es ist 21 Uhr, und wie Sie sehen können, ist Barcelona menschenleer.

53
00:05:57,239 --> 00:05:58,639
So ist es nun einmal.

54
00:05:58,729 --> 00:06:01,239
Schönen Abend noch, bis bald!
