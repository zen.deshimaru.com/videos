Zen and Sophrology: Fundamental Postures and Their Influence on the Brain

Master Kosen has been invited to the 2013 congress of the Federation of Professional Schools of Sophrology, in France (www.sophro.fr).

He explains the four fundamental postures of Zen and their influence on the brain. He returns to the sources of dynamic relaxation. He insists on the importance of the three pillars: posture, breathing and state of mind in Buddhist meditation, and especially in the practice of Zen, zazen.
