1
00:00:00,000 --> 00:00:04,660
U zei dat als de staatshoofden prosternaties uitvoeren,

2
00:00:05,080 --> 00:00:08,560
zou de wereld vreedzamer zijn.

3
00:00:08,880 --> 00:00:13,040
Ik vroeg me af…

4
00:00:13,340 --> 00:00:18,340
in vergelijking met andere religies.

5
00:00:18,620 --> 00:00:23,080
In de moslimreligie is er sprake van prosternatie.

6
00:00:23,460 --> 00:00:34,540
Toch verscheuren sommige landen in het Midden-Oosten elkaar.

7
00:00:35,180 --> 00:00:42,020
Ik wilde weten waarom.

8
00:00:42,320 --> 00:00:46,000
Want prosternatie is hetzelfde in Zen en in de Islam.

9
00:00:46,340 --> 00:00:52,660
Ik denk dat er goed en slecht is in alle religies.

10
00:00:53,080 --> 00:00:58,240
Maar prosternatie is een goede zaak, in de moslimreligie.

11
00:00:58,400 --> 00:01:04,620
Christenen slaan ook de handen ineen op dezelfde manier als in Zen, in Gasshô.

12
00:01:05,100 --> 00:01:08,520
Dat is niet erg.

13
00:01:08,700 --> 00:01:14,040
Er is ook fanatisme, dat is er altijd geweest.

14
00:01:14,300 --> 00:01:18,820
Er zijn altijd al religieuze oorlogen geweest.

15
00:01:21,320 --> 00:01:26,920
In Zen wordt prosternatie niet voor een bepaald doel gedaan.

16
00:01:27,280 --> 00:01:33,440
Noch om het beeld op het altaar te aanbidden, noch om te bidden tot Boeddha.

17
00:01:33,960 --> 00:01:39,640
Het is de praktijk van de houding met je hele lichaam.

18
00:01:40,020 --> 00:01:46,140
Op het moment dat je de grond raakt, is er een besef.

19
00:01:46,500 --> 00:01:50,860
Meestal raken de hersenen nooit de grond.

20
00:01:51,100 --> 00:01:55,820
Het is altijd in de lucht, altijd wijzend naar de sterren.

21
00:01:56,100 --> 00:02:01,480
Dus het voelt gewoon goed. Om opnieuw verbinding te maken, bukken, aanraken.

22
00:02:01,740 --> 00:02:06,300
Als moslims dit doen, is dat omdat er heel goede dingen zijn in de Islam.

23
00:02:06,660 --> 00:02:13,000
Gezinnen voeden hun kinderen goed op.

24
00:02:15,500 --> 00:02:21,620
Echte moslims zijn vol liefde.

25
00:02:24,200 --> 00:02:30,020
Gooi de baby niet met het badwater weg!

26
00:02:30,550 --> 00:02:34,450
Het is goed dat ze prosterneren!

