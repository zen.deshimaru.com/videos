1
00:00:07,490 --> 00:00:10,656
La gente pensa che lo zazen sia una tecnica … 

2
00:00:10,656 --> 00:00:13,173
chi li aiuterà nelle loro vite. 

3
00:00:13,173 --> 00:00:16,800
Lo rendono una cosa ridicolmente piccola. 

4
00:00:16,800 --> 00:00:21,820
e fai lo zazen nella speranza che li aiuti nella loro vita. 

5
00:00:21,820 --> 00:00:31,280
Questo è vero, perché in questo momento molto consapevole …, 

6
00:00:31,280 --> 00:00:35,300
il più energico, il più presente che c’è …. 

7
00:00:35,300 --> 00:00:39,820
…. abbiamo piena influenza su tutti i fenomeni della nostra vita, questo è certo. 

8
00:00:40,020 --> 00:00:46,240
Ma non lo stiamo facendo per migliorare il nostro ego …. 

9
00:00:46,250 --> 00:00:50,720
Per calmarci …. 

10
00:00:50,720 --> 00:00:52,880
Perché ci farà bene … 

11
00:00:52,880 --> 00:00:58,500
Perché ci farà risparmiare qualche ora di sonno …. 

12
00:00:58,500 --> 00:01:02,759
Dandoci maggiori prestazioni sessuali …. 

13
00:01:02,759 --> 00:01:06,100
O aiutaci ad affrontare lo stress. 

14
00:01:06,100 --> 00:01:11,500
Questo abbassa lo zazen …. 

15
00:01:11,500 --> 00:01:16,230
a livello di strumento per la nostra vita illusoria. 

16
00:01:16,230 --> 00:01:23,760
Ma è il contrario. 

17
00:01:23,920 --> 00:01:28,980
Concentreremo le nostre vite per generare lo zazen più efficace. 

18
00:01:30,890 --> 00:01:33,880
Siediti nella posizione del Buddha, 

19
00:01:33,880 --> 00:01:39,380
è il momento più intenso, più consapevole. 

20
00:01:39,380 --> 00:01:43,020
Ecco di cosa si tratta. 

21
00:01:43,020 --> 00:01:46,340
Tutto si unisce: il passato, il presente, il futuro. 

22
00:01:46,340 --> 00:01:52,060
Tutti i fenomeni della nostra vita sono nello zazen. 

23
00:01:52,060 --> 00:02:00,040
È nello zazen che abbiamo potere, coscienza, 

24
00:02:06,620 --> 00:02:12,020
per cambiare fondamentalmente i dettagli della nostra vita. 

25
00:02:16,320 --> 00:02:21,240
Nello zazen abbiamo un atteggiamento dignitoso. 

26
00:02:21,240 --> 00:02:25,380
Un atteggiamento di eroi, Buddha, Dio. 

27
00:02:25,500 --> 00:02:31,320
Siamo etero, forte, bello, calmo, calmo, calmo, immobile. 

28
00:02:31,320 --> 00:02:36,600
Non dovresti addormentarti, dovresti allungare la colonna vertebrale. 

29
00:02:36,600 --> 00:02:41,980
Devi respirare, rilassarti. 

30
00:02:46,640 --> 00:02:50,740
Nello zazen c’è tutto, tutto ciò che siamo. 

31
00:02:50,740 --> 00:02:55,800
Tutto il nostro passato, il nostro presente e indirettamente il nostro futuro. 

32
00:02:55,980 --> 00:03:05,460
C’è il nostro corpo, con i suoi punti deboli, la sua stanchezza, 

33
00:03:05,600 --> 00:03:11,200
le sue impurità, le sue tossine, 

34
00:03:11,209 --> 00:03:14,900
le sue malattie, il suo karma, ecc. 

35
00:03:14,900 --> 00:03:21,380
C’è la nostra mente, la nostra coscienza. 

36
00:03:21,380 --> 00:03:25,055
Tutto è unito nello zazen. 

37
00:03:25,055 --> 00:03:29,320
L’atteggiamento zazen, 

38
00:03:29,320 --> 00:03:33,880
è l’immagine di sé più bella che puoi mostrare. 

39
00:03:40,620 --> 00:03:46,090
Ho incontrato il Maestro Deshimaru, mi ha insegnato a zazen, mi sono seduto in zazen. 

40
00:03:46,090 --> 00:03:48,620
E ho subito detto: "Questo è il migliore!" 

41
00:04:01,360 --> 00:04:05,680
Per sapere cosa crei, qual è la realtà che crei … 

42
00:04:05,680 --> 00:04:10,680
Sei tu quello che crea la realtà. Vivi il tuo sogno. 

43
00:04:10,680 --> 00:04:15,920
Ognuno sta vivendo il proprio sogno e tu devi prendere il tuo sogno, questa è la cosa. 

44
00:04:15,920 --> 00:04:19,480
Per me, Zazen è sempre stato così. 

