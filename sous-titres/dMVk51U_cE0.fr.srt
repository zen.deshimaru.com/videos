1
00:00:07,600 --> 00:00:09,910
Je vais faire l'initiation.

2
00:00:10,080 --> 00:00:13,570
Il y a au moins une personne inscrite.

3
00:00:13,850 --> 00:00:17,860
Alors nous allons traverser le quartier !

4
00:00:29,760 --> 00:00:32,305
Les mesures de protection sont :

5
00:00:33,225 --> 00:00:36,380
l'alcool et le gel, plusieurs fois à l'entrée et à la sortie.

6
00:00:36,530 --> 00:00:39,390
La distance sociale d'un mètre et demi.

7
00:00:39,550 --> 00:00:44,960
Dans l'entrée et les vestiaires, dans le dojo.

8
00:00:45,140 --> 00:00:48,740
On porte le masque tout le temps.

9
00:00:48,910 --> 00:00:52,370
On l'enlève pour faire zazen, on le garde dans le kimono.

10
00:00:52,450 --> 00:00:55,175
Et on le remet pendant la marche de kin-hin.

11
00:00:55,325 --> 00:00:58,940
On garde les fenêtres ouvertes, malgré le bruit ou le froid.

12
00:01:00,330 --> 00:01:03,280
Bonjour, il est 6h15 du matin.

13
00:01:03,360 --> 00:01:08,110
Nous allons au dojo pour arriver avec du temps pour tout préparer.

14
00:01:08,330 --> 00:01:11,490
Profitez bien du Barcelone City Tour!

15
00:01:35,120 --> 00:01:37,935
Notre dojo de la rue Tordera est en travaux.

16
00:01:38,005 --> 00:01:44,940
Nous nous préparons pour la demi-journée de zazen
dirigée par Maître Pierre Soko à la fin du mois.

17
00:01:45,020 --> 00:01:50,790
C'est une salle de Barcelone où l'on pratique des activités liées au bouddhisme.

18
00:01:50,900 --> 00:01:56,587
Nous l'avons louée pour continuer à pratiquer zazen.

19
00:01:56,700 --> 00:02:00,730
Avec le covid, nous avons déprogrammé toutes les activités.

20
00:02:00,990 --> 00:02:08,090
On ne peut pas faire de sesshin, de journée, ou de cours.

21
00:02:08,470 --> 00:02:14,610
Je ne peux rien faire de spécial pour diffuser la pratique : nous sommes bloqués.

22
00:02:14,880 --> 00:02:19,040
On maintient une pratique du mieux que l'on peut.

23
00:02:19,143 --> 00:02:25,460
Mais on ne peut pas faire de publicité ou développer le dojo zen.

24
00:02:25,760 --> 00:02:30,375
La seule publicité que nous pouvons faire c'est sur le web et les réseaux sociaux.

25
00:02:30,536 --> 00:02:34,905
Mais on ne met pas d'affiches dans la rue,
chose qu'on faisait depuis 20 ans.

26
00:02:35,521 --> 00:02:38,825
Bon. Un jour de plus. Pandémie.

27
00:02:38,955 --> 00:02:43,280
Nous avons eu pas mal de pratiquants aujourd'hui, on est contents.

28
00:02:50,000 --> 00:03:00,060
Le covid a bouleversé ma vie familiale et professionnelle.

29
00:03:02,300 --> 00:03:03,950
Je suis français.

30
00:03:04,041 --> 00:03:12,920
Pour le coronavirus, je suis les précautions habituelles :
le masque, se laver les mains...

31
00:03:13,440 --> 00:03:15,390
Bonjour, je m'appelle Helena.

32
00:03:15,500 --> 00:03:18,140
Je pratique au dojo Ryokan depuis 15 ans.

33
00:03:18,200 --> 00:03:25,850
Il m'est très difficile de ne pas pouvoir pratiquer au dojo.

34
00:03:26,160 --> 00:03:30,660
Mais il faut être prudent, accepter les choses telles qu'elles sont.

35
00:03:30,760 --> 00:03:34,080
Nous sommes en pleine deuxième vague.

36
00:03:34,350 --> 00:03:38,468
Je fais partie du groupe à risques et je dois rester à la maison.

37
00:03:39,100 --> 00:03:42,200
J'ai commencé à pratiquer le zen à "Zen y playa".

38
00:03:42,440 --> 00:03:48,610
Je suis comédienne et le monde de la culture connaît beaucoup d'incertitude...

39
00:03:48,810 --> 00:03:53,970
Bonjour, je m'appelle David et je suis pratiquant au dojo.

40
00:03:54,070 --> 00:03:59,110
Malgré le covid, je me sens ici en sécurité.

41
00:03:59,230 --> 00:04:01,877
J'ai très envie de pratiquer zazen.

42
00:04:02,000 --> 00:04:06,200
Je reviens à l'initiation, car je pratiquais avant au dojo de la rue Tordera.

43
00:04:06,520 --> 00:04:09,140
Il fallait que je refasse une initiation.

44
00:04:09,260 --> 00:04:16,910
Ces derniers temps, je ressens souvent de la peur.

45
00:04:20,100 --> 00:04:23,890
Je veux à la fois prendre soin de moi et continuer à vivre.

46
00:04:44,720 --> 00:04:49,120
Pour grandir spirituellement.

47
00:04:51,800 --> 00:04:59,050
Maître Dogen nous invite à une pratique altruiste, désintéressée.

48
00:05:16,070 --> 00:05:17,650
Bonjour.

49
00:05:40,030 --> 00:05:42,400
Voilà. Fin du zazen.

50
00:05:42,660 --> 00:05:46,460
Je vais faire 40 minutes de métro.

51
00:05:46,690 --> 00:05:51,340
Et on remet ça demain matin à 7 heures pour le premier zazen.

52
00:05:52,015 --> 00:05:56,015
Il est 21 heures et comme vous le voyez, Barcelone est déserte.

53
00:05:57,239 --> 00:05:58,639
C'est ainsi.

54
00:05:58,729 --> 00:06:01,239
Bonne soirée, à bientôt !
