1
00:00:05,860 --> 00:00:08,580
Deshimaru, ich weiß, dass ich bald sterben werde. 

2
00:00:08,820 --> 00:00:17,320
Es ist an der Zeit, Sie als Mönch zu ordinieren, um Bodhidarmas Lehre in einem neuen Land weiterzugeben. 

3
00:00:31,020 --> 00:00:32,820
Ich möchte Gott helfen. 

4
00:00:33,100 --> 00:00:34,860
Ich möchte Christus helfen. 

5
00:00:43,260 --> 00:00:48,000
Ich möchte dem wahren Gott helfen. 

6
00:00:48,240 --> 00:00:52,760
Ich möchte zum wahren Gott zurückkehren. 

7
00:00:53,360 --> 00:01:00,360
40 Jahre seit der Ankunft von Meister Deshimaru in Europa. 

8
00:01:00,800 --> 00:01:04,000
Gott ist niedergeschlagen. 

9
00:01:04,320 --> 00:01:09,040
Gott ist "müde", müde. 

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. Das Bodhidarma der Neuzeit. 

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru kam im Sommer 1967 am Bahnhof Gare du Nord in Paris an. 

12
00:01:31,120 --> 00:01:33,480
Nur ein Mönch ohne Geld, der kein Französisch spricht. 

13
00:01:33,820 --> 00:01:36,600
In seinem Gepäck nur der frische Samen des Zen. 

14
00:01:45,440 --> 00:01:49,600
Die Ankunft 

15
00:02:59,140 --> 00:03:03,060
Wenn wir zur Geschichte des Zen zurückkehren können … 

16
00:03:04,380 --> 00:03:09,520
Alles begann in Indien und dann: Was ist passiert? 

17
00:03:09,720 --> 00:03:13,340
Wie ist es von Indien nach Japan gekommen? 

18
00:03:13,640 --> 00:03:19,020
Indien, sehr tiefe Meditation. 

19
00:03:19,480 --> 00:03:22,340
Aber zu traditionell. 

20
00:03:24,380 --> 00:03:29,980
Yoga ist in Indien noch nicht beendet. 

21
00:03:30,720 --> 00:03:36,160
Einige Menschen haben ein hohes spirituelles Niveau, die meisten jedoch nicht. 

22
00:03:36,760 --> 00:03:43,440
Aber Yoga, fertig. haben wir nicht fertig 

23
00:03:43,760 --> 00:03:48,240
Aber das Land ist zu alt. 

24
00:03:48,940 --> 00:03:53,660
Bodhidarma brachte Zazen von Indien nach China. 

25
00:03:53,900 --> 00:03:58,280
Aber in Indien gibt es kein Zen mehr. 

26
00:03:58,860 --> 00:04:04,880
Dann brachte Dogen Zazen von China nach Japan. 

27
00:04:05,160 --> 00:04:11,360
Meinen Sie damit, dass der Boden in Asien, Japan, China oder Indien zu alt ist? 

28
00:04:11,740 --> 00:04:14,740
Ja, Indien ist zu alt. 

29
00:04:14,960 --> 00:04:17,700
China ist auch zu alt. 

30
00:04:18,280 --> 00:04:20,280
Zen ist in China fertig. 

31
00:04:20,700 --> 00:04:23,940
Japan ist zu an Zen gewöhnt. 

32
00:04:24,680 --> 00:04:27,240
Sie kümmern sich nur um Zeremonien … 

33
00:04:27,680 --> 00:04:30,380
Sie vermissen die Essenz des Zen. 

34
00:04:36,160 --> 00:04:48,320
Denken Sie, dass der Geist des Zen tendenziell abnimmt? 

35
00:04:48,640 --> 00:04:50,620
Ja Ja Ja! 

36
00:04:51,040 --> 00:04:58,720
Denken Sie, dass der Geist des Zen in Frankreich fangen kann, wie der Samen einer Blume? 

37
00:04:59,040 --> 00:05:01,120
Ja! 

38
00:05:03,020 --> 00:05:06,980
Der Unterricht beginnt. 

39
00:06:24,680 --> 00:06:30,080
Warum haben Sie sich in Frankreich niedergelassen? 

40
00:06:30,400 --> 00:06:37,000
Frankreich ist besser als ein anderes Land, um Zen zu verstehen. 

41
00:06:37,520 --> 00:06:44,300
Sehr leicht zu verstehen, Zazen. 

42
00:06:45,120 --> 00:06:46,700
Oder Philosophie. 

43
00:06:46,980 --> 00:06:48,980
Für Ideen. 

44
00:06:49,300 --> 00:06:52,060
Sehr tief. 

45
00:06:52,520 --> 00:06:57,980
Zum Beispiel Montaigne: 

46
00:06:58,420 --> 00:07:03,900
"Es ist mir nicht so wichtig, was ich für andere bin, sondern was ich für mich selbst bin." 

47
00:07:05,880 --> 00:07:09,640
Descartes, Bergson … 

48
00:07:10,140 --> 00:07:14,080
Denker, Philosophen … 

49
00:07:14,800 --> 00:07:16,840
Sie können Zen verstehen. 

50
00:07:18,200 --> 00:07:25,740
Sie kennen Zen nicht, aber sie denken genauso … 

51
00:07:26,460 --> 00:07:29,760
Warum bist du nicht nach Amerika gegangen? 

52
00:07:30,280 --> 00:07:33,840
Ich wollte zuerst Zen in die USA bringen. 

53
00:07:34,220 --> 00:07:41,180
Aber sie wollen Zen für Unternehmen, Industrie, Psychologie, Psychoanalyse verwenden … 

54
00:07:41,540 --> 00:07:44,180
Sie wollen es benutzen. 

55
00:07:44,340 --> 00:07:49,020
Es ist nicht wahr, Zen. Nicht tief. 

56
00:07:52,500 --> 00:07:56,160
Eine Sangha erscheint. 

57
00:09:08,270 --> 00:09:14,100
Vorher Bildung gut, aber in der heutigen Zeit nur durchschnittlich. 

58
00:09:14,100 --> 00:09:23,740
Die Essenz ist beendet. 

59
00:09:24,560 --> 00:09:33,700
Wir suchen nur den Durchschnitt … 

60
00:09:34,380 --> 00:09:41,600
Für eine starke Ausbildung ist nur das Wesentliche notwendig. 

61
00:09:43,380 --> 00:09:49,960
Immer rechts oder links … 

62
00:09:51,040 --> 00:09:54,260
Aber wir müssen Widersprüche annehmen. 

63
00:09:54,600 --> 00:09:59,720
Nur wenn wir unser Ego aufgeben, können wir unsere wahre Individualität finden. 

64
00:10:03,920 --> 00:10:13,100
Unsere höchste Persönlichkeit, wenn wir der kosmischen Wahrheit folgen, dem Kosmos. 

65
00:10:13,500 --> 00:10:19,480
Das können wir unser wahres, starkes Ego finden. 

66
00:10:20,580 --> 00:10:25,680
Sie wollen Menschen stark machen? 

67
00:10:26,540 --> 00:10:32,060
Ja, ausgeglichen und stark, Stück für Stück. 

68
00:10:32,800 --> 00:10:41,600
Wir müssen die wahre Sparmaßnahme finden. 

69
00:10:43,360 --> 00:10:47,600
Unterrichten 

70
00:12:46,000 --> 00:12:57,680
Die Essenz des Zen ist "Mushotoku", ohne Zweck. 

71
00:12:59,200 --> 00:13:05,520
Alle Religionen haben ein Objekt. 

72
00:13:06,400 --> 00:13:10,560
Sie beten, reich oder glücklich zu werden. 

73
00:13:11,440 --> 00:13:17,040
Aber wenn wir einen Zweck haben, ist er nicht stark. 

74
00:13:18,400 --> 00:13:30,000
Wir dürfen keinen Zweck haben. 

75
00:13:31,680 --> 00:13:38,000
Du kannst dich nicht konzentrieren. 

76
00:13:39,360 --> 00:13:47,680
Meinst du, dass die richtige Realisierung spontan sein sollte? 

77
00:13:48,480 --> 00:13:56,080
Wir müssen zu den normalen Bedingungen zurückkehren. 

78
00:13:57,040 --> 00:14:01,440
Wahre Satori, transzendentales Bewusstsein … 

79
00:14:02,560 --> 00:14:07,520
ist zu normalen Bedingungen zurückzukehren. 

80
00:14:09,440 --> 00:14:18,080
Menschen sind in der heutigen Zeit abnormal. 

81
00:14:19,120 --> 00:14:27,120
Nur der wahre Gott, zum Beispiel Christus oder Buddha, hatte normale Bedingungen für Menschen. 

82
00:14:27,920 --> 00:14:33,900
Ist die erste notwendige Bedingung die eigene Entschlossenheit? 

83
00:14:34,340 --> 00:14:39,500
Und die zweite, um diese Entschlossenheit aufzugeben? 

84
00:14:39,760 --> 00:14:45,940
Ja. Individualität ist notwendig, und wir müssen sie aufgeben. 

85
00:14:46,320 --> 00:14:50,180
Wir müssen Widersprüche annehmen. 

86
00:14:50,920 --> 00:14:56,560
Es scheint, dass Sie viel rauchen … 

87
00:14:57,540 --> 00:15:03,260
Nicht sehr viel! 

88
00:15:04,220 --> 00:15:10,620
Hat Zen Sie stark genug gemacht, um etwas zu akzeptieren? 

89
00:15:11,140 --> 00:15:15,360
Alles ist möglich! 

90
00:15:15,920 --> 00:15:20,240
Es ist nicht so wichtig, sich um das eigene Leben zu kümmern. 

91
00:15:20,540 --> 00:15:25,960
Die Europäer haben zu viel Egoismus. 

92
00:15:26,400 --> 00:15:31,840
Sie müssen alles akzeptieren und darüber hinausgehen! 

93
00:15:32,180 --> 00:15:36,960
Aber Zen ist weder Askese noch Demütigung. 

94
00:15:37,920 --> 00:15:41,460
Wir müssen für das kosmische Leben. 

95
00:15:41,680 --> 00:15:45,900
Wir dürfen keine Grenzen setzen. 

96
00:15:46,320 --> 00:15:48,780
Ansonsten wird das Leben eng. 

97
00:15:52,340 --> 00:15:55,880
Mit Japan in Kontakt bleiben 

98
00:18:11,940 --> 00:18:15,640
Über die Erziehung von Kindern … 

99
00:18:15,980 --> 00:18:27,960
In der heutigen Zeit konzentrieren wir uns nur auf die intellektuelle Seite der Bildung. 

100
00:18:28,360 --> 00:18:35,880
Wir müssen auch die physische Seite sehen, die richtige Muskelspannung. 

101
00:18:38,600 --> 00:18:45,740
Wir müssen uns auch auf die alltäglichen Aspekte des täglichen Lebens konzentrieren. 

102
00:18:46,100 --> 00:18:50,560
Konzentration und Körperhaltung sind sehr wichtig. 

103
00:18:50,960 --> 00:18:59,200
Wir dürfen zum Beispiel beim Essen nicht schlampig sein. 

104
00:18:59,740 --> 00:19:04,200
Die Konzentration auf das tägliche Leben ist entscheidend. 

105
00:19:04,620 --> 00:19:10,920
Auch unsere Haltung im Schlaf ist entscheidend! 

106
00:19:11,860 --> 00:19:19,460
Schullehrer achten nicht genug auf die Körperhaltung. 

107
00:19:19,800 --> 00:19:23,920
Sie kümmern sich nur um die intellektuelle Seite. 

108
00:19:24,220 --> 00:19:27,679
Warum sind Kinder nicht gut erzogen? 

109
00:19:28,260 --> 00:19:30,680
Ist Bildung zu einfach? 

110
00:19:31,200 --> 00:19:36,100
Wir kommen in der Neuzeit zu Tieren zurück! 

111
00:19:37,160 --> 00:19:46,380
Alles ist zu einfach … 

112
00:19:46,660 --> 00:19:50,040
Wir fahren Autos und wollen nicht mehr laufen … 

113
00:19:50,420 --> 00:20:00,160
Auch die Familienerziehung ist zu einfach. Es ist ein Fehler. 

114
00:20:01,360 --> 00:20:09,479
Warmes Essen. Zu "weich-weich"! 

115
00:20:10,600 --> 00:20:19,320
Unser Körper wird schwach. 

116
00:20:21,540 --> 00:20:26,180
Suchen wir die Freiheit der Tiere? 

117
00:20:26,540 --> 00:20:28,540
Wilde Tiere sind stark. 

118
00:20:28,940 --> 00:20:40,340
Gefangene, gut ernährte Tiere in Zoos sind schwach. 

119
00:20:40,680 --> 00:20:42,680
Ihnen fehlt die Aktivität. 

120
00:20:45,200 --> 00:20:47,700
Wilde Tiere sind stark! 

121
00:20:56,140 --> 00:20:59,100
Intelligenz ist wichtig. 

122
00:20:59,360 --> 00:21:01,100
Aber es reicht nicht. 

123
00:21:01,420 --> 00:21:06,720
Wir brauchen auch einen starken Körper und höhere Weisheit. 

124
00:21:09,080 --> 00:21:12,800
Schaffung des Gendronière-Tempels 

125
00:23:45,260 --> 00:23:50,540
Sie haben die Früchte des Zen nach Europa gebracht, damit die Menschen sie probieren können. 

126
00:23:50,820 --> 00:24:05,540
Indien, China, Japan haben die wahre Essenz der spirituellen Kultur. 

127
00:24:07,480 --> 00:24:18,000
Wir müssen jetzt die asiatischen und europäischen Zivilisationen zusammenführen. 

128
00:24:23,660 --> 00:24:31,000
Zen ist die Essenz der asiatischen Kultur. 

129
00:24:31,760 --> 00:24:39,580
Wenn Sie Zen praktizieren, werden Sie asiatische Zivilisationen verstehen. 

130
00:24:40,660 --> 00:24:55,280
Wird es durch die Begegnung von westlicher Tradition und Zen eine spirituelle Renaissance geben? 

131
00:24:55,620 --> 00:24:59,780
Ich glaube schon. Hoffentlich. 

132
00:25:00,380 --> 00:25:04,780
Es ist wichtig, dass jeder Gott wird. 

133
00:25:05,120 --> 00:25:07,120
Was ist der Weg, um dies zu erreichen? 

134
00:25:07,400 --> 00:25:11,320
Wenn Sie Zazen praktizieren, können Sie Gott werden. 

135
00:25:11,660 --> 00:25:21,300
Ist der wahre Gott der natürliche Gott in uns? 

136
00:25:21,520 --> 00:25:29,520
Ja, jeder muss den wahren Gott erschaffen. 

137
00:25:31,200 --> 00:25:35,180
Die letzte Lehre 

138
00:25:36,600 --> 00:25:40,360
Die Zen-Philosophie ist sehr weit gefasst. 

139
00:25:40,800 --> 00:25:44,980
Sie müssen Widersprüche annehmen! 

140
00:25:45,420 --> 00:25:47,440
Leben und Tod zum Beispiel? 

141
00:25:47,680 --> 00:25:50,400
Ja, Leben und Tod! 

142
00:28:10,080 --> 00:28:27,360
Mokudo Taisen Deshimaru gewidmet (1914-1982) 

