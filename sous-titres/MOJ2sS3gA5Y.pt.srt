1
00:00:04,240 --> 00:00:07,040
O que é Zen? 

2
00:00:07,320 --> 00:00:10,120
Zen é entender a si mesmo. 

3
00:00:10,400 --> 00:00:11,800
Sua própria mente. 

4
00:00:12,080 --> 00:00:14,140
É da conta de todos. 

5
00:00:14,420 --> 00:00:16,500
Era isso que Deshimaru queria transmitir. 

6
00:00:16,800 --> 00:00:19,460
É isso que quero transmitir e perceber. 

7
00:00:19,720 --> 00:00:22,460
Eu tinha visto um discípulo de Kodo Sawaki. 

8
00:00:22,840 --> 00:00:24,720
Ele me disse: 

9
00:00:25,120 --> 00:00:27,620
"Você tem que se olhar!" 

10
00:00:27,960 --> 00:00:29,960
Ele estava certo! 

11
00:00:30,380 --> 00:00:33,300
O que significa um caminho espiritual? 

12
00:00:33,640 --> 00:00:41,880
O significado de um caminho espiritual é "voltar ao sonhador". 

13
00:00:46,020 --> 00:00:48,260
Dogen diz: 

14
00:00:48,520 --> 00:00:51,260
"Estudar o budismo é estudar a si mesmo." 

15
00:00:51,540 --> 00:00:53,980
Estude o sonhador. 

16
00:00:54,380 --> 00:00:57,300
Por que ele sonha assim? 

17
00:00:58,260 --> 00:01:00,240
Como ele pode mudar seu sonho? 

18
00:01:00,540 --> 00:01:02,020
Como ele pode controlar seu sonho? 

19
00:01:02,840 --> 00:01:04,000
Quando o Buda diz: 

20
00:01:04,440 --> 00:01:06,300
"Tudo é um sonho." 

21
00:01:06,620 --> 00:01:09,820
Isso não quer dizer "nada importa". 

22
00:01:10,100 --> 00:01:13,580
Não é o sonho que é importante, é o sonhador. 

23
00:01:13,900 --> 00:01:16,540
Pare de sonhar, é o que chamamos de "despertar" … 

24
00:01:17,180 --> 00:01:21,040
O despertar está se tornando um sonhador. 

25
00:01:21,340 --> 00:01:22,980
Controlando seu sonho. 

26
00:01:24,000 --> 00:01:26,120
Esse é o caminho espiritual. 

27
00:01:26,420 --> 00:01:27,980
Isso é muito difícil. 

28
00:01:28,560 --> 00:01:31,760
Como posso começar? 

29
00:01:32,660 --> 00:01:45,080
A principal motivação para praticar um caminho espiritual é: ser capaz de controlar seu sonho. 

30
00:01:46,700 --> 00:01:49,340
O que é importante no Zen? 

31
00:01:49,920 --> 00:01:55,880
Desta forma, é sua mente que é importante. 

32
00:01:56,280 --> 00:02:01,280
Zazen não é um objeto de adoração. 

33
00:02:01,540 --> 00:02:03,980
O segundo ponto importante de seu ensino: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku. 

35
00:02:07,180 --> 00:02:10,780
O que significa "sem fins lucrativos". 

36
00:02:11,340 --> 00:02:23,340
É a porta que nos leva do espírito de Deus ao mundo material. 

37
00:02:23,640 --> 00:02:27,000
Isso é muito importante. 

38
00:02:27,360 --> 00:02:37,420
Mesmo no mundo cotidiano, mesmo nos fenômenos, você não perde sua concentração. 

39
00:02:37,800 --> 00:02:42,920
Você não perde sua verdadeira natureza. 

40
00:02:43,180 --> 00:02:45,100
Isso é Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
Mas o que é fácil? 

42
00:02:55,780 --> 00:03:00,260
As pessoas sempre seguem o caminho mais fácil. 

43
00:03:00,620 --> 00:03:03,780
Nós caímos nas coisas fáceis. 

44
00:03:10,640 --> 00:03:13,600
Poder é fácil. 

45
00:03:14,060 --> 00:03:19,220
É fácil estar com pessoas que sempre sentem o mesmo. 

46
00:03:21,160 --> 00:03:26,400
Fazer uma igreja com zen é fácil. 

47
00:03:26,800 --> 00:03:32,080
Anexar-se a uma igreja é fácil. 

48
00:03:32,360 --> 00:03:37,300
As pessoas entram em algo seguro. 

49
00:03:37,600 --> 00:03:41,060
Sem nenhum esforço. 

50
00:03:41,420 --> 00:03:44,620
Onde eles nunca são realmente confrontados consigo mesmos. 

51
00:03:44,920 --> 00:03:47,080
Onde eles nunca estão sozinhos. 

52
00:03:47,500 --> 00:03:52,040
Como você vê seus antigos colegas seguidores? 

53
00:03:52,360 --> 00:03:58,720
Admiro muitos dos discípulos do mestre Deshimaru. 

54
00:03:58,980 --> 00:04:07,980
Quem recebeu sua educação e continua praticando sozinho. 

55
00:04:08,240 --> 00:04:14,320
É melhor praticar juntos. 

56
00:04:14,700 --> 00:04:19,140
Mas pelo menos eles fazem suas próprias coisas. 

57
00:04:19,660 --> 00:04:23,980
Eles estão sozinhos. 

