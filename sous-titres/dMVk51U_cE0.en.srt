1
00:00:07,600 --> 00:00:09,910
I will do the initiation.

2
00:00:10,080 --> 00:00:13,570
There is at least one person registered.

3
00:00:13,850 --> 00:00:17,860
Then we will cross the neighborhood!

4
00:00:29,760 --> 00:00:32,305
The protective measures are :

5
00:00:33,225 --> 00:00:36,380
alcohol and gel, several times at the entrance and exit.

6
00:00:36,530 --> 00:00:39,390
The social distance of one and a half meters.

7
00:00:39,550 --> 00:00:44,960
In the entrance and locker rooms, in the dojo.

8
00:00:45,140 --> 00:00:48,740
We wear the mask all the time.

9
00:00:48,910 --> 00:00:52,370
We take it off to do zazen, we keep it in the kimono.

10
00:00:52,450 --> 00:00:55,175
And we put it back on during the kin-hin walk.

11
00:00:55,325 --> 00:00:58,940
We keep the windows open, in spite of the noise or the cold.

12
00:01:00,330 --> 00:01:03,280
Good morning, it's 6:15 a.m.

13
00:01:03,360 --> 00:01:08,110
We go to the dojo to arrive with time to prepare everything.

14
00:01:08,330 --> 00:01:11,490
Enjoy the Barcelona City Tour!

15
00:01:35,120 --> 00:01:37,935
Our dojo on Tordera Street is under repair.

16
00:01:38,005 --> 00:01:44,940
We prepare for the half-day of zazen
directed by Master Pierre Soko at the end of the month.

17
00:01:45,020 --> 00:01:50,790
It is a room in Barcelona where activities related to Buddhism are practiced.

18
00:01:50,900 --> 00:01:56,587
We rented it to continue practicing zazen.

19
00:01:56,700 --> 00:02:00,730
With the covid, we have deprogrammed all activities.

20
00:02:00,990 --> 00:02:08,090
You can't do sesshin, day, or class.

21
00:02:08,470 --> 00:02:14,610
I can't do anything special to spread the practice: we're stuck.

22
00:02:14,880 --> 00:02:19,040
We maintain a practice to the best of our ability.

23
00:02:19,143 --> 00:02:25,460
But we cannot advertise or develop the Zen dojo.

24
00:02:25,760 --> 00:02:30,375
The only advertising we can do is on the web and social networks.

25
00:02:30,536 --> 00:02:34,905
But we don't put posters in the street,
something we've been doing for 20 years.

26
00:02:35,521 --> 00:02:38,825
Good. One more day. Pandemic.

27
00:02:38,955 --> 00:02:43,280
We had a lot of practitioners today, we are happy.

28
00:02:50,000 --> 00:03:00,060
The covid changed my family and professional life.

29
00:03:02,300 --> 00:03:03,950
I am French.

30
00:03:04,041 --> 00:03:12,920
For the coronavirus, I follow the usual precautions:
the mask, washing your hands...

31
00:03:13,440 --> 00:03:15,390
Hello, my name is Helena.

32
00:03:15,500 --> 00:03:18,140
I have been practicing at the Ryokan dojo for 15 years.

33
00:03:18,200 --> 00:03:25,850
It is very difficult for me not to be able to practice at the dojo.

34
00:03:26,160 --> 00:03:30,660
But you have to be careful, accept things as they are.

35
00:03:30,760 --> 00:03:34,080
We are in the middle of the second wave.

36
00:03:34,350 --> 00:03:38,468
I am part of the at-risk group and I have to stay home.

37
00:03:39,100 --> 00:03:42,200
I started practicing Zen at "Zen y playa".

38
00:03:42,440 --> 00:03:48,610
I am an actress and the world of culture is very uncertain...

39
00:03:48,810 --> 00:03:53,970
Hello, my name is David and I am a practitioner at the dojo.

40
00:03:54,070 --> 00:03:59,110
Despite the covid, I feel safe here.

41
00:03:59,230 --> 00:04:01,877
I really want to practice zazen.

42
00:04:02,000 --> 00:04:06,200
I return to the initiation because I used to practice at the Tordera street dojo.

43
00:04:06,520 --> 00:04:09,140
I had to do an initiation again.

44
00:04:09,260 --> 00:04:16,910
Lately, I often feel fear.

45
00:04:20,100 --> 00:04:23,890
I want to both take care of myself and continue to live.

46
00:04:44,720 --> 00:04:49,120
To grow spiritually.

47
00:04:51,800 --> 00:04:59,050
Master Dogen invites us to an altruistic, selfless practice.

48
00:05:16,070 --> 00:05:17,650
Hello.

49
00:05:40,030 --> 00:05:42,400
There you go. End of zazen.

50
00:05:42,660 --> 00:05:46,460
I'm going to take the subway for 40 minutes.

51
00:05:46,690 --> 00:05:51,340
And we'll do it again tomorrow morning at 7:00 a.m. for the first zazen.

52
00:05:52,015 --> 00:05:56,015
It's 9pm and as you can see, Barcelona is deserted.

53
00:05:57,239 --> 00:05:58,639
That's how it is.

54
00:05:58,729 --> 00:06:01,239
Have a good evening, see you soon!
