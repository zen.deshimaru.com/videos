1
00:00:08,980 --> 00:00:16,230
Hola ! Voy a explicarte cómo sentarte a practicar la meditación Zen,
2
00:00:17,200 --> 00:00:19,180
llamado zazen.

3
00:00:19,180 --> 00:00:22,829
El material necesario es muy simple, un ser humano...

4
00:00:23,320 --> 00:00:28,349
y un cojín de meditación. Si tienes un cojín

5
00:00:29,230 --> 00:00:33,689
bastante acolchado, está bien, y si no, haces uno con una manta...

6
00:00:35,320 --> 00:00:39,060
o un saco de dormir por ejemplo, debe ser lo suficientemente firme.

7
00:00:57,490 --> 00:01:04,120
Explico la postura de la meditación Zen.

8
00:01:05,560 --> 00:01:08,940
Lo primero que hay que saber es que es una postura...

9
00:01:09,440 --> 00:01:10,880
base

10
00:01:11,240 --> 00:01:13,440
inmóvil y silencioso.

11
00:01:14,120 --> 00:01:16,660
La particularidad de esta postura

12
00:01:17,300 --> 00:01:19,300
es que lo practicamos

13
00:01:19,400 --> 00:01:21,400
de cara a la pared.

14
00:01:21,800 --> 00:01:27,460
Aquí, por supuesto, me paro frente a la cámara para enseñarte.

15
00:01:27,960 --> 00:01:34,640
Lo que tienes que hacer es sentarte en un cojín, como expliqué, que tiene que ser

16
00:01:35,000 --> 00:01:37,000
suficientemente acolchado, suficientemente duro.

17
00:01:37,300 --> 00:01:44,060
Te sientas bien en el fondo del cojín, no te sientes en el borde que puedes deslizar,

18
00:01:44,440 --> 00:01:48,360
y debes cruzar las piernas lo más lejos posible.

19
00:01:48,820 --> 00:01:56,220
Puedes cruzarlos así, así, así, así, se llama medio loto,

20
00:01:56,660 --> 00:01:59,410
si pones tu otra pierna aquí, se llama loto.

21
00:02:02,860 --> 00:02:07,620
Lo importante es que tus rodillas vayan al suelo,

22
00:02:08,220 --> 00:02:09,660
para empujar,

23
00:02:10,789 --> 00:02:13,420
para aplicar presión de rodilla al suelo.

24
00:02:13,880 --> 00:02:20,079
Entonces, ¿cómo lo hacemos? Hazlo sin ningún esfuerzo físico,

25
00:02:20,720 --> 00:02:25,240
nada muscular en absoluto. Postura de meditación zen, zazen,

26
00:02:25,900 --> 00:02:27,780
es una postura en la que tienes que soltarte.

27
00:02:28,300 --> 00:02:33,000
Para que tus rodillas vayan al suelo

28
00:02:33,920 --> 00:02:37,360
vas a inclinar la pelvis ligeramente

29
00:02:38,440 --> 00:02:42,220
hacia adelante, la parte superior de la pelvis va hacia adelante,

30
00:02:42,760 --> 00:02:46,300
el cóccix retrocede un poco.

31
00:02:46,760 --> 00:02:48,760
Te lo mostraré de perfil

32
00:02:55,700 --> 00:03:03,040
Mi pelvis no está puesta así, como si estuviera conduciendo un coche, por ejemplo,

33
00:03:03,260 --> 00:03:08,700
ves aquí mis rodillas suben por detrás porque mi centro de gravedad

34
00:03:08,900 --> 00:03:15,700
fue detrás de mi cuerpo. Quiero que mi centro de gravedad vaya al frente y lo hago con mi pelvis.

35
00:03:15,940 --> 00:03:24,300
Naturalmente, el centro de gravedad pasa por delante con la gravedad, mis rodillas van al suelo.

36
00:03:24,300 --> 00:03:30,860
Ahí, eso es lo que tienes que establecer primero en la postura,

37
00:03:31,160 --> 00:03:37,040
un asiento en el que las rodillas van al suelo.

38
00:03:37,480 --> 00:03:40,200
Cuando te diste cuenta de que estar sentado

39
00:03:40,800 --> 00:03:45,500
de la pelvis, que está inclinada ligeramente hacia adelante...

40
00:03:45,900 --> 00:03:49,280
vas a enderezar la columna vertebral tanto como sea posible

41
00:03:50,200 --> 00:03:52,860
hasta la parte superior de la cabeza.

42
00:03:54,580 --> 00:04:02,780
Asumiremos que la columna llega hasta la cima. Como si hubiera puesto un gancho

43
00:04:05,060 --> 00:04:10,500
y subo. Lo que significa que estiro mi cuello, meto mi barbilla,

44
00:04:11,150 --> 00:04:15,699
Verás, no tengo la cabeza sobre los hombros, no tengo la espalda encorvada...

45
00:04:16,849 --> 00:04:18,849
Soy tan recto como puedo serlo.

46
00:04:19,520 --> 00:04:22,000
Te lo mostraré de perfil

47
00:04:23,300 --> 00:04:28,040
No soy así, no soy así...

48
00:04:28,560 --> 00:04:35,780
o agachado o lo que sea, soy tan recto como puedo ser.

49
00:04:38,800 --> 00:04:43,820
Cuando has conseguido sentar cabeza, no te mueves.

50
00:04:45,320 --> 00:04:47,160
Yo sigo.

51
00:04:47,380 --> 00:04:49,280
la mano izquierda

52
00:04:49,540 --> 00:04:51,540
los dedos están unidos

53
00:04:52,250 --> 00:04:57,220
los dedos de la mano derecha están unidos y voy a superponer los dedos de la mano izquierda

54
00:04:58,070 --> 00:05:00,070
en los dedos de la mano derecha

55
00:05:01,490 --> 00:05:03,680
Mis pulgares se encuentran

56
00:05:05,730 --> 00:05:08,329
Superpuesto a los dedos de la mano izquierda,

57
00:05:09,630 --> 00:05:11,630
Formo una línea horizontal.

58
00:05:12,690 --> 00:05:20,059
El borde de mis manos desde mi muñeca hasta aquí, el borde de mis manos

59
00:05:21,030 --> 00:05:23,929
vas a aplicarlo en la base del abdomen...

60
00:05:25,280 --> 00:05:27,820
para hacer esto,

61
00:05:29,580 --> 00:05:33,440
usa algo que te permita juntar las manos.

62
00:05:34,160 --> 00:05:36,280
de tal manera que puedas

63
00:05:36,700 --> 00:05:38,620
dejar salir completamente

64
00:05:38,880 --> 00:05:43,300
la tensión en los hombros la postura es una postura en la que tienes que soltarte

65
00:05:44,280 --> 00:05:50,089
libera la tensión, ves que mis codos están libres he estado así o así durante mucho tiempo,

66
00:05:51,380 --> 00:05:53,940
en silencio. Pero no hago ningún esfuerzo

67
00:05:54,540 --> 00:06:00,940
para sostener mis manos, el esfuerzo es para mantener mis pulgares horizontales.

68
00:06:01,220 --> 00:06:03,220
una vez que me di cuenta

69
00:06:03,750 --> 00:06:05,750
estos puntos de postura

70
00:06:06,600 --> 00:06:08,600
No me voy a mover,

71
00:06:09,160 --> 00:06:11,160
Esa es la postura física.

72
00:06:20,060 --> 00:06:27,420
Después de esta primera parte sobre la postura física, la segunda parte: la actitud mental.

73
00:06:28,380 --> 00:06:32,340
Una vez que estoy en una postura de meditación Zen, zazen

74
00:06:33,620 --> 00:06:37,960
de cara a la pared, ¿qué hago, qué hago?

75
00:06:39,720 --> 00:06:42,260
En el silencio de esta postura,

76
00:06:43,770 --> 00:06:48,380
te vas a dar cuenta de que piensas,

77
00:06:49,350 --> 00:06:51,769
o incluso que no puedes dejar de pensar

78
00:06:52,290 --> 00:06:57,469
y hay miles de pensamientos que vienen, vienen, vienen.

79
00:06:57,980 --> 00:07:02,420
Estás a punto de decirme que es un fenómeno natural...

80
00:07:03,500 --> 00:07:05,500
No puedes controlarlo.

81
00:07:05,940 --> 00:07:11,900
Lo que vamos a hacer, durante la meditación Zen, vamos a dejar pasar nuestros pensamientos.

82
00:07:12,740 --> 00:07:15,580
es decir que, en esta inmovilidad..,

83
00:07:16,040 --> 00:07:20,440
podremos observar lo que pensamos,

84
00:07:21,040 --> 00:07:25,960
observará sus pensamientos y estos pensamientos en lugar de entretenerlos,

85
00:07:26,360 --> 00:07:32,860
en lugar de cultivarlas, las vas a dejar pasar.

86
00:07:33,160 --> 00:07:39,220
Así que no se trata de rechazar nada, de nuevo se trata de dejarlo ir, dejarlo ir.

87
00:07:40,080 --> 00:07:42,400
Cómo pasar

88
00:07:43,260 --> 00:07:45,260
atención, concentración,

89
00:07:46,080 --> 00:07:47,860
aquí mismo, ahora mismo,

90
00:07:48,240 --> 00:07:50,240
en tu respiración.

91
00:07:55,930 --> 00:07:58,680
Respirar en Zen es respirar en el abdomen.

92
00:08:00,460 --> 00:08:08,320
Respiras a través, a través de tu fosa nasal, y respiras suavemente,

93
00:08:09,640 --> 00:08:13,780
lentamente, obviamente en silencio, meditación silenciosa.

94
00:08:15,180 --> 00:08:20,180
La característica de esta respiración es que vas a centrar tu atención

95
00:08:20,540 --> 00:08:24,120
en el lado de la exhalación.

96
00:08:24,420 --> 00:08:26,420
Inspiramos

97
00:08:26,620 --> 00:08:33,680
a través de la nariz, en silencio, ser generoso, llenar

98
00:08:34,300 --> 00:08:41,880
y la cara de exhalación cuando vas a exhalar todo el aire lo haces lentamente,

99
00:08:42,340 --> 00:08:46,940
lentamente, como si se fuera a hacer cada vez más largo

100
00:08:56,700 --> 00:09:00,140
Cuando estás al final de tu vida útil...

101
00:09:00,880 --> 00:09:02,380
aspiras

102
00:09:02,850 --> 00:09:06,340
y otra vez expira

103
00:09:07,520 --> 00:09:12,020
y te estás tomando más tiempo, más lento, más profundo,

104
00:09:12,200 --> 00:09:15,760
una exhalación más profunda, como una ola, de nuevo.

105
00:09:16,280 --> 00:09:18,280
a través del océano

106
00:09:18,560 --> 00:09:20,560
viniendo a la playa

107
00:09:22,580 --> 00:09:24,580
Todo esto con la nariz.

108
00:09:30,060 --> 00:09:33,100
Si te concentras aquí y ahora

109
00:09:33,400 --> 00:09:40,880
en los puntos de la postura, los puntos físicos por lo tanto la atención, una atención aquí y ahora en su cuerpo

110
00:09:41,420 --> 00:09:48,960
si te concentras aquí y ahora, al mismo tiempo, en no seguir tus pensamientos,

111
00:09:49,820 --> 00:09:52,640
volviendo a los puntos de esa postura,

112
00:09:52,980 --> 00:09:55,880
si te concentras aquí y ahora

113
00:09:56,460 --> 00:10:02,320
en establecer una respiración armoniosa, yendo y viniendo, y poco a poco

114
00:10:02,900 --> 00:10:06,180
que el tiempo de expiración,

115
00:10:06,800 --> 00:10:10,620
...sales, el aire se hace cada vez más largo.

116
00:10:11,600 --> 00:10:16,240
No tendrás tiempo para pensar en... lo que quieras!

117
00:10:16,800 --> 00:10:22,940
Estás aquí y ahora en una postura de zazen.

118
00:10:23,520 --> 00:10:30,360
En la postura, ni el cuerpo ni la materia, en la postura. Le recuerdo que presione,

119
00:10:30,660 --> 00:10:34,420
...aplicas presión desde tus rodillas al suelo,

120
00:10:34,680 --> 00:10:38,840
empujas la tierra con tus rodillas

121
00:10:38,900 --> 00:10:40,980
y te enderezas

122
00:10:41,500 --> 00:10:43,500
tanto como sea posible la columna vertebral

123
00:10:44,089 --> 00:10:48,999
hasta la parte superior del cráneo como para empujar el cielo con la cabeza.

124
00:10:49,760 --> 00:10:55,270
Mano izquierda en la mano derecha con pulgares horizontales el borde de las manos

125
00:10:55,780 --> 00:10:58,440
contra la parte baja del abdomen.

126
00:10:58,840 --> 00:11:00,820
Estoy inmóvil.

127
00:11:01,190 --> 00:11:03,160
de cara a la pared

128
00:11:03,170 --> 00:11:05,170
De cara a la pared no hay nada que ver especialmente,

129
00:11:05,360 --> 00:11:11,169
lo que voy a hacer en su lugar es volver mi mirada hacia adentro,

130
00:11:11,959 --> 00:11:13,959
así que los sentidos,

131
00:11:14,410 --> 00:11:16,959
No voy a usarlos más durante esta meditación.

132
00:11:17,900 --> 00:11:23,360
Estoy de cara a la pared, así que no hay nada que ver, así que es como si

133
00:11:24,080 --> 00:11:28,640
Tomo mi vista, mi visión, la tomo, la pongo delante de mí, ya no me importa.

134
00:11:29,020 --> 00:11:33,180
Durante la meditación, meditarás con la vista,

135
00:11:34,160 --> 00:11:37,240
45° diagonalmente hacia abajo

136
00:11:38,300 --> 00:11:46,920
Puedes meditar con los ojos cerrados, pero si mantienes los ojos entreabiertos te mantienes en contacto con la realidad.

137
00:11:47,340 --> 00:11:49,980
No vayas especialmente en tus pensamientos.

138
00:11:50,320 --> 00:11:54,380
Como estamos en silencio, tampoco usamos nuestro oído,

139
00:11:54,600 --> 00:11:58,980
así que la audición, no hay necesidad de ella, no hay música, no hay nada.

140
00:11:59,260 --> 00:12:03,740
y ya hemos visto que estamos inmóviles.

141
00:12:04,260 --> 00:12:09,960
Poco a poco, de repente nos aislamos de las demandas del mundo exterior...

142
00:12:10,400 --> 00:12:12,680
nos deshacemos de todo eso.

143
00:12:13,140 --> 00:12:18,920
Meditación, estás sentado físicamente, es el cuerpo el que está en postura,

144
00:12:19,910 --> 00:12:26,350
te aíslas del mundo exterior, pones todo en modo avión.

145
00:12:26,920 --> 00:12:33,740
No medites con el móvil a tu lado.

146
00:13:09,580 --> 00:13:12,000
Para terminar un consejo para principiantes,

147
00:13:12,660 --> 00:13:17,740
no es fácil estar así, confinado ahora,

148
00:13:18,140 --> 00:13:21,720
meditando frente a una pared,

149
00:13:22,820 --> 00:13:25,940
pero, uh, es realmente una buena ayuda.

150
00:13:26,220 --> 00:13:28,640
Realmente te aconsejo que lo practiques,

151
00:13:28,900 --> 00:13:36,940
para probarlo y verás que en lugar de tener pensamientos que pueden no ser muy brillantes,

152
00:13:37,260 --> 00:13:43,920
no muy feliz, en lugar de estar de mal humor, en lugar de sentir tensión, estrés,

153
00:13:44,560 --> 00:13:48,569
todo esto es normal, con este trabajo en el cuerpo.

154
00:13:49,200 --> 00:13:55,500
la actitud de la mente que dejas ir, te alejas de todo lo que te está pasando.

155
00:13:55,940 --> 00:13:58,240
verás que te calmarás

156
00:13:58,560 --> 00:13:59,900
físicamente

157
00:14:00,100 --> 00:14:01,480
emocionalmente

158
00:14:01,660 --> 00:14:04,820
la respiración, todo eso, te ayudará a soltarte...

159
00:14:05,280 --> 00:14:09,880
te ayudará mucho a ti y a tu familia si vives con alguien.

160
00:14:10,340 --> 00:14:14,480
Así que no dudes en practicarlo. Gracias. 