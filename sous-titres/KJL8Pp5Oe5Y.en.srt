1
00:01:31,170 --> 00:01:35,220
In Soto Zen, which we practice, we say: "Don’t seek great enlightenment".

2
00:01:35,560 --> 00:01:38,407
Enlightenment, in itself, does not exist.

3
00:01:38,407 --> 00:01:40,170
Why would she exist?

4
00:01:40,780 --> 00:01:45,720
You have to look at nature and things as they are to see the truth.

5
00:01:46,200 --> 00:01:49,663
Illumination is absolutely natural.

6
00:01:49,663 --> 00:01:52,780
It’s not a special condition.

7
00:01:53,320 --> 00:01:59,400
In Zen, there is no special condition in enlightenment unless there is a disease of the mind.

8
00:01:59,700 --> 00:02:04,760
There is no special enlightenment per se and one should not seek one.

9
00:02:05,540 --> 00:02:07,433
Many masters insist on it.

10
00:02:07,433 --> 00:02:08,681
This is extremely important.

11
00:02:08,681 --> 00:02:11,000
Enlightenment is at every moment, at every zazen.

12
00:02:11,320 --> 00:02:15,580
I was talking to a specialist in psychotropic drugs…

13
00:02:15,790 --> 00:02:20,700
designed to achieve special states of consciousness.

14
00:02:21,130 --> 00:02:24,686
He maintained that zazen was a means to achieve enlightenment.

15
00:02:24,686 --> 00:02:25,889
That’s not true at all.

16
00:02:26,350 --> 00:02:31,379
Zazen is illumination itself.

17
00:02:32,410 --> 00:02:33,254
Dogen said:

18
00:02:33,254 --> 00:02:36,269
"I went to China and met a real master."

19
00:02:37,390 --> 00:02:41,729
"What I learned from it is that the eyes are horizontal and the nose is vertical."

20
00:02:42,370 --> 00:02:45,810
"And that one should not be fooled, either by oneself or others."

21
00:03:44,750 --> 00:03:46,750
and every muscle in the belly

22
00:03:47,269 --> 00:03:52,358
should be relaxed to give the impression that the intestines are weighing down.

23
00:04:09,010 --> 00:04:15,420
if we concentrate completely in the posture at that moment we can let the thoughts

24
00:04:15,850 --> 00:04:17,850
without stopping there.

25
00:04:25,810 --> 00:04:28,914
The most beautiful thing you can do with your body is zazen.

26
00:04:28,914 --> 00:04:30,419
This is the splendour of nature.

27
00:04:30,849 --> 00:04:33,728
It inspires calm, harmony, strength.

28
00:04:33,728 --> 00:04:36,539
Everything is contained in the posture of zazen

29
00:04:39,969 --> 00:04:43,447
At summer camp I try to encourage practice.

30
00:04:43,447 --> 00:04:46,111
The atmosphere in the dojo, the postures.

31
00:04:46,111 --> 00:04:47,909
A person who disturbs that

32
00:04:50,540 --> 00:04:53,940
with her personal history, I’m willing to kill her.

33
00:04:57,120 --> 00:04:59,120
It’s the only rule I know: compassion.

34
00:05:00,600 --> 00:05:02,477
Sensei was showing off his kyosaku:

35
00:05:02,477 --> 00:05:04,160
"the staff of compassion!"

36
00:05:04,660 --> 00:05:11,080
Sometimes he’d hit 20 shots in a row.

37
00:05:14,440 --> 00:05:19,640
He’d say, "Sensei, very compassionate!"

38
00:05:26,280 --> 00:05:32,940
When a disciple wants to be helped in his meditation by being hit on the shoulder with a stick, he asks for it.

39
00:05:33,180 --> 00:05:36,800
Because he feels his attention starting to wane.

40
00:05:37,140 --> 00:05:41,360
When you feel sleepy.

41
00:05:41,580 --> 00:05:46,300
Or when you feel complications clouding your brain.

42
00:05:46,540 --> 00:05:49,520
Not so quiet anymore.

43
00:05:49,940 --> 00:05:54,600
If you feel sleepy or if your mind is restless…

44
00:06:01,240 --> 00:06:03,240
Gasshô!

45
00:06:21,580 --> 00:06:23,580
Zazen continues during

46
00:06:23,840 --> 00:06:25,233
for several hours every day.

47
00:06:25,233 --> 00:06:25,929
And sometimes, day and night.

48
00:06:26,180 --> 00:06:30,291
But he is interrupted by a meditation while walking.

49
00:06:30,291 --> 00:06:33,489
That is, the inner state remains,

50
00:06:34,280 --> 00:06:38,799
but the posture is different.

51
00:06:39,940 --> 00:06:44,040
All the attention is focused on the thumb.

52
00:06:44,600 --> 00:06:47,500
Each breath corresponds to half a step.

53
00:06:58,880 --> 00:07:05,020
Eyes are on the ground 3 meters in front.

54
00:07:13,340 --> 00:07:18,580
It is the same breathing as in zazen, during the meditation in sitting posture.

55
00:07:20,740 --> 00:07:28,300
Zen breathing is the opposite of normal breathing.

56
00:07:28,560 --> 00:07:30,940
When you ask someone to breathe,

57
00:07:31,240 --> 00:07:34,600
the reflex is to take a deep breath.

58
00:07:35,060 --> 00:07:38,860
In zen, we’re going to focus

59
00:07:39,080 --> 00:07:42,740
especially on the exhalation.

60
00:07:43,040 --> 00:07:46,780
The exhalation is long, calm and deep.

61
00:07:54,360 --> 00:07:56,160
It’s anchored in the ground.

62
00:07:56,160 --> 00:07:58,860
It can last up to one, two minutes.

63
00:07:59,080 --> 00:08:02,940
Depending on his state of concentration and calm.

64
00:08:03,280 --> 00:08:09,600
It can be very soft, imperceptible.

65
00:08:09,860 --> 00:08:15,640
We first study this breathing by insisting, like a cow mooing.

66
00:08:38,020 --> 00:08:44,640
At the end of the exhalation, we let the inspiration take place automatically.

67
00:08:45,880 --> 00:08:49,640
We relax everything, open the lungs.

68
00:08:50,300 --> 00:08:55,380
We let the inspiration come automatically.

69
00:08:55,820 --> 00:08:58,689
We don’t put a lot of intention into inspiration.

70
00:08:59,600 --> 00:09:03,129
By doing so, you will understand

71
00:09:04,550 --> 00:09:06,550
that breathing is everything.

72
00:09:07,910 --> 00:09:09,910
It’s a circle.

73
00:09:10,070 --> 00:09:12,070
Often masters draw a circle.

74
00:09:12,560 --> 00:09:14,560
It’s about breathing.

75
00:09:15,040 --> 00:09:18,880
That breathing had a lot of influence,

76
00:09:19,340 --> 00:09:23,200
especially in the martial arts.

77
00:09:23,480 --> 00:09:29,160
In Japanese martial arts, Zen breathing is used.

78
00:09:29,580 --> 00:09:34,840
Many great martial arts masters such as Miyamoto Musashi

79
00:09:35,180 --> 00:09:39,640
practiced Zen and became disciples of great masters.

80
00:09:39,940 --> 00:09:45,480
And have adapted the teaching of Zen and breathing to their martial art.

81
00:09:45,820 --> 00:09:53,860
In the end, the breathing is beyond the techniques and is totally free.

82
00:09:58,080 --> 00:10:03,340
The Buddha’s teaching to his disciples is very simple.

83
00:10:03,640 --> 00:10:09,440
It is based on the fundamental acts of the human being.

84
00:10:09,660 --> 00:10:11,260
How to move.

85
00:10:11,480 --> 00:10:14,120
How to eat, how to breathe.

86
00:10:14,440 --> 00:10:15,836
How to think.

87
00:10:15,836 --> 00:10:20,860
And finally, how to sit down to realize his divinity.

88
00:11:24,940 --> 00:11:28,702
Genmai has been the food of monks since Shakyamuni Buddha.

89
00:11:28,702 --> 00:11:31,800
We mix the vegetables we have on hand with rice.

90
00:11:32,280 --> 00:11:39,440
We cook for a very long time, with a lot of attention and concentration.

91
00:11:39,780 --> 00:11:42,730
It’s a food passed down from the Buddha.

92
00:11:42,730 --> 00:11:45,920
In China and Japan, we eat the same food.

93
00:11:47,660 --> 00:11:49,980
It really is the body of the Buddha.

94
00:11:50,280 --> 00:11:53,080
When we’re about to have our meal,

95
00:11:53,440 --> 00:11:55,560
you have to ask yourself the question:

96
00:11:55,900 --> 00:11:59,460
"Why am I going to eat?"

97
00:11:59,680 --> 00:12:01,580
An animal or a lout

98
00:12:01,960 --> 00:12:05,680
isn’t going to be wondering and jumping up and down on his plate.

99
00:12:06,040 --> 00:12:09,820
Obviously it’s for survival, but survival for what?

100
00:12:10,060 --> 00:12:13,520
That’s what it says in the sutras that are sung during meals.

101
00:12:13,780 --> 00:12:15,240
"Why are we eating?"

102
00:12:15,520 --> 00:12:17,591
"Where does this food come from?

103
00:12:17,800 --> 00:12:18,860
Who prepared it?"

104
00:12:19,180 --> 00:12:21,140
"Who grew the rice and vegetables?"

105
00:12:21,400 --> 00:12:26,140
"Who did the cooking?"

106
00:12:26,500 --> 00:12:29,960
We dedicate the meal and thank all these people.

107
00:12:30,240 --> 00:12:32,140
And in full consciousness,

108
00:12:32,620 --> 00:12:37,340
I decide to eat this food for

109
00:12:37,780 --> 00:12:44,620
the evolution I have come to accomplish on this earth, in this material world.

110
00:12:52,360 --> 00:12:56,900
EMS is at 10:30. 6 people for the kitchen.

111
00:13:01,080 --> 00:13:04,080
Two people for the big dive.

112
00:13:05,940 --> 00:13:10,340
Three people for the service.

113
00:13:19,600 --> 00:13:23,580
Samu isn’t especially doing masonry, sweeping up…

114
00:13:24,190 --> 00:13:27,989
or do the dishes.

115
00:13:28,540 --> 00:13:30,540
Samu is all action

116
00:13:30,760 --> 00:13:32,939
(Writing a book is a samu)

117
00:13:35,320 --> 00:13:37,320
carried out without

118
00:13:37,640 --> 00:13:38,860
intention

119
00:13:39,180 --> 00:13:40,160
hidden

120
00:13:44,950 --> 00:13:47,249
no secret motives.

121
00:13:47,980 --> 00:13:50,048
It’s like your job in everyday life.

122
00:13:50,048 --> 00:13:52,289
You can take it as personal enrichment

123
00:13:52,839 --> 00:13:57,749
or as a kind of gift that you give to yourself and everyone else at the same time.

124
00:13:59,320 --> 00:14:02,460
Sometimes I consider

125
00:14:03,400 --> 00:14:05,459
Some people may take it as a chore.

126
00:14:05,920 --> 00:14:09,390
But I bet those hurt in zazen.

127
00:14:16,400 --> 00:14:19,550
As a Zen nun,

128
00:14:23,040 --> 00:14:26,180
what attitude should you take at work?

129
00:14:27,080 --> 00:14:30,100
I don’t know, I’ve never worked!

130
00:14:33,960 --> 00:14:37,060
Do we have to make a samu out of him?

131
00:14:38,500 --> 00:14:39,520
Yes, that’s the point.

132
00:14:39,740 --> 00:14:41,100
I was talking crap, huh.

133
00:14:41,300 --> 00:14:47,500
When I was younger I had a job as a janitor.

134
00:14:48,090 --> 00:14:54,920
I cleaned the stairs and took out the trash every morning at 5:00.

135
00:14:55,140 --> 00:15:00,619
The stairs, it was a hooray. There was dog shit.

136
00:15:00,920 --> 00:15:05,360
I thought, "But this isn’t the dojo or Zen."

137
00:15:05,760 --> 00:15:09,760
"Never mind. For me, it’s like a temple."

138
00:15:10,320 --> 00:15:14,160
I did it like a samurai.

139
00:15:14,480 --> 00:15:17,040
And Sensei asked me:

140
00:15:17,420 --> 00:15:20,080
"Stéphane, I’d like you to clean the dojo."

141
00:15:20,400 --> 00:15:23,660
"Yes, I will. Friday I’m free."

142
00:15:25,440 --> 00:15:27,920
"I’ll do it next Friday."

143
00:15:28,580 --> 00:15:29,660
He said to me:

144
00:15:29,940 --> 00:15:32,500
"No, I want you to do it every day."

145
00:15:33,120 --> 00:15:34,272
Then he said:

146
00:15:34,520 --> 00:15:37,440
"A day without work, a day without food."

147
00:15:37,860 --> 00:15:40,720
"But it has nothing to do with food!"

148
00:15:41,000 --> 00:15:44,440
"A day without working, a day without eating mushotoku!"

149
00:15:44,780 --> 00:15:47,380
It was a very original expression:

150
00:15:47,700 --> 00:15:50,380
"You must eat mushotoku!"

151
00:15:54,760 --> 00:15:57,540
It didn’t mean anything!

152
00:15:58,180 --> 00:16:02,500
Saying you’re a monk, practicing samu is revolution.

153
00:16:02,880 --> 00:16:05,880
We must make the revolution, I’ve always said that!

154
00:16:06,920 --> 00:16:10,657
But I don’t know how to use Molotov cocktails!

155
00:16:11,000 --> 00:16:14,100
So, in his work, you have to practice samu.

156
00:16:14,400 --> 00:16:19,780
To solve all the world’s problems, all you have to do is find this.

157
00:16:20,100 --> 00:16:22,060
People don’t have the spirit of the samu anymore.

158
00:16:22,520 --> 00:16:23,920
They don’t eat mushotoku anymore.

159
00:16:24,160 --> 00:16:25,580
They eat hamburger and drink Coke.

160
00:16:26,180 --> 00:16:28,880
So, whoever says to you:

161
00:16:29,580 --> 00:16:31,580
But it’s 6:30, you’re the only one left!

162
00:16:31,880 --> 00:16:33,140
You tell him:

163
00:16:33,360 --> 00:16:36,160
"Me, I practice samu!"

164
00:16:45,640 --> 00:16:47,640
"Because I want to eat mushotoku!"

165
00:16:49,780 --> 00:16:51,200
"What’s mushotoku?"

166
00:16:51,440 --> 00:16:52,780
"I want some too!"

167
00:16:57,040 --> 00:17:01,200
We have to teach people without being afraid.

168
00:17:03,180 --> 00:17:06,660
Be proud to be my disciples and to be monks!

169
00:17:07,000 --> 00:17:09,000
And to be disciples of Deshimaru.

