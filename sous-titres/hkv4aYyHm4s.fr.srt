1
00:00:13,360 --> 00:00:15,750
Un conte conté par Maître Deshimaru

2
00:00:17,320 --> 00:00:19,320
qui s’appelle "Se promener dans la montagne".

3
00:00:19,980 --> 00:00:21,980
Alors déjà rien que le titre,

4
00:00:22,560 --> 00:00:24,080
c’est merveilleux,

5
00:00:24,609 --> 00:00:30,360
pouvoir se promener dans la montagne. Un maître se promenait dans la montagne et à son retour

6
00:00:31,179 --> 00:00:33,179
un de ses disciples lui demanda

7
00:00:33,700 --> 00:00:35,700
"Maître, où êtes-vous allé vous promener?"

8
00:00:35,980 --> 00:00:40,420
"Dans la montagne" répondit le maître et le disciple insista "Mais quel chemin

9
00:00:40,900 --> 00:00:43,380
avez vous pris, qu’avez vous vu?"

10
00:00:45,260 --> 00:00:52,940
Le maître lui répondit "J’ai suivi l’odeur des fleurs et j’ai flâné selon les jeunes pousses printanières."

11
00:00:55,480 --> 00:00:57,480
Maître Deshimaru continue

12
00:00:58,540 --> 00:01:02,180
"Il faut se laisser guider par le Dharma du Bouddha

13
00:01:02,380 --> 00:01:03,820
faire confiance

14
00:01:04,140 --> 00:01:08,040
aux herbes et aux fleurs qui poussent sans but,

15
00:01:08,600 --> 00:01:10,220
sans égoïsme,

16
00:01:10,440 --> 00:01:15,640
naturellement, inconsciemment. Cette réponse venait de la source de la sagesse

17
00:01:16,900 --> 00:01:20,560
la véritable sagesse doit être créée au delà du savoir

18
00:01:21,100 --> 00:01:26,240
et de la mémoire." C’est un enseignement assez puissant .

19
00:01:26,700 --> 00:01:29,160
Dans un kusen,

20
00:01:29,340 --> 00:01:37,040
l’enseignement donné dans le dojo, Maître Kosen enseigne : "La philosophie de Shikantaza ,

21
00:01:37,900 --> 00:01:40,580
la philosophie de ’seulement zazen’

22
00:01:40,800 --> 00:01:47,380
doit guider notre vie c’est la véritable essence du bouddhisme, la méthode la plus parfaite

23
00:01:48,249 --> 00:01:50,699
zazen lui-même est satori, l’éveil.

24
00:01:51,520 --> 00:01:53,520
On se trompe

25
00:01:54,159 --> 00:01:56,368
si on pense qu’il y a un zazen avant

26
00:01:57,100 --> 00:02:02,460
et qu’avec l’accumulation des zazen, zazen, quinze jours, il se passe quelque chose, l’éveil.

27
00:02:05,300 --> 00:02:11,520
Immédiatement n’importe quelle personne qui pratique zazen déclenche le processus de l’éveil.

28
00:02:13,020 --> 00:02:15,800
Bon, ce processus ne correspond pas à nos catégories

29
00:02:16,900 --> 00:02:18,900
parfois les gens disent

30
00:02:18,959 --> 00:02:20,959
’ le zazen était très très dur,

31
00:02:21,420 --> 00:02:24,949
c’était un mauvais zazen’ ou parfois

32
00:02:25,860 --> 00:02:27,860
’c’est très confortable’

33
00:02:28,409 --> 00:02:35,929
et le processus qui déclenche le zazen est au delà nos catégories ou de notre conscience personnelle."

34
00:02:36,660 --> 00:02:42,169
Donc la méthode de Dogen, celle des autres Maîtres, elle est géniale elle est unique.

35
00:02:43,799 --> 00:02:48,769
C’est des enseignements qui paraissent très simples que l’on reçoit,

36
00:02:50,190 --> 00:02:52,190
que l’on reçoit pendant zazen

37
00:02:53,400 --> 00:02:55,400
et que l’on va

38
00:02:58,530 --> 00:03:02,750
intégrer c’est à dire qu’on va se les approprier,

39
00:03:04,019 --> 00:03:09,319
doucement, profondément, inconsciemment, c’est des enseignements précieux.
