1
00:00:07,490 --> 00:00:10,656
As pessoas pensam que o zazen é uma técnica … 

2
00:00:10,656 --> 00:00:13,173
quem os ajudará em suas vidas. 

3
00:00:13,173 --> 00:00:16,800
Eles fazem disso uma coisa ridiculamente pequena. 

4
00:00:16,800 --> 00:00:21,820
e faça zazen na esperança de que isso os ajude em suas vidas. 

5
00:00:21,820 --> 00:00:31,280
Isso é verdade, porque neste momento muito consciente …, 

6
00:00:31,280 --> 00:00:35,300
o mais enérgico, o mais presente que existe …. 

7
00:00:35,300 --> 00:00:39,820
… temos plena influência em todos os fenômenos da nossa vida, isso é certo. 

8
00:00:40,020 --> 00:00:46,240
Mas não estamos fazendo isso para melhorar nosso ego … 

9
00:00:46,250 --> 00:00:50,720
Para nos acalmar …. 

10
00:00:50,720 --> 00:00:52,880
Porque nos fará bem …. 

11
00:00:52,880 --> 00:00:58,500
Porque nos poupará algumas horas de sono …. 

12
00:00:58,500 --> 00:01:02,759
Dando-nos maior desempenho sexual …. 

13
00:01:02,759 --> 00:01:06,100
Ou ajude-nos a lidar com o estresse. 

14
00:01:06,100 --> 00:01:11,500
Isso reduz o zazen …. 

15
00:01:11,500 --> 00:01:16,230
no nível de um instrumento para nossa própria vida ilusória. 

16
00:01:16,230 --> 00:01:23,760
Mas é o contrário. 

17
00:01:23,920 --> 00:01:28,980
Vamos focar nossas vidas para gerar o zazen mais eficaz. 

18
00:01:30,890 --> 00:01:33,880
Sente-se na posição de Buda, 

19
00:01:33,880 --> 00:01:39,380
é o momento mais intenso e mais consciente. 

20
00:01:39,380 --> 00:01:43,020
É disso que se trata. 

21
00:01:43,020 --> 00:01:46,340
Tudo se junta: o passado, o presente, o futuro. 

22
00:01:46,340 --> 00:01:52,060
Todos os fenômenos da nossa vida estão no zazen. 

23
00:01:52,060 --> 00:02:00,040
É no zazen que temos poder, consciência, 

24
00:02:06,620 --> 00:02:12,020
mudar fundamentalmente os meandros de nossas vidas. 

25
00:02:16,320 --> 00:02:21,240
No zazen, estamos em uma atitude digna. 

26
00:02:21,240 --> 00:02:25,380
Uma atitude de heróis, Buda, Deus. 

27
00:02:25,500 --> 00:02:31,320
Somos heterossexuais, fortes, bonitos, calmos, calmos, calmos, imóveis. 

28
00:02:31,320 --> 00:02:36,600
Você não deve adormecer, deve esticar a coluna. 

29
00:02:36,600 --> 00:02:41,980
Você tem que respirar, relaxar. 

30
00:02:46,640 --> 00:02:50,740
No zazen, há tudo, tudo o que somos. 

31
00:02:50,740 --> 00:02:55,800
Todo o nosso passado, nosso presente e indiretamente nosso futuro. 

32
00:02:55,980 --> 00:03:05,460
Existe o nosso corpo, com suas fraquezas, sua fadiga, 

33
00:03:05,600 --> 00:03:11,200
suas impurezas, suas toxinas, 

34
00:03:11,209 --> 00:03:14,900
suas doenças, seu carma, etc. 

35
00:03:14,900 --> 00:03:21,380
Existe a nossa mente, a nossa consciência. 

36
00:03:21,380 --> 00:03:25,055
Tudo está unido no zazen. 

37
00:03:25,055 --> 00:03:29,320
A atitude zazen, 

38
00:03:29,320 --> 00:03:33,880
é a auto-imagem mais bonita que você pode mostrar. 

39
00:03:40,620 --> 00:03:46,090
Eu conheci o Mestre Deshimaru, ele me ensinou a zazen, eu me sentei em zazen. 

40
00:03:46,090 --> 00:03:48,620
E eu disse imediatamente: "Este é o melhor!" 

41
00:04:01,360 --> 00:04:05,680
Para saber o que você cria, qual é a realidade que você cria … 

42
00:04:05,680 --> 00:04:10,680
Você é quem cria a realidade. Você vive o seu sonho. 

43
00:04:10,680 --> 00:04:15,920
Todo mundo está vivendo seu sonho, e você tem que realizar o seu sonho, é isso. 

44
00:04:15,920 --> 00:04:19,480
Para mim, o Zazen sempre foi assim. 

