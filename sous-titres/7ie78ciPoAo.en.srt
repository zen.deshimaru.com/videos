1
00:00:00,100 --> 00:00:02,020
Zen is zazen! 

2
00:00:02,220 --> 00:00:04,920
Zazen sits like a Buddha. 

3
00:00:05,160 --> 00:00:08,400
Take the original Buddha pose. 

4
00:00:08,620 --> 00:00:09,920
Take his heart. 

5
00:00:10,140 --> 00:00:12,360
Sit in this position. 

6
00:00:12,660 --> 00:00:16,000
And gradually forget that we are a painful body. 

7
00:00:16,320 --> 00:00:18,840
All twisted, with a complicated mind. 

8
00:00:19,080 --> 00:00:23,120
And let the transmutation happen, the alchemy of zazen. 

9
00:00:23,760 --> 00:00:25,700
Only people can exercise. 

10
00:00:26,060 --> 00:00:33,200
It is our evolution over hundreds of thousands of years that makes it possible to practice this attitude. 

11
00:00:33,520 --> 00:00:36,700
Balance, stability, calm. 

12
00:00:37,000 --> 00:00:40,200
Stretch, open up, forget yourself. 

13
00:00:40,480 --> 00:00:46,960
Unity with all beings, with everything, with the entire universe. 

14
00:00:47,740 --> 00:00:51,760
Master Kosen says that zazen is the world heritage of humanity. 

15
00:00:52,040 --> 00:00:54,600
We are not going to force someone to do zazen. 

16
00:00:54,860 --> 00:00:56,220
And sometimes it is not the right time. 

17
00:00:56,460 --> 00:00:57,960
We can sit down at some point. 

18
00:00:58,220 --> 00:01:02,940
And miss what it could mean for our lives. 

19
00:01:03,220 --> 00:01:07,460
And then at another moment, suddenly, it is there. 

20
00:01:07,680 --> 00:01:09,560
There is a question and an answer. 

21
00:01:09,760 --> 00:01:11,120
That is exactly what we should practice. 

22
00:01:11,360 --> 00:01:17,460
Doing zazen only once, even if it’s not your thing, if you are receptive, if you feel like it, it gives you information. 

23
00:01:18,700 --> 00:01:21,140
There are many different practices. 

24
00:01:21,380 --> 00:01:22,880
Which is definitely good. 

25
00:01:23,160 --> 00:01:25,700
You can’t want to put things together. 

26
00:01:26,000 --> 00:01:29,940
Zazen is a rather demanding practice. 

27
00:01:30,240 --> 00:01:32,700
You don’t just take a stand. 

28
00:01:33,040 --> 00:01:38,440
Posture and the accompanying mood are essential. 

29
00:01:39,160 --> 00:01:43,900
But zazen has its own life, its own functioning. 

30
00:01:44,160 --> 00:01:46,220
It is also called a "meditation". 

31
00:01:46,420 --> 00:01:47,980
But it is only a word. 

32
00:01:48,320 --> 00:01:51,580
We call it "zazen", "Buddha’s attitude". 

33
00:01:51,800 --> 00:01:53,700
It’s just words. 

34
00:01:53,940 --> 00:01:56,180
But that is the attitude we practice. 

35
00:01:56,840 --> 00:01:59,220
We do not call ourselves a teacher or master. 

36
00:01:59,500 --> 00:02:03,040
Even if you have taken 1, 2, 3 courses. 

37
00:02:03,300 --> 00:02:06,140
There is an intimate relationship, you follow a master. 

38
00:02:06,380 --> 00:02:09,460
We don’t follow him everywhere he goes. 

39
00:02:09,780 --> 00:02:12,920
We follow his teaching, his mind. 

40
00:02:13,200 --> 00:02:14,760
There is zazen itself. 

41
00:02:15,000 --> 00:02:22,300
Then, don’t follow your own thing, your own idea. 

42
00:02:22,580 --> 00:02:25,120
But to follow something bigger. 

43
00:02:25,560 --> 00:02:29,480
Something that spans generations. 

44
00:02:29,860 --> 00:02:34,060
Since the historical Buddha and before him. 

45
00:02:34,380 --> 00:02:36,960
There are other Buddhas who have preceded him. 

46
00:02:37,240 --> 00:02:40,020
Others who sat in the pose. 

47
00:02:40,360 --> 00:02:42,720
Others who have reached the path. 

48
00:02:43,500 --> 00:02:45,520
And it is serious! 

49
00:02:45,840 --> 00:02:59,580
If you liked this video, feel free to like it and subscribe to the channel! 

