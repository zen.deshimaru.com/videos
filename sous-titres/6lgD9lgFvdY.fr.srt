1
00:00:00,800 --> 00:00:06,680
Kin-hin "La marche méditative du Zen"

2
00:00:07,600 --> 00:00:09,140
Bonjour

3
00:00:09,400 --> 00:00:12,060
Aujourd’hui, je voudrais expliquer

4
00:00:12,060 --> 00:00:16,600
comment se pratique la marche de méditation zen

5
00:00:16,820 --> 00:00:20,300
que nous appelons "kin hin".

6
00:00:20,400 --> 00:00:23,340
Cette marche est faite

7
00:00:23,340 --> 00:00:26,515
entre deux périodes de méditation assise.

8
00:00:26,520 --> 00:00:31,140
En général, une séance de méditation zen

9
00:00:31,140 --> 00:00:36,060
commence avec trente ou quarante minutes de zazen.

10
00:00:36,360 --> 00:00:38,920
Dans la posture que j’ai expliquée 
(dans une autre vidéo) :

11
00:00:38,920 --> 00:00:41,740
tranquille, silencieux,

12
00:00:41,740 --> 00:00:43,500
devant le mur.

13
00:00:43,660 --> 00:00:46,240
Ainsi, cette marche

14
00:00:46,240 --> 00:00:49,860
est fait pendant 5 ou 10 minutes.

15
00:00:50,140 --> 00:00:53,420
et à la fin de cette marche,

16
00:00:53,480 --> 00:00:58,480
nous retournons à l’endroit où nous avons notre coussin de méditation

17
00:00:58,700 --> 00:01:04,160
pour s’asseoir en zazen, debout devant le mur.

18
00:01:04,620 --> 00:01:08,580
Comment marche-t-on quand on fait "Kin hin" ?

19
00:01:08,900 --> 00:01:11,480
Puisque nous marchons,

20
00:01:11,480 --> 00:01:13,780
Allons-y.

21
00:01:14,000 --> 00:01:16,820
Comment se déplacer ?

22
00:01:17,660 --> 00:01:20,740
Nous avons mis les pieds dans le plat.

23
00:01:21,380 --> 00:01:23,500
Ce n’est pas comme ça.

24
00:01:23,800 --> 00:01:25,500
Ce n’est pas comme ça.

25
00:01:25,500 --> 00:01:28,480
Parallèle et alignée.

26
00:01:28,480 --> 00:01:34,580
Séparés les uns des autres, à la distance d’un poing.

27
00:01:35,300 --> 00:01:40,420
Déplaçons l’équivalent d’un demi-pied.

28
00:01:40,880 --> 00:01:47,180
Un pied mesure environ 30 ou 35 centimètres.

29
00:01:47,640 --> 00:01:50,820
On va donc faire des pas de 6 ou 8 pouces

30
00:01:51,000 --> 00:01:52,300
tout au plus.

31
00:01:52,600 --> 00:01:55,555
Si vous regardez mes pieds,

32
00:01:55,560 --> 00:01:59,100
vous verrez mes pas se dérouler comme ça.

33
00:01:59,240 --> 00:02:02,120
Nous disons un demi-pied.

34
00:02:08,120 --> 00:02:09,120
Eh bien...

35
00:02:09,520 --> 00:02:12,425
Comment allons-nous marcher ?

36
00:02:12,425 --> 00:02:16,620
Marchons avec notre souffle.

37
00:02:17,540 --> 00:02:23,840
Notre respiration nous donne le rythme.

38
00:02:25,100 --> 00:02:28,320
Inspirez.

39
00:02:28,320 --> 00:02:31,720
C’est le même type de respiration que nous faisons pendant zazen.

40
00:02:31,980 --> 00:02:34,760
Nous inspirons...

41
00:02:34,760 --> 00:02:38,720
Pendant cette période où nous prenons l’air,

42
00:02:38,720 --> 00:02:42,040
une jambe en avant

43
00:02:44,955 --> 00:02:47,875
Pendant l’expiration,

44
00:02:47,875 --> 00:02:51,075
lente et profonde,

45
00:02:51,075 --> 00:02:53,725
en poussant vers le bas

46
00:02:54,100 --> 00:02:56,960
en poussant les intestins vers le bas

47
00:02:57,265 --> 00:02:59,440
Pendant l’expiration

48
00:02:59,780 --> 00:03:02,120
par le nez,

49
00:03:02,660 --> 00:03:07,340
nous mettrons tout le poids sur le corps

50
00:03:07,520 --> 00:03:10,800
dans la jambe avant.

51
00:03:11,140 --> 00:03:15,160
La jambe avant est tendue.

52
00:03:15,160 --> 00:03:18,700
toute la plante du pied reposant sur le sol,

53
00:03:18,700 --> 00:03:21,220
y compris le talon.

54
00:03:21,620 --> 00:03:23,980
La patte arrière,

55
00:03:23,980 --> 00:03:26,660
qui n’a pas de poids,

56
00:03:27,060 --> 00:03:31,880
est maintenu en liberté.

57
00:03:31,880 --> 00:03:34,300
Mais aussi,

58
00:03:34,300 --> 00:03:36,340
toute la plante du pied,

59
00:03:36,360 --> 00:03:42,660
y compris le talon, reste posée sur le sol.

60
00:03:42,660 --> 00:03:46,540
nous ne levons pas les talons.

61
00:03:47,465 --> 00:03:48,595
Donc,

62
00:03:48,920 --> 00:03:53,260
J’inspire et je fais le pas.

63
00:03:53,460 --> 00:03:57,460
Expiration longue et profonde.

64
00:03:57,660 --> 00:04:00,500
Pendant l’expiration

65
00:04:00,720 --> 00:04:06,100
transférer le poids du corps sur la jambe avant,

66
00:04:06,100 --> 00:04:11,420
à la racine du gros doigt de devant,

67
00:04:11,520 --> 00:04:15,140
comme s’il voulait laisser une marque sur le sol.

68
00:04:15,300 --> 00:04:21,380
Imaginez que le sol est de l’argile ou du sable.

69
00:04:22,380 --> 00:04:27,620
L’empreinte de votre pied serait alors enregistrée

70
00:04:27,880 --> 00:04:30,900
Inspiration : je fais le pas.

71
00:04:31,340 --> 00:04:37,740
Expiration : transfert du poids du corps à la jambe avant

72
00:04:37,940 --> 00:04:40,420
J’ai atteint la fin de mon expiration.

73
00:04:40,660 --> 00:04:45,600
Je n’ai pas d’autre choix que de reprendre un peu d’air.

74
00:04:45,640 --> 00:04:52,060
Je vais passer la jambe qui est plus en arrière, en avant.

75
00:04:52,300 --> 00:04:55,600
Je transfère le poids du corps

76
00:04:55,600 --> 00:04:58,680
sur la jambe qui a fait le mouvement

77
00:04:58,680 --> 00:05:02,260
et encore sur l’autre jambe.

78
00:05:07,340 --> 00:05:10,620
Le rythme de notre respiration

79
00:05:10,640 --> 00:05:15,300
donner le rythme de la marche

80
00:05:15,860 --> 00:05:17,800
Évidemment,

81
00:05:17,800 --> 00:05:25,100
nous gardons la même attitude mentale que pendant la méditation assise

82
00:05:25,360 --> 00:05:27,920
Je me suis concentré ici et maintenant,

83
00:05:27,920 --> 00:05:30,000
dans ce que je fais.

84
00:05:30,000 --> 00:05:32,400
Je ne suis pas mes pensées.

85
00:05:32,400 --> 00:05:35,305
Même souffle.

86
00:05:35,305 --> 00:05:39,960
Seule l’activité physique a changé.

87
00:05:39,960 --> 00:05:45,060
"La posture de la main"

88
00:05:46,360 --> 00:05:48,380
Pendant que nous marchons,

89
00:05:48,540 --> 00:05:51,660
que faisons-nous de nos mains ?

90
00:05:52,355 --> 00:05:55,455
quand on est devant le mur,

91
00:05:55,765 --> 00:05:58,545
nous avons cette attitude

92
00:06:00,020 --> 00:06:02,935
Lâchez votre main droite.

93
00:06:02,940 --> 00:06:05,800
Laissez votre main gauche.

94
00:06:05,920 --> 00:06:08,760
Je vais envelopper mon pouce avec mes doigts.

95
00:06:08,940 --> 00:06:11,920
Tous les doigts ensemble,

96
00:06:12,100 --> 00:06:17,020
en laissant ressortir la racine du pouce gauche.

97
00:06:17,260 --> 00:06:19,400
Main droite.

98
00:06:19,540 --> 00:06:22,580
Tous les doigts ensemble

99
00:06:22,580 --> 00:06:28,800
Les doigts de la main droite impliquent la main gauche.

100
00:06:29,560 --> 00:06:33,180
Je vais me serrer un peu les poignets.

101
00:06:33,760 --> 00:06:36,720
afin que les deux avant-bras

102
00:06:36,720 --> 00:06:39,720
sont dans la même ligne.

103
00:06:39,720 --> 00:06:45,780
Je n’ai pas un avant-bras plus haut que l’autre.

104
00:06:47,300 --> 00:06:51,860
La racine du pouce gauche

105
00:06:52,640 --> 00:06:57,260
est en contact avec le plexus solaire.

106
00:06:57,820 --> 00:06:59,740
La base du sternum.

107
00:06:59,980 --> 00:07:03,860
Chaque être humain a un petit trou ici,

108
00:07:03,880 --> 00:07:05,760
à la base du sternum.

109
00:07:05,780 --> 00:07:14,160
c’est là que vous devez entrer en contact avec la racine de votre pouce.

110
00:07:15,060 --> 00:07:18,640
Mes avant-bras sont parallèles au sol.

111
00:07:18,700 --> 00:07:22,420
Mes mains aussi.

112
00:07:22,820 --> 00:07:25,020
Pas comme ça.

113
00:07:25,480 --> 00:07:30,080
Je garde cette posture avec les épaules détendues.

114
00:07:30,200 --> 00:07:33,360
Je ne garde pas cette posture avec mes épaules comme ça :

115
00:07:33,400 --> 00:07:35,360
avec les coudes qui montent.

116
00:07:35,500 --> 00:07:38,260
mais pas comme ça non plus.

117
00:07:40,820 --> 00:07:42,880
Comme en zazen :

118
00:07:42,940 --> 00:07:44,600
colonne droite,

119
00:07:44,680 --> 00:07:46,020
la nuque étendue,

120
00:07:46,360 --> 00:07:48,320
Le menton est entré,

121
00:07:48,340 --> 00:07:51,920
Regardez la 45e diagonale.

122
00:07:54,520 --> 00:07:57,320
Maintenant, comment vais-je marcher ?

123
00:07:57,900 --> 00:08:02,600
Pendant l’inspiration,

124
00:08:02,600 --> 00:08:04,120
nous faisons un pas en avant.

125
00:08:06,800 --> 00:08:09,660
Dans l’expiration

126
00:08:10,840 --> 00:08:15,740
...vous transférez le poids du corps d’une jambe à l’autre.

127
00:08:15,820 --> 00:08:18,060
comme je l’ai expliqué.

128
00:08:18,060 --> 00:08:20,240
En même temps,

129
00:08:20,240 --> 00:08:25,740
vous allez serrer légèrement la main.

130
00:08:25,740 --> 00:08:30,500
c’est pourquoi la flexibilité des poignets est importante.

131
00:08:31,980 --> 00:08:33,560
En même temps,

132
00:08:33,680 --> 00:08:38,235
une légère pression de la racine du pouce gauche

133
00:08:38,235 --> 00:08:41,405
contre le plexus solaire.

134
00:08:42,020 --> 00:08:45,120
Tout cela pendant l’expiration.

135
00:08:46,120 --> 00:08:51,540
Resumen

136
00:08:53,040 --> 00:08:56,500
Main gauche, pouce, doigts joints,

137
00:08:56,520 --> 00:08:58,760
main droite,

138
00:08:58,760 --> 00:08:59,580
au sommet.

139
00:08:59,780 --> 00:09:04,000
La racine du pouce gauche sur le sternum.

140
00:09:04,160 --> 00:09:08,880
Je suis une source d’inspiration.

141
00:09:09,760 --> 00:09:13,680
Ma jambe va de l’avant

142
00:09:14,140 --> 00:09:16,120
Phase d’expiration :

143
00:09:16,300 --> 00:09:17,340
En combien

144
00:09:17,480 --> 00:09:22,120
Je déplace tout le poids de mon corps sur cette jambe,

145
00:09:22,120 --> 00:09:24,480
bien tendu,

146
00:09:24,800 --> 00:09:27,920
presse à main

147
00:09:27,920 --> 00:09:30,340
et contre le plexus solaire.

148
00:09:31,280 --> 00:09:32,620
L’inspiration.

149
00:09:32,860 --> 00:09:35,620
Un pas en avant.

150
00:09:35,620 --> 00:09:37,840
Expiration longue et profonde

151
00:09:38,020 --> 00:09:41,300
le poids sur la jambe avant.

152
00:09:41,320 --> 00:09:45,940
Pression entre les mains et contre le plexus solaire.

153
00:09:48,440 --> 00:09:51,180
Fin de l’expiration

154
00:09:51,320 --> 00:09:55,640
Je vais faire un pas et profiter de l’occasion pour me détendre.

155
00:09:55,740 --> 00:09:57,500
J’y vais encore...

156
00:09:57,540 --> 00:10:00,660
Je presse le terrain

157
00:10:00,820 --> 00:10:03,815
avec le poids de mon corps.

158
00:10:03,820 --> 00:10:05,040
la pression entre les mains

159
00:10:05,240 --> 00:10:07,380
et contre le plexus.

160
00:10:07,420 --> 00:10:09,460
Les pressions sont douces.

161
00:10:09,460 --> 00:10:13,000
Il ne s’agit pas de se blesser.

162
00:10:14,140 --> 00:10:15,840
Tout cela,

163
00:10:15,840 --> 00:10:20,340
guidés par le rythme de notre propre respiration,

164
00:10:20,340 --> 00:10:22,460
pendant 5 ou 10 minutes

165
00:10:22,660 --> 00:10:24,920
c’est que nous revenons à ce moment-là,

166
00:10:24,920 --> 00:10:28,980
à l’endroit où se trouve notre coussin de méditation.

167
00:10:29,280 --> 00:10:30,280
Dans la vie de tous les jours,

168
00:10:30,560 --> 00:10:34,740
cette marche méditative appelée "Kin hin"

169
00:10:35,560 --> 00:10:37,940
est très pratique, très efficace.

170
00:10:38,960 --> 00:10:43,040
Vous êtes peut-être dans un moment d’attente

171
00:10:43,520 --> 00:10:46,075
ou en période de stress.

172
00:10:46,075 --> 00:10:47,805
Et vous pouvez vous isoler

173
00:10:48,200 --> 00:10:50,860
pendant 2, 3 ou 5 minutes.

174
00:10:50,980 --> 00:10:54,340
Où allez-vous vous connecter avec l’ici et maintenant

175
00:10:54,815 --> 00:10:57,965
Dans la conscience de votre souffle

176
00:10:57,965 --> 00:11:03,840
et en tenant compte des points que j’ai expliqués.

177
00:11:04,620 --> 00:11:07,755
Pratiquer cette marche de méditation

178
00:11:07,760 --> 00:11:15,480
vous verrez à quelle vitesse vous vous sentirez moins stressé

179
00:11:15,480 --> 00:11:21,880
plus détendue et en harmonie.

180
00:11:22,880 --> 00:11:27,820
J’espère que vous pourrez faire un zazen complet maintenant.

181
00:11:27,820 --> 00:11:31,060
C’est-à-dire une première partie de séance,

182
00:11:31,060 --> 00:11:32,055
alors,

183
00:11:32,055 --> 00:11:34,855
5 ou 10 minutes de cette marche

184
00:11:35,360 --> 00:11:38,960
et revenir à une deuxième partie de la méditation.

185
00:11:39,140 --> 00:11:40,300
Merci beaucoup.
