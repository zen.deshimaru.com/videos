1
00:00:00,290 --> 00:00:08,820
Ich bin die Nonne Gyu Ji, die vor fast 40 Jahren von meinem Meister ordiniert wurde.

2
00:00:08,820 --> 00:00:16,440
und außerdem bin ich von meinem Meister, Meister Kosen, zertifiziert, von dem ich das so genannte Shihô erhalten habe.

3
00:00:16,720 --> 00:00:21,580
was mir erlaubt
mich für den Unterricht öffnen

4
00:00:21,880 --> 00:00:25,200
zur Weitergabe von Zen beitragen.

5
00:00:25,520 --> 00:00:29,880
Ich lebe im Tempel, was bedeutet, dass ich derjenige bin, der die

6
00:00:30,220 --> 00:00:35,400
außerhalb von Sesshin das Leben dieses Tempels,

7
00:00:35,680 --> 00:00:40,680
die dafür sorgt, dass es erhalten bleibt, dass es Menschen außerhalb von Sesshin aufnehmen kann...

8
00:00:40,680 --> 00:00:43,700
damit er sein ganzes Leben ausleben kann
das Jahr.

9
00:00:43,700 --> 00:00:49,950
Wir befinden uns im Caroux, anderthalb Stunden von Montpellier und eine Stunde von Béziers entfernt.

10
00:00:49,950 --> 00:00:55,370
Wir sind mitten in der Natur, es ist ein Tempel...
das gibt es nun schon seit zehn Jahren.

11
00:00:55,370 --> 00:01:03,390
Also, das Leben im Yujo Nyusanji Tempel ist...
einfach, es ist rhythmisch.

12
00:01:03,390 --> 00:01:09,600
Im Rhythmus der Zazen-Praxis stehen wir morgens auf, wir gehen ins Dojo, wir machen Zazen, das, was uns gelehrt wird.

13
00:01:09,600 --> 00:01:15,450
die Abstammung der Meister, was immer es ist 
Meister Kosen, der am Ursprung steht

14
00:01:15,450 --> 00:01:21,180
dieses Tempels, sein Meister Meister Deshimaru und
wir gehen nach oben, wir gehen nach oben, wir gehen wieder nach oben, wir gehen wieder nach oben

15
00:01:21,180 --> 00:01:26,700
Shakyamuni-Buddha. Zazen ist
das Zentrum der Praxis dieses Tempels

16
00:01:26,700 --> 00:01:33,960
es ist eine Sitzung, eine stille Meditation, eine Meditation in Ruhe

17
00:01:33,960 --> 00:01:42,570
wo, Körperhaltung und Zustand
des Geistes in Harmonie sind.

18
00:01:42,570 --> 00:01:47,820
Das Leben im Tempel ist auch ein Leben in Gemeinschaft mit anderen,

19
00:01:48,140 --> 00:01:52,220
die Momente, in denen wir das Samu machen, d.h. die Aufgabenteilung...

20
00:01:52,500 --> 00:02:00,140
ob es ums Kochen, Gärtnern, Mauern, alles, was es zu tun gibt, von
damit dieser Tempel leben kann.

21
00:02:00,420 --> 00:02:06,960
Aber es ist ein offener Ort, es ist kein Ort
wo Sie verpflichtet sind, sich zu engagieren

22
00:02:06,960 --> 00:02:12,680
im Buddhismus, wo man gezwungen ist, eine religiöse Person zu werden...

23
00:02:12,680 --> 00:02:18,109
es ist ein Ort für alle. Außerdem sagte uns Meister Kosen einmal

24
00:02:18,109 --> 00:02:23,390
wir sind Laienmönche, es ist wichtig, dass wir uns mit

25
00:02:23,390 --> 00:02:28,579
für das Leben eines jeden Menschen, einschließlich unserer Praxis,

26
00:02:28,579 --> 00:02:33,280
dass unsere Praxis die treibende Kraft ist, die uns dazu bringt, ein Leben für alle zu führen
die Tage.

27
00:02:33,280 --> 00:02:40,010
Der Tempel ist der Ort, wo wir hingehen.
einige Zeit damit verbringen, seine Praxis zu vertiefen,

28
00:02:40,010 --> 00:02:45,900
einen Schritt zurückzutreten
in Bezug auf sein tägliches Leben,

29
00:02:46,440 --> 00:02:51,280
Es könnte nur ein Wochenende sein. Es könnte...
es könnte ein Monat sein, es könnten auch drei Monate sein.

30
00:02:51,780 --> 00:02:59,500
Es ist ein Durchgang der Tempel, es ist ein
Moment in seinem Leben, in seinem Jahr.

31
00:03:00,580 --> 00:03:05,180
Zum Leben des Tempels gehören auch Rückzugsmöglichkeiten, das so genannte Sesshin.

32
00:03:05,180 --> 00:03:11,540
Jetzt sind wir in der Unterzahl,
es sind intensivere Perioden

33
00:03:11,540 --> 00:03:16,820
Zazen natürlich auch mit Samu zu praktizieren.

34
00:03:17,340 --> 00:03:27,520
Die Besonderheit dieses Tempels besteht darin, dass wir nicht über die Optik verfügen, um etwas zu imitieren, was bereits existiert.

35
00:03:27,820 --> 00:03:33,300
es ist ein Zen, wie Meister Deshimaru es gelehrt hat, sehr kreativ,

36
00:03:33,840 --> 00:03:37,800
mit dem, was wir sind, mit dem, was wir haben.

37
00:03:38,260 --> 00:03:46,120
Auch in diesem Tempel werden Rituale praktiziert, allerdings nur sparsam,

38
00:03:46,440 --> 00:03:51,360
Wie Meister Kosen sagen würde, ist die richtige Dosis weder zu viel noch zu wenig.

39
00:03:51,900 --> 00:03:56,820
Ich lebe vor Ort und heiße Sie willkommen.
