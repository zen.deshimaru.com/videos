1
00:00:08,480 --> 00:00:13,559
Olá pessoal, vou explicar como você 

2
00:00:13,559 --> 00:00:20,760
sente-se para praticar a meditação zen chamada zazen, o material necessário 

3
00:00:20,760 --> 00:00:27,090
é muito simples um ser humano e uma garota de meditação, se você tem um 

4
00:00:27,090 --> 00:00:30,869
almofada em vez de Hamburgo e isso é bom 

5
00:00:30,869 --> 00:00:35,730
e se não você faz um com um cobertor onde um saco de dormir por 

6
00:00:35,730 --> 00:00:41,060
exemplo, deve ser firme o suficiente 

7
00:00:57,490 --> 00:01:06,070
Então, de acordo com você, explica a postura de meditação Zen, a meditação Zen é 

8
00:01:06,070 --> 00:01:13,620
acima de tudo, saber que é uma postura sentada imóvel e silenciosa 

9
00:01:13,620 --> 00:01:22,630
e a particularidade dessa postura e que praticamos de frente para a parede aqui 

10
00:01:22,630 --> 00:01:26,620
Obviamente, eu encaro a câmera para informá-lo 

11
00:01:26,620 --> 00:01:32,560
então o que você tem a fazer é sentar juntos amortecer o que eu 

12
00:01:32,560 --> 00:01:37,479
explique-o que precisa ser suficientemente acolchoado, insuficientemente duro, você 

13
00:01:37,479 --> 00:01:42,520
sente-se bem no fundo da garota, não sente na borda, se você tiver 

14
00:01:42,520 --> 00:01:49,330
escorregou e você deve, tanto quanto possível cruzar as pernas para 

15
00:01:49,330 --> 00:01:55,600
atravessá-los como estes como este sozinho e é chamado de 2010 se 

16
00:01:55,600 --> 00:02:04,270
você coloca suas outras pessoas aqui, é chamado de lótus, o importante é que 

17
00:02:04,270 --> 00:02:13,480
seus joelhos vão ao chão para pressionar colocar pressão dos joelhos no chão 

18
00:02:13,480 --> 00:02:19,780
Então, como fazer isso para fazê-lo sem nenhuma organização ou sem nenhum esforço 

19
00:02:19,780 --> 00:02:27,810
bons músculos físicos em toda a postura de meditação zen do que deixar ir 

20
00:02:27,810 --> 00:02:37,950
para que seus joelhos caiam, você inclina a pélvis levemente 

21
00:02:37,950 --> 00:02:46,180
encaminhar o topo da pelve avança como 6 a um pouco passado eu 

22
00:02:46,180 --> 00:02:49,440
mostra para você no perfil 

23
00:02:55,200 --> 00:03:02,769
minha pelve não é cínica como se, por exemplo, eu estivesse dirigindo 

24
00:03:02,769 --> 00:03:07,629
um carro vê lá, mas eu mostro-nos por último 

25
00:03:07,629 --> 00:03:11,860
porque meu centro de gravidade está atrás do meu corpo, eu quero 

26
00:03:11,860 --> 00:03:19,420
parece alta qualidade oi na frente e eu faço isso em pontes Dassin naturalmente o 

27
00:03:19,420 --> 00:03:24,970
o que gostaríamos e passar na frente com gravidade meus joelhos vão para o chão 

28
00:03:24,970 --> 00:03:32,970
é isso que você deve começar estabelecendo na postura um assento 

29
00:03:32,970 --> 00:03:42,609
em que o joelho entra em greve quando você realiza 7 a 6 da pelve 

30
00:03:42,609 --> 00:03:44,920
que se inclinou levemente para fazer a solicitação 

31
00:03:44,920 --> 00:03:51,940
você endireitará a coluna o máximo possível 

32
00:03:51,940 --> 00:03:57,000
topo da cabeça considerar o nosso são considerados que 

33
00:03:57,000 --> 00:04:05,889
Colônia vai tão longe quanto se eu colocasse um jogo não é um gancho, reli 

34
00:04:05,889 --> 00:04:12,280
o que significa que eu puxei a nuca, encaixei meu queixo, vejo que não tenho a 

35
00:04:12,280 --> 00:04:16,349
cabeça girando na frente eu não tenho uma volta curvada 

36
00:04:16,349 --> 00:04:24,610
Sou o mais direto possível. Mostro dois perfis. Não sou 

37
00:04:24,610 --> 00:04:33,910
como se eu não estivesse em leilão antes ou o que eu mais sou 

38
00:04:33,910 --> 00:04:43,659
da cruz também quando você conseguiu sentar em linha reta você não se move 

39
00:04:43,659 --> 00:04:53,080
quanto mais eu continuo a mão esquerda os dedos estão unidos a mão direita 

40
00:04:53,080 --> 00:04:58,690
os dedos são abençoados e eu vou sobrepor os dedos esquerdos no 

41
00:04:58,690 --> 00:05:08,030
faixas da mão direita, mas os impulsos se encontram sobrepõem o dedo do 

42
00:05:08,030 --> 00:05:14,300
mão esquerda uma linha horizontal forma a borda das minhas mãos 

43
00:05:14,300 --> 00:05:17,840
para que pudéssemos o condado por todo o pulso 

44
00:05:17,840 --> 00:05:23,480
até então a ponta das minhas mãos amanhã você aplicará na base 

45
00:05:23,480 --> 00:05:31,370
do abdômen para fazer isso usou algo que 

46
00:05:31,370 --> 00:05:37,460
pode permitir que você pare nas mãos do terrasson que você pode liberar 

47
00:05:37,460 --> 00:05:43,780
atenção total dos ombros a postura de uma postura onde você tem que deixar ir 

48
00:05:43,780 --> 00:05:47,990
liberar a tensão que você vê meus cotovelos estão livres, eu sou um longo tempo 

49
00:05:47,990 --> 00:05:56,090
assim ou assim calmamente, mas eu não faço um esforço para manter 

50
00:05:56,090 --> 00:06:01,070
minhas mãos as idéias de esforço para manter os empurrões no restaurante 

51
00:06:01,070 --> 00:06:08,650
uma vez que percebo que este é o ponto de postura, eu toco 

52
00:06:08,650 --> 00:06:13,120
isso é postura física 

53
00:06:20,000 --> 00:06:25,460
após esta primeira parte na postura física segunda parte 

54
00:06:25,460 --> 00:06:30,860
a atitude mental, uma vez que estou na postura de 

55
00:06:30,860 --> 00:06:39,220
meditação zen de frente para a parede o que eu faço o que devo fazer 

56
00:06:39,220 --> 00:06:46,190
no silêncio dessa postura você irá outros 

57
00:06:46,190 --> 00:06:53,390
perceber que você acha que vê que continua pensando e que existem 

58
00:06:53,390 --> 00:06:58,490
Milhares de pincéis de pensamento que vêm e que dizem que são enviados a você 

59
00:06:58,490 --> 00:07:03,010
vamos perceber que este é um fenômeno natural 

60
00:07:03,010 --> 00:07:08,840
você não pode controlar o que faremos durante a meditação zen 

61
00:07:08,840 --> 00:07:16,790
vamos deixar nossos pensamentos passarem, ou seja, nessa imobilidade 

62
00:07:16,790 --> 00:07:23,150
seremos capazes de observar que com um resgate você observará seus pensamentos e 

63
00:07:23,150 --> 00:07:29,419
pensamentos em vez de alimentá-los em vez de cultivá-los você vai 

64
00:07:29,419 --> 00:07:34,610
deixá-los passar, então não se trata de recusar o que 

65
00:07:34,610 --> 00:07:39,580
deixe estar lá novamente, esta é a máquina que você deixará passar 

66
00:07:39,580 --> 00:07:46,190
como deixar passar a atenção 

67
00:07:46,190 --> 00:07:51,400
aqui agora em seu hálito 

68
00:07:55,430 --> 00:07:59,960
respirar zen é respiração abdominal 

69
00:07:59,960 --> 00:08:10,400
você respira pela narina e respira lentamente lentamente 

70
00:08:10,400 --> 00:08:14,270
recesso silencioso meditação silenciosa 

71
00:08:14,270 --> 00:08:20,090
a característica que respira e você vai chamar sua atenção 

72
00:08:20,090 --> 00:08:30,930
diante da exaltação, inspiramos por chegarmos quietos ao sermos 

73
00:08:30,930 --> 00:08:37,620
generoso preencher e enfrentar a nação quando 

74
00:08:37,620 --> 00:08:44,070
você sai todo o ar que você faz lentamente devagar como para 

75
00:08:44,070 --> 00:08:47,720
ficar mais e mais 

76
00:08:56,540 --> 00:09:03,889
quando você está no final de seus relacionamentos, você anseia e novamente 

77
00:09:03,889 --> 00:09:12,709
você expira e não estava mais lentamente mais nu, por mais tempo 

78
00:09:12,709 --> 00:09:18,069
profundamente novo como as ondas em um oceano 

79
00:09:18,069 --> 00:09:31,639
porque eles são a praia e tudo isso fala com a v6 você foca 

80
00:09:31,639 --> 00:09:36,440
aqui agora nos pontos da postura pontos físicos 

81
00:09:36,440 --> 00:09:42,259
então cuidado cuidado aqui agora seu corpo se você 

82
00:09:42,259 --> 00:09:47,660
focado aqui agora, ao mesmo tempo, para não seguir o seu 

83
00:09:47,660 --> 00:09:55,040
pensamentos, vamos voltar aos pontos que postulam se você se concentrar aqui 

84
00:09:55,040 --> 00:10:03,139
agora em estabelecer uma respiração valente harmoniosa e pouco a pouco 

85
00:10:03,139 --> 00:10:11,110
obter o togo a respiração expirar mais e mais 

86
00:10:11,110 --> 00:10:18,860
mas as nadadeiras não têm mais tempo para pensar no que você está aqui e 

87
00:10:18,860 --> 00:10:27,860
agora na postura de Zaza aqui está na postura nem a matéria corporal em 

88
00:10:27,860 --> 00:10:35,569
postura lembro que você pressiona gelo um joelho de pressão em direção ao 

89
00:10:35,569 --> 00:10:38,680
solo você empurra a terra 

90
00:10:38,680 --> 00:10:44,920
e endireite sua coluna o máximo possível para o topo da 

91
00:10:44,920 --> 00:10:49,260
crânio como se empurrasse o céu com a cabeça 

92
00:10:49,260 --> 00:10:55,290
a mão esquerda, como à direita, os polegares horizontais, o gume das mãos 

93
00:10:55,290 --> 00:11:05,020
contra a bola bem, eu ainda estou em saumur de frente para a parede não há nada a fazer 

94
00:11:05,020 --> 00:11:11,459
especialmente o que vou fazer é virar meu olhar para dentro 

95
00:11:11,459 --> 00:11:16,330
então os sentidos não serão mais usados ​​durante 

96
00:11:16,330 --> 00:11:22,060
este fato meditação eu estou de frente para a parede, então não há nada para ver, por isso é 

97
00:11:22,060 --> 00:11:29,890
como se minha visão eu colocasse na minha frente no meu ocupado, portanto, durante 

98
00:11:29,890 --> 00:11:38,620
meditação houve a vista de 45 ° na diagonal para a Europa você pode 

99
00:11:38,620 --> 00:11:43,839
medite com os olhos fechados, mas se você mantiver os olhos abertos ou o último 

100
00:11:43,839 --> 00:11:50,100
contato com a realidade não vai especialmente em seus pensamentos 

101
00:11:50,100 --> 00:11:56,110
como estão em silêncio por nós também não usamos a água lá, então 

102
00:11:56,110 --> 00:12:00,880
ouvir isso não precisa de um pouco de música e já vimos que somos 

103
00:12:00,880 --> 00:12:09,040
Pokémon tão pouco a pouco de repente cortamos com as solicitações do mundo 

104
00:12:09,040 --> 00:12:15,880
fora fazemos a meditação tiro você senta 

105
00:12:15,880 --> 00:12:21,970
fisicamente é o corpo que está na postura que você se corta 

106
00:12:21,970 --> 00:12:28,360
solicitações com as barracas você coloca tudo no modo avião instantâneo não me diga 

107
00:12:28,360 --> 00:12:34,340
não medite com seu celular ao lado 

108
00:13:09,540 --> 00:13:14,399
Portanto, para completar uma dica para iniciantes, não é fácil 

109
00:13:14,399 --> 00:13:23,519
encontrar assim confidencia agora não é nem t como armadura, mas é 

110
00:13:23,519 --> 00:13:28,579
realmente uma boa ajuda você realmente eu aconselho você a 

111
00:13:28,579 --> 00:13:33,959
pratique experimentando e você verá, em vez de pensamentos divertidos 

112
00:13:33,959 --> 00:13:38,790
que eles talvez não estejam prontos brilhantes talvez não estejam prontos alegres 

113
00:13:38,790 --> 00:13:43,380
estar de mau humor ou no centro das atenções do estresse tudo isso é 

114
00:13:43,380 --> 00:13:52,620
normal aqui com este trabalho sobre o ouro a atitude da mente que você deixar ir 

115
00:13:52,620 --> 00:13:57,810
você se distancia de tudo o que acontece conosco para ir ver que você 

116
00:13:57,810 --> 00:14:03,329
você vai se acalmar respirando fisicamente emocionalmente tudo isso 

117
00:14:03,329 --> 00:14:08,310
isso o ajudará a deixar ir, isso ajudará profundamente você e seu 

118
00:14:08,310 --> 00:14:13,079
comitiva se você foi acompanhado quando não duvidou da 

119
00:14:13,079 --> 00:14:15,529
praticar 

