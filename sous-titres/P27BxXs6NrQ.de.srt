1
00:00:00,000 --> 00:00:02,200
Viele Menschen interessieren sich für Meditation … 

2
00:00:02,200 --> 00:00:04,220
… und suche auf Youtube nach Informationen. 

3
00:00:04,420 --> 00:00:07,860
Das Problem ist, dass wir nicht viel davon sehen. Meister Deshimarus Zen … 

4
00:00:07,980 --> 00:00:10,900
… noch die Kosen Sangha. 

5
00:00:11,100 --> 00:00:13,180
Viele talentierte Praktizierende … 

6
00:00:13,360 --> 00:00:16,300
… machte Kosen Sangha Ketten. 

7
00:00:16,520 --> 00:00:18,660
Aber die breite Öffentlichkeit wundert sich: 

8
00:00:18,940 --> 00:00:21,080
Welches ist das richtige? 

9
00:00:21,300 --> 00:00:23,600
Jetzt werden wir alles miteinander verketten: 

10
00:00:23,820 --> 00:00:26,840
bit.ly/chosen 

11
00:00:28,420 --> 00:00:31,160
Wenn Sie also so vielen Menschen wie möglich Zazen vorstellen möchten: 

12
00:00:32,260 --> 00:00:33,980
Abonnieren Sie den Kanal. 

13
00:00:33,980 --> 00:00:35,220
Sehen Sie sich die Videos an. 

14
00:00:35,220 --> 00:00:36,520
Bitte kommentieren. 

15
00:00:36,700 --> 00:00:38,700
Und zögern Sie nicht, sie zu mögen! 

