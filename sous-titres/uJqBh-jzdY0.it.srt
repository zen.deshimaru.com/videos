1
00:00:06,180 --> 00:00:09,540
Non mi interessa lo Zen. 

2
00:00:09,840 --> 00:00:12,100
È solo un altro pezzo di rifiuti della chiesa. 

3
00:00:12,480 --> 00:00:17,520
Ci sono migliaia di maestri Zen negli Stati Uniti. 

4
00:00:17,820 --> 00:00:20,880
Tutti vogliono essere un maestro Zen. 

5
00:00:21,220 --> 00:00:24,680
E lo Zen è di gran moda, 

6
00:00:25,280 --> 00:00:28,820
e non sono interessato. 

7
00:00:29,320 --> 00:00:31,960
Perché pensi che sia di gran moda? 

8
00:00:32,260 --> 00:00:36,240
No, ora è fuori moda! 

9
00:00:37,300 --> 00:00:40,060
In Argentina, tutti acquistano un piccolo giardino zen. 

10
00:00:40,340 --> 00:00:42,720
Lo abbiamo messo in ufficio, nel suo salotto. 

11
00:00:43,020 --> 00:00:45,740
È bellissimo È divertente. È divertente. 

12
00:00:46,100 --> 00:00:49,000
Ma lo Zen è inutile! 

13
00:00:53,900 --> 00:00:57,360
Cosa ne pensi dell’espressione di Krishnamurti: 

14
00:00:57,640 --> 00:01:01,060
"Non permettere a nessuno di mettersi tra te e la verità"? 

15
00:01:01,380 --> 00:01:07,140
Se c’è una discrepanza tra te e la verità, non è la verità. 

16
00:01:11,480 --> 00:01:16,780
Voleva dire che era sempre controcorrente. 

17
00:01:17,040 --> 00:01:21,960
Si era separato dall’intera società teologica, che voleva prenderlo come riferimento. 

18
00:01:22,300 --> 00:01:24,260
Lo ammiri di lui? 

19
00:01:24,540 --> 00:01:31,440
Sì. Mia madre, quando era incinta di me, era una discepola di Krishnamurti. 

20
00:01:31,760 --> 00:01:37,500
Un giorno, durante una conferenza, sentì un’estasi, un satori. 

21
00:01:37,760 --> 00:01:42,620
Si disse: "mio figlio diventerà un santo". 

22
00:01:43,020 --> 00:01:50,060
Krishnamurti mi piace molto, era una persona straordinaria. 

23
00:01:51,640 --> 00:01:56,940
Sai Baba? Non lo so, non lo conosco. 

24
00:01:57,360 --> 00:02:02,020
È l’unico insegnante che abbia mai sentito dire che era Dio. 

25
00:02:02,320 --> 00:02:04,680
Ma tutti sono Dio! 

26
00:02:05,740 --> 00:02:11,540
Un vero maestro non deve dire "Io sono Dio". 

27
00:02:11,940 --> 00:02:16,200
Ma devi dire agli altri che sei Dio. 

28
00:02:16,500 --> 00:02:22,740
Quindi l’uomo dice: "Io sono Dio. Dammi i tuoi soldi!" 

29
00:02:23,060 --> 00:02:27,960
E il fatto che faccia trucchi come far apparire gli orologi, indovina? 

30
00:02:28,240 --> 00:02:32,940
Non lo so Non lo so Non lo so Sì, forse ha una forza. Sì è possibile 

31
00:02:33,080 --> 00:02:34,400
È possibile? Sì è possibile 

32
00:02:34,680 --> 00:02:37,040
Ci sono maghi che dicono che è illusionismo. 

33
00:02:37,320 --> 00:02:40,820
È anche possibile che fosse uno stratagemma. 

34
00:02:40,980 --> 00:02:48,180
Penso che tutte le religioni, tutte le chiese, siano contro l’uomo. 

35
00:02:48,280 --> 00:02:50,280
Tutti, senza distinzione? 

36
00:02:51,740 --> 00:02:54,760
Anche buddismo, tutti. 

37
00:02:55,020 --> 00:02:57,820
Queste sono chiese 

38
00:02:58,400 --> 00:03:00,400
Non mi piace 

39
00:03:02,740 --> 00:03:08,100
Il vero Zen di Deshimaru non è una chiesa, è una scuola. 

40
00:03:08,480 --> 00:03:10,060
È qualcos’altro 

41
00:03:10,380 --> 00:03:12,380
Come puoi dire la differenza? 

42
00:03:12,900 --> 00:03:16,740
La chiesa, sono i giapponesi! 

43
00:03:20,060 --> 00:03:26,000
In una chiesa la gente cerca una via spirituale. 

44
00:03:26,400 --> 00:03:33,240
E dopo 10 anni stanno cercando una laurea, in una gerarchia, un potere sugli altri. 

45
00:03:33,560 --> 00:03:42,620
È una cosa terribile che uccide la verità. 

46
00:03:43,160 --> 00:03:45,680
Pratico zazen. 

47
00:03:46,040 --> 00:03:49,820
Pratico gli insegnamenti dei Buddha, il che è molto semplice. 

48
00:03:50,120 --> 00:03:54,520
Per me è un tesoro dell’umanità. 

49
00:03:56,500 --> 00:03:58,640
Zazen [meditazione seduta]. 

50
00:03:58,960 --> 00:04:02,180
Kinhin. [Cammina lentamente] 

51
00:04:02,700 --> 00:04:04,700
Sampai. 

52
00:04:04,900 --> 00:04:09,020
Genmai [pasto del mattino] e samu [concentrazione sul lavoro]. 

53
00:04:09,360 --> 00:04:10,880
È molto semplice. 

54
00:04:11,180 --> 00:04:12,880
Ma cambia sempre. 

55
00:04:13,140 --> 00:04:16,000
Aggiungi sempre nuove cose. 

56
00:04:16,320 --> 00:04:18,340
Sì, sto imparando. 

57
00:04:18,480 --> 00:04:27,580
Perché penso che mi piace imparare e avere più saggezza per evolversi. 

58
00:04:27,880 --> 00:04:29,480
Interrogami 

59
00:04:29,800 --> 00:04:33,660
Mi piace essere un discepolo più che un maestro. 

60
00:04:34,100 --> 00:04:35,740
Mi piacciono entrambi. 

61
00:04:36,120 --> 00:04:40,540
Sono un bravo maestro, ma sono uno studente molto cattivo. 

62
00:04:40,820 --> 00:04:42,780
Molto indisciplinato. 

63
00:04:43,200 --> 00:04:46,160
Sei sempre stato così? Incluso con Deshimaru? 

64
00:04:46,400 --> 00:04:47,140
Sì. 

65
00:04:47,420 --> 00:04:48,840
E ti ha corretto? 

66
00:04:49,080 --> 00:04:50,700
Sì, mi ha aiutato molto. 

67
00:04:50,980 --> 00:04:54,240
Mi ha incaricato, mi ha calmato un po ’. 

68
00:04:58,060 --> 00:05:00,280
"Che cosa ho intenzione di fare con Stéphane?" 

69
00:05:00,560 --> 00:05:04,180
"Ecco, io sono responsabile di lui!" 

70
00:05:08,920 --> 00:05:11,860
Ti ricordi il giorno in cui hai incontrato Deshimaru? 

71
00:05:12,120 --> 00:05:12,620
Sì. 

72
00:05:12,920 --> 00:05:14,920
Com’è stato? 

73
00:05:15,280 --> 00:05:16,820
Non posso dirlo. 

74
00:05:17,060 --> 00:05:19,240
Ero a Zazen ed era dietro di me con il kyosaku. 

75
00:05:22,920 --> 00:05:28,320
Più tardi ho comprato fiori nella metropolitana mentre andavo al dojo. 

76
00:05:28,640 --> 00:05:33,060
All’ingresso del dojo dico: "Ho dei fiori per il maestro". » 

77
00:05:33,400 --> 00:05:36,080
E dopo Zazen, mi chiamano. 

78
00:05:36,340 --> 00:05:38,540
"Il maestro ti chiede." 

79
00:05:38,780 --> 00:05:42,860
Vado nella sua stanza, dove è seduto. 

80
00:05:43,060 --> 00:05:46,760
"Chi mi ha portato questi fiori?" 

81
00:05:47,040 --> 00:05:47,800
I. 

82
00:05:48,660 --> 00:05:51,580
"Bene, bene, bene." 

83
00:05:54,280 --> 00:05:58,080
Ricordi l’ultimo giorno che l’hai visto? 

84
00:05:58,580 --> 00:05:59,920
Era morto. 

85
00:06:05,280 --> 00:06:07,280
Il suo corpo sarebbe stato bruciato. 

86
00:06:07,520 --> 00:06:09,160
Addio! 

87
00:06:10,320 --> 00:06:12,320
E l’ultimo giorno che l’hai visto vivo? 

88
00:06:13,860 --> 00:06:19,800
Quindi è andato in Giappone per l’ultima volta e ha detto "arrivederci!". 

89
00:06:20,200 --> 00:06:23,680
Il giorno della morte di Deshimaru hai pianto? 

90
00:06:24,360 --> 00:06:34,420
Quando è morto, ero nel dojo, seduto nello zazen … 

91
00:06:37,260 --> 00:06:40,780
Qualcuno entra nel dojo e dice: 

92
00:06:41,060 --> 00:06:45,840
"Dovremo continuare senza Deshimaru." 

93
00:06:46,100 --> 00:06:48,900
Ho vissuto in zazen. 

94
00:06:49,200 --> 00:06:50,560
Sono sempre stato in piedi davanti a lui. 

95
00:06:50,900 --> 00:06:53,500
Sono sempre stato un pilastro per lui. 

96
00:06:53,940 --> 00:06:57,120
È così che ho vissuto la sua morte. 

97
00:06:57,420 --> 00:07:02,200
Ho dovuto rimanere forte per gli altri. 

98
00:07:02,840 --> 00:07:11,040
Poi sono andato direttamente in Giappone per la cerimonia della cremazione. 

99
00:07:13,100 --> 00:07:23,920
Ci fu una cerimonia con molti insegnanti, con l’intera chiesa giapponese. Enormemente. 

100
00:07:24,660 --> 00:07:31,060
Ed era in una scatola, con molti fiori. 

101
00:07:31,500 --> 00:07:37,790
Il suo viso bellissimo, molto sorridente. 

102
00:07:38,100 --> 00:07:50,620
Quando era vivo, ogni volta che si metteva un cappello o un naso falso, diventava molto comico. 

103
00:07:50,940 --> 00:07:54,380
Era lo stesso lì, con i fiori che lo circondavano. 

104
00:07:54,720 --> 00:07:59,520
Abbiamo fatto la cerimonia, sono rimasto molto concentrato e impassibile. 

105
00:07:59,820 --> 00:08:05,400
Quindi mettiamo la scatola in una macchina. 

106
00:08:05,880 --> 00:08:09,720
A quattro discepoli. 

107
00:08:09,840 --> 00:08:20,280
Sono stato l’ultimo e spingo la scatola su un carrello. 

108
00:08:20,600 --> 00:08:27,160
All’ultimo momento mi stringo il dito con il petto. 

109
00:08:29,180 --> 00:08:37,700
Ho iniziato a piangere e non riuscivo a smettere per due o tre ore. 

110
00:08:38,020 --> 00:08:42,220
È uscito e ho pianto e pianto. 

111
00:08:42,520 --> 00:08:45,560
Tutti mi guardarono perché nessuno piangeva così. 

112
00:08:46,040 --> 00:08:51,440
Ho pianto incessantemente di fronte alla famiglia e agli altri partecipanti. 

113
00:08:52,280 --> 00:08:57,500
Non riuscivo a fermarmi. 

114
00:09:00,560 --> 00:09:05,720
Fino a quando ho visto il fumo proveniente dal camino. 

115
00:09:19,379 --> 00:09:27,799
Morto - vivo, tutto è vivo, tutto è morto. 

116
00:09:28,840 --> 00:09:33,500
Siamo ancora vivi, ma siamo morti. 

117
00:09:33,800 --> 00:09:42,220
Quando muori, vedi la tua vita balenare. 

118
00:09:42,540 --> 00:09:48,580
Abbiamo osservato tutte le nostre vite ora. 

119
00:10:10,010 --> 00:10:13,620
Zazen è lo stato normale. 

