1
00:00:00,000 --> 00:00:02,240
Soy un monje zen. 

2
00:00:04,540 --> 00:00:09,960
También soy un calígrafo. 

3
00:00:10,980 --> 00:00:18,660
La caligrafía china es apreciada por las personas que practican zazen. 

4
00:00:19,280 --> 00:00:24,980
No es fácil hablar de zen. 

5
00:00:25,400 --> 00:00:28,820
Para expresarlo, para manifestarlo. 

6
00:00:29,240 --> 00:00:34,480
A través de la caligrafía, vemos el espíritu de una persona haciendo zazen. 

7
00:00:35,000 --> 00:00:39,100
Quien practica la actitud de despertar. 

8
00:00:40,020 --> 00:00:46,260
El Maestro Deshimaru explica que la caligrafía no es solo una técnica. 

9
00:00:46,980 --> 00:00:52,700
No solo hacemos caligrafía según técnicas, propiedades … 

10
00:00:53,360 --> 00:00:58,400
Hacemos caligrafía con la mente, con la espinilla. 

11
00:00:58,880 --> 00:01:09,440
Con la mente, con todo el cuerpo y la mente en uno. 

12
00:01:10,720 --> 00:01:18,400
La caligrafía imprime eso en papel. Es una presencia. 

13
00:01:18,960 --> 00:01:24,320
Se siente en la caligrafía de grandes maestros como Deshimaru y Niwa Zenji. 

14
00:01:25,760 --> 00:01:30,000
El valor de la caligrafía no está solo en la forma. 

15
00:01:31,440 --> 00:01:36,560
Pero también en el impulso, la presencia, la energía. 

16
00:01:37,280 --> 00:01:46,240
En la actividad que surge … 

17
00:01:47,040 --> 00:01:52,080
Y que sentimos y percibimos. 

18
00:01:52,500 --> 00:01:55,800
Partimos de un impulso que proviene de la inmovilidad. 

19
00:01:56,950 --> 00:01:58,853
Zen, calma, silencio, silencio. 

20
00:02:00,160 --> 00:02:06,093
Para no hacer nada. 

21
00:02:06,100 --> 00:02:07,910
Dejar pasar los pensamientos. 

22
00:02:07,950 --> 00:02:10,750
No para no ser monopolizado. 

23
00:02:10,840 --> 00:02:15,040
Estar enfocado en una actitud. 

24
00:02:15,920 --> 00:02:21,329
Piernas cruzadas 

25
00:02:21,329 --> 00:02:26,880
El cojín permite inclinar la pelvis hacia adelante. 

26
00:02:26,880 --> 00:02:31,280
Para limpiar toda la caja torácica. 

27
00:02:32,240 --> 00:02:37,040
En esta pose, recuperamos nuestras manos. 

28
00:02:38,160 --> 00:02:43,680
Nos permite unirnos y centrarnos. 

29
00:02:44,400 --> 00:02:51,040
Es la actitud de la meditación zen, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
Es a partir de esto, y del impulso, de la actividad, que emergerá la caligrafía. 

31
00:02:58,480 --> 00:03:05,200
No por complicaciones, por querer hacer el bien, por querer hacer el bien, por querer hacer buen tiempo … 

32
00:03:06,240 --> 00:03:11,520
Solo este impulso de la vida. 

33
00:03:12,560 --> 00:03:22,160
La caligrafía surgirá de este impulso zen, este impulso, este impulso. 

34
00:03:23,280 --> 00:03:33,200
De eso se trata la caligrafía zen. 

35
00:03:34,720 --> 00:03:59,600
Considerando la fase de civilización que China había alcanzado durante la dinastía Tang … 

36
00:04:01,120 --> 00:04:15,520
En los siglos VII, VIII y IX, los países vecinos, Japón, Corea y Vietnam, fueron atraídos por esta cultura y por el idioma, los caracteres chinos. 

37
00:04:16,400 --> 00:04:27,200
Entonces la escritura china se ha extendido a estos países. 

38
00:04:28,080 --> 00:04:34,480
Pero todos lo han grabado de acuerdo con sus propios sentimientos, su propio idioma. 

39
00:04:35,280 --> 00:04:44,560
Cada uno de ellos ha desarrollado su propia forma de caligrafía. 

40
00:04:45,520 --> 00:04:56,080
También introdujeron syllabi, caracteres cercanos a un alfabeto mezclados con caracteres chinos. 

41
00:04:58,320 --> 00:05:04,480
La caligrafía japonesa tiene pequeñas diferencias con la caligrafía china. 

42
00:05:05,680 --> 00:05:13,440
Todos usan pinceles, dibujan caracteres chinos, pero también usan sistemas silábicos. 

43
00:05:15,040 --> 00:05:23,280
Los vietnamitas, anteriormente influenciados por China, fueron aún más creativos. 

44
00:05:24,800 --> 00:05:31,600
Inventaron su propio sistema ideográfico para transcribir su idioma al modelo chino. 

45
00:05:33,440 --> 00:05:38,400
Está escrito: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
Es el sutra del Espíritu de la Gran Sabiduría lo que nos mueve hacia adelante. 

48
00:05:59,840 --> 00:06:06,160
Esta es la enseñanza universal del Buda, específica de todas las escuelas budistas. 

49
00:06:07,360 --> 00:06:16,800
Se canta en chino antiguo, japonés antiguo, en transcripción fonética del sánscrito. 

50
00:06:18,080 --> 00:06:24,000
Eso lo convierte en un texto universal, porque nadie lo entiende ni lo lee en su propio idioma. 

51
00:06:25,360 --> 00:06:39,120
Los japoneses, los chinos, los indios … Lo pronunciamos nosotros mismos con algunos ajustes menores …. 

52
00:06:40,980 --> 00:06:47,460
Es algo que pertenece a todos y a nadie en particular. 

53
00:06:48,220 --> 00:06:53,440
Esta es la oración que escribí aquí, en un estilo de escritura llamado cursiva. 

54
00:06:53,680 --> 00:06:58,520
No está escribiendo el diccionario. 

55
00:06:58,560 --> 00:07:03,680
No es escritura regular. 

56
00:07:05,740 --> 00:07:09,100
Está desarrollado para escritura rápida. 

57
00:07:09,480 --> 00:07:15,960
Una especie de taquigrafía. 

58
00:07:16,240 --> 00:07:28,560
Mantenemos las líneas principales del personaje simplificando, a través de curvaturas, a través de puntos, donde había una línea … 

59
00:07:29,600 --> 00:07:39,440
El personaje se ha simplificado al extremo, adquiere una forma diferente de su forma original. 

60
00:07:39,760 --> 00:07:47,680
Nos movemos hacia una abstracción, preservando la esencia de la forma original. 

61
00:07:48,800 --> 00:07:55,920
Te permite escribir más rápido. 

62
00:07:58,080 --> 00:08:05,240
En la trama encontramos este impulso, este impulso, este impulso de zazen. 

63
00:08:05,640 --> 00:08:13,280
Porque conectamos todo el personaje juntos. 

64
00:08:13,660 --> 00:08:18,479
A la vez. 

65
00:08:18,479 --> 00:08:23,470
Incluso podemos conectar a los personajes juntos. 

66
00:08:24,000 --> 00:08:31,840
Vinculamos cada uno de los personajes, cada una de las propiedades del personaje. 

67
00:08:33,120 --> 00:08:38,520
Si se dibujan varios caracteres, se pueden vincular entre sí. 

68
00:08:38,940 --> 00:08:44,960
Lo hacemos en la respiración, en el impulso. 

69
00:08:45,400 --> 00:08:51,540
En el momento, la energía, la dinámica, la actividad de todo el cuerpo, toda la mente en uno. 

70
00:08:51,540 --> 00:08:55,200
En un movimiento, de principio a fin. 

71
00:08:55,200 --> 00:08:59,200
No podemos regresar, no podemos actualizarlo. 

72
00:08:59,200 --> 00:09:02,900
No podemos corregirlo. 

73
00:09:03,000 --> 00:09:05,120
Lo hacemos en conciencia. 

74
00:09:05,920 --> 00:09:11,600
También nos dejamos llevar con alegría. 

75
00:09:12,060 --> 00:09:18,220
Pero no podemos corregir nada. 

76
00:09:18,560 --> 00:09:22,160
Cuando se rastrea, se rastrea. 

77
00:09:22,200 --> 00:09:25,529
Es el aquí y el ahora. 

78
00:09:25,529 --> 00:09:29,000
Es algo que nunca volverá. 

79
00:09:29,100 --> 00:09:33,800
Eso existe completamente, eternamente, en el momento. 

80
00:09:33,900 --> 00:09:36,200
Eso también es zen. 

81
00:09:36,250 --> 00:09:39,640
Cada vez, cada respiración zazen es única. 

82
00:09:42,520 --> 00:09:44,640
Ella no va a volver 

83
00:09:45,340 --> 00:09:52,540
Es un punto, un guión de la actitud. 

84
00:09:52,600 --> 00:09:58,320
En el estado de ánimo, la respiración y la actividad total. 

85
00:09:58,420 --> 00:10:03,160
Es un tiempo de vida, de conciencia, absoluto, universal. 

86
00:10:03,720 --> 00:10:06,760
¿Quién no vuelve? 

87
00:10:07,120 --> 00:10:11,000
Es muy importante en caligrafía. 

88
00:10:11,000 --> 00:10:17,500
Si caligrafías como un robot sin poner todos tus pensamientos en él, es una técnica. 

89
00:10:17,540 --> 00:10:18,920
Zazen es igual. 

90
00:10:19,000 --> 00:10:25,880
Si practicas zazen como una técnica de bienestar, no es la dimensión del zen. 

