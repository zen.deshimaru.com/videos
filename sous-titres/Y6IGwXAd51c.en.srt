1
00:00:00,000 --> 00:00:02,240
I am a Zen monk. 

2
00:00:04,540 --> 00:00:09,960
I am also a calligrapher. 

3
00:00:10,980 --> 00:00:18,660
Chinese calligraphy is appreciated by people who practice zazen. 

4
00:00:19,280 --> 00:00:24,980
It is not easy to talk about Zen. 

5
00:00:25,400 --> 00:00:28,820
To express it, to manifest it. 

6
00:00:29,240 --> 00:00:34,480
Through calligraphy, we see the spirit of a person doing zazen. 

7
00:00:35,000 --> 00:00:39,100
Who practices the awakening attitude. 

8
00:00:40,020 --> 00:00:46,260
Master Deshimaru explains that calligraphy is not just a technique. 

9
00:00:46,980 --> 00:00:52,700
We do not only calligraphy according to techniques, properties … 

10
00:00:53,360 --> 00:00:58,400
We calligraphy with the mind, with shin. 

11
00:00:58,880 --> 00:01:09,440
With the mind, with the whole body and mind in one. 

12
00:01:10,720 --> 00:01:18,400
Calligraphy prints that on paper. It is a presence. 

13
00:01:18,960 --> 00:01:24,320
It is felt in the calligraphy of great masters like Deshimaru and Niwa Zenji. 

14
00:01:25,760 --> 00:01:30,000
The value of calligraphy is not just in the form. 

15
00:01:31,440 --> 00:01:36,560
But also in the momentum, the presence, the energy. 

16
00:01:37,280 --> 00:01:46,240
In the activity that arises …. 

17
00:01:47,040 --> 00:01:52,080
And that we feel and perceive. 

18
00:01:52,500 --> 00:01:55,800
We start from a momentum that comes from immobility. 

19
00:01:56,950 --> 00:01:58,853
Zen, calm, quiet, silence. 

20
00:02:00,160 --> 00:02:06,093
To keep nothing going. 

21
00:02:06,100 --> 00:02:07,910
To let thoughts pass. 

22
00:02:07,950 --> 00:02:10,750
Not not to be monopolized. 

23
00:02:10,840 --> 00:02:15,040
To be focused in an attitude. 

24
00:02:15,920 --> 00:02:21,329
Crossed legs. 

25
00:02:21,329 --> 00:02:26,880
The cushion makes it possible to tilt the pelvis forward. 

26
00:02:26,880 --> 00:02:31,280
To clear the entire rib cage. 

27
00:02:32,240 --> 00:02:37,040
In this pose, we bring our hands back. 

28
00:02:38,160 --> 00:02:43,680
It allows us to unite and center. 

29
00:02:44,400 --> 00:02:51,040
It is the attitude of Zen meditation, hokkai jo in. 

30
00:02:52,560 --> 00:02:57,600
It is from this, and from the momentum, from the activity, that calligraphy will emerge. 

31
00:02:58,480 --> 00:03:05,200
Not from complications, from wanting to do good, from wanting to do good, from wanting to do good weather … 

32
00:03:06,240 --> 00:03:11,520
Only this momentum of life. 

33
00:03:12,560 --> 00:03:22,160
Calligraphy will emerge from this Zen impulse, this impulse, this impulse. 

34
00:03:23,280 --> 00:03:33,200
That’s what Zen calligraphy is about. 

35
00:03:34,720 --> 00:03:59,600
Considering the civilization phase that China had reached during the Tang Dynasty … 

36
00:04:01,120 --> 00:04:15,520
In the 7th, 8th and 9th centuries, the surrounding countries, Japan, Korea and Vietnam, were attracted by this culture and by the language, the Chinese characters. 

37
00:04:16,400 --> 00:04:27,200
So the Chinese script has spread to these countries. 

38
00:04:28,080 --> 00:04:34,480
But everyone has recorded it according to their own feeling, their own language. 

39
00:04:35,280 --> 00:04:44,560
Each of them has developed their own way of calligraphy. 

40
00:04:45,520 --> 00:04:56,080
They also introduced syllabi, characters close to an alphabet mixed with Chinese characters. 

41
00:04:58,320 --> 00:05:04,480
Japanese calligraphy has minor differences from Chinese calligraphy. 

42
00:05:05,680 --> 00:05:13,440
All use brushes, draw Chinese characters, but also use syllabic systems. 

43
00:05:15,040 --> 00:05:23,280
The Vietnamese, previously influenced by China, were even more creative. 

44
00:05:24,800 --> 00:05:31,600
They invented their own ideographic system to transcribe their language to the model of Chinese. 

45
00:05:33,440 --> 00:05:38,400
It is written: 

46
00:05:40,710 --> 00:05:49,520
Maka Hannya Hannya Haramita Shingyo … 

47
00:05:49,520 --> 00:05:59,320
It is the sutra of the Spirit of the Great Wisdom that moves us forward. 

48
00:05:59,840 --> 00:06:06,160
This is the universal teaching of the Buddha, specific to all Buddhist schools. 

49
00:06:07,360 --> 00:06:16,800
It is sung in ancient Chinese, ancient Japanese, in phonetic transcription from Sanskrit. 

50
00:06:18,080 --> 00:06:24,000
That makes it a universal text, because no one understands or reads it in their own language. 

51
00:06:25,360 --> 00:06:39,120
The Japanese, the Chinese, the Indians …. We pronounce it ourselves with some minor adjustments …. 

52
00:06:40,980 --> 00:06:47,460
It is something that belongs to everyone and no one in particular. 

53
00:06:48,220 --> 00:06:53,440
This is the sentence I wrote here, in a writing style called cursive. 

54
00:06:53,680 --> 00:06:58,520
It is not writing the dictionary. 

55
00:06:58,560 --> 00:07:03,680
It is not regular writing. 

56
00:07:05,740 --> 00:07:09,100
It is developed for fast writing. 

57
00:07:09,480 --> 00:07:15,960
A kind of shorthand. 

58
00:07:16,240 --> 00:07:28,560
We maintain the main lines of the character by simplifying, through curvatures, through points, where there was a line … 

59
00:07:29,600 --> 00:07:39,440
The character has been simplified to the extreme, it takes on a different shape from its original shape. 

60
00:07:39,760 --> 00:07:47,680
We move towards an abstraction, preserving the essence of the original form. 

61
00:07:48,800 --> 00:07:55,920
It allows you to write faster. 

62
00:07:58,080 --> 00:08:05,240
In the plot we find this impulse, this impulse, this impulse of zazen. 

63
00:08:05,640 --> 00:08:13,280
Because we connect the whole character together. 

64
00:08:13,660 --> 00:08:18,479
In one go. 

65
00:08:18,479 --> 00:08:23,470
We can even connect the characters together. 

66
00:08:24,000 --> 00:08:31,840
We link each of the characters, each of the properties in the character. 

67
00:08:33,120 --> 00:08:38,520
If multiple characters are drawn, they can be linked together. 

68
00:08:38,940 --> 00:08:44,960
We do it on the breath, on the impulse. 

69
00:08:45,400 --> 00:08:51,540
At the momentum, the energy, the dynamics, the activity of the whole body, the whole mind in one. 

70
00:08:51,540 --> 00:08:55,200
In one movement, from start to finish. 

71
00:08:55,200 --> 00:08:59,200
We can’t go back, we can’t update it. 

72
00:08:59,200 --> 00:09:02,900
We cannot correct it. 

73
00:09:03,000 --> 00:09:05,120
We do it in conscience. 

74
00:09:05,920 --> 00:09:11,600
We also let ourselves go with joy. 

75
00:09:12,060 --> 00:09:18,220
But we cannot correct anything. 

76
00:09:18,560 --> 00:09:22,160
When it is traced, it is traced. 

77
00:09:22,200 --> 00:09:25,529
It is the here and now. 

78
00:09:25,529 --> 00:09:29,000
It is something that will never come back. 

79
00:09:29,100 --> 00:09:33,800
That exists completely, eternally, in the moment. 

80
00:09:33,900 --> 00:09:36,200
That’s Zen too. 

81
00:09:36,250 --> 00:09:39,640
Every time, every zazen breath is unique. 

82
00:09:42,520 --> 00:09:44,640
She’s not coming back. 

83
00:09:45,340 --> 00:09:52,540
It is a point, a hyphen of the attitude. 

84
00:09:52,600 --> 00:09:58,320
In the mood, breathing and total activity. 

85
00:09:58,420 --> 00:10:03,160
It is a time of life, of consciousness, absolute, universal. 

86
00:10:03,720 --> 00:10:06,760
Who don’t come back. 

87
00:10:07,120 --> 00:10:11,000
It is very important in calligraphy. 

88
00:10:11,000 --> 00:10:17,500
If you calligraphy like a robot without putting all your thoughts into it, it’s a technique. 

89
00:10:17,540 --> 00:10:18,920
Zazen is the same. 

90
00:10:19,000 --> 00:10:25,880
If you practice zazen as a technique of well-being, it is not the dimension of Zen. 

