1
00:00:07,490 --> 00:00:10,656
La gente piensa que el zazen es una técnica … 

2
00:00:10,656 --> 00:00:13,173
quien los ayudará en sus vidas. 

3
00:00:13,173 --> 00:00:16,800
Lo hacen una cosa ridículamente pequeña. 

4
00:00:16,800 --> 00:00:21,820
y hacer zazen con la esperanza de que les ayudará en sus vidas. 

5
00:00:21,820 --> 00:00:31,280
Eso es cierto, porque en este momento muy consciente … 

6
00:00:31,280 --> 00:00:35,300
el más enérgico, el más presente que hay …. 

7
00:00:35,300 --> 00:00:39,820
… tenemos plena influencia en todos los fenómenos de nuestra vida, eso es seguro. 

8
00:00:40,020 --> 00:00:46,240
Pero no estamos haciendo esto para mejorar nuestro ego … 

9
00:00:46,250 --> 00:00:50,720
Para calmarnos …. 

10
00:00:50,720 --> 00:00:52,880
Porque nos hará bien … 

11
00:00:52,880 --> 00:00:58,500
Porque nos ahorrará algunas horas de sueño …. 

12
00:00:58,500 --> 00:01:02,759
Dándonos mayor rendimiento sexual …. 

13
00:01:02,759 --> 00:01:06,100
O ayúdanos a lidiar con el estrés. 

14
00:01:06,100 --> 00:01:11,500
Esto baja el zazen … 

15
00:01:11,500 --> 00:01:16,230
a nivel de un instrumento para nuestra propia vida ilusoria. 

16
00:01:16,230 --> 00:01:23,760
Pero es todo lo contrario. 

17
00:01:23,920 --> 00:01:28,980
Centraremos nuestras vidas para generar el zazen más efectivo. 

18
00:01:30,890 --> 00:01:33,880
Siéntate en la posición del Buda, 

19
00:01:33,880 --> 00:01:39,380
Es el momento más intenso y consciente. 

20
00:01:39,380 --> 00:01:43,020
De eso se trata. 

21
00:01:43,020 --> 00:01:46,340
Todo se une: el pasado, el presente, el futuro. 

22
00:01:46,340 --> 00:01:52,060
Todos los fenómenos de nuestra vida están en zazen. 

23
00:01:52,060 --> 00:02:00,040
Es en zazen donde tenemos poder, conciencia, 

24
00:02:06,620 --> 00:02:12,020
para cambiar fundamentalmente los entresijos de nuestras vidas. 

25
00:02:16,320 --> 00:02:21,240
En zazen estamos en una actitud digna. 

26
00:02:21,240 --> 00:02:25,380
Una actitud de héroes, Buda, Dios. 

27
00:02:25,500 --> 00:02:31,320
Somos heterosexuales, fuertes, guapos, tranquilos, tranquilos, tranquilos, inmóviles. 

28
00:02:31,320 --> 00:02:36,600
No debes dormirte, debes estirar la columna vertebral. 

29
00:02:36,600 --> 00:02:41,980
Tienes que respirar, relajarte. 

30
00:02:46,640 --> 00:02:50,740
En zazen hay de todo, todo lo que somos. 

31
00:02:50,740 --> 00:02:55,800
Todo nuestro pasado, nuestro presente e indirectamente nuestro futuro. 

32
00:02:55,980 --> 00:03:05,460
Está nuestro cuerpo, con sus debilidades, su fatiga, 

33
00:03:05,600 --> 00:03:11,200
sus impurezas, sus toxinas, 

34
00:03:11,209 --> 00:03:14,900
sus enfermedades, su karma, etc. 

35
00:03:14,900 --> 00:03:21,380
Está nuestra mente, nuestra conciencia. 

36
00:03:21,380 --> 00:03:25,055
Todo está unido en zazen. 

37
00:03:25,055 --> 00:03:29,320
La actitud zazen, 

38
00:03:29,320 --> 00:03:33,880
Es la autoimagen más bella que puedes mostrar. 

39
00:03:40,620 --> 00:03:46,090
Conocí al Maestro Deshimaru, me enseñó a zazen, me senté en zazen. 

40
00:03:46,090 --> 00:03:48,620
E inmediatamente dije: "¡Esto es lo mejor!" 

41
00:04:01,360 --> 00:04:05,680
Para saber lo que creas, cuál es la realidad que creas … 

42
00:04:05,680 --> 00:04:10,680
Tú eres quien crea la realidad. Vives tu sueño. 

43
00:04:10,680 --> 00:04:15,920
Todos están viviendo su sueño, y tienes que tomar tu sueño, esa es la cuestión. 

44
00:04:15,920 --> 00:04:19,480
Para mí, Zazen siempre ha sido así. 

