1
00:00:00,000 --> 00:00:05,742
My master taught me back when I was a beginner (I am still …). 

2
00:00:05,842 --> 00:00:07,832
"Zazen is satori." 

3
00:00:07,932 --> 00:00:18,497
At the time, in the 1970s, the word "satori" was very interesting, we wanted to know what satori was, a kind of enlightenment. 

4
00:00:18,597 --> 00:00:22,731
People wanted to do zazen to get satori. 

5
00:00:22,831 --> 00:00:31,329
And Sensei taught that it was the attitude of Buddha himself, that there is no satori outside. 

6
00:00:31,429 --> 00:00:35,793
That is Master Deshimaru’s teaching. He always said: 

7
00:00:35,893 --> 00:00:39,950
"Satori is the return to normal circumstances." 

8
00:00:40,050 --> 00:00:41,529
We young people were disappointed. 

9
00:00:41,629 --> 00:00:46,520
Normal conditions are boring. 

10
00:00:46,620 --> 00:00:49,388
We liked to smoke weed, took LSD … 

11
00:00:49,490 --> 00:00:53,471
We were not interested in normal circumstances. 

12
00:00:53,571 --> 00:00:58,853
Now, in 2018, normal conditions are in high demand. 

13
00:00:58,960 --> 00:01:02,400
Normal seasons, normal climate, normal air quality. 

14
00:01:02,400 --> 00:01:04,400
A normal ocean, normal fauna and flora. 

15
00:01:08,380 --> 00:01:10,980
Normality becomes expensive. 

16
00:01:11,088 --> 00:01:13,155
Never forget these words: 

17
00:01:13,255 --> 00:01:15,705
"Zazen himself is satori." 

18
00:01:15,805 --> 00:01:19,809
Satori is about being in the right place at the right time. 

19
00:01:19,909 --> 00:01:21,516
It is very difficult. 

20
00:01:21,616 --> 00:01:27,840
If you adopt the Buddha pose, you are in the right place at the right time. 

21
00:01:27,940 --> 00:01:32,939
You are normal. You are synchronizing with the cosmic order. 

22
00:01:33,039 --> 00:01:34,264
It is mechanical. 

23
00:01:34,364 --> 00:01:38,498
With his skeleton of an abnormal person, a polluter … 

24
00:01:38,598 --> 00:01:42,831
If you take the Buddha’s attitude, it is mechanical. 

25
00:01:42,931 --> 00:01:47,854
We return to the universal cosmic rhythm. That is satori. 

26
00:01:47,954 --> 00:01:50,503
At the same time, it is very simple. 

27
00:01:50,603 --> 00:01:59,507
But if you’re as far away from the normal conditions as we are now, it has tremendous value. 

28
00:01:59,607 --> 00:02:01,521
And what is Zen? 

29
00:02:01,621 --> 00:02:04,017
It takes the exact position. 

30
00:02:04,117 --> 00:02:07,945
It is like a code to open a secret door. 

31
00:02:08,045 --> 00:02:10,035
You see it in the movies. 

32
00:02:10,135 --> 00:02:16,359
You have to press this, you have to get the light beam in the right place. 

33
00:02:16,459 --> 00:02:17,990
And the door opens. 

34
00:02:18,090 --> 00:02:21,229
It is the same if you align your position. 

35
00:02:25,280 --> 00:02:29,320
You are beautiful on the pins. On the pillow, in lotus. 

36
00:02:29,720 --> 00:02:32,400
The Buddha’s position is in lotus. 

37
00:02:32,860 --> 00:02:37,700
If you cannot do the lotus, do half a lotus. 

38
00:02:37,980 --> 00:02:40,580
But the lotus has nothing to do with it. 

39
00:02:41,060 --> 00:02:44,120
It reverses the polarities. 

40
00:02:44,760 --> 00:02:52,720
As soon as you take the lotus, instead of forcing it, instead of hurting it, you let go. 

41
00:02:53,480 --> 00:02:56,380
The Buddha’s attitude is precise. 

42
00:02:56,940 --> 00:03:03,020
If you put on just the right clothes. 

43
00:03:03,140 --> 00:03:08,240
Breathe well, heal your body and mind. 

44
00:03:08,240 --> 00:03:14,360
It is beautiful and it is very simple. 

45
00:03:14,860 --> 00:03:17,840
It is not a special condition. 

46
00:03:18,520 --> 00:03:23,020
You don’t have to think, "I have more satori than the others." 

47
00:03:23,400 --> 00:03:26,080
It’s just normal. 

48
00:03:26,600 --> 00:03:32,940
If there is anything abnormal in the solar system, fart everything. 

49
00:03:33,280 --> 00:03:38,720
So everything is exactly in the right place at the right time, and in one movement. 

50
00:03:38,920 --> 00:03:42,660
The secret is clear: we know the method. 

51
00:03:42,940 --> 00:03:45,700
You should take the trouble. 

52
00:03:46,100 --> 00:03:48,460
It takes practice. 

53
00:03:48,740 --> 00:03:52,100
It is extraordinary to be able to fail the cosmic order. 

54
00:03:52,300 --> 00:03:56,000
Sensei kept repeating, "Follow the cosmic order." 

55
00:03:56,160 --> 00:03:58,080
He yelled at me: 

56
00:03:58,320 --> 00:04:01,500
"STIEFAN, WHY DON’T YOU FOLLOW THE COSMIC ORDER? YOU MUST FOLLOW THE COSMIC ORDER!" 

