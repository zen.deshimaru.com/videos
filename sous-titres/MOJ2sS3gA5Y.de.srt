1
00:00:04,240 --> 00:00:07,040
Was ist Zen? 

2
00:00:07,320 --> 00:00:10,120
Beim Zen geht es darum, sich selbst zu verstehen. 

3
00:00:10,400 --> 00:00:11,800
Dein eigener Verstand. 

4
00:00:12,080 --> 00:00:14,140
Es geht alle an. 

5
00:00:14,420 --> 00:00:16,500
Das wollte Deshimaru senden. 

6
00:00:16,800 --> 00:00:19,460
Das möchte ich weitergeben und realisieren. 

7
00:00:19,720 --> 00:00:22,460
Ich hatte einen Schüler von Kodo Sawaki gesehen. 

8
00:00:22,840 --> 00:00:24,720
Er sagte zu mir: 

9
00:00:25,120 --> 00:00:27,620
"Du musst dich selbst anschauen!" 

10
00:00:27,960 --> 00:00:29,960
Er hatte recht! 

11
00:00:30,380 --> 00:00:33,300
Was bedeutet ein spiritueller Weg? 

12
00:00:33,640 --> 00:00:41,880
Die Bedeutung eines spirituellen Weges ist "zum Träumer zurückkehren". 

13
00:00:46,020 --> 00:00:48,260
Dogen sagt: 

14
00:00:48,520 --> 00:00:51,260
"Buddhismus studieren heißt sich selbst studieren." 

15
00:00:51,540 --> 00:00:53,980
Studiere den Träumer. 

16
00:00:54,380 --> 00:00:57,300
Warum träumt er so? 

17
00:00:58,260 --> 00:01:00,240
Wie kann er seinen Traum ändern? 

18
00:01:00,540 --> 00:01:02,020
Wie kann er seinen Traum kontrollieren? 

19
00:01:02,840 --> 00:01:04,000
Wenn der Buddha sagt: 

20
00:01:04,440 --> 00:01:06,300
"Alles ist ein Traum." 

21
00:01:06,620 --> 00:01:09,820
Das heißt nicht "Nichts ist wichtig". 

22
00:01:10,100 --> 00:01:13,580
Es ist nicht der Traum, der wichtig ist, es ist der Träumer. 

23
00:01:13,900 --> 00:01:16,540
Hör auf zu träumen, das nennen wir "Erwachen" … 

24
00:01:17,180 --> 00:01:21,040
Das Erwachen wird zum Träumer. 

25
00:01:21,340 --> 00:01:22,980
Kontrolliere deinen Traum. 

26
00:01:24,000 --> 00:01:26,120
Das ist der spirituelle Weg. 

27
00:01:26,420 --> 00:01:27,980
Es ist sehr schwierig. 

28
00:01:28,560 --> 00:01:31,760
Wie kann ich anfangen? 

29
00:01:32,660 --> 00:01:45,080
Die Hauptmotivation für das Üben eines spirituellen Weges ist: in der Lage zu sein, Ihren Traum zu kontrollieren. 

30
00:01:46,700 --> 00:01:49,340
Was ist im Zen wichtig? 

31
00:01:49,920 --> 00:01:55,880
Auf diese Weise ist Ihr Verstand wichtig. 

32
00:01:56,280 --> 00:02:01,280
Zazen ist kein Gegenstand der Anbetung. 

33
00:02:01,540 --> 00:02:03,980
Der zweite wichtige Punkt seiner Lehre: 

34
00:02:04,320 --> 00:02:06,320
Mushotoku. 

35
00:02:07,180 --> 00:02:10,780
Was bedeutet "gemeinnützig". 

36
00:02:11,340 --> 00:02:23,340
Es ist die Tür, die uns vom Geist Gottes in die materielle Welt führt. 

37
00:02:23,640 --> 00:02:27,000
Es ist sehr wichtig. 

38
00:02:27,360 --> 00:02:37,420
Selbst in der Alltagswelt, auch in Phänomenen, verlieren Sie nicht Ihre Konzentration. 

39
00:02:37,800 --> 00:02:42,920
Sie verlieren nicht Ihre wahre Natur. 

40
00:02:43,180 --> 00:02:45,100
Das ist Mushotoku. 

41
00:02:45,400 --> 00:02:49,660
Aber was ist einfach? 

42
00:02:55,780 --> 00:03:00,260
Die Leute gehen immer den einfachen Weg. 

43
00:03:00,620 --> 00:03:03,780
Wir fallen in die einfachen Sachen. 

44
00:03:10,640 --> 00:03:13,600
Macht ist einfach. 

45
00:03:14,060 --> 00:03:19,220
Es ist einfach, mit Menschen zusammen zu sein, denen es immer genauso geht. 

46
00:03:21,160 --> 00:03:26,400
Aus Zen eine Kirche zu machen ist einfach. 

47
00:03:26,800 --> 00:03:32,080
Das Anbringen an einer Kirche ist einfach. 

48
00:03:32,360 --> 00:03:37,300
Die Leute gehen in etwas Sicheres. 

49
00:03:37,600 --> 00:03:41,060
Ohne Anstrengung. 

50
00:03:41,420 --> 00:03:44,620
Wo sie nie wirklich mit sich selbst konfrontiert werden. 

51
00:03:44,920 --> 00:03:47,080
Wo sie nie alleine sind. 

52
00:03:47,500 --> 00:03:52,040
Wie sehen Sie Ihre ehemaligen Mitstreiter? 

53
00:03:52,360 --> 00:03:58,720
Ich bewundere viele Schüler von Meister Deshimaru. 

54
00:03:58,980 --> 00:04:07,980
Wer hat seine Ausbildung erhalten und praktiziert weiterhin alleine. 

55
00:04:08,240 --> 00:04:14,320
Es ist besser zusammen zu üben. 

56
00:04:14,700 --> 00:04:19,140
Aber zumindest machen sie ihre eigenen Sachen. 

57
00:04:19,660 --> 00:04:23,980
Sie sind allein. 

