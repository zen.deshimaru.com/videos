1
00:00:00,000 --> 00:00:05,742
Mon maître m’enseignait, quand j’étais débutant (je le suis toujours...).

2
00:00:05,842 --> 00:00:07,832
« Zazen, c’est le satori. »

3
00:00:07,932 --> 00:00:18,497
À l’époque, dans les années 70, le mot « satori » était très intéressant, on voulait savoir ce qu’était le satori, une sorte d’illumination.

4
00:00:18,597 --> 00:00:22,731
Les gens voulaient faire zazen pour obtenir le satori.

5
00:00:22,831 --> 00:00:31,329
Et Sensei enseignait que c’était la posture de Bouddha elle-même, qu’il n’y a pas de satori en dehors de zazen.

6
00:00:31,429 --> 00:00:35,793
C’est ça, l’enseignement de maître Deshimaru. Il disait:

7
00:00:35,893 --> 00:00:39,950
« Le satori, c’est le retour aux conditions normales. »

8
00:00:40,050 --> 00:00:41,529
Nous, les jeunes, on était déçus.

9
00:00:41,629 --> 00:00:46,520
Les conditions normales, c’est chiant.

10
00:00:46,620 --> 00:00:49,388
On aimait bien fumer des joints, prendre du LSD...

11
00:00:49,490 --> 00:00:53,471
Les conditions normales, ça ne nous intéressait pas.

12
00:00:53,571 --> 00:00:58,853
Maintenant, en 2018, les conditions normales, c’est très recherché.

13
00:00:58,960 --> 00:01:02,400
Les saisons normales, un climat normal, une qualité de l’air normale.

14
00:01:02,400 --> 00:01:04,400
Un océan normal, une faune et une flore normales.

15
00:01:08,380 --> 00:01:10,980
Ça devient précieux, la normalité.

16
00:01:11,088 --> 00:01:13,155
N’oubliez jamais ces mots :

17
00:01:13,255 --> 00:01:15,705
« Zazen lui-même est le satori. »

18
00:01:15,805 --> 00:01:19,809
Le satori, c’est être au bon moment au bon endroit.

19
00:01:19,909 --> 00:01:21,516
C’est très difficile.

20
00:01:21,616 --> 00:01:27,840
Quand vous prenez la posture du Bouddha, vous êtes au bon moment au bon endroit.

21
00:01:27,940 --> 00:01:32,939
Vous êtes normal. Vous vous resynchronisez avec l’ordre cosmique.

22
00:01:33,039 --> 00:01:34,264
C’est mécanique.

23
00:01:34,364 --> 00:01:38,498
Avec son squelette d’être humain anormal, pollueur...

24
00:01:38,598 --> 00:01:42,831
Quand on prend la posture du Bouddha, c’est mécanique.

25
00:01:42,931 --> 00:01:47,854
On se recale dans le rythme cosmique universel. C’est le satori.

26
00:01:47,954 --> 00:01:50,503
En même temps, c’est tout simple.

27
00:01:50,603 --> 00:01:59,507
Mais quand on est tellement éloigné des conditions normales comme nous le sommes maintenant, ça a une valeur énorme.

28
00:01:59,607 --> 00:02:01,521
Et qu’est-ce que le zen ?

29
00:02:01,621 --> 00:02:04,017
C’est prendre la posture exacte.

30
00:02:04,117 --> 00:02:07,945
C’est comme un code pour ouvrir une porte secrète.

31
00:02:08,045 --> 00:02:10,035
On voit ça dans les films.

32
00:02:10,135 --> 00:02:16,359
Il faut appuyer sur ça, il faut que le rayon lumière arrive juste au bon endroit.

33
00:02:16,459 --> 00:02:17,990
Et la porte s’ouvre.

34
00:02:18,090 --> 00:02:21,229
C’est la même chose si vous alignez votre position.

35
00:02:21,320 --> 00:02:24,300


36
00:02:25,280 --> 00:02:29,320
Vous êtes assez sur les ischions. Sur le coussin, en lotus.

37
00:02:29,720 --> 00:02:32,400
La position du Bouddha, c’est en lotus.

38
00:02:32,860 --> 00:02:37,700
Si vous ne pouvez pas faire le lotus, faites un demi-lotus.

39
00:02:37,980 --> 00:02:40,580
Mais le lotus, ça n’a rien à voir.

40
00:02:41,060 --> 00:02:44,120
Ça inverse les polarités.

41
00:02:44,760 --> 00:02:52,720
Dès que vous prenez le lotus, au lieu de forcer, au lieu d’avoir mal, vous lâchez.

42
00:02:53,480 --> 00:02:56,380
La posture du Bouddha c’est précis.

43
00:02:56,940 --> 00:03:03,020
Quand on se met précisément comme il faut.

44
00:03:03,140 --> 00:03:08,240
Qu’on respire comme il faut, on soigne son corps et son esprit.

45
00:03:08,240 --> 00:03:14,360
C’est merveilleux et c’est très simple.

46
00:03:14,860 --> 00:03:17,840
Ce n’est pas une condition spéciale.

47
00:03:18,520 --> 00:03:23,020
Inutile de penser : « moi j’ai plus le satori que les autres ».

48
00:03:23,400 --> 00:03:26,080
C’est juste normal.

49
00:03:26,600 --> 00:03:32,940
S’il y a quelque chose d’anormal dans le système solaire, tout pète.

50
00:03:33,280 --> 00:03:38,720
Tout est donc précisément au bon endroit au bon moment, et dans un mouvement.

51
00:03:38,920 --> 00:03:42,660
Le secret est clair : on connaît la méthode.

52
00:03:42,940 --> 00:03:45,700
Il faut faire l’effort pour y arriver.

53
00:03:46,100 --> 00:03:48,460
Il faut s’entraîner.

54
00:03:48,740 --> 00:03:52,100
C’est extraordinaire de pouvoir se recaler à l’ordre cosmique.

55
00:03:52,300 --> 00:03:56,000
Sensei répétait tout le temps : « Follow cosmic order ».

56
00:03:56,160 --> 00:03:58,080
Il me criait dessus :

57
00:03:58,320 --> 00:04:01,500
« STÉPHANE, WHY DON’T YOU FOLLOW COSMIC ORDER? YOU MUST FOLLOW COSMIC ORDER! »
