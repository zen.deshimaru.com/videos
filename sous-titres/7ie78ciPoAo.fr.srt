1
00:00:00,000 --> 00:00:02,020
Le zen, c’est zazen !

2
00:00:02,220 --> 00:00:04,920
Zazen, c’est s’asseoir comme un Bouddha.

3
00:00:05,160 --> 00:00:08,400
Prendre la posture originelle de Bouddha.

4
00:00:08,620 --> 00:00:09,920
En saisir le cœur.

5
00:00:10,140 --> 00:00:12,360
S’installer dans cette posture.

6
00:00:12,660 --> 00:00:16,000
Et progressivement oublier qu’on est un corps douloureux.

7
00:00:16,320 --> 00:00:18,840
Tout tordu, avec un esprit compliqué.

8
00:00:19,080 --> 00:00:23,120
Et laisser faire la transmutation, l’alchimie du zazen.

9
00:00:23,760 --> 00:00:25,700
Seul l’être humain peut pratiquer.

10
00:00:26,060 --> 00:00:33,200
C’est notre évolution depuis des centaines de milliers d’années qui fait qu’on peut pratiquer cette posture.

11
00:00:33,520 --> 00:00:36,700
D’équilibre, de stabilité, de calme.

12
00:00:37,000 --> 00:00:40,200
D’étirement, d’ouverture, d’oubli de soi.

13
00:00:40,480 --> 00:00:46,960
D’unité avec tous les êtres, avec toute chose, avec l’univers entier.

14
00:00:47,740 --> 00:00:51,760
Maître Kosen dit que zazen, c’est le patrimoine mondial de l’humanité.

15
00:00:52,040 --> 00:00:54,600
On ne va pas obliger quelqu’un à faire zazen.

16
00:00:54,860 --> 00:00:56,220
Et ce n’est parfois pas le bon moment.

17
00:00:56,460 --> 00:00:57,960
On peut s’asseoir à un moment.

18
00:00:58,220 --> 00:01:02,940
Et passer à côté de ce que ça peut évoquer pour notre vie.

19
00:01:03,220 --> 00:01:07,460
Et puis à un autre moment, d’un seul coup, ça y est.

20
00:01:07,680 --> 00:01:09,560
Il y a question et réponse.

21
00:01:09,760 --> 00:01:11,120
C’est exactement ça qu’il faut qu’on pratique.

22
00:01:11,360 --> 00:01:17,460
Faire zazen une seule fois, même si ce n’est pas son truc, si on est réceptif, si on a envie, ça donne une information.

23
00:01:18,700 --> 00:01:21,140
Il y a énormément de pratiques différentes.

24
00:01:21,380 --> 00:01:22,880
Qui sont sûrement très bien.

25
00:01:23,160 --> 00:01:25,700
Il ne faut pas vouloir opposer les choses les unes aux autres.

26
00:01:26,000 --> 00:01:29,940
Zazen est une pratique assez exigeante.

27
00:01:30,240 --> 00:01:32,700
On ne prend pas n’importe quelle posture.

28
00:01:33,040 --> 00:01:38,440
La posture et l’état d’esprit correspondant sont essentiels.

29
00:01:39,160 --> 00:01:43,900
Mais zazen a sa propre vie, son propre fonctionnement.

30
00:01:44,160 --> 00:01:46,220
On dit parfois que c’est une « méditation ».

31
00:01:46,420 --> 00:01:47,980
Mais c’est juste un mot.

32
00:01:48,320 --> 00:01:51,580
On l’appelle « zazen », « posture du Bouddha ».

33
00:01:51,800 --> 00:01:53,700
Ce ne sont que des mots.

34
00:01:53,940 --> 00:01:56,180
Mais c’est cette posture-là que l’on pratique.

35
00:01:56,840 --> 00:01:59,220
On ne s’autoproclame pas enseignant ou maître.

36
00:01:59,500 --> 00:02:03,040
Même si l’on a suivi 1, 2, 3 stages.

37
00:02:03,300 --> 00:02:06,140
Il y a une relation intime, on suit un maître.

38
00:02:06,380 --> 00:02:09,460
On ne le suit pas partout où il va.

39
00:02:09,780 --> 00:02:12,920
On suit son enseignement, son esprit.

40
00:02:13,200 --> 00:02:14,760
Il y a le zazen lui-même.

41
00:02:15,000 --> 00:02:22,300
Puis, le fait de ne pas suivre son truc à soi, son idée propre.

42
00:02:22,580 --> 00:02:25,120
Mais de suivre quelque chose de plus grand.

43
00:02:25,560 --> 00:02:29,480
Quelque chose qui s’étend sur des générations.

44
00:02:29,860 --> 00:02:34,060
Depuis le Bouddha historique et avant lui.

45
00:02:34,380 --> 00:02:36,960
Il y a d’autres Bouddhas qui l’ont précédé.

46
00:02:37,240 --> 00:02:40,020
D’autres qui se sont assis dans la posture.

47
00:02:40,360 --> 00:02:42,720
D’autres qui ont réalisé la voie.

48
00:02:43,500 --> 00:02:45,520
Et c’est sérieux !

49
00:02:45,840 --> 00:02:59,580
Si vous avez aimé cette vidéo, n’hésitez pas à la liker et à vous abonner à la chaîne !
