1
00:00:05,860 --> 00:00:08,580
Deshimaru, I know I’m going to die soon.

2
00:00:08,820 --> 00:00:17,320
The time has come to ordain you as a monk to pass on Bodhidarma’s teaching in a new land.

3
00:00:31,020 --> 00:00:32,820
I want to help God.

4
00:00:33,100 --> 00:00:34,860
I want to help Christ.

5
00:00:43,260 --> 00:00:48,000
I want to help the true god.

6
00:00:48,240 --> 00:00:52,760
I want to come back to the true god.

7
00:00:53,360 --> 00:01:00,360
40 years since the arrival of Master Deshimaru in Europe.

8
00:01:00,800 --> 00:01:04,000
God is down.

9
00:01:04,320 --> 00:01:09,040
God is "fatigué", tired.

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. The Bodhidarma of modern times.

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru arrived in Paris in the summer of 1967 at the Gare du Nord railway station.

12
00:01:31,120 --> 00:01:33,480
Only a monk, with no money, who doesn’t speak French.

13
00:01:33,820 --> 00:01:36,600
In his luggage, only the fresh seed of Zen.

14
00:01:45,440 --> 00:01:49,600
The arrival

15
00:02:59,140 --> 00:03:03,060
If we can go back to the history of Zen…

16
00:03:04,380 --> 00:03:09,520
It all began in India and then: what happened?

17
00:03:09,720 --> 00:03:13,340
How did it go
from India to Japan?

18
00:03:13,640 --> 00:03:19,020
India, very deep meditation.

19
00:03:19,480 --> 00:03:22,340
But too traditional.

20
00:03:24,380 --> 00:03:29,980
Yoga is not finished in India.

21
00:03:30,720 --> 00:03:36,160
Some people have a high spiritual level, but most people don’t.

22
00:03:36,760 --> 00:03:43,440
But, yoga, finished. didn’t we finish

23
00:03:43,760 --> 00:03:48,240
But the land is too old.

24
00:03:48,940 --> 00:03:53,660
Bodhidarma brought zazen from India to China.

25
00:03:53,900 --> 00:03:58,280
But there is no longer zen in India.

26
00:03:58,860 --> 00:04:04,880
Then Dogen brought zazen from China to Japan.

27
00:04:05,160 --> 00:04:11,360
Do you mean that the soil is too old in Asia, Japan, China or India?

28
00:04:11,740 --> 00:04:14,740
Yes, India is too old.

29
00:04:14,960 --> 00:04:17,700
China is too old as well.

30
00:04:18,280 --> 00:04:20,280
Zen is finished in China.

31
00:04:20,700 --> 00:04:23,940
Japan is too accustomed to zen.

32
00:04:24,680 --> 00:04:27,240
They only care for ceremonies…

33
00:04:27,680 --> 00:04:30,380
They miss the essence of zen.

34
00:04:36,160 --> 00:04:48,320
Do you think that the spirit of Zen tends to decline?

35
00:04:48,640 --> 00:04:50,620
Yes, yes, yes!

36
00:04:51,040 --> 00:04:58,720
Do you think that the spirit of Zen can catch in France, like the seed of a flower?

37
00:04:59,040 --> 00:05:01,120
Yes!

38
00:05:03,020 --> 00:05:06,980
Teaching begins.

39
00:06:24,680 --> 00:06:30,080
Why did you choose to settle in France?

40
00:06:30,400 --> 00:06:37,000
France is better than another country to understand zen.

41
00:06:37,520 --> 00:06:44,300
Very easy to understand zazen.

42
00:06:45,120 --> 00:06:46,700
Or philosophy.

43
00:06:46,980 --> 00:06:48,980
For ideas.

44
00:06:49,300 --> 00:06:52,060
Very deep.

45
00:06:52,520 --> 00:06:57,980
For instance, Montaigne :

46
00:06:58,420 --> 00:07:03,900
“I do not care so much what I am to others as I care what I am to myself.”

47
00:07:05,880 --> 00:07:09,640
Descartes, Bergson…

48
00:07:10,140 --> 00:07:14,080
Mind-bearers, philosophers…

49
00:07:14,800 --> 00:07:16,840
They can undertand zen.

50
00:07:18,200 --> 00:07:25,740
They don’t know zen, but their thinking is the same…

51
00:07:26,460 --> 00:07:29,760
Why did you not go to America?

52
00:07:30,280 --> 00:07:33,840
I first wanted to bring Zen in the United-States.

53
00:07:34,220 --> 00:07:41,180
But they want to use zen for business,
for industry, psychology, psychoanalysis…

54
00:07:41,540 --> 00:07:44,180
They want to use it.

55
00:07:44,340 --> 00:07:49,020
It is not true zen. Not deep.

56
00:07:52,500 --> 00:07:56,160
A sangha appears.

57
00:09:08,270 --> 00:09:14,100
Before, education good, but in modern times, only average.

58
00:09:14,100 --> 00:09:23,740
The essence is finished.

59
00:09:24,560 --> 00:09:33,700
We only look for the average…

60
00:09:34,380 --> 00:09:41,600
Only the essence is necessary for a strong education.

61
00:09:43,380 --> 00:09:49,960
Always right or left…

62
00:09:51,040 --> 00:09:54,260
But we must embrace contradictions.

63
00:09:54,600 --> 00:09:59,720
Only by abandoning our ego can we find our true individuality.

64
00:10:03,920 --> 00:10:13,100
Our highest personality in when we follow
the cosmic truth, the cosmos.

65
00:10:13,500 --> 00:10:19,480
The we can find our true, strong ego.

66
00:10:20,580 --> 00:10:25,680
You want to make people strong?

67
00:10:26,540 --> 00:10:32,060
Yes, balanced and strong, little by little.

68
00:10:32,800 --> 00:10:41,600
We must find the true austerity.

69
00:10:43,360 --> 00:10:47,600
Teaching

70
00:12:46,000 --> 00:12:57,680
The essence of Zen is "Mushotoku", without purpose.

71
00:12:59,200 --> 00:13:05,520
All religions have an object.

72
00:13:06,400 --> 00:13:10,560
You pray to become rich or happy.

73
00:13:11,440 --> 00:13:17,040
But if we have a purpose, it is not strong.

74
00:13:18,400 --> 00:13:30,000
We must not have any purpose.

75
00:13:31,680 --> 00:13:38,000
You cannot concentrate.

76
00:13:39,360 --> 00:13:47,680
Do you mean that proper realization should be spontaneous?

77
00:13:48,480 --> 00:13:56,080
We must come back to the normal conditions.

78
00:13:57,040 --> 00:14:01,440
True Satori, transcendental
consciousness…

79
00:14:02,560 --> 00:14:07,520
is to come back to normal conditions.

80
00:14:09,440 --> 00:14:18,080
People are abnormal in modern times.

81
00:14:19,120 --> 00:14:27,120
Only the true god, for example, Christ or Buddha, had normal conditions for humans.

82
00:14:27,920 --> 00:14:33,900
Is the first necessary condition is one’s own determination?

83
00:14:34,340 --> 00:14:39,500
And the second, to abandon this determination?

84
00:14:39,760 --> 00:14:45,940
Yes. Individuality is necessary, and we must abandon it.

85
00:14:46,320 --> 00:14:50,180
We must embrace contradictions.

86
00:14:50,920 --> 00:14:56,560
It seems that you smoke a lot…

87
00:14:57,540 --> 00:15:03,260
Not that much!

88
00:15:04,220 --> 00:15:10,620
Does zen has made you strong enough to accept anything?

89
00:15:11,140 --> 00:15:15,360
Everything is possible!

90
00:15:15,920 --> 00:15:20,240
It is not so important to care about one’s life.

91
00:15:20,540 --> 00:15:25,960
The Europeans have too much egotism.

92
00:15:26,400 --> 00:15:31,840
You must accept everything, and go beyond!

93
00:15:32,180 --> 00:15:36,960
But zen is not asceticism, nor mortification.

94
00:15:37,920 --> 00:15:41,460
We must for cosmic life.

95
00:15:41,680 --> 00:15:45,900
We must not put limits.

96
00:15:46,320 --> 00:15:48,780
Otherwise, life becomes narrow.

97
00:15:52,340 --> 00:15:55,880
Keeping in touch with Japan

98
00:18:11,940 --> 00:18:15,640
About the education of children…

99
00:18:15,980 --> 00:18:27,960
In modern times, we focus only on the intellectual side of education.

100
00:18:28,360 --> 00:18:35,880
We must also see the physical side, the right muscular tension.

101
00:18:38,600 --> 00:18:45,740
Also, we must concentrate on the mundane aspects of daily life.

102
00:18:46,100 --> 00:18:50,560
Concentration and physical posture are very important.

103
00:18:50,960 --> 00:18:59,200
We must not be sloppy while eating, for example.

104
00:18:59,740 --> 00:19:04,200
Concentrating on daily life is critical.

105
00:19:04,620 --> 00:19:10,920
Even our posture while sleeping is critical!

106
00:19:11,860 --> 00:19:19,460
School teachers do not pay enough attention to physical posture.

107
00:19:19,800 --> 00:19:23,920
They only care about the intellectual side.

108
00:19:24,220 --> 00:19:27,679
Why are children not well brought up?

109
00:19:28,260 --> 00:19:30,680
Is education too easy?

110
00:19:31,200 --> 00:19:36,100
We come back to animals in modern times!

111
00:19:37,160 --> 00:19:46,380
Everything is too easy…

112
00:19:46,660 --> 00:19:50,040
We drive cars and no longer want to walk…

113
00:19:50,420 --> 00:20:00,160
Family education is too easy as well. It is a mistake.

114
00:20:01,360 --> 00:20:09,479
Warm, food. Too "softy-softy"!

115
00:20:10,600 --> 00:20:19,320
Our body becomes weak.

116
00:20:21,540 --> 00:20:26,180
Do we look for the freedom of animals?

117
00:20:26,540 --> 00:20:28,540
Wild animals are strong.

118
00:20:28,940 --> 00:20:40,340
Captive, well-fed animals in zoos are weak.

119
00:20:40,680 --> 00:20:42,680
They lack activity.

120
00:20:45,200 --> 00:20:47,700
Wild animals are strong!

121
00:20:56,140 --> 00:20:59,100
Intelligence is important.

122
00:20:59,360 --> 00:21:01,100
But it is not enough.

123
00:21:01,420 --> 00:21:06,720
We also need a strong body and higher wisdom.

124
00:21:09,080 --> 00:21:12,800
Creating the Gendronière temple

125
00:23:45,260 --> 00:23:50,540
You have brought to Europe the fruit of Zen so that people can taste it.

126
00:23:50,820 --> 00:24:05,540
India, China, Japan have the true essence of spiritual culture.

127
00:24:07,480 --> 00:24:18,000
We must now merge the Asian and European civilizations.

128
00:24:23,660 --> 00:24:31,000
Zen is the essence of Asian culture.

129
00:24:31,760 --> 00:24:39,580
If you practice Zen, you will understand Asian civilizations.

130
00:24:40,660 --> 00:24:55,280
Will there be a spiritual renaissance through the meeting of Western tradition and Zen?

131
00:24:55,620 --> 00:24:59,780
I think so. I hope so.

132
00:25:00,380 --> 00:25:04,780
It’s important that everybody become God.

133
00:25:05,120 --> 00:25:07,120
What is the way to achieve this?

134
00:25:07,400 --> 00:25:11,320
If you practice zazen, you can become God.

135
00:25:11,660 --> 00:25:21,300
Is the true god the natural god in us?

136
00:25:21,520 --> 00:25:29,520
Yes, everyone must create the true god.

137
00:25:31,200 --> 00:25:35,180
The last teaching

138
00:25:36,600 --> 00:25:40,360
Zen philosophy is very wide.

139
00:25:40,800 --> 00:25:44,980
You must embrace contradictions!

140
00:25:45,420 --> 00:25:47,440
Life and death, for instance?

141
00:25:47,680 --> 00:25:50,400
Yes, life and death!

142
00:28:10,080 --> 00:28:27,360
Devoted to Mokudo Taisen Deshimaru (1914-1982)

