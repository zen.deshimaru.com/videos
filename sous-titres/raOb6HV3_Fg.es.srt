1
00:00:00,000 --> 00:00:05,742
Mi maestro me enseñó cuando era principiante (todavía soy …). 

2
00:00:05,842 --> 00:00:07,832
"Zazen es satori". 

3
00:00:07,932 --> 00:00:18,497
En ese momento, en la década de 1970, la palabra "satori" era muy interesante, queríamos saber qué era satori, una especie de iluminación. 

4
00:00:18,597 --> 00:00:22,731
La gente quería hacer zazen para obtener satori. 

5
00:00:22,831 --> 00:00:31,329
Y Sensei enseñó que era la actitud del mismo Buda, que no hay satori afuera. 

6
00:00:31,429 --> 00:00:35,793
Esa es la enseñanza del Maestro Deshimaru. El siempre decía: 

7
00:00:35,893 --> 00:00:39,950
"Satori es el regreso a las circunstancias normales". 

8
00:00:40,050 --> 00:00:41,529
Los jóvenes estábamos decepcionados. 

9
00:00:41,629 --> 00:00:46,520
Las condiciones normales son aburridas. 

10
00:00:46,620 --> 00:00:49,388
Nos gustaba fumar marihuana, tomamos LSD … 

11
00:00:49,490 --> 00:00:53,471
No estábamos interesados ​​en circunstancias normales. 

12
00:00:53,571 --> 00:00:58,853
Ahora, en 2018, las condiciones normales tienen una gran demanda. 

13
00:00:58,960 --> 00:01:02,400
Estaciones normales, clima normal, calidad del aire normal. 

14
00:01:02,400 --> 00:01:04,400
Un océano normal, fauna y flora normales. 

15
00:01:08,380 --> 00:01:10,980
La normalidad se vuelve cara. 

16
00:01:11,088 --> 00:01:13,155
Nunca olvides estas palabras: 

17
00:01:13,255 --> 00:01:15,705
"El propio Zazen es satori". 

18
00:01:15,805 --> 00:01:19,809
Satori se trata de estar en el lugar correcto en el momento correcto. 

19
00:01:19,909 --> 00:01:21,516
Es muy dificil. 

20
00:01:21,616 --> 00:01:27,840
Si adoptas la postura de Buda, estás en el lugar correcto en el momento correcto. 

21
00:01:27,940 --> 00:01:32,939
Eres normal Estás sincronizando con el orden cósmico. 

22
00:01:33,039 --> 00:01:34,264
Es mecanico. 

23
00:01:34,364 --> 00:01:38,498
Con su esqueleto de una persona anormal, un contaminador … 

24
00:01:38,598 --> 00:01:42,831
Si tomas la actitud del Buda, es mecánica. 

25
00:01:42,931 --> 00:01:47,854
Regresamos al ritmo cósmico universal. Eso es satori. 

26
00:01:47,954 --> 00:01:50,503
Al mismo tiempo, es muy simple. 

27
00:01:50,603 --> 00:01:59,507
Pero si está tan lejos de las condiciones normales como nosotros ahora, tiene un gran valor. 

28
00:01:59,607 --> 00:02:01,521
¿Y qué es el zen? 

29
00:02:01,621 --> 00:02:04,017
Toma la posición exacta. 

30
00:02:04,117 --> 00:02:07,945
Es como un código para abrir una puerta secreta. 

31
00:02:08,045 --> 00:02:10,035
Lo ves en el cine. 

32
00:02:10,135 --> 00:02:16,359
Tienes que presionar esto, debes colocar el haz de luz en el lugar correcto. 

33
00:02:16,459 --> 00:02:17,990
Y la puerta se abre. 

34
00:02:18,090 --> 00:02:21,229
Es lo mismo si alineas tu posición. 

35
00:02:25,280 --> 00:02:29,320
Eres hermosa en los alfileres. Sobre la almohada, en loto. 

36
00:02:29,720 --> 00:02:32,400
La posición del Buda es en loto. 

37
00:02:32,860 --> 00:02:37,700
Si no puedes hacer el loto, haz medio loto. 

38
00:02:37,980 --> 00:02:40,580
Pero el loto no tiene nada que ver con eso. 

39
00:02:41,060 --> 00:02:44,120
Invierte las polaridades. 

40
00:02:44,760 --> 00:02:52,720
Tan pronto como tomes el loto, en lugar de forzarlo, en lugar de lastimarlo, lo dejas ir. 

41
00:02:53,480 --> 00:02:56,380
La actitud del Buda es precisa. 

42
00:02:56,940 --> 00:03:03,020
Si te pones la ropa adecuada. 

43
00:03:03,140 --> 00:03:08,240
Respira bien, sana tu cuerpo y tu mente. 

44
00:03:08,240 --> 00:03:14,360
Es hermoso y es muy simple. 

45
00:03:14,860 --> 00:03:17,840
No es una condición especial. 

46
00:03:18,520 --> 00:03:23,020
No tienes que pensar: "Tengo más satori que los demás". 

47
00:03:23,400 --> 00:03:26,080
Es normal 

48
00:03:26,600 --> 00:03:32,940
Si hay algo anormal en el sistema solar, tira un pedo a todo. 

49
00:03:33,280 --> 00:03:38,720
Entonces todo está exactamente en el lugar correcto en el momento correcto y en un solo movimiento. 

50
00:03:38,920 --> 00:03:42,660
El secreto es claro: conocemos el método. 

51
00:03:42,940 --> 00:03:45,700
Deberías tomarte la molestia. 

52
00:03:46,100 --> 00:03:48,460
Se necesita práctica. 

53
00:03:48,740 --> 00:03:52,100
Es extraordinario poder fallar en el orden cósmico. 

54
00:03:52,300 --> 00:03:56,000
Sensei repetía: "Sigue el orden cósmico". 

55
00:03:56,160 --> 00:03:58,080
Me gritó: 

56
00:03:58,320 --> 00:04:01,500
"ENTONCES, ¿POR QUÉ NO SIGUE LA ORDEN CÓSMICA? ¡DEBE SEGUIR LA ORDEN CÓSMICA!" 

