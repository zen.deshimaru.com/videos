1
00:00:00,000 --> 00:00:04,799
Wie synchronisiert man das Gehen und Atmen von Verwandten? 

2
00:00:04,799 --> 00:00:08,550
Sie sagten, wir würden mit jedem Ausatmen noch einen Schritt weiter gehen. 

3
00:00:08,550 --> 00:00:15,690
Aber die meisten Menschen atmen zweimal in einem Schritt. 

4
00:00:15,690 --> 00:00:19,470
Natürlich möchte ich zwei machen. 

5
00:00:19,470 --> 00:00:24,180
Menschen verdoppeln ihre Atmung, weil sie zwischen den Atemzügen keine Pause machen. 

6
00:00:24,180 --> 00:00:28,650
Wenn Sie langsam mit dem Kind gehen, ist die kleine Pause von grundlegender Bedeutung. 

7
00:00:28,650 --> 00:00:33,059
Wenn Sie sich ein wenig auf dem Ausatmen ausruhen … 

8
00:00:33,059 --> 00:00:38,210
Du bist im Rhythmus. 

9
00:00:38,210 --> 00:00:44,309
Dogen und sein Meister Nyojo sagen …. 

10
00:00:44,309 --> 00:00:50,219
… dass der Buddha selbst gesagt hat, dass Sie Ihren Atem nicht verdoppeln. 

11
00:00:50,219 --> 00:00:55,050
Dazu muss am Ende der Ausatmung eine kleine Pause eingelegt werden … 

12
00:00:55,050 --> 00:01:03,239
…. in dem wir einen halben Schritt vorwärts gehen … 

13
00:01:03,239 --> 00:01:09,299
… nach einer kurzen Pause vor dem Einatmen und wir sind in einem guten Rhythmus. 

14
00:01:09,299 --> 00:01:14,610
Wir haben es satt … viel weniger. 

15
00:01:14,610 --> 00:01:20,580
Während des Sesshin erweiterte ich die Familienhinweise. 

16
00:01:20,580 --> 00:01:26,159
Ich hatte dies bereits in einer anderen Übung mit einem anderen Meister gelebt … 

17
00:01:26,159 --> 00:01:32,670
Sie mussten lange mit verbundenen Augen gehen. 

18
00:01:32,670 --> 00:01:38,280
Wir müssen gehen, ohne etwas zu sehen. 

19
00:01:38,280 --> 00:01:42,270
Es ist anstrengend, weil Sie auch Ihre Hände in eine Position bringen müssen …. 

20
00:01:42,270 --> 00:01:45,540
… weniger bequem als in kind-hin. 

21
00:01:45,540 --> 00:01:51,180
Wenn Sie lange Zeit Familie hin tun, wird es müde …. 

22
00:01:51,180 --> 00:01:54,659
… ein weiterer Aspekt von uns. 

23
00:01:54,659 --> 00:01:59,360
Es ist ein aktiver Aspekt. Ich habe in der chinesischen Medizin gelernt …. 

24
00:01:59,360 --> 00:02:06,920
…. dass, um übermäßige Aktivität zu heilen, Sie Ruhe brauchen. 

25
00:02:13,340 --> 00:02:18,170
Wenn Sie lange Zeit Familie hin tun, sind Sie erschöpft. 

26
00:02:18,170 --> 00:02:23,230
Sie haben Rückenschmerzen, Schulterschmerzen, Schmerzen in den Armen … 

27
00:02:23,230 --> 00:02:29,330
Wenn Sie eine Stunde lang so laufen, sind die Leute erschöpft. 

28
00:02:29,330 --> 00:02:33,290
Sie werden nur eines wollen: in Zazen sein. 

29
00:02:33,290 --> 00:02:41,470
Was für eine Erleichterung, wenn Sie in Zazen sind: Es ist komplementär. 

30
00:02:41,470 --> 00:02:46,549
Du sagst dir: Was für eine Entspannung, was für eine Hingabe! 

31
00:02:46,549 --> 00:02:51,650
Du willst wirklich in Zazen sein. 

32
00:02:51,650 --> 00:02:57,920
Nach einer Stunde in Zazen werden Sie das Gefühl haben … 

33
00:02:57,920 --> 00:03:02,720
….. unangenehmes Kribbeln … 

34
00:03:02,720 --> 00:03:07,040
Sie werden sich bewegen wollen und wünschen, es würde wegen des Schmerzes enden. 

35
00:03:07,040 --> 00:03:11,630
Zazen und langsames Gehen ergänzen sich vollständig. 

36
00:03:11,630 --> 00:03:14,870
Aber weil wir für kurze Zeit Kinderhin üben, merken wir es nicht. 

37
00:03:14,870 --> 00:03:19,430
Ich werde jetzt das Kind während sesshinsµ länger hins machen. 

38
00:03:19,430 --> 00:03:25,130
Damit sich die Leute gerne hinsetzen und bestimmte Dinge in Zazen loslassen. 

39
00:03:25,130 --> 00:03:29,350
Ihre Frage ist sehr gut und nicht neu. 

40
00:03:29,350 --> 00:03:38,920
Master Dogen stellte das Problem vor 1.400 Jahren. 

