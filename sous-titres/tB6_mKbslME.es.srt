1
00:00:00,000 --> 00:00:04,660
Dijiste que si los jefes de estado llevan a cabo próstatas, 

2
00:00:05,080 --> 00:00:08,560
El mundo sería más pacífico. 

3
00:00:08,880 --> 00:00:13,040
Me preguntaba … 

4
00:00:13,340 --> 00:00:18,340
en comparación con otras religiones. 

5
00:00:18,620 --> 00:00:23,080
En la religión musulmana se habla de postración. 

6
00:00:23,460 --> 00:00:34,540
Aún así, algunos países del Medio Oriente se están desgarrando entre sí. 

7
00:00:35,180 --> 00:00:42,020
Quería saber por qué. 

8
00:00:42,320 --> 00:00:46,000
Porque la postración es la misma en el zen y en el islam. 

9
00:00:46,340 --> 00:00:52,660
Creo que hay buenas y malas en todas las religiones. 

10
00:00:53,080 --> 00:00:58,240
Pero la postración es algo bueno en la religión musulmana. 

11
00:00:58,400 --> 00:01:04,620
Los cristianos también unen sus manos de la misma manera que en el Zen, en Gasshô. 

12
00:01:05,100 --> 00:01:08,520
Esta bien 

13
00:01:08,700 --> 00:01:14,040
También hay fanatismo, siempre ha estado ahí. 

14
00:01:14,300 --> 00:01:18,820
Siempre ha habido guerras religiosas. 

15
00:01:21,320 --> 00:01:26,920
En el zen, la postración no se realiza con un propósito específico. 

16
00:01:27,280 --> 00:01:33,440
Ni para adorar la estatua en el altar, ni para rezar a Buda. 

17
00:01:33,960 --> 00:01:39,640
Es la práctica de la postura con todo tu cuerpo. 

18
00:01:40,020 --> 00:01:46,140
En el momento en que tocas el suelo, hay una conciencia. 

19
00:01:46,500 --> 00:01:50,860
Por lo general, el cerebro nunca toca el suelo. 

20
00:01:51,100 --> 00:01:55,820
Siempre está en el cielo, siempre apuntando a las estrellas. 

21
00:01:56,100 --> 00:02:01,480
Entonces se siente bien. Para volver a conectar, inclinarse, tocar. 

22
00:02:01,740 --> 00:02:06,300
Cuando los musulmanes hacen esto, es porque hay cosas muy buenas en el Islam. 

23
00:02:06,660 --> 00:02:13,000
Las familias crían bien a sus hijos. 

24
00:02:15,500 --> 00:02:21,620
Los verdaderos musulmanes están llenos de amor. 

25
00:02:24,200 --> 00:02:30,020
¡No tire al bebé con el agua del baño! 

26
00:02:30,550 --> 00:02:34,450
¡Es bueno que se postran! 

