1
00:00:10,260 --> 00:00:23,160
Todos los días, en todo el mundo, los miembros de la sangha Kosen practican zazen juntos. 

2
00:00:24,360 --> 00:00:28,520
Te he contado sobre la infancia del Maestro Deshimaru. 

3
00:00:28,820 --> 00:00:30,820
De sus dudas, su investigación … 

4
00:00:30,820 --> 00:00:35,760
Hasta conocer a Kodo Sawaki y encontrar su pose de zazen. 

5
00:00:36,040 --> 00:00:39,100
Encontrar tu propia pose de zazen: 

6
00:00:40,240 --> 00:00:41,860
"¡Qué placer!" 

7
00:00:42,240 --> 00:00:46,100
Creo que aquellos que conocieron su actitud zazen … 

8
00:00:46,100 --> 00:00:49,120
No han perdido su tiempo en este planeta. 

9
00:00:49,500 --> 00:00:52,540
Es un regalo inconmensurable. 

10
00:00:52,540 --> 00:00:54,239
Incluso si en esta actitud … 

11
00:00:54,540 --> 00:00:59,920
También hay sufrimiento y miedo. 

12
00:01:00,320 --> 00:01:01,960
Todo está presente en esta actitud. 

13
00:01:01,960 --> 00:01:06,160
Pero lo miras desde arriba. 

14
00:01:06,500 --> 00:01:10,740
Antes de aprender las lecciones de Kodo Sawaki … 

15
00:01:12,040 --> 00:01:15,980
El maestro Deshimaru había estudiado la Biblia. 

16
00:01:17,360 --> 00:01:21,020
Estaba un poco enamorado de una chica holandesa … 

17
00:01:23,020 --> 00:01:24,820
La hija de un ministro. 

18
00:01:24,960 --> 00:01:28,380
Aprovechó la oportunidad para estudiar la Biblia y el inglés. 

19
00:01:28,720 --> 00:01:32,140
En realidad, estudió inglés a partir del estudio de la Biblia. 

20
00:01:32,140 --> 00:01:37,060
Visitó iglesias cristianas y cantó himnos. 

21
00:01:37,220 --> 00:01:41,360
Te digo esto porque es Pascua. 

22
00:01:41,660 --> 00:01:51,880
Y ahora Jesús está entre su tumba y la cuarta o quinta dimensión. 

23
00:01:52,340 --> 00:01:56,580
Sensei nos dijo que cuando vino a Francia … 

24
00:01:57,000 --> 00:02:02,360
Había traído su Biblia y los himnos de su juventud. 

25
00:02:02,680 --> 00:02:07,940
Durante un sesshin de Pascua, nos dio un beso sobre Cristo. 

26
00:02:08,280 --> 00:02:11,720
Según el Evangelio de Lucas .. 

27
00:02:11,720 --> 00:02:15,620
Para que Cristo … 

28
00:02:16,140 --> 00:02:20,240
Podría hablar para que se juzgue a Poncio Pilato. 

29
00:02:20,600 --> 00:02:26,240
Poncio Pilato llamó a los principales sacerdotes. 

30
00:02:26,240 --> 00:02:28,820
Y cuestionó a Jesús en su presencia. 

31
00:02:29,180 --> 00:02:30,340
Él les dice a ellos: 

32
00:02:30,600 --> 00:02:33,020
"Me presentaste a este hombre como … 

33
00:02:33,020 --> 00:02:39,820
Alguien que está guiando a la gente en la dirección equivocada ". 

34
00:02:40,280 --> 00:02:47,640
"Le pregunté y no encontré nada malo en este hombre". 

35
00:02:48,000 --> 00:02:50,260
"No he encontrado ninguno de los motivos por los que lo acusas". 

36
00:02:50,580 --> 00:02:56,940
"Herodes tampoco ha encontrado ninguna razón para acusar". 

37
00:02:57,260 --> 00:03:02,620
"Concluyo que este hombre no ha hecho nada para merecer la pena de muerte". 

38
00:03:03,240 --> 00:03:06,480
En ese momento, la gente exclamó con 1 voz: 

39
00:03:06,480 --> 00:03:15,100
"Muerto para Jesús, ¡suelta a Barrabás!" 

40
00:03:16,240 --> 00:03:24,280
"Barrabás fue encarcelado por conducta desordenada y asesinato". 

41
00:03:24,620 --> 00:03:29,100
Poncio Pilato ofreció .. 

42
00:03:29,100 --> 00:03:32,200
Para liberar a uno de los prisioneros. 

43
00:03:32,800 --> 00:03:38,480
Pero la gente dijo: "¡Libertad a Barrabás!" 

44
00:03:38,940 --> 00:03:44,260
Poncio Pilato se dirige a las personas nuevamente: 

45
00:03:44,260 --> 00:03:49,640
Y él las vuelve a decir y le habla a la gente: 

46
00:03:49,960 --> 00:03:53,820
"Sé razonable, Jesús no hizo nada malo .. 

47
00:03:53,820 --> 00:03:55,800
¡Él solo habló, eso es todo! " 

48
00:03:55,800 --> 00:03:59,620
Todos gritaron: "¡Crucifícalo!" 

49
00:03:59,860 --> 00:04:02,320
Poncio Pilato finalmente dijo: 

50
00:04:02,620 --> 00:04:04,340
"Como quieras .. 

51
00:04:04,340 --> 00:04:06,300
No es mi problema .. " 

52
00:04:07,200 --> 00:04:11,640
Y en respuesta pronunció la famosa frase: 

53
00:04:11,640 --> 00:04:13,500
"Yo era mis manos en inocencia!" 

54
00:04:13,560 --> 00:04:17,540
Esta expresión significa que … 

55
00:04:17,540 --> 00:04:19,960
Debe lavarse las manos con la mayor frecuencia posible. 

56
00:04:20,140 --> 00:04:22,320
Para evitar el coronavirus. 

57
00:04:22,400 --> 00:04:24,920
Tienes que decir cada vez: "¡Me lavo las manos con inocencia!" 

58
00:04:25,240 --> 00:04:29,880
Esta es una enseñanza de Deshimaru, dice esto. 

59
00:04:30,360 --> 00:04:35,760
"Así, Jesús fue condenado por la opinión pública". 

60
00:04:36,020 --> 00:04:39,140
"La psicología de masas mató a Cristo". 

61
00:04:39,380 --> 00:04:44,200
"Mimetismo, negativa a cuestionarse a sí misma … 

62
00:04:44,200 --> 00:04:46,160
Eso mató a Cristo ". 

63
00:04:46,300 --> 00:04:48,940
La multitud, la multitud … 

64
00:04:48,940 --> 00:04:54,800
Contiene en sí mismo una energía poderosa. 

65
00:04:54,800 --> 00:04:57,720
Que puede ser tanto negativo como peligroso. 

66
00:04:57,800 --> 00:05:02,900
Es diferente cuando nos unimos para hacer zazen. 

67
00:05:03,180 --> 00:05:05,640
Cada uno de nosotros nos mira a nosotros mismos … 

68
00:05:05,640 --> 00:05:11,820
Mientras está en armonía con el grupo. 

69
00:05:12,160 --> 00:05:16,260
¡Esto es especialmente importante por ahora con Zazoom! 

70
00:05:16,680 --> 00:05:20,520
Por ejemplo, quemé incienso en mi habitación. 

71
00:05:20,920 --> 00:05:25,260
Sensei solía decir: "En zazen estás solo". 

72
00:05:25,640 --> 00:05:28,220
Pero si practicas zazen en un dojo … 

73
00:05:28,220 --> 00:05:31,460
Estamos en un grupo, incluso cuando estamos solos. 

74
00:05:31,820 --> 00:05:35,260
Pero entonces no hay confusión emocional. 

75
00:05:35,560 --> 00:05:38,320
Kodo Sawaki solía decir sobre esto: 

76
00:05:38,320 --> 00:05:41,640
"No deberíamos estar viviendo en una tontería grupal". 

77
00:05:42,020 --> 00:05:45,680
"Y no guardes esta aberración para la experiencia real". 

78
00:05:45,940 --> 00:05:48,480
"Este es el destino de la gente común … 

79
00:05:48,480 --> 00:05:50,540
Quien no tiene otra forma de verlo … 

80
00:05:50,860 --> 00:05:54,180
Luego con los ojos de la imbecilidad colectiva ". 

81
00:05:54,700 --> 00:06:02,080
El Buda daba una lección pública una vez por semana. 

82
00:06:02,600 --> 00:06:05,620
Todos los discípulos, monjes y laicos. 

83
00:06:05,620 --> 00:06:08,360
Reunidos afuera … 

84
00:06:08,360 --> 00:06:11,000
Ciertamente en lugares privilegiados. 

85
00:06:11,880 --> 00:06:14,960
El Buda le dio un beso. 

86
00:06:15,320 --> 00:06:16,980
Y todos estaban en zazen … 

87
00:06:17,960 --> 00:06:19,880
Y lo escuché .. 

88
00:06:19,980 --> 00:06:23,260
Y luego todos volvieron a su camino. 

89
00:06:23,760 --> 00:06:25,560
Todos practicaban solos … 

90
00:06:25,560 --> 00:06:27,960
Una semana, un mes … 

91
00:06:28,240 --> 00:06:30,100
Y luego regresó regularmente .. 

92
00:06:30,280 --> 00:06:32,680
escuchar las palabras del Buda 

93
00:06:32,680 --> 00:06:37,840
Dojos solo apareció en China. 

94
00:06:38,180 --> 00:06:40,900
Y comenzamos a practicar en grupo … 

95
00:06:40,900 --> 00:06:42,560
Con un orden y reglas. 

96
00:06:44,580 --> 00:06:48,280
Y entonces el grupo es muy poderoso … 

97
00:06:48,800 --> 00:06:50,960
Tanto buenos como malos. 

98
00:06:51,200 --> 00:06:53,020
Sensei realmente dijo: 

99
00:06:53,020 --> 00:06:57,240
"Jesucristo le dio a su cuerpo el sabor de la muerte". 

100
00:06:57,620 --> 00:07:01,700
"Por el profundo lago de la nada y la desesperación". 

101
00:07:02,180 --> 00:07:06,420
"Se ha convertido en un símbolo de la vida eterna". 

102
00:07:06,740 --> 00:07:09,180
"Su vida es idéntica a la de un koan zen". 

103
00:07:09,380 --> 00:07:11,380
Esto es lo que dijo Sensei. 

