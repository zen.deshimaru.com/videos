1
00:00:06,180 --> 00:00:09,540
No me importa el zen. 

2
00:00:09,840 --> 00:00:12,100
Es solo otro pedazo de basura de la iglesia. 

3
00:00:12,480 --> 00:00:17,520
Hay miles de maestros zen en los Estados Unidos. 

4
00:00:17,820 --> 00:00:20,880
Todos quieren ser maestros zen. 

5
00:00:21,220 --> 00:00:24,680
Y el zen está de moda 

6
00:00:25,280 --> 00:00:28,820
Y no estoy interesado. 

7
00:00:29,320 --> 00:00:31,960
¿Por qué crees que está de moda? 

8
00:00:32,260 --> 00:00:36,240
No, ahora está pasado de moda! 

9
00:00:37,300 --> 00:00:40,060
En Argentina, todos compran un pequeño jardín zen. 

10
00:00:40,340 --> 00:00:42,720
Lo pusimos en la oficina, en su sala de estar. 

11
00:00:43,020 --> 00:00:45,740
Es hermoso Es divertido Es divertido 

12
00:00:46,100 --> 00:00:49,000
¡Pero el zen es inútil! 

13
00:00:53,900 --> 00:00:57,360
¿Qué opinas de la expresión de Krishnamurti: 

14
00:00:57,640 --> 00:01:01,060
"¿No dejes que nadie se interponga entre tú y la verdad"? 

15
00:01:01,380 --> 00:01:07,140
Si hay una discrepancia entre usted y la verdad, no es la verdad. 

16
00:01:11,480 --> 00:01:16,780
Quería decir que siempre estaba en contra de la corriente. 

17
00:01:17,040 --> 00:01:21,960
Se había separado de toda la sociedad teológica, que quería tomarlo como referencia. 

18
00:01:22,300 --> 00:01:24,260
¿Admiras eso de él? 

19
00:01:24,540 --> 00:01:31,440
Si Mi madre, cuando estaba embarazada de mí, era discípula de Krishnamurti. 

20
00:01:31,760 --> 00:01:37,500
Un día, en una conferencia, sintió un éxtasis, un satori. 

21
00:01:37,760 --> 00:01:42,620
Se dijo a sí misma: "mi hijo será un santo". 

22
00:01:43,020 --> 00:01:50,060
Krishnamurti me gusta mucho, era una persona extraordinaria. 

23
00:01:51,640 --> 00:01:56,940
Sai Baba? No lo sé, no lo conozco. 

24
00:01:57,360 --> 00:02:02,020
Es el único maestro que escuché decir que era Dios. 

25
00:02:02,320 --> 00:02:04,680
¡Pero todos son Dios! 

26
00:02:05,740 --> 00:02:11,540
Un verdadero maestro no tiene que decir "Yo soy Dios". 

27
00:02:11,940 --> 00:02:16,200
Pero tienes que decirles a los demás que eres Dios. 

28
00:02:16,500 --> 00:02:22,740
Entonces el hombre dice: "Yo soy Dios. ¡Dame tu dinero!" 

29
00:02:23,060 --> 00:02:27,960
Y el hecho de que hace trucos como hacer aparecer relojes, ¿adivina qué? 

30
00:02:28,240 --> 00:02:32,940
No lo se No lo se No lo se Sí, tal vez él tiene una fuerza. Si es posible. 

31
00:02:33,080 --> 00:02:34,400
¿Es posible? Si es posible. 

32
00:02:34,680 --> 00:02:37,040
Hay magos que dicen que es ilusionismo. 

33
00:02:37,320 --> 00:02:40,820
También es posible que fuera una artimaña. 

34
00:02:40,980 --> 00:02:48,180
Creo que todas las religiones, todas las iglesias, están en contra del hombre. 

35
00:02:48,280 --> 00:02:50,280
¿Todos sin distinción? 

36
00:02:51,740 --> 00:02:54,760
También el budismo, todos. 

37
00:02:55,020 --> 00:02:57,820
Estas son las iglesias. 

38
00:02:58,400 --> 00:03:00,400
No me gusta 

39
00:03:02,740 --> 00:03:08,100
El verdadero Zen de Deshimaru no es una iglesia, es una escuela. 

40
00:03:08,480 --> 00:03:10,060
Es otra cosa. 

41
00:03:10,380 --> 00:03:12,380
¿Cómo puedes notar la diferencia? 

42
00:03:12,900 --> 00:03:16,740
¡La iglesia, son los japoneses! 

43
00:03:20,060 --> 00:03:26,000
En una iglesia la gente busca un camino espiritual. 

44
00:03:26,400 --> 00:03:33,240
Y después de 10 años están buscando un título, en una jerarquía, un poder sobre los demás. 

45
00:03:33,560 --> 00:03:42,620
Es una cosa terrible que mata la verdad. 

46
00:03:43,160 --> 00:03:45,680
Yo practico zazen. 

47
00:03:46,040 --> 00:03:49,820
Practico las enseñanzas de los Budas, lo cual es muy simple. 

48
00:03:50,120 --> 00:03:54,520
Para mí es un tesoro de la humanidad. 

49
00:03:56,500 --> 00:03:58,640
Zazen [meditación sentada]. 

50
00:03:58,960 --> 00:04:02,180
Kinhin [Camina despacio] 

51
00:04:02,700 --> 00:04:04,700
Sampaï. 

52
00:04:04,900 --> 00:04:09,020
Genmai [comida de la mañana] y samu [concentración en el trabajo]. 

53
00:04:09,360 --> 00:04:10,880
Es muy simple 

54
00:04:11,180 --> 00:04:12,880
Pero siempre cambia. 

55
00:04:13,140 --> 00:04:16,000
Siempre agregas cosas nuevas. 

56
00:04:16,320 --> 00:04:18,340
Si estoy aprendiendo 

57
00:04:18,480 --> 00:04:27,580
Porque creo que me gusta aprender y tener más sabiduría para evolucionar. 

58
00:04:27,880 --> 00:04:29,480
Preguntame 

59
00:04:29,800 --> 00:04:33,660
Disfruto ser un discípulo más que un maestro. 

60
00:04:34,100 --> 00:04:35,740
Me gustan los dos. 

61
00:04:36,120 --> 00:04:40,540
Soy un buen maestro, pero soy muy mal estudiante. 

62
00:04:40,820 --> 00:04:42,780
Muy indisciplinado. 

63
00:04:43,200 --> 00:04:46,160
¿Siempre has sido así? Incluido con Deshimaru? 

64
00:04:46,400 --> 00:04:47,140
Si 

65
00:04:47,420 --> 00:04:48,840
¿Y él te corrigió? 

66
00:04:49,080 --> 00:04:50,700
Sí, él me ayudó mucho. 

67
00:04:50,980 --> 00:04:54,240
Me puso a cargo, me calmó un poco. 

68
00:04:58,060 --> 00:05:00,280
"¿Qué voy a hacer con Stéphane?" 

69
00:05:00,560 --> 00:05:04,180
"¡Aquí estoy a cargo de él!" 

70
00:05:08,920 --> 00:05:11,860
¿Recuerdas el día que conociste a Deshimaru? 

71
00:05:12,120 --> 00:05:12,620
Si 

72
00:05:12,920 --> 00:05:14,920
Como fue 

73
00:05:15,280 --> 00:05:16,820
No puedo decirlo 

74
00:05:17,060 --> 00:05:19,240
Estaba en Zazen y él estaba detrás de mí con el kyosaku. 

75
00:05:22,920 --> 00:05:28,320
Más tarde compré flores en el metro camino al dojo. 

76
00:05:28,640 --> 00:05:33,060
A la entrada del dojo digo: "Tengo flores para el maestro". »» 

77
00:05:33,400 --> 00:05:36,080
Y después de Zazen, me llaman. 

78
00:05:36,340 --> 00:05:38,540
"El maestro pregunta por ti". 

79
00:05:38,780 --> 00:05:42,860
Voy a su habitación, donde está sentado. 

80
00:05:43,060 --> 00:05:46,760
"¿Quién me trajo estas flores?" 

81
00:05:47,040 --> 00:05:47,800
Yo 

82
00:05:48,660 --> 00:05:51,580
"Bien, bien, bien". 

83
00:05:54,280 --> 00:05:58,080
¿Recuerdas el último día que lo viste? 

84
00:05:58,580 --> 00:05:59,920
El estaba muerto. 

85
00:06:05,280 --> 00:06:07,280
Su cuerpo iba a ser quemado. 

86
00:06:07,520 --> 00:06:09,160
Adios 

87
00:06:10,320 --> 00:06:12,320
¿Y el último día que lo viste con vida? 

88
00:06:13,860 --> 00:06:19,800
Luego fue a Japón por última vez y dijo "¡adiós!". 

89
00:06:20,200 --> 00:06:23,680
El día que murió Deshimaru, ¿lloraste? 

90
00:06:24,360 --> 00:06:34,420
Cuando murió, yo estaba en el dojo, sentado en zazen … 

91
00:06:37,260 --> 00:06:40,780
Alguien entra al dojo y dice: 

92
00:06:41,060 --> 00:06:45,840
"Tendremos que continuar sin Deshimaru". 

93
00:06:46,100 --> 00:06:48,900
Yo vivía en zazen. 

94
00:06:49,200 --> 00:06:50,560
Siempre estuve delante de él. 

95
00:06:50,900 --> 00:06:53,500
Siempre fui un pilar para él. 

96
00:06:53,940 --> 00:06:57,120
Así fue como experimenté su muerte. 

97
00:06:57,420 --> 00:07:02,200
Tenía que mantenerme fuerte para los demás. 

98
00:07:02,840 --> 00:07:11,040
Luego fui directamente a Japón para la ceremonia de cremación. 

99
00:07:13,100 --> 00:07:23,920
Hubo una ceremonia con muchos maestros, con toda la iglesia japonesa. Enorme 

100
00:07:24,660 --> 00:07:31,060
Y estaba en una caja, con muchas flores. 

101
00:07:31,500 --> 00:07:37,790
Su cara muy hermosa, muy sonriente. 

102
00:07:38,100 --> 00:07:50,620
Cuando estaba vivo, cada vez que se ponía un sombrero o una nariz falsa, se volvía muy cómico. 

103
00:07:50,940 --> 00:07:54,380
Era lo mismo allí, con las flores que lo rodeaban. 

104
00:07:54,720 --> 00:07:59,520
Hicimos la ceremonia, permanecí muy concentrado e impasible. 

105
00:07:59,820 --> 00:08:05,400
Luego ponemos la caja en un auto. 

106
00:08:05,880 --> 00:08:09,720
A cuatro discípulos. 

107
00:08:09,840 --> 00:08:20,280
Fui el último y empujé la caja en un carrito. 

108
00:08:20,600 --> 00:08:27,160
En el último momento aprieto mi dedo con el cofre. 

109
00:08:29,180 --> 00:08:37,700
Empecé a llorar y no pude parar por dos o tres horas. 

110
00:08:38,020 --> 00:08:42,220
Salió y lloré y lloré. 

111
00:08:42,520 --> 00:08:45,560
Todos me miraron porque nadie lloraba así. 

112
00:08:46,040 --> 00:08:51,440
Lloré sin cesar frente a la familia y los demás participantes. 

113
00:08:52,280 --> 00:08:57,500
No pude evitarlo. 

114
00:09:00,560 --> 00:09:05,720
Hasta que vi salir el humo de la chimenea. 

115
00:09:19,379 --> 00:09:27,799
Muerto - vivo, todo está vivo, todo está muerto. 

116
00:09:28,840 --> 00:09:33,500
Todavía estamos vivos, pero estamos muertos. 

117
00:09:33,800 --> 00:09:42,220
Cuando mueres, ves pasar tu vida. 

118
00:09:42,540 --> 00:09:48,580
Hemos estado mirando nuestras vidas enteras ahora. 

119
00:10:10,010 --> 00:10:13,620
Zazen es el estado normal. 

