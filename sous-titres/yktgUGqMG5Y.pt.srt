1
00:00:23,160 --> 00:00:26,820
Arti, você está aí? 

2
00:00:26,820 --> 00:00:28,560
Eu estou bem aqui. 

3
00:00:28,560 --> 00:00:32,940
É bom ver você, como tem passado? 

4
00:00:32,940 --> 00:00:34,240
Estou bem… 

5
00:00:34,240 --> 00:00:38,620
Você sabe que parece engraçado, o que está acontecendo com você ultimamente? 

6
00:00:38,740 --> 00:00:41,620
Estou cansado, tenho festa com os vizinhos. 

7
00:00:41,620 --> 00:00:46,184
O que, mas é um bloqueio, você está brincando? Você não tem o direito de sair. 

8
00:00:46,184 --> 00:00:50,520
Não tenho permissão, mas não saí, está na varanda. 

9
00:00:50,520 --> 00:00:56,280
Você me faz sentir melhor. Cuidado, logo atrás, volte. 

10
00:00:56,440 --> 00:00:58,360
Oh, cara, você está sofrendo. 

11
00:00:58,360 --> 00:01:01,980
Toda noite é um aperitivo. 

12
00:01:01,980 --> 00:01:09,000
E ontem caí na casa do vizinho, encontrei-me de cueca, um andar abaixo, na lata de gerânio. 

13
00:01:09,000 --> 00:01:12,180
Você deve ter cuidado Arti de qualquer maneira. 

14
00:01:12,180 --> 00:01:14,140
Como vai você? 

15
00:01:14,140 --> 00:01:21,220
Está tudo bem, você sabe que eu sou uma batata, não é à toa que eles me chamam de patata. 

16
00:01:21,400 --> 00:01:24,840
Oh sim, certo, o que você faz no seu dia? 

17
00:01:24,840 --> 00:01:29,957
Vou começar com um pouco de meditação. 

18
00:01:29,960 --> 00:01:31,340
Conte-me… 

19
00:01:31,340 --> 00:01:40,920
Não há nada a dizer, eu sento assim, olha … eu me concentro em respirar. 

20
00:01:40,980 --> 00:01:44,140
Talvez eu deva fazer isso … 

21
00:01:44,140 --> 00:01:49,340
Então me sinto melhor, com todo mundo aqui … Estamos cheios. 

22
00:01:49,340 --> 00:01:53,420
Estamos cheios? Mas você não está autorizado, é … bloqueio! 

23
00:01:53,420 --> 00:01:55,980
Não, mas você não entende, nós fazemos isso com o zoom. 

24
00:01:55,980 --> 00:01:57,460
Com o zoom? 

25
00:01:57,460 --> 00:02:03,200
Sim, você se conecta e fica com muitas pessoas … 

26
00:02:03,200 --> 00:02:05,820
Estou interessado em sua coisa. 

27
00:02:05,820 --> 00:02:10,452
Amanhã sábado, praticamos com o Mestre Kosen. 

28
00:02:10,460 --> 00:02:13,320
Estou interessado. Quem é ele? 

29
00:02:13,320 --> 00:02:17,340
Bem, olhe, ele não está mal. Amanhã ao meio dia. 

30
00:02:17,500 --> 00:02:20,740
Tudo bem, eu estarei lá. Você vai me explicar? 

31
00:02:20,740 --> 00:02:25,580
Sim, vamos lá, eu vou explicar agora. 

32
00:02:25,740 --> 00:02:28,040
Vadio! Vadio? 

33
00:02:28,040 --> 00:02:29,760
O que há Arti? 

34
00:02:29,760 --> 00:02:33,360
Estou ficando um pouco quente na panela. 

35
00:02:33,360 --> 00:02:39,260
É bom para o que você tem Arti, mata o vírus, eu garanto. 

36
00:02:39,260 --> 00:02:43,560
Sim, estou começando a esquentar. 

37
00:02:43,560 --> 00:02:45,280
Não se preocupe… 

