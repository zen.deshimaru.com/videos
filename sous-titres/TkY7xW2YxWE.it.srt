1
00:00:00,200 --> 00:00:03,460
A volte puoi trovarlo solo nello Zen, 

2
00:00:03,780 --> 00:00:06,680
quando stiamo insieme per un po ’, 

3
00:00:06,920 --> 00:00:08,960
che alla gente manca la compassione. 

4
00:00:09,180 --> 00:00:11,600
Penso che a volte devi cambiare il modo in cui guardi le cose. 

5
00:00:11,980 --> 00:00:16,400
Invece di pensare: "Ehi, è l’altro che mi ha derubato!" 

6
00:00:17,000 --> 00:00:20,680
pensando "che diavolo dovrei fare con questo?" 

7
00:00:21,180 --> 00:00:23,160
E prova a capovolgerlo. 

8
00:00:24,360 --> 00:00:27,900
È molto sincero. 

9
00:00:28,300 --> 00:00:30,340
Perché le persone sono se stesse. 

10
00:00:30,600 --> 00:00:32,560
Cercano di non svolgere un ruolo: 

11
00:00:32,760 --> 00:00:36,900
"Sto bene, sto bene, sto bene …" 

12
00:00:38,240 --> 00:00:41,760
Ci sono certamente momenti in cui ci arrabbiamo. 

13
00:00:42,040 --> 00:00:45,280
E dove spiccano le nostre reazioni. 

14
00:00:45,600 --> 00:00:50,780
Per me è importante non stabilirci lì. 

15
00:00:51,040 --> 00:00:52,880
Per non crogiolarsi nell’idea: 

16
00:00:53,200 --> 00:00:55,700
"In una sesshin non ci sono persone simpatiche." 

17
00:00:55,960 --> 00:00:59,320
Quando arrivo a Sesshin, 

18
00:00:59,880 --> 00:01:04,060
la mia idea è di trasformare tutto in positivo. 

19
00:01:04,600 --> 00:01:07,340
Non so se posso farlo tutto il tempo! 

20
00:01:08,360 --> 00:01:10,960
Per me, tutte le difficoltà che si presentano 

21
00:01:11,180 --> 00:01:16,960
i suoi inviti a continuare nel suo stato d’animo. 

22
00:01:17,360 --> 00:01:24,680
Direi anche che se veniamo ingiustamente criticati, 

23
00:01:24,980 --> 00:01:27,600
possiamo ricavarne qualcosa di positivo. 

24
00:01:27,900 --> 00:01:33,580
Ad esempio, ti costringe a prenderti meno sul serio. 

25
00:01:37,620 --> 00:01:40,340
Quello che mi piace di questo sangha, 

26
00:01:40,640 --> 00:01:42,860
è che proviamo ad ascoltarci un po ’di più. 

27
00:01:43,140 --> 00:01:45,720
Perché ci vuole tempo per ascoltarci di più, 

28
00:01:46,040 --> 00:01:50,460
per tenere conto di alcuni altri punti di vista. 

29
00:01:50,760 --> 00:01:53,580
Sappiamo che le cose sono sbagliate, 

30
00:01:53,980 --> 00:01:58,120
ma cerchiamo sempre di migliorare. 

31
00:01:59,400 --> 00:02:07,120
Se ti è piaciuto questo video, sentiti libero di apprezzarlo e iscriviti al canale! 

