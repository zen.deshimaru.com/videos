1
00:01:31,170 --> 00:01:35,220
Nel Soto Zen, che pratichiamo, diciamo: "Non cercare una grande illuminazione". 

2
00:01:35,560 --> 00:01:38,407
L’illuminazione, di per sé, non esiste. 

3
00:01:38,407 --> 00:01:40,170
Perché dovrebbe esistere? 

4
00:01:40,780 --> 00:01:45,720
Devi guardare la natura e le cose come sono per vedere la verità. 

5
00:01:46,200 --> 00:01:49,663
L’illuminazione è assolutamente naturale. 

6
00:01:49,663 --> 00:01:52,780
Non è una condizione speciale. 

7
00:01:53,320 --> 00:01:59,400
Nello Zen, non c’è una condizione speciale nell’illuminazione se non c’è una malattia della mente. 

8
00:01:59,700 --> 00:02:04,760
Di per sé non c’è illuminazione speciale e non si dovrebbe cercarne una. 

9
00:02:05,540 --> 00:02:07,433
Molti maestri insistono su questo. 

10
00:02:07,433 --> 00:02:08,681
Questo è estremamente importante. 

11
00:02:08,681 --> 00:02:11,000
L’illuminazione è in ogni momento, in ogni zazen. 

12
00:02:11,320 --> 00:02:15,580
Stavo parlando con uno specialista in droghe psicotrope … 

13
00:02:15,790 --> 00:02:20,700
progettato per raggiungere stati speciali di coscienza. 

14
00:02:21,130 --> 00:02:24,686
Sosteneva che lo zazen fosse un mezzo per raggiungere l’illuminazione. 

15
00:02:24,686 --> 00:02:25,889
Questo non è affatto vero. 

16
00:02:26,350 --> 00:02:31,379
Zazen è l’illuminazione stessa. 

17
00:02:32,410 --> 00:02:33,254
Dogen ha detto: 

18
00:02:33,254 --> 00:02:36,269
"Sono andato in Cina e ho incontrato un vero maestro." 

19
00:02:37,390 --> 00:02:41,729
"Quello che ho imparato da esso è che gli occhi sono orizzontali e il naso è verticale." 

20
00:02:42,370 --> 00:02:45,810
"E quello non dovrebbe essere ingannato, né da soli né da altri." 

21
00:03:44,750 --> 00:03:46,750
e ogni muscolo nella pancia 

22
00:03:47,269 --> 00:03:52,358
dovrebbe essere rilassato per dare l’impressione che gli intestini stiano pesando. 

23
00:04:09,010 --> 00:04:15,420
se ci concentriamo completamente sulla postura in quel momento, possiamo lasciare che i pensieri 

24
00:04:15,850 --> 00:04:17,850
senza fermarsi lì. 

25
00:04:25,810 --> 00:04:28,914
La cosa più bella che puoi fare con il tuo corpo è lo zazen. 

26
00:04:28,914 --> 00:04:30,419
Questo è lo splendore della natura. 

27
00:04:30,849 --> 00:04:33,728
Ispira calma, armonia, forza. 

28
00:04:33,728 --> 00:04:36,539
Tutto è contenuto nella postura dello zazen 

29
00:04:39,969 --> 00:04:43,447
Al campo estivo cerco di incoraggiare la pratica. 

30
00:04:43,447 --> 00:04:46,111
L’atmosfera nel dojo, le posture. 

31
00:04:46,111 --> 00:04:47,909
Una persona che lo disturba 

32
00:04:50,540 --> 00:04:53,940
con la sua storia personale, sono disposto a ucciderla. 

33
00:04:57,120 --> 00:04:59,120
È l’unica regola che conosco: compassione. 

34
00:05:00,600 --> 00:05:02,477
Sensei stava mostrando il suo kyosaku: 

35
00:05:02,477 --> 00:05:04,160
"lo staff della compassione!" 

36
00:05:04,660 --> 00:05:11,080
A volte colpiva 20 colpi di fila. 

37
00:05:14,440 --> 00:05:19,640
Diceva: "Sensei, molto compassionevole!" 

38
00:05:26,280 --> 00:05:32,940
Quando un discepolo vuole essere aiutato nella sua meditazione venendo colpito alla spalla con un bastone, lo chiede. 

39
00:05:33,180 --> 00:05:36,800
Perché sente che la sua attenzione inizia a calare. 

40
00:05:37,140 --> 00:05:41,360
Quando ti senti assonnato. 

41
00:05:41,580 --> 00:05:46,300
O quando senti complicazioni annebbiare il cervello. 

42
00:05:46,540 --> 00:05:49,520
Non più così silenzioso. 

43
00:05:49,940 --> 00:05:54,600
Se ti senti assonnato o se la tua mente è irrequieta … 

44
00:06:01,240 --> 00:06:03,240
Gassho! 

45
00:06:21,580 --> 00:06:23,580
Zazen continua durante 

46
00:06:23,840 --> 00:06:25,233
per diverse ore ogni giorno. 

47
00:06:25,233 --> 00:06:25,929
E a volte, giorno e notte. 

48
00:06:26,180 --> 00:06:30,291
Ma viene interrotto da una meditazione mentre cammina. 

49
00:06:30,291 --> 00:06:33,489
Cioè, lo stato interiore rimane, 

50
00:06:34,280 --> 00:06:38,799
ma la postura è diversa. 

51
00:06:39,940 --> 00:06:44,040
Tutta l’attenzione è focalizzata sul pollice. 

52
00:06:44,600 --> 00:06:47,500
Ogni respiro corrisponde a mezzo passo. 

53
00:06:58,880 --> 00:07:05,020
Gli occhi sono a terra 3 metri di fronte. 

54
00:07:13,340 --> 00:07:18,580
È lo stesso respiro che nello zazen, durante la meditazione in postura seduta. 

55
00:07:20,740 --> 00:07:28,300
La respirazione Zen è l’opposto della respirazione normale. 

56
00:07:28,560 --> 00:07:30,940
Quando chiedi a qualcuno di respirare, 

57
00:07:31,240 --> 00:07:34,600
il riflesso è fare un respiro profondo. 

58
00:07:35,060 --> 00:07:38,860
In zen, ci concentreremo 

59
00:07:39,080 --> 00:07:42,740
soprattutto sull’espirazione. 

60
00:07:43,040 --> 00:07:46,780
L’espirazione è lunga, calma e profonda. 

61
00:07:54,360 --> 00:07:56,160
È ancorato nel terreno. 

62
00:07:56,160 --> 00:07:58,860
Può durare fino a uno, due minuti. 

63
00:07:59,080 --> 00:08:02,940
A seconda del suo stato di concentrazione e calma. 

64
00:08:03,280 --> 00:08:09,600
Può essere molto morbido, impercettibile. 

65
00:08:09,860 --> 00:08:15,640
Per prima cosa studiamo questo respiro insistendo, come un muggito di mucca. 

66
00:08:38,020 --> 00:08:44,640
Alla fine dell’espirazione, lasciamo che l’ispirazione abbia luogo automaticamente. 

67
00:08:45,880 --> 00:08:49,640
Rilassiamo tutto, apriamo i polmoni. 

68
00:08:50,300 --> 00:08:55,380
Lasciamo che l’ispirazione arrivi automaticamente. 

69
00:08:55,820 --> 00:08:58,689
Non diamo molta intenzione all’ispirazione. 

70
00:08:59,600 --> 00:09:03,129
In questo modo, capirai 

71
00:09:04,550 --> 00:09:06,550
che respirare è tutto. 

72
00:09:07,910 --> 00:09:09,910
È un cerchio. 

73
00:09:10,070 --> 00:09:12,070
Spesso i maestri disegnano un cerchio. 

74
00:09:12,560 --> 00:09:14,560
Si tratta di respirare. 

75
00:09:15,040 --> 00:09:18,880
Quella respirazione ha avuto molta influenza, 

76
00:09:19,340 --> 00:09:23,200
soprattutto nelle arti marziali. 

77
00:09:23,480 --> 00:09:29,160
Nelle arti marziali giapponesi viene utilizzata la respirazione Zen. 

78
00:09:29,580 --> 00:09:34,840
Molti grandi maestri di arti marziali come Miyamoto Musashi 

79
00:09:35,180 --> 00:09:39,640
praticò lo Zen e divenne discepolo di grandi maestri. 

80
00:09:39,940 --> 00:09:45,480
E hanno adattato l’insegnamento dello Zen e la respirazione alla loro arte marziale. 

81
00:09:45,820 --> 00:09:53,860
Alla fine, la respirazione va oltre le tecniche ed è totalmente libera. 

82
00:09:58,080 --> 00:10:03,340
L’insegnamento del Buddha ai suoi discepoli è molto semplice. 

83
00:10:03,640 --> 00:10:09,440
Si basa sugli atti fondamentali dell’essere umano. 

84
00:10:09,660 --> 00:10:11,260
Come muoversi 

85
00:10:11,480 --> 00:10:14,120
Come mangiare, come respirare. 

86
00:10:14,440 --> 00:10:15,836
Come pensare 

87
00:10:15,836 --> 00:10:20,860
E infine, come sedersi per realizzare la sua divinità. 

88
00:11:24,940 --> 00:11:28,702
Genmai è stato il cibo dei monaci dal Buddha Shakyamuni. 

89
00:11:28,702 --> 00:11:31,800
Mescoliamo le verdure che abbiamo a portata di mano con il riso. 

90
00:11:32,280 --> 00:11:39,440
Cuciniamo da molto tempo, con molta attenzione e concentrazione. 

91
00:11:39,780 --> 00:11:42,730
È un cibo tramandato dal Buddha. 

92
00:11:42,730 --> 00:11:45,920
In Cina e in Giappone, mangiamo lo stesso cibo. 

93
00:11:47,660 --> 00:11:49,980
È davvero il corpo del Buddha. 

94
00:11:50,280 --> 00:11:53,080
Quando stiamo per mangiare, 

95
00:11:53,440 --> 00:11:55,560
devi farti la domanda: 

96
00:11:55,900 --> 00:11:59,460
"Perché vado a mangiare?" 

97
00:11:59,680 --> 00:12:01,580
Un animale o un zoticone 

98
00:12:01,960 --> 00:12:05,680
non si starà chiedendo e saltando su e giù nel suo piatto. 

99
00:12:06,040 --> 00:12:09,820
Ovviamente è per la sopravvivenza, ma la sopravvivenza per cosa? 

100
00:12:10,060 --> 00:12:13,520
Questo è ciò che dice nei sutra che vengono cantati durante i pasti. 

101
00:12:13,780 --> 00:12:15,240
"Perché mangiamo?" 

102
00:12:15,520 --> 00:12:17,591
"Da dove viene questo cibo? 

103
00:12:17,800 --> 00:12:18,860
Chi l’ha preparato? " 

104
00:12:19,180 --> 00:12:21,140
"Chi ha coltivato il riso e le verdure?" 

105
00:12:21,400 --> 00:12:26,140
"Chi ha cucinato?" 

106
00:12:26,500 --> 00:12:29,960
Dedichiamo il pasto e ringraziamo tutte queste persone. 

107
00:12:30,240 --> 00:12:32,140
E in piena coscienza, 

108
00:12:32,620 --> 00:12:37,340
Decido di mangiare questo cibo per 

109
00:12:37,780 --> 00:12:44,620
l’evoluzione che sono arrivato a compiere su questa terra, in questo mondo materiale. 

110
00:12:52,360 --> 00:12:56,900
EMS è alle 10:30. 6 persone per la cucina. 

111
00:13:01,080 --> 00:13:04,080
Due persone per la grande immersione. 

112
00:13:05,940 --> 00:13:10,340
Tre persone per il servizio. 

113
00:13:19,600 --> 00:13:23,580
Samu non sta particolarmente facendo muratura, spazzando su … 

114
00:13:24,190 --> 00:13:27,989
o lavare i piatti. 

115
00:13:28,540 --> 00:13:30,540
Samu è tutta azione 

116
00:13:30,760 --> 00:13:32,939
(Scrivere un libro è un samu) 

117
00:13:35,320 --> 00:13:37,320
effettuato senza 

118
00:13:37,640 --> 00:13:38,860
Intenzione 

119
00:13:39,180 --> 00:13:40,160
nascosto 

120
00:13:44,950 --> 00:13:47,249
nessun motivo segreto. 

121
00:13:47,980 --> 00:13:50,048
È come il tuo lavoro nella vita di tutti i giorni. 

122
00:13:50,048 --> 00:13:52,289
Puoi prenderlo come arricchimento personale 

123
00:13:52,839 --> 00:13:57,749
o come una specie di dono che fai a te stesso e a tutti gli altri allo stesso tempo. 

124
00:13:59,320 --> 00:14:02,460
A volte lo considero 

125
00:14:03,400 --> 00:14:05,459
Alcune persone possono prenderlo come un lavoro ingrato. 

126
00:14:05,920 --> 00:14:09,390
Ma scommetto che quelli feriti nello zazen. 

127
00:14:16,400 --> 00:14:19,550
Come suora Zen, 

128
00:14:23,040 --> 00:14:26,180
che atteggiamento dovresti assumere al lavoro? 

129
00:14:27,080 --> 00:14:30,100
Non lo so, non ho mai lavorato! 

130
00:14:33,960 --> 00:14:37,060
Dobbiamo farne un samu? 

131
00:14:38,500 --> 00:14:39,520
Sì, questo è il punto. 

132
00:14:39,740 --> 00:14:41,100
Stavo parlando di merda, eh. 

133
00:14:41,300 --> 00:14:47,500
Quando ero più giovane avevo un lavoro come bidello. 

134
00:14:48,090 --> 00:14:54,920
Ho pulito le scale e portato fuori la spazzatura ogni mattina alle 5:00. 

135
00:14:55,140 --> 00:15:00,619
Le scale, è stata una evviva. C’era merda di cane. 

136
00:15:00,920 --> 00:15:05,360
Ho pensato: "Ma questo non è il dojo o lo Zen". 

137
00:15:05,760 --> 00:15:09,760
"Non importa. Per me, è come un tempio." 

138
00:15:10,320 --> 00:15:14,160
L’ho fatto come un samurai. 

139
00:15:14,480 --> 00:15:17,040
E Sensei mi ha chiesto: 

140
00:15:17,420 --> 00:15:20,080
"Stéphane, vorrei che tu pulissi il dojo." 

141
00:15:20,400 --> 00:15:23,660
"Sì, lo farò. Venerdì sono libero." 

142
00:15:25,440 --> 00:15:27,920
"Lo farò venerdì prossimo." 

143
00:15:28,580 --> 00:15:29,660
Lui mi disse: 

144
00:15:29,940 --> 00:15:32,500
"No, voglio che lo faccia ogni giorno." 

145
00:15:33,120 --> 00:15:34,272
Poi, lui ha detto: 

146
00:15:34,520 --> 00:15:37,440
"Un giorno senza lavoro, un giorno senza cibo." 

147
00:15:37,860 --> 00:15:40,720
"Ma non ha nulla a che fare con il cibo!" 

148
00:15:41,000 --> 00:15:44,440
"Un giorno senza lavorare, un giorno senza mangiare mushotoku!" 

149
00:15:44,780 --> 00:15:47,380
Era un’espressione molto originale: 

150
00:15:47,700 --> 00:15:50,380
"Devi mangiare mushotoku!" 

151
00:15:54,760 --> 00:15:57,540
Non significava niente! 

152
00:15:58,180 --> 00:16:02,500
Dire che sei un monaco, praticare il samu è una rivoluzione. 

153
00:16:02,880 --> 00:16:05,880
Dobbiamo fare la rivoluzione, l’ho sempre detto! 

154
00:16:06,920 --> 00:16:10,657
Ma non so come usare i cocktail Molotov! 

155
00:16:11,000 --> 00:16:14,100
Quindi, nel suo lavoro, devi praticare il samu. 

156
00:16:14,400 --> 00:16:19,780
Per risolvere tutti i problemi del mondo, tutto ciò che devi fare è trovare questo. 

157
00:16:20,100 --> 00:16:22,060
Le persone non hanno più lo spirito del samu. 

158
00:16:22,520 --> 00:16:23,920
Non mangiano più mushotoku. 

159
00:16:24,160 --> 00:16:25,580
Mangiano hamburger e bevono Coca-Cola. 

160
00:16:26,180 --> 00:16:28,880
Quindi, chiunque ti dica: 

161
00:16:29,580 --> 00:16:31,580
Ma sono le 6:30, sei l’unico rimasto! 

162
00:16:31,880 --> 00:16:33,140
Gli dici: 

163
00:16:33,360 --> 00:16:36,160
"Io pratico il samu!" 

164
00:16:45,640 --> 00:16:47,640
"Perché voglio mangiare mushotoku!" 

165
00:16:49,780 --> 00:16:51,200
"Che cos’è mushotoku?" 

166
00:16:51,440 --> 00:16:52,780
"Ne voglio anche io!" 

167
00:16:57,040 --> 00:17:01,200
Dobbiamo insegnare alle persone senza avere paura. 

168
00:17:03,180 --> 00:17:06,660
Sii orgoglioso di essere i miei discepoli e di essere monaci! 

169
00:17:07,000 --> 00:17:09,000
E di essere discepoli di Deshimaru. 

