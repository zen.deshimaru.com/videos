1
00:00:03,300 --> 00:00:07,060
Wie spät ist es? 

2
00:00:07,240 --> 00:00:11,780
Die Zeit ist erst jetzt. 

3
00:00:12,080 --> 00:00:15,080
Zen ist "hier und jetzt". 

4
00:00:15,340 --> 00:00:16,820
Es ist wichtig. 

5
00:00:17,220 --> 00:00:25,160
Das Verständnis der Vergangenheit ist jedoch ein außergewöhnlicher Trick, um die Gegenwart zu verstehen. 

6
00:00:25,580 --> 00:00:31,640
Das Verständnis der Zukunft ist ebenfalls sehr wichtig. 

7
00:00:32,460 --> 00:00:34,780
Wie gehst du durch die Zeit? 

8
00:00:35,360 --> 00:00:38,400
Zeit ist eine Frage der Frequenz. 

9
00:00:38,700 --> 00:00:43,640
Je höher die Frequenz, desto schneller die Zeit, das ist alles. 

10
00:00:44,100 --> 00:00:49,760
Das Bewusstsein entwickelt sich auch entsprechend den Frequenzen. 

11
00:00:59,700 --> 00:01:03,440
Teuer ist nur ein materieller Eindruck. 

12
00:01:03,820 --> 00:01:06,000
Es ist eine Freude. 

13
00:01:06,400 --> 00:01:09,100
Was ist mit Zazen? 

14
00:01:09,400 --> 00:01:16,700
Für einen Liebhaber von Zazen ist Zazen-Zeit sehr kostbar. 

15
00:01:17,120 --> 00:01:22,560
Wir kommen zum Dojo und haben nur anderthalb Stunden, bis zu zwei Stunden. 

16
00:01:23,020 --> 00:01:27,720
In dieser Dimension leben können. 

17
00:01:28,000 --> 00:01:31,520
Und setzen Sie die Dinge an ihre Stelle. 

18
00:01:31,780 --> 00:01:35,440
Sie müssen Ihre Zazen-Zeit sparen. 

19
00:01:35,540 --> 00:01:38,760
Es dauert eine Weile, bis man dem auf den Grund geht. 

20
00:01:39,000 --> 00:01:40,760
Und in guter Haltung zu sein. 

21
00:01:41,060 --> 00:01:43,960
Ist Zeit mit Karma verbunden? 

22
00:01:44,720 --> 00:01:46,920
Die Vergangenheit existiert erst jetzt. 

23
00:01:47,280 --> 00:01:57,240
Jedes "Jetzt" entspricht einer bestimmten Frequenz der Vergangenheitsform mit Ihrem "Jetzt". 

24
00:01:57,620 --> 00:02:00,900
Ihre Vergangenheit ändert sich ständig. 

25
00:02:01,000 --> 00:02:10,700
Aber wenn du in deine Vergangenheit gehst, vergisst du deine Gegenwart. 

26
00:02:11,040 --> 00:02:13,560
Und deine Gegenwart wird zur Vergangenheit. 

27
00:02:13,900 --> 00:02:17,720
Und Sie gehen auf etwas ein, das immer komplizierter wird. 

28
00:02:17,940 --> 00:02:19,340
Im Karma. 

29
00:02:19,820 --> 00:02:24,160
"Es ist die Schuld meines Karmas …" 

30
00:02:25,240 --> 00:02:27,960
"Meine Eltern, meine Sachen …" 

31
00:02:28,280 --> 00:02:30,440
"Mein Leben ist kompliziert …" 

32
00:02:30,700 --> 00:02:32,380
"Meine Vorfahren waren Alkoholiker …" 

33
00:02:32,660 --> 00:02:35,420
Usw. Und du verlierst dich dort. 

34
00:02:35,780 --> 00:02:40,060
Sie identifizieren sich mit diesem Charakter. 

35
00:02:40,340 --> 00:02:41,720
Du wirst schwächer. 

36
00:02:42,060 --> 00:02:44,140
Du bist nicht länger Gott. 

37
00:02:44,420 --> 00:02:45,720
Du bist verwirrt. 

38
00:02:46,080 --> 00:02:47,760
Sie sind in der Materialisierung verloren. 

39
00:02:48,060 --> 00:02:53,300
Nach unserem derzeitigen Stand gibt es in der Vergangenheit immer Ursachen. 

40
00:02:53,680 --> 00:02:58,540
Wenn wir die aktuelle Situation ändern, wird sich auch die Vergangenheit ändern. 

41
00:02:59,160 --> 00:03:01,940
Was ist mit der Zukunft? 

42
00:03:02,340 --> 00:03:05,300
Die Zukunft ist eine andere Geschichte … 

