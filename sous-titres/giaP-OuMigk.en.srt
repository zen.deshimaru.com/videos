1
00:00:00,080 --> 00:00:00,920
Question: 

2
00:00:01,140 --> 00:00:05,380
I sometimes feel in zazen meditation a very pleasant sensation. 

3
00:00:05,680 --> 00:00:09,880
A little tingling all over the body. 

4
00:00:10,340 --> 00:00:15,040
Like small electric shocks. 

5
00:00:15,440 --> 00:00:17,880
Response from Master Kosen: 

6
00:00:18,400 --> 00:00:22,420
All feelings can be good or bad. 

7
00:00:23,080 --> 00:00:28,760
The same feeling may seem good or bad. 

8
00:00:29,220 --> 00:00:33,520
The important thing is that it is pleasant. 

9
00:00:34,180 --> 00:00:37,240
May it relax you. 

10
00:00:37,500 --> 00:00:42,000
It is usually related to the circulation of blood. 

11
00:00:42,340 --> 00:00:50,640
In the lotus pose, there is little blood circulation in the legs. 

12
00:00:51,020 --> 00:00:55,960
It circulates much more in the abdominal part. 

13
00:00:56,400 --> 00:01:00,840
The upper body, including the brain, is better irrigated. 

14
00:01:01,100 --> 00:01:05,240
It can cause a pleasant feeling. 

15
00:01:05,500 --> 00:01:09,660
It’s good, because it’s a positive thought. 

16
00:01:10,060 --> 00:01:13,580
When you feel good, it is a positive consciousness. 

17
00:01:13,820 --> 00:01:16,240
It also influences our lives. 

18
00:01:16,480 --> 00:01:18,160
Master Deshimaru always said: 

19
00:01:18,380 --> 00:01:20,500
“Zazen is not a mortification. " 

20
00:01:20,840 --> 00:01:26,260
But when you have to face your body, 

21
00:01:26,620 --> 00:01:31,480
some times are unpleasant or painful. 

22
00:01:31,860 --> 00:01:34,940
When you feel good it’s great! 

