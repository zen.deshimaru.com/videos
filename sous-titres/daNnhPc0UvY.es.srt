1
00:00:05,860 --> 00:00:08,580
Deshimaru, sé que voy a morir pronto. 

2
00:00:08,820 --> 00:00:17,320
Ha llegado el momento de ordenarte como monje para transmitir la enseñanza de Bodhidarma en una nueva tierra. 

3
00:00:31,020 --> 00:00:32,820
Quiero ayudar a Dios 

4
00:00:33,100 --> 00:00:34,860
Quiero ayudar a Cristo 

5
00:00:43,260 --> 00:00:48,000
Quiero ayudar al verdadero dios. 

6
00:00:48,240 --> 00:00:52,760
Quiero volver al dios verdadero. 

7
00:00:53,360 --> 00:01:00,360
40 años desde la llegada del Maestro Deshimaru a Europa. 

8
00:01:00,800 --> 00:01:04,000
Dios está abajo 

9
00:01:04,320 --> 00:01:09,040
Dios está "fatigado", cansado. 

10
00:01:15,680 --> 00:01:22,680
Mokudo Taisen Deshimaru. El Bodhidarma de los tiempos modernos. 

11
00:01:28,060 --> 00:01:30,900
Taisen Deshimaru llegó a París en el verano de 1967 a la estación de tren Gare du Nord. 

12
00:01:31,120 --> 00:01:33,480
Solo un monje, sin dinero, que no habla francés. 

13
00:01:33,820 --> 00:01:36,600
En su equipaje, solo la semilla fresca del zen. 

14
00:01:45,440 --> 00:01:49,600
La llegada 

15
00:02:59,140 --> 00:03:03,060
Si podemos volver a la historia del Zen … 

16
00:03:04,380 --> 00:03:09,520
Todo comenzó en la India y luego: ¿qué pasó? 

17
00:03:09,720 --> 00:03:13,340
¿Cómo fue de India a Japón? 

18
00:03:13,640 --> 00:03:19,020
India, meditación muy profunda. 

19
00:03:19,480 --> 00:03:22,340
Pero muy tradicional. 

20
00:03:24,380 --> 00:03:29,980
El yoga no está terminado en la India. 

21
00:03:30,720 --> 00:03:36,160
Algunas personas tienen un alto nivel espiritual, pero la mayoría no. 

22
00:03:36,760 --> 00:03:43,440
Pero, yoga, terminado. no terminamos 

23
00:03:43,760 --> 00:03:48,240
Pero la tierra es muy vieja. 

24
00:03:48,940 --> 00:03:53,660
Bodhidarma trajo zazen de la India a China. 

25
00:03:53,900 --> 00:03:58,280
Pero ya no hay zen en la India. 

26
00:03:58,860 --> 00:04:04,880
Entonces Dogen trajo zazen de China a Japón. 

27
00:04:05,160 --> 00:04:11,360
¿Quieres decir que el suelo es demasiado viejo en Asia, Japón, China o India? 

28
00:04:11,740 --> 00:04:14,740
Sí, India es muy vieja. 

29
00:04:14,960 --> 00:04:17,700
China también es muy vieja. 

30
00:04:18,280 --> 00:04:20,280
El zen está terminado en China. 

31
00:04:20,700 --> 00:04:23,940
Japón está demasiado acostumbrado al zen. 

32
00:04:24,680 --> 00:04:27,240
Solo se preocupan por las ceremonias … 

33
00:04:27,680 --> 00:04:30,380
Extrañan la esencia del zen. 

34
00:04:36,160 --> 00:04:48,320
¿Crees que el espíritu del zen tiende a declinar? 

35
00:04:48,640 --> 00:04:50,620
¡Si si si! 

36
00:04:51,040 --> 00:04:58,720
¿Crees que el espíritu del zen puede atrapar en Francia, como la semilla de una flor? 

37
00:04:59,040 --> 00:05:01,120
¡Si! 

38
00:05:03,020 --> 00:05:06,980
La enseñanza comienza. 

39
00:06:24,680 --> 00:06:30,080
¿Por qué elegiste establecerte en Francia? 

40
00:06:30,400 --> 00:06:37,000
Francia es mejor que otro país para entender el zen. 

41
00:06:37,520 --> 00:06:44,300
Muy fácil de entender zazen. 

42
00:06:45,120 --> 00:06:46,700
O filosofia. 

43
00:06:46,980 --> 00:06:48,980
Para ideas 

44
00:06:49,300 --> 00:06:52,060
Muy profundo. 

45
00:06:52,520 --> 00:06:57,980
Por ejemplo, Montaigne: 

46
00:06:58,420 --> 00:07:03,900
"No me importa tanto lo que soy para los demás como lo que soy para mí". 

47
00:07:05,880 --> 00:07:09,640
Descartes, Bergson … 

48
00:07:10,140 --> 00:07:14,080
Portadores de la mente, filósofos … 

49
00:07:14,800 --> 00:07:16,840
Pueden entender zen. 

50
00:07:18,200 --> 00:07:25,740
No conocen el zen, pero su pensamiento es el mismo … 

51
00:07:26,460 --> 00:07:29,760
¿Por qué no fuiste a América? 

52
00:07:30,280 --> 00:07:33,840
Primero quería traer el Zen a los Estados Unidos. 

53
00:07:34,220 --> 00:07:41,180
Pero quieren usar el zen para los negocios, la industria, la psicología, el psicoanálisis … 

54
00:07:41,540 --> 00:07:44,180
Quieren usarlo. 

55
00:07:44,340 --> 00:07:49,020
No es cierto zen. No profundo. 

56
00:07:52,500 --> 00:07:56,160
Aparece una sangha. 

57
00:09:08,270 --> 00:09:14,100
Antes, buena educación, pero en los tiempos modernos, solo promedio. 

58
00:09:14,100 --> 00:09:23,740
La esencia está terminada. 

59
00:09:24,560 --> 00:09:33,700
Solo buscamos el promedio … 

60
00:09:34,380 --> 00:09:41,600
Solo la esencia es necesaria para una educación sólida. 

61
00:09:43,380 --> 00:09:49,960
Siempre a la derecha o a la izquierda … 

62
00:09:51,040 --> 00:09:54,260
Pero debemos aceptar las contradicciones. 

63
00:09:54,600 --> 00:09:59,720
Solo al abandonar nuestro ego podemos encontrar nuestra verdadera individualidad. 

64
00:10:03,920 --> 00:10:13,100
Nuestra personalidad más alta cuando seguimos la verdad cósmica, el cosmos. 

65
00:10:13,500 --> 00:10:19,480
Podemos encontrar nuestro verdadero y fuerte ego. 

66
00:10:20,580 --> 00:10:25,680
¿Quieres hacer que la gente sea fuerte? 

67
00:10:26,540 --> 00:10:32,060
Sí, equilibrado y fuerte, poco a poco. 

68
00:10:32,800 --> 00:10:41,600
Debemos encontrar la verdadera austeridad. 

69
00:10:43,360 --> 00:10:47,600
Enseñando 

70
00:12:46,000 --> 00:12:57,680
La esencia del Zen es "Mushotoku", sin propósito. 

71
00:12:59,200 --> 00:13:05,520
Todas las religiones tienen un objeto. 

72
00:13:06,400 --> 00:13:10,560
Rezas para hacerte rico o feliz. 

73
00:13:11,440 --> 00:13:17,040
Pero si tenemos un propósito, no es fuerte. 

74
00:13:18,400 --> 00:13:30,000
No debemos tener ningún propósito. 

75
00:13:31,680 --> 00:13:38,000
No puedes concentrarte. 

76
00:13:39,360 --> 00:13:47,680
¿Quiere decir que la realización adecuada debería ser espontánea? 

77
00:13:48,480 --> 00:13:56,080
Debemos volver a las condiciones normales. 

78
00:13:57,040 --> 00:14:01,440
Verdadero Satori, conciencia trascendental … 

79
00:14:02,560 --> 00:14:07,520
es volver a las condiciones normales. 

80
00:14:09,440 --> 00:14:18,080
Las personas son anormales en los tiempos modernos. 

81
00:14:19,120 --> 00:14:27,120
Solo el dios verdadero, por ejemplo, Cristo o Buda, tenía condiciones normales para los humanos. 

82
00:14:27,920 --> 00:14:33,900
¿La primera condición necesaria es la propia determinación? 

83
00:14:34,340 --> 00:14:39,500
¿Y el segundo, abandonar esta determinación? 

84
00:14:39,760 --> 00:14:45,940
Si. La individualidad es necesaria y debemos abandonarla. 

85
00:14:46,320 --> 00:14:50,180
Debemos aceptar las contradicciones. 

86
00:14:50,920 --> 00:14:56,560
Parece que fumas mucho … 

87
00:14:57,540 --> 00:15:03,260
¡No mucho! 

88
00:15:04,220 --> 00:15:10,620
¿Zen te ha hecho lo suficientemente fuerte como para aceptar algo? 

89
00:15:11,140 --> 00:15:15,360
¡Todo es posible! 

90
00:15:15,920 --> 00:15:20,240
No es tan importante preocuparse por la vida. 

91
00:15:20,540 --> 00:15:25,960
Los europeos tienen demasiado egoísmo. 

92
00:15:26,400 --> 00:15:31,840
¡Debes aceptar todo e ir más allá! 

93
00:15:32,180 --> 00:15:36,960
Pero el zen no es ascetismo, ni mortificación. 

94
00:15:37,920 --> 00:15:41,460
Debemos para la vida cósmica. 

95
00:15:41,680 --> 00:15:45,900
No debemos poner límites. 

96
00:15:46,320 --> 00:15:48,780
De lo contrario, la vida se vuelve estrecha. 

97
00:15:52,340 --> 00:15:55,880
Mantenerse en contacto con Japón 

98
00:18:11,940 --> 00:18:15,640
Sobre la educación de los niños … 

99
00:18:15,980 --> 00:18:27,960
En los tiempos modernos, nos centramos solo en el lado intelectual de la educación. 

100
00:18:28,360 --> 00:18:35,880
También debemos ver el lado físico, la tensión muscular correcta. 

101
00:18:38,600 --> 00:18:45,740
Además, debemos concentrarnos en los aspectos mundanos de la vida diaria. 

102
00:18:46,100 --> 00:18:50,560
La concentración y la postura física son muy importantes. 

103
00:18:50,960 --> 00:18:59,200
No debemos ser descuidados mientras comemos, por ejemplo. 

104
00:18:59,740 --> 00:19:04,200
Concentrarse en la vida diaria es fundamental. 

105
00:19:04,620 --> 00:19:10,920
¡Incluso nuestra postura mientras dormimos es crítica! 

106
00:19:11,860 --> 00:19:19,460
Los maestros de escuela no prestan suficiente atención a la postura física. 

107
00:19:19,800 --> 00:19:23,920
Solo les importa el lado intelectual. 

108
00:19:24,220 --> 00:19:27,679
¿Por qué los niños no están bien educados? 

109
00:19:28,260 --> 00:19:30,680
¿Es la educación demasiado fácil? 

110
00:19:31,200 --> 00:19:36,100
¡Volvemos a los animales en los tiempos modernos! 

111
00:19:37,160 --> 00:19:46,380
Todo es demasiado fácil … 

112
00:19:46,660 --> 00:19:50,040
Manejamos autos y ya no queremos caminar … 

113
00:19:50,420 --> 00:20:00,160
La educación familiar también es demasiado fácil. Es un error. 

114
00:20:01,360 --> 00:20:09,479
Cálido, comida. Demasiado "softy-softy"! 

115
00:20:10,600 --> 00:20:19,320
Nuestro cuerpo se debilita. 

116
00:20:21,540 --> 00:20:26,180
¿Buscamos la libertad de los animales? 

117
00:20:26,540 --> 00:20:28,540
Los animales salvajes son fuertes. 

118
00:20:28,940 --> 00:20:40,340
Los animales cautivos y bien alimentados en los zoológicos son débiles. 

119
00:20:40,680 --> 00:20:42,680
Carecen de actividad. 

120
00:20:45,200 --> 00:20:47,700
¡Los animales salvajes son fuertes! 

121
00:20:56,140 --> 00:20:59,100
La inteligencia es importante. 

122
00:20:59,360 --> 00:21:01,100
Pero no es suficiente. 

123
00:21:01,420 --> 00:21:06,720
También necesitamos un cuerpo fuerte y una sabiduría superior. 

124
00:21:09,080 --> 00:21:12,800
Creando el templo Gendronière 

125
00:23:45,260 --> 00:23:50,540
Has traído a Europa el fruto del zen para que la gente pueda probarlo. 

126
00:23:50,820 --> 00:24:05,540
India, China, Japón tienen la verdadera esencia de la cultura espiritual. 

127
00:24:07,480 --> 00:24:18,000
Ahora debemos fusionar las civilizaciones asiática y europea. 

128
00:24:23,660 --> 00:24:31,000
El zen es la esencia de la cultura asiática. 

129
00:24:31,760 --> 00:24:39,580
Si practicas Zen, entenderás las civilizaciones asiáticas. 

130
00:24:40,660 --> 00:24:55,280
¿Habrá un renacimiento espiritual a través del encuentro de la tradición occidental y el zen? 

131
00:24:55,620 --> 00:24:59,780
Creo que sí. Yo espero que sí. 

132
00:25:00,380 --> 00:25:04,780
Es importante que todos se conviertan en Dios. 

133
00:25:05,120 --> 00:25:07,120
¿Cuál es la forma de lograr esto? 

134
00:25:07,400 --> 00:25:11,320
Si practicas zazen, puedes convertirte en Dios. 

135
00:25:11,660 --> 00:25:21,300
¿Es el dios verdadero el dios natural en nosotros? 

136
00:25:21,520 --> 00:25:29,520
Sí, todos deben crear al dios verdadero. 

137
00:25:31,200 --> 00:25:35,180
La ultima enseñanza 

138
00:25:36,600 --> 00:25:40,360
La filosofía zen es muy amplia. 

139
00:25:40,800 --> 00:25:44,980
¡Debes aceptar las contradicciones! 

140
00:25:45,420 --> 00:25:47,440
¿Vida y muerte, por ejemplo? 

141
00:25:47,680 --> 00:25:50,400
¡Sí, vida y muerte! 

142
00:28:10,080 --> 00:28:27,360
Dedicado a Mokudo Taisen Deshimaru (1914-1982) 

