1
00:00:00,280 --> 00:00:03,640
Vita di tutti i giorni 

2
00:00:03,840 --> 00:00:06,460
Un giorno 

3
00:00:07,200 --> 00:00:12,320
Durante il giorno dobbiamo interrompere ciò che facciamo regolarmente. 

4
00:00:13,040 --> 00:00:15,320
Ferma il suo lavoro. 

5
00:00:15,780 --> 00:00:18,780
E dai. 

6
00:00:19,020 --> 00:00:22,220
Nel suo lavoro, nella vita di tutti i giorni, trovi la mente. 

7
00:00:22,560 --> 00:00:24,720
Bastano solo due respiri. 

8
00:00:25,060 --> 00:00:26,340
Lavori alla tua scrivania … 

9
00:00:26,680 --> 00:00:28,820
Basta raddrizzarlo. 

10
00:00:29,340 --> 00:00:32,760
E respira. 

11
00:00:41,120 --> 00:00:49,260
E per tornare immediatamente al suo essere. 

12
00:00:50,960 --> 00:00:54,140
Conoscevo una comunità chiamata Lanza del Vasto. 

13
00:00:55,000 --> 00:00:57,240
Lavorano molto duramente. 

14
00:00:57,580 --> 00:01:00,400
Lavorano la terra. 

15
00:01:00,660 --> 00:01:02,660
Costruiscono la loro casa. 

16
00:01:02,880 --> 00:01:04,540
Fanno molto lavoro manuale. 

17
00:01:04,740 --> 00:01:06,040
Lavorano tutto il giorno. 

18
00:01:06,300 --> 00:01:07,500
Si alzano alle 5 del mattino 

19
00:01:07,720 --> 00:01:08,820
Dicono le loro preghiere. 

20
00:01:09,080 --> 00:01:10,840
Stanno facendo colazione. 

21
00:01:11,160 --> 00:01:12,420
Vanno a lavorare. 

22
00:01:12,780 --> 00:01:15,820
Una campana suona ogni ora. 

23
00:01:16,180 --> 00:01:18,060
Tutti devono smettere di lavorare. 

24
00:01:18,720 --> 00:01:25,520
E rifocalizzare per un minuto. 

25
00:01:26,060 --> 00:01:28,960
Ognuno a modo suo. 

26
00:01:34,080 --> 00:01:36,040
E poi tornano indietro. 

27
00:01:36,560 --> 00:01:39,160
Quindi non c’è solo zazen. 

28
00:01:39,540 --> 00:01:42,100
Esistono molti modi per tornare a te stesso. 

29
00:01:42,640 --> 00:01:45,220
La vita quotidiana dei monaci Zen … 

30
00:01:45,540 --> 00:01:48,820
Viviamo la nostra vita per lo zazen. 

31
00:01:49,080 --> 00:01:52,150
Per raccogliere questa coscienza, questa conoscenza. 

32
00:01:52,440 --> 00:01:53,580
Quell’energia. 

33
00:01:54,880 --> 00:01:58,780
Al momento dell’esperienza di zazen. 

34
00:01:58,900 --> 00:02:03,120
Non si tratta di rinunciare alla tua vita per Zazen. 

35
00:02:03,380 --> 00:02:05,400
In uno spirito settario. 

36
00:02:05,740 --> 00:02:07,680
Non è un’ideologia. 

37
00:02:07,900 --> 00:02:10,380
Non è "-ismo". 

38
00:02:10,840 --> 00:02:14,780
È "Vivo la mia vita in modo tale da poter fare questa esperienza". 

39
00:02:15,060 --> 00:02:16,560
"Il più profondo possibile." 

40
00:02:16,900 --> 00:02:18,980
Questa è un’esperienza di meditazione zazen. 

41
00:02:19,220 --> 00:02:25,060
Quando sei nello zazen, hai l’opportunità di cambiare tutta la tua vita. 

42
00:02:25,400 --> 00:02:27,900
Questo è lo zazen! 

43
00:02:28,340 --> 00:02:31,980
Non è un modo piccolo per sentirsi bene. 

44
00:02:32,500 --> 00:02:35,040
Zen nella vita di tutti i giorni … 

45
00:02:35,640 --> 00:02:39,200
Anche nel profitto e nel lavoro … 

46
00:02:39,480 --> 00:02:41,500
"Mushotoku", (senza obiettivo o spirito di profitto) … 

47
00:02:41,760 --> 00:02:43,140
questo è il segreto. 

48
00:02:43,340 --> 00:02:46,320
In altre parole, rimani concentrato sulla tua essenza. 

49
00:02:46,540 --> 00:02:48,880
Quindi possiamo ottenere tutto. 

50
00:02:49,080 --> 00:02:50,360
Abbiamo tutto. 

51
00:02:50,880 --> 00:02:55,280
Qual è la differenza tra la vita di un maestro e quella di un essere ordinario? 

52
00:02:55,660 --> 00:02:59,520
Non esiste una cosa come ordinaria. 

53
00:02:59,840 --> 00:03:05,780
Ci sono solo creature straordinarie. 

54
00:03:06,660 --> 00:03:14,900
La cosa brutta è che le persone non sanno di essere straordinarie. 

55
00:03:16,420 --> 00:03:22,260
Non mi piace la frase "solo essere"! 

